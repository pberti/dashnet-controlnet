﻿using IOT.Domain;
using IOT.ORM;
using NHibernate;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace IOT.Services
{
    public class ServOTs : BaseService
    {
        private Repositorio<OTs> oRepo = null;

        public ServOTs(ISession pSession) : base(pSession)
        {
            this.oRepo = new Repositorio<OTs>(this.oSession);
        }

        #region ABM OTs
        public long AgregarOTs(OTs pObj)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                this.oRepo.Agregar(pObj);
                transactionScope.Complete();
            }
            return pObj.Id;
        }

        public int ActualizarOTs(OTs pObj)
        {
            int result;
            using (TransactionScope transactionScope = new TransactionScope())
            {
                int num = this.oRepo.Actualizar(pObj);
                transactionScope.Complete();
                result = num;
            }
            return result;
        }

        #endregion

        public OTs TraerPorIDOT(long pId)
        {
            return this.oRepo.TraerUnico((OTs p) => p.Id_OT == pId);
        }

        public List<string> TraerProyectos()
        {
            return (from p in this.oRepo.TraerTodos()
                    orderby p.Proyecto_Nombre
                    select p.Proyecto_Nombre).Distinct<string>().ToList<string>();
        }

        public List<string> TraerCC()
        {
            return (from p in this.oRepo.TraerTodos()
                    orderby p.Proyecto_CC
                    select p.Proyecto_CC).Distinct<string>().ToList<string>();
        }

        public IQueryable<OTs> TraerTodosOTs_MesInicioVersion(int pMes, int pYear)
        {
            return this.oRepo.TraerTodos();
        }

        public List<OTs> TraerOTs_Agrupadas(int pMes, int pYear, bool pProducto, bool pCC, bool pProyectoPadre, bool pProyecto, bool pVersion, string pFiltro)
        {
            int year = DateTime.Now.Year;
            string text = "SELECT *, count(*) ";
            string text2 = " FROM cnet_otdash";
            string text3 = string.Concat(new object[]
            {
                " WHERE ((Month(Version_Fecha_Inicio) = ",
                pMes,
                " AND YEAR(Version_Fecha_Inicio) = ",
                pYear,
                ") OR (Month(Version_Fecha_Est_FiN) = ",
                pMes,
                " AND YEAR(Version_Fecha_Est_FiN) = ",
                pYear,
                "))"
            });
            bool flag = pFiltro.Length > 0;
            if (flag)
            {
                text3 += pFiltro;
            }
            string text4 = "";
            string text5 = " Order by ";
            if (pProducto)
            {
                text += ", version_producto as pProducto ";
                bool flag2 = text4.Length == 0;
                if (flag2)
                {
                    text4 += " GROUP BY version_producto";
                }
                else
                {
                    text4 += ",version_producto";
                }
                bool flag3 = text5 == " Order by ";
                if (flag3)
                {
                    text5 += " version_producto";
                }
                else
                {
                    text5 += ", version_producto";
                }
            }
            if (pCC)
            {
                text += ", Proyecto_CC as pCC ";
                bool flag4 = text4.Length == 0;
                if (flag4)
                {
                    text4 += " GROUP BY Proyecto_CC";
                }
                else
                {
                    text4 += ",Proyecto_CC";
                }
                bool flag5 = text5 == " Order by ";
                if (flag5)
                {
                    text5 += " Proyecto_CC";
                }
                else
                {
                    text5 += ", Proyecto_CC";
                }
            }
            if (pProyectoPadre)
            {
                text += ", Proyecto_Padre_Nombre as pProyectoPadre ";
                bool flag6 = text4.Length == 0;
                if (flag6)
                {
                    text4 += " GROUP BY Proyecto_Padre_Nombre";
                }
                else
                {
                    text4 += ",Proyecto_Padre_Nombre";
                }
                bool flag7 = text5 == " Order by ";
                if (flag7)
                {
                    text5 += " Proyecto_Padre_Nombre";
                }
                else
                {
                    text5 += ", Proyecto_Padre_Nombre";
                }
            }
            if (pProyecto)
            {
                text += ", Proyecto_Nombre as pProyecto ";
                bool flag8 = text4.Length == 0;
                if (flag8)
                {
                    text4 += " GROUP BY Proyecto_Nombre";
                }
                else
                {
                    text4 += ",Proyecto_Nombre";
                }
                bool flag9 = text5 == " Order by ";
                if (flag9)
                {
                    text5 += " Proyecto_Nombre";
                }
                else
                {
                    text5 += ",Proyecto_Nombre";
                }
            }
            if (pVersion)
            {
                text += ", Version_Nombre as pVersion ";
                bool flag10 = text4.Length == 0;
                if (flag10)
                {
                    text4 += " GROUP BY Version_Nombre";
                }
                else
                {
                    text4 += ",Version_Nombre";
                }
                bool flag11 = text5 == " Order by ";
                if (flag11)
                {
                    text5 += " Version_Nombre";
                }
                else
                {
                    text5 += ", Version_Nombre";
                }
            }
            string text6 = string.Concat(new string[]
            {
                text,
                text2,
                text3,
                text4,
                text5
            });
            ISQLQuery iSQLQuery = this.oRepo.Session.CreateSQLQuery(text6);
            IList list = iSQLQuery.List();
            List<OTs> list2 = new List<OTs>();
            foreach (object[] array in list)
            {
                OTs oTs = new OTs();
                oTs.Id = Convert.ToInt64(array[0].ToString());
                oTs.Id_OT = Convert.ToInt64(array[1].ToString());
                oTs.OT_Asunto = array[2].ToString();
                bool flag12 = array[3] == null;
                if (flag12)
                {
                    oTs.OT_Horas_Estimadas = "0";
                }
                else
                {
                    oTs.OT_Horas_Estimadas = array[3].ToString();
                }
                oTs.Id_Estado = Convert.ToInt64(array[0].ToString());
                oTs.OT_Estado = array[5].ToString();
                oTs.OT_Cerrada = array[6].ToString();
                bool flag13 = oTs.OT_Cerrada == "1";
                if (flag13)
                {
                    oTs.OT_Fecha_Cierre = Convert.ToDateTime(array[7].ToString());
                }
                oTs.Proyecto_Nombre = array[8].ToString();
                oTs.Proyecto_CC = array[9].ToString();
                oTs.Proyecto_Descipcion = array[10].ToString();
                oTs.Proyecto_Padre_Nombre = array[11].ToString();
                oTs.Version_Proyecto = array[12].ToString();
                oTs.Version_Nombre = array[13].ToString();
                oTs.Version_Fecha_Inicio = Convert.ToDateTime(array[14].ToString());
                oTs.Version_Fecha_Est_FiN = Convert.ToDateTime(array[15].ToString());
                oTs.Version_Producto = array[16].ToString();
                oTs.OT_Cant_Total = Convert.ToInt64(array[17].ToString());
                int num = 0;
                string text7 = text3;
                if (pProducto)
                {
                    int num2 = num;
                    num = num2 + 1;
                    oTs.Version_Producto = array[17 + num].ToString();
                    text7 = text7 + " AND Version_Producto='" + oTs.Version_Producto + "'";
                }
                if (pCC)
                {
                    int num2 = num;
                    num = num2 + 1;
                    oTs.Proyecto_CC = array[17 + num].ToString();
                    text7 = text7 + " AND Proyecto_CC='" + oTs.Proyecto_CC + "'";
                }
                if (pProyectoPadre)
                {
                    int num2 = num;
                    num = num2 + 1;
                    oTs.Proyecto_Padre_Nombre = array[17 + num].ToString();
                    text7 = text7 + " AND Proyecto_Padre_Nombre='" + oTs.Proyecto_Padre_Nombre + "'";
                }
                if (pProyecto)
                {
                    int num2 = num;
                    num = num2 + 1;
                    oTs.Proyecto_Nombre = array[17 + num].ToString();
                    text7 = text7 + " AND Proyecto_Nombre='" + oTs.Proyecto_Nombre + "'";
                }
                if (pVersion)
                {
                    int num2 = num;
                    num = num2 + 1;
                    oTs.Version_Nombre = array[17 + num].ToString();
                    text7 = text7 + " AND Version_Nombre='" + oTs.Version_Nombre + "'";
                }
                oTs.OT_Cant_Cerradas = this.TraerOtCerradas(text2, text7);
                oTs.OT_Cant_Abiertas = oTs.OT_Cant_Total - oTs.OT_Cant_Cerradas;
                oTs.OT_Cant_Pendientes = this.TraerOtSegunEstado(text2, text7, "1,11");
                oTs.OT_Cant_Progreso = this.TraerOtSegunEstado(text2, text7, "2,13");
                oTs.OT_Cant_Finalizadas = this.TraerOtSegunEstado(text2, text7, "9");
                oTs.OT_Porc_Cerradas = (oTs.OT_Cant_Cerradas * 100L / oTs.OT_Cant_Total).ToString();
                oTs.Version_FechaFinal = oTs.Version_Fecha_Real_FiN.ToShortDateString();
                oTs.Version_FechaInicio = oTs.Version_Fecha_Inicio.ToShortDateString();
                oTs.Version_FechaEstFinal = oTs.Version_Fecha_Est_FiN.ToShortDateString();
                if (pVersion)
                {
                    oTs.Version_Cantidad = 1L;
                }
                else
                {
                    oTs.Version_Cantidad = this.CantidadVersiones(text2, text7);
                }
                oTs.OT_Horas_Usadas_Total = this.CantidadHoras_Usadas(true, text2, text7, pMes, pYear);
                oTs.OT_Horas_Usadas_Mes = this.CantidadHoras_Usadas(false, text2, text7, pMes, pYear);
                oTs.OT_Horas_Estimadas_Total = this.CantidadHoras_Estimadas(text2, text7);
                bool flag14 = oTs.OT_Horas_Estimadas_Total != "0";
                if (flag14)
                {
                    double num3 = Convert.ToDouble(oTs.OT_Horas_Estimadas_Total);
                    double num4 = Convert.ToDouble(oTs.OT_Horas_Usadas_Total);
                    oTs.OT_Horas_Porc_Consumido = (num4 / num3 * 100.0).ToString("0.##") + "%";
                }
                else
                {
                    bool flag15 = oTs.OT_Horas_Estimadas_Total != "0" && oTs.OT_Horas_Usadas_Total != "0";
                    if (flag15)
                    {
                        oTs.OT_Horas_Porc_Consumido = "0";
                    }
                    else
                    {
                        oTs.OT_Horas_Porc_Consumido = "Sin Est.";
                    }
                }
                oTs.OT_Porc_Cerradas = this.DesviacionOT_Cerradas_HorasEstimadas(text2, text3, pMes, pYear).ToString();
                list2.Add(oTs);
            }
            return list2.ToList<OTs>();
        }

        public List<OTs> TraerOTs_Agrupadas(DateTime pFechaDesde, DateTime pFechaHasta, bool pProducto, bool pCC, bool pProyectoPadre, bool pProyecto, bool pVersion, string pFiltro)
        {
            string text = "SELECT *, count(*) ";
            string text2 = " FROM cnet_otdash";
            string text3 = string.Concat(new string[]
            {
                " WHERE ((Version_Fecha_Est_FiN >= '",
                pFechaDesde.ToString("yyyy-MM-dd"),
                "') AND  (Version_Fecha_Est_FiN <= '",
                pFechaHasta.ToString("yyyy-MM-dd"),
                "')) AND VERSION_ID IS NOT NULL "
            });
            bool flag = pFiltro.Length > 0;
            if (flag)
            {
                text3 += pFiltro;
            }
            string text4 = "";
            string text5 = " Order by ";
            if (pProducto)
            {
                text += ", version_producto as pProducto ";
                bool flag2 = text4.Length == 0;
                if (flag2)
                {
                    text4 += " GROUP BY version_producto";
                }
                else
                {
                    text4 += ",version_producto";
                }
                bool flag3 = text5 == " Order by ";
                if (flag3)
                {
                    text5 += " version_producto";
                }
                else
                {
                    text5 += ", version_producto";
                }
            }
            if (pCC)
            {
                text += ", Proyecto_CC as pCC ";
                bool flag4 = text4.Length == 0;
                if (flag4)
                {
                    text4 += " GROUP BY Proyecto_CC";
                }
                else
                {
                    text4 += ",Proyecto_CC";
                }
                bool flag5 = text5 == " Order by ";
                if (flag5)
                {
                    text5 += " Proyecto_CC";
                }
                else
                {
                    text5 += ", Proyecto_CC";
                }
            }
            if (pProyectoPadre)
            {
                text += ", Proyecto_Padre_Nombre as pProyectoPadre ";
                bool flag6 = text4.Length == 0;
                if (flag6)
                {
                    text4 += " GROUP BY Proyecto_Padre_Nombre";
                }
                else
                {
                    text4 += ",Proyecto_Padre_Nombre";
                }
                bool flag7 = text5 == " Order by ";
                if (flag7)
                {
                    text5 += " Proyecto_Padre_Nombre";
                }
                else
                {
                    text5 += ", Proyecto_Padre_Nombre";
                }
            }
            if (pProyecto)
            {
                text += ", Proyecto_Nombre as pProyecto ";
                bool flag8 = text4.Length == 0;
                if (flag8)
                {
                    text4 += " GROUP BY Proyecto_Nombre";
                }
                else
                {
                    text4 += ",Proyecto_Nombre";
                }
                bool flag9 = text5 == " Order by ";
                if (flag9)
                {
                    text5 += " Proyecto_Nombre";
                }
                else
                {
                    text5 += ",Proyecto_Nombre";
                }
            }
            if (pVersion)
            {
                text += ", Version_Nombre as pVersion ";
                bool flag10 = text4.Length == 0;
                if (flag10)
                {
                    text4 += " GROUP BY Version_Nombre";
                }
                else
                {
                    text4 += ",Version_Nombre";
                }
                bool flag11 = text5 == " Order by ";
                if (flag11)
                {
                    text5 += " Version_Nombre";
                }
                else
                {
                    text5 += ", Version_Nombre";
                }
            }
            string text6 = string.Concat(new string[]
            {
                text,
                text2,
                text3,
                text4,
                text5
            });
            ISQLQuery iSQLQuery = this.oRepo.Session.CreateSQLQuery(text6);
            IList list = iSQLQuery.List();
            List<OTs> list2 = new List<OTs>();
            foreach (object[] array in list)
            {
                OTs oTs = new OTs();
                oTs.Id = Convert.ToInt64(array[0].ToString());
                oTs.Id_OT = Convert.ToInt64(array[1].ToString());
                oTs.OT_Asunto = array[2].ToString();
                bool flag12 = array[3] == null;
                if (flag12)
                {
                    oTs.OT_Horas_Estimadas = "0";
                }
                else
                {
                    oTs.OT_Horas_Estimadas = array[3].ToString();
                }
                oTs.Id_Estado = Convert.ToInt64(array[0].ToString());
                oTs.OT_Estado = array[5].ToString();
                oTs.OT_Cerrada = array[6].ToString();
                bool flag13 = oTs.OT_Cerrada == "1";
                if (flag13)
                {
                    oTs.OT_Fecha_Cierre = Convert.ToDateTime(array[7].ToString());
                }
                oTs.Proyecto_Nombre = array[8].ToString();
                oTs.Proyecto_CC = array[9].ToString();
                oTs.Proyecto_Descipcion = array[10].ToString();
                oTs.Proyecto_Padre_Nombre = array[11].ToString();
                oTs.Version_Proyecto = array[12].ToString();
                oTs.Version_Nombre = array[13].ToString();
                oTs.Version_Fecha_Inicio = Convert.ToDateTime(array[14].ToString());
                oTs.Version_Fecha_Est_FiN = Convert.ToDateTime(array[15].ToString());
                bool flag14 = array[16] == null;
                if (flag14)
                {
                    oTs.Version_Fecha_Real_FiN = DateTime.MinValue;
                }
                else
                {
                    oTs.Version_Fecha_Real_FiN = Convert.ToDateTime(array[16].ToString());
                }
                oTs.Version_Producto = array[17].ToString();
                oTs.Version_Id = array[18].ToString();
                oTs.OT_Cant_Total = Convert.ToInt64(array[19].ToString());
                int num = 0;
                string text7 = text3;
                if (pProducto)
                {
                    int num2 = num;
                    num = num2 + 1;
                    oTs.Version_Producto = array[19 + num].ToString();
                    text7 = text7 + " AND Version_Producto='" + oTs.Version_Producto + "'";
                }
                if (pCC)
                {
                    int num2 = num;
                    num = num2 + 1;
                    oTs.Proyecto_CC = array[19 + num].ToString();
                    text7 = text7 + " AND Proyecto_CC='" + oTs.Proyecto_CC + "'";
                }
                if (pProyectoPadre)
                {
                    int num2 = num;
                    num = num2 + 1;
                    oTs.Proyecto_Padre_Nombre = array[19 + num].ToString();
                    text7 = text7 + " AND Proyecto_Padre_Nombre='" + oTs.Proyecto_Padre_Nombre + "'";
                }
                if (pProyecto)
                {
                    int num2 = num;
                    num = num2 + 1;
                    oTs.Proyecto_Nombre = array[19 + num].ToString();
                    text7 = text7 + " AND Proyecto_Nombre='" + oTs.Proyecto_Nombre + "'";
                }
                if (pVersion)
                {
                    int num2 = num;
                    num = num2 + 1;
                    oTs.Version_Nombre = array[19 + num].ToString();
                    text7 = text7 + " AND Version_Nombre='" + oTs.Version_Nombre + "'";
                }
                oTs.OT_Cant_Cerradas = this.TraerOtCerradas(text2, text7);
                oTs.OT_Cant_Abiertas = oTs.OT_Cant_Total - oTs.OT_Cant_Cerradas;
                oTs.OT_Cant_Pendientes = this.TraerOtSegunEstado(text2, text7, "1,11");
                oTs.OT_Cant_Progreso = this.TraerOtSegunEstado(text2, text7, "2,13");
                oTs.OT_Cant_Finalizadas = this.TraerOtSegunEstado(text2, text7, "9");
                oTs.OT_Porc_Cerradas = (oTs.OT_Cant_Cerradas * 100L / oTs.OT_Cant_Total).ToString();
                oTs.Version_FechaFinal = oTs.Version_Fecha_Real_FiN.ToShortDateString();
                oTs.Version_FechaInicio = oTs.Version_Fecha_Inicio.ToShortDateString();
                oTs.Version_FechaEstFinal = oTs.Version_Fecha_Est_FiN.ToShortDateString();
                if (pVersion)
                {
                    oTs.Version_Cantidad = 1L;
                }
                else
                {
                    oTs.Version_Cantidad = this.CantidadVersiones(text2, text7);
                }
                oTs.OT_Horas_Usadas_Total = this.CantidadHoras_Usadas(true, text2, text7);
                oTs.OT_Horas_Usadas_Mes = this.CantidadHoras_Usadas(false, text2, text7);
                oTs.OT_Horas_Estimadas_Total = this.CantidadHoras_Estimadas(text2, text7);
                bool flag15 = oTs.OT_Horas_Estimadas_Total != "0";
                if (flag15)
                {
                    double num3 = Convert.ToDouble(oTs.OT_Horas_Estimadas_Total);
                    double num4 = Convert.ToDouble(oTs.OT_Horas_Usadas_Total);
                    oTs.OT_Horas_Porc_Consumido = (num4 / num3 * 100.0).ToString("0.##") + "%";
                }
                else
                {
                    bool flag16 = oTs.OT_Horas_Estimadas_Total != "0" && oTs.OT_Horas_Usadas_Total != "0";
                    if (flag16)
                    {
                        oTs.OT_Horas_Porc_Consumido = "0";
                    }
                    else
                    {
                        oTs.OT_Horas_Porc_Consumido = "Sin Est.";
                    }
                }
                list2.Add(oTs);
            }
            return list2.ToList<OTs>();
        }

        private double DesviacionOT_Cerradas_HorasEstimadas(string pQueryFrom, string pQueryWhere, int pMes, int pYear)
        {
            string str = "Select sum(OT_Horas_Estimadas)";
            pQueryWhere += " AND ( OT_Cerrada=1 OR id_estado in (9))";
            string text = str + pQueryFrom + pQueryWhere;
            ISQLQuery iSQLQuery = this.oRepo.Session.CreateSQLQuery(text);
            IList list = iSQLQuery.List();
            double result = Convert.ToDouble(list[0]);
            string text2 = this.CantidadHoras_Usadas(true, pQueryFrom, pQueryWhere, pMes, pYear);
            return result;
        }

        private double DesviacionOT_Cerradas_HorasEstimadas(string pQueryFrom, string pQueryWhere)
        {
            string str = "Select sum(OT_Horas_Estimadas)";
            pQueryWhere += " AND ( OT_Cerrada=1 OR id_estado in (9))";
            string text = str + pQueryFrom + pQueryWhere;
            ISQLQuery iSQLQuery = this.oRepo.Session.CreateSQLQuery(text);
            IList list = iSQLQuery.List();
            double result = Convert.ToDouble(list[0]);
            string text2 = this.CantidadHoras_Usadas(true, pQueryFrom, pQueryWhere);
            return result;
        }

        private string CantidadHoras_Estimadas(string pQueryFrom, string pQueryWhere)
        {
            string str = "Select sum(OT_Horas_Estimadas)";
            string text = str + pQueryFrom + pQueryWhere;
            ISQLQuery iSQLQuery = this.oRepo.Session.CreateSQLQuery(text);
            IList list = iSQLQuery.List();
            bool flag = list[0] == null;
            string result;
            if (flag)
            {
                result = "0";
            }
            else
            {
                result = list[0].ToString();
            }
            return result;
        }

        private string CantidadHoras_Usadas(bool pTotal, string pQueryFrom, string pQueryWhere, int pMes, int pYear)
        {
            string str;
            if (pTotal)
            {
                str = "SELECT sum(hours) FROM time_entries WHERE ISSUE_ID IN(Select Id_OT ";
            }
            else
            {
                str = string.Concat(new object[]
                {
                    "SELECT sum(hours) FROM time_entries WHERE TMONTH = ",
                    pMes,
                    " AND TYEAR = ",
                    pYear,
                    " AND ISSUE_ID IN(Select Id_OT "
                });
            }
            string text = str + pQueryFrom + pQueryWhere + ")";
            ISQLQuery iSQLQuery = this.oRepo.Session.CreateSQLQuery(text);
            IList list = iSQLQuery.List();
            bool flag = list[0] == null;
            string result;
            if (flag)
            {
                result = "0";
            }
            else
            {
                result = list[0].ToString();
            }
            return result;
        }

        private string CantidadHoras_Usadas(bool pTotal, string pQueryFrom, string pQueryWhere)
        {
            string str;
            if (pTotal)
            {
                str = "SELECT sum(hours) FROM time_entries WHERE ISSUE_ID IN(Select Id_OT ";
            }
            else
            {
                str = "SELECT sum(hours) FROM time_entries WHERE ISSUE_ID IN(Select Id_OT ";
            }
            string text = str + pQueryFrom + pQueryWhere + ")";
            ISQLQuery iSQLQuery = this.oRepo.Session.CreateSQLQuery(text);
            IList list = iSQLQuery.List();
            bool flag = list[0] == null;
            string result;
            if (flag)
            {
                result = "0";
            }
            else
            {
                result = list[0].ToString();
            }
            return result;
        }

        public long CantidadVersiones(string pQueryFrom, string pQueryWhere)
        {
            string str = "Select count(Distinct(Version_Nombre))";
            string text = str + pQueryFrom + pQueryWhere;
            ISQLQuery iSQLQuery = this.oRepo.Session.CreateSQLQuery(text);
            IList list = iSQLQuery.List();
            return Convert.ToInt64(list[0]);
        }

        public long TraerOtSegunEstado(string pQueryFrom, string pQueryWhere, string pIDs)
        {
            string str = "Select count(*)";
            pQueryWhere = pQueryWhere + " AND id_estado in (" + pIDs + ")";
            string text = str + pQueryFrom + pQueryWhere;
            ISQLQuery iSQLQuery = this.oRepo.Session.CreateSQLQuery(text);
            IList list = iSQLQuery.List();
            return Convert.ToInt64(list[0]);
        }

        public long TraerOtCerradas(string pQueryFrom, string pQueryWhere)
        {
            string str = "Select count(*)";
            pQueryWhere += " AND OT_Cerrada=1";
            string text = str + pQueryFrom + pQueryWhere;
            ISQLQuery iSQLQuery = this.oRepo.Session.CreateSQLQuery(text);
            IList list = iSQLQuery.List();
            return Convert.ToInt64(list[0]);
        }

        public List<OTs> Indicador_VersionesCerradas(DateTime pFechaDesde, DateTime pFechaHasta, string pFiltro)
        {
            string text = "SELECT a.proyecto_cc, a.Proyecto_Nombre, a.Version_Producto, a.Version_Id, a.Version_Nombre as pVersion,  a.Version_Fecha_Est_FiN, a.Version_Fecha_Real_FiN, b.value, sum(OT_Horas_Estimadas), count(*),a.Proyecto_Padre_Nombre, a.Version_Fecha_Inicio ";
            string text2 = " FROM cnet_otdash as a LEFT JOIN custom_values as b on (B.custom_field_id = 76 AND b.customized_id = a.Version_Id) ";
            string text3 = string.Concat(new string[]
            {
                " WHERE ((a.Version_Fecha_Real_FiN >= '",
                pFechaDesde.ToString("yyyy-MM-dd"),
                "') AND  (a.Version_Fecha_Real_FiN <= '",
                pFechaHasta.ToString("yyyy-MM-dd"),
                "')) AND a.VERSION_ID IS NOT NULL "
            });
            bool flag = pFiltro.Length > 0;
            if (flag)
            {
                text3 += pFiltro;
            }
            string text4 = " GROUP BY a.Version_Nombre ";
            string text5 = " ORDER BY a.proyecto_cc, a.Proyecto_Nombre, a.Version_Producto,Version_Id ";
            string text6 = string.Concat(new string[]
            {
                text,
                text2,
                text3,
                text4,
                text5
            });
            ISQLQuery iSQLQuery = this.oRepo.Session.CreateSQLQuery(text6);
            IList list = iSQLQuery.List();
            List<OTs> list2 = new List<OTs>();
            foreach (object[] array in list)
            {
                OTs oTs = new OTs();
                oTs.Proyecto_CC = array[0].ToString();
                oTs.Proyecto_Nombre = array[1].ToString();
                oTs.Version_Producto = array[2].ToString();
                oTs.Version_Id = array[3].ToString();
                oTs.Version_Nombre = array[4].ToString();
                oTs.Version_Fecha_Est_FiN = Convert.ToDateTime(array[5].ToString());
                oTs.Version_Fecha_Real_FiN = Convert.ToDateTime(array[6].ToString());
                oTs.Proyecto_Padre_Nombre = array[10].ToString();
                oTs.Version_Fecha_Inicio = Convert.ToDateTime(array[11].ToString());
                bool flag2 = oTs.Version_Fecha_Real_FiN > oTs.Version_Fecha_Est_FiN;
                if (flag2)
                {
                    oTs.Version_Cerrada_Ok = false;
                }
                else
                {
                    oTs.Version_Cerrada_Ok = true;
                }
                bool flag3 = array[7] != null;
                if (flag3)
                {
                    oTs.Version_AprobCliente = array[7].ToString().ToUpper();
                }
                else
                {
                    oTs.Version_AprobCliente = "PENDIENTE";
                }
                oTs.OT_Cant_Total = (long)Convert.ToInt32(array[9].ToString());
                text2 = " From cnet_otdash";
                string pQueryWhere = (" Where VERSION_ID = " + oTs.Version_Id) ?? "";
                oTs.OT_Cant_Cerradas = this.TraerOtCerradas(text2, pQueryWhere);
                oTs.OT_Cant_Abiertas = oTs.OT_Cant_Total - oTs.OT_Cant_Cerradas;
                oTs.OT_Cant_Pendientes = this.TraerOtSegunEstado(text2, pQueryWhere, "1,11");
                oTs.OT_Cant_Progreso = this.TraerOtSegunEstado(text2, pQueryWhere, "2,13");
                oTs.OT_Cant_Finalizadas = this.TraerOtSegunEstado(text2, pQueryWhere, "9");
                oTs.OT_Porc_Cerradas = (oTs.OT_Cant_Cerradas * 100L / oTs.OT_Cant_Total).ToString();
                oTs.Version_FechaFinal = oTs.Version_Fecha_Real_FiN.ToShortDateString();
                oTs.Version_FechaInicio = oTs.Version_Fecha_Inicio.ToShortDateString();
                oTs.Version_FechaEstFinal = oTs.Version_Fecha_Est_FiN.ToShortDateString();
                bool flag4 = array[8] != null;
                if (flag4)
                {
                    oTs.OT_Horas_Estimadas_Total = array[8].ToString();
                }
                else
                {
                    oTs.OT_Horas_Estimadas_Total = "0";
                }
                oTs.OT_Horas_Usadas_Total = this.CalculoHorasUsadas_PorVersion(oTs.Version_Id);
                bool flag5 = oTs.OT_Horas_Estimadas_Total != "0";
                if (flag5)
                {
                    double num = Convert.ToDouble(oTs.OT_Horas_Estimadas_Total);
                    double num2 = Convert.ToDouble(oTs.OT_Horas_Usadas_Total);
                    oTs.OT_Horas_Porc_Consumido = (num2 / num * 100.0).ToString("0.##") + "%";
                }
                else
                {
                    bool flag6 = oTs.OT_Horas_Estimadas_Total != "0" && oTs.OT_Horas_Usadas_Total != "0";
                    if (flag6)
                    {
                        oTs.OT_Horas_Porc_Consumido = "0";
                    }
                    else
                    {
                        oTs.OT_Horas_Porc_Consumido = "Sin Est.";
                    }
                }
                list2.Add(oTs);
            }
            return list2;
        }

        private string CalculoHorasUsadas_PorVersion(string pIdVersion)
        {
            string text = "SELECT sum(hours) FROM time_entries WHERE ISSUE_ID IN(Select Id_OT FROM cnet_otdash Where VERSION_ID = " + pIdVersion + ")";
            ISQLQuery iSQLQuery = this.oRepo.Session.CreateSQLQuery(text);
            bool flag = iSQLQuery.List().Count > 0;
            string result;
            if (flag)
            {
                IList list = iSQLQuery.List();
                bool flag2 = list[0] != null;
                if (flag2)
                {
                    result = list[0].ToString();
                }
                else
                {
                    result = "0";
                }
            }
            else
            {
                result = "0";
            }
            return result;
        }

        public List<OTs> Indicador_VersionesActivas(string pFiltro)
        {
            string specifier = "#,0.0#;(#,0.0#)";
            string text = "SELECT a.proyecto_cc, a.Proyecto_Nombre, a.Version_Producto, a.Version_Id, a.Version_Nombre as pVersion,  a.Version_Fecha_Est_FiN, a.Version_Fecha_Real_FiN, null, sum(OT_Horas_Estimadas), count(*),a.Proyecto_Padre_Nombre, a.Version_Fecha_Inicio ";
            string text2 = " FROM cnet_otdash as a  ";
            string text3 = " WHERE (a.Version_Fecha_Real_FiN is NULL or version_fecha_real_fin = date('0001-01-01')) AND a.VERSION_ID IS NOT NULL ";
            bool flag = pFiltro.Length > 0;
            if (flag)
            {
                text3 += pFiltro;
            }
            string text4 = " GROUP BY a.Version_Nombre ";
            string text5 = " ORDER BY a.proyecto_cc, a.Proyecto_Nombre, a.Version_Producto,Version_Id ";
            string text6 = string.Concat(new string[]
            {
                text,
                text2,
                text3,
                text4,
                text5
            });
            ISQLQuery iSQLQuery = this.oRepo.Session.CreateSQLQuery(text6);
            IList list = iSQLQuery.List();
            List<OTs> list2 = new List<OTs>();
            foreach (object[] array in list)
            {
                OTs oTs = new OTs();
                oTs.Proyecto_CC = array[0].ToString();
                oTs.Proyecto_Nombre = array[1].ToString();
                oTs.Version_Producto = array[2].ToString();
                oTs.Version_Id = array[3].ToString();
                oTs.Version_Nombre = array[4].ToString();
                oTs.Version_Fecha_Est_FiN = Convert.ToDateTime(array[5].ToString());
                bool flag2 = array[6] != null;
                if (flag2)
                {
                    oTs.Version_Fecha_Real_FiN = Convert.ToDateTime(array[6].ToString());
                }
                else
                {
                    oTs.Version_Fecha_Real_FiN = DateTime.MinValue;
                }
                oTs.Proyecto_Padre_Nombre = array[10].ToString();
                oTs.Version_Fecha_Inicio = Convert.ToDateTime(array[11].ToString());
                bool flag3 = oTs.Version_Fecha_Real_FiN > oTs.Version_Fecha_Est_FiN || oTs.Version_Fecha_Real_FiN == DateTime.MinValue;
                if (flag3)
                {
                    oTs.Version_Cerrada_Ok = false;
                }
                else
                {
                    oTs.Version_Cerrada_Ok = true;
                }
                bool flag4 = array[7] != null;
                if (flag4)
                {
                    oTs.Version_AprobCliente = array[7].ToString().ToUpper();
                }
                else
                {
                    oTs.Version_AprobCliente = "PENDIENTE";
                }
                oTs.OT_Cant_Total = (long)Convert.ToInt32(array[9].ToString());
                text2 = " From cnet_otdash";
                string pQueryWhere = (" Where VERSION_ID = " + oTs.Version_Id) ?? "";
                oTs.OT_Cant_Cerradas = this.TraerOtCerradas(text2, pQueryWhere);
                oTs.OT_Cant_Abiertas = oTs.OT_Cant_Total - oTs.OT_Cant_Cerradas;
                oTs.OT_Cant_Pendientes = this.TraerOtSegunEstado(text2, pQueryWhere, "1,11");
                oTs.OT_Cant_Progreso = this.TraerOtSegunEstado(text2, pQueryWhere, "2,13");
                oTs.OT_Cant_Finalizadas = this.TraerOtSegunEstado(text2, pQueryWhere, "9");
                oTs.OT_Porc_Cerradas = (oTs.OT_Cant_Cerradas * 100L / oTs.OT_Cant_Total).ToString();
                oTs.Version_FechaFinal = oTs.Version_Fecha_Real_FiN.ToShortDateString();
                oTs.Version_FechaInicio = oTs.Version_Fecha_Inicio.ToShortDateString();
                oTs.Version_FechaEstFinal = oTs.Version_Fecha_Est_FiN.ToShortDateString();
                bool flag5 = array[8] != null;
                if (flag5)
                {
                    double mTemp = Convert.ToDouble(array[8].ToString());
                    oTs.OT_Horas_Estimadas_Total = mTemp.ToString(specifier);
                }
                else
                {
                    oTs.OT_Horas_Estimadas_Total = "0";
                }
                oTs.OT_Horas_Usadas_Total = this.CalculoHorasUsadas_PorVersion(oTs.Version_Id);
                bool flag6 = oTs.OT_Horas_Estimadas_Total != "0";
                if (flag6)
                {
                    double num = Convert.ToDouble(oTs.OT_Horas_Estimadas_Total);
                    double num2 = Convert.ToDouble(oTs.OT_Horas_Usadas_Total);
                    oTs.OT_Horas_Porc_Consumido = (num2 / num * 100.0).ToString("0.##") + "%";
                }
                else
                {
                    bool flag7 = oTs.OT_Horas_Estimadas_Total != "0" && oTs.OT_Horas_Usadas_Total != "0";
                    if (flag7)
                    {
                        oTs.OT_Horas_Porc_Consumido = "0";
                    }
                    else
                    {
                        oTs.OT_Horas_Porc_Consumido = "Sin Est.";
                    }
                }
                list2.Add(oTs);
            }
            return list2;
        }

        public int ActualizacionDatosRedmine()
        {
            int num = 0;
            int result;
            try
            {
                string text = "SELECT i.id as Id_OT, i.subject as OT_Asunto, i.estimated_hours as OT_Horas_Estimadas, i.status_id as Id_Estado, st.name as OT_Estado, st.is_closed as OT_Cerrada, DATE_FORMAT(i.closed_on, '%Y-%m-%d') as OT_Fecha_Cierre, p.name as Proyecto_Nombre, cv4.value as Proyecto_CC, SUBSTR(p.description,1,200) as Proyecto_Descipcion, p2.name as Proyecto_Padre_Nombre, v.project_id as Version_Proyecto, v.name as Version_Nombre, DATE_FORMAT(cv.value, '%Y-%m-%d') as Version_Fecha_Inicio, DATE_FORMAT(cv3.value, '%Y-%m-%d') as Version_Fecha_Estimada_Fin, DATE_FORMAT(v.effective_date, '%Y-%m-%d') as Version_Fecha_Real_Fin, cv2.value as Version_Producto, v.Id as Version_Id FROM versions as v,custom_values as cv,custom_values as cv2,custom_values as cv3,custom_values as cv4,issues i,projects p,projects p2,issue_statuses as st WHERE v.id = cv.customized_id AND cv.custom_field_id = 71 AND v.id = cv2.customized_id AND cv2.custom_field_id = 70 AND v.id = cv3.customized_id AND cv3.custom_field_id = 72 AND v.project_id = cv4.customized_id AND cv4.custom_field_id = 37 AND i.fixed_version_id = v.id AND v.project_id = p.id AND i.status_id = st.id AND p.parent_id = p2.id AND date(i.updated_on) >= date(DATE_SUB(NOW(),INTERVAL 10 MONTH)) ORDER BY V.ID";
                ISQLQuery iSQLQuery = this.oRepo.Session.CreateSQLQuery(text);
                IList list = iSQLQuery.List();
                int count = iSQLQuery.List().Count;
                List<OTs> list2 = new List<OTs>();
                foreach (object[] array in list)
                {
                    OTs mOT = new OTs();
                    mOT.Id_OT = Convert.ToInt64(array[0]);
                    mOT.OT_Asunto = array[1].ToString();
                    bool flag = array[2] != null;
                    if (flag)
                    {
                        mOT.OT_Horas_Estimadas_Total = array[2].ToString();
                    }
                    else
                    {
                        mOT.OT_Horas_Estimadas_Total = "0";
                    }
                    mOT.OT_Horas_Estimadas = mOT.OT_Horas_Estimadas_Total;
                    mOT.Id_Estado = Convert.ToInt64(array[3]);
                    mOT.OT_Estado = array[4].ToString();
                    bool flag2 = array[5].ToString() == "True";
                    if (flag2)
                    {
                        mOT.OT_Cerrada = "1";
                    }
                    else
                    {
                        mOT.OT_Cerrada = "0";
                    }
                    bool flag3 = array[6] != null;
                    if (flag3)
                    {
                        bool flag4 = array[6].ToString().Length > 0;
                        if (flag4)
                        {
                            mOT.OT_Fecha_Cierre = Convert.ToDateTime(array[6].ToString());
                        }
                    }
                    mOT.Proyecto_Nombre = array[7].ToString();
                    mOT.Proyecto_CC = array[8].ToString();
                    mOT.Proyecto_Descipcion = array[9].ToString();
                    mOT.Proyecto_Padre_Nombre = array[10].ToString();
                    mOT.Version_Proyecto = array[11].ToString();
                    mOT.Version_Nombre = array[12].ToString();
                    bool flag5 = array[13] != null;
                    if (flag5)
                    {
                        bool flag6 = array[13].ToString().Length > 0;
                        if (flag6)
                        {
                            mOT.Version_Fecha_Inicio = Convert.ToDateTime(array[13].ToString());
                        }
                    }
                    bool flag7 = array[14] != null;
                    if (flag7)
                    {
                        bool flag8 = array[14].ToString().Length > 0;
                        if (flag8)
                        {
                            mOT.Version_Fecha_Est_FiN = Convert.ToDateTime(array[14].ToString());
                        }
                    }
                    bool flag9 = array[15] != null;
                    if (flag9)
                    {
                        bool flag10 = array[15].ToString().Length > 0;
                        if (flag10)
                        {
                            mOT.Version_Fecha_Real_FiN = Convert.ToDateTime(array[15].ToString());
                        }
                    }
                    mOT.Version_Producto = array[16].ToString();
                    mOT.Version_Id = array[17].ToString();
                    mOT.OT_Horas_Estimadas = mOT.OT_Horas_Estimadas.Replace(',', '.');
                    mOT.OT_Horas_Estimadas_Total = mOT.OT_Horas_Estimadas_Total.Replace(',', '.');
                    OTs oTs = this.oRepo.TraerUnico((OTs p) => p.Id_OT == mOT.Id_OT);
                    bool flag11 = oTs != null;
                    if (flag11)
                    {
                        mOT.Id = oTs.Id;
                        this.oRepo.Actualizar(mOT);
                    }
                    else
                    {
                        this.oRepo.Agregar(mOT);
                    }
                    int num2 = num;
                    num = num2 + 1;
                }
                this.BorrarOTDadasBajaRedmine();
                result = num;
            }
            catch (Exception var_25_45B)
            {
                result = num;
            }
            return result;
        }

        private void BorrarOTDadasBajaRedmine()
        {
            try
            {
                string text = "SELECT id, id FROM cnet_otdash WHERE Id_OT not in (SELECT id FROM ISSUES )";
                ISQLQuery iSQLQuery = this.oRepo.Session.CreateSQLQuery(text);
                IList list = iSQLQuery.List();
                int count = iSQLQuery.List().Count;
                List<OTs> list2 = new List<OTs>();
                foreach (object[] array in list)
                {
                    OTs entity = new OTs();
                    entity = this.oRepo.TraerPorId(Convert.ToInt64(array[0]));
                    int num = this.oRepo.Borrar(entity);
                }
            }
            catch (Exception var_10_9F)
            {
                throw;
            }
        }
    }
}
