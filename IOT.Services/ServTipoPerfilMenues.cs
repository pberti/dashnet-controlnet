﻿using IOT.Domain;
using IOT.ORM;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace IOT.Services
{
    public class ServTipoPerfilMenues : BaseService
    {
        private Repositorio<TipoPerfilMenues> oRepo = null;

        public ServTipoPerfilMenues(ISession pSession) : base(pSession)
        {
            this.oRepo = new Repositorio<TipoPerfilMenues>(this.oSession);
        }

        public long AgregarTipoPerfilMenues(TipoPerfilMenues pObj)
        {
            long id;
            try
            {
                using (TransactionScope transactionScope = new TransactionScope())
                {
                    this.oRepo.Agregar(pObj);
                    transactionScope.Complete();
                }
                id = pObj.Id;
            }
            catch (Exception)
            {
                throw new Exception("Error en el metodo AgregarTipoPerfilMenues");
            }
            return id;
        }

        public long AgregarNuevaListaPerfilMenu(List<TipoPerfilMenues> pListPerfilMenu)
        {
            long id;
            try
            {
                using (TransactionScope transactionScope = new TransactionScope())
                {
                    foreach (TipoPerfilMenues current in pListPerfilMenu)
                    {
                        this.oRepo.Agregar(current);
                    }
                    transactionScope.Complete();
                }
                id = pListPerfilMenu[0].Id;
            }
            catch (Exception)
            {
                throw new Exception("Error en el metodo AgregarNuevaListaPerfilMenu");
            }
            return id;
        }

        public int ActualizarTipoPerfilMenues(TipoPerfilMenues pObj)
        {
            int result;
            try
            {
                using (TransactionScope transactionScope = new TransactionScope())
                {
                    int num = this.oRepo.Actualizar(pObj);
                    transactionScope.Complete();
                    result = num;
                }
            }
            catch (Exception)
            {
                throw new Exception("Error en el metodo ActualizarTipoPerfilMenues");
            }
            return result;
        }

        public long BorrarTipoPerfilMenuPorPerfil(long pIdPerfil)
        {
            long pIdPerfil2;
            try
            {
                using (TransactionScope transactionScope = new TransactionScope())
                {
                    List<TipoPerfilMenues> list = this.oRepo.TraerTodos((TipoPerfilMenues p) => p.IdTipoPerfil == pIdPerfil).ToList<TipoPerfilMenues>();
                    foreach (TipoPerfilMenues current in list)
                    {
                        this.oRepo.Borrar(current);
                    }
                    transactionScope.Complete();
                    pIdPerfil2 = pIdPerfil;
                }
            }
            catch (Exception)
            {
                throw new Exception("Error en el metodo BorrarTipoPerfilMenuPorPerfil");
            }
            return pIdPerfil2;
        }

        public IQueryable<TipoPerfilMenues> TraerTodosTipoPefilMenues()
        {
            IQueryable<TipoPerfilMenues> result;
            try
            {
                IQueryable<TipoPerfilMenues> queryable = this.oRepo.TraerTodos();
                result = queryable;
            }
            catch (Exception)
            {
                throw new Exception("Error en el metodo TraerTodosTipoPefilMenues");
            }
            return result;
        }

        public IQueryable<TipoPerfilMenues> TraerTodosTipoPefilMenuesPorPerfil(long pIdPerfil)
        {
            IQueryable<TipoPerfilMenues> result;
            try
            {
                IQueryable<TipoPerfilMenues> queryable = this.oRepo.TraerTodos((TipoPerfilMenues p) => p.IdTipoPerfil == pIdPerfil);
                result = queryable;
            }
            catch (Exception)
            {
                throw new Exception("Error en el metodo TraerTodosTipoPefilMenues");
            }
            return result;
        }

        public TipoPerfilMenues TraerUnicoTipoPerfilMenues(long pId)
        {
            TipoPerfilMenues result;
            try
            {
                result = this.oRepo.TraerUnico((TipoPerfilMenues p) => p.Id == pId);
            }
            catch (Exception)
            {
                throw new Exception("Error en el metodo TraerUnicoTipoPerfilMenues");
            }
            return result;
        }
    }
}
