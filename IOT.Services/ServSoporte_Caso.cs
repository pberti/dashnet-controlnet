﻿using System;
using System.Linq;
using IOT.ORM;
using IOT.Domain;
using NHibernate;
using System.Transactions;

namespace IOT.Services
{
    public class ServSoporte_Caso : BaseService
    {
        private Repositorio<Soporte_Caso> oRepo = null;
        private Repositorio<Soporte_Estados> oRepoEstados = null;
        private Repositorio<Soporte_Resolucion> oRepoResol = null;
        private Repositorio<Soporte_Categoria> oRepoCategoria = null;
        private Repositorio<Soporte_Historial> oRepoHistorial = null;

        #region Constructores
        public ServSoporte_Caso(ISession pSession) : base(pSession)
        {
            oRepo = new Repositorio<Soporte_Caso>(oSession);
            oRepoEstados = new Repositorio<Soporte_Estados>(oSession);
            oRepoCategoria = new Repositorio<Soporte_Categoria>(oSession);
            oRepoResol = new Repositorio<Soporte_Resolucion>(oSession);
            oRepoHistorial = new Repositorio<Soporte_Historial>(oSession);
        }

        #endregion

        #region Soporte Caso

        #region Métodos ABM

        public long Agregar(Soporte_Caso pObj)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                oRepo.Agregar(pObj);
                scope.Complete();
            }
            return pObj.Id;
        }

        public int Actualizar(Soporte_Caso pObj)
        {

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    int res = oRepo.Actualizar(pObj);
                    scope.Complete();
                    return res;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error en el metodo Actualizar");
            }

        }
        #endregion

        #region Métodos de Consulta
        public IQueryable<Soporte_Caso> TraerTodos()
        {
            IQueryable<Soporte_Caso> lista = null;
            try
            {
                lista = oRepo.TraerTodos();
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodos");
            }
            finally
            {
                lista = null;
            }
        }

        public Soporte_Caso TraerUnicoXId(long pId)
        {
            return oRepo.TraerUnico(p => p.Id == pId);
        }
        #endregion

        #endregion

        #region Soporte Estados

        #region Métodos de Consulta
        public IQueryable<Soporte_Estados> TraerTodosEstados()
        {
            IQueryable<Soporte_Estados> lista = null;
            try
            {
                lista = oRepoEstados.TraerTodos().OrderBy(p => p.NroOrden);
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodos");
            }
            finally
            {
                lista = null;
            }
        }

        public Soporte_Estados TraerUnicoEstadoXId(long pId)
        {
            return oRepoEstados.TraerUnico(p => p.Id == pId);
        }
        #endregion

        #endregion

        #region Soporte Categorias

        #region Métodos de Consulta
        public IQueryable<Soporte_Categoria> TraerTodosCategoriasPadres()
        {
            IQueryable<Soporte_Categoria> lista = null;
            try
            {
                lista = oRepoCategoria.TraerTodos().Where(p => p.IdPadre == 0) .OrderBy(p => p.Nombre);
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodosCategoriasPadres");
            }
            finally
            {
                lista = null;
            }
        }
        public IQueryable<Soporte_Categoria> TraerTodosCategoriasHijos( long pIdPadre)
        {
            IQueryable<Soporte_Categoria> lista = null;
            try
            {
                lista = oRepoCategoria.TraerTodos().Where(p => p.IdPadre == pIdPadre).OrderBy(p => p.Nombre);
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodosCategoriasHijos");
            }
            finally
            {
                lista = null;
            }
        }

        public Soporte_Categoria TraerUnicoCategoriaXId(long pId)
        {
            return oRepoCategoria.TraerUnico(p => p.Id == pId);
        }
        #endregion

        #endregion

        #region Soporte Resolucion

        #region Métodos de Consulta
        public IQueryable<Soporte_Resolucion> TraerTodosResolucion()
        {
            IQueryable<Soporte_Resolucion> lista = null;
            try
            {
                lista = oRepoResol.TraerTodos().OrderBy(p => p.Nombre);
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodosResolucion");
            }
            finally
            {
                lista = null;
            }
        }

        public Soporte_Resolucion TraerUnicoResolucionXId(long pId)
        {
            return oRepoResol.TraerUnico(p => p.Id == pId);
        }
        #endregion

        #endregion

        #region Soporte Historial

        #region Métodos ABM

        public long AgregarHistorial(Soporte_Historial pObj)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                oRepoHistorial.Agregar(pObj);
                scope.Complete();
            }
            return pObj.Id;
        }

        public int ActualizarHistorial(Soporte_Historial pObj)
        {

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    int res = oRepoHistorial.Actualizar(pObj);
                    scope.Complete();
                    return res;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error en el metodo Actualizar");
            }

        }
        #endregion

        #region Métodos de Consulta
        public IQueryable<Soporte_Historial> TraerTodosHistorial()
        {
            IQueryable<Soporte_Historial> lista = null;
            try
            {
                lista = oRepoHistorial.TraerTodos();
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodos");
            }
            finally
            {
                lista = null;
            }
        }

        public IQueryable<Soporte_Historial> TraerTodosHistorialPorCaso(long pIdSoporteCaso)
        {
            IQueryable<Soporte_Historial> lista = null;
            try
            {
                lista = oRepoHistorial.TraerTodos().Where(p => p.IdSoporteCaso == pIdSoporteCaso);
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodos");
            }
            finally
            {
                lista = null;
            }
        }
        public Soporte_Historial TraerUnicoHistorialXId(long pId)
        {
            return oRepoHistorial.TraerUnico(p => p.Id == pId);
        }
        #endregion

        #endregion

    }
}
