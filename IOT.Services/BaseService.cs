﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using IOT.ORM;

namespace IOT.Services
{
    public class BaseService
    {
        public ISession oSession;

        public BaseService(ISession pSession)
        {
            oSession = pSession;
        }
    }
}
