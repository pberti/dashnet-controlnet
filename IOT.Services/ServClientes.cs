﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IOT.ORM;
using IOT.Domain;
using NHibernate;
using System.Transactions;

namespace IOT.Services
{
    public class ServClientes : BaseService
    {
        private Repositorio<Clientes> oRepo = null;

        private Repositorio<TempExcel> oRepoExcel = null;

        #region Constructores
        public ServClientes(ISession pSession) : base(pSession)
        {
            oRepo = new Repositorio<Clientes>(oSession);
            oRepoExcel = new Repositorio<TempExcel>(oSession);
        }

        #endregion

        #region Métodos ABM

        public long Agregar(Clientes pObj)
        {
                using (TransactionScope scope = new TransactionScope())
                {
                     oRepo.Agregar(pObj);
                     scope.Complete();
                }
                return pObj.Id;
        }

        public int Actualizar(Clientes pObj)
        {

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                     int res = oRepo.Actualizar(pObj);
                     scope.Complete();
                     return res;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error en el metodo Actualizar Cliente");
            }
            
        }




        #endregion

        #region Métodos de Consulta
        public IQueryable<Clientes> TraerTodos(bool? pSoloActivos)
        {
            IQueryable<Clientes> lista = null;
            try
            {
                lista = oRepo.TraerTodos().Where(e =>
                pSoloActivos == null || e.Inactivo == !pSoloActivos);
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodos Clientes");
            }
            finally
            {
                lista = null;
            }
        }

        public Clientes TraerUnicoXId(long pId)
        {
            return oRepo.TraerUnico(p => p.Id == pId);
        }
        #endregion

        #region TempExcel
        #region Métodos de Consulta
        public void ImportacionClientes()
        {
            IQueryable<TempExcel> lstClientesExcel = null;
            try
            {
                lstClientesExcel = oRepoExcel.TraerTodos();
                foreach (var item in lstClientesExcel)
                {
                    //Verifico si ya existe en la base de clientes actuales.
                    //Busco si ya existe
                    Clientes oCli = oRepo.TraerTodos().Where(p => p.Nombre.Contains(item.Cliente)).FirstOrDefault();
                    if (oCli == null)
                    {
                        oCli = new Clientes();
                        oCli.Id = 0;
                    }

                    oCli.Contacto = item.Contacto;
                    oCli.Correo = item.Correo;
                    oCli.Cuit = item.Cuit;
                    oCli.Domicilio = item.Domicilio;
                    oCli.FecUltimaModificacion = DateTime.Now;
                    oCli.IdUsuarioModifico = 1;
                    oCli.Inactivo = false;
                    oCli.Nombre = item.Cliente.ToUpper();
                    oCli.Provincia = item.Provincia;
                    oCli.Telefono = item.Telefono;

                    if (oCli.Id == 0)
                        oRepo.Agregar(oCli);
                    else
                        oRepo.Actualizar(oCli);
                }

            }
            catch (Exception e) 
            {
                throw new Exception("Error en el metodo TraerTodos Clientes");
            }
        }
        #endregion
        #endregion
    }
}
