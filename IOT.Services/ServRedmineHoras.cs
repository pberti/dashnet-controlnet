﻿using IOT.Domain;
using IOT.ORM;
using NHibernate;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace IOT.Services
{
    public class ServRedmineHoras : BaseService
    {
        private Repositorio<Usuario> oRepo = null;

        public ServRedmineHoras(ISession pSession) : base(pSession)
        {
            this.oRepo = new Repositorio<Usuario>(this.oSession);
        }

        public List<RedmineUser> Redmine_UsuarioActivos()
        {
            List<RedmineUser> list = new List<RedmineUser>();
            int year = DateTime.Now.Year;
            string str = "SELECT b.id,b.firstname, b.lastname, sum(a.hours) as cantidad  FROM time_entries as a, users as b";
            string str2 = " where a.user_id = b.id and b.status = 1  and a.tyear >= '" + year + "'";
            string str3 = " group by a.tyear, user_id having cantidad > 0 order by b.lastname desc;";
            string text = str + str2 + str3;
            ISQLQuery iSQLQuery = this.oRepo.Session.CreateSQLQuery(text);
            IList list2 = iSQLQuery.List();
            foreach (object[] array in list2)
            {
                list.Add(new RedmineUser
                {
                    Id = Convert.ToInt64(array[0].ToString()),
                    firstname = array[1].ToString(),
                    lastname = array[2].ToString()
                });
            }
            return list;
        }

        public List<RedmineUser> Redmine_UsuariosHorasIncompletas(int mYear, int mMonth)
        {
            List<RedmineUser> list = new List<RedmineUser>();
            List<RedmineUser> result;
            try
            {
                string str = "SELECT b.id,b.firstname, b.lastname, a.tweek,sum(a.hours) as cantidad FROM time_entries as a, users as b";
                string str2 = string.Concat(new object[]
                {
                    " where a.user_id = b.id and b.status = 1  and a.tyear = '",
                    mYear,
                    "' and a.tmonth = '",
                    mMonth,
                    "'"
                });
                string str3 = " group by a.tweek, user_id having cantidad > 0 and cantidad < 44 order by b.lastname, a.tweek desc;";
                string text = str + str2 + str3;
                ISQLQuery iSQLQuery = this.oRepo.Session.CreateSQLQuery(text);
                IList list2 = iSQLQuery.List();
                foreach (object[] array in list2)
                {
                    list.Add(new RedmineUser
                    {
                        Id = Convert.ToInt64(array[0].ToString()),
                        firstname = array[1].ToString(),
                        lastname = array[2].ToString(),
                        Semana = array[3].ToString(),
                        Cantidad = array[4].ToString()
                    });
                }
                result = list.ToList<RedmineUser>();
            }
            catch (Exception var_12_125)
            {
                throw;
            }
            return result;
        }

        public List<RedmineHoras> HorasPorEmpleadoCC(DateTime pFechaDesde, DateTime pFechaHasta)
        {
            List<RedmineHoras> list = new List<RedmineHoras>();
            string text = string.Concat(new string[]
            {
                "SELECT   u.id,u.firstname, u.lastname, cv.value, SUM(a.hours) FROM time_entries as a, users as u,  custom_values as cv WHERE (a.spent_on >= date('",
                pFechaDesde.ToString("yyyy-MM-dd"),
                "') AND a.spent_on <= date('",
                pFechaHasta.ToString("yyyy-MM-dd"),
                "')) AND(cv.custom_field_id = 37 AND cv.customized_id = a.project_id) AND u.id = a.user_id group by u.id , cv.value order by u.lastname, cv.value "
            });
            ISQLQuery iSQLQuery = this.oRepo.Session.CreateSQLQuery(text);
            IList list2 = iSQLQuery.List();
            foreach (object[] mRow in list2)
            { 
                    bool flag = !string.IsNullOrWhiteSpace(mRow[3].ToString());
                if (flag)
                {
                    RedmineHoras redmineHoras = new RedmineHoras();
                    bool flag2 = list.Count > 0;
                    if (flag2)
                    {
                        redmineHoras = (from p in list
                                        where p.IdUsuario == Convert.ToInt64(mRow[0].ToString())
                                        select p).SingleOrDefault<RedmineHoras>();
                    }
                    else
                    {
                        redmineHoras = null;
                    }
                    bool flag3 = redmineHoras == null;
                    uint num;
                    if (flag3)
                    {
                        redmineHoras = new RedmineHoras();
                        redmineHoras.IdUsuario = Convert.ToInt64(mRow[0].ToString());
                        redmineHoras.firstname = mRow[1].ToString();
                        redmineHoras.lastname = mRow[2].ToString();
                        string text2 = mRow[3].ToString();

                        if (text2 == "PHI")
                        {
                            redmineHoras.PHI = Convert.ToDouble(mRow[4].ToString());
                        }
                        else if (text2 == "SOP")
                        {
                            redmineHoras.SOP = Convert.ToDouble(mRow[4].ToString());
                        }
                        else if (text2 == "STP")
                        {
                            redmineHoras.STP = Convert.ToDouble(mRow[4].ToString());
                        }

                        if (text2 == "SSCC")
                        {
                            redmineHoras.SSCC = Convert.ToDouble(mRow[4].ToString());
                        }
                        else if (text2 == "PHP")
                        {
                            redmineHoras.PHP = Convert.ToDouble(mRow[4].ToString());
                        }

                        if (text2 == "IPI")
                        {
                            redmineHoras.IPI = Convert.ToDouble(mRow[4].ToString());
                        }
                        else if (text2 == "INP")
                        {
                            redmineHoras.INP = Convert.ToDouble(mRow[4].ToString());
                        }
                        redmineHoras.CCTOTAL += Convert.ToDouble(mRow[4].ToString());
                        list.Add(redmineHoras);
                    }
                    else
                    {
                        string text3 = mRow[3].ToString();

                        if (text3 == "PHI")
                        {
                            redmineHoras.PHI = Convert.ToDouble(mRow[4].ToString());
                        }
                        else if (text3 == "SOP")
                        {
                            redmineHoras.SOP = Convert.ToDouble(mRow[4].ToString());
                        }
                        else if (text3 == "STP")
                        {
                            redmineHoras.STP = Convert.ToDouble(mRow[4].ToString());
                        }

                        if (text3 == "SSCC")
                        {
                            redmineHoras.SSCC = Convert.ToDouble(mRow[4].ToString());
                        }
                        else if (text3 == "PHP")
                        {
                            redmineHoras.PHP = Convert.ToDouble(mRow[4].ToString());
                        }
                        if (text3 == "IPI")
                        {
                            redmineHoras.IPI = Convert.ToDouble(mRow[4].ToString());
                        }
                        else if (text3 == "INP")
                        {
                            redmineHoras.INP = Convert.ToDouble(mRow[4].ToString());
                        }
                        redmineHoras.CCTOTAL += Convert.ToDouble(mRow[4].ToString());
                    }
                }
            }
            return list.ToList<RedmineHoras>();
        }

        public List<RedmineHoras> HorasPorCentroCosto(DateTime pFechaDesde, DateTime pFechaHasta)
        {
            List<RedmineHoras> list = new List<RedmineHoras>();
            string text = string.Concat(new string[]
            {
                "SELECT cv.value as CC, SUM(a.hours) FROM time_entries as a, custom_values as cv WHERE (a.spent_on >= date('",
                pFechaDesde.ToString("yyyy-MM-dd"),
                "') AND a.spent_on <= date('",
                pFechaHasta.ToString("yyyy-MM-dd"),
                "')) AND(cv.custom_field_id = 37 AND cv.customized_id = a.project_id) group by cv.value order by CC"
            });
            ISQLQuery iSQLQuery = this.oRepo.Session.CreateSQLQuery(text);
            IList list2 = iSQLQuery.List();
            double num = 0.0;
            foreach (object[] array in list2)
            {
                bool flag = array[0].ToString().Trim() != "";
                if (flag)
                {
                    RedmineHoras redmineHoras = new RedmineHoras();
                    redmineHoras.Horas = Convert.ToDouble(array[1].ToString());
                    redmineHoras.CC = array[0].ToString();
                    list.Add(redmineHoras);
                    num += redmineHoras.Horas;
                }
            }
            list.Add(new RedmineHoras
            {
                Horas = num,
                CC = "Total General"
            });
            List<RedmineHoras> list3 = new List<RedmineHoras>();
            foreach (RedmineHoras current in list)
            {
                double porcentaje = current.Horas / num * 100.0;
                RedmineHoras redmineHoras2 = new RedmineHoras();
                redmineHoras2 = current;
                redmineHoras2.Porcentaje = porcentaje;
                list3.Add(redmineHoras2);
            }
            return list3.ToList<RedmineHoras>();
        }

        public List<RedmineHoras> HorasPorProyecto(DateTime pFechaDesde, DateTime pFechaHasta)
        {
            List<RedmineHoras> list = new List<RedmineHoras>();
            string text = string.Concat(new string[]
            {
                "SELECT SUM(a.hours), p.name as Proyecto_Nombre FROM time_entries as a, projects as p WHERE (a.spent_on >= date('",
                pFechaDesde.ToString("yyyy-MM-dd"),
                "') AND a.spent_on <= date('",
                pFechaHasta.ToString("yyyy-MM-dd"),
                "')) AND a.project_id = p.id group by p.id order by p.name "
            });
            ISQLQuery iSQLQuery = this.oRepo.Session.CreateSQLQuery(text);
            IList list2 = iSQLQuery.List();
            double num = 0.0;
            foreach (object[] array in list2)
            {
                bool flag = array[0].ToString().Trim() != "";
                if (flag)
                {
                    RedmineHoras redmineHoras = new RedmineHoras();
                    redmineHoras.Horas = Convert.ToDouble(array[0].ToString());
                    redmineHoras.Proyecto_Nombre = array[1].ToString();
                    list.Add(redmineHoras);
                    num += redmineHoras.Horas;
                }
            }
            list.Add(new RedmineHoras
            {
                Horas = num,
                Proyecto_Nombre = "Total General",
                CC = "Total General"
            });
            List<RedmineHoras> list3 = new List<RedmineHoras>();
            foreach (RedmineHoras current in list)
            {
                double porcentaje = current.Horas / num * 100.0;
                RedmineHoras redmineHoras2 = new RedmineHoras();
                redmineHoras2 = current;
                redmineHoras2.Porcentaje = porcentaje;
                list3.Add(redmineHoras2);
            }
            return list3.ToList<RedmineHoras>();
        }

        public List<RedmineHoras> HorasPorCCProyecto(DateTime pFechaDesde, DateTime pFechaHasta)
        {
            List<RedmineHoras> list = new List<RedmineHoras>();
            string text = string.Concat(new string[]
            {
                "SELECT  cv.value as CC , p.name as Proyecto_Nombre, SUM(a.hours) FROM time_entries as a, projects as p, custom_values as cv WHERE (a.spent_on >= date('",
                pFechaDesde.ToString("yyyy-MM-dd"),
                "') AND a.spent_on <= date('",
                pFechaHasta.ToString("yyyy-MM-dd"),
                "')) AND(cv.custom_field_id = 37 AND cv.customized_id = p.id) AND a.project_id = p.id group by cv.value, p.id order by cv.value, p.name "
            });
            ISQLQuery iSQLQuery = this.oRepo.Session.CreateSQLQuery(text);
            IList list2 = iSQLQuery.List();
            double num = 0.0;
            double num2 = 0.0;
            string text2 = "";
            foreach (object[] array in list2)
            {
                bool flag = array[0].ToString().Trim() != "";
                if (flag)
                {
                    string text3 = array[0].ToString();
                    bool flag2 = text2 == "";
                    if (flag2)
                    {
                        text2 = array[0].ToString();
                        num = 0.0;
                    }
                    bool flag3 = text3 != text2;
                    if (flag3)
                    {
                        list.Add(new RedmineHoras
                        {
                            Horas = num,
                            CC = "Total " + text2,
                            Proyecto_Nombre = ""
                        });
                        num = 0.0;
                        text2 = text3;
                    }
                    RedmineHoras redmineHoras = new RedmineHoras();
                    redmineHoras.Horas = Convert.ToDouble(array[2].ToString());
                    redmineHoras.CC = array[0].ToString();
                    redmineHoras.Proyecto_Nombre = array[1].ToString();
                    list.Add(redmineHoras);
                    num += redmineHoras.Horas;
                    num2 += redmineHoras.Horas;
                }
            }
            list.Add(new RedmineHoras
            {
                Horas = num,
                CC = "Total " + text2,
                Proyecto_Nombre = ""
            });
            list.Add(new RedmineHoras
            {
                Horas = num2,
                CC = "Total General"
            });
            return list.ToList<RedmineHoras>();
        }
    }
}
