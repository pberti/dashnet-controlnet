﻿using IOT.Domain;
using IOT.ORM;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace IOT.Services
{
    public class ServFormCampo : BaseService
    {

        private Repositorio<FormCampo> oRepo = null;

        #region Constructores

        public ServFormCampo(ISession pSession) : base(pSession)
        {
            oRepo = new Repositorio<FormCampo>(oSession);
        }

        #endregion

        #region Métodos ABM

        public long Agregar(FormCampo pObj)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    oRepo.Agregar(pObj);
                    scope.Complete();
                }
                return pObj.Id;
            }
            catch (Exception ex)
            {
                throw new Exception("Error en el método Agregar", ex);
            }
        }

        public int Actualizar(FormCampo pObj)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    int res = oRepo.Actualizar(pObj);
                    scope.Complete();
                    return res;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error en el método Actualizar", ex);
            }
        }

        #endregion

        #region Métodos de Consulta

        public IQueryable<FormCampo> TraerTodos(bool? pSoloActivas)
        {
            try
            {
                IQueryable<FormCampo> lista = oRepo.TraerTodos().Where(r => (pSoloActivas == null || r.Inactivo == !pSoloActivas));
                return lista;
            }
            catch(Exception ex)
            {
                throw new Exception("Error en el metodo TraerTodos", ex);
            }
        }

        public FormCampo TraerXId(long pId)
        {
            try
            {
                return oRepo.TraerUnico(p => p.Id == pId);
            }
            catch(Exception ex)
            {
                throw new Exception("Error en el método TraerXId", ex);
            }
        }

        public IQueryable<FormCampo> TraerXIdFormulario(long pIdFormulario, bool? pSoloActivos)
        {
            try
            {
                return oRepo.TraerTodos(
                    fc => 
                    (pSoloActivos == null || fc.Inactivo != pSoloActivos) &&
                    (fc.IdFormulario == pIdFormulario)); // where...
            }
            catch(Exception ex)
            {
                throw new Exception("Error en el método TraerXIdFormulario", ex);
            }
        }

        #endregion

    }
}
