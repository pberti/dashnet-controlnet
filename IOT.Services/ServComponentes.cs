﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IOT.ORM;
using IOT.Domain;
using NHibernate;
using System.Transactions;

namespace IOT.Services
{
    public class ServComponentes : BaseService
    {
        private Repositorio<TipoComponentes> oRepo = null;
        #region Constructores

        public ServComponentes(ISession pSession) : base(pSession)
        {
            oRepo = new Repositorio<TipoComponentes>(oSession);
        }

        #endregion

        #region Métodos ABM

        public long Agregar(TipoComponentes pObj)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                oRepo.Agregar(pObj);
                scope.Complete();
            }

            return pObj.Id;
        }

        public int Actualizar(TipoComponentes pObj)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    int res = oRepo.Actualizar(pObj);
                    scope.Complete();
                    return res;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error en el metodo Actualizar Componentes");
            }
        }

        public long Borrar(long pId)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    List<TipoComponentes> oLista = oRepo.TraerTodos(p => p.Id == pId).ToList();
                    foreach (TipoComponentes item in oLista)
                    {
                        oRepo.Borrar(item);
                    }

                    scope.Complete();
                    return pId;
                }
            }
            catch (Exception)
            {
                throw new Exception("Error en el metodo Borrar Componentes");
            }
        }
        #endregion

        #region Métodos de Consulta


        public IQueryable<TipoComponentes> TraerTodos()
        {

            IQueryable<TipoComponentes> lista = null;
            try
            {
                lista = oRepo.TraerTodos();
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodos Componentes");
            }
            finally
            {
                lista = null;
            }

        }

        public TipoComponentes TraerUnicoXId(long pId)
        {
            return oRepo.TraerUnico(p => p.Id == pId);
        }

        #endregion
    }
}
