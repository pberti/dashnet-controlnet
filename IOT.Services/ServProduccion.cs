﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IOT.ORM;
using IOT.Domain;
using NHibernate;
using System.Transactions;
using System.Globalization;

namespace IOT.Services
{
    public class ServProduccion : BaseService
    {
        #region Propiedades & Variables
        private Repositorio<Produccion> oRepo = null;
        private Repositorio<Produccion_Componentes> oRepoProduccionComponentes = null;
        private Repositorio<Produccion_Historial_Estado> oRepoProduccion_Historial = null;
        private Repositorio<TipoProducto_Componente> oRepoTipoProductoComponente = null;
        private Repositorio<Workflow_Ruta> oRepoWorkflowRuta = null;
        private Repositorio<Workflow_Permisos> oRepoWorkflowPermisos = null;
        private Repositorio<TipoProducto> oRepoTipoProducto = null;
        private Repositorio<FormulariosXProduccion> oRepoFormulariosXProduccion = null;
        #endregion

        #region Constructores

        public ServProduccion(ISession pSession) : base(pSession)
        {
            oRepo = new Repositorio<Produccion>(oSession);
            oRepoProduccionComponentes = new Repositorio<Produccion_Componentes>(oSession);
            oRepoProduccion_Historial = new Repositorio<Produccion_Historial_Estado>(oSession);
            oRepoTipoProductoComponente = new Repositorio<TipoProducto_Componente>(oSession);
            oRepoWorkflowRuta = new Repositorio<Workflow_Ruta>(oSession);
            oRepoTipoProducto = new Repositorio<TipoProducto>(oSession);
            oRepoWorkflowPermisos = new Repositorio<Workflow_Permisos>(oSession);
            oRepoFormulariosXProduccion = new Repositorio<FormulariosXProduccion>(oSession);
        }

        #endregion

        #region Produccion
        #region Métodos ABM

        public long Agregar(Produccion pObj)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                oRepo.Agregar(pObj);
                scope.Complete();
            }

            return pObj.Id;
        }

        public int Actualizar(Produccion pObj)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    int res = oRepo.Actualizar(pObj);
                    scope.Complete();
                    return res;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error en el metodo Actualizar Produccion");
            }
        }

        public long Borrar(long pId)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    List<Produccion> oLista = oRepo.TraerTodos(p => p.Id == pId).ToList();
                    foreach (Produccion item in oLista)
                    {
                        oRepo.Borrar(item);
                    }

                    scope.Complete();
                    return pId;
                }
            }
            catch (Exception)
            {
                throw new Exception("Error en el metodo Borrar Produccion");
            }
        }
        /// <summary>
        /// Metodo para crear las nuevas producciones
        /// </summary>
        /// <param name="pObj"></param>
        /// <param name="idUsuarioLogueado"></param>
        public void CrearProducciones(List<OpProductos> pObj, long idUsuarioLogueado)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    foreach (OpProductos p in pObj)
                    {
                        Produccion prod = NuevaProduccion(p);
                        Produccion asistente = oRepo.TraerTodos().Where(j => j.NroSeriePPPC == prod.NroSeriePPPC).OrderBy(j => j.NroSerieContador).ToList().LastOrDefault();
                        long contador = 1;
                        if (asistente != null)
                        {
                            contador = Convert.ToInt64(asistente.NroSerieContador) + 1;
                        }


                        for (int i = 0; i < p.Cantidad; i++)
                        {
                            /// Cargo el nro de serie contador y guardo la nueva produccion
                            Produccion nuevaproduccion = prod;
                            nuevaproduccion.IdEstadoActual = 0;
                            string nroSerie = (contador + i).ToString();
                            while (nroSerie.Length < 5)
                            {
                                nroSerie = "0" + nroSerie;
                            }

                            nuevaproduccion.NroSerieContador = nroSerie;
                            oRepo.Agregar(nuevaproduccion);

                            // Creo los componentes de cada produccion
                            CrearProduccion_ComponenteXProduccion(nuevaproduccion);

                            // Creo los historiales de cracion
                            PasarEstado(nuevaproduccion, idUsuarioLogueado, "Creación de Produccion");
                        }
                    }

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error en el metodo CrearProducciones");
            }
        }
        /// <summary>
        ///  cargo los datos de la nueva produccion, salvo el nro de serie contador
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        private Produccion NuevaProduccion(OpProductos p)
        {
            Produccion nuevaproduccion = new Produccion();
            nuevaproduccion.FechaCreacion = DateTime.Now;
            nuevaproduccion.IdProducto = p.IdMaestroProducto;
            nuevaproduccion.IdOrdenProduccion = p.IdOP;
            nuevaproduccion.IdWorkflow = p.IdWorkflow;
            int Semana = System.Globalization.CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(DateTime.Today, System.Globalization.CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
            nuevaproduccion.NroSerieAASS = (DateTime.Now.Year % 2000).ToString() + Semana.ToString();
            nuevaproduccion.NroSeriePPPC = oRepoTipoProducto.TraerPorId(p.IdMaestroProducto).CodigoProducto + p.OpcionMontaje;
            return nuevaproduccion;

        }

        public void PasarInicioProduccion(List<Produccion> producciones, long idUsuario, string comentario, DateTime fecha)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    foreach (Produccion p in producciones)
                    {
                        p.FechaInicioProduccion = fecha;
                        PasarEstado(p, idUsuario, comentario);
                    }
                    scope.Complete();
                }
            }
            catch (Exception)
            {
                throw new Exception("Error en el metodo pasar de estado");
            }

        }
        public void PasarEstadoMasivo(List<Produccion> producciones, long idUsuario, string comentario)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    foreach (Produccion p in producciones)
                    {
                        PasarEstado(p, idUsuario, comentario);
                    }
                    scope.Complete();
                }
            }
            catch (Exception)
            {
                throw new Exception("Error en el metodo pasar de estado");
            }

        }
        #endregion

        #region Métodos de Consulta


        public IQueryable<Produccion> TraerTodos()
        {

            IQueryable<Produccion> lista = null;
            try
            {
                lista = oRepo.TraerTodos();
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodos Produccion");
            }
            finally
            {
                lista = null;
            }

        }

        /// <summary>
        /// Trae todas las producciones de acuerdo a los permisos que tenga el usuario que llama al metodo y a los filtros cargados
        /// </summary>
        /// <param name="idUsuario"></param>
        /// <param name="prod"></param>
        /// <returns></returns>
        public List<Produccion> TraerTodosXFiltro(long idUsuario, Produccion prod)
        {

            List<Produccion> lista = null;
            try
            {
                if (prod != null)
                {
                    if (prod.Id != 0)
                    {
                        lista = new List<Produccion>();
                        lista.Add(oRepo.TraerPorId(prod.Id));
                    }
                    else
                    {
                        if (prod.IdOrdenProduccion != 0)
                        {
                            lista = oRepo.TraerTodos().Where(p => p.IdOrdenProduccion == prod.IdOrdenProduccion).ToList();

                            if (prod.IdEstadoActual != 0)
                            {
                                if (lista != null)
                                {
                                    lista.RemoveAll(p => p.IdEstadoActual != prod.IdEstadoActual);
                                }
                            }
                        }
                        else
                        {
                            if (prod.IdEstadoActual != 0)
                            {
                                lista = oRepo.TraerTodos().Where(p => p.IdEstadoActual == prod.IdEstadoActual).ToList();
                            }
                            if (lista == null)
                            {
                                lista = oRepo.TraerTodos().ToList();
                            }
                        }
                    }
                }

                List<Workflow_Permisos> permisos = oRepoWorkflowPermisos.TraerTodos().Where(p => p.IdPerfil == idUsuario).ToList();
                List<Workflow_Estado> estados = new List<Workflow_Estado>();

                foreach (Workflow_Permisos p in permisos)
                {
                    if (p.PuedeVer)
                        estados.Add(p.oWorkflowEstado);
                }

                List<Produccion> final = new List<Produccion>();

                foreach (Produccion p in lista)
                {
                    if (estados.Contains(p.oEstadoActual))
                    {
                        final.Add(p);
                    }
                }

                return final;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodos Produccion");
            }
            finally
            {
                lista = null;
            }

        }

        public Produccion TraerUnicoXId(long pId)
        {
            return oRepo.TraerUnico(p => p.Id == pId);
        }

        #endregion
        #endregion

        #region Produccion_Componentes

        #region Métodos ABM

        public long AgregarProduccion_Componente(Produccion_Componentes pObj)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                oRepoProduccionComponentes.Agregar(pObj);
                scope.Complete();
            }

            return pObj.Id;
        }

        public int ActualizarProduccion_Componente(Produccion_Componentes pObj)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    int res = oRepoProduccionComponentes.Actualizar(pObj);
                    scope.Complete();
                    return res;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error en el metodo Actualizar Produccion_Componentes");
            }
        }

        public long BorrarProduccion_Componente(long pId)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    List<Produccion_Componentes> oLista = oRepoProduccionComponentes.TraerTodos(p => p.Id == pId).ToList();
                    foreach (Produccion_Componentes item in oLista)
                    {
                        oRepoProduccionComponentes.Borrar(item);
                    }

                    scope.Complete();
                    return pId;
                }
            }
            catch (Exception)
            {
                throw new Exception("Error en el metodo Borrar Produccion_Componentes");
            }
        }

        /// <summary>
        /// Metodo que crea los componentes de cada producion
        /// </summary>
        /// <param name="pObj"></param>
        public void CrearProduccion_ComponenteXProduccion(Produccion pObj)
        {

            List<TipoProducto_Componente> componentes = oRepoTipoProductoComponente.TraerTodos().Where(p => p.IdProducto == pObj.IdProducto).ToList();
            foreach (TipoProducto_Componente c in componentes)
            {
                Produccion_Componentes oObj = new Produccion_Componentes();
                if (c.Cantidad == 1)
                {
                    oObj.IdComponente = c.IdComponente;
                    oObj.IdProduccion = pObj.Id;
                    oObj.NroOrden = 1;
                    oRepoProduccionComponentes.Agregar(oObj);
                }
                else
                {
                    for (int i = 0; i < c.Cantidad; i++)
                    {
                        oObj.IdComponente = c.IdComponente;
                        oObj.IdProduccion = pObj.Id;
                        oObj.NroOrden = i + 1;
                        oRepoProduccionComponentes.Agregar(oObj);
                    }
                }
            }
        }
        #endregion

        public void ActualizarTodosComponentesXProduccion(List<Produccion_Componentes> componentes)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    foreach (Produccion_Componentes c in componentes)
                    {
                        int res = oRepoProduccionComponentes.Actualizar(c);
                    }
                    scope.Complete();
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error en el metodo Actualizar Produccion_ComponentesVarios");
            }
        }

        #region Métodos de Consulta

        public IQueryable<Produccion_Componentes> TraerTodosProduccion_Componente()
        {

            IQueryable<Produccion_Componentes> lista = null;
            try
            {
                lista = oRepoProduccionComponentes.TraerTodos();
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodos Produccion_Componentes");
            }
            finally
            {
                lista = null;
            }

        }

        public Produccion_Componentes TraerUnicoXIdProduccion_Componente(long pId)
        {
            return oRepoProduccionComponentes.TraerUnico(p => p.Id == pId);
        }

        public IQueryable<Produccion_Componentes> TraerComponentesXProduccion(long pId)
        {
            IQueryable<Produccion_Componentes> lista = null;
            try
            {
                lista = oRepoProduccionComponentes.TraerTodos().Where(p => p.IdProduccion == pId);
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerComponentesXProduccion");
            }
            finally
            {
                lista = null;
            }
        }
        #endregion

        #endregion

        #region Produccion_Historial

        #region Métodos ABM

        public long AgregarProduccion_Historial(Produccion_Historial_Estado pObj)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                oRepoProduccion_Historial.Agregar(pObj);
                scope.Complete();
            }

            return pObj.Id;
        }

        public int ActualizarProduccion_Historial(Produccion_Historial_Estado pObj)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    int res = oRepoProduccion_Historial.Actualizar(pObj);
                    scope.Complete();
                    return res;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error en el metodo Actualizar Produccion_Historial");
            }
        }

        public long BorrarProduccion_Historial(long pId)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    List<Produccion_Historial_Estado> oLista = oRepoProduccion_Historial.TraerTodos(p => p.Id == pId).ToList();
                    foreach (Produccion_Historial_Estado item in oLista)
                    {
                        oRepoProduccion_Historial.Borrar(item);
                    }

                    scope.Complete();
                    return pId;
                }
            }
            catch (Exception)
            {
                throw new Exception("Error en el metodo Borrar Produccion_Historial");
            }
        }

        /// <summary>
        /// Metodo que sirve para crear pasar de estado una produccion y crear los historiales correspondientes 
        /// </summary>
        /// <param name="prod"></param>
        /// <param name="IdUsuariologueado"></param>
        /// <param name="comentario"></param>
        public void PasarEstado(Produccion prod, long IdUsuariologueado, string comentario)
        {
            List<Workflow_Ruta> listaEstados;
            Workflow_Estado estadoSiguiente;
            if (prod.IdEstadoActual == 0)
            {
                listaEstados = oRepoWorkflowRuta.TraerTodos().Where(w => w.IdWorkflow == prod.IdWorkflow).ToList();
                listaEstados.RemoveAll(r => r.oWorkflowEstado.EsInicial != true);
                estadoSiguiente = listaEstados.FirstOrDefault().oWorkflowEstado;
            }
            else
            {
                listaEstados = oRepoWorkflowRuta.TraerTodos().Where(w => w.IdWorkflow == prod.IdWorkflow && w.IdWorkflowEstado == prod.IdEstadoActual).ToList();
                estadoSiguiente = listaEstados.FirstOrDefault().oEstadoSiguiente;
            }

            Produccion_Historial_Estado historial = new Produccion_Historial_Estado();
            historial.IdUsuarioModifico = IdUsuariologueado;
            historial.IdNuevoEstado = estadoSiguiente.Id;
            historial.IdEstadoActual = prod.IdEstadoActual;
            historial.IdProduccion = prod.Id;
            historial.FecUltimaModificacion = DateTime.Now;
            historial.Comentario = comentario;

            oRepoProduccion_Historial.Agregar(historial);

            prod.IdEstadoActual = estadoSiguiente.Id;
            oRepo.Actualizar(prod);

        }

        public void PasarEstadoB(Produccion prod, long IdUsuariologueado, string comentario)
        {
            List<Workflow_Ruta> listaEstados;
            Workflow_Estado estadoSiguiente;

            listaEstados = oRepoWorkflowRuta.TraerTodos().Where(w => w.IdWorkflow == prod.IdWorkflow && w.IdWorkflowEstado == prod.IdEstadoActual).ToList();
            estadoSiguiente = listaEstados.FirstOrDefault().oEstadoSiguienteB;
            
            Produccion_Historial_Estado historial = new Produccion_Historial_Estado();
            historial.IdUsuarioModifico = IdUsuariologueado;
            historial.IdNuevoEstado = estadoSiguiente.Id;
            historial.IdEstadoActual = prod.IdEstadoActual;
            historial.IdProduccion = prod.Id;
            historial.FecUltimaModificacion = DateTime.Now;
            historial.Comentario = comentario;

            oRepoProduccion_Historial.Agregar(historial);

            prod.IdEstadoActual = estadoSiguiente.Id;
            oRepo.Actualizar(prod);

        }
        #endregion

        #region Métodos de Consulta


        public IQueryable<Produccion_Historial_Estado> TraerTodosProduccion_Historial()
        {

            IQueryable<Produccion_Historial_Estado> lista = null;
            try
            {
                lista = oRepoProduccion_Historial.TraerTodos();
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodos Produccion_Historial");
            }
            finally
            {
                lista = null;
            }

        }

        public Produccion_Historial_Estado TraerUnicoXIdProduccion_Historial(long pId)
        {
            return oRepoProduccion_Historial.TraerUnico(p => p.Id == pId);
        }


        public IQueryable<Produccion_Historial_Estado> TraerHistorialXProduccion(long Id)
        {
            IQueryable<Produccion_Historial_Estado> lista = null;
            try
            {
                lista = oRepoProduccion_Historial.TraerTodos().Where(p => p.IdProduccion == Id);
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerHistorialXProduccion");
            }
            finally
            {
                lista = null;
            }
        }
        #endregion

        #endregion

        #region FormularioXProduccion
        #region Métodos ABM

        public long AgregarFormularioXProduccion(FormulariosXProduccion pObj)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                oRepoFormulariosXProduccion.Agregar(pObj);
                scope.Complete();
            }

            return pObj.Id;
        }

        public int ActualizarFormulariosXProduccion(FormulariosXProduccion pObj)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    int res = oRepoFormulariosXProduccion.Actualizar(pObj);
                    scope.Complete();
                    return res;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error en el metodo Actualizar Producto");
            }
        }

        public long BorrarFormulariosXProduccion(long pId)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    List<FormulariosXProduccion> oLista = oRepoFormulariosXProduccion.TraerTodos(p => p.Id == pId).ToList();
                    foreach (FormulariosXProduccion item in oLista)
                    {
                        oRepoFormulariosXProduccion.Borrar(item);
                    }

                    scope.Complete();
                    return pId;
                }
            }
            catch (Exception)
            {
                throw new Exception("Error en el metodo Borrar FormulariosXProduccion");
            }
        }
        #endregion

        #region Métodos de Consulta


        public IQueryable<FormulariosXProduccion> TraerTodosFormulariosXProduccion()
        {

            IQueryable<FormulariosXProduccion> lista = null;
            try
            {
                lista = oRepoFormulariosXProduccion.TraerTodos();
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodos FormulariosXProduccion");
            }
            finally
            {
                lista = null;
            }

        }

        public FormulariosXProduccion TraerUnicoXIdFormulariosXProduccion(long pId)
        {
            return oRepoFormulariosXProduccion.TraerUnico(p => p.Id == pId);
        }

        public FormulariosXProduccion TraerUnicoFormularioXproduccion(long idProduccion, long idEstado)
        {
            return oRepoFormulariosXProduccion.TraerUnico(p => p.IdProduccion == idProduccion && p.IdEstado == idEstado);
        }

        #endregion
        #endregion


        
    }
}
