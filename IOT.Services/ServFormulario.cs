﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IOT.ORM;
using IOT.Domain;
using NHibernate;
using System.Transactions;

namespace IOT.Services
{
    public class ServFormulario : BaseService
    {

        private Repositorio<Formulario> oRepo = null;

        #region Constructores

        public ServFormulario(ISession pSession) : base(pSession)
        {
            oRepo = new Repositorio<Formulario>(oSession);
        }

        #endregion

        #region Métodos ABM

        public long Agregar(Formulario pObj)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    oRepo.Agregar(pObj);
                    scope.Complete();
                }
                return pObj.Id;
            }
            catch(Exception ex)
            {
                throw new Exception("Error en el método Agregar", ex);
            }
        }

        public int Actualizar(Formulario pObj)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    int res = oRepo.Actualizar(pObj);
                    scope.Complete();
                    return res;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error en el método Actualizar", ex);
            }
        }

        #endregion

        #region Métodos de Consulta

        public IQueryable<Formulario> TraerTodos(bool? pSoloActivas)
        {
            try
            {
                IQueryable<Formulario> lista = oRepo.TraerTodos().Where(r => (pSoloActivas == null || r.Inactivo == !pSoloActivas));
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodos");
            }
        }

        public Formulario TraerXId(long pId)
        {
            try
            {
                return oRepo.TraerUnico(p => p.Id == pId);
            }
            catch
            {
                throw new Exception("Error en el método TraerXId");
            }
        }
        
        public Formulario TraerXTitulo(string pTitulo)
        {
            try
            {
                return oRepo.TraerUnico(p => p.Titulo == pTitulo);
            }
            catch
            {
                throw new Exception("Error en el método TraerXId");
            }
        }

        public IQueryable<Formulario> TraerXFormulario(Formulario oFormulario, bool? pSoloActivos)
        {
            try
            {
                if (oFormulario == null) oFormulario = new Formulario();
                return oRepo.TraerTodos().Where(r =>
                (string.IsNullOrEmpty(oFormulario.Titulo) || r.Titulo.Contains(oFormulario.Titulo)) &&
                (pSoloActivos == null || oFormulario.Inactivo != pSoloActivos));
            }
            catch
            {
                throw new Exception("Error en el método TraerXFormulario");
            }
        }

        #endregion
    }
}
