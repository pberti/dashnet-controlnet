﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IOT.Services
{
    using System;
    using System.Security.Cryptography;
    using System.Text;

    /// <summary>
    /// Proveee servicios para encripatacion de strings usando MD5
    /// </summary>
    public static class Encriptacion
    {
        /// <summary>
        /// Devuelve una cadena de caracteres encriptada con el formato MD5
        /// </summary>
        /// <param name="PasswordOriginal">La cadena a encriptar</param>
        /// <returns>La cadena encriptada</returns>
        public static string Encriptar(string PasswordOriginal)
        {
            //Declaraciones
            Byte[] BytesOriginales;
            Byte[] BytesCodificados;
            MD5 md5;

            //Instacia el MD5CryptoServiceProvide, convierte el string en bytes, los codifica y
            //los devuelve como string
            md5 = new MD5CryptoServiceProvider();
            BytesOriginales = ASCIIEncoding.Default.GetBytes(PasswordOriginal);
            BytesCodificados = md5.ComputeHash(BytesOriginales);
            return BitConverter.ToString(BytesCodificados);
        }
    }
}
