﻿using IOT.Domain;
using IOT.ORM;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;


namespace IOT.Services
{
    public class ServWorkFlow_Estado : BaseService
    {
        private Repositorio<Workflow_Estado> oRepo = null;

        #region Constructores
        public ServWorkFlow_Estado(ISession pSession) : base(pSession)
        {
            oRepo = new Repositorio<Workflow_Estado>(oSession);
        }

        #endregion

        #region Métodos ABM

        public long Agregar(Workflow_Estado pObj)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                oRepo.Agregar(pObj);
                scope.Complete();
            }
            return pObj.Id;
        }

        public int Actualizar(Workflow_Estado pObj)
        {

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    int res = oRepo.Actualizar(pObj);
                    scope.Complete();
                    return res;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error en el metodo Actualizar Workflow_Estado");
            }

        }




        #endregion

        #region Métodos de Consulta
        public IQueryable<Workflow_Estado> TraerTodos()
        {
            IQueryable<Workflow_Estado> lista = null;
            try
            {
                lista = oRepo.TraerTodos();
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodos Workflow_estado");
            }
            finally
            {
                lista = null;
            }
        }

        public Workflow_Estado TraerUnicoXId(long pId)
        {
            return oRepo.TraerUnico(p => p.Id == pId);
        }
        #endregion
    }
}
