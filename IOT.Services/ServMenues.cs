﻿using IOT.Domain;
using IOT.ORM;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace IOT.Services
{
    public class ServMenues : BaseService
    {
        private Repositorio<Menues> oRepo = null;

        public ServMenues(ISession pSession) : base(pSession)
        {
            this.oRepo = new Repositorio<Menues>(this.oSession);
        }

        public long AgregarMenues(Menues pObj)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                this.oRepo.Agregar(pObj);
                transactionScope.Complete();
            }
            return pObj.Id;
        }

        public int ActualizarMenues(Menues pObj)
        {
            int result;
            using (TransactionScope transactionScope = new TransactionScope())
            {
                int num = this.oRepo.Actualizar(pObj);
                transactionScope.Complete();
                result = num;
            }
            return result;
        }

        public IQueryable<Menues> TraerTodosMenues()
        {
            return this.oRepo.TraerTodos();
        }

        public IQueryable<Menues> TraerTodosMenuesHojas()
        {
            List<Menues> list = new List<Menues>();
            IQueryable<Menues> queryable = this.oRepo.TraerTodos();
            foreach (Menues current in queryable)
            {
                string posicion = current.Posicion;
                bool flag = posicion.Length >= 5 && posicion[4] != '0';
                if (flag)
                {
                    list.Add(current);
                }
            }
            return list.AsQueryable<Menues>();
        }

        public Menues TraerUnicoMenues(long pId)
        {
            return this.oRepo.TraerUnico((Menues p) => p.Id == pId);
        }

        public Menues TraerUnicoMenues(string pPos)
        {
            return this.oRepo.TraerUnico((Menues p) => p.Posicion == pPos);
        }

        public IQueryable<Menues> TraerMenuesRaiz()
        {
            return from p in this.oRepo.TraerTodos((Menues p) => p.IdMenuPadre == 1L)
                   orderby p.Posicion
                   select p;
        }

        public IQueryable<Menues> TraerMenuesHijos(long pIdPadre)
        {
            return from p in this.oRepo.TraerTodos((Menues p) => p.IdMenuPadre == pIdPadre)
                   orderby p.Posicion
                   select p;
        }

        public IQueryable<Menues> TraerTodosMenuesParam(Menues pMenues)
        {
            IQueryable<Menues> queryable = this.oRepo.TraerTodos((Menues p) => p.Nombre.Contains(pMenues.Nombre));
            bool flag = pMenues.Id > 0L;
            if (flag)
            {
                queryable = from p in queryable
                            where p.Id == pMenues.Id
                            select p;
            }
            bool flag2 = pMenues.IdMenuPadre > 0L;
            if (flag2)
            {
                queryable = from p in queryable
                            where p.IdMenuPadre == pMenues.IdMenuPadre
                            select p;
            }
            bool flag3 = pMenues.Posicion != string.Empty;
            if (flag3)
            {
                queryable = from p in queryable
                            where p.Posicion.Contains(pMenues.Posicion)
                            select p;
            }
            return queryable;
        }

        public bool MenuTieneHijos(long pId)
        {
            IQueryable<Menues> source = from p in this.oRepo.TraerTodos()
                                        where p.IdMenuPadre == pId
                                        select p;
            return source.Count<Menues>() > 0;
        }
    }
}
