﻿using IOT.Domain;
using IOT.ORM;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace IOT.Services
{
    public class ServWorkflow_Ruta : BaseService
    {
        private Repositorio<Workflow_Ruta> oRepo = null;

        #region Constructores
        public ServWorkflow_Ruta(ISession pSession) : base(pSession)
        {
            oRepo = new Repositorio<Workflow_Ruta>(oSession);
        }

        #endregion

        #region Métodos ABM

        public long Agregar(Workflow_Ruta pObj)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                oRepo.Agregar(pObj);
                scope.Complete();
            }
            return pObj.Id;
        }

        public int Actualizar(Workflow_Ruta pObj)
        {

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    int res = oRepo.Actualizar(pObj);
                    scope.Complete();
                    return res;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error en el metodo Actualizar Workflow_Ruta");
            }

        }

        #endregion

        #region Métodos de Consulta
        public IQueryable<Workflow_Ruta> TraerTodos()
        {
            IQueryable<Workflow_Ruta> lista = null;
            try
            {
                lista = oRepo.TraerTodos();
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodos Workflow_Ruta");
            }
            finally
            {
                lista = null;
            }
        }

        public Workflow_Ruta TraerUnicoXId(long pId)
        {
            return oRepo.TraerUnico(p => p.Id == pId);
        }
        #endregion
    }
}
