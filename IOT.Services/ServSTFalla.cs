﻿using System;
using System.Linq;
using IOT.ORM;
using IOT.Domain;
using NHibernate;
using System.Transactions;

namespace IOT.Services
{
    public class ServSTFAlla : BaseService
    {
        private Repositorio<STFalla> oRepo = null;
        private Repositorio<FallasXProducto> oRepoFallaXProducto = null;
        private Repositorio<FallasXIngreso> oRepoFallaXIngreso = null;

        #region Constructores
        public ServSTFAlla(ISession pSession) : base(pSession)
        {
            oRepo = new Repositorio<STFalla>(oSession);
            oRepoFallaXProducto = new Repositorio<FallasXProducto>(oSession);
            oRepoFallaXIngreso = new Repositorio<FallasXIngreso>(oSession);

        }

        #endregion

        #region STFalla
        #region Métodos ABM STFalla

        public long Agregar(STFalla pObj)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                oRepo.Agregar(pObj);
                scope.Complete();
            }
            return pObj.Id;
        }

        public int Actualizar(STFalla pObj)
        {

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    int res = oRepo.Actualizar(pObj);
                    scope.Complete();
                    return res;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error en el metodo Actualizar STFalla");
            }

        }
        #endregion

        #region Métodos de Consulta STFalla
        public IQueryable<STFalla> TraerTodos()
        {
            IQueryable<STFalla> lista = null;
            try
            {
                lista = oRepo.TraerTodos();
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodos STFalla");
            }
            finally
            {
                lista = null;
            }
        }

        public STFalla TraerUnicoXId(long pId)
        {
            return oRepo.TraerUnico(p => p.Id == pId);
        }
        #endregion
        #endregion

        #region FallaxProducto
        #region Métodos ABM FallaxProducto
        public long AgregarFallaXProducto(FallasXProducto pObj)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                oRepoFallaXProducto.Agregar(pObj);
                scope.Complete();
            }
            return pObj.Id;
        }

        public int Actualizar(FallasXProducto pObj)
        {

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    int res = oRepoFallaXProducto.Actualizar(pObj);
                    scope.Complete();
                    return res;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error en el metodo Actualizar FallaXProducto");
            }

        }
        #endregion

        #region Métodos de Consulta FallaxProducto
        public IQueryable<FallasXProducto> TraerTodosFallaXProducto()
        {
            IQueryable<FallasXProducto> lista = null;
            try
            {
                lista = oRepoFallaXProducto.TraerTodos();
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodos FallaXProducto");
            }
            finally
            {
                lista = null;
            }
        }

        public FallasXProducto TraerUnicoFallaXProductoXId(long pId)
        {
            return oRepoFallaXProducto.TraerUnico(p => p.Id == pId);
        }
        #endregion
        #endregion

        #region FallaxIngreso
        #region Métodos ABM FallaxIngreso
        public long AgregarFallaXIngreso(FallasXIngreso pObj)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                oRepoFallaXIngreso.Agregar(pObj);
                scope.Complete();
            }
            return pObj.Id;
        }

        public int Actualizar(FallasXIngreso pObj)
        {

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    int res = oRepoFallaXIngreso.Actualizar(pObj);
                    scope.Complete();
                    return res;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error en el metodo Actualizar FallaXIngreso");
            }

        }
        #endregion

        #region Métodos de Consulta FallaxIngreso
        public IQueryable<FallasXIngreso> TraerTodosFallasXIngreso()
        {
            IQueryable<FallasXIngreso> lista = null;
            try
            {
                lista = oRepoFallaXIngreso.TraerTodos();
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodos FallasXIngreso");
            }
            finally
            {
                lista = null;
            }
        }

        public FallasXIngreso TraerUnicoXIdFallasXIngreso(long pId)
        {
            return oRepoFallaXIngreso.TraerUnico(p => p.Id == pId);
        }
        #endregion
        #endregion
    }
}
