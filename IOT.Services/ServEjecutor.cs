﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IOT.ORM;
using IOT.Domain;
using NHibernate;
using System.Transactions;

namespace IOT.Services
{
    public class ServEjecutor: BaseService
    {
        private Repositorio<Ejecutor> oRepo = null;

        #region Constructores
        public ServEjecutor(ISession pSession) : base(pSession)
        {
            oRepo = new Repositorio<Ejecutor>(oSession);
        }

        #endregion

        #region Métodos ABM

        public long Agregar(Ejecutor pObj)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                oRepo.Agregar(pObj);
                scope.Complete();
            }
            return pObj.Id;
        }

        public int Actualizar(Ejecutor pObj)
        {

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    int res = oRepo.Actualizar(pObj);
                    scope.Complete();
                    return res;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error en el metodo Actualizar Ejecutor");
            }

        }




        #endregion

        #region Métodos de Consulta
        public IQueryable<Ejecutor> TraerTodos(bool? pSoloActivos)
        {
            IQueryable<Ejecutor> lista = null;
            try
            {
                lista = oRepo.TraerTodos().Where(e =>
                pSoloActivos == null || e.Inactivo == !pSoloActivos);
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodos Ejecutor");
            }
            finally
            {
                lista = null;
            }
        }

        public Ejecutor TraerUnicoXId(long pId)
        {
            return oRepo.TraerUnico(p => p.Id == pId);
        }
        #endregion
    }
}
