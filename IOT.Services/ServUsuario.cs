﻿using IOT.Domain;
using IOT.ORM;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace IOT.Services
{
    public class ServUsuario : BaseService
    {
        #region Constructores
        private Repositorio<Usuario> oRepo = null;
        private Repositorio<TipoPerfil> oRepoPerfiles = null;
        

        public ServUsuario(ISession pSession)
            : base(pSession)
        {
            oRepo = new Repositorio<Usuario>(oSession);
            oRepoPerfiles = new Repositorio<TipoPerfil>(oSession);
        }

        #endregion

        #region Usuarios

        #region Métodos ABM

        public long AgregarUsuario(Usuario pObj)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                oRepo.Agregar(pObj);
                scope.Complete();
            }

            return pObj.Id;
        }

        public int ActualizarUsuario(Usuario pObj)
        {
            using (var scope = new TransactionScope())
            {
                int res = oRepo.Actualizar(pObj);
                scope.Complete();
                return res;
            }
        }

        public long LogearUsuario(string username, string password)
        {
            password = Encriptacion.Encriptar(password);
            var oUsuario = oRepo.TraerUnico(x => x.Password == password && x.UserName == username && x.Activo == true);
            return oUsuario != null ? oUsuario.Id : -1;
        }

        public bool CambiarPass(string username, string oldpassword, string newpassword)
        {
            oldpassword = Encriptacion.Encriptar(oldpassword);
            newpassword = Encriptacion.Encriptar(newpassword);
            bool result = false;
            var oUsuario = oRepo.TraerUnico(x => x.Password == oldpassword && x.UserName == username);
            if (oUsuario != null)
            {
                oUsuario.Password = newpassword;
                ActualizarUsuario(oUsuario);
            }

            return result;
        }


        #endregion

        #region Métodos de Consulta

        public IQueryable<Usuario> TraerTodos()
        {
            IQueryable<Usuario> lista = null;
            try
            {
                lista = oRepo.TraerTodos().OrderBy(u => u.Apellido);
                return lista;
            }
            catch
            {
                throw new Exception("Error en el método TraerTodosUsuario");
            }
            finally
            {
                lista = null;
            }
        }

        /// <summary>
        /// pFiltrarXUsuarioInt indica si se tiene en cuenta el parametro EsUSuarioInterno
        /// </summary>
        /// <returns>Retorna un objeto de tipo IQueryable</returns>
        public IQueryable<Usuario> TraerTodosUsuariosParam(Usuario pUsuario, bool pFiltrarXUsuarioInt = false)
        {
            IQueryable<Usuario> lista = null;
            try
            {
                lista = oRepo.TraerTodos();

                if (!String.IsNullOrEmpty(pUsuario.UserName))
                {
                    lista = lista.Where(p => p.UserName.Contains(pUsuario.UserName));
                }

                if (!String.IsNullOrEmpty(pUsuario.Nombre))
                {
                    lista = lista.Where(p => p.Nombre.Contains(pUsuario.Nombre.ToUpper()) ||
                                            p.Nombre.Contains(pUsuario.Nombre));
                }

                if (!String.IsNullOrEmpty(pUsuario.Apellido))
                {
                    lista = lista.Where(p => p.Apellido.Contains(pUsuario.Apellido.ToUpper()) ||
                                        p.Apellido.Contains(pUsuario.Apellido));
                }

                if (pUsuario.Activo != null)
                {
                    lista = lista.Where(p => p.Activo == pUsuario.Activo);
                }

                if(pUsuario.IdTipoPerfil > 0)
                {
                    lista = lista.Where(p => p.IdTipoPerfil == pUsuario.IdTipoPerfil);
                }
                var count = lista.Count();
                return lista;
            }
            catch(Exception ex)
            {
                throw new Exception("Error en el método TraerTodosUsuariosParam");
            }
            finally
            {
                lista = null;
            }
        }

        public Usuario TraerPrimerUsuarioPorSector(long pIdSector, long pIdUbicacion)
        {
            return null;// repoUsuarios.TraerUnico(x => x.IdSector == pIdSector && x.IdUbicacionFisica == pIdUbicacion);
        }

        public Usuario TraerUnicoUsuarioXCodigoActivacion(string pCodigoActivacion)
        {
            return null;// repoUsuarios.TraerUnico(p => p.CodigoActivacion == pCodigoActivacion);
        }

        public Usuario TraerUnicoUsuario(long pId)
        {
            return oRepo.TraerUnico(p => p.Id == pId);
        }

        public Usuario TraerPorUsuario(string username)
        {
            return oRepo.TraerUnico(p => p.UserName == username);
        }

        public Usuario TraerPorEmail(string email)
        {
            return null;// repoUsuarios.TraerUnico(p => p.Email == email);
        }

        //TODO: #Nico método para verificar  si existe en BD un usuario ya registrado
        //Verifico UserName
        public Usuario ExisteUsuarioExterno(Usuario pUsuario)
        {
            //UserName de usuario Externo = Email
            return null;// repoUsuarios.TraerUnico(p => p.Username == pUsuario.Email || p.Email == pUsuario.Email || p.oPersonas.NroDocumento == pUsuario.DNI);
        }


        public Usuario ExisteUsuarioInterno(Usuario pUsuario)
        {
            return null;// repoUsuarios.TraerUnico(p => p.Username == pUsuario.Username || p.Email == pUsuario.Email || p.oPersonas.NroDocumento == pUsuario.oPersonas.NroDocumento);
        }

        public List<Usuario> TraerUsuariosPorSector(long pIdSector)
        {
            List<Usuario> listUsuarios = null;
            //ServSector oServSector;
            //UsuarioSector oUsrSect;
            try
            {
                //oServSector = new ServSector(oSession);
                //oUsrSect = new UsuarioSector();
                //oUsrSect.IdSector = pIdSector;
                //oUsrSect.IdUsuario = 0;
                //var listUsrSector = oServSector.TraerTodosUsuarioSectorParam(oUsrSect);

                //listUsuarios = new List<Usuarios>();

                //foreach (UsuarioSector item in listUsrSector)
                //{
                //    listUsuarios.Add(item.oUsuario);
                //}

                return listUsuarios;
            }
            catch
            {
                throw new Exception("Error en el método TraerUsuariosPorSector");
            }
            finally
            {
                //oServSector = null;
                listUsuarios = null;
            }
        }

        public IQueryable<Usuario> TraerUsuariosPorUbicacionyEstadoTramite(long pIdUbicacion, long pIdUbicacionUsuario, long pIdEstado)
        {
            //var oEstado = new Repositorio<TramiteEstados>(oSession).TraerPorId(pIdEstado);
            //var oConfiguracion = new Repositorio<Configuracion>(oSession).TraerTodos().First();
            //List<long> lstIdPerfiles = oEstado.listTramiteEstadoPerfiles.Select(p => p.IdPerfil).ToList();
            IQueryable<Usuario> lstUsuarios = null;

            //if (pIdUbicacionUsuario != oConfiguracion.IdUbicacionCentral || oConfiguracion.CentralVeTodos == false)
            //{
            //    lstUsuarios = TraerTodosUsuario().Where(p => lstIdPerfiles.Contains(p.IdTipoPerfil) && p.IdUbicacionFisica == pIdUbicacion);
            //}
            //else
            //{
            //    lstUsuarios = TraerTodosUsuario().Where(p => lstIdPerfiles.Contains(p.IdTipoPerfil) && p.IdUbicacionFisica == oConfiguracion.IdUbicacionCentral);
            //}

            return lstUsuarios;
        }

        /// <summary>
        /// Metodo que devuelve un IQueryable de Usuario según una Instancia de Usuario seteada con datos filtro
        /// </summary>
        /// <param name="oUsuario">Instancia de Usuario seteada con datos filtro</param>
        /// <returns>IQueryable de Usuario</returns>
        public IQueryable<Usuario> TraerXUsuario(Usuario oUsuario)
        {
            try
            {
                return oRepo.TraerTodos(
                    u =>
                    (oUsuario.Activo == null || u.Activo == oUsuario.Activo) &&
                    (
                        (string.IsNullOrEmpty(oUsuario.Apellido) || u.Apellido.Contains(oUsuario.Apellido)) ||
                        (string.IsNullOrEmpty(oUsuario.Nombre) || u.Nombre.Contains(oUsuario.Nombre))
                    )
                ).OrderBy(u => u.Apellido);
            }
            catch
            {
                throw new Exception("Error en el método TraerXUsuario");
            }
        }


        public IQueryable<Usuario> TraerTodosUsuario()
        {
            IQueryable<Usuario> result;
            try
            {
                IQueryable<Usuario> queryable = this.oRepo.TraerTodos();
                result = queryable;
            }
            catch
            {
                throw new Exception("Error en el método TraerTodosUsuario");
            }
            finally
            {
            }
            return result;
        }
        #endregion

        #endregion

        #region Perfiles

        #region Metodos de ABM

        public long AgregarTipoPerfil(TipoPerfil pObj)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    oRepoPerfiles.Agregar(pObj);
                    scope.Complete();
                    return pObj.Id;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error en el metodo AgregarTipoPerfil");
            }
        }

        public int ActualizarTipoPerfil(TipoPerfil pObj)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    int res = oRepoPerfiles.Actualizar(pObj);
                    scope.Complete();
                    return res;
                }
            }
            catch (Exception)
            {
                throw new Exception("Error en el metodo ActualizarTipoPerfil");
            }
        }

        #endregion

        #region Metodos de Consulta

        public TipoPerfil TraerUnicoTipoPerfil(long pId)
        {
            try
            {
                return oRepoPerfiles.TraerUnico(p => p.Id == pId);
            }
            catch (Exception)
            {
                throw new Exception("Error en el metodo TraerUnicoTipoPerfil");
            }
        }

        public TipoPerfil TraerUnicoTipoPerfil(string pDescripcion)
        {
            try
            {
                return oRepoPerfiles.TraerUnico(p => p.Descripcion.Contains(pDescripcion));
            }
            catch (Exception)
            {
                throw new Exception("Error en el metodo TraerUnicoTipoPerfil");
            }
        }

        public TipoPerfil TraerTipoPerfil(string pPerfil)
        {
            try
            {
                return oRepoPerfiles.TraerUnico(p => p.Perfil == pPerfil);
            }
            catch (Exception)
            {
                throw new Exception("Error en el metodo TraerUnicoTipoPerfil");
            }
        }

        public IQueryable<TipoPerfil> TraerTodosPerfiles()
        {
            try
            {
                IQueryable<TipoPerfil> oLista = oRepoPerfiles.TraerTodos();
                return oLista;
            }
            catch (Exception)
            {
                throw new Exception("Error en el metodo TraerTodosPerfiles");
            }
        }

        //TODO: guiarme de aca!
        public List<TipoPerfil> TraerPerfilesFiltro(string pPerfil, string pUsuario)
        {
            try
            {
                List<TipoPerfil> oLista = new List<TipoPerfil>();

                oLista = oRepoPerfiles.TraerTodos(p => p.Descripcion.Contains(pPerfil) &&
                    p.listUsuarios.Any(x => x.UserName.Contains(pUsuario))).ToList();
                return oLista;
            }
            catch (Exception)
            {
                throw new Exception("Error en el metodo TraerPerfilesFiltro");
            }

        }

        public IQueryable<TipoPerfil> TraerTodosPerfilesParam(string pPerfil, string pDesc)
        {
            try
            {
                IQueryable<TipoPerfil> oLista = oRepoPerfiles.TraerTodos(p => p.Perfil.Contains(pPerfil) &&
                                                    p.Descripcion.Contains(pDesc));
                return oLista;
            }
            catch (Exception)
            {
                throw new Exception("Error en el metodo TraerTodosPerfiles");
            }
        }

        public TipoPerfil TraerPerfilPorUsuario(string pUser)
        {

            //try
            //{
            //    return oRepo.TraerUnico(p => p.UserName == pUser).oTipoPerfil;
            //}
            //catch (Exception)
            //{
            //    throw new Exception("Error en el metodo TraerPerfilPorUsuario");
            //}
            return null;
        }


        public List<TipoPerfil> TraerPerfilesInternos()
        {
            try
            {
                List<TipoPerfil> oLista = new List<TipoPerfil>();

                oLista = oRepoPerfiles.TraerTodos(p => !p.Descripcion.Contains("Externo")).ToList();
                return oLista;
            }
            catch (Exception)
            {
                throw new Exception("Error en el metodo TraerPerfilesInternos");
            }

        }
        #endregion

        #endregion
    }
}
