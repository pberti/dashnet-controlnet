﻿using IOT.Domain;
using IOT.ORM;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace IOT.Services
{
    public class ServFormRespuestaCampo : BaseService
    {

        private Repositorio<FormRespuestaCampo> oRepo = null;

        #region Constructores

        public ServFormRespuestaCampo(ISession pSession) : base(pSession)
        {
            oRepo = new Repositorio<FormRespuestaCampo>(oSession);
        }

        #endregion

        #region Métodos ABM

        public long Agregar(FormRespuestaCampo pObj)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    oRepo.Agregar(pObj);
                    scope.Complete();
                }
                return pObj.Id;
            }
            catch (Exception ex)
            {
                throw new Exception("Error en el método Agregar", ex);
            }
        }

        public int Actualizar(FormRespuestaCampo pObj)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    int res = oRepo.Actualizar(pObj);
                    scope.Complete();
                    return res;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error en el método Actualizar", ex);
            }
        }

        #endregion

        #region Métodos de Consulta

        public IQueryable<FormRespuestaCampo> TraerTodos(bool? pSoloActivas)
        {
            try
            {
                IQueryable<FormRespuestaCampo> lista = oRepo.TraerTodos().Where(r => (pSoloActivas == null || r.Inactivo == !pSoloActivas));
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodos");
            }
        }

        public FormRespuestaCampo TraerXId(long pId)
        {
            try
            {
                return oRepo.TraerUnico(p => p.Id == pId);
            }
            catch
            {
                throw new Exception("Error en el método TraerXId");
            }
        }
        
        #endregion
    }
}
