﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using IOT.Domain;
using IOT.ORM;
using NHibernate;

namespace IOT.Services
{
    public class ServWorkflow_Permisos : BaseService
    {
        private Repositorio<Workflow_Permisos> oRepo = null;

        #region Constructores
        public ServWorkflow_Permisos(ISession pSession) : base(pSession)
        {
            oRepo = new Repositorio<Workflow_Permisos>(oSession);
        }

        #endregion

        #region Métodos ABM

        public long Agregar(Workflow_Permisos pObj)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                oRepo.Agregar(pObj);
                scope.Complete();
            }
            return pObj.Id;
        }

        public int Actualizar(Workflow_Permisos pObj)
        {

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    int res = oRepo.Actualizar(pObj);
                    scope.Complete();
                    return res;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error en el metodo Actualizar Workflow_Permisos");
            }

        }

        #endregion

        #region Métodos de Consulta
        public IQueryable<Workflow_Permisos> TraerTodos()
        {
            IQueryable<Workflow_Permisos> lista = null;
            try
            {
                lista = oRepo.TraerTodos();
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodos Workflow_Permisos");
            }
            finally
            {
                lista = null;
            }
        }

        public Workflow_Permisos TraerUnicoXId(long pId)
        {
            return oRepo.TraerUnico(p => p.Id == pId);
        }

        public Workflow_Permisos TraerXEstadoYPerfil(long idPerfil, long idEstado)
        {
            List<Workflow_Permisos> permisos = oRepo.TraerTodos().ToList();
            permisos.RemoveAll(p => p.IdWorkflowEstado != idEstado);
            permisos.RemoveAll(p => p.IdPerfil != idPerfil);
            if (permisos.Count == 0)
                return null;
            else
                return permisos.First();
        }

        public IQueryable<Workflow_Permisos> TraerXPerfil(long idPerfil)
        {
            IQueryable<Workflow_Permisos> permisos = null; 
            try
            {
               permisos = oRepo.TraerTodos().Where(p => p.IdPerfil == idPerfil);
                return permisos;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerXPerfil Workflow_Permisos");
            }
            finally
            {
                permisos = null;
            }
        }
        #endregion
    }
}
