﻿using IOT.Domain;
using IOT.ORM;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace IOT.Services
{
    public class ServFormRespuesta : BaseService
    {

        private Repositorio<FormRespuesta> oRepo = null;

        #region Constructores

        public ServFormRespuesta(ISession pSession) : base(pSession)
        {
            oRepo = new Repositorio<FormRespuesta>(oSession);
        }

        #endregion

        #region Métodos ABM

        public long Agregar(FormRespuesta oFormRespuesta, List<FormRespuestaCampo> lFormRespuestasCampos, long pIdUsuarioLogueado)
        {
            try
            {
                ServFormRespuestaCampo oServFRCampo = new ServFormRespuestaCampo(oSession);
                using (TransactionScope scope = new TransactionScope())
                {
                    oFormRespuesta.IdUsuarioModifico = pIdUsuarioLogueado;
                    oFormRespuesta.FecUltimaModificacion = DateTime.Now;
                    oRepo.Agregar(oFormRespuesta);
                    lFormRespuestasCampos.ForEach(
                        frc =>
                        {
                            frc.IdFormRespuesta = oFormRespuesta.Id;
                            frc.IdUsuarioModifico = pIdUsuarioLogueado;
                            frc.FecUltimaModificacion = DateTime.Now;
                            oServFRCampo.Agregar(frc);
                        });
                    scope.Complete();
                }
                return oFormRespuesta.Id;
            }
            catch (Exception ex)
            {
                throw new Exception("Error en el método Agregar", ex);
            }
        }

        public int Actualizar(FormRespuesta pObj)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    int res = oRepo.Actualizar(pObj);
                    scope.Complete();
                    return res;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error en el método Actualizar", ex);
            }
        }

        public int Actualizar(FormRespuesta oFormRespuesta, List<FormRespuestaCampo> lFormRespuestasCampos, long pIdUsuarioLogueado)
        {
            try
            {
                int cantidadRegistros = 0;
                ServFormRespuestaCampo oServFRCampo = new ServFormRespuestaCampo(oSession);
                using (TransactionScope scope = new TransactionScope())
                {
                    oFormRespuesta.IdUsuarioModifico = pIdUsuarioLogueado;
                    oFormRespuesta.FecUltimaModificacion = DateTime.Now;
                    oRepo.Actualizar(oFormRespuesta);
                    lFormRespuestasCampos.ForEach(
                        frc =>
                        {
                            frc.IdFormRespuesta = oFormRespuesta.Id;
                            frc.IdUsuarioModifico = pIdUsuarioLogueado;
                            frc.FecUltimaModificacion = DateTime.Now;
                            if (frc.Id == 0)
                                oServFRCampo.Agregar(frc);
                            else
                                oServFRCampo.Actualizar(frc);
                            cantidadRegistros++;
                        });
                    scope.Complete();
                }
                return cantidadRegistros;
            }
            catch (Exception ex)
            {
                throw new Exception("Error en el método Agregar", ex);
            }
        }

        #endregion

        #region Métodos de Consulta

        public IQueryable<FormRespuesta> TraerTodos(bool? pSoloActivas)
        {
            try
            {
                IQueryable<FormRespuesta> lista = oRepo.TraerTodos().Where(r => (pSoloActivas == null || r.Inactivo == !pSoloActivas));
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodos");
            }
        }

        public FormRespuesta TraerXId(long pId)
        {
            try
            {
                return oRepo.TraerUnico(p => p.Id == pId);
            }
            catch
            {
                throw new Exception("Error en el método TraerXId");
            }
        }

        public IQueryable<FormRespuesta> TraerXIdFormularioIdUsuarioRespondio(long pIdFormulario, long pIdUsuarioRespondio)
        {
            try
            {
                return oRepo.TraerTodos(fr => fr.IdFormulario == pIdFormulario && fr.IdUsuarioRespondio == pIdUsuarioRespondio);
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodos");
            }
        }

        #endregion
    }
}
