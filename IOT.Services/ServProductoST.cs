﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using IOT.ORM;
using IOT.Domain;
using NHibernate;
using System.Transactions;

namespace IOT.Services
{
    public class ServProductoST : BaseService
    {
        private Repositorio<ProductoST> oRepo = null;
        private Repositorio<ProductoST_Componentes> oRepoComponentes = null;
        private Repositorio<TipoProducto_Componente> oRepoTipoComponentes = null;
        private Repositorio<ProductoST_Historial> oRepoHistorial = null;
        private Repositorio<ProductoST_Ingreso> oRepoIngreso = null;
        private Repositorio<Workflow_Ruta> oRepoWorkflowRuta = null;
        private Repositorio<Produccion_Componentes> oRepoProduccionComponente = null;
        private Repositorio<Produccion> oRepoProduccion = null;

        #region Constructores
        public ServProductoST(ISession pSession) : base(pSession)
        {
            oRepo = new Repositorio<ProductoST>(oSession);
            oRepoComponentes = new Repositorio<ProductoST_Componentes>(oSession);
            oRepoTipoComponentes = new Repositorio<TipoProducto_Componente>(oSession);
            oRepoHistorial = new Repositorio<ProductoST_Historial>(oSession);
            oRepoWorkflowRuta = new Repositorio<Workflow_Ruta>(oSession);
            oRepoIngreso = new Repositorio<ProductoST_Ingreso>(oSession);
            oRepoProduccionComponente = new Repositorio<Produccion_Componentes>(oSession);
            oRepoProduccion = new Repositorio<Produccion>(oSession);
        }

        #endregion

        #region Métodos ABM

        public long Agregar(ProductoST pObj)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    oRepo.Agregar(pObj);

                    CrearComponentesXProductoSt(pObj);
                    scope.Complete();
                }
                return pObj.Id;
            }
            catch (Exception e)
            {
                throw new Exception("Error en el metodo Agregar ProductoST");
            }
        }

        public int Actualizar(ProductoST pObj)
        {

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    int res = oRepo.Actualizar(pObj);
                    scope.Complete();
                    return res;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error en el metodo Actualizar ProductoST");
            }

        }
        #endregion

        #region Métodos de Consulta
        public IQueryable<ProductoST> TraerTodos()
        {
            IQueryable<ProductoST> lista = null;
            try
            {
                lista = oRepo.TraerTodos();
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodos ProductoST");
            }
            finally
            {
                lista = null;
            }
        }

        public ProductoST TraerUnicoXId(long pId)
        {
            return oRepo.TraerUnico(p => p.Id == pId);
        }

        public IQueryable<ProductoST> TraerTodosFiltro(ProductoST prod)
        {
            IQueryable<ProductoST> lista = null;

            try
            {
                if (!string.IsNullOrEmpty(prod.NroSerie))
                {
                    if (prod.IdTipoProducto == 0)
                    {
                        lista = oRepo.TraerTodos().Where(p => p.NroSerie == prod.NroSerie);
                    }
                    else
                    {
                        lista = oRepo.TraerTodos().Where(p => p.NroSerie == prod.NroSerie && p.IdTipoProducto == prod.IdTipoProducto);
                    }
                    if(lista.Count() == 0)
                    {
                        string NroSerieAASS = string.Empty;
                        string NroSeriePPPC = string.Empty;
                        string NroSerieContador = string.Empty;
                        Produccion produccion = new Produccion();

                        if (prod.NroSerie.Length == 13)
                        {
                            NroSerieAASS = prod.NroSerie.Substring(0, 4);
                            NroSeriePPPC = prod.NroSerie.Substring(4, 4);
                            NroSerieContador = prod.NroSerie.Substring(8);

                           produccion = oRepoProduccion.TraerTodos().Where(p => p.NroSerieAASS== NroSerieAASS && p.NroSeriePPPC == NroSeriePPPC && p.NroSerieContador == NroSerieContador).FirstOrDefault();
                        }
                        
                        if (produccion.Id != 0)
                        {
                            ProductoST nuevoProducto = new ProductoST();
                            nuevoProducto.NroSerie = produccion.NroSerie;
                            nuevoProducto.IdTipoProducto = produccion.IdProducto;
                            nuevoProducto.IdProduccion = produccion.Id;

                            nuevoProducto.Id = Agregar(nuevoProducto);
                            lista = oRepo.TraerTodos().Where(p => p.Id == nuevoProducto.Id);
                        }
                    }
                }
                else
                {
                    lista = oRepo.TraerTodos().Where(p => p.IdTipoProducto == prod.IdTipoProducto);

                }

                return lista;
            }
            catch(Exception e)
            {
                throw new Exception("Error en el metodo TraerTodos ProductoST_Ingreso");
            }
            finally
            {
                lista = null;
            }
        }
        #endregion

        #region Componentes
        #region Métodos ABM

        public long AgregarComponente(ProductoST_Componentes pObj)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                oRepoComponentes.Agregar(pObj);
                scope.Complete();
            }
            return pObj.Id;
        }

        public int ActualizarComponenete(ProductoST_Componentes pObj)
        {

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    int res = oRepoComponentes.Actualizar(pObj);
                    scope.Complete();
                    return res;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error en el metodo Actualizar Produccion_Componentes");
            }

        }


        /// <summary>
        /// Metodo que crea los componentes de cada producion
        /// </summary>
        /// <param name="pObj"></param>
        public void CrearComponentesXProductoSt(ProductoST pObj)
        {

            List<TipoProducto_Componente> componentes = oRepoTipoComponentes.TraerTodos().Where(p => p.IdProducto == pObj.IdTipoProducto).ToList();
            foreach (TipoProducto_Componente c in componentes)
            {
                ProductoST_Componentes oObj = new ProductoST_Componentes();
                if (c.Cantidad == 1)
                {
                    oObj.IdComponente = c.IdComponente;
                    oObj.IdProductoST = pObj.Id;
                    oObj.NroOrden = 1;

                    if(pObj.IdProduccion != 0)
                    {
                        Produccion_Componentes comp = oRepoProduccionComponente.TraerTodos().Where(p => p.IdComponente == oObj.IdComponente && p.IdProduccion == pObj.IdProduccion && p.NroOrden == oObj.NroOrden).FirstOrDefault();
                        if (!string.IsNullOrEmpty(comp.NroSerie))
                        {
                            oObj.NroSerie = comp.NroSerie;
                        }
                    }

                    oRepoComponentes.Agregar(oObj);
                }
                else
                {
                    for (int i = 0; i < c.Cantidad; i++)
                    {
                        oObj.IdComponente = c.IdComponente;
                        oObj.IdProductoST = pObj.Id;
                        oObj.NroOrden = i + 1;
                        oRepoComponentes.Agregar(oObj);

                        if (pObj.IdProduccion != 0)
                        {
                            Produccion_Componentes comp = oRepoProduccionComponente.TraerTodos().Where(p => p.IdComponente == oObj.IdComponente && p.IdProduccion == pObj.IdProduccion && p.NroOrden == oObj.NroOrden).FirstOrDefault();
                            if (!string.IsNullOrEmpty(comp.NroSerie))
                            {
                                oObj.NroSerie = comp.NroSerie;
                            }
                        }
                    }
                }
            }
        }

        public void ActualizarTodosComponentesXProducto(List<ProductoST_Componentes> componentes)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    foreach (ProductoST_Componentes c in componentes)
                    {
                        int res = oRepoComponentes.Actualizar(c);
                    }
                    scope.Complete();
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error en el metodo Actualizar Produccion_ComponentesVarios");
            }
        }
        #endregion

        #region Métodos de Consulta
        public IQueryable<ProductoST_Componentes> TraerTodosComponentes()
        {
            IQueryable<ProductoST_Componentes> lista = null;
            try
            {
                lista = oRepoComponentes.TraerTodos();
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodos Produccion_Componentes");
            }
            finally
            {
                lista = null;
            }
        }

        public IQueryable<ProductoST_Componentes> TraerTodosComponentesXProducto(long idProducto)
        {
            IQueryable<ProductoST_Componentes> lista = null;
            try
            {
                lista = oRepoComponentes.TraerTodos().Where(c => c.IdProductoST == idProducto);
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodos Produccion_Componentes");
            }
            finally
            {
                lista = null;
            }
        }

        public ProductoST_Componentes TraerUnicoComponenteXId(long pId)
        {
            return oRepoComponentes.TraerUnico(p => p.Id == pId);
        }
        #endregion
        #endregion

        #region Ingresos

        #region Métodos ABM

        public long AgregarIngreso(ProductoST_Ingreso pObj, long idUsuarioLogueado)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                oRepoIngreso.Agregar(pObj);

                PasarEstado(pObj, idUsuarioLogueado, "Se ingreso el producto");
                scope.Complete();
            }
            return pObj.Id;
        }

        public int ActualizarIngreso(ProductoST_Ingreso pObj)
        {

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    int res = oRepoIngreso.Actualizar(pObj);
                    scope.Complete();
                    return res;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error en el metodo Actualizar ProductoST_Ingreso");
            }

        }
        #endregion

        #region Métodos de Consulta
        public IQueryable<ProductoST_Ingreso> TraerTodosIngresos()
        {
            IQueryable<ProductoST_Ingreso> lista = null;
            try
            {
                lista = oRepoIngreso.TraerTodos();
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodos ProductoST_Ingreso");
            }
            finally
            {
                lista = null;
            }
        }

        public IQueryable<ProductoST_Ingreso> TraerTodosIngresosFiltro(ProductoST prod)
        {
            IQueryable<ProductoST_Ingreso> lista = null;
            IQueryable<ProductoST> asist = null;
            try
            {
                if (!string.IsNullOrEmpty(prod.NroSerie))
                {
                    if (prod.IdTipoProducto == 0)
                    {
                        asist = oRepo.TraerTodos().Where(p => p.NroSerie == prod.NroSerie || p.NroGabinete == prod.NroSerie);
                    }
                    else
                    {
                        asist = oRepo.TraerTodos().Where(p => p.NroSerie == prod.NroSerie || p.NroGabinete == prod.NroSerie && p.IdTipoProducto == prod.IdTipoProducto);
                    }
                }
                else
                {
                    //asist = oRepo.TraerTodos().Where(p => p.IdTipoProducto == prod.IdTipoProducto);
                    return oRepoIngreso.TraerTodos().Where(i => i.oProductoST.IdTipoProducto == prod.IdTipoProducto).OrderBy("FecUltimaModificacion descending");
                }
                if(asist.Count()>0)
                    lista = oRepoIngreso.TraerTodos().Where(i => i.IdProductoST == asist.FirstOrDefault().Id).OrderBy("FecUltimaModificacion descending");
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodos ProductoST_Ingreso");
            }
            finally
            {
                lista = null;
            }
        }

        public ProductoST_Ingreso TraerUnicoIngresoXId(long pId)
        {
            return oRepoIngreso.TraerUnico(p => p.Id == pId);
        }
        #endregion
        #endregion

        #region Historial
        #region Métodos ABM

        public long AgregarHistorial(ProductoST_Historial pObj)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                oRepoHistorial.Agregar(pObj);
                scope.Complete();
            }
            return pObj.Id;
        }

        public int ActualizarHistorial(ProductoST_Historial pObj)
        {

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    int res = oRepoHistorial.Actualizar(pObj);
                    scope.Complete();
                    return res;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error en el metodo Actualizar ProductoST_Historial");
            }

        }
        #endregion

        #region Métodos de Consulta
        public IQueryable<ProductoST_Historial> TraerTodosHistoriales()
        {
            IQueryable<ProductoST_Historial> lista = null;
            try
            {
                lista = oRepoHistorial.TraerTodos();
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodos ProductoST_Historial");
            }
            finally
            {
                lista = null;
            }
        }

        public ProductoST_Historial TraerUnicoHistorialXId(long pId)
        {
            return oRepoHistorial.TraerUnico(p => p.Id == pId);
        }

        public void PasarEstado(ProductoST_Ingreso ingreso, long IdUsuariologueado, string comentario,long idForm = 0, long idFormRespuesta = 0, long idEstadoSiguiente = 0)
        {
            List<Workflow_Ruta> listaEstados;
            Workflow_Estado estadoSiguiente;
            if (ingreso.IdEstadoActual == 0)
            {
                listaEstados = oRepoWorkflowRuta.TraerTodos().Where(w => w.IdWorkflow == ingreso.IdWorkflow).ToList();
                listaEstados.RemoveAll(r => r.oWorkflowEstado.EsInicial != true);
                estadoSiguiente = listaEstados.FirstOrDefault().oWorkflowEstado;
            }
            else
            {
                listaEstados = oRepoWorkflowRuta.TraerTodos().Where(w => w.IdWorkflow == ingreso.IdWorkflow && w.IdWorkflowEstado == ingreso.IdEstadoActual).ToList();
                estadoSiguiente = listaEstados.FirstOrDefault().oEstadoSiguiente;
            }

            ProductoST_Historial historial = new ProductoST_Historial();
            historial.IdUsuarioModifico = IdUsuariologueado;
            if (idEstadoSiguiente == 0)
                historial.IdNuevoEstado = estadoSiguiente.Id;
            else
                historial.IdNuevoEstado = idEstadoSiguiente;
            historial.IdEstadoAnterior = ingreso.IdEstadoActual;
            historial.IdProductoSTIngreso = ingreso.Id;
            historial.FecUltimaModificacion = DateTime.Now;
            historial.Comentario = comentario;
            historial.IdFormulario = idForm;
            historial.IdFormrespuestas = idFormRespuesta;

            //Calculo la duracion de tiempo en este estado
            if (historial.IdEstadoAnterior != 0)
            {
                //No es un estado inicial
                //Busco el estado anterior para sacar la fecha de inicio
                DateTime mFechaIngresoEstado = oRepoHistorial.TraerTodos().Where(p => p.IdNuevoEstado == historial.IdEstadoAnterior && p.IdProductoSTIngreso == historial.IdProductoSTIngreso).FirstOrDefault().FecUltimaModificacion;
                DateTime mFechaEgresoEstado = historial.FecUltimaModificacion;

                var mDiferencia = CantidadHorasLaborales_Duracion(mFechaIngresoEstado, mFechaEgresoEstado);
                historial.Diferencia = Convert.ToInt64( mDiferencia.TotalMinutes);
            }


            oRepoHistorial.Agregar(historial);

            if (idEstadoSiguiente == 0)
                ingreso.IdEstadoActual = estadoSiguiente.Id;
            else
                ingreso.IdEstadoActual = idEstadoSiguiente;
            oRepoIngreso.Actualizar(ingreso);

        }

        public void PasarEstadoB(ProductoST_Ingreso ingreso, long IdUsuariologueado, string comentario, long idForm = 0, long idFormRespuesta = 0)
        {
            List<Workflow_Ruta> listaEstados;
            Workflow_Estado estadoSiguiente;

            listaEstados = oRepoWorkflowRuta.TraerTodos().Where(w => w.IdWorkflow == ingreso.IdWorkflow && w.IdWorkflowEstado == ingreso.IdEstadoActual).ToList();
            estadoSiguiente = listaEstados.FirstOrDefault().oEstadoSiguienteB;

            ProductoST_Historial historial = new ProductoST_Historial();
            historial.IdUsuarioModifico = IdUsuariologueado;
            historial.IdNuevoEstado = estadoSiguiente.Id;
            historial.IdEstadoAnterior = ingreso.IdEstadoActual;
            historial.IdProductoSTIngreso = ingreso.Id;
            historial.FecUltimaModificacion = DateTime.Now;
            historial.Comentario = comentario;
            historial.IdFormulario = idForm;
            historial.IdFormrespuestas = idFormRespuesta;

            //Calculo la duracion de tiempo en este estado
            if (historial.IdEstadoAnterior != 0)
            {
                //No es un estado inicial
                //Busco el estado anterior para sacar la fecha de inicio
                DateTime mFechaIngresoEstado = oRepoHistorial.TraerTodos().Where(p => p.IdNuevoEstado == historial.IdEstadoAnterior && p.IdProductoSTIngreso == historial.IdProductoSTIngreso).FirstOrDefault().FecUltimaModificacion;
                DateTime mFechaEgresoEstado = historial.FecUltimaModificacion;

                var mDiferencia = CantidadHorasLaborales_Duracion(mFechaIngresoEstado, mFechaEgresoEstado);
                historial.Diferencia = Convert.ToInt64(mDiferencia.TotalMinutes);
            }

            oRepoHistorial.Agregar(historial);

            ingreso.IdEstadoActual = estadoSiguiente.Id;
            oRepoIngreso.Actualizar(ingreso);
        }

        public IQueryable<ProductoST_Historial> TraerHistorialXIngreso(long Id)
        {
            IQueryable<ProductoST_Historial> lista = null;
            try
            {
                lista = oRepoHistorial.TraerTodos().Where(p => p.IdProductoSTIngreso == Id);
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerHistorialXIngreso");
            }
            finally
            {
                lista = null;
            }
        }
        #endregion
        #endregion


        #region Calculo de Tiempo en el Estado
        public TimeSpan CantidadHorasLaborales_Duracion(DateTime pFechaDesde, DateTime pFechaHasta)
        {
            double mTotalMinutes = 0;

            TimeSpan mDias = (pFechaHasta - pFechaDesde).Duration();

            int mDiferenciaMinutos = Convert.ToInt32(mDias.TotalMinutes);

            for (int pMinute = 0; pMinute <= mDiferenciaMinutos; pMinute++)
            {
                DateTime mTiempo = pFechaDesde.AddMinutes(pMinute);
                if ((mTiempo.DayOfWeek <= DayOfWeek.Friday && mTiempo.DayOfWeek >= DayOfWeek.Monday) && (mTiempo.Hour >= 8 && mTiempo.Hour <= 17))
                {
                    mTotalMinutes += 1;
                }
            }
            TimeSpan mTotalHours;
            mTotalHours = TimeSpan.FromMinutes(mTotalMinutes);
            return mTotalHours;
        }
        #endregion
    }
}
