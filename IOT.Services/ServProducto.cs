﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IOT.ORM;
using IOT.Domain;
using NHibernate;
using System.Transactions;

namespace IOT.Services
{
    public class ServProducto : BaseService
    {
        private Repositorio<TipoProducto> oRepo = null;
        private Repositorio<TipoProducto_Componente> oRepoComp = null;
        #region Constructores

        public ServProducto(ISession pSession) : base(pSession)
        {
            oRepo = new Repositorio<TipoProducto>(oSession);
            oRepoComp = new Repositorio<TipoProducto_Componente>(oSession);
        }

        #endregion

        #region Métodos ABM

        public long Agregar(TipoProducto pObj)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                oRepo.Agregar(pObj);
                scope.Complete();
            }

            return pObj.Id;
        }

        public int Actualizar(TipoProducto pObj)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    int res = oRepo.Actualizar(pObj);
                    scope.Complete();
                    return res;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error en el metodo Actualizar Producto");
            }
        }

        public long Borrar(long pId)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    List<TipoProducto> oLista = oRepo.TraerTodos(p => p.Id == pId).ToList();
                    foreach (TipoProducto item in oLista)
                    {
                        oRepo.Borrar(item);
                    }

                    scope.Complete();
                    return pId;
                }
            }
            catch (Exception)
            {
                throw new Exception("Error en el metodo Borrar Producto");
            }
        }
        #endregion

        #region Métodos de Consulta


        public IQueryable<TipoProducto> TraerTodos()
        {

            IQueryable<TipoProducto> lista = null;
            try
            {
                lista = oRepo.TraerTodos();
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodos Producto");
            }
            finally
            {
                lista = null;
            }

        }

        public TipoProducto TraerUnicoXId(long pId)
        {
            return oRepo.TraerUnico(p => p.Id == pId);
        }

        #endregion

        #region Componentes Relacionados
        #region Métodos ABM

        public long AgregarComp(TipoProducto_Componente pObj)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                oRepoComp.Agregar(pObj);
                scope.Complete();
            }

            return pObj.Id;
        }

        public int ActualizarComp(TipoProducto_Componente pObj)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    int res = oRepoComp.Actualizar(pObj);
                    scope.Complete();
                    return res;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error en el metodo Actualizar Producto_Componente");
            }
        }

        public long BorrarComp(long pId)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    List<TipoProducto_Componente> oLista = oRepoComp.TraerTodos(p => p.Id == pId).ToList();
                    foreach (TipoProducto_Componente item in oLista)
                    {
                        oRepoComp.Borrar(item);
                    }

                    scope.Complete();
                    return pId;
                }
            }
            catch (Exception)
            {
                throw new Exception("Error en el metodo Borrar Producto_Componente");
            }
        }
        #endregion

        #region Métodos de Consulta


        public IQueryable<TipoProducto_Componente> TraerTodosComp()
        {

            IQueryable<TipoProducto_Componente> lista = null;
            try
            {
                lista = oRepoComp.TraerTodos();
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodos Producto");
            }
            finally
            {
                lista = null;
            }

        }

        public IQueryable<TipoProducto_Componente> TraerTodosCompXProducto(long pIdProducto)
        {

            IQueryable<TipoProducto_Componente> lista = null;
            try
            {
                lista = oRepoComp.TraerTodos().Where(p => p.IdProducto == pIdProducto);
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodosCompXProducto");
            }
            finally
            {
                lista = null;
            }

        }
        public IQueryable<TipoProducto_Componente> TraerTodosCompXComponente(long pIdComponente)
        {

            IQueryable<TipoProducto_Componente> lista = null;
            try
            {
                lista = oRepoComp.TraerTodos().Where(p => p.IdComponente == pIdComponente);
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodosCompXComponente");
            }
            finally
            {
                lista = null;
            }

        }

        public TipoProducto_Componente TraerUnicoXIdComp(long pId)
        {
            return oRepoComp.TraerUnico(p => p.Id == pId);
        }

        #endregion

        #endregion
    }
}
