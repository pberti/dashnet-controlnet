﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IOT.ORM;
using IOT.Domain;
using NHibernate;
using System.Transactions;

namespace IOT.Services
{
    public class ServOrdenProduccion : BaseService
    {
        #region variables
        private Repositorio<OrdenProd> oRepo = null;
        private Repositorio<OpProductos> oRepoProductos = null;
        private Repositorio<OpEntregas> oRepoEntregas = null;
        private Repositorio<OpNovedades> oRepoNovedades = null;
        private Repositorio<OpEstados> oRepoEstados = null;
        private Repositorio<OpEntregasEstado> oRepoEntregasEstado = null;
        private Repositorio<TipoProducto> oRepoTipoProducto = null;
        #endregion

        #region Constructores

        public ServOrdenProduccion(ISession pSession) : base(pSession)
        {
            oRepo = new Repositorio<OrdenProd>(oSession);
            oRepoProductos = new Repositorio<OpProductos>(oSession);
            oRepoEntregas = new Repositorio<OpEntregas>(oSession);
            oRepoNovedades = new Repositorio<OpNovedades>(oSession);
            oRepoEstados = new Repositorio<OpEstados>(oSession);
            oRepoEntregasEstado = new Repositorio<OpEntregasEstado>(oSession);
            oRepoTipoProducto = new Repositorio<TipoProducto>(oSession);
        }

        #endregion

        #region Ordenes de Produccion

        #region Métodos ABM

        public long Agregar(OrdenProd pObj)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                oRepo.Agregar(pObj);
                scope.Complete();
            }

            return pObj.Id;
        }

        public int Actualizar(OrdenProd pObj)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    int res = oRepo.Actualizar(pObj);
                    scope.Complete();
                    return res;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error en el metodo Actualizar OrdenProd");
            }
        }

        public long Borrar(long pId)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    List<OrdenProd> oLista = oRepo.TraerTodos(p => p.Id == pId).ToList();
                    foreach (OrdenProd item in oLista)
                    {
                        oRepo.Borrar(item);
                    }

                    scope.Complete();
                    return pId;
                }
            }
            catch (Exception)
            {
                throw new Exception("Error en el metodo Borrar OrdenProd");
            }
        }
        #endregion

        #region Métodos de Consulta


        public IQueryable<OrdenProd> TraerTodos()
        {

            IQueryable<OrdenProd> lista = null;
            try
            {
                lista = oRepo.TraerTodos();
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodos OrdenProd");
            }
            finally
            {
                lista = null;
            }

        }
        public IQueryable<OrdenProd> TraerTodosFiltro(OrdenProd pObj)
        {

            IQueryable<OrdenProd> lista = null;
            try
            {
                lista = oRepo.TraerTodos();
                if (pObj.SolicitadoPor != string.Empty)
                {
                    lista = lista.Where(p => p.SolicitadoPor.Contains(pObj.SolicitadoPor));
                }
                if (pObj.NroPedidoExterno != string.Empty)
                {
                    lista = lista.Where(p => p.NroPedidoExterno.Contains(pObj.NroPedidoExterno));
                }
                if (pObj.NroPedidoInterno != string.Empty)
                {
                    lista = lista.Where(p => p.NroPedidoInterno.Contains(pObj.NroPedidoInterno));
                }
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodos OrdenProd");
            }
            finally
            {
                lista = null;
            }

        }

        public OrdenProd TraerUnicoXId(long pId)
        {
            return oRepo.TraerUnico(p => p.Id == pId);
        }

        #endregion

        #endregion

        #region Productos de las Ordenes de Produccion

        #region Métodos ABM

        public long AgregarOPP(OpProductos pObj)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    oRepoProductos.Agregar(pObj);
                    scope.Complete();
                }

                return pObj.Id;
            }
            catch (Exception e)
            {
                throw new Exception("Error en el metodo Agregar OPP");
            }
        }

        public int ActualizarOPP(OpProductos pObj)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    int res = oRepoProductos.Actualizar(pObj);

                    scope.Complete();
                    return res;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error en el metodo Actualizar ActualizarOPP");
            }
        }

        public long BorrarOPP(long pId)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    List<OpProductos> oLista = oRepoProductos.TraerTodos(p => p.Id == pId).ToList();
                    foreach (OpProductos item in oLista)
                    {
                        oRepoProductos.Borrar(item);
                    }

                    scope.Complete();
                    return pId;
                }
            }
            catch (Exception)
            {
                throw new Exception("Error en el metodo Borrar BorrarOPP");
            }
        }
        #endregion

        #region Métodos de Consulta


        public IQueryable<OpProductos> TraerTodosOPP()
        {

            IQueryable<OpProductos> lista = null;
            try
            {
                lista = oRepoProductos.TraerTodos();
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodos TraerTodosOPP");
            }
            finally
            {
                lista = null;
            }

        }

        public OpProductos TraerUnicoXIdOPP(long pId)
        {
            return oRepoProductos.TraerUnico(p => p.Id == pId);
        }


        public IQueryable<OpProductos> TraerTodosProdxOrden(long idOrdenP)
        {

            IQueryable<OpProductos> lista = null;
            try
            {
                lista = oRepoProductos.TraerTodos().Where(p => p.IdOP == idOrdenP);
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodosProdxOrden");
            }
            finally
            {
                lista = null;
            }

        }

        #endregion

        #endregion

        #region Entregas de las Ordenes de Produccion

        #region Métodos ABM

        public long AgregarOPE(OpEntregas pObj)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                oRepoEntregas.Agregar(pObj);
                scope.Complete();
            }

            return pObj.Id;
        }

        public int ActualizarOPE(OpEntregas pObj)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    int res = oRepoEntregas.Actualizar(pObj);
                    scope.Complete();
                    return res;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error en el metodo Actualizar ActualizarOPE");
            }
        }

        public long BorrarOPE(long pId)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    List<OpEntregas> oLista = oRepoEntregas.TraerTodos(p => p.Id == pId).ToList();
                    foreach (OpEntregas item in oLista)
                    {
                        oRepoEntregas.Borrar(item);
                    }

                    scope.Complete();
                    return pId;
                }
            }
            catch (Exception)
            {
                throw new Exception("Error en el metodo Borrar BorrarOPE");
            }
        }
        #endregion

        #region Métodos de Consulta


        public IQueryable<OpEntregas> TraerTodosOPE()
        {

            IQueryable<OpEntregas> lista = null;
            try
            {
                lista = oRepoEntregas.TraerTodos();
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodos Orden-Entregas");
            }
            finally
            {
                lista = null;
            }

        }


        public OpEntregas TraerUnicoXIdOPE(long pId)
        {
            return oRepoEntregas.TraerUnico(p => p.Id == pId);
        }

        public IQueryable<OpEntregas> TraerTodasEntregasxOrden(long idOrdenP)
        {

            IQueryable<OpEntregas> lista = null;
            try
            {
                lista = oRepoEntregas.TraerTodos().Where(p => p.IdOP == idOrdenP);
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodasEntregasxOrden");
            }
            finally
            {
                lista = null;
            }

        }
        #endregion

        #region Estado Entrega

        #region Métodos ABM

        public long AgregarOPEntregaEstado(OpEntregasEstado nObj)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                oRepoEntregasEstado.Agregar(nObj);
                scope.Complete();
            }

            return nObj.Id;
        }

        public int ActualizarOPEntregaEstado(OpEntregasEstado pObj)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    int res = oRepoEntregasEstado.Actualizar(pObj);
                    scope.Complete();
                    return res;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error en el metodo Actualizar ActualizarOPEntregaEstado");
            }
        }

        public long BorrarOPEntregaEstado(long nId)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    List<OpEntregasEstado> oLista = oRepoEntregasEstado.TraerTodos(n => n.Id == nId).ToList();
                    foreach (OpEntregasEstado item in oLista)
                    {
                        oRepoEntregasEstado.Borrar(item);
                    }

                    scope.Complete();
                    return nId;
                }
            }
            catch (Exception)
            {
                throw new Exception("Error en el metodo Borrar BorrarOPEntregaEstado");
            }
        }
        #endregion

        #region Métodos de Consulta


        public IQueryable<OpEntregasEstado> TraerTodosOPEntregaEstados()
        {

            IQueryable<OpEntregasEstado> lista = null;
            try
            {
                lista = oRepoEntregasEstado.TraerTodos();
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodos TraerTodosOPEntregaEstados");
            }
            finally
            {
                lista = null;
            }

        }

        public OpEntregasEstado TraerUnicoXIdOPEntregaEstado(long nId)
        {
            return oRepoEntregasEstado.TraerUnico(n => n.Id == nId);
        }

        #endregion

        #endregion

        #endregion

        #region Novedades de las Ordenes de Produccion

        #region Métodos ABM

        public long AgregarOPN(OpNovedades nObj)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                oRepoNovedades.Agregar(nObj);
                scope.Complete();
            }

            return nObj.Id;
        }

        public int ActualizarOPN(OpNovedades pObj)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    int res = oRepoNovedades.Actualizar(pObj);
                    scope.Complete();
                    return res;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error en el metodo Actualizar ActualizarOPN");
            }
        }

        public long BorrarOPN(long nId)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    List<OpNovedades> oLista = oRepoNovedades.TraerTodos(n => n.Id == nId).ToList();
                    foreach (OpNovedades item in oLista)
                    {
                        oRepoNovedades.Borrar(item);
                    }

                    scope.Complete();
                    return nId;
                }
            }
            catch (Exception)
            {
                throw new Exception("Error en el metodo Borrar BorrarOPN");
            }
        }
        #endregion

        #region Métodos de Consulta


        public IQueryable<OpNovedades> TraerTodosOPN()
        {

            IQueryable<OpNovedades> lista = null;
            try
            {
                lista = oRepoNovedades.TraerTodos();
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodos TraerTodosOPN");
            }
            finally
            {
                lista = null;
            }

        }

        public OpNovedades TraerUnicoXIdOPN(long nId)
        {
            return oRepoNovedades.TraerUnico(n => n.Id == nId);
        }


        public IQueryable<OpNovedades> TraerTodasNovedadesXOP(long idOrdenP)
        {

            IQueryable<OpNovedades> lista = null;
            try
            {
                lista = oRepoNovedades.TraerTodos().Where(n => n.IdOP == idOrdenP);
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodasNovedadesXOP");
            }
            finally
            {
                lista = null;
            }

        }

        #endregion

        #endregion

        #region Estados de las Ordenes de Produccion

        #region Métodos ABM

        public long AgregarOPEstados(OpEstados nObj)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                oRepoEstados.Agregar(nObj);
                scope.Complete();
            }

            return nObj.Id;
        }

        public int ActualizarOPEstados(OpEstados pObj)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    int res = oRepoEstados.Actualizar(pObj);
                    scope.Complete();
                    return res;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error en el metodo Actualizar ActualizarOPEstados");
            }
        }

        public long BorrarOPEstados(long nId)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    List<OpEstados> oLista = oRepoEstados.TraerTodos(n => n.Id == nId).ToList();
                    foreach (OpEstados item in oLista)
                    {
                        oRepoEstados.Borrar(item);
                    }

                    scope.Complete();
                    return nId;
                }
            }
            catch (Exception)
            {
                throw new Exception("Error en el metodo Borrar BorrarOPEstados");
            }
        }
        #endregion

        #region Métodos de Consulta
        public IQueryable<OpEstados> TraerTodosOPEstados()
        {

            IQueryable<OpEstados> lista = null;
            try
            {
                lista = oRepoEstados.TraerTodos();
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodos TraerTodosOPEstados");
            }
            finally
            {
                lista = null;
            }

        }

        public OpEstados TraerUnicoXIdOPEstado(long nId)
        {
            return oRepoEstados.TraerUnico(n => n.Id == nId);
        }

        #endregion

        #endregion

    }
}
