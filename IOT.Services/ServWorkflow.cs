﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IOT.ORM;
using IOT.Domain;
using NHibernate;
using System.Transactions;

namespace IOT.Services
{
    public class ServWorkflow : BaseService
    {
        private Repositorio<Workflow> oRepo = null;

        #region Constructores
        public ServWorkflow(ISession pSession) : base(pSession)
        {
            oRepo = new Repositorio<Workflow>(oSession);
        }

        #endregion

        #region Métodos ABM

        public long Agregar(Workflow pObj)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                oRepo.Agregar(pObj);
                scope.Complete();
            }
            return pObj.Id;
        }

        public int Actualizar(Workflow pObj)
        {

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    int res = oRepo.Actualizar(pObj);
                    scope.Complete();
                    return res;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error en el metodo Actualizar Workflow");
            }

        }




        #endregion

        #region Métodos de Consulta
        public IQueryable<Workflow> TraerTodos(bool? pSoloActivos)
        {
            IQueryable<Workflow> lista = null;
            try
            {
                lista = oRepo.TraerTodos().Where(e =>
                pSoloActivos == null || e.Inactivo == !pSoloActivos);
                return lista;
            }
            catch
            {
                throw new Exception("Error en el metodo TraerTodos Workflow");
            }
            finally
            {
                lista = null;
            }
        }

        public Workflow TraerUnicoXId(long pId)
        {
            return oRepo.TraerUnico(p => p.Id == pId);
        }
        #endregion

    }
}
