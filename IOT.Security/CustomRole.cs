﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;
using IOT.Services;
using NHibernate;

namespace IOT.Security
{
    public class CustomRole : RoleProvider
    {
        private string applicationName;

        private ISession session = IOT.ORM.SessionHelper.BuildSessionFactory().OpenSession();

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override string ApplicationName
        {
            get
            {
                return applicationName;
            }
            set
            {
                applicationName = value;
            }
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            ServUsuario oServ = new ServUsuario(session);
            return oServ.TraerPerfilesFiltro(roleName, usernameToMatch).Select(x => x.Descripcion).ToArray();
        }

        public override string[] GetAllRoles()
        {
            ServUsuario oServ = new ServUsuario(session);
            return oServ.TraerTodosPerfiles().Select(x => x.Descripcion).ToArray();
        }

        public override string[] GetRolesForUser(string username)
        {
            ServUsuario oServ = new ServUsuario(session);
            string[] oPerfiles = { oServ.TraerPerfilPorUsuario(username).Descripcion };
            return oPerfiles;
        }

        public override string[] GetUsersInRole(string roleName) 
        {
            ServUsuario oServ = new ServUsuario(session);
            return oServ.TraerPerfilesFiltro(roleName, "").Select(x => x.Descripcion).ToArray();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            ServUsuario oServ = new ServUsuario(session);
            Boolean tienePerfil = oServ.TraerPerfilesFiltro(roleName, username).Count > 0;
            return tienePerfil;
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            ServUsuario oServ = new ServUsuario(session);
            Boolean existePerfil = oServ.TraerPerfilesFiltro(roleName, "").Count > 0;
            return existePerfil;
        }
    }
}
