﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;
using System.Transactions;
using NHibernate;
using IOT.Services;
using IOT.ORM;
using IOT.Domain;

namespace IOT.Security
{
    public class CustomMembership : MembershipProvider
    {
        private string applicationName;

        private ISession session = SessionHelper.BuildSessionFactory().OpenSession();

        public override bool ValidateUser(string username, string password)
        {
            ServUsuario oServices = new ServUsuario(session);
            return oServices.LogearUsuario(username, password) > 0;
        }

        public override string ApplicationName
        {
            get
            {
                return applicationName;
            }
            set
            {
                applicationName = value;
            }
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            ServUsuario oServices = new ServUsuario(session);
            return oServices.CambiarPass(username, oldPassword, newPassword);
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            //membershipuser omember = new membershipuser(this.name,
            //    username,
            //    provideruserkey,
            //    email,
            //    passwordquestion,
            //    "usuario creado por autogestión",
            //    isapproved,
            //    false,
            //    datetime.now,
            //    datetime.now,
            //    datetime.now,
            //    datetime.now,
            //    datetime.now);

            //ServUsuarios oservices = new ServUsuarios();
            //Usuario ousuario = new Usuario();
            //ousuario.UserName = username;
            //ousuario.Pass = encriptacion.encriptar(password);
            ////ousuario.ay = "";
            //ousuario.IdTipoPerfil = 1;
            //ousuario.Activo = true;
            //ousuario.Email = email;


            //status = membershipcreatestatus.success;

            //try
            //{
            //    oservices.agregar(ousuario);
            //}
            //catch (exception ex)
            //{
            //    status = membershipcreatestatus.providererror;
            //    throw ex;
            //}

            //return omember;
            status = MembershipCreateStatus.ProviderError;
            return null;
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            //ServUsuarios oServices = new ServUsuarios();
            //var oUsuario = oServices.TraerPorUsuario(username);
            //oUsuario.Activo = false;
            //return oServices.Actualizar(oUsuario) > 0;
            return true;
        }

        public override bool EnablePasswordReset
        {
            get { return false; }
        }

        public override bool EnablePasswordRetrieval
        {
            get { return true; }
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            int countRecords = 0;
            ServUsuario oServices = new ServUsuario(session);
            var oUsuarios = oServices.TraerTodos();
            MembershipUserCollection oMembers = new MembershipUserCollection();
            foreach (var user in oUsuarios)
            {
                oMembers.Add(ConvertToMembership(user));
                countRecords++;
            }

            totalRecords = countRecords;
            return oMembers;
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotSupportedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            ServUsuario oServices = new ServUsuario(session);
            var oUsuarios = oServices.TraerPorUsuario(username);
            var oMember = ConvertToMembership(oUsuarios);
            return oMember;
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            ServUsuario oServices = new ServUsuario(session);
            var oUsuarios = oServices.TraerUnicoUsuario((Int64)providerUserKey);
            var oMember = ConvertToMembership(oUsuarios);
            return oMember;
        }

        public override string GetUserNameByEmail(string email)
        {
            ServUsuario oServices = new ServUsuario(session);
            var oUsuarios = oServices.TraerPorEmail(email);
            return oUsuarios.UserName;
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { return 50; }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { return 5; }
        }

        public override int MinRequiredPasswordLength
        {
            get { return 6; }
        }

        public override int PasswordAttemptWindow
        {
            get { return 50; }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { return MembershipPasswordFormat.Clear; }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { return "@\"(?=.{6,})(?=(.*\\d){1,})(?=(.*\\W){1,})"; }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { return false; }
        }

        public override bool RequiresUniqueEmail
        {
            get { return true; }
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }
        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new NotImplementedException();
        }

        private MembershipUser ConvertToMembership(Usuario oUsuarios)
        {
            if (oUsuarios == null)
            {
                return null;
            }
            else
            {
                MembershipUser oMember = new MembershipUser(this.Name,
                                                            oUsuarios.UserName,
                                                            oUsuarios.Id,
                                                            "",
                                                            "",
                                                            "",
                                                            (bool)oUsuarios.Activo,
                                                            false,
                                                            DateTime.Now,
                                                            DateTime.Now,
                                                            DateTime.Now,
                                                            DateTime.Now,
                                                            DateTime.Now);

                return oMember;
            }
        }
    }
}
