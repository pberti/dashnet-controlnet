﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Text;
using System.IO;
using System.Net;
using System.Collections.Specialized;

public static class ResponseHelper
{
    public static string getValue(string value)
    {
        return getValue(value, "");
    }

    public static string getValue(string value, string defaultValue)
    {
        if (string.IsNullOrWhiteSpace(value))
            return defaultValue;
        return value;
    }

    private static String PreparePOSTForm(string url, NameValueCollection data, string attr)
    {
        //Set a name for the form
        string formID = "PostForm";

        //Build the form using the specified data to be posted.
        StringBuilder strForm = new StringBuilder();
        strForm.Append("<form " + attr + " id=\"" + formID + "\" name=\"" + formID + "\" action=\"" + url + "\" method=\"POST\">");
        foreach (string key in data)
        {
            strForm.Append("<input type=\"hidden\" name=\"" + key + "\" value=\"" + data[key] + "\">");
        }
        strForm.Append("</form>");

        //Build the JavaScript which will do the Posting operation.
        StringBuilder strScript = new StringBuilder();
        strScript.Append("<script language='javascript'>");
        strScript.Append("var v" + formID + " = document." + formID + ";");
        strScript.Append("v" + formID + ".submit();");
        strScript.Append("</script>");

        //Return the form and the script concatenated. (The order is important, Form then JavaScript)
        return strForm.ToString() + strScript.ToString();
    }

    public static void RedirectAndPOST(Page page, string destinationUrl, NameValueCollection data)
    {
        RedirectAndPOST(page, destinationUrl, data, "", true);
    }

    public static void RedirectAndPOST(Page page, string destinationUrl, NameValueCollection data, string attr, bool controlsHiden)
    {
        if (controlsHiden)
        {
            for (int i = 0; i < page.Controls.Count; i++)
            {
                page.Controls[i].Visible = false;
            }
        }
        //Prepare the Posting form
        string strForm = PreparePOSTForm(ToAbsoluteUrl(destinationUrl), data, attr);

        //Add a literal control the specified page holding the Post Form, this is to submit the Posting form with the request.
        page.Controls.Add(new LiteralControl(strForm));
        StringBuilder cstext2 = new StringBuilder();
        cstext2.Append("<script type=\"text/javascript\"> ");
        cstext2.Append("setTimeout(function () { $(\"#mModal\").modal({ backdrop: 'static', keyboard: false }); }, 100);");
        cstext2.Append("</script>");
        Type cstype = page.GetType();
        String csname1 = "openBlockScreen";
        page.ClientScript.RegisterClientScriptBlock(cstype, csname1, cstext2.ToString(), false);
    }


    public static string ToAbsoluteUrl(this string relativeUrl)
    {
        if (string.IsNullOrEmpty(relativeUrl))
            return relativeUrl;

        if (HttpContext.Current == null)
            return relativeUrl;

        if (relativeUrl.Substring(0, Math.Min(relativeUrl.Length, 10)).ToLower().Contains("://"))
            return relativeUrl;

        if (relativeUrl.StartsWith("/"))
            relativeUrl = relativeUrl.Insert(0, "~");
        if (!relativeUrl.StartsWith("~/"))
            relativeUrl = relativeUrl.Insert(0, "~/");

        var url = HttpContext.Current.Request.Url;
        var port = url.Port != 80 ? (":" + url.Port) : String.Empty;

        return String.Format("{0}://{1}{2}{3}",
            url.Scheme, url.Host, port, VirtualPathUtility.ToAbsolute(relativeUrl));
    }

    public static NameValueCollection Post(params object[] list)
    {
        NameValueCollection post = new NameValueCollection();
        
        if (list.Length > 1)
        {
            for (int i = 0; i < list.Length; i = i + 2)
            {
                if (list.Length > i + 1)
                {
                    post.Add(list[i].ToString(), list[i + 1].ToString());
                }
            }
        }

        return post;
    }

    public static void Redirect(this HttpResponse response, string url, string target, string windowFeatures)
    {

        if ((String.IsNullOrEmpty(target) || target.Equals("_self", StringComparison.OrdinalIgnoreCase)) && String.IsNullOrEmpty(windowFeatures))
        {
            response.Redirect(url);
        }
        else
        {
            Page page = (Page)HttpContext.Current.Handler;

            if (page == null)
            {
                throw new InvalidOperationException("Cannot redirect to new window outside Page context.");
            }
            url = page.ResolveClientUrl(url);

            string script;
            if (!String.IsNullOrEmpty(windowFeatures))
            {
                script = @"window.open(""{0}"", ""{1}"", ""{2}"");";
            }
            else
            {
                script = @"window.open(""{0}"", ""{1}"");";
            }
            script = String.Format(script, url, target, windowFeatures);
            ScriptManager.RegisterStartupScript(page, typeof(Page), "Redirect", script, true);
        }
    }
}