﻿using System.Threading.Tasks;
using IOT.Domain;
using IOT.Services;

/// <summary>
/// Summary description for Schedule
/// </summary>
public static class Schedule
{
    public static object SchedulerLock = new object();

    public static bool IsRun { get; private set; }



    public static void Start()
    {
        lock (SchedulerLock)
        {
            if (!IsRun)
            {
                Run();
            }
        }
    }

    private static Task Run()
    {
        IsRun = true;
        return Task.Factory.StartNew(async () =>
        {
            while (IsRun)
            {
                try
                {
                    //TODO
                    //Poner logica para ejecutar tareas
                    int mTiempo = 60000  * 240;
                    ActualizadoOTs();
                    await Task.Delay(mTiempo);
                }
                catch (System.Exception ex)
                {

                    throw;
                }
                

            }
        });
    }

    private static void ActualizadoOTs()
    {
        try
        {
            var mResult = Services.Get<ServOTs>().ActualizacionDatosRedmine();
        }
        catch (System.Exception ex)
        {

            throw;
        }

    }

    public static void Stop()
    {
        lock (SchedulerLock)
        {
            IsRun = false;
        }
    }
}