﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Urls
/// </summary>
public class Urls
{
    public static string BaseUrl
    {
        get
        {
            string url = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + HttpContext.Current.Request.ApplicationPath;
            return url;
        }
    }
}