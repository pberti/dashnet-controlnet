﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using IOT.Mapping;
using IOT.Services;
using NHibernate;
using StructureMap;
using System;

/// <summary>
/// Summary description for Services
/// </summary>
public static class Services
{
    public static T Get<T>() where T : BaseService
    {
        ISession session = ObjectFactory.GetInstance<ISession>();

        T oService = (T)Activator.CreateInstance(typeof(T), new object[] { session });

        return oService;
    }

    public static ISessionFactory GetSessionFactory()
    {
        ISessionFactory sessionFactory = null;

        try
        {

            sessionFactory = Fluently.Configure()
                .Database(MySQLConfiguration
                .Standard
                .ShowSql()
                .QuerySubstitutions("1 true, 0 false")
                .ConnectionString(c => c.
                FromConnectionStringWithKey
                ("IOT.ORM.Properties.Settings.ConnectionString")))
                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<IOT.Mapping.MapUsuario>())
                .BuildSessionFactory();
        }
        catch (Exception ex)
        {
            if (ex.InnerException == null)
            {
                throw ex;
            }
            else
            {
                throw ex.InnerException;
            }
        }

        return sessionFactory;
    }
}

