﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.ComponentModel.DataAnnotations;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Castle.Core.Internal;
using System.Web.Security;
using System.Configuration;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using IOT.Domain;
using IOT.Services;


/// <summary>
/// Summary description for PageBaae
/// </summary>
public class PageBase : Page
{
    #region --- Propiedades ---


    public string URLAnterior
    {
        get { if (Session["_URLAnterior_"] != null) { return Session["_URLAnterior_"].ToString(); } else { return ""; } }
        set { Session.Add("_URLAnterior_", value); }
    }

    public string user
    {
        get { if (Session["_user_"] != null) { return Session["_user_"].ToString(); } else { return ""; } }
        set { Session.Add("_user_", value); }
    }

    public long IdUsuarioLogueado
    {
        get
        {
            MembershipUser member = null;
            // Busco el usuario, dependiendo si el usuario ya se ha logueado o no, si no se ha logueado tenemos opcion de pasarle el parametro user para que busque por nombre de
            // usuario.
            // Luego si es null retornamos -1(no existe), y si existe retornamos el Id.
            if (string.IsNullOrEmpty(user))
            {
                member = Membership.GetUser();
            }
            else
            {
                member = Membership.GetUser(user);
            }

            if (member == null)
            {
                return -1;
            }

            return (Int64)member.ProviderUserKey;
        }
    }

    public string TituloForm
    {
        get { return getValue<string>("TituloForm", ""); }
        set { ViewState["TituloForm"] = value; }
    }
    protected Boolean EsNuevo
    {
        get { return getValue<Boolean>("_EsNuevo", false); }
        set { ViewState["_EsNuevo"] = value; }
    }


    public void DeterminarCriterioOrdenacion(string pSortExpression)
    {
        if (DireccionOrdenacion == "")
        {
            DireccionOrdenacion = "ascending";
        }
        else
        {
            if (DireccionOrdenacion == "ascending")
                DireccionOrdenacion = "descending";
            else
                DireccionOrdenacion = "ascending";
        }
        CriterioOrdenacion = string.Format("{0} {1}",
            pSortExpression == string.Empty ? "Id" : pSortExpression,
            DireccionOrdenacion);
    }

    protected Boolean EraNuevo
    {
        get { return getValue<Boolean>("_EraNuevo", false); }
        set { ViewState["_EraNuevo"] = value; }
    }

    public long IdPerfilUsuarioLogueado
    {
        get { return TraerUsuarioLogeado().IdTipoPerfil; }
        private set { }
    }

    public long EntityId
    {
        get { return getValue<long>("EntityId", -1); }
        set { ViewState["EntityId"] = value; }
    }

    public string DireccionOrdenacion
    {
        get { return getValue<string>("Direccion", ""); }
        set { ViewState["Direccion"] = value; }
    }

    public long? IdTramite
    {
        get { return getValue<long>("IdTramite", 0); }
        set { ViewState["IdTramite"] = value; }
    }

    //Lista de los productos que estan en produccion y se pasan como parametro para cambiar de estado
    public List<Produccion> auxLstProduccion
    {
        get { return getValue<List<Produccion>>("auxLstProduccion", new List<Produccion>()); }
        set { ViewState["auxLstProduccion"] = value; }
    }

    /// <summary>
    /// Esta se usa solamente cuando debo pasar por parametro el IdTramite
    /// </summary>
    public long? IdTramiteURL
    {
        get { return getValue<long>("IdTramiteURL", 0); }
        set { ViewState["IdTramiteURL"] = value; }
    }

    public T getValue<T>(string key, T defaultValue)
    {
        T value = defaultValue;
        if (value.Equals(defaultValue))
        {
            if (ViewState[key] != null)
            {
                try
                {
                    value = (T)Convert.ChangeType(ViewState[key], typeof(T));
                }
                catch { }
            }
        }

        if (value.Equals(defaultValue))
        {
            if (Request.Params[key] != null)
            {
                try
                {
                    value = (T)Convert.ChangeType(Request.Params[key].ToString(), typeof(T));
                }
                catch { }
            }
        }
        if (value.Equals(defaultValue))
        {
            if (Request.QueryString[key] != null)
            {
                try
                {
                    value = (T)Convert.ChangeType(Request.QueryString[key].ToString(), typeof(T));
                }
                catch { }
            }
        }
        return value;
    }

    public string CriterioOrdenacion
    {
        get { return getValue<string>("OrderBy", ""); }
        set { ViewState["OrderBy"] = value; }
    }
    #endregion

    #region --- Métodos de Usuario Logueado ---

    public Usuario TraerUsuarioLogeado()
    {
        var oMemUser = Membership.GetUser();

        if (oMemUser == null)
        {
            return null;
        }
        else
        {
            ServUsuario oServ = Services.Get<ServUsuario>();
            var oUsuario = oServ.TraerUnicoUsuario(Convert.ToInt64(oMemUser.ProviderUserKey));
            return oUsuario;
        }
    }

    #endregion

    #region --- Métodos de Cambio de estado ---
    public Workflow_Estado EstadoSiguiente(long idEstadoActual, long idWorkflow)
    {
        Workflow_Estado EstadoSiguiente;
        ServWorkflow_Ruta oServ = Services.Get<ServWorkflow_Ruta>();
        ServWorkFlow_Estado oServEstado = Services.Get<ServWorkFlow_Estado>();

        Workflow_Ruta EstadoActual = oServ.TraerTodos().Where(r => r.IdWorkflowEstado == idEstadoActual && r.IdWorkflow == idWorkflow).FirstOrDefault();

        EstadoSiguiente = oServEstado.TraerUnicoXId(EstadoActual.IdEstadoSiguiente);

        if (EstadoSiguiente != null)
            return EstadoSiguiente;
        else
            return null;
    }

    public Workflow_Estado EstadoSiguienteB(long idEstadoActual, long idWorkflow)
    {
        Workflow_Estado EstadoSiguiente;
        ServWorkflow_Ruta oServ = Services.Get<ServWorkflow_Ruta>();
        ServWorkFlow_Estado oServEstado = Services.Get<ServWorkFlow_Estado>();

        Workflow_Ruta EstadoActual = oServ.TraerTodos().Where(r => r.IdWorkflowEstado == idEstadoActual && r.IdWorkflow == idWorkflow).FirstOrDefault();

        EstadoSiguiente = oServEstado.TraerUnicoXId(EstadoActual.IdEstadoSiguienteB);

        if (EstadoSiguiente != null)
            return EstadoSiguiente;
        else
            return null;
    }

    public bool ComprobarPermiso(long IdEstadoActual, long id)
    {
        ServWorkflow_Permisos oServ = Services.Get<ServWorkflow_Permisos>();
        List<Workflow_Permisos> permisos = oServ.TraerXPerfil(id).ToList();
        List<Workflow_Estado> asist = new List<Workflow_Estado>();

        return permisos.Find(p => p.IdWorkflowEstado == IdEstadoActual).PuedeGrabar;
    }
    #endregion

    #region --- Eventos de la Página ---

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (!IsPostBack)
        {
            if (Page.Request.UrlReferrer != null)
            {
                URLAnterior = Page.Request.UrlReferrer.ToString();
            }
        }
    }

    #endregion

    #region Validate Model
    public bool ValidateModel<T>(T pEntity, ref HtmlGenericControl pForm) where T : IEntidad
    {
        var results = new List<ValidationResult>();
        var context = new ValidationContext(pEntity, serviceProvider: null, items: null);
        var isValid = Validator.TryValidateObject(pEntity, context, results, true);

        //reseteo los controles
        foreach (Control item in pForm.Controls)
        {
            if (item.ID != null)
            {
                if (item.ID.StartsWith("cg"))
                {
                    HtmlGenericControl control = (HtmlGenericControl)item;
                    control.Attributes.Remove("class");
                    control.Attributes.Add("class", "control-group");
                    var label = (Label)control.FindControlRecursive("lblVal" + item.ID.Replace("cg", string.Empty));
                    if (label != null)
                    {
                        label.Text = string.Empty;
                    }
                }
            }
        }

        if (!isValid)
        {
            //seteo los que tienen error
            foreach (ValidationResult validationResult in results)
            {
                string propName = validationResult.MemberNames.First();
                string errortext = validationResult.ErrorMessage;
                string controlId = "cg" + propName;
                HtmlGenericControl control = (HtmlGenericControl)pForm.FindControlRecursive(controlId);
                if (control != null)
                {
                    control.Attributes.Add("class", "control-group error");

                    var label = (Label)control.FindControlRecursive("lblVal" + propName);
                    if (label != null)
                    {
                        label.Text = errortext;
                    }
                }
            }

            return false;
        }

        return true;
    }

    /// <summary>
    /// Este metodo se utiliza para limpiar la pantalla despues de haberse validad, si es que se cancela o despues de grabarse
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="pEntity"></param>
    /// <param name="pForm"></param>
    public void ValidateModelFalse<T>(T pEntity, ref HtmlGenericControl pForm) where T : IEntidad
    {
        var results = new List<ValidationResult>();
        var context = new ValidationContext(pEntity, serviceProvider: null, items: null);

        //reseteo los controles
        foreach (Control item in pForm.Controls)
        {
            if (item.ID != null)
            {
                if (item.ID.StartsWith("cg"))
                {
                    HtmlGenericControl control = (HtmlGenericControl)item;
                    control.Attributes.Remove("class");
                    control.Attributes.Add("class", "control-group");
                    var label = (Label)control.FindControlRecursive("lblVal" + item.ID.Replace("cg", string.Empty));
                    if (label != null)
                    {
                        label.Text = string.Empty;
                    }
                }
            }
        }
    }
    #endregion

    public void Redirect(string url, params object[] parametros)
    {
        NameValueCollection post = ResponseHelper.Post("EntityId", EntityId, "IdTramite", IdTramite, "IdTramiteURL", IdTramiteURL, "TituloForm", TituloForm, "URLAnterior", URLAnterior);
        if (parametros.Length > 1)
        {
            for (int i = 0; i < parametros.Length; i = i + 2)
            {
                if (parametros.Length > i + 1)
                {
                    post.Add(parametros[i].ToString(), parametros[i + 1].ToString());
                }
            }
        }

        ResponseHelper.RedirectAndPOST(this, url, post);
    }

    public void RedirectBlank(string url, params object[] parametros)
    {
        NameValueCollection post = ResponseHelper.Post("EntityId", EntityId, "IdTramite", IdTramite, "IdTramiteURL", IdTramiteURL, "TituloForm", TituloForm, "URLAnterior", URLAnterior);
        if (parametros.Length > 1)
        {
            for (int i = 0; i < parametros.Length; i = i + 2)
            {
                if (parametros.Length > i + 1)
                {
                    post.Add(parametros[i].ToString(), parametros[i + 1].ToString());
                }
            }
        }

        ResponseHelper.RedirectAndPOST(this, url, post, "target='_blank'", false);
    }


    public PageBase()
    {
        //this.Theme = "Default";
    }

    /// <summary>
    /// Método genérico que carga un DropDownList según una lista de instancias casteadas a List<Object> (ambos pasados como parámetro)
    /// </summary>
    /// <param name="lObjects">List de instancias Object</param>
    /// <param name="pDdl">DropDownList a cargar</param>
    /// <param name="pDataTextField">Nombre del campo de la entidad original cuyo valor se mostrará en la selección (puede ser null)</param>
    /// <param name="pDataValueField">Nombre del campo de la entidad original cuyo valor será el valor de selección (puede ser null)</param>
    /// <param name="pDefaultOption">Opción que se muetra deleccionada por defecto (si es NULL a Empty entonces no carga nada)</param>
    public static void CargarDDL(List<Object> lObjects, DropDownList pDdl, string pDataTextField, string pDataValueField, string pDefaultOption, string pFynalOption)
    {
        try
        {

            if (lObjects.Count() == 0)
            {
                pDdl.Items.Clear();
                if (!string.IsNullOrEmpty(pDefaultOption))
                    pDdl.Items.Insert(0, new ListItem(pDefaultOption, ValoresConstantes.valueCeroString));
                else if (!string.IsNullOrEmpty(pFynalOption))
                    pDdl.Items.Insert(0, new ListItem(pFynalOption, ValoresConstantes.noOpcionesString));
                else
                    pDdl.Items.Insert(0, new ListItem(ValoresConstantes.sinOpciones, string.Empty));
                return;
            }
            else
            {
                pDdl.DataSource = lObjects;
                if (!string.IsNullOrEmpty(pDataTextField)) pDdl.DataTextField = pDataTextField;
                if (!string.IsNullOrEmpty(pDataValueField)) pDdl.DataValueField = pDataValueField;
            }
            pDdl.DataBind();
            if (!string.IsNullOrEmpty(pDefaultOption)) pDdl.Items.Insert(0, new ListItem(pDefaultOption, string.IsNullOrEmpty(pDataValueField) ? pDefaultOption : "0"));
            if (!string.IsNullOrEmpty(pFynalOption)) pDdl.Items.Add(new ListItem(pFynalOption, string.IsNullOrEmpty(pDataValueField) ? pFynalOption : "-1"));
        }
        catch
        {
            throw new Exception("Error en el método CargarDDL de PageBase");
        }
    }


}