﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Utils
/// </summary>
public class Utils
{
    public static TimeSpan CantidadHorasLaborales_Duracion(DateTime pFechaDesde, DateTime pFechaHasta)
    {
        double mTotalMinutes = 0;

        TimeSpan mDias = (pFechaHasta - pFechaDesde).Duration();

        int mDiferenciaMinutos = Convert.ToInt32(mDias.TotalMinutes);

        for (int pMinute = 0; pMinute <= mDiferenciaMinutos; pMinute++)
        {
            DateTime mTiempo = pFechaDesde.AddMinutes(pMinute);
            if ((mTiempo.DayOfWeek <= DayOfWeek.Friday && mTiempo.DayOfWeek >= DayOfWeek.Monday) && (mTiempo.Hour >= 8 && mTiempo.Hour <= 17))
            {
                mTotalMinutes += 1;
            }
        }
        TimeSpan mTotalHours;
        mTotalHours = TimeSpan.FromMinutes(mTotalMinutes);
        return mTotalHours;
    }
    /// <summary>
    /// Devuelve el Formato TIMESPAN en un String con formato entendible
    /// </summary>
    /// <param name="span"></param>
    /// <returns></returns>
    public static string ToReadableString(TimeSpan span)
    {
        string formatted = string.Format("{0}{1}{2}{3}",
            span.Duration().Days > 0 ? string.Format("{0:0} dias{1}, ", span.Days, span.Days == 1 ? String.Empty : "s") : string.Empty,
            span.Duration().Hours > 0 ? string.Format("{0:0} hrs{1}, ", span.Hours, span.Hours == 1 ? String.Empty : "s") : string.Empty,
            span.Duration().Minutes > 0 ? string.Format("{0:0} min{1}, ", span.Minutes, span.Minutes == 1 ? String.Empty : "s") : string.Empty,
            span.Duration().Seconds > 0 ? string.Format("{0:0} seg{1}", span.Seconds, span.Seconds == 1 ? String.Empty : "s") : string.Empty);

        if (formatted.EndsWith(", ")) formatted = formatted.Substring(0, formatted.Length - 2);

        if (string.IsNullOrEmpty(formatted)) formatted = "0 seg";

        return formatted;
    }

    public static string ToReadableString1(TimeSpan span)
    {
        string formatted = string.Format("{0}{1}{2}",
            (span.Days / 7) > 0 ? string.Format("{0:0} weeks, ", span.Days / 7) : string.Empty,
            span.Days % 7 > 0 ? string.Format("{0:0} days, ", span.Days % 7) : string.Empty,
            span.Hours > 0 ? string.Format("{0:0} hours, ", span.Hours) : string.Empty);

        if (formatted.EndsWith(", ")) formatted = formatted.Substring(0, formatted.Length - 2);

        return formatted;
    }
}