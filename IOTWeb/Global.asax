﻿<%@ Application Language="C#" %>

<script runat="server">
    
    public override void Init()
    {
        base.Init();
        EndRequest += new EventHandler(global_asax_EndRequest);
    }

    void Application_Start(object sender, EventArgs e)
    {
        // Code that runs on application startup
        System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("es-AR");

        log4net.Config.XmlConfigurator.Configure();

        StructureMap.ObjectFactory.Initialize(x =>
        {
            // ISessionFactory is expensive to initialize, so create it as a singleton.
            x.For<NHibernate.ISessionFactory>()
                .Singleton()
                .Use(Services.GetSessionFactory());

            // Cache each ISession per web request. Remember to dispose this!
            x.For<NHibernate.ISession>()
                .HttpContextScoped()
                .Use(context => context.GetInstance<NHibernate.ISessionFactory>().OpenSession());
        });

        Console.SetOut(new System.IO.StreamWriter(System.IO.Stream.Null));
        Console.SetError(new System.IO.StreamWriter(System.IO.Stream.Null));

        Schedule.Start();
    }

    void global_asax_EndRequest(object sender, EventArgs e)
    {
        StructureMap.ObjectFactory.ReleaseAndDisposeAllHttpScopedObjects();
    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown

    }
        
    void Application_Error(object sender, EventArgs e) 
    { 
        // Code that runs when an unhandled error occurs

    }

    void Session_Start(object sender, EventArgs e) 
    {
        // Code that runs when a new session is started

    }

    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }
       
</script>
