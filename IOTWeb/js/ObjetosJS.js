﻿
/** Clase JS para manipular los "Eventos" de FullCalendar  **/
class Evento {
    constructor() {
        this.id = 0;
        this.title = '';
        this.allDay = false;
        this.start = '';
        this.end = '';
        this.url = '';
        this.className = '';
        this.editable = false;
        /* según la documentación de fullcalendar hay otras propiedades entre 'editable' y 'color'. Pero aquí no las utilizaremos */
        this.color = '';
        this.backgroundColor = '';
        this.borderColor = '';
        this.textColor = '';
        this.error = '';
    }
    clonar(evento) {
        this.id = evento.id;
        this.title = evento.title;
        this.allDay = evento.allDay;
        this.start = evento.start;
        this.end = evento.end;
        this.url = evento.end;
        this.className = evento.end;
        this.editable = evento.editable;
        this.color = evento.color;
        this.backgroundColor = evento.backgroundColor;
        this.borderColor = evento.borderColor;
        this.textColor = evento.textColor;
        this.error = evento.error;
    }
}