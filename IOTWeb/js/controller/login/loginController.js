﻿app.controller('appController', function ($scope, ajax) {
    $scope.UsuarioNombre = "";
    $scope.UsuarioClave = "";
    $scope.ErrorShow = false;
    $scope.onLogin = function () {
        $scope.ErrorShow = false;
        var result = ajax.post("Login", "Login", { Usuario: $scope.UsuarioNombre, Clave: $scope.UsuarioClave },
            function (data) {
                if (data != null) {
                    window.location.href = config.baseURL + "Home";
                }
                else {
                    $scope.ErrorShow = true;
                }
            },
            function (data) {
                $scope.ErrorShow = true;
            });
    }
});