﻿app.controller('appController', function ($scope, NgTableParams, ajax, $uibModal) {

    $scope.roles = [];

    CrudController($scope, NgTableParams, ajax, $uibModal, "Usuario");

    $scope.filterCondition.Nombre = "con";
    $scope.filterCondition.Apellido = "con";
    $scope.filterCondition.NombreUsuario = "con";
    $scope.filterCondition.Email = "con";

    $scope.getRoles = function () {
        ajax.post("Usuario", "GetPerfiles", null, function (data) {
            $scope.roles = data;
        });
    };

    $scope.onAgregarAfter = function () {
        $scope.selecteditem.estado = "A";
        return true;
    };

    $scope.getRoles();

});