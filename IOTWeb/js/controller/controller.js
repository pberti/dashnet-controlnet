﻿function isString(obj) {
    return obj !== undefined && obj != null && obj.toLowerCase !== undefined;
}

function isArray(value) {
    return (!!value) && (value.constructor === Array);
}

function isObject(value) {
    return value instanceof Object || typeof value == "object";
}

function isDate(value) {
    return value instanceof Date && !isNaN(value.valueOf());
}

function isNumber(value) {
    return (typeof value == 'number') && !isNaN(value - 0) && value !== '';
}

function isInt(value) {
    return (typeof value == 'number') && !isNaN(value - 0) && parseInt(value) == value;
}




function round(value, decimals) {
    value = value * 1;
    value = (value === undefined || value == null || isNaN(value)) ? 0 : value
    var result = value * Math.pow(10, decimals);
    var point = result.toString().indexOf('.');
    point = point == -1 ? result.toString().length : point;
    result = (result.toString().substr(0, point) * 1);
    return result / Math.pow(10, decimals);
}

function replaceNaNNull(value, replaceValue) {
    replaceValue = replaceValue === undefined ? 0 : replaceValue;
    if (isNaN(value) || value == null || value == "")
        return replaceValue;
    return value;
}

function replaceIntoArray(array, property, item) {
    var index = inexOfArray(array, property, item[property]);
    if (index >= 0) {
        array[index] = item;
        return true;
    }
    return false;
}

function inexOfArray(array, property, value) {
    if (!isArray(array))
        array = [array];
    for (var i = 0; i < array.length; i++) {
        if (array[i][property] == value)
            return i;
    }
    return -1;
}


function removeFromArray(item, array) {
    var index = array.indexOf(item);
    array.splice(index, 1);
};

function convertToTime(value) {
    var totalSec = value / 1000;
    var hours = parseInt(totalSec / 3600) % 24;
    var minutes = parseInt(totalSec / 60) % 60;
    var seconds = parseInt(totalSec % 60);
    return (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds);
}

function findArray(array, obj) {
    var result = [];
    for (var i = 0; i < array.length; i++) {
        var isOk = true;

        for (var key in obj) {
            if (array[i][key] != obj[key]) {
                isOk = false;
                break;
            }
        }
        if (isOk)
            result.push(array[i]);
    }
    return result;
}






function registerController(moduleName, controllerName) {
    var queue = angular.module(moduleName)._invokeQueue;
    for (var i = 0; i < queue.length; i++) {
        var call = queue[i];
        if (call[0] == "$controllerProvider" &&
           call[1] == "register" &&
           call[2][0] == controllerName) {
            app.register.controller(controllerName, call[2][1]);
        }
    }
}

var app = angular.module('AngularJSApp', ['ui.bootstrap', 'ngSanitize', 'ngTable', 'ui.select']);

app.config(function ($controllerProvider, $compileProvider, $filterProvider, $provide) {
    app.register = {
        controller: $controllerProvider.register,
        directive: $compileProvider.directive,
        filter: $filterProvider.register,
        factory: $provide.factory,
        service: $provide.service
    };
});

app.service('ajax', function ($http) {
    var self = this;

    self.post = function (controller, method, senderObject, returnResult, returnFault) {
        $http.post(config.baseURL + controller + '/' + method, senderObject).then(
            function (response) { //success
                if (returnResult !== undefined && returnResult != null) {
                    if (response.data.isError == false)
                        returnResult(response.data.data);
                    else
                        returnFault(response);
                }
            }, function (response) { //Error
                if (returnFault !== undefined && returnFault != null)
                    returnFault(response);
            });
    };

    self.get = function (controller, method, senderObject, returnResult, returnFault) {
        $http.get(config.baseURL + controller + '/' + method + '/' + senderObject).then(
            function (response) { //success
                if (returnResult !== undefined && returnResult != null) {
                    if (response.data.isError == false)
                        returnResult(response.data.data);
                    else
                        returnFault(response);
                }
            }, function (response) { //Error
                if (returnFault !== undefined && returnFault != null)
                    returnFault(response);
            });
    };

});

app.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter, { 'event': event });
                });

                event.preventDefault();
            }
        });
    };
});

app.directive('isNumber', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            scope.$watch(attrs.ngModel, function (newValue, oldValue) {

                var arr = String(newValue).split("");
                if (arr.length === 0) return;
                if (arr.length === 1 && (arr[0] == '-' || arr[0] === '.')) return;
                if (arr.length === 2 && newValue === '-.') return;
                if (isNaN(newValue)) {
                    var scopeVar = scope;
                    var a = attrs.ngModel.split(".");
                    for (i = 0; i < a.length - 1; i++) {
                        if (scopeVar === undefined || scopeVar == null || newValue == null) {
                            return;
                        }
                        scopeVar = scopeVar[a[i]];
                    }
                    scopeVar[a[a.length - 1]] = oldValue;
                }
            });
        }
    };
});

app.directive('isHexa', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            scope.$watch(attrs.ngModel, function (newValue, oldValue) {
                var ChartsPermit = '0123456789abcdefABCDEF'.split("");
                var arr = String(newValue).split("");
                if (arr.length === 0) return;
                var isFail = false;
                for (var i = 0; i < arr.length; i++) {
                    if (ChartsPermit.indexOf(arr[i]) == -1) {
                        isFail = true;
                        break;
                    }
                }
                if (isFail) {
                    var scopeVar = scope;
                    var a = attrs.ngModel.split(".");
                    for (i = 0; i < a.length - 1; i++) {
                        if (scopeVar === undefined || scopeVar == null || newValue == null) {
                            return;
                        }
                        scopeVar = scopeVar[a[i]];
                    }
                    scopeVar[a[a.length - 1]] = oldValue;
                }
            });
        }
    };
});

app.directive('isText', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            scope.$watch(attrs.ngModel, function (newValue, oldValue) {
                var ChartsPermit = '0123456789-/*+{}[]()\\!?¿¡_.:,;'.split("");
                var arr = String(newValue).split("");
                if (arr.length === 0) return;
                var isFail = false;
                for (var i = 0; i < arr.length; i++) {
                    if (ChartsPermit.indexOf(arr[i]) != -1) {
                        isFail = true;
                        break;
                    }
                }
                if (isFail) {
                    var scopeVar = scope;
                    var a = attrs.ngModel.split(".");
                    for (i = 0; i < a.length - 1; i++) {
                        if (scopeVar === undefined || scopeVar == null || newValue == null) {
                            return;
                        }
                        scopeVar = scopeVar[a[i]];
                    }
                    scopeVar[a[a.length - 1]] = oldValue;
                }
            });
        }
    };
});




function BaseController($scope, $uibModal) {


    $scope.onApply = function () {
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };

    $scope.modalInstance = null;

    $scope.CloseDialog = function () {
        try {
            if ($scope.modalInstance !== null)
                $scope.modalInstance.hide();
        }
        catch (err) {

        }
    };

    function defaultModalController($scope, $uibModalInstance, $sce, data) {
        $scope.data = data;

        $scope.deliberatelyTrustDangerousSnippet = function (htmlText) {
            return $sce.trustAsHtml(htmlText);
        };

        $scope.onOk = function () {
            $uibModalInstance.close($scope.data);
        };

        $scope.onCancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

    $scope.onOpenDialog = function (titulo, mensaje, returnFunction, size, templateUrl, extraData, controller, icono, claseColor) {

        size = (size == undefined || size == null) ? 'lg' : size; //'lg' 'md' 'sm'
        templateUrl = (templateUrl === undefined || templateUrl == null) ? "modalOkCancel.html" : templateUrl; //"modalOkCancel.html" "modalOk.html" "modalYesNo.html"
        returnFunction = (returnFunction == undefined || returnFunction == null) ? function (result) { } : returnFunction;
        controller = (controller == undefined || controller == null) ? defaultModalController : controller;
        claseColorModal = (claseColor == undefined || claseColor == null) ? "" : claseColor;
        aplicarOutline = (claseColorModal != "") ? true : false;
        iconoTituloModal = (icono == undefined || icono == null) ? "" : icono;

        ///registerController('AngularJSApp', controller);

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: templateUrl,
            controller: controller,
            size: size,
            windowClass: claseColorModal,
            resolve: {
                data: function () {
                    return { titulo: titulo, mensaje: mensaje, extraData: extraData, iconoTituloModal: iconoTituloModal, aplicarOutline: aplicarOutline };
                }
            }
        });

        modalInstance.result.then(function (result) {
            returnFunction(result);
        }, function () {
            //$log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.GetEstadoNombre = function (value) {
        switch (value.toLowerCase()) {
            case "a":
                return "ACTIVO";
                break;
            case "b":
                return "BORRADO";
                break;
            case "d":
                return "DESACTIVADO";
                break;
            default:
                return "SIN ESTADO";
                break;
        }
    };



    $scope.onDesasociarUsuarioOK = function () {
        //var items = $scope.usuarioSel.list;
        //var index = -1;
        //for (var i = 0; i < item.length; i++) {

        //    index = -1;
        //    for (var ii = 0; ii < $scope.selecteditem.listaUsuarios.length; ii++) {
        //        index++;
        //        if ($scope.selecteditem.listaUsuarios[ii].idUsuario == items[i]) break;
        //    }
        //    $scope.selecteditem.listaUsuarios.splice(index, 1);
        //}
        //$scope.onApply();
        //$scope.onCancel();
        alert("hola mundo");
    };



    $scope.onValidarCUIT = function (cuit) {
        var suma = 0;
        var arrayMult = [5, 4, 3, 2, 7, 6, 5, 4, 3, 2];
        cuit = cuit.replace(" - ", "");
        cuit = cuit.replace(" - ", "");
        cuit = cuit.replace(".", "");
        cuit = cuit.replace(".", "");

        if (cuit.length < 11) {
            $scope.onOpenDialog("Atención", "Al CUIT ingresado, le faltan dígitos. Recuerde que en total son <strong>11</strong>", null, "md", "modalOk.html", null, null, "fa-info-circle", "modal-danger");
            return false;
        }

        for (var i = 0; i < 10; i++) {
            suma += arrayMult[i] * parseInt(cuit[i]);
        }
        var verif = parseInt(cuit[10]);

        var restoDiv = suma % 11;

        if (11 - restoDiv != verif) {
            $scope.onOpenDialog("Validación Incorrecta", "El número de CUIT ingresado <strong>no es válido.</strong>", null, "sm", "modalOk.html", null, null, "fa-info-circle", "modal-danger");
            return false;
        }       
        
        return true;
    };


};

function CrudController($scope, NgTableParams, ajax, $uibModal, controllerName) {
    $scope.popup = {};
    $scope.dateOptions = {
        startingDay: 1
    };    

    $scope.format = 'dd/MM/yyyy';
    $scope.altInputFormats = ['d!/M!/yyyy'];
    
    BaseController($scope, $uibModal);

    $scope.selecteditem = {};

    $scope.MODO_BUSCAR = "buscar";
    $scope.MODO_EDITAR = "editar";
    $scope.MODO_AGREGAR = "agregar";

    $scope.showButtonAdd = false;
    $scope.showButtonSave = false;
    $scope.showButtonCancel = false;

    $scope.showTableButtonEdit = true;
    $scope.showTableButtonDelete = true;

    $scope.changeMode = function (mode) {
        $scope.modo = mode;

        $scope.showButtonAdd = false;
        $scope.showButtonSave = false;
        $scope.showButtonCancel = false;

        switch ($scope.modo) {
            case $scope.MODO_BUSCAR:
                $scope.showButtonAdd = true;
                break;
            case $scope.MODO_EDITAR:
            case $scope.MODO_AGREGAR:
                $scope.showButtonSave = true;
                $scope.showButtonCancel = true;
                break;
        }
        $scope.onApply();
    };

    $scope.errorObject = {};

    $scope.filterCondition = { Estado: '=' };
    $scope.filterObject = { Estado: 'A' };

    if ($scope.tableDefaultPageCount === undefined)
        $scope.tableDefaultPageCount = 25;

    if ($scope.tableDefaultSorting === undefined)
        $scope.tableDefaultSorting = {};

    if ($scope.tableDefaultFilter === undefined)
        $scope.tableDefaultFilter = {};

    $scope.tableBuscar = new NgTableParams({
        page: 1,                                // show first page
        count: $scope.tableDefaultPageCount,    // count per page
        sorting: $scope.tableDefaultSorting,    // sort
        filter: $scope.tableDefaultFilter       // filter
    }, {
        counts: [],                 // hide page counts control
        total: 0,
        getData: function (params) {
            var result = [];

            var page = new Paging({
                pageIndex: params.page(),
                pageSize: params.count(),
                //pageFilter: params.filterby()
                //pageOrder: params.orderBy()
            });

            page.filter = [];

            for (var key in $scope.filterObject) {
                if ($scope.filterObject[key] > '') { //&& (key != 'Estado' || (key == 'Estado' && $scope.filterObject[key] != 'T'))
                    page.filter.push({ Property: key, Condition: $scope.filterCondition[key], Value: $scope.filterObject[key] })
                }
            }

            page.order = [];
            for (var i = 0; i < params.orderBy().length; i++) {
                page.order.push({ Property: params.orderBy()[i].substring(1), Direction: params.orderBy()[i].substring(0, 1) })
            }


            ajax.post(controllerName, "Buscar", page, function (data) {

                for (var i = 0; i < data.data.length; i++)
                    result.push(data.data[i]);

            }, $scope.onDefaultReturnFault);

            return result;
        }
    });

    $scope.onBuscar = function () {
        $scope.tableBuscar.reload();
    };

    $scope.onAgregar = function () {
        if ($scope.onAgregarBefore()) {
            $scope.changeMode($scope.MODO_AGREGAR);
            $scope.selecteditem = {};
            $scope.onAgregarAfter();
        }
    };

    $scope.onModificar = function (item) {
        if ($scope.onModificarBefore()) {
            $scope.changeMode($scope.MODO_EDITAR);
            $scope.selecteditem = item;
            $scope.onModificarAfter();
        }
    };

    $scope.onBorrar = function (item) {
        if ($scope.onBorrarBefore()) {
            $scope.onOpenDialog("Borrar", "¿Realmente desea Borrar este item?", function () {
                ajax.post(controllerName, "Borrar", item, function (data) {
                    $scope.onCancelar();
                }, $scope.onDefaultReturnFault);
            }, "sm", "modalYesNo.html", null, null, "fa-question-circle-o", "modal-danger");
        }
    };

    $scope.onGuardar = function () {
        if ($scope.onGuardarBefore()) {
            $scope.onOpenDialog("Guardar", "¿Está seguro de Guardar?", function () {
                ajax.post(controllerName, "Guardar", $scope.selecteditem, function (data) {
                    $scope.onCancelar();
                }, $scope.onDefaultReturnFault);
            }, "sm", "modalYesNo.html", null, null, "fa-question-circle-o", "");
        }
    };

    $scope.onCancelar = function () {
        $scope.changeMode($scope.MODO_BUSCAR);
        $scope.onBuscar();
        $scope.errorObject = {};
    };

    $scope.onDefaultReturnFault = function (data) {
        var popup = false;
        if (data.data != null && data.data.isError !== undefined && data.data.isError) {
            if (isArray(data.data.data)) {
                angular.forEach(data.data.data, function (item) {
                    $scope.errorObject[item.memberNames[0]] = item.errorMessage;
                });
                return;
            }
            else if (isString(data.data.data)) {
                popup = true;
                $scope.onOpenDialog("Error", JSON.stringify(data.data.data), null, null, "modalOk.html", null, null, "fa-exclamation", "modal-danger");
            }
            
        }
        if(popup == false) $scope.onOpenDialog("Error", JSON.stringify(data), null, null, "modalOk.html", null, null, "fa-exclamation", "modal-danger");
    };

    $scope.hasError = function (value) {
        if ($scope.errorObject !== undefined)
            return $scope.errorObject[value] > ''
        return false;
    }

    $scope.errorClass = function (value) {
        return $scope.hasError(value) ? 'bad' : '';
    }

    $scope.errorRemove = function (value) {
        if ($scope.errorObject !== undefined)
            $scope.errorObject[value] = '';
    }

    $scope.errorMessage = function (value) {
        if ($scope.errorObject !== undefined)
            return $scope.errorObject[value];
        return '';
    }


    $scope.onAgregarBefore = function () { return true; };
    $scope.onAgregarAfter = function () { return true; };
    $scope.onModificarBefore = function () { return true; };
    $scope.onModificarAfter = function () { return true; };
    $scope.onGuardarBefore = function () { return true; };
    $scope.onGuardarAfter = function () { return true; };
    $scope.onBorrarBefore = function () { return true; };
    $scope.onBorrarAfter = function () { return true; };

    $scope.changeMode($scope.MODO_BUSCAR);

    setTimeout(function () {
        $(".content-wrapper").removeClass("hidden");
    }, 1000);

}

var Paging = function (value) {
    var self = this;
    value = (value == null || value === undefined) ? {} : value;

    self.pageIndex = value.pageIndex === undefined ? null : value.pageIndex;
    self.pageSize = value.pageSize === undefined ? null : value.pageSize;
    self.pageCount = value.pageCount === undefined ? null : value.pageCount;
    self.recordCount = value.recordCount === undefined ? null : value.recordCount;
    self.filter = value.filter === undefined ? null : value.filter;
    self.order = value.order === undefined ? null : value.order;
    self.data = value.data === undefined ? null : value.data;
};

function isArray(value) {
    return (!!value) && (value.constructor === Array);
}