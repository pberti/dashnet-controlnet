﻿if ($("#sidebar-menu").length === 1) {
    app.controller('mainSidebarController', function ($scope, ajax, $sce, $uibModal) {

        BaseController($scope, $uibModal);

        $scope.deliberatelyTrustDangerousSnippet = function (htmlText) {
            return $sce.trustAsHtml(htmlText);
        };

        $scope.menues = [];
        ajax.post("Menu", "Buscar", null, function (data) {
            $scope.menues = data;
            $scope.onApply();
        });

        $scope.getPath = function (value) {
            return config.baseURL + value;
        }

        setTimeout(function () {
            $("#sidebar-menu").removeClass("hidden");
        }, 1000);

    });
}
