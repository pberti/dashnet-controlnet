﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IOT.ORM;
using IOT.Domain;
using System.Data;
using IOT.Services;
using System.Text;
using System.IO;
using System.Web.UI.HtmlControls;
using SelectPdf;

public partial class Application_Ots_Default : PageBase
{
    private ServTipoPerfilMenues oServPerfilMenus = Services.Get<ServTipoPerfilMenues>();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (base.IdUsuarioLogueado == -1)
        {
            Response.Redirect("~/Application/Login/Default.aspx", false);
            return;
        }
        if (!Page.IsPostBack)
            InicializacionPantalla();
    }

    private void InicializacionPantalla()
    {
        CargoPermisosdePantalla();
        chkProducto.Checked = false;
        chkCC.Checked = false;
        chkProyectoPadre.Checked = false;
        CargoComboFiltro();
        List<OTs> lstOTs = Services.Get<ServOTs>().Indicador_VersionesActivas(CargarFiltro());
        if (lstOTs.Count > 0)
        {
            CargoGrilla();
            CalculoIndicadores(lstOTs);
        }
    }


    private void CargoPermisosdePantalla()
    {
        var tipoPerfilMenu = oServPerfilMenus.TraerTodosTipoPefilMenuesPorPerfil(IdPerfilUsuarioLogueado).Where(p => p.oMenu.PathPagina.Contains("/VersionesActivas"));
        if (tipoPerfilMenu.Count() != 0)
        {
            List<TipoPerfilMenues> t = tipoPerfilMenu.ToList();
        }
        else
            Response.Redirect("~/Application/Default.aspx", false);

    }
    #region Eventos del Filtro
    private void CargoComboFiltro()
    {

        //Cargo ComboProyectos
        ddlFiltroProyecto.Items.Clear();
        List<string> lstProyectos = Services.Get<ServOTs>().TraerProyectos();
        ListItem mItem = new ListItem("TODOS", "1");
        ddlFiltroProyecto.Items.Add(mItem);
        int mValue = 2;
        foreach (var item in lstProyectos)
        {
            mItem = new ListItem(item.ToUpper().ToString(), mValue.ToString());
            ddlFiltroProyecto.Items.Add(mItem);
            mValue++;
        }
        ddlFiltroProyecto.DataBind();

        //Cargo ComboCentros de Costo
        ddlFiltroCC.Items.Clear();
        List<string> lstCC = Services.Get<ServOTs>().TraerCC();
        ListItem mItem2 = new ListItem("TODOS", "1");
        ddlFiltroCC.Items.Add(mItem2);
        mValue = 2;
        foreach (var item in lstCC)
        {

            mItem = new ListItem(item.ToUpper().ToString(), mValue.ToString());
            ddlFiltroCC.Items.Add(mItem);
            mValue++;
        }
        ddlFiltroCC.DataBind();

    }

    private string CargarFiltro() {

        string mFiltro = "";
        if (ddlFiltroProyecto.SelectedItem.Text.ToString() != "TODOS")
        {
            mFiltro = " AND PROYECTO_NOMBRE = '" + ddlFiltroProyecto.SelectedItem.ToString() + "'";
        }
        //ddlFiltroProyecto.ClearSelection();
        if (ddlFiltroCC.SelectedItem.Text.ToString() != "TODOS")
        {
            mFiltro = mFiltro + " AND PROYECTO_CC = '" + ddlFiltroCC.SelectedItem.ToString() + "'";
        }
        return mFiltro;
    }

    #endregion

    #region BTNBuscar
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        // Verifica que campos del filtro contienen datos para realizar el filtro.
        try
        {
            //LImpio todo
            lblIndicador1.Text = "";
            lblIndicador2.Text = "";
            lblIndicador3.Text = "";
            // Limpio Agrupadores
            LimpioAgrupadores();
            string mFiltro = CargarFiltro();
            List<OTs> lstOTs = Services.Get<ServOTs>().Indicador_VersionesActivas(mFiltro);
            if (lstOTs.Count > 0)
            {
                CargoGrilla();
                CalculoIndicadores(lstOTs);
            }
        }
        catch (Exception ex)
        {
            //errorMensaje.ErrorBind(ex, "btnBuscar_Click", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    #endregion Eventos del Filtro

    #region "Eventos Grilla"

    private void CargoGrilla()
    {
        lblIndicador1.Text = "SD";
        lblIndicador2.Text = "SD";
        lblIndicador3.Text = "SD";
        bool mProducto = chkProducto.Checked;
        bool mCC = chkCC.Checked;
        bool mProyectoPadre = chkProyectoPadre.Checked;
        bool mProyecto = chkProyecto.Checked;
        bool mVersion = chkVersion.Checked;
        //Oculto columnas
        if (mProducto)
            this.grdGrilla.Columns[1].Visible = true;
        else
            this.grdGrilla.Columns[1].Visible = false;
        if (mCC)
            this.grdGrilla.Columns[2].Visible = true;
        else
            this.grdGrilla.Columns[2].Visible = false;
        if (mProyectoPadre)
            this.grdGrilla.Columns[3].Visible = true;
        else
            this.grdGrilla.Columns[3].Visible = false;
        if (mProyecto)
            this.grdGrilla.Columns[4].Visible = true;
        else
            this.grdGrilla.Columns[4].Visible = false;
        if (mVersion)
        {
            this.grdGrilla.Columns[5].Visible = true;
            this.grdGrilla.Columns[6].Visible = false;
        }
        else
        {
            this.grdGrilla.Columns[5].Visible = false;
            this.grdGrilla.Columns[6].Visible = true;
        }

        List<OTs> lstOTs = Services.Get<ServOTs>().Indicador_VersionesActivas(CargarFiltro());
        if (lstOTs.Count > 0)
        {
            grdGrilla.DataSource = lstOTs;
            lblCantidadFilas.Text = lstOTs.Count().ToString();
            grdGrilla.DataBind();
        }
    }

    private void CalculoIndicadores(List<OTs> plstOTs)
    {
        string specifier = "#,0.0#;(#,0.0#)";
        //Calculo de los Indicadores
        //Indicador 1 = Procentaje de FEcha de entregas cumplidas
        double mCantEntregas = Convert.ToDouble(plstOTs.Count());
        double mCantEntregas_ok = Convert.ToDouble(plstOTs.Where(p => p.Version_Cerrada_Ok == true).Count());
        double mOperacion = ((mCantEntregas_ok / mCantEntregas) * 100);
        lblIndicador1.Text = mOperacion.ToString(specifier);
        if (lblIndicador1.Text == "NaN" || lblIndicador1.Text == "NuN")
            lblIndicador1.Text = "SD";

        //Indicador 2 Entregas Aceptadas
        double mCant1 = Convert.ToDouble(plstOTs.Count(p => p.Version_AprobCliente == "SI"));
        double mCant2 = Convert.ToDouble(plstOTs.Count(p => p.Version_AprobCliente != "PENDIENTE"));  //Total de las Entregas que no son Pendientes
        mOperacion = ((mCant1 / mCant2) * 100);
        lblIndicador2.Text = mOperacion.ToString(specifier);
        if (lblIndicador2.Text == "NaN" || lblIndicador2.Text == "NuN")
            lblIndicador2.Text = "SD";


        //Indicador 3 - Tiempo Consumido
        double mAcumEstimadas = 0;
        double mAcumUsadas = 0;
        long mOT_Cant_Cerradas = 0;
        long mOT_Cant_Abiertas = 0;
        long mOT_Cant_Pendientes = 0;
        long mOT_Cant_Progreso = 0;
        long mOT_Cant_Finalizadas = 0;
        long mOT_Cant_Total = 0;
        foreach (var item in plstOTs)
        {
            mAcumEstimadas = mAcumEstimadas + Convert.ToDouble(item.OT_Horas_Estimadas_Total);
            mAcumUsadas = mAcumUsadas + Convert.ToDouble(item.OT_Horas_Usadas_Total);


            mOT_Cant_Cerradas = mOT_Cant_Cerradas + item.OT_Cant_Cerradas;
            mOT_Cant_Pendientes = mOT_Cant_Pendientes + item.OT_Cant_Pendientes;
            mOT_Cant_Progreso = mOT_Cant_Progreso + item.OT_Cant_Progreso;
            mOT_Cant_Finalizadas = mOT_Cant_Finalizadas + item.OT_Cant_Finalizadas;
        }
        mOperacion = ((mAcumUsadas / mAcumEstimadas) * 100);
        lblIndicador3.Text = mOperacion.ToString(specifier);
        if (lblIndicador3.Text == "NaN" || lblIndicador3.Text == "NuN")
            lblIndicador3.Text = "SD";

        //Cargo el Agrupador de las horas que ya lo tengo aca
        lblInd_Horas_Total.Text = mAcumUsadas.ToString(specifier);
        lblInd_Hrs_Estimadas.Text = mAcumEstimadas.ToString(specifier);

        // Cargo los Agrupadores de OTs
        mOT_Cant_Total = mOT_Cant_Cerradas + mOT_Cant_Abiertas + mOT_Cant_Pendientes + mOT_Cant_Progreso + mOT_Cant_Finalizadas;
        string total = mOT_Cant_Total.ToString();
        string Prog = mOT_Cant_Progreso.ToString();
        string Pend = mOT_Cant_Pendientes.ToString();
        string Cump = mOT_Cant_Finalizadas.ToString();
        string Cerr = mOT_Cant_Cerradas.ToString();

        lblInd_OTs_Total.Text = total;


        //Cantidad de Proyectos
        lblInd_Proyectos_Total.Text = plstOTs.GroupBy(p => p.Proyecto_Nombre).Count().ToString();

        //Cantidad de Versiones
        lblInd_Ver_Total.Text = plstOTs.GroupBy(p => p.Version_Id).Count().ToString();
        string mVersionesCerradasOK = "0";
        string mVersionesCerradasNoOK = plstOTs.Where(p => Convert.ToDateTime(p.Version_FechaEstFinal) < DateTime.Now).Count().ToString();
        string mVersionesAbiertas = plstOTs.Where(p => Convert.ToDateTime(p.Version_FechaEstFinal) >= DateTime.Now).Count().ToString();

        List<string> listVersionesTotales = new List<string>();
        listVersionesTotales.Add(mVersionesCerradasNoOK);
        listVersionesTotales.Add(mVersionesCerradasOK);
        listVersionesTotales.Add(mVersionesAbiertas);

        List<string> listVersionesTipos = new List<string>();
        listVersionesTipos.Add("Vencidas");
        listVersionesTipos.Add("Cerradas");
        listVersionesTipos.Add("Abiertas");

        List<string> listOTTotales = new List<string>();
        listOTTotales.Add(Prog);
        listOTTotales.Add(Pend);
        listOTTotales.Add(Cump);
        listOTTotales.Add(Cerr);

        List<string> listOTTipos = new List<string>();
        listOTTipos.Add("Programadas");
        listOTTipos.Add("Pendientes");
        listOTTipos.Add("Cumplimentadas");
        listOTTipos.Add("Cerradas");

        string jsonA = ObtenerObjChartDoughnutRecType(listOTTipos, listOTTotales);
        string jsonB = ObtenerObjChartDoughnutRecType(listVersionesTipos, listVersionesTotales);

        string datos = "['" + Convert.ToInt16(mAcumUsadas).ToString() + "','" + Convert.ToInt16(mAcumEstimadas).ToString() + "']";
        string backcolor = "['" + ValoresConstantes.listaColores[7] + "','" + ValoresConstantes.listaColores[8] + "']"; ;
        string hoverBackgroundColor = "['" + ValoresConstantes.listaColores[0] + "','" + ValoresConstantes.listaColores[3] + "']"; ;

        string objbarchar = ObtenerJsonBarChar(datos, backcolor, hoverBackgroundColor);

        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "",
       "renderizarCharts(" + jsonA + "," + jsonB + "," + objbarchar + ");", true);

    }

    private string ObtenerJsonBarChar(string datos, string backcolor, string hoverBackgroundColor)
    {
        string Json = string.Empty;
        try
        {
             Json = "{ " +
                    "type: 'horizontalBar'," +
                    "data: " +
                        "{" +
                           " labels:['Dedicadas','Estimadas']," +
                                "datasets: [{" +
                                "label: 'Cantidad'," +
                                    "data: " + datos + "," +
                                    "backgroundColor: " + backcolor + "," +
                                    "hoverBackgroundColor:" + hoverBackgroundColor +
                                "}]" +
                            "}," +
                               " options: { scales: " +
             " { xAxes: [{ ticks: { " +
                          " beginAtZero: 'true' " +
              "  } " +
               "     }]," +
           " yAxes: [{  stacked: true " + 
           " }] } " +
                "}}";
        }

        catch (Exception ex)
        {

        }

        return Json;
    }

    private void LimpioAgrupadores()
    {

        lblInd_Ver_Total.Text = "";
        lblInd_Proyectos_Total.Text = "";
        lblInd_OTs_Total.Text = "";
        lblInd_Horas_Total.Text = "";
        lblInd_Hrs_Estimadas.Text = "";

    }
    #endregion

    #region BtnActualizar
    protected void btnActualizar_Click(object sender, EventArgs e)
    {
        // Verifica que campos del filtro contienen datos para realizar el filtro.
        try
        {
            Services.Get<ServOTs>().ActualizacionDatosRedmine();
        }
        catch (Exception)
        {

            throw;
        }
    }
    #endregion

    #region Boton Excel
    protected void btnExportar_Click(object sender, EventArgs e)
    {
        ExportarExcel();
    }
    private void ExportarExcel()
    {
        StringBuilder sb = new StringBuilder();
        StringWriter sw = new StringWriter(sb);
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        Page page = new Page();
        HtmlForm form = new HtmlForm();

        GridView grilla = new GridView();
        grilla.EnableViewState = false;
        grilla.AllowPaging = false;
        grilla.DataSource = CargoGrillaExcel();
        grilla.DataBind();

        page.EnableEventValidation = false;
        page.DesignerInitialize();
        page.Controls.Add(form);
        form.Controls.Add(grilla);
        page.RenderControl(htw);
        Response.Clear();
        Response.Buffer = true;
        Response.ContentType = "application/ms-excel";
        Response.AddHeader("Content-Disposition", "attachment;filename=Metricas_Version_Activas.xls");
        Response.Charset = "UTF-8";
        Response.ContentEncoding = Encoding.Default;
        Response.Write(sb.ToString());
        Response.End();

    }
    private List<OTs_Excel> CargoGrillaExcel()
    {
        string mFiltro = "";
        if (ddlFiltroProyecto.SelectedItem.Text.ToString() != "TODOS")
        {
            mFiltro = " AND PROYECTO_NOMBRE = '" + ddlFiltroProyecto.SelectedItem.ToString() + "'";
        }
        //ddlFiltroProyecto.ClearSelection();
        if (ddlFiltroCC.SelectedItem.Text.ToString() != "TODOS")
        {
            mFiltro = mFiltro + " AND PROYECTO_CC = '" + ddlFiltroCC.SelectedItem.ToString() + "'";
        }
        List<OTs> lstOTs = Services.Get<ServOTs>().Indicador_VersionesActivas(mFiltro);
        List<OTs_Excel> lstOTsExcel = new List<OTs_Excel>();
        foreach (var item in lstOTs)
        {
            OTs_Excel oOT = new OTs_Excel();
            oOT.OT_Cant_Abiertas = item.OT_Cant_Abiertas.ToString();
            oOT.OT_Cant_AprobCliente = item.OT_Cant_AprobCliente.ToString();
            oOT.OT_Cant_Cerradas = item.OT_Cant_Cerradas.ToString();
            oOT.OT_Cant_Finalizadas = item.OT_Cant_Finalizadas.ToString();
            oOT.OT_Cant_Pendientes = item.OT_Cant_Pendientes.ToString();
            oOT.OT_Cant_Progreso = item.OT_Cant_Progreso.ToString();
            oOT.OT_Cant_Total = item.OT_Cant_Total.ToString();

            oOT.OT_Horas_Estimadas_Total = item.OT_Horas_Estimadas_Total.ToString();
            oOT.OT_Horas_Porc_Consumido = item.OT_Horas_Porc_Consumido.ToString();
            oOT.OT_Horas_Usadas_Total = item.OT_Horas_Usadas_Total.ToString();
            oOT.OT_Porc_Cerradas = item.OT_Porc_Cerradas.ToString();
            oOT.Proyecto_Nombre = item.Proyecto_Nombre;
            oOT.Proyecto_CC = item.Proyecto_CC;
            oOT.Proyecto_Padre_Nombre = item.Proyecto_Padre_Nombre;
            oOT.Version_Nombre = item.Version_Nombre;
            oOT.Version_Fecha_Inicio = item.Version_Fecha_Inicio.ToShortDateString();
            oOT.Version_Fecha_Est_FiN = item.Version_Fecha_Est_FiN.ToShortDateString();
            oOT.Version_Fecha_Real_FiN = item.Version_Fecha_Real_FiN.ToShortDateString();
            oOT.Version_Producto = item.Version_Producto;
            oOT.Version_AMS = item.Version_AMS;
            oOT.Version_Cantidad = item.Version_Cantidad.ToString();
            oOT.Version_FechaEstFinal = item.Version_FechaEstFinal.ToString();
            oOT.Version_FechaFinal = item.Version_FechaFinal.ToString();
            oOT.Version_AprobCliente = item.Version_AprobCliente.ToString();
            lstOTsExcel.Add(oOT);
        }
        return lstOTsExcel;
    }
    #endregion

    #region Botón PDF
    protected void btnExportarPDF_Click(object sender, EventArgs e)
    {
        string baseUrl = "http://" + Request.Url.Authority + Request.Url.Segments[0] + Request.Url.Segments[1];
        string url = baseUrl + "VersionesActivas/pdf/Default.aspx";

        Session["ddlFiltroProyecto"] = ddlFiltroProyecto.SelectedItem.Text;
        Session["ddlFiltroCC"] = ddlFiltroCC.SelectedItem.Text;

        Response.Redirect(url, "_blank", null);
    }
    #endregion

    #region Graficos
    private string ObtenerObjChartDoughnutRecType(List<string> listTipos, List<string> listTotales)
    {
        string JSObj = string.Empty;
        try
        {
            //Tipos a graficar
            string types = "['";
            //Cantidades a graficar
            string cantidades = "['";
            // Colores para graficar. Separamos por '.' porque desde JavaScript, al aplicar 'split()' no podremos separar por ',' 
            // debido a que obtendríamos un resultado incorrectos dado que el formato de una cadena de valores rgba se separan también por ','. 
            string colores = "['";

            for (int i = 0; i < listTipos.Count - 1; i++)
            {
                types = types + listTipos[i] + " - ";
                cantidades = cantidades + listTotales[i] + "','";
                types = types + listTotales[i] + "','";
                colores = colores + ValoresConstantes.listaColores[i] + "','";
            }

            types = types + listTipos.Last() + " - " + listTotales.Last() + "']";
            cantidades = cantidades + listTotales.Last() + "']";
            colores = colores + ValoresConstantes.listaColores[listTipos.Count] + "']";

            // por el momento realizaremos el obj JS como un string de c#.
            // pero lo correcto sería crear objetos para encapsular los charts deseados y luego exportar a JS.
            JSObj = "{ " +
                "type: 'doughnut'," +
                "data: " +
                    "{" +
                       " labels: " + types + "," +
                            "datasets: [{" +
                            "label: 'Cantidad'," +
                                "data: " + cantidades + "," +
                                "backgroundColor: " + colores +
                            "}]" +
                        "}," +
                        "options:" +
                    "{" +
                       " animation:" +
                        "{" +
                           " animateRotate: true" +
                            "} " +
                    "}" +
                "}";
        }
        catch (Exception ex)
        {

        }
        return JSObj;
    }

    protected void lbtnBuscar_Click(object sender, EventArgs e)
    {
        string mFiltro = CargarFiltro();
        List<OTs> lstOTs = Services.Get<ServOTs>().Indicador_VersionesActivas(mFiltro);
        if (lstOTs.Count > 0)
        {
            CalculoIndicadores(lstOTs);
        }
    }

    protected void btnDetalles_Click1(object sender, EventArgs e)
    {
        if (pnlGeneral.Visible)
        {
            pnlDetalleGrilla.Visible = true;
            pnlGeneral.Visible = false;
            btnDetalles.Text = "Volver";
        }
        else
        {
            pnlDetalleGrilla.Visible = false;
            pnlGeneral.Visible = true;
            btnDetalles.Text = "Detalles";
        }
    }
    #endregion
}