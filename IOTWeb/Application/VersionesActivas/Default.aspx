﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Application_Ots_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script src='<%= Page.ResolveUrl("~/js/html2canvas.js") %>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("~/js/vfs_fonts.js") %>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("~/js/pdfmake.min.js") %>' type="text/javascript"></script>

    <asp:Panel ID="pnlGrilla" runat="server">
        <div class="">
            <div class="page-title">
                <div>
                </div>
                <div class="title_right">
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div>
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/img/Logo_Cnet_2175x457.png" Width="30%" />
                        </div>
                        <br />
                        <div class="x_title">
                            <h2>Metrica de Versiones Acivas</h2>
                            <div class="clearfix"></div>
                        </div>
                        <%--FILTRO--%>
                        <div class="x_content">
                            <div class="x_panel hidden-xs">
                                <div class="form-horizontal form-label-left  hidden-xs">
                                    <div class="col-md-12 col-sm-12  ">
                                        <div class="form-group">
                                            <asp:Label ID="Label3" runat="server" Text="Proyecto" class="control-label col-md-1 col-sm-1 col-xs-12" ToolTip="Se tomaran todas las versiones cuya fecha de inicio este comprendida en este periodo"></asp:Label>
                                            <div class="col-md-2 col-sm-2 col-xs-2">
                                                <asp:DropDownList ID="ddlFiltroProyecto" runat="server" CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                            <asp:Label ID="Label5" runat="server" Text="CC" class="control-label col-md-1 col-sm-1 col-xs-12" ToolTip="Se tomaran todas las versiones cuya fecha de inicio este comprendida en este periodo"></asp:Label>
                                            <div class="col-md-2 col-sm-2 col-xs-2">
                                                <asp:DropDownList ID="ddlFiltroCC" runat="server" CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="form-group" runat="server" id="pnlChecks" visible="false">
                                            <asp:Label ID="Label1" runat="server" Text="Columnas " class="control-label col-md-1 col-sm-1 col-xs-12" ToolTip="Se tomaran todas las versiones cuya fecha de inicio este comprendida en este periodo"></asp:Label>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-2 col-sm-2 col-xs-2">
                                                <asp:CheckBox ID="chkProducto" runat="server" class="form-control" Checked="false" TextAlign="Left" Text="Producto: " />
                                            </div>
                                            <div class="col-md-3 col-sm-3 col-xs-3">
                                                <asp:CheckBox ID="chkCC" runat="server" class="form-control" Checked="false" TextAlign="Left" Text="Centro de Costo: " />
                                            </div>
                                            <div class="col-md-3 col-sm-3 col-xs-3">
                                                <asp:CheckBox ID="chkProyectoPadre" runat="server" class="form-control" Checked="false" TextAlign="Left" Text="Proyecto Padre: " />
                                            </div>
                                            <div class="col-md-2 col-sm-2 col-xs-12">
                                                <asp:CheckBox ID="chkProyecto" runat="server" class="form-control" Checked="true" TextAlign="Left" Text="Proyecto: " />
                                            </div>
                                            <div class="col-md-2 col-sm-2 col-xs-12">
                                                <asp:CheckBox ID="chkVersion" runat="server" class="form-control" Checked="true" TextAlign="Left" Text="Version: " />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <asp:LinkButton ID="btnBuscar" runat="server" CssClass="btn btn-round  btn-info pull-right hidden-xs" OnClick="btnBuscar_Click">
                                    <i class="fa fa-search"></i> Buscar
                                </asp:LinkButton>
                                <asp:LinkButton ID="btnDetalles" runat="server" CssClass="btn btn-round  btn-info pull-right hidden-xs" OnClick="btnDetalles_Click1">
                                     Detalles
                                </asp:LinkButton>
                                <br>
                                <button type="submit" id="btnActualizar" runat="server" causesvalidation="False" class="btn btn-round btn-danger pull-right hidden-xs" onserverclick="btnActualizar_Click">
                                    <i class="fa fa-download"></i>Actualizar
                                </button>
                                <asp:LinkButton ID="btnExportar" runat="server" CssClass="btn btn-round  btn-info pull-right hidden-xs" OnClick="btnExportar_Click">
                                    <i class="fa fa-file-excel-o"></i> Exportar a Excel
                                </asp:LinkButton>
                                <asp:LinkButton ID="btnExportarPDF" runat="server" CssClass="btn btn-round  btn-info pull-right hidden-xs" OnClick="btnExportarPDF_Click">
                                    <i class="fa fa-file-excel-o"></i> Exportar a PDF
                                </asp:LinkButton>
                            </div>

                            <div class="x_panel" runat="server" id="pnlGeneral">
                                <%--INDICADORES--%>
                                <div class="panel" runat="server" id="pnlIndicadores">
                                    <div class="x_title">
                                        <h2>Indicadores</h2>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="row tile_count">
                                        <div class="col-md-4 col-sm-4 col-xs-4 tile_stats_count">
                                            <span class="count_top">% Cump. Fecha Entrega</span>
                                            <div class="count">
                                                <asp:Label runat="server" ID="lblIndicador1" class="count green"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-4 tile_stats_count">
                                            <span class="count_top">% Entregas Aceptadas</span>
                                            <div class="count">
                                                <asp:Label runat="server" ID="lblIndicador2" class="count green"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-4 tile_stats_count">
                                            <span class="count_top">% Tiempo Consumido</span>
                                            <div class="count">
                                                <asp:Label runat="server" ID="lblIndicador3" class="count green"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <%--SUBINDICADORES--%>
                                <div class="panel" runat="server" id="pnlAgrupadores">
                                    <div class="x_title">
                                        <h2>Agrupadores</h2>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="row tile_count">
                                        <div class="col-md-3 col-sm-3 col-xs-3 tile_stats_count">
                                            <span class="count_top"><i class="fa fa-cubes"></i>Cantidad de Proyectos</span>
                                            <div class="count">
                                                <asp:Label runat="server" ID="lblInd_Proyectos_Total" class="count green"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-3 col-xs-3 tile_stats_count">
                                            <span class="count_top"><i class="fa fa-code-fork"></i>Cantidad de Versiones</span>
                                            <div class="count">
                                                <asp:Label runat="server" ID="lblInd_Ver_Total" class="count green"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-3 col-xs-3 tile_stats_count">
                                            <span class="count_top"><i class="fa fa-file-code-o"></i>Ordenes de Trabajo</span>
                                            <div class="count">
                                                <asp:Label runat="server" ID="lblInd_OTs_Total" class="count green"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-3 col-xs-3 tile_stats_count">
                                            <span class="count_top"><i class="fa fa-user"></i>Horas</span>
                                            <div class="count green">
                                                <asp:Label runat="server" ID="lblInd_Horas_Total" class="count green"></asp:Label>
                                            </div>
                                            <span class="count_bottom"><i class="green">
                                                <asp:Label runat="server" ID="lblInd_Hrs_Estimadas"></asp:Label></i> Estimadas</span>
                                        </div>
                                    </div>
                                </div>

                                <%--Graficos--%>
                                <div class="form-group" runat="server" id="pnlGráficos">
                                    <div class="panel col-md-12 col-sm-12 col-xs-12" id="pnlBarra">
                                        <div class="x_title">
                                            <h2>Horas Estidmadas y Dedicadas</h2>
                                            <div class="clearfix"></div>
                                        </div>
                                        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Panel runat="server" class="col-md-12 col-sm-12 col-xs-12" ID="Panel3">
                                                    <asp:LinkButton runat="server" ID="lbtnBarra" CssClass="btn btn-round btn-alert hidden" OnClick="lbtnBuscar_Click">
                                                              Buscar </asp:LinkButton>
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <canvas id="barchar" height="50"></canvas>
                                                    </div>
                                                </asp:Panel>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div class="panel col-md-6 col-sm-6 col-xs-6" id="pnlGraficoB">
                                        <div class="x_title">
                                            <h2>Cantidad de versiones</h2>
                                            <div class="clearfix"></div>
                                        </div>
                                        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Panel runat="server" class="col-md-12 col-sm-12 col-xs-12" ID="Panel2">
                                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                                        <canvas id="chartDoughnutB"></canvas>
                                                    </div>
                                                    <div id="divDoughnutLegendsB" class="col-md-5 col-sm-5 col-xs-5">
                                                    </div>
                                                </asp:Panel>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div class="panel col-md-6 col-sm-6 col-xs-6" id="pnlGraficoA">
                                        <div class="x_title">
                                            <h2>Ordenes de trabajo</h2>
                                            <div class="clearfix"></div>
                                        </div>
                                        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Panel runat="server" class="col-md-12 col-sm-12 col-xs-12" ID="Panel1">
                                                    <asp:LinkButton runat="server" ID="lbtnBuscar" CssClass="btn btn-round btn-alert hidden" OnClick="lbtnBuscar_Click">
                                                              Buscar </asp:LinkButton>
                                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                                        <canvas id="chartDoughnutA"></canvas>
                                                    </div>
                                                    <div id="divDoughnutLegendsA" class="col-md-5 col-sm-5 col-xs-5">
                                                    </div>
                                                </asp:Panel>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                            <%--GRILLA--%>
                            <div class="x_panel col-md-12 col-sm-12 col-xs-12" visible="false" runat="server" id="pnlDetalleGrilla">
                                <h3>Datos</h3>
                                <div style="overflow-x: scroll">
                                    <asp:GridView ID="grdGrilla" runat="server"
                                        AllowSorting="false"
                                        AutoGenerateColumns="False" CssClass="table table-striped table-bordered">
                                        <Columns>

                                            <%-- Pos:0 --%><asp:BoundField DataField="Id" HeaderText="ID" SortExpression="Id" Visible="false" />
                                            <%-- Pos:1 --%><asp:BoundField DataField="Version_Producto" HeaderText="Producto" SortExpression="Version_Producto" />
                                            <%-- Pos:2 --%><asp:BoundField DataField="Proyecto_CC" HeaderText="CC" SortExpression="Proyecto_CC" HeaderStyle-Font-Bold="true" />
                                            <%-- Pos:3 --%><asp:BoundField DataField="Proyecto_Padre_Nombre" HeaderText="Proy Padre" SortExpression="Proyecto_Padre_Nombre" HeaderStyle-Font-Bold="true" />
                                            <%-- Pos:4 --%><asp:BoundField DataField="Proyecto_Nombre" HeaderText="Proyecto" SortExpression="Proyecto_Nombre" HeaderStyle-Font-Bold="true" />
                                            <%-- Pos:5 --%><asp:BoundField DataField="Version_Nombre" HeaderText="Version" SortExpression="Version_Nombre" HeaderStyle-Font-Bold="true" />
                                            <%-- Pos:6 --%><asp:BoundField DataField="Version_Cantidad" HeaderText="Versiones" SortExpression="Version_Cantidad" HeaderStyle-Font-Bold="true" />
                                            <%-- Pos:7 --%><asp:BoundField DataField="Version_FechaInicio" HeaderText="F. Inicio" SortExpression="Version_Fecha_Inicio" HeaderStyle-Font-Bold="true" />
                                            <%-- Pos:8 --%><asp:BoundField DataField="Version_FechaEstFinal" HeaderText="F. Acordada" SortExpression="Version_Fecha_Est_FiN" HeaderStyle-Font-Bold="true" />
                                            <%-- Pos:9 --%><asp:BoundField DataField="OT_Horas_Porc_Consumido" HeaderText="% Cons" SortExpression="OT_Horas_Porc_Consumido" HeaderStyle-Font-Bold="true" />
                                            <%-- Pos:10 --%><asp:BoundField DataField="OT_Horas_Usadas_Total" HeaderText="Hrs. Ded" SortExpression="OT_Horas_Usadas_Total" HeaderStyle-Font-Bold="true" />
                                            <%-- Pos:11 --%><asp:BoundField DataField="OT_Horas_Estimadas_Total" HeaderText="Hrs. Est" SortExpression="OT_Horas_Estimadas_Total" HeaderStyle-Font-Bold="true" />
                                            <%-- Pos:12 --%><asp:BoundField DataField="OT_Cant_Progreso" HeaderText="OT Prog" SortExpression="OT_Cant_Progreso" HeaderStyle-Font-Bold="true" />
                                            <%-- Pos:13 --%><asp:BoundField DataField="OT_Cant_Pendientes" HeaderText="OT Pend" SortExpression="OT_Cant_Pendientes" HeaderStyle-Font-Bold="true" />
                                            <%-- Pos:14 --%><asp:BoundField DataField="OT_Cant_Finalizadas" HeaderText="OT Cump" SortExpression="OT_Cant_Finalizadas" HeaderStyle-Font-Bold="true" />
                                            <%-- Pos:15 --%><asp:BoundField DataField="OT_Cant_Cerradas" HeaderText="Ots Cerr" SortExpression="OT_Cant_Cerradas" HeaderStyle-Font-Bold="true" />
                                            <%-- Pos:16 --%><asp:BoundField DataField="OT_Cant_Total" HeaderText="OT Total" SortExpression="OT_Cant_Total" HeaderStyle-Font-Bold="true" />
                                            <%-- Pos:17 --%><asp:BoundField DataField="_Version_AprobCliente" HeaderText="A Cliente" SortExpression="Version_AprobCliente" HeaderStyle-Font-Bold="true" />
                                            <%-- Pos:18 --%><asp:BoundField DataField="Version_AMS" HeaderText="AMS" SortExpression="Version_AMS" HeaderStyle-Font-Bold="true" />
                                            <%-- Pos:19 --%><asp:BoundField DataField="OT_Porc_Cerradas" HeaderText="% D OT Cerradas" SortExpression="OT_Porc_Cerradas" HeaderStyle-Font-Bold="true" />
                                        </Columns>
                                        <PagerStyle CssClass="pgr" />
                                    </asp:GridView>
                                </div>
                                <br />
                                <div class="alert alert-success alert-dismissible fade in" role="alert">
                                    <div class="pull-left">
                                        Total de Registros:
                                        <asp:Label ID="lblCantidadFilas" runat="server"></asp:Label>
                                    </div>
                                    <br />
                                    <br />
                                </div>
                            </div>
                        </div>
                        <br />
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>

    <script>
        // Para renderizar chartA
        function renderizarCharts(objChartDoughnutA, objChartDoughnutB, objbarchar) {
            var ctxDoughnut = document.getElementById('chartDoughnutA').getContext('2d');
            var chartDoughnut = new Chart(ctxDoughnut, objChartDoughnutA);
            // Generamos las leyendas
            var divDoughnutLegends = document.getElementById("divDoughnutLegendsA");
            var legends = "";
            for (index in objChartDoughnutA.data.labels) {
                legends += "<p><i class='fa fa-square' style='color: " +
                    objChartDoughnutA.data.datasets[0].backgroundColor[index] + "'></i> " + objChartDoughnutA.data.labels[index] + " </p>";
            }
            divDoughnutLegendsA.innerHTML = legends;
            // Para renderizar chartB
            var ctxDoughnutB = document.getElementById('chartDoughnutB').getContext('2d');
            var chartDoughnutB = new Chart(ctxDoughnutB, objChartDoughnutB);
            // Generamos las leyendas
            var divDoughnutLegendsB = document.getElementById("divDoughnutLegendsB");
            var legendsB = "";
            for (index in objChartDoughnutB.data.labels) {
                legendsB += "<p><i class='fa fa-square' style='color: " +
                    objChartDoughnutB.data.datasets[0].backgroundColor[index] + "'></i> " + objChartDoughnutB.data.labels[index] + " </p>";
            }
            divDoughnutLegendsB.innerHTML = legendsB;

            // Para renderizar barchart
            var ctxbarchar = document.getElementById('barchar').getContext('2d');
            var ctxbarchar = new Chart(ctxbarchar, objbarchar);
        }

        window.onload = function () {
            document.getElementById('<%= lbtnBuscar.ClientID %>').click();
        };
    </script>

</asp:Content>

