﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Application_VersionesActivas_pdf_Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<script runat="server">

    protected void btnExportarPDF_Click(object sender, EventArgs e)
    {

    }
</script>


<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>ControlNet</title>
    <link rel="stylesheet" href="~/js/validator-master/fv.css" type="text/css" />
    <link rel="shortcut icon" type="image/x-ic1on" href="~/img/favicon.ico" />
    <link rel="stylesheet" href="~/lib/bootstrap/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="~/lib/font-awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="~/lib/font-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" href="~/lib/ionicons/css/ionicons.css" />
    <link rel="stylesheet" href="~/lib/ui-select/select.css" />
    <link rel="stylesheet" href="~/lib/ng-table/ng-table.css" />
    <link rel="stylesheet" href="~/lib/iCheck/skins/flat/green.css" />
    <link rel="stylesheet" href="~/lib/gentelella/css/custom.css" />
    <link rel="stylesheet" href="~/css/site.css" />
    <link rel="stylesheet" href="~/lib/nprogress/nprogress.css" />
    <link rel="stylesheet" href="~/lib/fullcalendar/fullcalendar.min.css" />
    <link rel="stylesheet" type="text/css" media="print" href="~/lib/fullcalendar/fullcalendar.print.min.css" />
    <link rel="stylesheet" href="~/css/jquery-ui.css" />
    <link rel="stylesheet" href="~/css/custom.css" />

    <script src='<%= Page.ResolveUrl("~/lib/jquery/dist/jquery.js") %>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("~/lib/jquery/dist/jquery.min.js") %>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("~/lib/jquery/dist/jquery-ui.min.js") %>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("~/lib/moment/moment.min.js") %>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("~/lib/fullcalendar/fullcalendar.min.js") %>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("~/lib/fullcalendar/locale/es.js") %>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("~/lib/easy-pie-chart-master/dist/jquery.easypiechart.js") %>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("~/lib/chart/Chart.js") %>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("~/js/html2canvas.js") %>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("~/js/vfs_fonts.js") %>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("~/js/pdfmake.min.js") %>' type="text/javascript"></script>


</head>
<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <form runat="server">
                <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnablePartialRendering="true" EnablePageMethods="true">
                </ajaxToolkit:ToolkitScriptManager>

                <asp:Panel ID="pnlGrilla" runat="server">
                    <div class="page-title">
                        <div>
                        </div>
                        <div class="title_right">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div>
                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/img/Logo_Cnet_2175x457.png" Width="30%" />
                                </div>
                                <br />
                                <div class="x_title" id="Titulo">
                                    <h2>
                                        <asp:Literal ID="LitNombre" runat="server" Text='' /></h2>
                                    <div class="clearfix"></div>
                                </div>

                                <%--INDICADORES--%>
                                <div class=" col-md-12 col-sm-12 col-xs-12" id="pnlIndicadores">
                                    <div class="panel" runat="server">
                                        <div class="x_title">
                                            <h2>Indicadores</h2>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="row tile_count">
                                            <div class="col-md-4 col-sm-4 col-xs-4 tile_stats_count">
                                                <span class="count_top">% Cump. Fecha Entrega</span>
                                                <div class="count">
                                                    <asp:Label runat="server" ID="lblIndicador1" class="count green"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-4 tile_stats_count">
                                                <span class="count_top">% Entregas Aceptadas</span>
                                                <div class="count">
                                                    <asp:Label runat="server" ID="lblIndicador2" class="count green"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-4 tile_stats_count">
                                                <span class="count_top">% Tiempo Consumido</span>
                                                <div class="count">
                                                    <asp:Label runat="server" ID="lblIndicador3" class="count green"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <%--SUBINDICADORES--%>
                                <div class=" col-md-12 col-sm-12 col-xs-12" id="pnlAgrupadores">
                                    <div class="panel" runat="server">
                                        <div class="x_title">
                                            <h2>Agrupadores</h2>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="row tile_count">
                                            <div class="col-md-3 col-sm-3 col-xs-3 tile_stats_count">
                                                <span class="count_top"><i class="fa fa-cubes"></i>Cantidad de Proyectos</span>
                                                <div class="count">
                                                    <asp:Label runat="server" ID="lblInd_Proyectos_Total" class="count green"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-3 col-xs-3 tile_stats_count">
                                                <span class="count_top"><i class="fa fa-code-fork"></i>Cantidad de Versiones</span>
                                                <div class="count">
                                                    <asp:Label runat="server" ID="lblInd_Ver_Total" class="count green"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-3 col-xs-3 tile_stats_count">
                                                <span class="count_top"><i class="fa fa-file-code-o"></i>Ordenes de Trabajo</span>
                                                <div class="count">
                                                    <asp:Label runat="server" ID="lblInd_OTs_Total" class="count green"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-3 col-xs-3 tile_stats_count">
                                                <span class="count_top"><i class="fa fa-user"></i>Horas</span>
                                                <div class="count green">
                                                    <asp:Label runat="server" ID="lblInd_Horas_Total" class="count green"></asp:Label>
                                                </div>
                                                <span class="count_bottom"><i class="green">
                                                    <asp:Label runat="server" ID="lblInd_Hrs_Estimadas"></asp:Label></i> Estimadas</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <%--Graficos--%>
                                <div class="col-md-12 col-sm-12 col-xs-12" id="pnlGraficos">
                                    <div class="panel col-md-12 col-sm-12 col-xs-12" id="pnlBarra">
                                        <div class="x_title">
                                            <h2>Horas Estidmadas y Dedicadas</h2>
                                            <div class="clearfix"></div>
                                        </div>
                                        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Panel runat="server" class="col-md-12 col-sm-12 col-xs-12" ID="Panel3">
                                                    <asp:LinkButton runat="server" ID="lbtnBarra" CssClass="btn btn-round btn-alert hidden" OnClick="lbtnBuscar_Click">
                                                              Buscar </asp:LinkButton>
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <canvas id="barchar" height="50"></canvas>
                                                    </div>
                                                </asp:Panel>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div class="form-group" runat="server">
                                        <div class="panel col-md-6 col-sm-6 col-xs-6" id="pnlGraficoA">
                                            <div class="x_title">
                                                <h2>Cantidad de Versiones</h2>
                                                <div class="clearfix"></div>
                                            </div>
                                            <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:Panel runat="server" class="col-md-12 col-sm-12 col-xs-12" ID="Panel1">
                                                        <asp:LinkButton runat="server" ID="lbtnBuscar" CssClass="btn btn-round btn-alert hidden" OnClick="lbtnBuscar_Click">
                                                              Buscar </asp:LinkButton>
                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                            <canvas id="chartDoughnutB"></canvas>
                                                        </div>
                                                        <div id="divDoughnutLegendsB" class="col-md-5 col-sm-5 col-xs-5">
                                                        </div>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>

                                        <div class="panel col-md-6 col-sm-6 col-xs-6" id="pnlGraficoB">
                                            <div class="x_title">
                                                <h2>Ordenes de trabajo</h2>
                                                <div class="clearfix"></div>
                                            </div>
                                            <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:Panel runat="server" class="col-md-12 col-sm-12 col-xs-12" ID="Panel2">
                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                            <canvas id="chartDoughnutA"></canvas>
                                                        </div>
                                                        <div id="divDoughnutLegendsA" class="col-md-5 col-sm-5 col-xs-5">
                                                        </div>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                        </div>
                    </div>
                </asp:Panel>

            </form>
        </div>
    </div>

    <script src='<%= Page.ResolveUrl("~/js/validator-master/multifield.js")%>'></script>
    <script src='<%= Page.ResolveUrl("~/js/validator-master/validator.js")%>'></script>

    <script>
        // Para renderizar chartA
        function renderizarCharts(objChartDoughnutA, objChartDoughnutB, objbarchar) {
            var ctxDoughnut = document.getElementById('chartDoughnutA').getContext('2d');
            var chartDoughnut = new Chart(ctxDoughnut, objChartDoughnutA);
            // Generamos las leyendas
            var divDoughnutLegends = document.getElementById("divDoughnutLegendsA");
            var legends = "";
            for (index in objChartDoughnutA.data.labels) {
                legends += "<p><i class='fa fa-square' style='color: " +
                    objChartDoughnutA.data.datasets[0].backgroundColor[index] + "'></i> " + objChartDoughnutA.data.labels[index] + " </p>";
            }
            divDoughnutLegendsA.innerHTML = legends;
            // Para renderizar chartB
            var ctxDoughnutB = document.getElementById('chartDoughnutB').getContext('2d');
            var chartDoughnutB = new Chart(ctxDoughnutB, objChartDoughnutB);
            // Generamos las leyendas
            var divDoughnutLegendsB = document.getElementById("divDoughnutLegendsB");
            var legendsB = "";
            for (index in objChartDoughnutB.data.labels) {
                legendsB += "<p><i class='fa fa-square' style='color: " +
                    objChartDoughnutB.data.datasets[0].backgroundColor[index] + "'></i> " + objChartDoughnutB.data.labels[index] + " </p>";
            }
            divDoughnutLegendsB.innerHTML = legendsB;

            // Para renderizar barchart
            var ctxbarchar = document.getElementById('barchar').getContext('2d');
            var ctxbarchar = new Chart(ctxbarchar, objbarchar);
        }

        window.onload = function () {
            document.getElementById('<%= lbtnBuscar.ClientID %>').click();
        };

            //Crea las imagenes de la info. gral. y los gráficos luego llama a la funcion que crea el pdf
            function CrearImagen() {
                var imagen;
                var imagen1;
                var imagen2;
                var imagenA;
                var imagenB;
                html2canvas(document.getElementById("Titulo")).then(canvas => {
                    imagenA = canvas.toDataURL();

                    html2canvas(document.getElementById("pnlIndicadores")).then(canvas => {
                        imagenB = canvas.toDataURL();

                        html2canvas(document.getElementById("pnlAgrupadores")).then(canvas => {
                            imagen1 = canvas.toDataURL();

                            html2canvas(document.getElementById("pnlGraficos")).then(canvas => {
                                imagen2 = canvas.toDataURL();

                                html2canvas(document.getElementById("Image1")).then(canvas => {
                                    imagen = canvas.toDataURL();
                                    CrearPDF(imagenA, imagenB, imagen1, imagen2, imagen);
                                });
                            });
                        });
                    });
                });
            };
            function CrearPDF(imagenA, imagenB, imagen1, imagen2, imagen) {
                var docDefinition = {
                    header: '',
                    pageSize: 'A4',
                    content: []
                };
                docDefinition.content.push({
                    image: imagen,
                    width: 150
                })
                docDefinition.content.push({
                    image: imagenA,
                    width: 500
                })
                docDefinition.content.push({
                    image: imagenB,
                    width: 500
                })
                docDefinition.content.push({
                    image: imagen1,
                    width: 500
                })
                docDefinition.content.push({
                    image: imagen2,
                    width: 500
                })
                pdfMake.createPdf(docDefinition).download('<%= LitNombre.Text %>' + '.pdf');
            };

            ///Método que se lanza tan pronto como se termina de cargar la página      
            window.onload = function () {
                document.getElementById('<%= lbtnBuscar.ClientID %>').click();
            };
            window.onload = function () {
                document.getElementById('<%= lbtnBuscar.ClientID %>').click();
                setTimeout(function () {
                    CrearImagen();
                }, 1400);
            };
    </script>

    <script src='<%= Page.ResolveUrl("~/lib/bootstrap/dist/js/bootstrap.js") %>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("~/lib/bootstrap/dist/js/bootstrap.min.js") %>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("~/lib/iCheck/icheck.js") %>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("~/lib/angular/angular.js") %>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("~/lib/angular/angular-sanitize.js") %>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("~/lib/angular/angular-animate.js") %>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("~/lib/ui-select/select.js") %>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("~/lib/ui-bootstrap/ui-bootstrap-tpls.js") %>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("~/js/controller/controller.js") %>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("~/js/site.js") %>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("~/lib/fastclick/lib/fastclick.js") %>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("~/lib/nprogress/nprogress.js") %>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("~/js/custom.js") %>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("~/lib/switchery/dist/switchery.min.js") %>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("~/js/html2canvas.js") %>' type="text/javascript"></script>

</body>
