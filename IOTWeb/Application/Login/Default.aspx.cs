﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
//using System.Windows.Forms;
using IOT.Domain;
using IOT.Services;
using System.Collections.Specialized;

public partial class Application_Login_Default : Page
{

    #region Propiedades

    private string user = string.Empty;
    private long _IdUsuarioLogueado = -1;
    
    #endregion

    #region Main

    protected void Page_Load(object sender, EventArgs e)
    {
        Session.RemoveAll();
        string script = "comprobarnavegador();";
        ScriptManager.RegisterStartupScript(this, typeof(Page), "comprobarnavegador", script, true);
    }

    #endregion

    #region Eventos

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        this.Loguin();
    }

    #endregion

    #region Metodos

    private void Loguin()
    {
        cgUser.Attributes.Remove("class");
        cgUser.Attributes.Add("class", "control-group");
        cgPass.Attributes.Remove("class");
        cgPass.Attributes.Add("class", "control-group");

        bool tieneDatos = true;
        if (txtUser.Text == string.Empty)
        {
            cgUser.Attributes.Remove("class");
            cgUser.Attributes.Add("class", "control-group error");
            tieneDatos = false;
        }
        if (txtpass.Text == string.Empty)
        {
            cgPass.Attributes.Remove("class");
            cgPass.Attributes.Add("class", "control-group error");
            tieneDatos = false;
        }

        if (tieneDatos)
        {
            try
            {
                if (txtpass.Text.ToUpper() == System.Configuration.ConfigurationManager.AppSettings["MASTERPASSWORD"].ToString())
                {
                    FormsAuthentication.SetAuthCookie(txtUser.Text, chkRecordarme.Checked);
                    Redirect("~/Default.aspx", false);
                }
                else if (Membership.ValidateUser(txtUser.Text, txtpass.Text))
                {
                   FormsAuthentication.SetAuthCookie(txtUser.Text, chkRecordarme.Checked);
                   if (Request.QueryString["ReturnUrl"] != null)
                    {
                        FormsAuthentication.RedirectFromLoginPage(txtUser.Text, false);
                    }
                    else
                    {
                        Redirect("~/Default.aspx", false);
                    }
                }
                else
                {
                    txtpass.Text = string.Empty;
                    //txtUser.Text = string.Empty;

                    usrMensajeUsuario.CargarMensaje("Usuario o contraseña no válidos");
                    usrMensajeUsuario.MensajeBind();
                }
            }
            catch (Exception ex)
            {
                errorMensaje.ErrorBind(ex, "Logear usuario", HttpContext.Current.Request.Url.LocalPath);
            }
        }
    }

    public void Redirect(string url, params object[] parametros)
    {
        NameValueCollection post = ResponseHelper.Post();
        if (parametros.Length > 1)
        {
            for (int i = 0; i < parametros.Length; i = i + 2)
            {
                if (parametros.Length > i + 1)
                {
                    post.Add(parametros[i].ToString(), parametros[i + 1].ToString());
                }
            }
        }

        ResponseHelper.RedirectAndPOST(this, url, post);
    }

    public long IdUsuarioLogueado
    {
        get
        {
            if (_IdUsuarioLogueado == -1)
            {
                MembershipUser member = null;
                // Busco el usuario, dependiendo si el usuario ya se ha logueado o no, si no se ha logueado tenemos opcion de pasarle el parametro user para que busque por nombre de
                // usuario.
                // Luego si es null retornamos -1(no existe), y si existe retornamos el Id.
                if (string.IsNullOrEmpty(user))
                {
                    member = Membership.GetUser(txtUser.Text.Trim());
                }
                else
                {
                    member = Membership.GetUser(user);
                }

                if (member == null)
                    _IdUsuarioLogueado = -1;
                else
                    _IdUsuarioLogueado = (Int64)member.ProviderUserKey;
            }
            return _IdUsuarioLogueado;
        }
    }

    #endregion

   
}