﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="Application_Login_Default" %>

<%@ Register Src="../../UserControl/usrMensajeUsuario.ascx" TagName="usrMensajeUsuario" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
   <script type="text/javascript">

     function comprobarnavegador() {
            
            var is_ie = navigator.userAgent.toLowerCase().indexOf('msie ') > -1;
            var is_firefox = navigator.userAgent.toLowerCase().indexOf('firefox/') > -1;

            if (is_ie) {

                var posicion = navigator.userAgent.toLowerCase().lastIndexOf('msie ');
                var ver_ie = navigator.userAgent.toLowerCase().substring(posicion + 5, posicion + 8);

                ver_ie = parseFloat(ver_ie);

                if (ver_ie <= 8)
                {
                    alert("Para usar esta aplicación es necesario una versión de internet explorer igual o superior a 9.0");
                    $('#ctl00_MainContent_btnLogin').attr("disabled", "disabled");
                }

            }

            if (is_firefox) {

                var posicion = navigator.userAgent.toLowerCase().lastIndexOf('firefox/');
                var ver_firefox = navigator.userAgent.toLowerCase().substring(posicion + 8, posicion + 12);

                ver_firefox = parseFloat(ver_firefox);

                if (ver_firefox <= 19)
                {
                    alert("Para usar esta aplicación es necesario una versión de firefox igual o superior a 20.0");
                    $('#ctl00_MainContent_btnLogin').attr("disabled", "disabled");
                }

            }
        }
   </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <%@ Register TagPrefix="usrError" TagName="Error" Src="~/UserControl/Error.ascx" %>
    <%@ Register Src="~/UserControl/userProcesando.ascx" TagPrefix="uc1" TagName="userProcesando" %>

    <div class="row">
        <div class="col-md-4 col-sm-12 col-xs-12 form-group">
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12 form-group mid_center">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Ingreso al Sistema</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <div class="form-horizontal form-label-left">
                        <div class="form-group"  id="cgUser"  runat="server">
                            <asp:Label ID="lblIdTitu" Text="ID" runat="server" Visible="true" class="control-label col-md-3 col-sm-3 col-xs-12">Usuario</asp:Label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <asp:TextBox ID="txtUser" runat="server" placeholder="Usuario" MaxLength="50" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group" runat="server" id="cgPass">
                            <asp:Label ID="Label1" Text="ID" runat="server" Visible="true" class="control-label col-md-3 col-sm-3 col-xs-12">Contraseña</asp:Label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <asp:TextBox ID="txtpass" TextMode="Password" placeholder="Contraseña" runat="server" MaxLength="50" class="form-control" />
                            </div>
                        </div>
                        <div class="Center">
                            <label>
                                <asp:CheckBox ID="chkRecordarme" runat="server" type="checkbox" class="js-switch" /> Recordarme
                            </label>
                        </div>
                        <button type="submit" id="btnLogin" runat="server" causesvalidation="False" class="btn btn-round btn-primary" onserverclick="btnLogin_Click">
                            <i class="fa fa-user"></i> Ingresar
                        </button>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12 form-group">
        </div>
    </div>
    <usrError:Error runat="server" ID="errorMensaje" />
    <uc1:usrMensajeUsuario ID="usrMensajeUsuario" runat="server" />
    <uc1:userProcesando runat="server" ID="userProcesando" />
</asp:Content>
