﻿using IOT.Domain;
using IOT.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Application_Clientes_Default : PageBase
{
    #region Variables
    private ServClientes oServCliente = Services.Get<ServClientes>();
    private ServTipoPerfilMenues oServPerfilMenus = Services.Get<ServTipoPerfilMenues>();
    #endregion

    #region Main

    protected void Page_Load(object sender, EventArgs e)
    {
        if (base.IdUsuarioLogueado == -1)
        {
            Response.Redirect("~/Application/Login/Default.aspx", false);
            return;
        }
        if (!Page.IsPostBack)
            InicializacionPantalla();
    }

    #endregion

    #region Metodos

    private void InicializacionPantalla()
    {
        try
        {
            CargoPermisosdePantalla();
            CriterioOrdenacion = "Identificacion ASC";
            CargarGrilla(CriterioOrdenacion);
            VisibilidadPaneles(true, false);
            usrUltimaModificacion1.Visible = true;
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "InicializarPantalla", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    private void CargoPermisosdePantalla()
    {
        var tipoPerfilMenu = oServPerfilMenus.TraerTodosTipoPefilMenuesPorPerfil(IdPerfilUsuarioLogueado).Where(p => p.oMenu.PathPagina.Contains("/Clientes"));
        if (tipoPerfilMenu.Count() != 0)
        {
            List<TipoPerfilMenues> t = tipoPerfilMenu.ToList();
            btnAgregar.Visible = t.First().PuedeGrabar;
            btnGrabar.Visible = t.First().PuedeGrabar;
        }
        else
            Response.Redirect("~/Application/Default.aspx", false);

    }


    private void CargarGrilla(string pOrderBy)
    {
        try
        {
            
            List<Clientes> wQuery;

            wQuery = oServCliente.TraerTodos(null).OrderBy(p => p.Nombre).ToList();
            grdGrilla.DataSource = wQuery;
            grdGrilla.DataBind();
            lblCantidadFilas.Text = wQuery.Count.ToString();
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargarGrilla", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    private void VisibilidadPaneles(bool pShowPnlPpal, bool pShowPnlDetalle)
    {
        pnlGrilla.Visible = pShowPnlPpal;
        pnlDetalles.Visible = pShowPnlDetalle;
    }

    private void CargarControlesABM()
    {
        try
        {
            Clientes oObj = oServCliente.TraerUnicoXId(EntityId);
            lblId.Text = oObj.Id.ToString();
            txtNombre.Text = oObj.Nombre.ToUpper().Replace(".","") ;

            txtComentario.Text = oObj.Comentario;
            txtContacto.Text = oObj.Contacto.ToUpper().Trim();
            txtCuit.Text = oObj.Cuit;
            txtDomicilio.Text = oObj.Domicilio;
            txtEmail.Text = oObj.Correo;
            txtProvincia.Text = oObj.Provincia.ToUpper().Trim();
            txtTelefono.Text = oObj.Telefono;

            chkInactivo.Checked = oObj.Inactivo;
            if (oObj.Inactivo)
            {
                pnlInactivo.Visible = true;
                txtInactivoFecha.Text = oObj.InactivoFecha.ToShortDateString();
                txtInactivoMotivo.Text = oObj.InactivoComentario;
            }
            else
            {
                pnlInactivo.Visible = false;
                txtInactivoFecha.Text = "";
                txtInactivoMotivo.Text = "";
            }
            usrUltimaModificacion1.Visible = true;
            CargarUserControl(oObj.IdUsuarioModifico, oObj.FecUltimaModificacion);
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargarControles", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    private void Grabar()
    {
        try
        {
            Clientes oObj = ObtenerDatos();
            if (ValidarDatos(oObj))
            {
                
                if (EsNuevo)
                {
                    EntityId = oServCliente.Agregar(oObj);
                }
                else
                {
                    oServCliente.Actualizar(oObj);
                }
                CargarGrilla(CriterioOrdenacion);
                LimpiarControlesABM();
                VisibilidadPaneles(true, false);
                usrUltimaModificacion1.Visible = false;
            }
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "GrabarDatos", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    private bool ValidarDatos(Clientes coObj)
    {
        bool comodin = true;
        pnlError.Visible = false;
        string error = string.Empty;

        if (coObj.Nombre.Trim().Length == 0)
        {
            error = "Debe ingresar un Nombre Valido <br>";
            comodin = false;
        }
        else
        {
            if (EsNuevo)
            {
                //Valido que no existe un componente con ese nombre
                Clientes oObj = oServCliente.TraerTodos(null).Where(p => p.Nombre == coObj.Nombre.Trim()).FirstOrDefault();
                if (oObj != null)
                {
                    error = error + "Ya Existe un Producto con ese Nombre, por favor reintente <br>";
                    comodin = false;
                }
                
            }
            else
            {
                List<Clientes> oObj = oServCliente.TraerTodos(null).Where(p => p.Nombre == coObj.Nombre.Trim()).ToList();
                oObj.RemoveAll(p => p.Id == coObj.Id);
                if (oObj.Count() != 0)
                {
                    error = error + "Ya Existe un Producto con ese Nombre, por favor reintente <br>";
                    comodin = false;
                }
            }
        }

        if (!string.IsNullOrEmpty(error))
        {
            pnlError.Visible = true;
            litError.Text = error;
        }

        return comodin;
    }

    /// <summary>
    /// Método que devuelve una instancia de Rubro cargada con los datos ingresados en los controles del form de ABM
    /// </summary>
    /// <returns>Rubro</returns>
    private Clientes ObtenerDatos()
    {
        try
        {
            Clientes oObj = new Clientes();
            if (!EsNuevo)
                oObj.Id = EntityId;
        
            oObj.Nombre = txtNombre.Text.ToUpper().Replace(".", "");

            oObj.Comentario = txtComentario.Text.Trim();
            oObj.Contacto = txtContacto.Text.Trim();
            oObj.Cuit = txtCuit.Text ;
            oObj.Domicilio = txtDomicilio.Text ;
            oObj.Correo = txtEmail.Text;
            oObj.Provincia = txtProvincia.Text.ToUpper().Trim();
            oObj.Telefono = txtTelefono.Text;


            oObj.Inactivo = chkInactivo.Checked;
            if (chkInactivo.Checked)
            {
                oObj.InactivoFecha = Convert.ToDateTime(txtInactivoFecha.Text);
                oObj.InactivoComentario = txtInactivoMotivo.Text;
            }
            else
            {
                oObj.InactivoFecha = DateTime.MinValue;
                oObj.InactivoComentario = "";
            }

            oObj.FecUltimaModificacion = DateTime.Now;
            //Usuario Logueado

            oObj.IdUsuarioModifico = base.IdUsuarioLogueado;

            return oObj;
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "ObtenerDatos", HttpContext.Current.Request.Url.LocalPath);
            return null;
        }
    }


    private void LimpiarControlesABM()
    {
        //CARGO GRILLAS VISTA DETALLE
       
        lblId.Text = string.Empty;
        txtNombre.Text = string.Empty;
        chkInactivo.Checked = false;
        pnlError.Visible = false;
        txtInactivoMotivo.Text = string.Empty;
        txtInactivoFecha.Text = string.Empty;

        txtComentario.Text = string.Empty;
        txtContacto.Text = string.Empty;
        txtCuit.Text = string.Empty;
        txtDomicilio.Text = string.Empty;
        txtEmail.Text = string.Empty;
        txtProvincia.Text = string.Empty;
        txtTelefono.Text = string.Empty;

    }

    /// <summary>
    /// Método que carga los datos en el usercontrol usrUltimaModificacion1.
    /// </summary>
    private void CargarUserControl(long pIdUsuarioMofifico, DateTime pFechaUltimaModificacion)
    {
        usrUltimaModificacion1.FecUltimaModificacion = pFechaUltimaModificacion.ToShortDateString();
        usrUltimaModificacion1.IdUsuarioModifico = pIdUsuarioMofifico;
        usrUltimaModificacion1.UltimaModificacionBind();
    }

    #endregion

    #region Eventos de Grilla Principal

    protected void grdGrilla_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdGrilla.PageIndex = e.NewPageIndex;
        CargarGrilla(CriterioOrdenacion);
    }

    protected void grdGrilla_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Editar")
        {
            EntityId = Convert.ToInt64(e.CommandArgument);
            EsNuevo = false;
            CargarControlesABM();
            litNombre.Text = "Editar Cliente";
            VisibilidadPaneles(false, true);
        }
    }

    protected void grdGrilla_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla(CriterioOrdenacion);
    }

    #endregion

    #region "Eventos Botones"
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
    }

    protected void btnAgregar_Click(object sender, EventArgs e)
    {
        litNombre.Text = "Nuevo Cliente";
        EsNuevo = true;
        LimpiarControlesABM();
        VisibilidadPaneles(false, true);
    }

    protected void btnGrabar_ServerClick(object sender, EventArgs e)
    {
        Page.Validate();
        if (Page.IsValid) Grabar();
    }

    protected void btnSalir_ServerClick(object sender, EventArgs e)
    {
        CargarGrilla(CriterioOrdenacion);
        LimpiarControlesABM();
        VisibilidadPaneles(true, false);
    }

    protected void chkInactivo_CheckedChanged(object sender, EventArgs e)
    {
        if (chkInactivo.Checked)
        {
            pnlInactivo.Visible = true;
            txtInactivoMotivo.Text = "";
            txtInactivoFecha.Text = "";
        }
        else
        {
            pnlInactivo.Visible = false;
            txtInactivoMotivo.Text = "";
            txtInactivoFecha.Text = "";
        }

    }
    #endregion

    protected void btnImportar_ServerClick(object sender, EventArgs e)
    {
        oServCliente.ImportacionClientes();
    }
}