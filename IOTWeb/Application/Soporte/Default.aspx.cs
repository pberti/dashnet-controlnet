﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IOT.Domain;
using System.Data;
using IOT.Services;
using System.IO;

public partial class Application_Soporte_Default : PageBase
{
    #region Propiedades & Variables
    private ServSoporte_Caso oServSoporte = Services.Get<ServSoporte_Caso>();
    private ServTipoPerfilMenues oServPerfilMenus = Services.Get<ServTipoPerfilMenues>();
    private ServClientes oServClientes = Services.Get<ServClientes>();
    private ServProducto oServTipoProductos = Services.Get<ServProducto>();
    #endregion

    #region "Main"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (base.IdUsuarioLogueado == -1)
        {
            Response.Redirect("~/Application/Login/Default.aspx", false);
            return;
        }
        if (!Page.IsPostBack)
            InicializacionPantalla();
    }
    private void InicializacionPantalla()
    {
        CargoPermisosdePantalla();
        CargoDDL();
        DireccionOrdenacion = string.Empty;
        VisibilidadPaneles(true, false);
        this.CargoGrilla("Fecha descending", this.ObtenerFiltro());
        pnlError.Visible = false;
        pnlSuccess.Visible = false;
    }

    private void CargoPermisosdePantalla()
    {
        var tipoPerfilMenu = oServPerfilMenus.TraerTodosTipoPefilMenuesPorPerfil(IdPerfilUsuarioLogueado).Where(p => p.oMenu.PathPagina.Contains("Soporte"));
        if (tipoPerfilMenu.Count() != 0)
        {
            List<TipoPerfilMenues> t = tipoPerfilMenu.ToList();
            btnAgregar.Visible = t.First().PuedeGrabar;
            btnGrabar.Visible = t.First().PuedeGrabar;
        }
        else
            Response.Redirect("~/Application/Default.aspx", false);

    }
    #endregion

    #region Metodos DDL
    private void CargoDDL()
    {
        ddlCliente.Items.Clear();
        List<Clientes> lstClientes = oServClientes.TraerTodos(true).OrderBy(p => p.Nombre).ToList();
        CargarDDL(lstClientes.Cast<Object>().ToList(), ddlCliente, "Nombre", "Id", null, null);

        //Filtro Clientes
        ddlFiltroCliente.Items.Clear();
        CargarDDL(lstClientes.Cast<Object>().ToList(), ddlFiltroCliente, "Nombre", "Id", "Todos", null);

        ddlTipoProducto.Items.Clear();
        List<TipoProducto> lstProductos = oServTipoProductos.TraerTodos().OrderBy(p => p.CodigoProducto).ToList();
        ddlTipoProducto.DataSource = lstProductos;
        ddlTipoProducto.DataTextField = "Nombre";
        ddlTipoProducto.DataValueField = "Id";
        ddlTipoProducto.DataBind();
        
        //Filtro Producto
        ddlFiltroProducto.Items.Clear();
        CargarDDL(lstProductos.Cast<Object>().ToList(), ddlFiltroProducto, "Nombre", "Id", "Todos", null);


        ddlCategoria.Items.Clear();
        List<Soporte_Categoria> lstCategorias = oServSoporte.TraerTodosCategoriasPadres().ToList();
        ddlCategoria.DataSource = lstCategorias;
        ddlCategoria.DataTextField = "Nombre";
        ddlCategoria.DataValueField = "Id";
        ddlCategoria.DataBind();

        ddlResolucion.Items.Clear();
        List<Soporte_Resolucion> lstResolucion = oServSoporte.TraerTodosResolucion().ToList();
        ddlResolucion.DataSource = lstResolucion;
        ddlResolucion.DataTextField = "Nombre";
        ddlResolucion.DataValueField = "Id";
        ddlResolucion.DataBind();
        //Filtro Resolucion
        ddlFiltroResolucion.Items.Clear();
        CargarDDL(lstResolucion.Cast<Object>().ToList(), ddlFiltroResolucion, "Nombre", "Id", "Todos", null);


        ddlHIstorialResolucion.Items.Clear();
        ddlHIstorialResolucion.DataSource = lstResolucion;
        ddlHIstorialResolucion.DataTextField = "Nombre";
        ddlHIstorialResolucion.DataValueField = "Id";
        ddlHIstorialResolucion.DataBind();

        ddlEstados.Items.Clear();
        List<Soporte_Estados> lstEstados = oServSoporte.TraerTodosEstados().ToList();
        ddlEstados.DataSource = lstEstados;
        ddlEstados.DataTextField = "Nombre";
        ddlEstados.DataValueField = "Id";
        ddlEstados.DataBind();
        //Filtro Estados
        ddlFiltroEstado.Items.Clear();
        CargarDDL(lstEstados.Cast<Object>().ToList(), ddlFiltroEstado, "Nombre", "Id", "Todos", null);


        ddlHistorialEstado.Items.Clear();
        ddlHistorialEstado.DataSource = lstEstados;
        ddlHistorialEstado.DataTextField = "Nombre";
        ddlHistorialEstado.DataValueField = "Id";
        ddlHistorialEstado.DataBind();
    }
    #endregion

    #region "Métodos - Generales Pantalla"
    private void VisibilidadPaneles(bool pPnlGrilla, bool pPnlDetalle)
    {
        pnlGrilla.Visible = pPnlGrilla;
        pnlDetalles.Visible = pPnlDetalle;
        pnlSuccess.Visible = false;
    }
    #endregion

    #region Metodos - Grilla
    private Soporte_Caso ObtenerFiltro()
    {
        Soporte_Caso oObj = null;
        return oObj;
    }

    private void ObtenerDireccionOrden(GridViewSortEventArgs evt)
    {
        if (DireccionOrdenacion == "")
        {
            DireccionOrdenacion = "descending";
        }
        else
        {
            if (DireccionOrdenacion == "ascending")
                DireccionOrdenacion = "descending";
            else
                DireccionOrdenacion = "ascending";
        }
    }

    private void CargoGrilla(string pOrderBy, Soporte_Caso pObj)
    {
        CriterioOrdenacion = pOrderBy;
        List<Soporte_Caso> wQuery = null;
        try
        {
            if (pObj == null)
            {
                wQuery = oServSoporte.TraerTodos().OrderBy(pOrderBy).ToList();
            }
            else
            {
                wQuery = oServSoporte.TraerTodos().OrderBy(pOrderBy).ToList();
            }

            //Verifico los Filtros
            //1-Estado
            if (ddlFiltroEstado.SelectedValue != "0")
                wQuery = wQuery.Where(p => p.IdEstadoCaso == Convert.ToInt64(ddlFiltroEstado.SelectedValue)).ToList();
            //2-Resolucion
            if (ddlFiltroResolucion.SelectedValue != "0")
                wQuery = wQuery.Where(p => p.IdResolucion == Convert.ToInt64(ddlFiltroResolucion.SelectedValue)).ToList();
            //3-Cliente
            if (ddlFiltroCliente.SelectedValue != "0")
                wQuery = wQuery.Where(p => p.IdCliente == Convert.ToInt64(ddlFiltroCliente.SelectedValue)).ToList();
            //4-Producto
            if (ddlFiltroProducto.SelectedValue != "0")
                wQuery = wQuery.Where(p => p.IdProducto == Convert.ToInt64(ddlFiltroProducto.SelectedValue)).ToList();
            //5-NroConsola
            if (txtFiltroNroConsola.Text.Trim() != "")
                wQuery = wQuery.Where(p => p.NroConsola.Contains(txtFiltroNroConsola.Text.Replace("-","").Trim())).ToList();
            //5-NRoReclamo
            if (txtFiltroNroReclamo.Text.Trim() != "")
                wQuery = wQuery.Where(p => p.NroReclamo!= null && p.NroReclamo.Contains(txtFiltroNroReclamo.Text.Trim())).ToList();

            grdGrilla.DataSource = wQuery;
            grdGrilla.DataBind();
            lblCantidadFilas.Text = wQuery.Count().ToString();
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargoGrilla", HttpContext.Current.Request.Url.LocalPath);
        }
        finally
        {
            wQuery = null;
        }
    }

    protected void grdGrilla_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdGrilla.PageIndex = e.NewPageIndex;
        this.CargoGrilla(CriterioOrdenacion, this.ObtenerFiltro());
    }

    protected void grdGrilla_Sorting(object sender, GridViewSortEventArgs e)
    {
        this.ObtenerDireccionOrden(e);
        this.CargoGrilla(string.Format("{0} {1}", e.SortExpression, DireccionOrdenacion), this.ObtenerFiltro());
    }

    protected void grdGrilla_RowCommand(Object sender, CommandEventArgs e)
    {
        if (e.CommandName == "Editar")
        {
            base.EntityId = Convert.ToInt64(e.CommandArgument);
            base.EsNuevo = false;
            litNombre.Text = "Editar Caso de Soporte";
            lblId.Text = EntityId.ToString();
            this.CargarControles();
            this.VisibilidadPaneles(false, true);
            pnlEncabezado.Enabled = true;
            this.pnlHistorial.Visible = true;
            pnlArchivoAdjunto.Visible = true;
            InicializarCamposHistorial();
            btnGrabar.Disabled = false;
        }
        if (e.CommandName == "Comentario")
        {
            base.EntityId = Convert.ToInt64(e.CommandArgument);
            base.EsNuevo = false;
            litNombre.Text = "Agregar Comentario al Caso de Soporte";
            lblId.Text = EntityId.ToString();
            CargarControles();
            VisibilidadPaneles(false, true);
            pnlHistorial.Visible = true;
            pnlArchivoAdjunto.Visible = true;
            InicializarCamposHistorial();
        }
    }
    protected void grdGrilla_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (DataBinder.Eval(e.Row.DataItem, "oEstado.Nombre") != null)
                {
                    string _Texto = DataBinder.Eval(e.Row.DataItem, "oEstado.Nombre").ToString();

                    if (_Texto == "Cumplimentada" || _Texto == "Cerrada")
                        e.Row.BackColor = System.Drawing.Color.LightGreen;
                    else
                    {
                        string _Fecha = DataBinder.Eval(e.Row.DataItem, "_Fecha").ToString();

                        if (Convert.ToDateTime(_Fecha) <= DateTime.Now.AddDays(-5))
                            e.Row.BackColor = System.Drawing.Color.Yellow;
                        if (Convert.ToDateTime(_Fecha) <= DateTime.Now.AddDays(-10))
                            e.Row.BackColor = System.Drawing.Color.LightCoral;
                    }
                }
            }
        }
        catch (Exception Ex)
        {

            throw;
        }


    }
    #endregion

    #region "Eventos Botones"

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        try
        {
            this.CargoGrilla("Fecha descending", this.ObtenerFiltro());

        }
        catch (SystemException ex)
        {
            errorMensaje.ErrorBind(ex, "btnBuscar_Click", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    protected void btnAgregar_Click(object sender, EventArgs e)
    {
        TituloForm = "Nuevo Caso de Soporte";
        this.CrearNuevo();
    }

    protected void btnGrabar_ServerClick(object sender, EventArgs e)
    {
        Page.Validate();
        if (Page.IsValid)
        {
            this.GrabarDatos();
            if (!pnlError.Visible)
            {
                pnlSuccess.Visible = true;
                litSuccess.Text = "Los datos se guardaron correctamente";
            }
        }
    }

    protected void btnSalir_ServerClick(object sender, EventArgs e)
    {
        CargoGrilla("Fecha descending", this.ObtenerFiltro());
        this.VisibilidadPaneles(true, false);
    }

    protected void btnBuscar_ServerClick(object sender, EventArgs e)
    {
        CargoGrilla("Fecha descending", this.ObtenerFiltro());
    }
    #endregion

    #region Metodos - Vista Detalle
    /// <summary>
    /// Metodo que carga los datos de los controles.
    /// </summary>
    private void CargarControles()
    {
        try
        {
            CargoDDL();
            var oObj = oServSoporte.TraerUnicoXId(base.EntityId);

            lblId.Text = base.EntityId.ToString();
            txtFechaIngreso.Text = oObj._Fecha;
            ddlCategoria.SelectedValue = oObj.IdCategoria.ToString();
            ddlCategoria_SelectedIndexChanged(null, null);
            ddlSubCategoria.SelectedValue = oObj.IdSubCategoria.ToString();

            ddlMedioContacto.SelectedValue = oObj.MedioContacto.ToString();
            ddlCliente.SelectedValue = oObj.IdCliente.ToString();
            txtContacto.Text = oObj.Contacto.ToUpper();
            txtContactoEmail.Text = oObj.ContactoEmail;

            txtNroConsola.Text = oObj.NroConsola.Replace("-", "");
            ddlTipoProducto.SelectedValue = oObj.IdTipoProducto.ToString();
            txtComentario.Text = oObj.Comentario;

            ddlResolucion.SelectedValue = oObj.IdResolucion.ToString();
            ddlEstados.SelectedValue = oObj.IdEstadoCaso.ToString();
            txtResponsable.Text = oObj.oUsuario.ApellidoYNombre2;

            pnlHistorial.Visible = true;
            if (oObj.NroReclamo != null)
                txtNroReclamo.Text = oObj.NroReclamo;
            else
                txtNroReclamo.Text = "No Posee";
            //Cargo los Archivo Adjuntos
            CargarArchivosAdjuntos(base.EntityId);
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargarControles", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    /// <summary>
    /// Metodo que guarda los datos establecidos en la pagina.
    /// </summary>
    private void GrabarDatos()
    {
        try
        {
            pnlError.Visible = false;
            Soporte_Caso oObj = ObtenerDatos();
            string mAccionRealizada = "";
            if (ValidoObjeto(oObj))
            //if (base.ValidateModel(oObj, ref this.frmDetalles))
            {
                if (EsNuevo)
                {
                    EntityId = oServSoporte.Agregar(oObj);
                    mAccionRealizada = "Creacion del Caso";
                }
                else
                {
                    oServSoporte.Actualizar(oObj);
                    mAccionRealizada = "Modificacion del Caso";
                }
                //Grabo un nuevo Historial
                Soporte_Historial oHistorial = new Soporte_Historial();
                oHistorial.IdEstado = oObj.IdEstadoCaso;
                oHistorial.IdResolucion = oObj.IdResolucion;
                oHistorial.IdSoporteCaso = oObj.Id;
                oHistorial.Comentario = mAccionRealizada;
                oHistorial.IdUsuario = base.IdUsuarioLogueado;
                oHistorial.Fecha = DateTime.Now;
                oServSoporte.AgregarHistorial(oHistorial);

                //Grabo el Archivo Adjunto
                GrabarArchivoAdjunto(oObj.Id);

                //Recargo las grillas
                CargoGrilla("Fecha descending", this.ObtenerFiltro());
                VisibilidadPaneles(true, false);
            }
        }
        catch (Exception ex)
        {
            pnlError.Visible = true;
            litError.Text = "Se produjo un error al Grabar el registro";
            errorMensaje.ErrorBind(ex, "GrabarDatos", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    private Soporte_Caso ObtenerDatos()
    {
        Soporte_Caso oObj = null;
        try
        {
            oObj = new Soporte_Caso();

            if (!EsNuevo)
            {
                oObj = oServSoporte.TraerUnicoXId(base.EntityId);
            }

            lblId.Text = base.EntityId.ToString();
            oObj.Fecha = Convert.ToDateTime(txtFechaIngreso.Text);
            oObj.IdCategoria = Convert.ToInt64(ddlCategoria.SelectedValue);
            oObj.IdSubCategoria = Convert.ToInt64(ddlSubCategoria.SelectedValue);
            Soporte_Caso.MedioComunicacion mEnum; 
            Enum.TryParse(ddlMedioContacto.SelectedValue.ToString(), out mEnum);
            oObj.MedioContacto = mEnum;
            oObj.IdCliente = Convert.ToInt64(ddlCliente.SelectedValue);
            oObj.Contacto = txtContacto.Text.ToUpper();
            oObj.ContactoEmail = txtContactoEmail.Text.Trim();

            oObj.IdTipoProducto = Convert.ToInt64(ddlTipoProducto.SelectedValue);
            oObj.NroConsola = txtNroConsola.Text.Replace("-", "");
            oObj.Comentario = txtComentario.Text.Trim();

            
            oObj.IdEstadoCaso = Convert.ToInt64(ddlEstados.SelectedValue);
            oObj.IdResolucion = Convert.ToInt64(ddlResolucion.SelectedValue);
            oObj.IdResponsable = base.IdUsuarioLogueado;

            oObj.NroReclamo = CrearNroReclamo();

        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "ObtenerDatos", HttpContext.Current.Request.Url.LocalPath);
        }
        return oObj;
    }


    private string CrearNroReclamo()
    {
        string mNroReclamo = DateTime.Now.ToString("yyyyMMdd") + "-";

        string mReclamoAux = null;
        Soporte_Caso oReclamoAux = oServSoporte.TraerTodos().Where(p => p.NroReclamo != null && p.NroReclamo.Contains(mNroReclamo)).OrderByDescending(p => p.Id).FirstOrDefault();
        if (oReclamoAux != null)
        {
            mReclamoAux = oReclamoAux.NroReclamo;
        }

        if (mReclamoAux == null)
        {
            mNroReclamo = mNroReclamo + "101";
        }
        else
        {
            string[] ArrayNroReclamo = mReclamoAux.Split('-');
            int mNro = Convert.ToInt32(ArrayNroReclamo[1]) + 1;
            mNroReclamo = mNroReclamo + mNro.ToString("#0");
        }
        return mNroReclamo;
    }
    private void CrearNuevo()
    {
        this.Limpiar();

        litNombre.Text = "Nuevo Caso de Soporte";
        this.VisibilidadPaneles(false, true);
        EsNuevo = true;

        pnlHistorial.Visible = false;
        pnlArchivoAdjunto.Visible = false;

        pnlEncabezado.Enabled = true;
        btnGrabar.Disabled = false;

        ddlCliente.SelectedIndex = 0;
        pnlError.Visible = false;
        pnlSuccess.Visible = false;

    }

    private void Limpiar()
    {
        lblId.Text = "0";
        txtFechaIngreso.Text = DateTime.Now.ToShortDateString();
        ddlMedioContacto.SelectedIndex = 0;
        ddlResolucion.SelectedValue = "1";

        txtNroConsola.Text = string.Empty;
        txtContacto.Text = string.Empty;
        txtContactoEmail.Text = string.Empty;
        txtComentario.Text = string.Empty;
        txtResponsable.Text = string.Empty;

        txtNroReclamo.Text = string.Empty;
        var AuxUsuario = base.TraerUsuarioLogeado();
        txtResponsable.Text = AuxUsuario.ApellidoYNombre2;
        pnlError.Visible = false;


    }

    #endregion

    #region Validacion de Campos
    /// <summary>
    /// Metodo que valida el tramite a cargar
    /// </summary>
    private bool ValidoObjeto(Soporte_Caso pObj)
    {
        bool comodin = true;
        pnlError.Visible = false;
        string error = string.Empty;

        return comodin;
    }
    #endregion

    #region Eventos PostBack
    protected void ddlCategoria_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlSubCategoria.Items.Clear();
        List<Soporte_Categoria> lstCategorias = oServSoporte.TraerTodosCategoriasHijos(Convert.ToInt64(ddlCategoria.SelectedValue)).ToList();
        ddlSubCategoria.DataSource = lstCategorias;
        ddlSubCategoria.DataTextField = "Nombre";
        ddlSubCategoria.DataValueField = "Id";
        ddlSubCategoria.DataBind();
    }

    protected void ddlCliente_SelectedIndexChanged(object sender, EventArgs e)
    {
        long mIdCliente = Convert.ToInt64(ddlCliente.SelectedValue);
        Clientes oCliente = oServClientes.TraerUnicoXId(mIdCliente);
        if (oCliente != null)
        {
            txtContactoEmail.Text = oCliente.Correo;
        }
    }
    #endregion

    #region Panel Historial

    #region Metodos de Historial
    private void InicializarCamposHistorial()
    {
        pnlEncabezado.Enabled = false;
        btnGrabar.Disabled = true;
        ddlHistorialEstado.SelectedValue = ddlEstados.SelectedValue;
        ddlHIstorialResolucion.SelectedValue = ddlResolucion.SelectedValue;
        txtHistorialComentario.Text = "";
        CargoGrillaHistorial();

        pnlError.Visible = false;
        pnlSuccess.Visible = false;
    }
    #endregion

    #region Botones
    protected void btnGrabarHistorial_ServerClick(object sender, EventArgs e)
    {
        //Grabo un nuevo Historial
        Soporte_Historial oObj = new Soporte_Historial();
        if (txtComentario.Text.Trim() != "")
        {
            oObj.IdEstado = Convert.ToInt64(ddlHistorialEstado.SelectedValue);
            oObj.IdResolucion = Convert.ToInt64(ddlHIstorialResolucion.SelectedValue);
            oObj.IdSoporteCaso = Convert.ToInt64(lblId.Text);
            oObj.Comentario = txtHistorialComentario.Text.Trim();
            oObj.IdUsuario = base.IdUsuarioLogueado;
            oObj.Fecha = DateTime.Now;
            oServSoporte.AgregarHistorial(oObj);
            //Actualizo el Estado y Resolucion del Caso
            Soporte_Caso oCaso = oServSoporte.TraerUnicoXId(oObj.IdSoporteCaso);
            oCaso.IdResolucion = Convert.ToInt64(ddlHIstorialResolucion.SelectedValue);
            oCaso.IdEstadoCaso = Convert.ToInt64(ddlHistorialEstado.SelectedValue);
            oServSoporte.Actualizar(oCaso);

            //Grabo el Archivo Adjunto
            GrabarArchivoAdjunto(Convert.ToInt64(lblId.Text));

            //Vuelvo a la Pantalla Principal
            InicializarCamposHistorial();
            CargoGrilla("Fecha descending", this.ObtenerFiltro());
            this.VisibilidadPaneles(true, false);
        }
    }

    protected void btnHistorialCancelar_ServerClick(object sender, EventArgs e)
    {
        InicializarCamposHistorial();
        CargoGrilla("Fecha descending", this.ObtenerFiltro());
        this.VisibilidadPaneles(true, false);
    }
    #endregion

    #region eventos de la Grilla Historial
    private void CargoGrillaHistorial()
    {
        List<Soporte_Historial> lstHistorial = oServSoporte.TraerTodosHistorialPorCaso(Convert.ToInt64(lblId.Text)).OrderByDescending(p => p.Fecha).ToList();
        grdHistorial.DataSource = lstHistorial;
        grdHistorial.DataBind();
    }
    protected void grdHistorial_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdHistorial.PageIndex = e.NewPageIndex;
        CargoGrillaHistorial();
    }
    #endregion

    #endregion

    #region Archivo Adjunto

    private void GrabarArchivoAdjunto(long pIdCaso)
    {
        try
        {
            if (ArchivoSelecionado())
            {
                string directoryName = "Files/Soporte/" + pIdCaso.ToString();
                var folder = Server.MapPath("~/" + directoryName);
                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }

                if ((fuArchivo.PostedFile.FileName != null) && (fuArchivo.PostedFile.ContentLength > 0))
                {
                    string fn = System.IO.Path.GetFileName(fuArchivo.PostedFile.FileName);
                    fn.Replace(" ", "_");

                    string path = Server.MapPath("~/Files/Soporte/" + pIdCaso.ToString() + "/");
                    string SaveLocation = path + fn;
                    fuArchivo.PostedFile.SaveAs(SaveLocation);
                    Response.Write("El archivo se ha cargado.");

                }
            }
        }
        catch (Exception ex)
        {
            pnlError.Visible = true;
            litError.Text = ex.Message;
        }
        
    }
    public bool ArchivoSelecionado()
    {
        if (fuArchivo.PostedFile != null)
        {
            if (!string.IsNullOrWhiteSpace(fuArchivo.PostedFile.FileName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }


    private void CargarArchivosAdjuntos(long pIdCaso)
    {
        string directoryName = "Files/Soporte/" + pIdCaso.ToString();
        var folder = Server.MapPath("~/" + directoryName + "/");

        List<object> lstArchivos = new List<object>();

        try
        {
            if (Directory.Exists(folder))
            {
                string[] files = Directory.GetFiles(folder);

                foreach (var item in files)
                {

                    object objArchivo = new
                    {
                        NombreArchivo = Path.GetFileName(item),
                        PathArchivo = "~/Files/Soporte/" + pIdCaso.ToString() + "/" + Path.GetFileName(item)
                    };

                    lstArchivos.Add(objArchivo);
                }

                //Cargo la Grilla

                
            }
            grdAdjuntos.DataSource = lstArchivos;
            grdAdjuntos.DataBind();

        }
        catch (Exception ex)
        {
            pnlError.Visible = true;
            litError.Text = ex.Message;
        }
        

    }
    protected void grdAdjuntos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

    }
    protected void grdAdjuntos_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Ver")
        {
            string mArchivo = e.CommandArgument.ToString();
            Response.Redirect(mArchivo, "_blank", null);
        }
        if (e.CommandName == "Borrar")
        {
            string mArchivo = e.CommandArgument.ToString();
            var folder = Server.MapPath(mArchivo);
            //Borro
            if (System.IO.File.Exists(folder))
            {
                System.IO.FileInfo info = new System.IO.FileInfo(folder);
                info.Attributes = System.IO.FileAttributes.Normal;
                System.IO.File.Delete(folder);
            }
            CargarArchivosAdjuntos(base.EntityId);
        }
    }
    #endregion

}