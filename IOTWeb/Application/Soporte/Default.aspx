﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Application_Soporte_Default" %>

<%@ Register TagPrefix="usrError" TagName="Error" Src="~/UserControl/Error.ascx" %>
<%@ Register Src="~/UserControl/userProcesando.ascx" TagPrefix="uc1" TagName="userProcesando" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <%--Vista Grilla--%>
    <asp:Panel ID="pnlGrilla" runat="server">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                </div>
                <div class="title_right">
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Casos de Soporte</h2>
                            <div class="pull-right">
                                <button type="submit" id="btnAgregar" runat="server" causesvalidation="False" class="btn btn-round btn-primary" onserverclick="btnAgregar_Click">
                                    <i class="fa fa-plus-circle"></i> Agregar Nuevo
                                </button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <%--FILTRO--%>
                        <div class="x_content">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Filtro</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-horizontal form-label-left pull-left">
                                    <div class="form-group">
                                        <%--ESTADO DEL CASO--%>
                                        <asp:Label runat="server" Text="Estado:" class="control-label col-md-1 col-sm-1 col-xs-6"></asp:Label>
                                        <div class="col-md-3 col-sm-3 col-xs-6">
                                            <asp:DropDownList runat="server" ID="ddlFiltroEstado" CssClass="form-control" Width="100%">
                                            </asp:DropDownList>
                                        </div>
                                        <%--RESOLUCION--%>
                                        <asp:Label runat="server" Text="Resolucion:" class="control-label col-md-1 col-sm-1 col-xs-6"></asp:Label>
                                        <div class="col-md-3 col-sm-3 col-xs-6">
                                            <asp:DropDownList runat="server" ID="ddlFiltroResolucion" CssClass="form-control" Width="100%">
                                            </asp:DropDownList>
                                        </div>
                                        
                                        <%--NRO CONSOLA--%>
                                        <asp:Label runat="server" Text="Nro Consola:" class="control-label col-md-2 col-sm-2 col-xs-6"></asp:Label>
                                        <div class="col-md-2 col-sm-2 col-xs-6">
                                            <asp:TextBox ID="txtFiltroNroConsola" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <%--CLIENTE--%>
                                        <asp:Label runat="server" Text="Cliente:" class="control-label col-md-1 col-sm-1 col-xs-6"></asp:Label>
                                        <div class="col-md-3 col-sm-3 col-xs-6">
                                            <asp:DropDownList runat="server" ID="ddlFiltroCliente" CssClass="form-control" Width="100%">
                                            </asp:DropDownList>
                                        </div>
                                        <%--TIPO PRODUCTO--%>
                                        <asp:Label runat="server" Text="Producto:" class="control-label col-md-1 col-sm-1 col-xs-2"></asp:Label>
                                        <div class="col-md-2 col-sm-2 col-xs-6">
                                            <asp:DropDownList runat="server" ID="ddlFiltroProducto" CssClass="form-control" Width="100%">
                                            </asp:DropDownList>
                                        </div>
                                        <%--Nro Reclamo--%>
                                        <asp:Label runat="server" Text="Nro Reclamo:" class="control-label col-md-2 col-sm-2 col-xs-2"></asp:Label>
                                        <div class="col-md-3 col-sm-3 col-xs-6">
                                            <asp:TextBox ID="txtFiltroNroReclamo" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="pull-right">
                                            <button type="submit" id="btnBuscar" runat="server" causesvalidation="False" class="btn btn-round btn-primary" onserverclick="btnBuscar_ServerClick">
                                                <i class="fa fa-search"></i> Buscar
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                    
                            </div>
                            <%--GRILLA--%>
                            <asp:GridView ID="grdGrilla" runat="server" OnPageIndexChanging="grdGrilla_PageIndexChanging"
                                OnSorting="grdGrilla_Sorting" OnRowDataBound="grdGrilla_RowDataBound" OnRowCommand="grdGrilla_RowCommand" AllowSorting="true"
                                AutoGenerateColumns="False" CssClass="table table-striped table-bordered">
                                <Columns>
                                    <asp:BoundField DataField="Id" HeaderText="ID" SortExpression="Id" Visible="True" />
                                    <asp:BoundField DataField="_Fecha" HeaderText="Fecha" SortExpression="Fecha" />
                                    <asp:BoundField DataField="NroReclamo" HeaderText="Nro Reclamo" SortExpression="NroReclamo" />
                                    <asp:BoundField DataField="NroConsola" HeaderText="Nro Consola" SortExpression="NroConsola" />
                                    <asp:BoundField DataField="oCliente.Nombre" HeaderText="Cliente" SortExpression="IdCliente" />
                                    <asp:BoundField DataField="oTipoProducto.Nombre" HeaderText="Producto" SortExpression="IdTipoProducto" />
                                    <asp:BoundField DataField="oCategoriaPadre.Nombre" HeaderText="Categoria" SortExpression="IdCategoria" />
                                    <asp:BoundField DataField="oEstado.Nombre" HeaderText="Estado" SortExpression="IdEstado" />
                                    <asp:BoundField DataField="oResolucion.Nombre" HeaderText="Resolucion" SortExpression="IdResolucion" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <div class="pull-right">
                                                <asp:LinkButton ID="btnEditar" runat="server" CommandName="Editar" CommandArgument='<%#Eval("Id") %>'
                                                    CssClass="btn btn-round"> <i class="fa fa-edit"></i> Editar</asp:LinkButton>
                                            </div>
                                            <div class="pull-right">
                                                <asp:LinkButton ID="btnComentario" runat="server" CommandName="Comentario" CommandArgument='<%#Eval("Id") %>'
                                                    CssClass="btn btn-round"> <i class="fa fa-plus"></i> Comentario</asp:LinkButton>
                                            </div>
                                        </ItemTemplate>
                                        <ItemStyle Width="10%" />
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle CssClass="pgr" />
                            </asp:GridView>
                            <br />
                            <div class="alert alert-success alert-dismissible fade in" role="alert">
                                <div class="pull-left">
                                    Total de Registros:
                                    <asp:Label ID="lblCantidadFilas" runat="server"></asp:Label>
                                </div>

                                <br />
                                <br />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>

    <%--Vista Detalles--%>
    <asp:Panel ID="pnlDetalles" runat="server">
        <div class="col-md-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>
                        <asp:Literal ID="litNombre" runat="server" Text='<%#TituloForm%>' /></h2>
                    <div class="clearfix"></div>
                </div>
                <%--PANEL DE ERRORES--%>
                <div class="alert alert-danger alert-dismissible fade in" role="alert" runat="server" id="pnlError" visible="false">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <strong><i class="fa fa-warning"></i>Atencion!!!</strong>
                    <br />
                    <asp:Literal ID="litError" runat="server" Text=""></asp:Literal>
                </div>
                <%--PANEL SUCCESS--%>
                <div class="alert alert-success alert-dismissible fade in" role="alert" runat="server" id="pnlSuccess" visible="false">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <strong><i class="fa fa-check"></i>Operacion Exitosa!!!</strong>
                    <br />
                    <asp:Literal ID="litSuccess" runat="server" Text=""></asp:Literal>
                </div>
                <div class="clearfix"></div>
                <div class="x_content">
                    <div class="form-horizontal form-label-left">
                        <asp:Panel ID="pnlEncabezado" runat="server">
                            <div class="form-group">
                                <%--ID--%>
                                <asp:Label ID="lblIdTitu" Text="ID" runat="server" Visible="true" class="control-label col-md-1 col-sm-1 col-xs-2">Id:</asp:Label>
                                <div class="col-md-1 col-sm-1 col-xs-2">
                                    <asp:Label ID="lblId" runat="server" Visible="true" class="form-control" />
                                </div>
                                <%--FECHA DE INGRESO--%>
                                <asp:Label ID="lblFechaIngreso" runat="server" Text="Fecha:" class="control-label col-md-1 col-sm-1 col-xs-12"></asp:Label>
                                <div class="col-md-2 col-sm-2 col-xs-12">
                                    <asp:UpdatePanel runat="server">
                                        <ContentTemplate>
                                            <div class='input-group date dtp' id='dtpFecHasta'>
                                                <asp:TextBox runat="server" ID="txtFechaIngreso" CssClass="form-control det-control" />
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <%--CATEGORIA--%>
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>
                                        <asp:Label runat="server" Text="Categoria:" class="control-label col-md-1 col-sm-1 col-xs-2"></asp:Label>
                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                            <asp:DropDownList runat="server" ID="ddlCategoria" CssClass="form-control" Width="100%" AutoPostBack="true" OnSelectedIndexChanged="ddlCategoria_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                        <%--SUB CATEGORIA--%>
                                        <asp:Label runat="server" Text="SubCategoria:" class="control-label col-md-1 col-sm-1 col-xs-2"></asp:Label>
                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                            <asp:DropDownList runat="server" ID="ddlSubCategoria" CssClass="form-control" Width="100%" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <asp:UpdatePanel runat="server">
                                <ContentTemplate>
                                    <div class="form-group">
                                        <%--MEDIO DE CONTACTO--%>
                                        <asp:Label runat="server" Text="Medio:" class="control-label col-md-1 col-sm-1 col-xs-6"></asp:Label>
                                        <div class="col-md-2 col-sm-2 col-xs-6">
                                            <asp:DropDownList runat="server" ID="ddlMedioContacto" CssClass="form-control" Width="100%">
                                                <asp:ListItem Text="Telefono" Value="Telefono"></asp:ListItem>
                                                <asp:ListItem Text="WhatsApp" Value="WhatsApp"></asp:ListItem>
                                                <asp:ListItem Text="Mail" Value="Mail"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <%--CLIENTE--%>
                                        <asp:Label runat="server" Text="Cliente:" class="control-label col-md-1 col-sm-1 col-xs-6"></asp:Label>
                                        <div class="col-md-3 col-sm-3 col-xs-12">
                                            <asp:DropDownList runat="server" ID="ddlCliente" CssClass="form-control" Width="100%" AutoPostBack="true" OnSelectedIndexChanged="ddlCliente_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                        <%--CONTACTO--%>
                                        <asp:Label runat="server" Text="Contacto:" class="control-label col-md-1 col-sm-1 col-xs-6"></asp:Label>
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <asp:TextBox ID="txtContacto" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                
                                    </div>
                                    <div class="form-group">
                                        <%--EMAIL--%>
                                        <asp:Label runat="server" Text="Email:" class="control-label col-md-1 col-sm-1 col-xs-6"></asp:Label>
                                        <div class="col-md-5 col-sm-5 col-xs-12">
                                            <asp:TextBox ID="txtContactoEmail" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                        <%--TIPO PRODUCTO--%>
                                        <asp:Label runat="server" Text="Producto:" class="control-label col-md-1 col-sm-1 col-xs-2"></asp:Label>
                                        <div class="col-md-2 col-sm-2 col-xs-6">
                                            <asp:DropDownList runat="server" ID="ddlTipoProducto" CssClass="form-control" Width="100%">
                                            </asp:DropDownList>
                                        </div>
                                        <%--NRO CONSOLA--%>
                                        <asp:Label runat="server" Text="Nro Consola:" class="control-label col-md-1 col-sm-1 col-xs-6"></asp:Label>
                                        <div class="col-md-2 col-sm-2 col-xs-6">
                                            <asp:TextBox ID="txtNroConsola" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <div class="form-group">
                                <%--OBSERVACIONES--%>
                                <asp:Label runat="server" Text="Observaciones:" class="control-label col-md-1 col-sm-1 col-xs-12"></asp:Label>
                                <div class="col-md-11 col-sm-11 col-xs-12">
                                    <asp:TextBox ID="txtComentario" runat="server" class="form-control" TextMode="MultiLine" Rows="3" placeholder="Ingrese Comentario"></asp:TextBox>
                                </div>
                            
                            </div>
                            <div class="form-group">
                                <%--ESTADO DEL CASO--%>
                                <asp:Label runat="server" Text="Estado:" class="control-label col-md-1 col-sm-1 col-xs-6"></asp:Label>
                                <div class="col-md-3 col-sm-3 col-xs-6">
                                    <asp:DropDownList runat="server" ID="ddlEstados" CssClass="form-control" Width="100%">
                                    </asp:DropDownList>
                                </div>
                                <%--RESOLUCION--%>
                                <asp:Label runat="server" Text="Resolucion:" class="control-label col-md-1 col-sm-1 col-xs-6"></asp:Label>
                                <div class="col-md-3 col-sm-3 col-xs-6">
                                    <asp:DropDownList runat="server" ID="ddlResolucion" CssClass="form-control" Width="100%">
                                    </asp:DropDownList>
                                </div>
                                <%--RESPONSABLE--%>
                                <asp:Label runat="server" Text="Responsable:" class="control-label col-md-1 col-sm-1 col-xs-6"></asp:Label>
                                <div class="col-md-3 col-sm-3 col-xs-6">
                                    <asp:TextBox ID="txtResponsable" runat="server" class="form-control" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                 <%--NRO DE RECLAMO--%>
                                <asp:Label runat="server" Text="Nro de Reclamo:" class="control-label col-md-2 col-sm-2 col-xs-6"></asp:Label>
                                 <div class="col-md-3 col-sm-3 col-xs-6">
                                    <asp:TextBox ID="txtNroReclamo" runat="server" class="form-control" Enabled="false" BackColor="LightGreen" Font-Bold="true" Font-Italic="true" Font-Size="XX-Large"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="pull-right">
                                    <button type="submit" id="btnGrabar" runat="server" causesvalidation="False" class="btn btn-round btn-primary" onserverclick="btnGrabar_ServerClick">
                                        <i class="fa fa-check"></i>Grabar
                                    </button>
                                    <button type="submit" id="btnSalir" runat="server" causesvalidation="False" class="btn btn-round btn-alert" onserverclick="btnSalir_ServerClick">
                                        <i class="fa fa-arrow-circle-left"></i>Volver
                                    </button>
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                </div>
            </div>



            <%-- Archivos Adjuntos --%>
            <div class="x_panel" runat="server" id="pnlArchivoAdjunto" visible="true">
                <div class="x_title">
                    <h2>
                        <asp:Literal ID="Literal2" runat="server" Text="Archivos Adjuntos del Caso de Soporte" />
                    </h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <asp:Label ID="lblImagenUsuario" Text="Archivo" runat="server" Visible="true" class="control-label col-md-3 col-sm-3 col-xs-3"></asp:Label>
                        <asp:FileUpload runat="server" ID="fuArchivo" class="btn btn-upload col-md-9 col-sm-9 col-xs-9"/>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="table-responsive">
                            <asp:GridView ID="grdAdjuntos" runat="server" 
                                AllowPaging="true"
                                PageSize="10"
                                OnPageIndexChanging="grdAdjuntos_PageIndexChanging"
                                OnRowCommand="grdAdjuntos_RowCommand"
                                AutoGenerateColumns="False" CssClass="table table-striped table-bordered">
                                <Columns>
                                    <asp:BoundField DataField="NombreArchivo" HeaderText="Nombre Archivo" SortExpression="NombreArchivo" Visible="True" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <div class="pull-right">
                                                <asp:LinkButton ID="btnArchAdjuntoVer" runat="server" CommandName="Ver" CommandArgument='<%#Eval("PathArchivo") %>'
                                                    CssClass="btn btn-round"> <i class="fa fa-file"></i> Ver</asp:LinkButton>
                                                <asp:LinkButton ID="btnArchAdBorrar" runat="server" CommandName="Borrar" CommandArgument='<%#Eval("PathArchivo") %>'
                                                    CssClass="btn btn-round"> <i class="fa fa-eraser"></i> Borrar</asp:LinkButton>
                                            </div>
                                        </ItemTemplate>
                                        <ItemStyle Width="10%" />
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle CssClass="pgr" />
                            </asp:GridView>
                        </div>
                    </div>
                    
                </div>
            </div>




            <%-- Historial el Caso de Soporte --%>
            <div class="x_panel" runat="server" id="pnlHistorial" visible="false">
                <div class="x_title">
                    <h2>
                        <asp:Literal ID="Literal1" runat="server" Text="Historial de Comentarios del Caso" /></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <asp:GridView ID="grdHistorial" runat="server" 
                            AllowPaging="true"
                            PageSize="20"
                            OnPageIndexChanging="grdHistorial_PageIndexChanging"
                            AutoGenerateColumns="False" CssClass="table table-striped table-bordered">
                            <Columns>
                                <asp:BoundField DataField="Id" HeaderText="ID" SortExpression="Id" Visible="True" />
                                <asp:BoundField DataField="Fecha" HeaderText="Fecha" SortExpression="Fecha" Visible="True" />
                                <asp:BoundField DataField="Comentario" HeaderText="Comentario" SortExpression="Comentario" Visible="True" />
                                <asp:BoundField DataField="oEstado.Nombre" HeaderText="Estado" SortExpression="IdEstado" />
                                <asp:BoundField DataField="oResolucion.Nombre" HeaderText="Resolucion" SortExpression="IdResolucion" />
                                <asp:BoundField DataField="oUsuario.ApellidoYNombre2" HeaderText="Responsable" SortExpression="IdUsuario" />
                            </Columns>
                            <PagerStyle CssClass="pgr" />
                        </asp:GridView>
                    </div>
                    <div class="form-group">
                        <%--RESPONSABLE--%>
                        <asp:Label runat="server" Text="Comentario:" class="control-label col-md-1 col-sm-1 col-xs-6"></asp:Label>
                        <div class="col-md-5 col-sm-5 col-xs-6">
                            <asp:TextBox ID="txtHistorialComentario" runat="server" class="form-control" Width="100%"></asp:TextBox>
                        </div>
                        <%--ESTADO DEL CASO--%>
                        <asp:Label runat="server" Text="Estado:" class="control-label col-md-1 col-sm-1 col-xs-6"></asp:Label>
                        <div class="col-md-2 col-sm-2 col-xs-6">
                            <asp:DropDownList runat="server" ID="ddlHistorialEstado" CssClass="form-control" Width="100%">
                            </asp:DropDownList>
                        </div>
                        <%--RESOLUCION--%>
                        <asp:Label runat="server" Text="Resolucion:" class="control-label col-md-1 col-sm-1 col-xs-6"></asp:Label>
                        <div class="col-md-2 col-sm-2 col-xs-6">
                            <asp:DropDownList runat="server" ID="ddlHIstorialResolucion" CssClass="form-control" Width="100%">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="pull-right">
                            <button type="submit" id="btnGrabarHistorial" runat="server" causesvalidation="False" class="btn btn-round btn-primary" onserverclick="btnGrabarHistorial_ServerClick">
                                <i class="fa fa-check"></i>Grabar
                            </button>
                            <button type="submit" id="btnHistorialCancelar" runat="server" causesvalidation="False" class="btn btn-round btn-alert" onserverclick="btnHistorialCancelar_ServerClick">
                                <i class="fa fa-eraser"></i>Volver
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <usrError:Error ID="errorMensaje" runat="server" />
    <uc1:userProcesando runat="server" ID="userProcesando" />
</asp:Content>

