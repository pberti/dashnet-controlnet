﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IOT.ORM;
using IOT.Domain;
using System.Transactions;
using System.Web.Security;
using System.Data;
using IOT.Services;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class Application_Produccion_Productos_Default : PageBase
{
    #region Propiedades & Variables
    private ServProducto oServProducto = Services.Get<ServProducto>();
    private ServComponentes oServComponentes = Services.Get<ServComponentes>();
    private ServTipoPerfilMenues oServPerfilMenus = Services.Get<ServTipoPerfilMenues>();
    #endregion

    #region "Main"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (base.IdUsuarioLogueado == -1)
        {
            Response.Redirect("~/Application/Login/Default.aspx", false);
            return;
        }
        if (!Page.IsPostBack)
            InicializacionPantalla();
    }
    private void InicializacionPantalla()
    {
        CargoPermisosdePantalla();
        DireccionOrdenacion = string.Empty;
        CargoCombo();
        this.CargoGrilla("Nombre ascending", this.ObtenerFiltro());

        lvlValNombre.Text = "";
    }

    private void CargoPermisosdePantalla()
    {
        var tipoPerfilMenu = oServPerfilMenus.TraerTodosTipoPefilMenuesPorPerfil(IdPerfilUsuarioLogueado).Where(p => p.oMenu.PathPagina.Contains("/Produccion/MaestroProductos"));
        if (tipoPerfilMenu.Count() != 0)
        {
            List<TipoPerfilMenues> t = tipoPerfilMenu.ToList();
            btnAgregar.Visible = t.First().PuedeGrabar;
            btnGrabar.Visible = t.First().PuedeGrabar;
            btnGrabarComponente.Visible = t.First().PuedeGrabar;
            btnBorrarComponente.Visible = t.First().PuedeBorrar;

        }
        else
            Response.Redirect("~/Application/Default.aspx", false);

    }

    private void CargoCombo()
    {
    }
    #endregion

    #region "Métodos - Generales Pantalla"
    private void VisibilidadPaneles(bool pPnlGrilla, bool pPnlDetalle)
    {
        pnlGrilla.Visible = pPnlGrilla;
        pnlDetalles.Visible = pPnlDetalle;
        if (!pPnlDetalle)
        {
            pnlComponentes.Visible = false;
        }
    }
    /// <summary>
    ///  Metodo que carga los datos del usercontrol de usrUltimaModificacion
    /// </summary>
    private void CargarUserControl(long pIdUsuarioMofifico, DateTime pFechaUltimaModificacion)
    {
        usrUltimaModificacion1.FecUltimaModificacion = pFechaUltimaModificacion.ToString();
        usrUltimaModificacion1.IdUsuarioModifico = pIdUsuarioMofifico;
        usrUltimaModificacion1.UltimaModificacionBind();
    }
    #endregion

    #region Metodos - Grilla

    /// <summary>
    /// Metodo que obtiene los datos establecidos de los filtros.
    /// </summary>
    /// <returns>Metodo que devuelve un objeto con los valores del filtro.</returns>
    private TipoProducto ObtenerFiltro()
    {
        TipoProducto oObj = null;
        return oObj;
    }

    /// <summary>
    /// Metodo que obtiene el ordenamiento de las columnas.
    /// </summary>
    private void ObtenerDireccionOrden(GridViewSortEventArgs evt)
    {
        if (DireccionOrdenacion == "")
        {
            DireccionOrdenacion = "ascending";
        }
        else
        {
            if (DireccionOrdenacion == "ascending")
                DireccionOrdenacion = "descending";
            else
                DireccionOrdenacion = "ascending";
        }
    }


    /// <summary>
    /// Metodo que cargs los datos en la grilla de la pagina.
    /// </summary>
    /// <param name="pOrderBy">Criterio de ordenamiento de las columnas</param>
    /// <param name="pObj">Objeto Viaje para determinar si existe algun filtro a aplicar a la grilla</param>
    private void CargoGrilla(string pOrderBy, TipoProducto pObj)
    {
        this.VisibilidadPaneles(true, false);
        CriterioOrdenacion = pOrderBy;
        List<TipoProducto> wQuery = null;
        try
        {
            if (pObj == null)
            {
                wQuery = oServProducto.TraerTodos().OrderBy(pOrderBy).ToList();
            }
            else
            {
                wQuery = oServProducto.TraerTodos().OrderBy(pOrderBy).ToList();
            }

            grdGrilla.DataSource = wQuery;
            grdGrilla.DataBind();
            lblCantidadFilas.Text = wQuery.Count().ToString();
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargoGrilla", HttpContext.Current.Request.Url.LocalPath);
        }
        finally
        {
            wQuery = null;
        }
    }

    protected void grdGrilla_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdGrilla.PageIndex = e.NewPageIndex;
        this.CargoGrilla(CriterioOrdenacion, this.ObtenerFiltro());
    }

    protected void grdGrilla_Sorting(object sender, GridViewSortEventArgs e)
    {
        this.ObtenerDireccionOrden(e);
        this.CargoGrilla(string.Format("{0} {1}", e.SortExpression, DireccionOrdenacion), this.ObtenerFiltro());
    }

    protected void grdGrilla_RowCommand(Object sender, CommandEventArgs e)
    {
        if (e.CommandName == "Editar")
        {
            base.EntityId = Convert.ToInt64(e.CommandArgument);
            base.EsNuevo = false;
            lblId.Text = EntityId.ToString();
            litNombre.Text = "Editar Tipo Producto";
            this.CargarControles();
            usrUltimaModificacion1.Visible = true;
            InicializoCamposComponentes();
            this.VisibilidadPaneles(false, true);
        }
    }
    #endregion

    #region "Eventos Botones"

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        try
        {
            this.CargoGrilla("Nombre ascending", this.ObtenerFiltro());

        }
        catch (SystemException ex)
        {
            errorMensaje.ErrorBind(ex, "btnBuscar_Click", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    protected void btnAgregar_Click(object sender, EventArgs e)
    {
        TituloForm = "Nuevo Producto";
        lblId.Text = "0";
        this.CrearNuevo();
    }

    protected void btnGrabar_ServerClick(object sender, EventArgs e)
    {
        Page.Validate();
        if (Page.IsValid)
        {
            this.GrabarDatos();
        }
    }

    protected void btnSalir_ServerClick(object sender, EventArgs e)
    {
        this.CargoGrilla("Nombre ascending", this.ObtenerFiltro());
        Limpiar();
        this.VisibilidadPaneles(true, false);
    }

    #endregion

    #region Metodos - Vista Detalle
    /// <summary>
    /// Metodo que carga los datos de los controles.
    /// </summary>
    private void CargarControles()
    {
        try
        {
            var oObj = oServProducto.TraerUnicoXId(base.EntityId);

            txtNombre.Text = oObj.Nombre;
            txtDescripcion.Text = oObj.Descripcion;
            txtCodigoProducto.Text = oObj.CodigoProducto;
            chkInactivo.Checked = oObj.Inactivo;            
            if (chkInactivo.Checked)
            {
                pnlInactivo.Visible = true;
                txtInactivoFecha.Text = oObj.InactivoFecha.ToShortDateString();
                txtInactivoMotivo.Text = oObj.InactivoComentario;
            }
            else
            {
                pnlInactivo.Visible = false;
                txtInactivoFecha.Text = "";
                txtInactivoMotivo.Text = "";
            }

            this.CargarUserControl(oObj.IdUsuarioModifico, oObj.FecUltimaModificacion);

            //Cargo la grilla con los Componentes relacionados
            pnlComponentes.Visible = true;
            CargoGrillaComponentes();
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargarControles", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    /// <summary>
    /// Metodo que guarda los datos establecidos en la pagina.
    /// </summary>
    private void GrabarDatos()
    {
        try
        {
            TipoProducto oObj = ObtenerDatos();

            if (ValidoObjeto(oObj))
            //if (base.ValidateModel(oObj, ref this.frmDetalles))
            {
                if (EsNuevo)
                {
                    EntityId = oServProducto.Agregar(oObj);
                    lblId.Text = EntityId.ToString();
                    //INICIALIZO LA SECCION DE COMPONENTES
                    InicializoCamposComponentes();
                }
                else
                {
                    oServProducto.Actualizar(oObj);
                    this.CargoGrilla(base.CriterioOrdenacion, this.ObtenerFiltro());
                }
            }
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "GrabarDatos", HttpContext.Current.Request.Url.LocalPath);
        }
    }



    /// <summary>
    /// Obtiene los datos de la pagina
    /// </summary>
    /// <returns>Retorna un objecto con los datos establecidos en la pagina</returns>
    private TipoProducto ObtenerDatos()
    {
        TipoProducto oObj = null;
        try
        {
            oObj = new TipoProducto();
            var oUsuario = base.TraerUsuarioLogeado();
            if (!EsNuevo)
            {
                oObj = oServProducto.TraerUnicoXId(base.EntityId);
            }

            oObj.Nombre = txtNombre.Text.Trim();
            oObj.Descripcion = txtDescripcion.Text;
            oObj.Inactivo = chkInactivo.Checked;
            oObj.CodigoProducto = txtCodigoProducto.Text;

            if (chkInactivo.Checked)
            {
                oObj.InactivoFecha = Convert.ToDateTime(txtInactivoFecha.Text);
                oObj.InactivoComentario = txtInactivoMotivo.Text;
            }
            else
            {
                oObj.InactivoComentario = "";
            }
            oObj.IdUsuarioModifico = oUsuario.Id;
            oObj.FecUltimaModificacion = DateTime.Now;
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "ObtenerDatos", HttpContext.Current.Request.Url.LocalPath);
        }
        return oObj;
    }

    /// <summary>
    /// Metodo que establece los datos en la pagina para crear un nuevo objecto TipoPerfil
    /// </summary>
    private void CrearNuevo()
    {
        this.Limpiar();
        litNombre.Text = "Nuevo Tipo de Producto";
        usrUltimaModificacion1.Visible = false;
        this.VisibilidadPaneles(false, true);
        EsNuevo = true;
    }

    /// <summary>
    /// Metodo que limpia los datos establecidos en la pagina.
    /// </summary>
    private void Limpiar()
    {
        txtCodigoProducto.Text = string.Empty;
        txtNombre.Text = string.Empty;
        txtDescripcion.Text = string.Empty;
        txtInactivoMotivo.Text = string.Empty;
        chkInactivo.Checked = false;
        chkInactivo_CheckedChanged(null, null);
        pnlError.Visible = false;

        //SECCION COMPONENTES
        InicializoCamposComponentes();
        pnlComponentes.Visible = false;
    }

    #endregion

    #region Validacion de Campos
    /// <summary>
    /// Metodo que valida el tramite a cargar
    /// </summary>
    private bool ValidoObjeto(TipoProducto pObj)
    {
        bool comodin = true;
        pnlError.Visible = false;
        string error = string.Empty;
        
        if (pObj.Nombre.Trim().Length == 0)
        {
            error = "Debe ingresar un Nombre Valido <br>";
            comodin = false;
        }
        else
        {
            if (EsNuevo)
            {
                //Valido que no existe un producto con ese nombre
                TipoProducto oObj = oServProducto.TraerTodos().Where(p => p.Nombre == pObj.Nombre.Trim()).FirstOrDefault();
                if (oObj != null)
                {
                    error = error + "Ya Existe un Producto con ese Nombre, por favor reintente <br>";
                    comodin = false;
                }
            }
            else
            {
                List<TipoProducto> oObj = oServProducto.TraerTodos().Where(p => p.Nombre == pObj.Nombre.Trim()).ToList();
                oObj.RemoveAll(p => p.Id == pObj.Id);
                if (oObj.Count() != 0)
                {
                    error = error + "Ya Existe un Producto con ese Nombre, por favor reintente <br>";
                    comodin = false;
                }
            }
        }

        if (txtCodigoProducto.Text.Length <=0 || txtCodigoProducto.Text.Length > 3)
        {
            error = error + "Ingrese un codigo de producto de hasta tres caracteres <br>";
            comodin = false;
        }

        if (!string.IsNullOrEmpty(error))
        {
            pnlError.Visible = true;
            litError.Text = error;
        }

        return comodin;
    }
    #endregion

    #region Eventos AutoPostBack
    protected void chkInactivo_CheckedChanged(object sender, EventArgs e)
    {
        if (chkInactivo.Checked)
        {
            pnlInactivo.Visible = true;
            txtInactivoMotivo.Text = "";
            txtInactivoFecha.Text = "";
        }
        else
        {
            pnlInactivo.Visible = false;
            txtInactivoMotivo.Text = "";
            txtInactivoFecha.Text = "";
        }

    }
    #endregion

    #region Relacion con Componentes

    #region Eventos de la Grilla
    private void CargoGrillaComponentes()
    {
        List<TipoProducto_Componente> wQuery = null;
        try
        {
            long pIdProducto = Convert.ToInt64(lblId.Text);
            wQuery = oServProducto.TraerTodosCompXProducto(pIdProducto).ToList();
            grdComponentes.DataSource = wQuery;
            grdComponentes.DataBind();
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargoGrillaComponentes", HttpContext.Current.Request.Url.LocalPath);
        }
        finally
        {
            wQuery = null;
        }
    }

    protected void grdComponentes_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdGrilla.PageIndex = e.NewPageIndex;
        this.CargoGrillaComponentes();
    }

    protected void grdComponentes_Sorting(object sender, GridViewSortEventArgs e)
    {
        this.CargoGrillaComponentes();
    }

    protected void grdComponentes_RowCommand(Object sender, CommandEventArgs e)
    {
        if (e.CommandName == "Editar")
        {
            
            InicializoCamposComponentes();
            lblIdComponente.Text = e.CommandArgument.ToString();
            this.CargarControlesComponentes();
        }
    }
    #endregion

    #region Eventos de los Campos y del Formulario y Los botones

    private void CargarControlesComponentes()
    {
        TipoProducto_Componente oObj = new TipoProducto_Componente();
        oObj = oServProducto.TraerUnicoXIdComp(Convert.ToInt64(lblIdComponente.Text));
        txtCantidad.Text = oObj.Cantidad.ToString();
        txtComentario.Text = oObj.Comentario;
        //Como el combo no tiene los componentes que ya estan seleccionados, cargo el DDL solo con el componente.
        CargoDDLComponentes(false, oObj.IdComponente);
        ddlComponente.SelectedValue = oObj.IdComponente.ToString();
    }
    private void InicializoCamposComponentes()
    {
        lblIdComponente.Text = "0";
        txtCantidad.Text = "1";
        txtComentario.Text = "";
        CargoDDLComponentes(true, 0);
        pnlComponentes.Visible = true;
        CargoGrillaComponentes();
    }


    private void CargoDDLComponentes(bool pTodos, long pIdComponente)
    {
        //Cargo los componentes
        ddlComponente.Items.Clear();
        List<TipoComponentes> wQuery;
        if (pTodos)
        { 
            wQuery = oServComponentes.TraerTodos().Where(p => p.Inactivo == false).OrderBy(p => p.Nombre).ToList();
            if (lblId.Text != "0")
            {
                //Si ya tengo cargado algun Producto Verifico de no poner en el compo componentes qye ya tengan
                //Traigo los componentes que ya tiene ese producto
                var wQuery2 = oServProducto.TraerTodosCompXProducto(Convert.ToInt64(lblId.Text));
                if (wQuery2 != null)
                {
                    foreach (var item in wQuery2)
                    {
                        TipoComponentes oObj = oServComponentes.TraerUnicoXId(item.IdComponente);
                        wQuery.Remove(oObj);
                    }
                }

            }
        }
        else
        {
            //Cargo El componente que seleccione
            wQuery = oServComponentes.TraerTodos().Where(p => p.Inactivo == false && p.Id == pIdComponente).OrderBy(p => p.Nombre).ToList();
        }

        ddlComponente.DataSource = wQuery;
        ddlComponente.DataTextField = "Nombre";
        ddlComponente.DataValueField = "Id";
        ddlComponente.DataBind();
    }
    protected void btnBorrarComponente_ServerClick(object sender, EventArgs e)
    {
        //Saco el id del componente y borro
        TipoProducto_Componente oObj = new TipoProducto_Componente();
        oServProducto.BorrarComp(Convert.ToInt64(lblIdComponente.Text));
        InicializoCamposComponentes();
    }

    protected void btnLimpiarComponente_ServerClick(object sender, EventArgs e)
    {
        InicializoCamposComponentes();
        pnlErrorComponentes.Visible = false;
    }

    protected void btnGrabarComponente_ServerClick(object sender, EventArgs e)
    {
        try
        {
            TipoProducto_Componente oObj = new TipoProducto_Componente();
            oObj.Id = Convert.ToInt64(lblIdComponente.Text);
            oObj.IdComponente = Convert.ToInt64(ddlComponente.SelectedValue);
            oObj.IdProducto = Convert.ToInt64(lblId.Text);
            oObj.Cantidad = Convert.ToInt32(txtCantidad.Text);
            oObj.Comentario = txtComentario.Text.Trim();

            oObj.IdUsuarioModifico = base.IdUsuarioLogueado;
            oObj.FecUltimaModificacion = DateTime.Now;

            if (ValidoObjetoComponente(oObj))
            {
                if (oObj.Id == 0)
                {
                    long Id = oServProducto.AgregarComp(oObj);
                }
                else
                {
                    oServProducto.ActualizarComp(oObj);
                }
                InicializoCamposComponentes();
                this.CargoGrillaComponentes();
            }
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "GrabarDatos", HttpContext.Current.Request.Url.LocalPath);
        }
    }
    private bool ValidoObjetoComponente(TipoProducto_Componente pObj)
    {
        bool comodin = true;
        pnlErrorComponentes.Visible = false;
        string error = string.Empty;

        if (int.Parse(txtCantidad.Text) <= 0)
        {
            comodin = false;
            error = "Elija una cantidad valida <br>";
        }

        if(!string.IsNullOrEmpty(error))
        {
            pnlErrorComponentes.Visible = true;
            LitErrorComponentes.Text = error;
        }

        return comodin;
    }
    #endregion
    #endregion

}