﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IOT.ORM;
using IOT.Domain;
using System.Transactions;
using System.Web.Security;
using System.Data;
using IOT.Services;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class Application_Produccion_Productos_Componentes_Default : PageBase
{

    #region Propiedades & Variables
    private ServComponentes oServComponentes = Services.Get<ServComponentes>();
    private ServTipoPerfilMenues oServPerfilMenus = Services.Get<ServTipoPerfilMenues>();
    #endregion

    #region "Main"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (base.IdUsuarioLogueado == -1)
        {
            Response.Redirect("~/Application/Login/Default.aspx", false);
            return;
        }
        if (!Page.IsPostBack)
            InicializacionPantalla();
    }
    private void InicializacionPantalla()
    {
        CargoPermisosdePantalla();
        DireccionOrdenacion = string.Empty;
        CargoCombo();
        this.CargoGrilla("Nombre ascending", this.ObtenerFiltro());
    }

    private void CargoPermisosdePantalla()
    {
        var tipoPerfilMenu = oServPerfilMenus.TraerTodosTipoPefilMenuesPorPerfil(IdPerfilUsuarioLogueado).Where(p => p.oMenu.PathPagina.Contains("/Produccion/MaestroProductos/MaestroComponentes"));
        if (tipoPerfilMenu.Count() != 0)
        {
            List<TipoPerfilMenues> t = tipoPerfilMenu.ToList();
            btnAgregar.Visible = t.First().PuedeGrabar;
            btnGrabar.Visible = t.First().PuedeGrabar;
        }
        else
            Response.Redirect("~/Application/Default.aspx", false);

    }

    private void CargoCombo()
    {
    }
    #endregion

    #region "Métodos - Generales Pantalla"
    private void VisibilidadPaneles(bool pPnlGrilla, bool pPnlDetalle)
    {
        pnlGrilla.Visible = pPnlGrilla;
        pnlDetalles.Visible = pPnlDetalle;
    }
    /// <summary>
    ///  Metodo que carga los datos del usercontrol de usrUltimaModificacion
    /// </summary>
    private void CargarUserControl(long pIdUsuarioMofifico, DateTime pFechaUltimaModificacion)
    {
        usrUltimaModificacion1.FecUltimaModificacion = pFechaUltimaModificacion.ToString();
        usrUltimaModificacion1.IdUsuarioModifico = pIdUsuarioMofifico;
        usrUltimaModificacion1.UltimaModificacionBind();
    }
    #endregion

    #region Metodos - Grilla

    /// <summary>
    /// Metodo que obtiene los datos establecidos de los filtros.
    /// </summary>
    /// <returns>Metodo que devuelve un objeto con los valores del filtro.</returns>
    private TipoComponentes ObtenerFiltro()
    {
        TipoComponentes oObj = null;
        return oObj;
    }

    /// <summary>
    /// Metodo que obtiene el ordenamiento de las columnas.
    /// </summary>
    private void ObtenerDireccionOrden(GridViewSortEventArgs evt)
    {
        if (DireccionOrdenacion == "")
        {
            DireccionOrdenacion = "ascending";
        }
        else
        {
            if (DireccionOrdenacion == "ascending")
                DireccionOrdenacion = "descending";
            else
                DireccionOrdenacion = "ascending";
        }
    }


    /// <summary>
    /// Metodo que cargs los datos en la grilla de la pagina.
    /// </summary>
    /// <param name="pOrderBy">Criterio de ordenamiento de las columnas</param>
    /// <param name="pObj">Objeto Viaje para determinar si existe algun filtro a aplicar a la grilla</param>
    private void CargoGrilla(string pOrderBy, TipoComponentes pObj)
    {
        this.VisibilidadPaneles(true, false);
        CriterioOrdenacion = pOrderBy;
        List<TipoComponentes> wQuery = null;
        try
        {
            if (pObj == null)
            {
                wQuery = oServComponentes.TraerTodos().OrderBy(pOrderBy).ToList();
            }
            else
            {
                wQuery = oServComponentes.TraerTodos().OrderBy(pOrderBy).ToList();
            }

            grdGrilla.DataSource = wQuery;
            grdGrilla.DataBind();
            lblCantidadFilas.Text = wQuery.Count().ToString();
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargoGrilla", HttpContext.Current.Request.Url.LocalPath);
        }
        finally
        {
            wQuery = null;
        }
    }

    protected void grdGrilla_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdGrilla.PageIndex = e.NewPageIndex;
        this.CargoGrilla(CriterioOrdenacion, this.ObtenerFiltro());
    }

    protected void grdGrilla_Sorting(object sender, GridViewSortEventArgs e)
    {
        this.ObtenerDireccionOrden(e);
        this.CargoGrilla(string.Format("{0} {1}", e.SortExpression, DireccionOrdenacion), this.ObtenerFiltro());
    }

    protected void grdGrilla_RowCommand(Object sender, CommandEventArgs e)
    {
        if (e.CommandName == "Editar")
        {
            base.EntityId = Convert.ToInt64(e.CommandArgument);
            base.EsNuevo = false;
            litNombre.Text = "Editar Tipo Componente";
            lblId.Text = EntityId.ToString();
            this.CargarControles();
            usrUltimaModificacion1.Visible = true;
            this.VisibilidadPaneles(false, true);
        }
    }
    #endregion

    #region "Eventos Botones"

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        try
        {
            this.CargoGrilla("Nombre ascending", this.ObtenerFiltro());

        }
        catch (SystemException ex)
        {
            errorMensaje.ErrorBind(ex, "btnBuscar_Click", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    protected void btnAgregar_Click(object sender, EventArgs e)
    {
        TituloForm = "Nuevo Tipo Componente";
        this.CrearNuevo();
    }

    protected void btnGrabar_ServerClick(object sender, EventArgs e)
    {
        Page.Validate();
        if (Page.IsValid)
        {
            this.GrabarDatos();
        }
    }

    protected void btnSalir_ServerClick(object sender, EventArgs e)
    {
        this.VisibilidadPaneles(true, false);
        Limpiar();
    }

    #endregion

    #region Metodos - Vista Detalle
    /// <summary>
    /// Metodo que carga los datos de los controles.
    /// </summary>
    private void CargarControles()
    {
        try
        {
            var oObj = oServComponentes.TraerUnicoXId(base.EntityId);

            txtNombre.Text = oObj.Nombre;
            txtDescripcion.Text = oObj.Descripcion;
            chkInactivo.Checked = oObj.Inactivo;
            chkRequiereNroSerie.Checked = oObj.RequiereNroSerie;
            if (chkInactivo.Checked)
            {
                pnlInactivo.Visible = true;
                txtInactivoFecha.Text = oObj.InactivoFecha.ToShortDateString();
                txtInactivoMotivo.Text = oObj.InactivoComentario;
            }
            else
            {
                pnlInactivo.Visible = false;
                txtInactivoFecha.Text = "";
                txtInactivoMotivo.Text = "";
            }

            this.CargarUserControl(oObj.IdUsuarioModifico, oObj.FecUltimaModificacion);
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargarControles", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    /// <summary>
    /// Metodo que guarda los datos establecidos en la pagina.
    /// </summary>
    private void GrabarDatos()
    {
        try
        {
            TipoComponentes oObj = ObtenerDatos();

            if (ValidoObjeto(oObj))
            //if (base.ValidateModel(oObj, ref this.frmDetalles))
            {
                if (EsNuevo)
                {
                    EntityId = oServComponentes.Agregar(oObj);
                }
                else
                {
                    oServComponentes.Actualizar(oObj);
                }
                this.CargoGrilla(base.CriterioOrdenacion, this.ObtenerFiltro());
            }
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "GrabarDatos", HttpContext.Current.Request.Url.LocalPath);
        }
    }



    /// <summary>
    /// Obtiene los datos de la pagina
    /// </summary>
    /// <returns>Retorna un objecto con los datos establecidos en la pagina</returns>
    private TipoComponentes ObtenerDatos()
    {
        TipoComponentes oObj = null;
        try
        {
            oObj = new TipoComponentes();
            var oUsuario = base.TraerUsuarioLogeado();
            if (!EsNuevo)
            {
                oObj = oServComponentes.TraerUnicoXId(base.EntityId);
            }

            oObj.Nombre = txtNombre.Text.Trim();
            oObj.Descripcion = txtDescripcion.Text;
            oObj.RequiereNroSerie = chkRequiereNroSerie.Checked;
            oObj.Inactivo = chkInactivo.Checked;

            if (chkInactivo.Checked)
            {
                oObj.InactivoFecha = Convert.ToDateTime(txtInactivoFecha.Text);
                oObj.InactivoComentario = txtInactivoMotivo.Text;
            }
            else
            {
                oObj.InactivoComentario = "";
            }
            oObj.IdUsuarioModifico = oUsuario.Id;
            oObj.FecUltimaModificacion = DateTime.Now;
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "ObtenerDatos", HttpContext.Current.Request.Url.LocalPath);
        }
        return oObj;
    }

    /// <summary>
    /// Metodo que establece los datos en la pagina para crear un nuevo objecto TipoPerfil
    /// </summary>
    private void CrearNuevo()
    {
        this.Limpiar();
        litNombre.Text = "Nuevo Tipo Componente";
        usrUltimaModificacion1.Visible = false;
        this.VisibilidadPaneles(false, true);
        EsNuevo = true;
    }

    /// <summary>
    /// Metodo que limpia los datos establecidos en la pagina.
    /// </summary>
    private void Limpiar()
    {
        
        txtNombre.Text = string.Empty;
        txtDescripcion.Text = string.Empty;
        txtInactivoMotivo.Text = string.Empty;
        chkInactivo.Checked = false;
        chkRequiereNroSerie.Checked = false;
        chkInactivo_CheckedChanged(null, null);
        pnlError.Visible = false;

    }

    #endregion

    #region Validacion de Campos
    /// <summary>
    /// Metodo que valida el tramite a cargar
    /// </summary>
    private bool ValidoObjeto(TipoComponentes pObj)
    {
        bool comodin = true;
        pnlError.Visible = false;
        string error = string.Empty;

        if (pObj.Nombre.Trim().Length == 0)
        {
            error = "Debe ingresar un Nombre Valido <br>";
            comodin = false;
        }
        else
        {
            if (EsNuevo)
            {
                //Valido que no existe un componente con ese nombre
                TipoComponentes oObj = oServComponentes.TraerTodos().Where(p => p.Nombre == pObj.Nombre.Trim()).FirstOrDefault();
                if (oObj != null)
                {
                    error = error + "Ya Existe un componente con ese Nombre, por favor reintente <br>";
                    comodin = false;
                }
               
            }
            else
            {
                List<TipoComponentes> oObj = oServComponentes.TraerTodos().Where(p => p.Nombre == pObj.Nombre.Trim()).ToList();
                oObj.RemoveAll(t => t.Id == pObj.Id);
                if (oObj.Count != 0)
                {
                    error = error + "Ya Existe un componente con ese Nombre, por favor reintente <br>";
                    comodin = false;
                }
            }
        }

        //Valido que se haya puesto un comentario y una fecha si el registro esta inactivo
        if (pObj.Inactivo == true)
        {
            if ((pObj.InactivoComentario.Trim().Length == 0 ) || (pObj.InactivoFecha == null) )
            {
                error = error + "Debe ingresarse una Fecha y un Commentario <br>";
                comodin = false;
            }
        }

        if (!string.IsNullOrEmpty(error))
        {
            pnlError.Visible = true;
            litError.Text = error;
        }

        return comodin;
    }
    #endregion

    #region Eventos AutoPostBack
    protected void chkInactivo_CheckedChanged(object sender, EventArgs e)
    {
        if (chkInactivo.Checked)
        {
            pnlInactivo.Visible = true;
            txtInactivoMotivo.Text = "";
            txtInactivoFecha.Text = "";
        }
        else
        {
            pnlInactivo.Visible = false;
            txtInactivoMotivo.Text = "";
            txtInactivoFecha.Text = "";
        }

    }

    #endregion
}