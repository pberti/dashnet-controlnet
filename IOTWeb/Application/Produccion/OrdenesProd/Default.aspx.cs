﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IOT.Domain;
using System.Data;
using IOT.Services;

public partial class Application_Produccion_OrdenesProd_Default : PageBase
{

    #region Propiedades & Variables
    private ServOrdenProduccion oServOP = Services.Get<ServOrdenProduccion>();
    private ServProducto oServProd = Services.Get<ServProducto>();
    private ServUsuario oServUsu = Services.Get<ServUsuario>();
    private ServTipoPerfilMenues oServPerfilMenus = Services.Get<ServTipoPerfilMenues>();
    private ServWorkflow oServWorkflow = Services.Get<ServWorkflow>();
    private ServProduccion oServProduccion = Services.Get<ServProduccion>();
    #endregion

    #region "Main"

    protected void Page_Load(object sender, EventArgs e)
    {
        if (base.IdUsuarioLogueado == -1)
        {
            Response.Redirect("~/Application/Login/Default.aspx", false);
            return;
        }
        if (!Page.IsPostBack)
            InicializacionPantalla();
    }

    private void InicializacionPantalla()
    {
        CargoPermisosdePantalla();
        DireccionOrdenacion = string.Empty;
        CargoCombo();
        VisibilidadPaneles(true, false);
        CargoGrilla("FechaPedido Descending", this.ObtenerFiltro());
    }

    private void CargoPermisosdePantalla()
    {
        var tipoPerfilMenu = oServPerfilMenus.TraerTodosTipoPefilMenuesPorPerfil(IdPerfilUsuarioLogueado).Where(p => p.oMenu.PathPagina.Contains("/Produccion/OrdenesProd"));
        if (tipoPerfilMenu.Count() != 0)
        {
            List<TipoPerfilMenues> t = tipoPerfilMenu.ToList();
            btnAgregar.Visible = t.First().PuedeGrabar;
            btnGrabar.Visible = t.First().PuedeGrabar;
            btnGrabarProducto.Visible = t.First().PuedeGrabar;
            btnGrabarNovedad.Visible = t.First().PuedeGrabar;
            btnGrabarEntr.Visible = t.First().PuedeGrabar;
            btnBorrarProducto.Visible = t.First().PuedeBorrar;
        }
        else
            Response.Redirect("~/Application/Default.aspx", false);

    }
    private void CargoCombo()
    {
    }
    #endregion

    #region Metodos Generales Orden de Produccion
    #region "Métodos - Generales Pantalla"
    private void VisibilidadPaneles(bool pPnlGrilla, bool pPnlDetalle)
    {
        pnlGrilla.Visible = pPnlGrilla;
        pnlDetalles.Visible = pPnlDetalle;
        pnlSuccess.Visible = false;

    }
    /// <summary>
    ///  Metodo que carga los datos del usercontrol de usrUltimaModificacion
    /// </summary>
    private void CargarUserControl(long pIdUsuarioMofifico, DateTime pFechaUltimaModificacion)
    {
        usrUltimaModificacion1.FecUltimaModificacion = pFechaUltimaModificacion.ToString();
        usrUltimaModificacion1.IdUsuarioModifico = pIdUsuarioMofifico;
        usrUltimaModificacion1.UltimaModificacionBind();
    }
    #endregion

    #region Metodos - Grilla

    /// <summary>
    /// Metodo que obtiene los datos establecidos de los filtros.
    /// </summary>
    /// <returns>Metodo que devuelve un objeto con los valores del filtro.</returns>
    private OrdenProd ObtenerFiltro()
    {
        OrdenProd oObj = null;
        if (txtFiltroNroPedidoExterno.Text != string.Empty)
        {
            if (oObj == null)
                oObj = new OrdenProd();
            oObj.NroPedidoExterno = txtFiltroNroPedidoExterno.Text.Trim();
        }
        if (txtFiltroNroPedidoInterno.Text != string.Empty)
        {
            if (oObj == null)
                oObj = new OrdenProd();
            oObj.NroPedidoInterno = txtFiltroNroPedidoInterno.Text.Trim();
        }
        if (txtFiltroSolicitadoPor.Text != string.Empty)
        {
            if (oObj == null)
                oObj = new OrdenProd();
            oObj.SolicitadoPor = txtFiltroSolicitadoPor.Text.Trim();
        }
        return oObj;
    }

    /// <summary>
    /// Metodo que obtiene el ordenamiento de las columnas.
    /// </summary>
    private void ObtenerDireccionOrden(GridViewSortEventArgs evt)
    {
        if (DireccionOrdenacion == "")
        {
            DireccionOrdenacion = "ascending";
        }
        else
        {
            if (DireccionOrdenacion == "ascending")
                DireccionOrdenacion = "descending";
            else
                DireccionOrdenacion = "ascending";
        }
    }


    /// <summary>
    /// Metodo que cargs los datos en la grilla de la pagina.
    /// </summary>
    /// <param name="pOrderBy">Criterio de ordenamiento de las columnas</param>
    /// <param name="pObj">Objeto Viaje para determinar si existe algun filtro a aplicar a la grilla</param>
    private void CargoGrilla(string pOrderBy, OrdenProd pObj)
    {
        pnlErroresGrilla.Visible = false;
        CriterioOrdenacion = pOrderBy;
        List<OrdenProd> wQuery = null;
        try
        {
            if (pObj == null)
            {
                wQuery = oServOP.TraerTodos().OrderBy(pOrderBy).OrderBy(p => p.Nombre).ToList();
            }
            else
            {
                wQuery = oServOP.TraerTodosFiltro(pObj).OrderBy(pOrderBy).OrderBy(p => p.Nombre).ToList();
            }

            grdGrilla.DataSource = wQuery;
            grdGrilla.DataBind();
            lblCantidadFilas.Text = wQuery.Count().ToString();
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargoGrilla", HttpContext.Current.Request.Url.LocalPath);
        }
        finally
        {
            wQuery = null;
        }
    }

    protected void grdGrilla_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdGrilla.PageIndex = e.NewPageIndex;
        this.CargoGrilla(CriterioOrdenacion, this.ObtenerFiltro());
    }

    protected void grdGrilla_Sorting(object sender, GridViewSortEventArgs e)
    {
        this.ObtenerDireccionOrden(e);
        this.CargoGrilla(string.Format("{0} {1}", e.SortExpression, DireccionOrdenacion), this.ObtenerFiltro());
    }

    protected void grdGrilla_RowCommand(Object sender, CommandEventArgs e)
    {
        if (e.CommandName == "Editar")
        {
            base.EntityId = Convert.ToInt64(e.CommandArgument);
            base.EsNuevo = false;
            lblId.Text = EntityId.ToString();
            litNombre.Text = "Editar Orden de Produccion";
            this.CargarControles();
            usrUltimaModificacion1.Visible = true;
            this.VisibilidadPaneles(false, true);
            HabilitarPanelesTab();
        }
        if (e.CommandName == "Ver")
        {
            base.EntityId = Convert.ToInt64(e.CommandArgument);
            List<Produccion> comprobar = oServProduccion.TraerTodos().Where(p => p.IdOrdenProduccion == EntityId).ToList();
            if (comprobar.Count() != 0)
            {
                Session["Op"] = EntityId;
                Response.Redirect("~/Application/Produccion/Produccion/Default.aspx?Bandera=1", false);
            }
            else
            {
                pnlErroresGrilla.Visible = true;
                litErrorGrilla.Text = "Debe Iniciar la produccion antes";
            }
        }
        if (e.CommandName == "Dashboard")
        {
            base.EntityId = Convert.ToInt64(e.CommandArgument);
            List<Produccion> comprobar = oServProduccion.TraerTodos().Where(p => p.IdOrdenProduccion == EntityId).ToList();
            if (comprobar.Count() != 0)
            {
                Session["Op"] = EntityId;
                Response.Redirect("~/Application/Produccion/OrdenesProd/TableroDeControlOP/Default.aspx", false);
            }
            else
            {
                pnlErroresGrilla.Visible = true;
                litErrorGrilla.Text = "Debe Iniciar la produccion antes";
            }
        }
    }
    #endregion

    #region "Eventos Botones"

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        try
        {
            this.CargoGrilla("Nombre ascending", this.ObtenerFiltro());
            userProcesando.CerrarModalProcesando();

        }
        catch (SystemException ex)
        {
            errorMensaje.ErrorBind(ex, "btnBuscar_Click", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    protected void btnAgregar_Click(object sender, EventArgs e)
    {
        TituloForm = "Nueva Orden de Produccion";
        lblId.Text = "0";
        CrearNuevo();
        base.EsNuevo = true;
        this.InicializoCamposProductos();
        this.HabilitarPanelesTab();
        btnIniciarProduccion.Visible = false;
    }

    protected void btnGrabar_Click(object sender, EventArgs e)
    {
        Page.Validate();
        if (Page.IsValid)
        {
            GrabarDatos();
            userProcesando.CerrarModalProcesando();
        }
    }


    protected void btnSalir_Click(object sender, EventArgs e)
    {
        this.CargoGrilla("FechaPedido Descending", this.ObtenerFiltro());
        this.VisibilidadPaneles(true, false);
        HabilitarPanelesTab();
    }

    protected void btnIniciarProduccion_Click(object sender, EventArgs e)
    {
        Page.Validate();
        if (Page.IsValid)
        {
            IniciarProduccion();
            userProcesando.CerrarModalProcesando();
            if (!pnlError.Visible)
            {
                btnIniciarProduccion.Visible = false;
            }
        }

    }

    #endregion

    #region Metodos - Vista Detalle
    /// <summary>
    /// Metodo que carga los datos de los controles.
    /// </summary>
    private void CargarControles()
    {
        try
        {
            var oObj = oServOP.TraerUnicoXId(base.EntityId);

            txtNombre.Text = oObj.Nombre;
            txtDescripcion.Text = oObj.Descripcion;
            txtSolicitadoPor.Text = oObj.SolicitadoPor;
            txtNroPedInterno.Text = oObj.NroPedidoInterno;
            txtNroPedExterno.Text = oObj.NroPedidoExterno;
            CargoDDLEstadoOp();
            if (oObj.oEstado.Nombre != "Sin Iniciar")
            {
                btnIniciarProduccion.Visible = false;
            }
            if (oObj.oEstado.Nombre == "Iniciada")
            {
                ddlEstadoOp.Items.RemoveAt(0);
            }
            ddlEstadoOp.Items.FindByText(oObj.oEstado.Nombre).Selected = true;

            this.CargarUserControl(oObj.IdUsuarioModifico, oObj.FecUltimaModificacion);

        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargarControles", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    /// <summary>
    /// Metodo que guarda los datos establecidos en la pagina.
    /// </summary>
    private void GrabarDatos()
    {
        try
        {
            OrdenProd oObj = ObtenerDatos();

            if (ValidoObjeto(oObj))
            {
                if (EsNuevo)
                {
                    EntityId = oServOP.Agregar(oObj);
                    lblId.Text = EntityId.ToString();
                    EsNuevo = false;
                }
                else
                {
                    oServOP.Actualizar(oObj);
                    this.CargoGrilla(base.CriterioOrdenacion, this.ObtenerFiltro());
                }
                this.HabilitarPanelesTab();
                pnlSuccess.Visible = true;
                litSuccess.Text = "Se guardaron los datos correctamente";
            }
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "GrabarDatos", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    private void GeneroNroInternoOP()
    {
        OrdenProd op = oServOP.TraerUnicoXId(EntityId);
        txtNroPedInterno.Text = op.NroPedidoInterno;
    }

    /// <summary>
    /// Obtiene los datos de la pagina
    /// </summary>
    /// <returns>Retorna un objecto con los datos establecidos en la pagina</returns>
    private OrdenProd ObtenerDatos()
    {
        OrdenProd oObj = null;
        try
        {
            oObj = new OrdenProd();
            var oUsuario = base.TraerUsuarioLogeado();
            if (!EsNuevo)
            {
                oObj = oServOP.TraerUnicoXId(base.EntityId);
            }

            oObj.Nombre = txtNombre.Text.Trim();
            oObj.NroPedidoExterno = txtNroPedExterno.Text;

            oObj.SolicitadoPor = txtSolicitadoPor.Text;
            oObj.Descripcion = txtDescripcion.Text;
            oObj.IdUsuarioModifico = oUsuario.Id;
            oObj.FecUltimaModificacion = DateTime.Now;
            oObj.Estado = Convert.ToInt64(ddlEstadoOp.SelectedValue).ToString();
            if (EsNuevo)
            {
                oObj.FechaPedido = DateTime.Now;
            }
            oObj.NroPedidoInterno = txtNroPedInterno.Text;
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "ObtenerDatos", HttpContext.Current.Request.Url.LocalPath);
        }
        return oObj;
    }

    /// <summary>
    /// Metodo que establece los datos en la pagina para crear un nuevo objecto Orden de produccion
    /// </summary>
    private void CrearNuevo()
    {
        this.Limpiar();
        litNombre.Text = "Nueva Orden de Produccion";
        usrUltimaModificacion1.Visible = false;
        this.VisibilidadPaneles(false, true);
        EsNuevo = true;
        txtSolicitadoPor.Text = "";
        CargoDDLEstadoOp();
    }

    private void HabilitarPanelesTab()
    {
        bool habilitar = true;
        long pIdOrden = Convert.ToInt64(lblId.Text);
        if (pIdOrden == 0)
            habilitar = false;

        pnlTabEntregas.Visible = habilitar;
        pnlTabProductos.Visible = habilitar;
        pnlTabNovedades.Visible = habilitar;

        if (habilitar)
        {
            InicializoCamposProductos();
            InicializoCamposEntregas();
            InicializoCamposNovedades();
            btnBorrarProducto.Visible = ControlarEstado(pIdOrden);
            btnGrabarProducto.Visible = ControlarEstado(pIdOrden);

            List<OpProductos> productos = oServOP.TraerTodosOPP().Where(p => p.IdOP == pIdOrden).ToList();
            if (productos.Count != 0)
                btnIniciarProduccion.Visible = ControlarEstado(pIdOrden);
        }
    }

    /// <summary>
    /// Metodo que limpia los datos establecidos en la pagina.
    /// </summary>
    protected void Limpiar()
    {
        txtNombre.Text = string.Empty;
        txtDescripcion.Text = string.Empty;
        txtNroPedInterno.Text = string.Empty;
        txtNroPedExterno.Text = string.Empty;
        txtSolicitadoPor.Text = string.Empty;
        pnlError.Visible = false;
        pnlSuccess.Visible = false;
        pnlErrorEntregas.Visible = false;
        pnlErrorProd.Visible = false;
    }

    #endregion

    #region Metodos ddl
    protected void CargoDDLEstadoOp()
    {

        ddlEstadoOp.Items.Clear();
        List<OpEstados> wQuery;

        wQuery = oServOP.TraerTodosOPEstados().ToList();


        ddlEstadoOp.DataSource = wQuery;
        ddlEstadoOp.DataTextField = "Nombre";
        ddlEstadoOp.DataValueField = "Id";
        ddlEstadoOp.DataBind();
    }

    #endregion

    #region Validacion de Campos
    /// <summary>
    /// Metodo que valida la orden a cargar
    /// </summary>
    private bool ValidoObjeto(OrdenProd pObj)
    {
        pnlError.Visible = false;
        bool comodin = true;
        string error = string.Empty;

        if (pObj.Nombre.Trim().Length == 0)
        {
            error = "Debe ingresar un Nombre Valido <br>";
            comodin = false;
        }
        else
        {
            if (EsNuevo)
            {
                //Valido que no existe una op con ese nombre
                OrdenProd oObj = oServOP.TraerTodos().Where(p => p.Nombre == pObj.Nombre.Trim()).FirstOrDefault();
                if (oObj != null)
                {
                    error = error + "Ya Existe una Orden de Produccion con ese Nombre, por favor reintente <br>";
                    comodin = false;
                }
            }
            else
            {
                List<OrdenProd> oObj = oServOP.TraerTodos().Where(p => p.Nombre == pObj.Nombre).ToList();
                oObj.RemoveAll(p => p.Id == pObj.Id);
                if (oObj.Count != 0)
                {
                    error = error + "Ya existe una orden con ese Nombre <br>";
                    comodin = false;
                }
            }
        }

        if (pObj.SolicitadoPor.Trim().Length == 0)
        {
            error = error + "Ingrese el nombre de quien solicito esta orden<br>";
            comodin = false;
        }

        if (EsNuevo)
        {
            if (!string.IsNullOrEmpty(pObj.NroPedidoExterno))
            {
                OrdenProd oObj = oServOP.TraerTodos().Where(p => p.NroPedidoInterno == pObj.NroPedidoInterno).FirstOrDefault();
                if (oObj != null)
                {
                    error = error + "Ya Existe una Orden de Produccion con ese numero Interno, por favor reintente <br>";
                    comodin = false;
                }

                oObj = oServOP.TraerTodos().Where(p => p.NroPedidoExterno == pObj.NroPedidoExterno).FirstOrDefault();
                if (oObj != null)
                {
                    error = error + "Ya Existe una Orden de Produccion con ese numero Externo, por favor reintente <br>";
                    comodin = false;
                }
            }
        }
        else
        {
            if (!string.IsNullOrEmpty(pObj.NroPedidoExterno))
            {
                List<OrdenProd> oObj = oServOP.TraerTodos().Where(p => p.NroPedidoInterno == pObj.NroPedidoInterno).ToList();
                oObj.RemoveAll(p => p.Id == pObj.Id);
                if (oObj.Count != 0)
                {
                    error = error + "Ya existe una orden con ese numero interno <br>";
                    comodin = false;
                }

                oObj.Clear();
                oObj = oServOP.TraerTodos().Where(p => p.NroPedidoExterno == pObj.NroPedidoExterno).ToList();
                oObj.RemoveAll(p => p.Id == pObj.Id);
                if (oObj.Count != 0)
                {
                    error = error + "Ya existe una orden con ese numero externo <br>";
                    comodin = false;
                }
            }
        }

        if (!string.IsNullOrEmpty(error))
        {
            pnlError.Visible = true;
            litError.Text = error;
        }

        return comodin;
    }
    #endregion

    #region Iniciar Produccion
    private void IniciarProduccion()
    {
        try
        {
            pnlSuccess.Visible = false;
            long idOP = Convert.ToInt64(lblId.Text);
            List<OpProductos> productos = oServOP.TraerTodosOPP().Where(p => p.IdOP == idOP).ToList();
            oServProduccion.CrearProducciones(productos, IdUsuarioLogueado);

            /// Actualizo el estado de la OP.
            CargoDDLEstadoOp();
            ddlEstadoOp.Items.FindByText("Iniciada").Selected = true;
            ddlEstadoOp.Items.RemoveAt(0);

            GrabarDatos();
            pnlSuccess.Visible = true;
            litSuccess.Text = "Se inicio la produccion";
        }
        catch (Exception ex)
        {
            pnlError.Visible = true;
            litError.Text = "No se pudo iniciar la produccion";
            errorMensaje.ErrorBind(ex, "Error en el metodo Iniciar Produccion", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    #endregion

    #endregion

    #region Productos

    #region Eventos de la Grilla
    private void CargoGrillaProductos()
    {
        List<OpProductos> wQuery = null;
        try
        {
            long pIdOrden = Convert.ToInt64(lblId.Text);
            wQuery = oServOP.TraerTodosProdxOrden(pIdOrden).ToList();
            grdProductos.DataSource = wQuery;
            grdProductos.DataBind();
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargoGrillaProductos", HttpContext.Current.Request.Url.LocalPath);
        }
        finally
        {
            wQuery = null;
        }
    }

    protected void grdProductos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdProductos.PageIndex = e.NewPageIndex;
        this.CargoGrillaProductos();
    }

    protected void grdProductos_Sorting(object sender, GridViewSortEventArgs e)
    {
        this.CargoGrillaProductos();
    }

    protected void grdProductos_RowCommand(Object sender, CommandEventArgs e)
    {
        if (e.CommandName == "Editar")
        {

            InicializoCamposProductos();
            lblIdProducto.Text = e.CommandArgument.ToString();
            this.CargarControlesProductos();
        }
    }
    #endregion

    #region Eventos Botones Productos
    protected void btnBorrarProducto_Click(object sender, EventArgs e)
    {
        //Saco el id del Producto y borro
        OpProductos oObj = new OpProductos();
        oServOP.BorrarOPP(Convert.ToInt64(lblIdProducto.Text));
        InicializoCamposProductos();
    }

    protected void btnLimpiarProducto_Click(object sender, EventArgs e)
    {
        InicializoCamposProductos();
        pnlErrorProd.Visible = false;
    }

    protected void btnGrabarProducto_Click(object sender, EventArgs e)
    {
        Page.Validate();
        if (Page.IsValid)
        {
            GrabarDatosProductos();
            userProcesando.CerrarModalProcesando();
        }

    }

    #endregion

    #region Eventos de los Campos y del Formulario
    private void CargarControlesProductos()
    {
        OpProductos oObj = new OpProductos();
        oObj = oServOP.TraerUnicoXIdOPP(Convert.ToInt64(lblIdProducto.Text));
        txtCantidad.Text = oObj.Cantidad.ToString();
        txtComentario.Text = oObj.Comentario;
        txtOpcionMontaje.Text = oObj.OpcionMontaje;
        //Como el combo no tiene los productos que ya estan seleccionados, cargo el DDL solo con el productos.
        CargoDDLProductos(false, oObj.IdMaestroProducto);
        CargoDDLWorkflow("");
        ddlWorkflow.Items.FindByText(oObj.oWorkflow.Nombre).Selected = true;
        ddlProducto.SelectedValue = oObj.IdMaestroProducto.ToString();
        CargarDDLFiltro();
    }

    private void InicializoCamposProductos()
    {
        CargarDDLFiltro();
        lblIdProducto.Text = "0";
        txtCantidad.Text = "1";
        txtComentario.Text = string.Empty;
        txtOpcionMontaje.Text = string.Empty;
        CargoDDLProductos(true, 0);
        CargoGrillaProductos();
        CargoDDLWorkflow("");
        ddlWorkflow.Items.FindByText("").Selected = true;
        if (grdProductos.Rows.Count != 0)
            btnIniciarProduccion.Visible = true;
        else
            btnIniciarProduccion.Visible = false;

    }

    protected void GrabarDatosProductos()
    {
        try
        {
            OpProductos oObj = new OpProductos();
            ObtenerDatosProducto(oObj);
            if (ValidoObjetoProducto(oObj))
                if (oObj.Id == 0)
                {
                    long Id = oServOP.AgregarOPP(oObj);
                }
                else
                {
                    oServOP.ActualizarOPP(oObj);
                }
            InicializoCamposProductos();

        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "GrabarDatos", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    protected void ObtenerDatosProducto(OpProductos oObj)
    {
        oObj.Id = Convert.ToInt64(lblIdProducto.Text);
        if (ddlProducto.Text.Trim().Length != 0)
            oObj.IdMaestroProducto = Convert.ToInt64(ddlProducto.SelectedValue);
        oObj.IdOP = Convert.ToInt64(lblId.Text);
        oObj.Cantidad = Convert.ToInt32(txtCantidad.Text);
        oObj.Comentario = txtComentario.Text.Trim();
        oObj.IdUsuarioModifico = base.IdUsuarioLogueado;
        oObj.FecUltimaModificacion = DateTime.Now;
        oObj.OpcionMontaje = txtOpcionMontaje.Text;
        if (ddlWorkflow.Text.Trim().Length != 0)
            oObj.IdWorkflow = Convert.ToInt64(ddlWorkflow.SelectedValue);


    }
    #endregion

    #region Metodos DDL Productos
    private void CargoDDLProductos(bool pTodos, long pIdProducto)
    {
        //Cargo los Productos
        ddlProducto.Items.Clear();
        List<TipoProducto> wQuery;
        if (pTodos)
        {
            wQuery = oServProd.TraerTodos().OrderBy(p => p.Nombre).ToList();
            if (lblId.Text != "0")
            {
                //Si ya tengo cargado algun Producto Verifico de no poner en el combo productos que ya tengan
                //Traigo los Productos que ya tiene esa orden
                var wQuery2 = oServOP.TraerTodosProdxOrden(Convert.ToInt64(lblId.Text));
                if (wQuery2 != null)
                {
                    foreach (var item in wQuery2)
                    {
                        TipoProducto oObj = oServProd.TraerUnicoXId(item.IdMaestroProducto);
                        wQuery.RemoveAll(p => p.Id != oObj.Id);
                    }
                }

            }
        }
        else
        {
            //Cargo El producto que seleccione
            wQuery = oServProd.TraerTodos().Where(p => p.Inactivo == false && p.Id == pIdProducto).OrderBy(p => p.Nombre).ToList();
        }

        ddlProducto.DataSource = wQuery;
        ddlProducto.DataTextField = "Nombre";
        ddlProducto.DataValueField = "Id";
        ddlProducto.DataBind();
    }

    protected void CargoDDLWorkflow(string destino)
    {
        ddlWorkflow.Items.Clear();
        List<Workflow> wQuery;

        if (string.IsNullOrEmpty(destino))
        {
            wQuery = oServWorkflow.TraerTodos(null).ToList();
        }
        else
            wQuery = oServWorkflow.TraerTodos(null).Where(w => w.Destino == destino).ToList();

        ddlWorkflow.DataSource = wQuery;
        ddlWorkflow.DataTextField = "Nombre";
        ddlWorkflow.DataValueField = "Id";
        ddlWorkflow.DataBind();

        ddlWorkflow.Items.Add("");
    }
    #endregion

    #region Valido Producto
    protected bool ValidoObjetoProducto(OpProductos oObj)
    {
        bool correcto = true;
        pnlErrorProd.Visible = false;
        string error = string.Empty;

        if (oObj.IdMaestroProducto == 0)
        {
            correcto = false;
            error = "Elija un producto valido <br>";
        }
        if (oObj.Cantidad <= 0)
        {
            correcto = false;
            error = error + "Elija una cantidad valida <br>";
        }

        if (oObj.IdWorkflow == 0)
        {
            correcto = false;
            error = error + "Ingrese un Workflow valido <br>";
        }

        if (txtOpcionMontaje.Text.Length == 0 || txtOpcionMontaje.Text.Length > 1)
        {
            correcto = false;
            error = error + "Ingrese una Opción de Montaje valida <br>";
        }

        if (!string.IsNullOrEmpty(error))
        {
            pnlErrorProd.Visible = true;
            LiteralProd.Text = error;
        }

        return correcto;
    }
    #endregion

    #region ControlEstados
    private bool ControlarEstado(long idOp)
    {
        var op = oServOP.TraerUnicoXId(idOp);
        return op.oEstado.AceptaModificaciones;
    }
    #endregion

    #endregion

    #region Entregas
    #region Eventos de la Grilla
    private void CargoGrillaEntregas()
    {
        List<OpEntregas> wQuery = null;
        try
        {
            long pIdOrden = Convert.ToInt64(lblId.Text);
            wQuery = oServOP.TraerTodasEntregasxOrden(pIdOrden).ToList();

            grdEntregas.DataSource = wQuery;
            grdEntregas.DataBind();
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargoGrillaEntregas", HttpContext.Current.Request.Url.LocalPath);
        }
        finally
        {
            wQuery = null;
        }
    }

    protected void grdEntregas_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdEntregas.PageIndex = e.NewPageIndex;
        this.CargoGrillaEntregas();
    }

    protected void grdEntregas_Sorting(object sender, GridViewSortEventArgs e)
    {
        this.CargoGrillaEntregas();
    }

    protected void grdEntregas_RowCommand(Object sender, CommandEventArgs e)
    {
        if (e.CommandName == "Editar")
        {
            lblIdEntrega.Text = e.CommandArgument.ToString();
            CargarControlesEntregas();
            this.CargoGrillaEntregas();
        }
    }
    #endregion

    #region Eventos de los Campos y del Formulario y Los botones

    private void CargarControlesEntregas()
    {
        OpEntregas oObj = new OpEntregas();
        oObj = oServOP.TraerUnicoXIdOPE(Convert.ToInt64(lblIdEntrega.Text));
        txtComentarioEntr.Text = oObj.Comentario;
        if (oObj.FechaEstimada != null)
            txtFechaEstimada.Text = oObj.FechaEstimadaSinHora;
        if (oObj.FechaReal != DateTime.MinValue)
        {
            txtFechaReal.Text = oObj.FechaRealSinHora;
            txtComentarioCierreEntr.Text = oObj.ComentarioCierre;
        }
        CargoDDLEstadoEntr();
        if (oObj.oEstadoEntrega != null)
            ddlEstadoEntrega.Items.FindByText(oObj.oEstadoEntrega.Nombre).Selected = true;
        else
            ddlEstadoEntrega.SelectedValue = "Sin Iniciar";
        if (oObj.IdusuarioCerro != 0)
            txtUsuarioCierre.Text = oObj.oUsuario.ApellidoYNombre2;


    }
    private void InicializoCamposEntregas()
    {
        lblIdEntrega.Text = "0";
        txtComentarioEntr.Text = "";
        txtComentarioCierreEntr.Text = "";
        txtFechaReal.Text = "";
        txtFechaReal.Enabled = false;
        txtUsuarioCierre.Text = "";
        txtUsuarioCierre.Enabled = false;
        txtFechaEstimada.Text = "";
        CargoDDLEstadoEntr();
        ddlEstadoEntrega.Items.FindByText("Sin Iniciar").Selected = true;
        CargoGrillaEntregas();

    }



    private void CargoDDLEstadoEntr()
    {
        ddlEstadoEntrega.Items.Clear();
        List<OpEntregasEstado> wQuery;

        wQuery = oServOP.TraerTodosOPEntregaEstados().ToList();


        ddlEstadoEntrega.DataSource = wQuery;
        ddlEstadoEntrega.DataTextField = "Nombre";
        ddlEstadoEntrega.DataValueField = "Id";
        ddlEstadoEntrega.DataBind();
    }

    protected void btnLimpiarEntr_Click(object sender, EventArgs e)
    {
        InicializoCamposEntregas();
        pnlErrorEntregas.Visible = false;
    }

    protected void btnGrabarEntr_Click(object sender, EventArgs e)
    {
        try
        {
            OpEntregas oObj = new OpEntregas();
            ObtenerDatosEntregas(oObj);
            if (ValidoObjetoEntrega(oObj))
            {
                if (oObj.Id == 0)
                {
                    long Id = oServOP.AgregarOPE(oObj);
                }
                else
                {
                    oServOP.ActualizarOPE(oObj);
                }
            }
            InicializoCamposEntregas();
            this.CargoGrillaEntregas();
            userProcesando.CerrarModalProcesando();
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "GrabarDatos", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    protected void ObtenerDatosEntregas(OpEntregas oObj)
    {
        oObj.Id = Convert.ToInt64(lblIdEntrega.Text);
        oObj.IdOP = Convert.ToInt64(lblId.Text);
        if (!string.IsNullOrEmpty(txtComentarioEntr.Text))
            oObj.Comentario = txtComentarioEntr.Text.Trim();
        if (!string.IsNullOrEmpty(txtFechaEstimada.Text))
            oObj.FechaEstimada = Convert.ToDateTime(txtFechaEstimada.Text);

        oObj.EstadoEntrega = Convert.ToInt64(ddlEstadoEntrega.SelectedValue).ToString();

        if (oObj.Id != 0)
        {
            OpEntregasEstado estado = oServOP.TraerUnicoXIdOPEntregaEstado(Convert.ToInt64(ddlEstadoEntrega.SelectedValue));
            if (estado.EstadoFinal)
            {
                oObj.IdusuarioCerro = IdUsuarioLogueado;
                oObj.ComentarioCierre = txtComentarioCierreEntr.Text.Trim();
                oObj.FechaReal = DateTime.Now;
            }
        }
    }

    protected bool ValidoObjetoEntrega(OpEntregas oObj)
    {
        bool correcto = true;
        pnlErrorEntregas.Visible = false;
        string error = string.Empty;

        if (string.IsNullOrEmpty(oObj.Comentario))
        {
            error = "Ingrese un comentario <br>";
            correcto = false;
        }

        if (oObj.FechaEstimada == DateTime.MinValue)
        {
            error = error + "Ingrese una Fecha Estimida <br>";
            correcto = false;
        }

        if (!string.IsNullOrEmpty(error))
        {
            pnlErrorEntregas.Visible = true;
            LiteralEntrega.Text = error;
        }

        return correcto;
    }

    #endregion
    #endregion

    #region Novedades
    #region Eventos de la Grilla
    private void CargoGrillaNovedades()
    {
        List<OpNovedades> wQuery = null;
        try
        {
            long pIdOrden = Convert.ToInt64(lblId.Text);
            wQuery = oServOP.TraerTodasNovedadesXOP(pIdOrden).ToList();

            grdNovedades.DataSource = wQuery;
            grdNovedades.DataBind();
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargoGrillaNovedades", HttpContext.Current.Request.Url.LocalPath);
        }
        finally
        {
            wQuery = null;
        }
    }

    protected void grdNovedades_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdNovedades.PageIndex = e.NewPageIndex;
        this.CargoGrillaNovedades();
    }

    protected void grdNovedades_Sorting(object sender, GridViewSortEventArgs e)
    {
        this.CargoGrillaNovedades();
    }

    protected void grdNovedades_RowCommand(Object sender, CommandEventArgs e)
    {
        if (e.CommandName == "Editar")
        {
            lblIdNovedad.Text = e.CommandArgument.ToString();
            CargarControlesNovedades();
            this.CargoGrillaNovedades();
        }
    }
    #endregion

    #region Eventos de los Campos y del Formulario y Los botones

    private void CargarControlesNovedades()
    {
        OpNovedades oObj = new OpNovedades();
        oObj = oServOP.TraerUnicoXIdOPN(Convert.ToInt64(lblIdNovedad.Text));
        txtComentarioNovedad.Text = oObj.Comentario;
        if (oObj.Fecha != null)
            txtFechaNovedad.Text = oObj.FechaSinHora;


    }
    private void InicializoCamposNovedades()
    {
        lblIdNovedad.Text = "0";
        txtComentarioNovedad.Text = "";
        txtFechaNovedad.Text = "";
        CargoGrillaNovedades();

    }



    protected void btnLimpiarNovedad_Click(object sender, EventArgs e)
    {
        InicializoCamposNovedades();
    }

    protected void btnGrabarNovedad_Click(object sender, EventArgs e)
    {
        try
        {
            OpNovedades oObj = new OpNovedades();
            var oUsuario = base.TraerUsuarioLogeado();

            oObj.Id = Convert.ToInt64(lblIdEntrega.Text);
            oObj.IdOP = Convert.ToInt64(lblId.Text);
            oObj.Fecha = Convert.ToDateTime(txtFechaNovedad.Text);
            oObj.Comentario = txtComentarioNovedad.Text.Trim();
            oObj.IdUsuarioModifico = oUsuario.Id;
            oObj.FecUltimaModificacion = DateTime.Now;

            if (oObj.Id == 0)
            {
                long Id = oServOP.AgregarOPN(oObj);
            }
            else
            {
                oServOP.ActualizarOPN(oObj);
            }
            InicializoCamposNovedades();
            this.CargoGrillaNovedades();
            userProcesando.CerrarModalProcesando();
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "GrabarDatos", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    #endregion
    #endregion

    private void CargarDDLFiltro()
    {
        ddlFiltroDestino.Items.Clear();

        ddlFiltroDestino.Items.Add("");
        ddlFiltroDestino.Items.Add("Produccion");
        ddlFiltroDestino.Items.Add("Servicio Técnico");
    }

    protected void btnBuscarWorkflow_Click(object sender, EventArgs e)
    {
        CargoDDLWorkflow(ddlFiltroDestino.SelectedItem.Text);
    }
}


