﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Application_Produccion_OrdenesProd_TableroDeControlOP_Default" %>

<%@ Register Src="~/UserControl/usrUltimaModificacion.ascx" TagName="usrUltimaModificacion" TagPrefix="uc1" %>
<%@ Register TagPrefix="usrError" TagName="Error" Src="~/UserControl/Error.ascx" %>
<%@ Register Src="~/UserControl/userProcesando.ascx" TagPrefix="uc1" TagName="userProcesando" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:Panel ID="pnlGrilla" runat="server">
        <div class="page-title">
            <div>
            </div>
            <div class="title_right">
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title" id="Titulo">
                        <h2>
                            <asp:Literal ID="LitNombre" runat="server" Text='<%#TituloForm%>' /></h2>
                        <div class="pull-right">
                            <asp:LinkButton runat="server" ID="btnExportarPDF" CssClass="btn btn-round btn-primary" OnClick="btnExportarPDF_Click">
                                                     <i class="fa fa-arrow-circle-down"></i> Exportar a PDF</asp:LinkButton>
                            <asp:LinkButton runat="server" ID="btnVolverOP" CssClass="btn btn-round btn-primary" OnClick="btnSalirOP_ServerClick">
                                                     <i class="fa fa-arrow-circle-left"></i> Volver</asp:LinkButton>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <%--Información General--%>
                    <div class="row tile_count" id="infoGral">
                        <div class="col-md-3 col-sm-3 col-xs-2 tile_stats_count">
                            <span class="count_top"><i class="fa fa-info-circle"></i>Produccion</span>
                            <div class="count green">
                                <asp:Label runat="server" ID="lblProduccion"></asp:Label>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3 tile_stats_count">
                            <span class="count_top"><i class="fa fa-info-circle"></i>Producto</span>
                            <div class="count green">
                                <asp:Label runat="server" ID="lblProducto"></asp:Label>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-2 tile_stats_count">
                            <span class="count_top"><i class="fa fa-info-circle"></i>Cantidad</span>
                            <div class="count green">
                                <asp:Label runat="server" ID="lblCantidadProd" CssClass="title"></asp:Label>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-2 tile_stats_count">
                            <span class="count_top"><i class="fa fa-info-circle"></i>Workflow</span>
                            <div class="count green">
                                <asp:Label runat="server" ID="lblWorkflow"></asp:Label>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-2 tile_stats_count">
                            <span class="count_top"><i class="fa fa-info-circle"></i>Estado</span>
                            <div class="count green">
                                <asp:Label runat="server" ID="lblEstado"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12" id="pnlA">
                            <%-- Gráfico Estado de los productos --%>
                            <div class="panel col-md-6 col-sm-6 col-xs-6" id="pnlGraficoC">
                                <div class="x_title">
                                    <h2>Estado de los productos</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Panel runat="server" class="col-md-12 col-sm-12 col-xs-12" ID="Panel1" Height="470" ScrollBars="Auto">
                                            <div class="col-md-7 col-sm-7 col-xs-12">
                                                <canvas id="chartDoughnutC"></canvas>
                                            </div>
                                            <div id="divDoughnutLegendsC" class="col-md-5 col-sm-5 col-xs-5">
                                            </div>
                                        </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            
                            <%-- Entregas--%>
                            <div class="panel col-md-6 col-sm-6 col-xs-6" id="pnlEntregas">
                                    <div class="x_title">
                                        <h2>Entregas</h2>
                                        <div class="clearfix"></div>
                                    </div>
                                    <asp:Panel runat="server" class="col-md-12 col-sm-12 col-xs-12" ID="pnlCuerpoEntregas" ScrollBars="Auto" Height="400">
                                        <div class="col-md-11 col-sm-11 col-xs-11 pull-right">
                                            <ul class="list-unstyled timeline">
                                                <asp:Repeater ID="rptEntregas" runat="server" Visible="true" OnItemDataBound="rptEntregas_ItemDataBound">
                                                    <ItemTemplate>
                                                        <li>
                                                            <asp:Panel runat="server" CssClass="panel body" ID="fondo">
                                                                <div class="col-md-1 col-sm-1 col-xs-1">
                                                                    <br />
                                                                    <h2 class="title">
                                                                        <a>
                                                                            <asp:Literal runat="server" /></a>
                                                                    </h2>
                                                                </div>
                                                                <div class="col-sm-11 col-xs-11">
                                                                    <p class="excerpt">
                                                                        <asp:Literal ID="LitEstadoEntrega" runat="server" />
                                                                        <asp:Literal ID="LitFechaEntrega" runat="server" />
                                                                        <asp:Literal ID="LitFechaRealEntrega" runat="server" />
                                                                        <asp:Literal ID="LitComentario" runat="server" />
                                                                    </p>
                                                                </div>
                                                            </asp:Panel>
                                                            <div class="clearfix"></div>
                                                        </li>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </ul>
                                        </div>
                                    </asp:Panel>
                                </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12" id="pnlB">
                            <div class="form-group">
                                <%-- Gráfico de dona B --%>
                                <div class="panel col-md-6 col-sm-6 col-xs-6" id="pnlGraficoB">
                                    <div class="x_title">
                                        <h2>Productos por ejecutor</h2>
                                        <div class="clearfix"></div>
                                    </div>
                                    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:Panel runat="server" class="col-md-12 col-sm-12 col-xs-12" ID="pnlCuerpoGraficoB" Height="400" ScrollBars="Auto">
                                                <asp:LinkButton runat="server" ID="lbtnB" CssClass="btn btn-round btn-alert hidden" OnClick="lbtnBuscar_Click">
                                                              Buscar </asp:LinkButton>
                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                    <canvas id="chartDoughnutB"></canvas>
                                                </div>
                                                <div id="divDoughnutLegendsB" class="col-md-5 col-sm-5 col-xs-5">
                                                </div>
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <%-- Gráfico Estado de los productos por ejecutor --%>
                                <div class="panel col-md-6 col-sm-6 col-xs-6" id="pnlGrafico">
                                <div class="x_title">
                                    <h2>Estado de los productos por ejecutor</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="panel col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <asp:Label ID="Label1" runat="server" Text="Ejecutor:" class="control-label col-md-3 col-sm-3 col-xs-3"></asp:Label>
                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                    <asp:DropDownList runat="server" ID="ddlEjecutor" CssClass="form-control"></asp:DropDownList>
                                                </div>
                                                <div class="pull-right">
                                                    <asp:LinkButton runat="server" ID="lbtnGrafico" CssClass="btn btn-round btn-primary" OnClick="lbtnBuscar_Click">
                                                              <i class="fa fa-search"></i> Buscar </asp:LinkButton>
                                                </div>
                                            </div>
                                            <br />
                                            <br />
                                        </div>
                                        <div class="clearfix"></div>
                                        <asp:Panel runat="server" class="col-md-12 col-sm-12 col-xs-12" ID="pnlCuerpoGrafico" Height="400" ScrollBars="Auto">
                                            <asp:LinkButton runat="server" ID="lbtnBuscar" CssClass="btn btn-round btn-alert hidden" OnClick="lbtnBuscar_Click">
                                                              Buscar </asp:LinkButton>
                                            <div class="col-md-7 col-sm-7 col-xs-12">
                                                <canvas id="chartDoughnut"></canvas>
                                            </div>
                                            <div id="divDoughnutLegends" class="col-md-5 col-sm-5 col-xs-5">
                                            </div>
                                        </asp:Panel>
                                        <div class="clearfix"></div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>

    <usrError:Error ID="errorMensaje" runat="server" />
    <uc1:userProcesando runat="server" ID="userProcesando" />

    <script>
        // Para renderizar chartA
        function renderizarCharts(objChartDoughnut, objChartDoughnutB, objChartDoughnutC) {
            var ctxDoughnut = document.getElementById('chartDoughnut').getContext('2d');
            var chartDoughnut = new Chart(ctxDoughnut, objChartDoughnut);
            // Generamos las leyendas
            var divDoughnutLegends = document.getElementById("divDoughnutLegends");
            var legends = "";
            for (index in objChartDoughnut.data.labels) {
                legends += "<p><i class='fa fa-square' style='color: " +
                    objChartDoughnut.data.datasets[0].backgroundColor[index] + "'></i> " + objChartDoughnut.data.labels[index] + " </p>";
            }
            divDoughnutLegends.innerHTML = legends;
            // Para renderizar chartB
            var ctxDoughnutB = document.getElementById('chartDoughnutB').getContext('2d');
            var chartDoughnutB = new Chart(ctxDoughnutB, objChartDoughnutB);
            // Generamos las leyendas
            var divDoughnutLegendsB = document.getElementById("divDoughnutLegendsB");
            var legendsB = "";
            for (index in objChartDoughnutB.data.labels) {
                legendsB += "<p><i class='fa fa-square' style='color: " +
                    objChartDoughnutB.data.datasets[0].backgroundColor[index] + "'></i> " + objChartDoughnutB.data.labels[index] + " </p>";
            }
            divDoughnutLegendsB.innerHTML = legendsB;
            // Para renderizar chartC
            var ctxDoughnutC = document.getElementById('chartDoughnutC').getContext('2d');
            var chartDoughnutC = new Chart(ctxDoughnutC, objChartDoughnutC);
            // Generamos las leyendas
            var divDoughnutLegendsC = document.getElementById("divDoughnutLegendsC");
            var legendsC = "";
            for (index in objChartDoughnutC.data.labels) {
                legendsC += "<p><i class='fa fa-square' style='color: " +
                    objChartDoughnutC.data.datasets[0].backgroundColor[index] + "'></i> " + objChartDoughnutC.data.labels[index] + " </p>";
            }
            divDoughnutLegendsC.innerHTML = legendsC;
        }

        ///Método que se lanza tan pronto como se termina de cargar la página      
        window.onload = function () {
            document.getElementById('<%= lbtnBuscar.ClientID %>').click();
        };

            window.onload = function () {
                document.getElementById('<%= lbtnB.ClientID %>').click();
            };
    </script>
</asp:Content>
