﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using IOT.Domain;
using IOT.Services;
using System.Web;
using System.Web.UI;


public partial class Application_Produccion_OrdenesProd_PDF : System.Web.UI.Page
{
    #region Propiedades
    ServOrdenProduccion oServOP = Services.Get<ServOrdenProduccion>();
    ServWorkflow_Ruta oServRuta = Services.Get<ServWorkflow_Ruta>();
    ServProduccion oServProduccion = Services.Get<ServProduccion>();
    ServEjecutor oServEjecutor = Services.Get<ServEjecutor>();
    #endregion

    #region Main
    protected void Page_Load(object sender, EventArgs e)
    {
        InicializacionPantalla();
    }

    private void InicializacionPantalla()
    {
        long idOP = Convert.ToInt64(Request.QueryString["IdOP"]);
        OrdenProd OP = oServOP.TraerUnicoXId(idOP);
        LitNombre.Text = "Tablero de control orden de produccion: " + OP.NroPedidoInterno;
        CargoDDLEjecutor();
        CargarDatos(OP);
    }
    /// <summary>
    /// Metodo que carga la información de la OP
    /// </summary>
    private void CargarDatos(OrdenProd op)
    {
        List<OpProductos> productos = oServOP.TraerTodosProdxOrden(op.Id).ToList();
        lblProducto.Text = productos.FirstOrDefault().oTipoProducto.Nombre;

        int cantidad = 0;
        foreach (OpProductos p in productos)
        {
            cantidad += p.Cantidad;
        }
        lblCantidadProd.Text = cantidad.ToString();

        lblWorkflow.Text = productos.FirstOrDefault().oWorkflow.Nombre;

        lblEstado.Text = op.oEstado.Nombre;

        //Creo las listas que necesito para el grafico una con los estados del workflow y otra con las producciones
        List<Workflow_Estado> listEstados = new List<Workflow_Estado>();
        List<Workflow_Ruta> asist = oServRuta.TraerTodos().Where(r => r.IdWorkflow == productos.FirstOrDefault().IdWorkflow).ToList();
        foreach (Workflow_Ruta r in asist)
        {
            listEstados.Add(r.oWorkflowEstado);
        }


        List<Produccion> listProduccion = oServProduccion.TraerTodos().Where(p => p.IdOrdenProduccion == op.Id).ToList();


        List<Ejecutor> ejecutores = new List<Ejecutor>();
        foreach (Workflow_Estado e in listEstados)
        {
            if (ejecutores.Find(j => j.Id == e.IdEjecutor) == null)
                ejecutores.Add(e.oEjecutor);
        }

        string comodinB = ObtenerObjChartDoughnutB(ejecutores, listProduccion);
        string comodinC = ObtenerObjChartDoughnutRecType(listEstados, listProduccion);

        if (ddlEjecutor.SelectedItem.Text != "")
        {
            listEstados.RemoveAll(e => e.IdEjecutor != Convert.ToInt64(ddlEjecutor.SelectedValue));
            listProduccion.RemoveAll(p => p.oEstadoActual.IdEjecutor != Convert.ToInt64(ddlEjecutor.SelectedValue));
        }

        //Cargo el gráfico
        string comodin = ObtenerObjChartDoughnutRecType(listEstados, listProduccion);
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "",
        "renderizarCharts(" + comodin + "," + comodinB + "," + comodinC + ");", true);


        //Cargo los repeater
        CargarRptEntregas(op);

    }


    private void CargarRptEntregas(OrdenProd op)
    {
        List<OpEntregas> lista = oServOP.TraerTodasEntregasxOrden(op.Id).OrderBy(e => e.FechaEstimada).ToList();
        rptEntregas.DataSource = lista;
        rptEntregas.DataBind();
    }


    protected void rptEntregas_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        OpEntregas entrega = new OpEntregas();
        entrega = (OpEntregas)e.Item.DataItem;

        Panel fondo = new Panel();
        fondo = (Panel)e.Item.FindControl("fondo");
        if (entrega.oEstadoEntrega.Nombre == "Finalizada" || entrega.FechaEstimada.AddDays(-2) > DateTime.Today)
        {
            fondo.BackColor = System.Drawing.Color.Green;
        }
        else if (entrega.oEstadoEntrega.Nombre != "Finalizada" && (entrega.FechaEstimada >= DateTime.Today))
        {
            fondo.BackColor = System.Drawing.Color.Yellow;
        }
        else if (entrega.oEstadoEntrega.Nombre != "Finalizada")
        {
            fondo.BackColor = System.Drawing.Color.Red;
        }
        if (entrega.oEstadoEntrega.Nombre == "Finalizada" && entrega.FechaEstimada.Date < entrega.FechaReal.Date)
        {
            fondo.BackColor = System.Drawing.Color.Red;
        }
        if (entrega.oEstadoEntrega.Nombre == "Finalizada" && entrega.FechaEstimada.Date >= entrega.FechaReal.Date)
        {
            fondo.BackColor = System.Drawing.Color.Green;
        }

        Literal Fecha = new Literal();
        Fecha = (Literal)e.Item.FindControl("LitFechaEntrega");
        Fecha.Text = "Fecha Estimada:" + entrega.FechaEstimadaSinHora + "<br>";

        Literal Estado = new Literal();
        Estado = (Literal)e.Item.FindControl("LitEstadoEntrega");
        Estado.Text = "Estado: " + entrega.oEstadoEntrega.Nombre + "<br>";

        Literal FechaReal = new Literal();
        FechaReal = (Literal)e.Item.FindControl("LitFechaRealEntrega");
        if (entrega.IdusuarioCerro != 0)
            FechaReal.Text = "Fecha Real:" + entrega.FechaRealSinHora;
        else
            FechaReal.Text = "Fecha Real:";
    }
    #endregion

    #region Grafico Donout 1
    /// <summary>
    /// Método que genera el Obj. JavaScript necesario para renderizar un gráfico de Dona según una lista de instancias de MantenimientoDocumento
    /// Criterios que evalua: intancias de MantenimientoDocumento cumplimentados por tipo de Recurso (Contratista, Empleado o Vehiculo)
    /// </summary>
    /// <param name="lMantDocs">Lista de instancias de MantenimientoDocumento</param>
    private string ObtenerObjChartDoughnutRecType(List<Workflow_Estado> listEstados, List<Produccion> listProduccion)
    {
        string JSObj = string.Empty;
        try
        {
            //Tipos a graficar
            string types = "['";
            //Cantidades a graficar
            string cantidades = "['";
            // Colores para graficar. Separamos por '.' porque desde JavaScript, al aplicar 'split()' no podremos separar por ',' 
            // debido a que obtendríamos un resultado incorrectos dado que el formato de una cadena de valores rgba se separan también por ','. 
            string colores = "['";

            for (int i = 0; i < listEstados.Count - 1; i++)
            {
                int num = listProduccion.Where(p => p.IdEstadoActual == listEstados[i].Id).Count();
                types = types + listEstados[i].Nombre + " - ";
                cantidades = cantidades + num + "','";
                types = types + num + "','";
                colores = colores + ValoresConstantes.listaColores[i] + "','";
            }

            int cantidad = listProduccion.Where(p => p.IdEstadoActual == listEstados.Last().Id).Count();
            types = types + listEstados.Last().Nombre + " - " + cantidad + "']";
            cantidades = cantidades + cantidad + "']";
            colores = colores + ValoresConstantes.listaColores[listEstados.Count] + "']";

            // por el momento realizaremos el obj JS como un string de c#.
            // pero lo correcto sería crear objetos para encapsular los charts deseados y luego exportar a JS.
            JSObj = "{ " +
                "type: 'doughnut'," +
                "data: " +
                    "{" +
                       " labels: " + types + "," +
                            "datasets: [{" +
                            "label: 'Cantidad'," +
                                "data: " + cantidades + "," +
                                "backgroundColor: " + colores +
                            "}]" +
                        "}," +
                        "options:" +
                    "{" +
                       " animation:" +
                        "{" +
                           " animateRotate: true" +
                            "} " +
                    "}" +
                "}";
        }
        catch (Exception ex)
        {

        }
        return JSObj;
    }

    private string ObtenerObjChartDoughnutB(List<Ejecutor> listEjecutores, List<Produccion> listProduccion)
    {
        string JSObj = string.Empty;
        try
        {
            //Tipos a graficar
            string types = "['";
            //Cantidades a graficar
            string cantidades = "['";
            // Colores para graficar. Separamos por '.' porque desde JavaScript, al aplicar 'split()' no podremos separar por ',' 
            // debido a que obtendríamos un resultado incorrectos dado que el formato de una cadena de valores rgba se separan también por ','. 
            string colores = "['";

            for (int i = 0; i < listEjecutores.Count - 1; i++)
            {
                int num = listProduccion.Where(p => p.oEstadoActual.IdEjecutor == listEjecutores[i].Id).Count();
                types = types + listEjecutores[i].Nombre + " - ";
                cantidades = cantidades + num + "','";
                types = types + num + "','";
                colores = colores + ValoresConstantes.listaColores[i] + "','";
            }

            int cantidad = listProduccion.Where(p => p.oEstadoActual.IdEjecutor == listEjecutores.Last().Id).Count();
            types = types + listEjecutores.Last().Nombre + " - " + cantidad + "']";
            cantidades = cantidades + cantidad + "']";
            colores = colores + ValoresConstantes.listaColores[listEjecutores.Count] + "']";

            // por el momento realizaremos el obj JS como un string de c#.
            // pero lo correcto sería crear objetos para encapsular los charts deseados y luego exportar a JS.
            JSObj = "{ " +
                "type: 'doughnut'," +
                "data: " +
                    "{" +
                       " labels: " + types + "," +
                            "datasets: [{" +
                            "label: 'Cantidad'," +
                                "data: " + cantidades + "," +
                                "backgroundColor: " + colores +
                            "}]" +
                        "}," +
                        "options:" +
                    "{" +
                       " animation:" +
                        "{" +
                           " animateRotate: true" +
                            "} " +
                    "}" +
                "}";
        }
        catch (Exception ex)
        {

        }
        return JSObj;
    }

    #endregion

    protected void lbtnBuscar_Click(object sender, EventArgs e)
    {
        long idOP = Convert.ToInt64(Request.QueryString["IdOP"]);
        OrdenProd OP = oServOP.TraerUnicoXId(idOP);
        LitNombre.Text = "Tablero de control orden de produccion: " + OP.NroPedidoInterno;
        CargarDatos(OP);
    }

    #region DDL
    private void CargoDDLEjecutor()
    {
        ddlEjecutor.Items.Clear();

        List<Ejecutor> Ejecutores = oServEjecutor.TraerTodos(null).ToList();

        ddlEjecutor.DataSource = Ejecutores;
        ddlEjecutor.DataTextField = "Nombre";
        ddlEjecutor.DataValueField = "Id";
        ddlEjecutor.DataBind();
        string ejecutor = (string)Session["ddl"];
        ddlEjecutor.Items.FindByValue(ejecutor).Selected = true;
    }
    #endregion
}
