﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Application_Produccion_OrdenesProd_Default" EnableEventValidation="false" %>

<%@ Register Src="~/UserControl/usrUltimaModificacion.ascx" TagName="usrUltimaModificacion" TagPrefix="uc1" %>
<%@ Register TagPrefix="usrError" TagName="Error" Src="~/UserControl/Error.ascx" %>
<%@ Register Src="~/UserControl/userProcesando.ascx" TagPrefix="uc1" TagName="userProcesando" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <%--Vista Grilla--%>
    <asp:Panel ID="pnlGrilla" runat="server">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                </div>
                <div class="title_right">
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Administracion Ordenes de Produccion</h2>
                            <div class="pull-right">
                                <button type="submit" id="btnAgregar" runat="server" causesvalidation="False" class="btn btn-round btn-primary" onserverclick="btnAgregar_Click">
                                    <i class="fa fa-plus-circle"></i>Agregar Nueva Orden
                                </button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <%--FILTRO--%>
                        <div class="x_content">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Filtro</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <div class="form-horizontal form-label-left pull-left hidden-xs">
                                        <%--Aca van los filtros que se quieran poner --%>
                                        <div class="col-md-12 col-sm-12  ">
                                            <div class="form-group">
                                                <asp:Label ID="Label3" runat="server" Text="Solicitado Por:" class="control-label col-md-2 col-sm-2 col-xs-6" ToolTip="Se tomaran todas las versiones cuya fecha de inicio este comprendida en este periodo"></asp:Label>
                                                <div class="col-md-1 col-sm-1 col-xs-3">
                                                    <asp:TextBox ID="txtFiltroSolicitadoPor" runat="server" class="form-control"></asp:TextBox>
                                                </div>
                                                <asp:Label ID="Label5" runat="server" Text="Nro. Ped. Externo" class="control-label col-md-2 col-sm-2 col-xs-6" ToolTip="Se tomaran todas las versiones cuya fecha de inicio este comprendida en este periodo"></asp:Label>
                                                <div class="col-md-3 col-sm-3 col-xs-3">
                                                    <asp:TextBox ID="txtFiltroNroPedidoExterno" runat="server" class="form-control"></asp:TextBox>
                                                </div>
                                                <asp:Label ID="Label1" runat="server" Text="Nro. Ped. Interno" class="control-label col-md-2 col-sm-2 col-xs-6" ToolTip="Se tomaran todas las versiones cuya fecha de inicio este comprendida en este periodo"></asp:Label>
                                                <div class="col-md-2 col-sm-2 col-xs-6">
                                                    <asp:TextBox ID="txtFiltroNroPedidoInterno" runat="server" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br />
                                </div>
                            </div>
                            <%--PANEL DE ERRORES--%>
                            <div class="alert alert-danger alert-dismissible fade in" role="alert" runat="server" id="pnlErroresGrilla" visible="false">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <strong><i class="fa fa-warning"></i>Atencion!!!</strong>
                                <br />
                                <asp:Literal ID="litErrorGrilla" runat="server" Text=""></asp:Literal>
                            </div>
                            <%--GRILLA--%>
                            <asp:GridView ID="grdGrilla" runat="server" OnPageIndexChanging="grdGrilla_PageIndexChanging"
                                OnSorting="grdGrilla_Sorting" OnRowCommand="grdGrilla_RowCommand" AllowSorting="true"
                                AutoGenerateColumns="False" CssClass="table table-striped table-bordered">
                                <Columns>
                                    <asp:BoundField DataField="Id" HeaderText="ID" SortExpression="Id" Visible="True" />
                                    <asp:BoundField DataField="FechaPedidoSinHora" HeaderText="Fecha Pedido" SortExpression="FechaPedidoSinHora" />
                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre" SortExpression="Nombre" />
                                    <asp:BoundField DataField="oEstado.Nombre" HeaderText="Estado" SortExpression="oEstadoNombre" />
                                    <asp:BoundField DataField="SolicitadoPor" HeaderText="Solicitado Por" SortExpression="SolicitadoPor" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <div class="form-group">
                                                <asp:LinkButton runat="server" ID="btnBuscar" CssClass="btn btn-info btn-xs" CommandName="Editar" CommandArgument='<%#Eval("Id")%>'>
                                                     <i class="fa fa-search"></i> Editar</asp:LinkButton>
                                                <asp:LinkButton ID="btnVerProduccion" runat="server" CssClass="btn btn-info btn-xs" CommandName="Ver" CommandArgument='<%#Eval("Id") %>'>
                                                     <i class="fa fa-search"></i> Ver Produccion</asp:LinkButton>
                                                <asp:LinkButton ID="btnDashboard" runat="server" CssClass="btn btn-info btn-xs" CommandName="Dashboard" CommandArgument='<%#Eval("Id") %>'>
                                                     <i class="fa fa-area-chart"></i> Dashboard</asp:LinkButton>
                                            </div>
                                        </ItemTemplate>
                                        <ItemStyle Width="33%" />
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle CssClass="pgr" />
                            </asp:GridView>
                            <br />
                            <div class="alert alert-success alert-dismissible fade in" role="alert">
                                <div class="pull-left">
                                    Total de Registros:
                                    <asp:Label ID="lblCantidadFilas" runat="server"></asp:Label>
                                </div>

                                <br />
                                <br />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <%--Vista Detalles--%>
    <asp:Panel ID="pnlDetalles" runat="server">
        <div class="col-md-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>
                        <asp:Literal ID="litNombre" runat="server" Text='<%#TituloForm%>' /></h2>
                    <div class="clearfix"></div>
                </div>
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <%--PANEL DE ERRORES--%>
                        <div class="alert alert-danger alert-dismissible fade in" role="alert" runat="server" id="pnlError" visible="false">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <strong><i class="fa fa-warning"></i>Atencion!!!</strong>
                            <br />
                            <asp:Literal ID="litError" runat="server" Text=""></asp:Literal>
                        </div>
                        <%--PANEL SUCCESS--%>
                        <div class="alert alert-success alert-dismissible fade in" role="alert" runat="server" id="pnlSuccess" visible="false">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <strong><i class="fa fa-check"></i>Operacion Exitosa!!!</strong>
                            <br />
                            <asp:Literal ID="litSuccess" runat="server" Text=""></asp:Literal>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="clearfix"></div>
                <div class="x_content">
                    <br />
                    <div class="form-horizontal form-label-left">
                        <div class="form-group">
                            <asp:Label ID="lblIdTitu" Text="ID" runat="server" Visible="true" class="control-label col-md-1 col-sm-1 col-xs-6">Id:</asp:Label>
                            <div class="col-md-1 col-sm-1 col-xs-6">
                                <asp:Label ID="lblId" runat="server" Visible="true" class="form-control" />
                            </div>
                            <asp:Label runat="server" Text="Nombre:" class="control-label col-md-1 col-sm-1 col-xs-6"></asp:Label>
                            <div class="col-md-3 col-sm-3 col-xs-6">
                                <asp:TextBox ID="txtNombre" runat="server" class="form-control"> </asp:TextBox>
                            </div>
                            <asp:Label runat="server" Text="Descripcion:" class="control-label col-md-1 col-sm-1 col-xs-6"></asp:Label>
                            <div class="col-md-5 col-sm-5 col-xs-6">
                                <asp:TextBox ID="txtDescripcion" runat="server" class="form-control" TextMode="MultiLine" MaxLength="255" Rows="3">
                                </asp:TextBox>

                            </div>
                        </div>
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <div class="form-group">
                                    <asp:Label runat="server" Text="Solicitado Por:" class="control-label col-md-2 col-sm-2 col-xs-6"></asp:Label>
                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                        <asp:TextBox ID="txtSolicitadoPor" runat="server" class="form-control">
                                        </asp:TextBox>
                                    </div>
                                    <asp:Label runat="server" Text="Nro Pedido Int.:" class="control-label col-md-2 col-sm-2 col-xs-6"></asp:Label>
                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                        <asp:TextBox ID="txtNroPedInterno" runat="server" class="form-control">
                                        </asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" Text="Nro Pedido Ext.:" class="control-label col-md-2 col-sm-2 col-xs-6"></asp:Label>
                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                        <asp:TextBox ID="txtNroPedExterno" runat="server" class="form-control">
                                        </asp:TextBox>
                                    </div>
                                    <asp:Label ID="EstadoOp" runat="server" Text="Estado:" class="control-label col-md-2 col-sm-2 col-xs-2"> </asp:Label>
                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                        <asp:DropDownList runat="server" ID="ddlEstadoOp" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="pull-right">
                                    <asp:LinkButton runat="server" ID="btnIniciarProduccion" CssClass="btn btn-round btn-primary requester" OnClick="btnIniciarProduccion_Click">
                                <i class="fa fa-check"></i> Iniciar Produccion
                                    </asp:LinkButton>
                                    <asp:LinkButton runat="server" ID="btnGrabar" CssClass="btn btn-round btn-primary" OnClick="btnGrabar_Click">
                                <i class="fa fa-plus-circle"></i> Grabar
                                    </asp:LinkButton>

                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="pull-right">
                            <asp:LinkButton runat="server" ID="btnSalir" CssClass="btn btn-round btn-alert" OnClientClick="btnSalir_Click" OnClick="btnSalir_Click">
                                <i class="fa fa-arrow-circle-left"></i> Volver
                            </asp:LinkButton>
                        </div>
                        <div class="clearfix"></div>
                        <uc1:usrUltimaModificacion ID="usrUltimaModificacion1" runat="server" />
                        <div class="clearfix"></div>

                        <%-- TAB--%>
                        <div class="" role="tabpanel" data-example-id="togglable-tabs">
                            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="#tabProductos" id="atabProductos" role="tab" data-toggle="tab" aria-expanded="false">
                                        <i class="fa fa-info-circle"></i>Productos
                                    </a>
                                </li>
                                <li role="presentation" class="">
                                    <a href="#tabEntregas" id="atabEntregas" role="tab" data-toggle="tab" aria-expanded="false">
                                        <i class="fa fa-info-circle"></i>Entregas
                                    </a>
                                </li>
                                <li role="presentation" class="">
                                    <a href="#tabNovedades" id="atabNovedades" role="tab" data-toggle="tab" aria-expanded="false">
                                        <i class="fa fa-info-circle"></i>Novedades
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div id="myTabContent" class="tab-content">
                            <%-- TAB PRODUCTOS--%>
                            <div role="tabpanel" class="tab-pane fade active in" id="tabProductos" aria-labelledby="home-tab">
                                <div class="panel panel-default">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h2>Filtro</h2>
                                            <ul class="nav navbar-right panel_toolbox">
                                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">
                                            <%--Aca van los filtros que se quieran poner --%>
                                            <div class="col-md-12 col-sm-12  ">
                                                <div class="form-group">
                                                    <asp:Label ID="lblDestino" runat="server" Text="Destino:" class="control-label col-md-1 col-sm-1 col-xs-1"> </asp:Label>
                                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                                        <asp:DropDownList runat="server" ID="ddlFiltroDestino" CssClass="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="pull-right">
                                                    <asp:LinkButton runat="server" ID="btnBuscarWorkflow" CssClass="btn btn-round btn-primary" OnClick="btnBuscarWorkflow_Click">
                                                     <i class="fa fa-search"></i> Buscar</asp:LinkButton>
                                                </div>
                                            </div>
                                            <br />
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <asp:UpdatePanel runat="server">
                                            <ContentTemplate>
                                                <asp:Panel runat="server" ID="pnlTabProductos">
                                                    <%--PANEL DE ERRORES--%>
                                                    <div class="alert alert-danger alert-dismissible fade in" role="alert" runat="server" id="pnlErrorProd" visible="false">
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                        <strong><i class="fa fa-warning"></i>Atencion!!!</strong>
                                                        <br />
                                                        <asp:Literal ID="LiteralProd" runat="server" Text=""></asp:Literal>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="x_content">
                                                        <asp:GridView ID="grdProductos" runat="server" OnPageIndexChanging="grdProductos_PageIndexChanging"
                                                            OnSorting="grdProductos_Sorting" OnRowCommand="grdProductos_RowCommand" AllowSorting="true"
                                                            AutoGenerateColumns="False" CssClass="table table-striped table-bordered">
                                                            <Columns>
                                                                <asp:BoundField DataField="Id" HeaderText="ID" SortExpression="Id" Visible="True" />
                                                                <asp:BoundField DataField="oTipoProducto.Nombre" HeaderText="Producto" SortExpression="oTipoProductoNombre" />
                                                                <asp:BoundField DataField="Cantidad" HeaderText="Cantidad" SortExpression="Cantidad" />
                                                                <asp:BoundField DataField="oWorkflow.Nombre" HeaderText="Workflow" SortExpression="oWorkflowNombre" />
                                                                <asp:BoundField DataField="OpcionMontaje" HeaderText="Opcion de Montaje" SortExpression="OpcionMontaje" />
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <div class="pull-right">
                                                                            <asp:LinkButton ID="btnEditarProducto" runat="server" CommandName="Editar" CommandArgument='<%#Eval("Id") %>'
                                                                                CssClass="btn btn-round"> <i class="fa fa-edit"></i> Editar</asp:LinkButton>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="10%" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <PagerStyle CssClass="pgr" />
                                                        </asp:GridView>
                                                    </div>

                                                    <%--RELACION ORDEN DE PROUCCION - PRODUCTO--%>
                                                    <br>
                                                    <div class="form-horizontal form-label-left">
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <asp:Label ID="Label6" Text="ID:" runat="server" Visible="true" class="control-label col-md-1 col-sm-1 col-xs-6">Id:</asp:Label>
                                                                <div class="col-md-1 col-sm-1 col-xs-6">
                                                                    <asp:Label ID="lblIdProducto" runat="server" Visible="true" class="form-control" />
                                                                </div>
                                                                <asp:Label ID="Label2" runat="server" Text="Producto:" class="control-label col-md-1 col-sm-1 col-xs-2"></asp:Label>
                                                                <div class="col-md-3 col-sm-3 col-xs-3">
                                                                    <asp:DropDownList runat="server" ID="ddlProducto" CssClass="form-control"></asp:DropDownList>
                                                                </div>
                                                                <asp:Label ID="lblCantidadProd" runat="server" Text="Cantidad:" class="control-label col-md-1 col-sm-1 col-xs-2"></asp:Label>
                                                                <div class="col-md-1 col-sm-1 col-xs-3">
                                                                    <asp:TextBox ID="txtCantidad" runat="server" CssClass="form-control fieldPpal" required="required" type="number" value="1"></asp:TextBox>
                                                                </div>
                                                                <asp:Label ID="lblOpcionMontaje" runat="server" Text="Opcion de Montaje:" class="control-label col-md-2 col-sm-2 col-xs-3"></asp:Label>
                                                                <div class="col-md-1 col-sm-1 col-xs-3">
                                                                    <asp:TextBox ID="txtOpcionMontaje" runat="server" CssClass="form-control fieldPpal" required="required"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:Label ID="lblComentarioProd" runat="server" Text="Comentario:" class="control-label col-md-1 col-sm-1 col-xs-2"></asp:Label>
                                                                <div class="col-md-5 col-sm-5 col-xs-5">
                                                                    <asp:TextBox ID="txtComentario" runat="server" CssClass="form-control fieldPpal"></asp:TextBox>
                                                                </div>
                                                                <asp:Label ID="Label4" runat="server" Text="Workflow:" class="control-label col-md-1 col-sm-1 col-xs-1"></asp:Label>
                                                                <div class="col-md-3 col-sm-3 col-xs-3">
                                                                    <asp:DropDownList runat="server" ID="ddlWorkflow" CssClass="form-control"></asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="pull-right">
                                                            <asp:LinkButton runat="server" ID="btnGrabarProducto" CssClass="btn btn-round btn-primary" OnClick=" btnGrabarProducto_Click">
                                                                <i class="fa fa-plus-circle"></i> Grabar
                                                            </asp:LinkButton>
                                                            <asp:LinkButton runat="server" ID="btnBorrarProducto" CssClass="btn btn-round btn-danger" OnClick="btnBorrarProducto_Click">
                                                                <i class="fa fa-eraser"></i>Borrar Prod.
                                                            </asp:LinkButton>
                                                            <asp:LinkButton runat="server" ID="btnLimpiarProducto" CssClass="btn btn-round btn-alert" OnClick="btnLimpiarProducto_Click">
                                                                <i class="fa fa-arrow-circle-left"></i> Limpiar
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>

                            <%-- TAB ENTREGAS--%>
                            <div role="tabpanel" class="tab-pane fade in" id="tabEntregas" aria-labelledby="home-tab">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <asp:UpdatePanel runat="server">
                                            <ContentTemplate>
                                                <asp:Panel runat="server" ID="pnlTabEntregas">
                                                    <%--PANEL DE ERRORES--%>
                                                    <div class="alert alert-danger alert-dismissible fade in" role="alert" runat="server" id="pnlErrorEntregas" visible="false">
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                        <strong><i class="fa fa-warning"></i>Atencion!!!</strong>
                                                        <br />
                                                        <asp:Literal ID="LiteralEntrega" runat="server" Text=""></asp:Literal>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="x_content">
                                                        <asp:GridView ID="grdEntregas" runat="server" OnPageIndexChanging="grdEntregas_PageIndexChanging"
                                                            OnSorting="grdEntregas_Sorting" OnRowCommand="grdEntregas_RowCommand" AllowSorting="true"
                                                            AutoGenerateColumns="False" CssClass="table table-striped table-bordered">
                                                            <Columns>
                                                                <asp:BoundField DataField="Id" HeaderText="ID" SortExpression="Id" Visible="True" />
                                                                <asp:BoundField DataField="FechaEstimadaSinHora" HeaderText="Fecha Estimada" SortExpression="FechaEstimadaSinHora" />
                                                                <asp:BoundField DataField="Comentario" HeaderText="Comentario" SortExpression="Comentario" />
                                                                <asp:BoundField DataField="oEstadoEntrega.Nombre" HeaderText="Estado" SortExpression="oEstadoEntregaNombre" />
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <div class="pull-right">
                                                                            <asp:LinkButton ID="btnEditarEntrega" runat="server" CommandName="Editar" CommandArgument='<%#Eval("Id") %>'
                                                                                CssClass="btn btn-round"> <i class="fa fa-edit"></i> Editar</asp:LinkButton>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="10%" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <PagerStyle CssClass="pgr" />
                                                        </asp:GridView>
                                                    </div>

                                                    <%--RELACION ORDEN DE PROUCCION - ENTREGA--%>
                                                    <br>
                                                    <div class="form-horizontal form-label-left">
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <asp:Label ID="lblIdEntrega" runat="server" Visible="false"></asp:Label>
                                                                <asp:Label ID="lblFechaEstimada" runat="server" Text="Fecha Estimada:" class="control-label col-md-1 col-sm-1 col-xs-2"></asp:Label>
                                                                <div class="col-md-2 col-sm-2 col-xs-3">
                                                                    <asp:UpdatePanel runat="server">
                                                                        <ContentTemplate>
                                                                            <div class='input-group date dtp' id='dtpFechaEstimada'>
                                                                                <asp:TextBox runat="server" ID="txtFechaEstimada" CssClass="form-control det-control" />
                                                                                <span class="input-group-addon">
                                                                                    <span class="fa fa-calendar"></span>
                                                                                </span>
                                                                            </div>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </div>
                                                                <asp:Label ID="lblEstadoEntrega" runat="server" Text="Estado:" class="control-label col-md-1 col-sm-1 col-xs-2"> </asp:Label>
                                                                <div class="col-md-2 col-sm-2 col-xs-2">
                                                                    <asp:DropDownList runat="server" ID="ddlEstadoEntrega" CssClass="form-control"></asp:DropDownList>
                                                                </div>
                                                                <asp:Label ID="lblComentarioEntr" runat="server" Text="Comentario: " class="control-label col-md-2 col-sm-2 col-xs-2"></asp:Label>
                                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                                    <asp:TextBox ID="txtComentarioEntr" runat="server" CssClass="form-control fieldPpal"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:Label ID="lblUsuarioCerro" runat="server" Text="Usuario Cerró:" class="control-label col-md-1 col-sm-1 col-xs-2"></asp:Label>
                                                                <div class="col-md-2 col-sm-2 col-xs-2">
                                                                    <asp:TextBox ID="txtUsuarioCierre" runat="server" CssClass="form-control fieldPpal"></asp:TextBox>
                                                                </div>
                                                                <asp:Label ID="lblFechaReal" runat="server" Text="Fecha Real:" class="control-label col-md-1 col-sm-1 col-xs-2"></asp:Label>
                                                                <div class="col-md-2 col-sm-2 col-xs-3">
                                                                    <asp:TextBox runat="server" ID="txtFechaReal" CssClass="form-control det-control" />
                                                                </div>
                                                                <asp:Label ID="lblComentarioCierre" runat="server" Text="Comentario Cierre: " class="control-label col-md-2 col-sm-2 col-xs-2"></asp:Label>
                                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                                    <asp:TextBox ID="txtComentarioCierreEntr" runat="server" CssClass="form-control fieldPpal" required="required"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="pull-right">
                                                            <asp:LinkButton runat="server" ID="btnGrabarEntr" CssClass="btn btn-round btn-primary" OnClick=" btnGrabarEntr_Click">
                                                                     <i class="fa fa-plus-circle"></i> Grabar
                                                            </asp:LinkButton>
                                                            <asp:LinkButton runat="server" ID="btnLimpiarEntr" CssClass="btn btn-round btn-alert" OnClick="btnLimpiarEntr_Click">
                                                                <i class="fa fa-arrow-circle-left"></i> Limpiar
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>

                            <%-- TAB NOVEDADES--%>
                            <div role="tabpanel" class="tab-pane fade in" id="tabNovedades" aria-labelledby="home-tab">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <asp:UpdatePanel runat="server">
                                            <ContentTemplate>
                                                <asp:Panel runat="server" ID="pnlTabNovedades">
                                                    <div class="x_content">
                                                        <asp:GridView ID="grdNovedades" runat="server" OnPageIndexChanging="grdNovedades_PageIndexChanging"
                                                            OnSorting="grdNovedades_Sorting" OnRowCommand="grdNovedades_RowCommand" AllowSorting="true"
                                                            AutoGenerateColumns="False" CssClass="table table-striped table-bordered">
                                                            <Columns>
                                                                <asp:BoundField DataField="Id" HeaderText="ID" SortExpression="Id" Visible="True" />
                                                                <asp:BoundField DataField="Comentario" HeaderText="Comentario" SortExpression="Comentario" />
                                                                <asp:BoundField DataField="FechaSinHora" HeaderText="Fecha" SortExpression="FechaSinHora" />
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <div class="pull-right">
                                                                            <asp:LinkButton ID="btnEditarNovedad" runat="server" CommandName="Editar" CommandArgument='<%#Eval("Id") %>'
                                                                                CssClass="btn btn-round"> <i class="fa fa-edit"></i> Editar</asp:LinkButton>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="10%" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <PagerStyle CssClass="pgr" />
                                                        </asp:GridView>
                                                    </div>

                                                    <%--RELACION ORDEN DE PROUCCION - Novedad--%>
                                                    <br>
                                                    <div class="form-horizontal form-label-left">
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <asp:Label ID="lblIdNovedad" runat="server" Visible="false"></asp:Label>
                                                                <asp:Label ID="lblComentarioNovedad" runat="server" Text="Comentario:" class="control-label col-md-1 col-sm-1 col-xs-2"></asp:Label>
                                                                <div class="col-md-5 col-sm-5 col-xs-5">
                                                                    <asp:TextBox ID="txtComentarioNovedad" runat="server" CssClass="form-control fieldPpal"></asp:TextBox>
                                                                </div>
                                                                <asp:Label ID="lblFechaNovedad" runat="server" Text="Fecha:" class="control-label col-md-1 col-sm-1 col-xs-2"></asp:Label>
                                                                <div class="col-md-2 col-sm-2 col-xs-3">
                                                                    <asp:UpdatePanel runat="server">
                                                                        <ContentTemplate>
                                                                            <div class='input-group date dtp' id='dtpFechaNovedad'>
                                                                                <asp:TextBox runat="server" ID="txtFechaNovedad" CssClass="form-control det-control" />
                                                                                <span class="input-group-addon">
                                                                                    <span class="fa fa-calendar"></span>
                                                                                </span>
                                                                            </div>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="pull-right">
                                                            <asp:LinkButton runat="server" ID="btnGrabarNovedad" CssClass="btn btn-round btn-primary" OnClick=" btnGrabarNovedad_Click">
                                                                     <i class="fa fa-plus-circle"></i> Grabar
                                                            </asp:LinkButton>
                                                            <asp:LinkButton runat="server" ID="btnLimpiarNovedad" CssClass="btn btn-round btn-alert" OnClick="btnLimpiarNovedad_Click">
                                                                <i class="fa fa-arrow-circle-left"></i> Limpiar
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </asp:Panel>
    <usrError:Error ID="errorMensaje" runat="server" />
    <uc1:userProcesando runat="server" ID="userProcesando" />
</asp:Content>

