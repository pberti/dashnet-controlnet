﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Application_Produccion_Workflow_Workflow_Estados_Default" %>

<%@ Register Src="~/UserControl/usrUltimaModificacion.ascx" TagName="usrUltimaModificacion" TagPrefix="uc1" %>
<%@ Register TagPrefix="usrError" TagName="Error" Src="~/UserControl/Error.ascx" %>
<%@ Register Src="~/UserControl/userProcesando.ascx" TagPrefix="uc1" TagName="userProcesando" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <%--Vista Grilla--%>
    <asp:Panel ID="pnlGrilla" runat="server">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                </div>
                <div class="title_right">
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Administracion de Estados de Workflow</h2>
                            <div class="pull-right">
                                <button type="submit" id="btnAgregar" runat="server" causesvalidation="False" class="btn btn-round btn-primary" onserverclick="btnAgregar_Click">
                                    <i class="fa fa-plus-circle"></i>Agregar Nuevo
                                </button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <%--FILTRO--%>
                        <div class="x_content">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Filtro</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-horizontal form-label-left pull-left">
                                    <%--Aca van los filtros que se quieran poner --%>
                                </div>
                            </div>
                            <%--GRILLA--%>
                            <asp:GridView ID="grdGrilla" runat="server" OnPageIndexChanging="grdGrilla_PageIndexChanging"
                                OnSorting="grdGrilla_Sorting" OnRowCommand="grdGrilla_RowCommand" AllowSorting="true"
                                AutoGenerateColumns="False" CssClass="table table-striped table-bordered">
                                <Columns>
                                    <asp:BoundField DataField="Id" HeaderText="ID" SortExpression="Id" Visible="True" />
                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre" SortExpression="Nombre" />
                                    <asp:BoundField DataField="Descripcion" HeaderText="Descripcion" SortExpression="Descripcion" />
                                    <asp:TemplateField HeaderText="Es Inicial">
                                        <ItemTemplate>
                                            <div class="center">
                                                <asp:CheckBox ID="chbxEsInicial" Checked='<%#Eval("EsInicial") %>' runat="server" Enabled="false" />
                                                <asp:Label ID="lblEsInicial" Text='<%#Eval("Id") %>' runat="server" Visible="false" />
                                            </div>
                                        </ItemTemplate>
                                        <ItemStyle Width="10%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Es Final">
                                        <ItemTemplate>
                                            <div class="center">
                                                <asp:CheckBox ID="chbxEsFinal" Checked='<%#Eval("EsFinal") %>' runat="server" Enabled="false" />
                                                <asp:Label ID="lblEsFinal" Text='<%#Eval("Id") %>' runat="server" Visible="false" />
                                            </div>
                                        </ItemTemplate>
                                        <ItemStyle Width="10%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <div class="pull-right">
                                                <asp:LinkButton ID="btnEditar" runat="server" CommandName="Editar" CommandArgument='<%#Eval("Id") %>'
                                                    CssClass="btn btn-round"> <i class="fa fa-edit"></i> Editar</asp:LinkButton>
                                            </div>
                                        </ItemTemplate>
                                        <ItemStyle Width="10%" />
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle CssClass="pgr" />
                            </asp:GridView>
                            <br />
                            <div class="alert alert-success alert-dismissible fade in" role="alert">
                                <div class="pull-left">
                                    Total de Registros:
                                    <asp:Label ID="lblCantidadFilas" runat="server"></asp:Label>
                                </div>

                                <br />
                                <br />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>

    <%--Vista Detalles--%>
    <asp:Panel ID="pnlDetalles" runat="server">
        <div class="col-md-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>
                        <asp:Literal ID="litNombre" runat="server" Text='<%#TituloForm%>' /></h2>
                    <div class="clearfix"></div>
                </div>
                <%--PANEL DE ERRORES--%>
                <div class="alert alert-danger alert-dismissible fade in" role="alert" runat="server" id="pnlError" visible="false">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <strong><i class="fa fa-warning"></i>Atencion!!!</strong>
                    <br />
                    <asp:Literal ID="litError" runat="server" Text=""></asp:Literal>
                </div>
                 <%--PANEL SUCCESS--%>
                <div class="alert alert-success alert-dismissible fade in" role="alert" runat="server" id="pnlSuccess" visible="false">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <strong><i class="fa fa-check"></i>Operacion Exitosa!!!</strong>
                    <br />
                    <asp:Literal ID="litSuccess" runat="server" Text=""></asp:Literal>
                </div>
                <div class="clearfix"></div>
                <div class="x_content">
                    <br />
                    <div class="form-horizontal form-label-left">
                        <div class="form-group">
                            <div class="pull-left">
                                <asp:Label ID="lblIdTitu" Text="ID:" runat="server" Visible="true" class="control-label col-md-2 col-sm-2 col-xs-3">Id:</asp:Label>
                                <div class="col-md-1 col-sm-1 col-xs-1">
                                    <asp:Label ID="lblId" runat="server" Visible="true" class="form-control" />
                                </div>
                            </div>
                            <asp:Label runat="server" Text="Nombre:" class="control-label col-md-2 col-sm-2 col-xs-3"></asp:Label>
                            <div class="col-md-2 col-sm-2 col-xs-3">
                                <asp:TextBox ID="txtNombre" runat="server" class="form-control">
                                </asp:TextBox>
                                <asp:Label Text="" runat="server" ID="lvlValNombre" CssClass="help-inline alert" />
                            </div>
                            <asp:UpdatePanel runat="server">
                                <ContentTemplate>
                                    <asp:Label runat="server" Text="Inicial:" class="control-label col-md-1 col-sm-1 col-xs-1"></asp:Label>
                                    <div class="col-md-1 col-sm-1 col-xs-1">
                                        <asp:CheckBox ID="chkEsInicial" runat="server" class="form-control" AutoPostBack="true"></asp:CheckBox>
                                    </div>
                                    <asp:Label runat="server" Text="Final:" class="control-label col-md-1 col-sm-1 col-xs-1"></asp:Label>
                                    <div class="col-md-1 col-sm-1 col-xs-1">
                                        <asp:CheckBox ID="chkEsFinal" runat="server" class="form-control" AutoPostBack="true"></asp:CheckBox>
                                    </div>
                                    <asp:Label runat="server" Text="Masivo:" class="control-label col-md-1 col-sm-1 col-xs-1"></asp:Label>
                                    <div class="col-md-1 col-sm-1 col-xs-1">
                                        <asp:CheckBox ID="chkMasivo" runat="server" class="form-control" AutoPostBack="true"></asp:CheckBox>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" Text="Descripcion:" class="control-label col-md-1 col-sm-1 col-xs-1"></asp:Label>
                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <asp:TextBox ID="txtDescripcion" runat="server" class="form-control"></asp:TextBox>
                            </div>
                            <asp:Label ID="lblPantalla" runat="server" Text="Pantalla:" class="control-label col-md-1 col-sm-1 col-xs-1"></asp:Label>
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <asp:DropDownList runat="server" ID="ddlPantalla" CssClass="form-control"></asp:DropDownList>
                            </div>
                            <asp:Label ID="Label1" runat="server" Text="Ejecutor:" class="control-label col-md-1 col-sm-1 col-xs-1"></asp:Label>
                            <div class="col-md-2 col-sm-2 col-xs-2">
                                <asp:DropDownList runat="server" ID="ddlEjecutor" CssClass="form-control"></asp:DropDownList>
                            </div>
                        </div>
                        <uc1:usrUltimaModificacion ID="usrUltimaModificacion1" runat="server" />
                        <div class="pull-right">
                            <button type="submit" id="btnGrabar" runat="server" causesvalidation="False" class="btn btn-round btn-primary" onserverclick="btnGrabar_ServerClick">
                                <i class="fa fa-check"></i>Grabar
                            </button>
                            <button type="submit" id="btnSalir" runat="server" causesvalidation="False" class="btn btn-round btn-alert" onserverclick="btnSalir_ServerClick">
                                <i class="fa fa-arrow-circle-left"></i>Volver
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <%-- Workflow Permisos--%>
            <div class="x_panel" runat="server" id="pnlPermiso" visible="false">
                <div class="x_title">
                    <h2>
                        <asp:Literal ID="Literal1" runat="server" Text="Permisos del Estado" /></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <asp:GridView ID="grdPermiso" runat="server" OnPageIndexChanging="grdPermiso_PageIndexChanging"
                        OnSorting="grdPermiso_Sorting" AllowSorting="true"
                        AutoGenerateColumns="False" CssClass="table table-striped table-bordered">
                        <Columns>
                            <asp:BoundField DataField="Id" HeaderText="ID" SortExpression="Id" Visible="True" />
                            <asp:BoundField DataField="oPerfil.Perfil" HeaderText="Perfil" SortExpression="oPerfilPerfil" Visible="True" />
                            <asp:BoundField DataField="oWorkflowEstado.Nombre" HeaderText="Estado" SortExpression="oWorkflowEstadoNombre" Visible="True" />
                            <asp:TemplateField HeaderText="Puede Ver">
                                <ItemTemplate>
                                    <div class="center">
                                        <asp:CheckBox ID="chbxPuedeVer" Checked='<%#Eval("PuedeVer")%>' runat="server"/>
                                        <asp:Label ID="lblPuedever" Text='<%#Eval("Id") %>' runat="server" Visible="false" />
                                    </div>
                                </ItemTemplate>
                                <ItemStyle Width="10%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Puede Grabar">
                                <ItemTemplate>
                                    <div class="center">
                                        <asp:CheckBox ID="chbxPuedeGrabar" Checked='<%#Eval("PuedeGrabar") %>' runat="server" />
                                        <asp:Label ID="lblPuedeGrabar" Text='<%#Eval("Id") %>' runat="server" Visible="false" />
                                    </div>
                                </ItemTemplate>
                                <ItemStyle Width="10%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Puede Volver">
                                <ItemTemplate>
                                    <div class="center">
                                        <asp:CheckBox ID="chbxPuedeVolver" Checked='<%#Eval("PuedeVolver") %>' runat="server"/>
                                        <asp:Label ID="lblPuedeVolver" Text='<%#Eval("Id") %>' runat="server" Visible="false" />
                                    </div>
                                </ItemTemplate>
                                <ItemStyle Width="10%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Puede Avanzar">
                                <ItemTemplate>
                                    <div class="center">
                                        <asp:CheckBox ID="chbxPuedeAvanzar" Checked='<%#Eval("PuedeAvanzar") %>' runat="server"/>
                                        <asp:Label ID="lblPuedeAvanzar" Text='<%#Eval("Id") %>' runat="server" Visible="false" />
                                    </div>
                                </ItemTemplate>
                                <ItemStyle Width="10%" />
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle CssClass="pgr" />
                    </asp:GridView>
                    <div class="pull-right">
                        <button type="submit" id="btnLimpiarPermiso" runat="server" causesvalidation="False" class="btn btn-round btn-alert" onserverclick="btnLimpiarPermiso_ServerClick">
                            <i class="fa fa-arrow-circle-left"></i>Limpiar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <usrError:Error ID="errorMensaje" runat="server" />
    <uc1:userProcesando runat="server" ID="userProcesando" />
</asp:Content>
