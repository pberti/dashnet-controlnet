﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IOT.ORM;
using IOT.Domain;
using System.Transactions;
using System.Web.Security;
using System.Data;
using IOT.Services;

public partial class Application_Produccion_Workflow_Workflow_Estados_Default : PageBase
{
    #region Propiedades & Variables
    private ServWorkFlow_Estado oServWorkflow_Estado = Services.Get<ServWorkFlow_Estado>();
    private ServTipoPerfilMenues oServPerfilMenus = Services.Get<ServTipoPerfilMenues>();
    private ServFormulario oServFormularios = Services.Get<ServFormulario>();
    private ServEjecutor oServEjecutor = Services.Get<ServEjecutor>();
    private ServWorkflow_Permisos oServWorkflow_Permisos = Services.Get<ServWorkflow_Permisos>();
    private ServUsuario oServUsuario = Services.Get<ServUsuario>();
    #endregion

    #region "Main"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (base.IdUsuarioLogueado == -1)
        {
            Response.Redirect("~/Application/Login/Default.aspx", false);
            return;
        }
        if (!Page.IsPostBack)
        {

            InicializacionPantalla();

        }

    }
    private void InicializacionPantalla()
    {
        CargoPermisosdePantalla();
        DireccionOrdenacion = string.Empty;
        this.CargoGrilla("FecUltimaModificacion descending", this.ObtenerFiltro());
        VisibilidadPaneles(true, false);
        lvlValNombre.Text = "";
    }


    private void CargoPermisosdePantalla()
    {
        var tipoPerfilMenu = oServPerfilMenus.TraerTodosTipoPefilMenuesPorPerfil(IdPerfilUsuarioLogueado).Where(p => p.oMenu.PathPagina.Contains("/Produccion/Workflow/Workflow_Estados"));
        if (tipoPerfilMenu.Count() != 0)
        {
            List<TipoPerfilMenues> t = tipoPerfilMenu.ToList();
            btnAgregar.Visible = t.First().PuedeGrabar;
            btnGrabar.Visible = t.First().PuedeGrabar;
        }
        else
            Response.Redirect("~/Application/Default.aspx", false);

    }
    #endregion

    #region "Métodos - Generales Pantalla"
    private void VisibilidadPaneles(bool pPnlGrilla, bool pPnlDetalle)
    {
        pnlGrilla.Visible = pPnlGrilla;
        pnlDetalles.Visible = pPnlDetalle;
    }
    /// <summary>
    ///  Metodo que carga los datos del usercontrol de usrUltimaModificacion
    /// </summary>
    private void CargarUserControl(long pIdUsuarioMofifico, DateTime pFechaUltimaModificacion)
    {
        usrUltimaModificacion1.FecUltimaModificacion = pFechaUltimaModificacion.ToString();
        usrUltimaModificacion1.IdUsuarioModifico = pIdUsuarioMofifico;
        usrUltimaModificacion1.UltimaModificacionBind();
    }
    #endregion

    #region Metodos - Grilla

    /// <summary>
    /// Metodo que obtiene los datos establecidos de los filtros.
    /// </summary>
    /// <returns>Metodo que devuelve un objeto con los valores del filtro.</returns>
    private Workflow_Estado ObtenerFiltro()
    {
        Workflow_Estado oObj = null;
        return oObj;
    }

    /// <summary>
    /// Metodo que obtiene el ordenamiento de las columnas.
    /// </summary>
    private void ObtenerDireccionOrden(GridViewSortEventArgs evt)
    {
        if (DireccionOrdenacion == "")
        {
            DireccionOrdenacion = "ascending";
        }
        else
        {
            if (DireccionOrdenacion == "ascending")
                DireccionOrdenacion = "descending";
            else
                DireccionOrdenacion = "ascending";
        }
    }


    /// <summary>
    /// Metodo que cargs los datos en la grilla de la pagina.
    /// </summary>
    /// <param name="pOrderBy">Criterio de ordenamiento de las columnas</param>
    /// <param name="pObj">Objeto Viaje para determinar si existe algun filtro a aplicar a la grilla</param>
    private void CargoGrilla(string pOrderBy, Workflow_Estado pObj)
    {
        CriterioOrdenacion = pOrderBy;
        List<Workflow_Estado> wQuery = null;
        try
        {
            if (pObj == null)
            {
                wQuery = oServWorkflow_Estado.TraerTodos().OrderBy(pOrderBy).ToList();
            }
            else
            {
                wQuery = oServWorkflow_Estado.TraerTodos().OrderBy(pOrderBy).ToList();
            }

            grdGrilla.DataSource = wQuery;
            grdGrilla.DataBind();
            lblCantidadFilas.Text = wQuery.Count().ToString();
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargoGrilla", HttpContext.Current.Request.Url.LocalPath);
        }
        finally
        {
            wQuery = null;
        }
    }

    protected void grdGrilla_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdGrilla.PageIndex = e.NewPageIndex;
        this.CargoGrilla(CriterioOrdenacion, this.ObtenerFiltro());
    }

    protected void grdGrilla_Sorting(object sender, GridViewSortEventArgs e)
    {
        this.ObtenerDireccionOrden(e);
        this.CargoGrilla(string.Format("{0} {1}", e.SortExpression, DireccionOrdenacion), this.ObtenerFiltro());
    }

    protected void grdGrilla_RowCommand(Object sender, CommandEventArgs e)
    {
        if (e.CommandName == "Editar")
        {
            base.EntityId = Convert.ToInt64(e.CommandArgument);
            base.EsNuevo = false;
            litNombre.Text = "Editar Estado";
            lblId.Text = EntityId.ToString();
            this.CargarControles();
            usrUltimaModificacion1.Visible = true;
            this.VisibilidadPaneles(false, true);
            pnlPermiso.Visible = true;
            CargoGrillaPermiso();
        }
    }
    #endregion

    #region "Eventos Botones"

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        try
        {
            this.CargoGrilla("FecUltimaModificacion descending", this.ObtenerFiltro());

        }
        catch (SystemException ex)
        {
            errorMensaje.ErrorBind(ex, "btnBuscar_Click", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    protected void btnAgregar_Click(object sender, EventArgs e)
    {
        TituloForm = "Nuevo Estado de Workflow";
        this.CrearNuevo();
        pnlPermiso.Visible = false;
    }

    protected void btnGrabar_ServerClick(object sender, EventArgs e)
    {
        Page.Validate();
        if (Page.IsValid)
        {
            this.GrabarDatos();
            if (pnlPermiso.Visible && !pnlError.Visible)
            {
                GrabarDatosPermisos();
            }
            if (!pnlError.Visible)
            {
                pnlPermiso.Visible = true;
                CargoGrillaPermiso();
                pnlSuccess.Visible = true;
            }
        }
    }

    protected void btnSalir_ServerClick(object sender, EventArgs e)
    {
        this.VisibilidadPaneles(true, false);
        CargoGrilla("FecUltimaModificacion descending", this.ObtenerFiltro());
        Limpiar();
    }

    #endregion

    #region Metodos - Vista Detalle
    /// <summary>
    /// Metodo que carga los datos de los controles.
    /// </summary>
    private void CargarControles()
    {
        try
        {
            var oObj = oServWorkflow_Estado.TraerUnicoXId(base.EntityId);

            txtNombre.Text = oObj.Nombre;
            txtDescripcion.Text = oObj.Descripcion;
            chkEsInicial.Checked = oObj.EsInicial;
            chkEsFinal.Checked = oObj.EsFinal;
            chkMasivo.Checked = oObj.Masivo;
            CargoDDLEjecutor();
            ddlEjecutor.Items.FindByText(oObj.oEjecutor.Nombre).Selected = true;
            CargoDDLPantalla();

            if (oObj.Pantalla.Contains("PantallaFecha"))
            {
                ddlPantalla.Items.FindByText("Pantalla Fecha").Selected = true;
            }
            else if (oObj.Pantalla.Contains("PantallaNroSerie"))
            {
                ddlPantalla.Items.FindByText("Pantalla NroSerie").Selected = true;
            }
            else if (oObj.Pantalla.Contains("PantallaFallas"))
            {
                ddlPantalla.Items.FindByText("Servicio Tecnico").Selected = true;
            }
            else if (oObj.Pantalla.Contains("PantallaFormulario"))
            {
                //Cuento con la dirección de pantalla actual para encontrar el id, si cambia la dirección cambian los indices para el substring
                string largo = oObj.Pantalla.Substring(81, 1);
                long id = Convert.ToInt64(largo);
                Formulario form = oServFormularios.TraerXId(id);
                ddlPantalla.Items.FindByText(form.Titulo).Selected = true;
            }
            else
                ddlPantalla.Items.FindByText("").Selected = true;

            this.CargarUserControl(oObj.IdUsuarioModifico, oObj.FecUltimaModificacion);
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargarControles", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    /// <summary>
    /// Metodo que guarda los datos establecidos en la pagina.
    /// </summary>
    private void GrabarDatos()
    {
        try
        {
            Workflow_Estado oObj = ObtenerDatos();

            if (ValidoObjeto(oObj))
            {
                if (EsNuevo)
                {
                    EntityId = oServWorkflow_Estado.Agregar(oObj);
                }
                else
                {
                    oServWorkflow_Estado.Actualizar(oObj);
                }
                EsNuevo = false;
                pnlError.Visible = false;
                CargarControles();
            }
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "GrabarDatos", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    /// <summary>
    /// Obtiene los datos de la pagina
    /// </summary>
    /// <returns>Retorna un objecto con los datos establecidos en la pagina</returns>
    private Workflow_Estado ObtenerDatos()
    {
        Workflow_Estado oObj = null;
        try
        {
            oObj = new Workflow_Estado();
            var oUsuario = base.TraerUsuarioLogeado();
            if (!EsNuevo)
            {
                oObj = oServWorkflow_Estado.TraerUnicoXId(base.EntityId);
            }
            oObj.IdUsuarioModifico = oUsuario.Id;

            oObj.Nombre = txtNombre.Text.Trim();
            oObj.Descripcion = txtDescripcion.Text;
            oObj.EsFinal = chkEsFinal.Checked;
            oObj.EsInicial = chkEsInicial.Checked;
            oObj.Masivo = chkMasivo.Checked;
            oObj.IdEjecutor = Convert.ToInt64(ddlEjecutor.SelectedValue);
            if (ddlPantalla.Text == "Pantalla Fecha")
            {
                oObj.Pantalla = "~/Application/Produccion/Produccion/Pantallas/PantallaFecha/Default.aspx?";
            }
            else if (ddlPantalla.Text == "Pantalla NroSerie")
            {
                oObj.Pantalla = "~/Application/Produccion/Produccion/Pantallas/PantallaNroSerie/Default.aspx?";
            }
            else if (ddlPantalla.Text == "Servicio Tecnico")
            {
                oObj.Pantalla = "~/Application/Produccion/Produccion/Pantallas/PantallaFallas/Default.aspx?dosPantallas=1&";
            }
            else
            {
                Formulario form = oServFormularios.TraerXTitulo(ddlPantalla.Text);
                oObj.Pantalla = "~/Application/Produccion/Produccion/Pantallas/PantallaFormulario/Default.aspx" + "?Id=" + form.Id + "&";
            }
            oObj.FecUltimaModificacion = DateTime.Now;
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "ObtenerDatos", HttpContext.Current.Request.Url.LocalPath);
        }
        return oObj;
    }

    /// <summary>
    /// Metodo que establece los datos en la pagina para crear un nuevo objecto TipoPerfil
    /// </summary>
    private void CrearNuevo()
    {
        this.Limpiar();
        litNombre.Text = "Nuevo Estado de Workflow";
        usrUltimaModificacion1.Visible = false;
        this.VisibilidadPaneles(false, true);
        CargoDDLEjecutor();
        EsNuevo = true;
    }

    /// <summary>
    /// Metodo que limpia los datos establecidos en la pagina.
    /// </summary>
    private void Limpiar()
    {
        txtNombre.Text = string.Empty;
        txtDescripcion.Text = string.Empty;
        chkEsInicial.Checked = false;
        chkEsFinal.Checked = false;
        lblId.Text = string.Empty;
        pnlError.Visible = false;
        CargoDDLPantalla();
        pnlSuccess.Visible = false;
    }

    #endregion

    #region Métodos DDL
    private void CargoDDLPantalla()
    {
        ddlPantalla.Items.Clear();
        ddlPantalla.Items.Add("Pantalla Fecha");
        ddlPantalla.Items.Add("Pantalla NroSerie");
        ddlPantalla.Items.Add("Servicio Tecnico");

        List<Formulario> formularios = oServFormularios.TraerTodos(null).Where(f => f.Inactivo == false).ToList();
        foreach (Formulario f in formularios)
        {
            ddlPantalla.Items.Add(f.Titulo);
        }

        ddlPantalla.Items.Add("");
    }

    private void CargoDDLEjecutor()
    {
        ddlEjecutor.Items.Clear();

        List<Ejecutor> Ejecutores = oServEjecutor.TraerTodos(null).ToList();

        ddlEjecutor.DataSource = Ejecutores;
        ddlEjecutor.DataTextField = "Nombre";
        ddlEjecutor.DataValueField = "Id";
        ddlEjecutor.DataBind();
    }
    #endregion

    #region Validacion de Campos
    /// <summary>
    /// Metodo que valida el tramite a cargar
    /// </summary>
    private bool ValidoObjeto(Workflow_Estado pObj)
    {
        bool comodin = true;
        pnlError.Visible = false;
        pnlSuccess.Visible = false;
        string Error = string.Empty;

        lvlValNombre.Text = "";
        if (pObj.Nombre.Trim().Length == 0)
        {
            Error = "Debe ingresar un Nombre Valido <br>";
            comodin = false;
        }
        else
        {
            if (EsNuevo)
            {
                //Valido que no existe un estado con ese nombre
                Workflow_Estado oObj = oServWorkflow_Estado.TraerTodos().Where(p => p.Nombre == pObj.Nombre.Trim()).FirstOrDefault();
                if (oObj != null)
                {
                    Error = Error + "Ya Existe un estado con ese Nombre, por favor reintente <br>";
                    comodin = false;
                }
            }
            else
            {

                List<Workflow_Estado> oObj = oServWorkflow_Estado.TraerTodos().Where(p => p.Nombre == pObj.Nombre.Trim()).ToList();
                oObj.RemoveAll(t => t.Id == pObj.Id);
                if (oObj.Count != 0)
                {
                    Error = Error + "Ya Existe un estado de workflow con ese Nombre, por favor reintente <br>";
                    comodin = false;
                }
            }
        }

        if (!string.IsNullOrEmpty(Error))
        {
            pnlError.Visible = true;
            litError.Text = Error;
        }
        else
        {
            pnlError.Visible = false;
        }

        return comodin;
    }
    #endregion

    #region Permisos
    #region Metodos - Grilla

    /// <summary>
    /// Metodo que cargs los datos en la grilla de la pagina.
    /// </summary>
    /// <param name="pOrderBy">Criterio de ordenamiento de las columnas</param>
    /// <param name="pObj">Objeto Viaje para determinar si existe algun filtro a aplicar a la grilla</param>
    private void CargoGrillaPermiso()
    {
        List<Workflow_Permisos> wQuery = null;
        try
        {
            wQuery = ComprobarPermisos();
            wQuery = oServWorkflow_Permisos.TraerTodos().Where(p => p.IdWorkflowEstado == EntityId).ToList();
            wQuery.Reverse();

            grdPermiso.DataSource = wQuery;
            grdPermiso.DataBind();

        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargoGrillaPermisos", HttpContext.Current.Request.Url.LocalPath);
        }
        finally
        {
            wQuery = null;
        }
    }

    protected void grdPermiso_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdPermiso.PageIndex = e.NewPageIndex;
        this.CargoGrillaPermiso();
    }

    protected void grdPermiso_Sorting(object sender, GridViewSortEventArgs e)
    {
        this.ObtenerDireccionOrden(e);
        this.CargoGrillaPermiso();
    }
    #endregion

    #region "Eventos Botones"

    protected void btnLimpiarPermiso_ServerClick(object sender, EventArgs e)
    {
        CargoGrillaPermiso();
    }

    #endregion

    #region Metodos Generales
    /// <summary>
    /// Metodo que guarda los datos establecidos en la pagina.
    /// </summary>
    private void GrabarDatosPermisos()
    {
        try
        {
            foreach (GridViewRow row in grdPermiso.Rows)
            {
                CheckBox checkAvanzar = (CheckBox)row.FindControl("chbxPuedeAvanzar");
                CheckBox checkVer = (CheckBox)row.FindControl("chbxPuedeVer");
                CheckBox checkGrabar = (CheckBox)row.FindControl("chbxPuedeGrabar");
                CheckBox checkVolver = (CheckBox)row.FindControl("chbxPuedeVolver");

                string perfil = row.Cells[1].Text;
                long idperfil = oServUsuario.TraerTipoPerfil(perfil).Id;
                Workflow_Permisos permiso = oServWorkflow_Permisos.TraerTodos().Where(p => p.IdPerfil == idperfil && p.IdWorkflowEstado == EntityId).FirstOrDefault();
                permiso.PuedeAvanzar = checkAvanzar.Checked;
                permiso.PuedeVer = checkVer.Checked;
                permiso.PuedeGrabar = checkGrabar.Checked;
                permiso.PuedeVolver = checkVolver.Checked;

                oServWorkflow_Permisos.Actualizar(permiso);
            }
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "GrabarDatos", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    List<Workflow_Permisos> ComprobarPermisos()
    {
        List<Workflow_Permisos> lista = new List<Workflow_Permisos>();
        List<TipoPerfil> perfiles = oServUsuario.TraerTodosPerfiles().ToList();

        foreach (TipoPerfil p in perfiles)
        {
            Workflow_Permisos permiso = oServWorkflow_Permisos.TraerXEstadoYPerfil(p.Id, EntityId);
            if (permiso == null)
            {
                permiso = new Workflow_Permisos();
                permiso.IdPerfil = p.Id;
                permiso.IdWorkflowEstado = EntityId;
                permiso.PuedeAvanzar = false;
                permiso.PuedeGrabar = false;
                permiso.PuedeVer = false;
                permiso.PuedeVolver = false;

                oServWorkflow_Permisos.Agregar(permiso);
            }

            lista.Add(permiso);
        }

        return lista;
    }

    #endregion
    #endregion
}