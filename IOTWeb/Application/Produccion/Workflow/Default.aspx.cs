﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IOT.Domain;
using System.Data;
using IOT.Services;




public partial class Application_Produccion_Workflow_Default : PageBase
{
    #region Propiedades & Variables
    private ServWorkflow oServWorkflow = Services.Get<ServWorkflow>();
    private ServTipoPerfilMenues oServPerfilMenus = Services.Get<ServTipoPerfilMenues>();
    private ServWorkflow_Ruta oServWorkflow_Ruta = Services.Get<ServWorkflow_Ruta>();
    private ServWorkFlow_Estado OservWorkflowEstado = Services.Get<ServWorkFlow_Estado>();
    #endregion

    #region "Main"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (base.IdUsuarioLogueado == -1)
        {
            Response.Redirect("~/Application/Login/Default.aspx", false);
            return;
        }
        if (!Page.IsPostBack)
            InicializacionPantalla();
    }
    private void InicializacionPantalla()
    {
        CargoPermisosdePantalla();
        DireccionOrdenacion = string.Empty;
        VisibilidadPaneles(true, false);
        this.CargoGrilla("Nombre ascending", this.ObtenerFiltro());
    }

    private void CargoPermisosdePantalla()
    {
        var tipoPerfilMenu = oServPerfilMenus.TraerTodosTipoPefilMenuesPorPerfil(IdPerfilUsuarioLogueado).Where(p => p.oMenu.PathPagina.Contains("/Produccion/Workflow"));
        if (tipoPerfilMenu.Count() != 0)
        {
            List<TipoPerfilMenues> t = tipoPerfilMenu.ToList();
            btnAgregar.Visible = t.First().PuedeGrabar;
            btnGrabar.Visible = t.First().PuedeGrabar;
        }
        else
            Response.Redirect("~/Application/Default.aspx", false);

    }
    #endregion

    #region "Métodos - Generales Pantalla"
    private void VisibilidadPaneles(bool pPnlGrilla, bool pPnlDetalle)
    {
        pnlGrilla.Visible = pPnlGrilla;
        pnlDetalles.Visible = pPnlDetalle;
        pnlSuccess.Visible = false;
    }
    /// <summary>
    ///  Metodo que carga los datos del usercontrol de usrUltimaModificacion
    /// </summary>
    private void CargarUserControl(long pIdUsuarioMofifico, DateTime pFechaUltimaModificacion)
    {
        usrUltimaModificacion1.FecUltimaModificacion = pFechaUltimaModificacion.ToString();
        usrUltimaModificacion1.IdUsuarioModifico = pIdUsuarioMofifico;
        usrUltimaModificacion1.UltimaModificacionBind();
    }
    #endregion

    #region Metodos - Grilla

    /// <summary>
    /// Metodo que obtiene los datos establecidos de los filtros.
    /// </summary>
    /// <returns>Metodo que devuelve un objeto con los valores del filtro.</returns>
    private Workflow ObtenerFiltro()
    {
        Workflow oObj = null;
        return oObj;
    }

    /// <summary>
    /// Metodo que obtiene el ordenamiento de las columnas.
    /// </summary>
    private void ObtenerDireccionOrden(GridViewSortEventArgs evt)
    {
        if (DireccionOrdenacion == "")
        {
            DireccionOrdenacion = "ascending";
        }
        else
        {
            if (DireccionOrdenacion == "ascending")
                DireccionOrdenacion = "descending";
            else
                DireccionOrdenacion = "ascending";
        }
    }


    /// <summary>
    /// Metodo que cargs los datos en la grilla de la pagina.
    /// </summary>
    /// <param name="pOrderBy">Criterio de ordenamiento de las columnas</param>
    /// <param name="pObj">Objeto Viaje para determinar si existe algun filtro a aplicar a la grilla</param>
    private void CargoGrilla(string pOrderBy, Workflow pObj)
    {
        CriterioOrdenacion = pOrderBy;
        List<Workflow> wQuery = null;
        try
        {
            if (pObj == null)
            {
                wQuery = oServWorkflow.TraerTodos(null).OrderBy(pOrderBy).ToList();
            }
            else
            {
                wQuery = oServWorkflow.TraerTodos(null).OrderBy(pOrderBy).ToList();
            }

            grdGrilla.DataSource = wQuery;
            grdGrilla.DataBind();
            lblCantidadFilas.Text = wQuery.Count().ToString();
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargoGrilla", HttpContext.Current.Request.Url.LocalPath);
        }
        finally
        {
            wQuery = null;
        }
    }

    protected void grdGrilla_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdGrilla.PageIndex = e.NewPageIndex;
        this.CargoGrilla(CriterioOrdenacion, this.ObtenerFiltro());
    }

    protected void grdGrilla_Sorting(object sender, GridViewSortEventArgs e)
    {
        this.ObtenerDireccionOrden(e);
        this.CargoGrilla(string.Format("{0} {1}", e.SortExpression, DireccionOrdenacion), this.ObtenerFiltro());
    }

    protected void grdGrilla_RowCommand(Object sender, CommandEventArgs e)
    {
        if (e.CommandName == "Editar")
        {
            base.EntityId = Convert.ToInt64(e.CommandArgument);
            base.EsNuevo = false;
            litNombre.Text = "Editar Workflow";
            lblId.Text = EntityId.ToString();
            this.CargarControles();
            usrUltimaModificacion1.Visible = true;
            this.VisibilidadPaneles(false, true);
        }
    }
    #endregion

    #region "Eventos Botones"

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        try
        {
            this.CargoGrilla("Nombre ascending", this.ObtenerFiltro());

        }
        catch (SystemException ex)
        {
            errorMensaje.ErrorBind(ex, "btnBuscar_Click", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    protected void btnAgregar_Click(object sender, EventArgs e)
    {
        TituloForm = "Nuevo Workflow";
        this.CrearNuevo();
    }

    protected void btnGrabar_ServerClick(object sender, EventArgs e)
    {
        Page.Validate();
        if (Page.IsValid)
        {
            this.GrabarDatos();
            pnlSuccess.Visible = true;
            litSuccess.Text = "Los datos se guardaron correctamente";
        }
    }

    protected void btnSalir_ServerClick(object sender, EventArgs e)
    {
        this.VisibilidadPaneles(true, false);
    }

    #endregion

    #region Metodos - Vista Detalle
    /// <summary>
    /// Metodo que carga los datos de los controles.
    /// </summary>
    private void CargarControles()
    {
        try
        {
            var oObj = oServWorkflow.TraerUnicoXId(base.EntityId);
            lblId.Text = base.EntityId.ToString();
            txtNombre.Text = oObj.Nombre;
            txtDescripcion.Text = oObj.Descripcion;
            chkInactivo.Checked = oObj.Inactivo;
            if (chkInactivo.Checked)
            {
                pnlInactivo.Visible = true;
                txtInactivoFecha.Text = oObj.InactivoFecha.ToShortDateString();
                txtInactivoMotivo.Text = oObj.InactivoComentario;
            }
            else
            {
                pnlInactivo.Visible = false;
                txtInactivoFecha.Text = "";
                txtInactivoMotivo.Text = "";
            }

            ddlDestino.Items.Clear();
            ddlDestino.Items.Add("Produccion");
            ddlDestino.Items.Add("Servicio Técnico");
            ddlDestino.Items.FindByText(oObj.Destino).Selected = true;

            this.CargarUserControl(oObj.idUsuarioCreo, oObj.FechaUltimaModificacion);
            pnlRuta.Visible = true;
            CargoGrillaRuta("Nombre ascending", this.ObtenerFiltroRuta());
            LimpiarRuta();
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargarControles", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    /// <summary>
    /// Metodo que guarda los datos establecidos en la pagina.
    /// </summary>
    private void GrabarDatos()
    {
        try
        {
            Workflow oObj = ObtenerDatos();

            if (ValidoObjeto(oObj))
            //if (base.ValidateModel(oObj, ref this.frmDetalles))
            {
                if (EsNuevo)
                {
                    EntityId = oServWorkflow.Agregar(oObj);
                }
                else
                {
                    oServWorkflow.Actualizar(oObj);
                }
                CargoGrilla("Nombre ascending", this.ObtenerFiltro());
                CargarControles();
                pnlRuta.Visible = true;
                CargoGrillaRuta("Nombre ascending", this.ObtenerFiltroRuta());
                LimpiarRuta();

            }
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "GrabarDatos", HttpContext.Current.Request.Url.LocalPath);
        }
    }



    /// <summary>
    /// Obtiene los datos de la pagina
    /// </summary>
    /// <returns>Retorna un objecto con los datos establecidos en la pagina</returns>
    private Workflow ObtenerDatos()
    {
        Workflow oObj = null;
        try
        {
            oObj = new Workflow();
            var oUsuario = base.TraerUsuarioLogeado();

            if (!EsNuevo)
            {
                oObj = oServWorkflow.TraerUnicoXId(base.EntityId);
            }
            else
                oObj.idUsuarioCreo = oUsuario.Id;

            oObj.Destino = ddlDestino.SelectedItem.Text;
            oObj.Nombre = txtNombre.Text.Trim();
            oObj.Descripcion = txtDescripcion.Text;
            oObj.Inactivo = chkInactivo.Checked;

            if (chkInactivo.Checked)
            {
                oObj.InactivoFecha = Convert.ToDateTime(txtInactivoFecha.Text);
                oObj.InactivoComentario = txtInactivoMotivo.Text;
            }
            else
            {
                oObj.InactivoComentario = "";
            }

            oObj.FechaUltimaModificacion = DateTime.Now;
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "ObtenerDatos", HttpContext.Current.Request.Url.LocalPath);
        }
        return oObj;
    }

    /// <summary>
    /// Metodo que establece los datos en la pagina para crear un nuevo objecto TipoPerfil
    /// </summary>
    private void CrearNuevo()
    {
        this.Limpiar();
        ddlDestino.Items.Clear();
        ddlDestino.Items.Add("Produccion");
        ddlDestino.Items.Add("Servicio Técnico");
        litNombre.Text = "Nuevo Workflow";
        usrUltimaModificacion1.Visible = false;
        this.VisibilidadPaneles(false, true);
        EsNuevo = true;
        pnlRuta.Visible = false;
    }

    /// <summary>
    /// Metodo que limpia los datos establecidos en la pagina.
    /// </summary>
    private void Limpiar()
    {
        lblId.Text = "0";
        txtNombre.Text = string.Empty;
        txtDescripcion.Text = string.Empty;
        txtInactivoMotivo.Text = string.Empty;
        chkInactivo.Checked = false;
        chkInactivo_CheckedChanged(null, null);
        pnlError.Visible = false;

    }

    #endregion

    #region Validacion de Campos
    /// <summary>
    /// Metodo que valida el tramite a cargar
    /// </summary>
    private bool ValidoObjeto(Workflow pObj)
    {
        bool comodin = true;
        pnlError.Visible = false;
        string error = string.Empty;

        if (pObj.Nombre.Trim().Length == 0)
        {
            error = "Debe ingresar un Nombre Valido <br>";
            comodin = false;
        }
        else
        {
            if (EsNuevo)
            {
                //Valido que no existe un componente con ese nombre
                Workflow oObj = oServWorkflow.TraerTodos(null).Where(p => p.Nombre == pObj.Nombre.Trim()).FirstOrDefault();
                if (oObj != null)
                {
                    error = error + "Ya Existe un workflow con ese Nombre, por favor reintente <br>";
                    comodin = false;
                }
            }
            else
            {
                List<Workflow> oObj = oServWorkflow.TraerTodos(null).Where(p => p.Nombre == pObj.Nombre.Trim()).ToList();
                oObj.RemoveAll(t => t.Id == pObj.Id);
                if (oObj.Count != 0)
                {
                    error = error + "Ya Existe un workflow con ese Nombre, por favor reintente <br>";
                    comodin = false;
                }

            }
        }

        //Valido que se haya puesto un comentario y una fecha si el registro esta inactivo
        if (pObj.Inactivo == true)
        {
            if ((pObj.InactivoComentario.Trim().Length == 0) || (pObj.InactivoFecha == null))
            {
                error = error + "Debe ingresarse una Fecha y un Commentario <br>";
                comodin = false;
            }

        }

        if (!string.IsNullOrEmpty(error))
        {
            pnlError.Visible = true;
            litError.Text = error;
        }

        return comodin;
    }
    #endregion

    #region Eventos AutoPostBack
    protected void chkInactivo_CheckedChanged(object sender, EventArgs e)
    {
        if (chkInactivo.Checked)
        {
            pnlInactivo.Visible = true;
            txtInactivoMotivo.Text = "";
            txtInactivoFecha.Text = "";
        }
        else
        {
            pnlInactivo.Visible = false;
            txtInactivoMotivo.Text = "";
            txtInactivoFecha.Text = "";
        }

    }

    #endregion

    #region Ruta
    #region Metodos - Grilla

    /// <summary>
    /// Metodo que obtiene los datos establecidos de los filtros.
    /// </summary>
    /// <returns>Metodo que devuelve un objeto con los valores del filtro.</returns>
    private Workflow_Ruta ObtenerFiltroRuta()
    {
        Workflow_Ruta oObj = null;
        return oObj;
    }

    /// <summary>
    /// Metodo que obtiene el ordenamiento de las columnas.
    /// </summary>
    private void ObtenerDireccionOrdenRuta(GridViewSortEventArgs evt)
    {
        if (DireccionOrdenacion == "")
        {
            DireccionOrdenacion = "ascending";
        }
        else
        {
            if (DireccionOrdenacion == "ascending")
                DireccionOrdenacion = "descending";
            else
                DireccionOrdenacion = "ascending";
        }
    }


    /// <summary>
    /// Metodo que cargs los datos en la grilla de la pagina.
    /// </summary>
    /// <param name="pOrderBy">Criterio de ordenamiento de las columnas</param>
    /// <param name="pObj">Objeto Viaje para determinar si existe algun filtro a aplicar a la grilla</param>
    private void CargoGrillaRuta(string pOrderBy, Workflow_Ruta pObj)
    {
        CriterioOrdenacion = pOrderBy;
        List<Workflow_Ruta> wQuery = null;
        try
        {
            if (pObj == null)
            {
                wQuery = oServWorkflow_Ruta.TraerTodos().Where(p => p.IdWorkflow == int.Parse(lblId.Text)).OrderBy(p => p.NroOrden).ToList();
            }
            else
            {
                wQuery = oServWorkflow_Ruta.TraerTodos().OrderBy(p => p.oWorkflow.Nombre).ToList();
            }

            grdRuta.DataSource = wQuery;
            grdRuta.DataBind();
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargoGrillaRuta", HttpContext.Current.Request.Url.LocalPath);
        }
        finally
        {
            wQuery = null;
        }
    }

    protected void grdRuta_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdRuta.PageIndex = e.NewPageIndex;
        this.CargoGrillaRuta(CriterioOrdenacion, this.ObtenerFiltroRuta());
    }

    protected void grdRuta_Sorting(object sender, GridViewSortEventArgs e)
    {
        this.ObtenerDireccionOrden(e);
        this.CargoGrillaRuta(string.Format("{0} {1}", e.SortExpression, DireccionOrdenacion), this.ObtenerFiltroRuta());
    }

    protected void grdRuta_RowCommand(Object sender, CommandEventArgs e)
    {
        if (e.CommandName == "EditarRuta")
        {
            LimpiarRuta();
            lblidruta.Text = e.CommandArgument.ToString();
            this.CargarControlesRuta();

        }
    }
    #endregion

    #region "Eventos Botones"

    protected void btnGrabarRuta_ServerClick(object sender, EventArgs e)
    {
        Page.Validate();
        if (Page.IsValid)
        {
            this.GrabarDatosRuta();
        }
    }

    protected void btnSalirRuta_ServerClick(object sender, EventArgs e)
    {
        this.LimpiarRuta();
    }

    #endregion

    #region Metodos - Vista Detalle
    /// <summary>
    /// Metodo que carga los datos de los controles.
    /// </summary>
    private void CargarControlesRuta()
    {
        try
        {
            var oObj = oServWorkflow_Ruta.TraerUnicoXId(Convert.ToInt64(lblidruta.Text));
            txtNroOrden.Text = oObj.NroOrden.ToString();
            CargoDDLEstado();
            cbxRegreso.Checked = oObj.Regreso;
            CargoDDLEstadoAnterior();
            CargoDDLEstadoSiguiente();
            ddlEstado.Items.FindByText(oObj.oWorkflowEstado.Nombre).Selected = true;
            if (oObj.IdEstadoAnterior != 0)
                ddlEstadoAnterior.Items.FindByText(oObj.oEstadoAnterior.Nombre).Selected = true;
            else
                ddlEstadoAnterior.Items.FindByText("").Selected = true;
            if (oObj.IdEstadoSiguiente != 0)
                ddlEstadoSiguiente.Items.FindByText(oObj.oEstadoSiguiente.Nombre).Selected = true;
            else
                ddlEstadoSiguiente.Items.FindByText("").Selected = true;
            if (oObj.IdEstadoSiguienteB != 0)
                ddlEstadoSiguienteB.Items.FindByText(oObj.oEstadoSiguienteB.Nombre).Selected = true;
            else
                ddlEstadoSiguienteB.Items.FindByText("").Selected = true;


        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargarControles", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    /// <summary>
    /// Metodo que guarda los datos establecidos en la pagina.
    /// </summary>
    private void GrabarDatosRuta()
    {
        try
        {
            Workflow_Ruta oObj = ObtenerDatosRuta();

            if (ValidoObjetoRuta(oObj))
            //if (base.ValidateModel(oObj, ref this.frmDetalles))
            {
                if (EsNuevo)
                {
                    EntityId = oServWorkflow_Ruta.Agregar(oObj);
                }
                else
                {
                    oServWorkflow_Ruta.Actualizar(oObj);
                    if(lblId.Text != "0")
                    {
                        LimpiarRuta();
                    }
                }
                this.CargoGrillaRuta(base.CriterioOrdenacion, this.ObtenerFiltroRuta());
            }
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "GrabarDatos", HttpContext.Current.Request.Url.LocalPath);
        }
    }



    /// <summary>
    /// Obtiene los datos de la pagina
    /// </summary>
    /// <returns>Retorna un objecto con los datos establecidos en la pagina</returns>
    private Workflow_Ruta ObtenerDatosRuta()
    {
        Workflow_Ruta oObj = null;
        try
        {
            oObj = new Workflow_Ruta();
            if (lblidruta.Text != "0")
            {
                oObj = oServWorkflow_Ruta.TraerUnicoXId(Convert.ToInt64(lblidruta.Text));
            }
            oObj.IdWorkflow = Convert.ToInt64(lblId.Text);
            oObj.IdWorkflowEstado = Convert.ToInt64(ddlEstado.SelectedValue);
            if (!string.IsNullOrEmpty(ddlEstadoAnterior.Text))
                oObj.IdEstadoAnterior = Convert.ToInt64(ddlEstadoAnterior.SelectedValue);
            else
            {
                oObj.IdEstadoAnterior = 0;
            }
            if (!string.IsNullOrEmpty(ddlEstadoSiguiente.Text))
                oObj.IdEstadoSiguiente = Convert.ToInt64(ddlEstadoSiguiente.SelectedValue);
            else
            {
                oObj.IdEstadoSiguiente = 0;
            }
            if (!string.IsNullOrEmpty(ddlEstadoSiguienteB.Text))
                oObj.IdEstadoSiguienteB = Convert.ToInt64(ddlEstadoSiguienteB.SelectedValue);
            else
            {
                oObj.IdEstadoSiguienteB = 0;
            }
            oObj.NroOrden = int.Parse(txtNroOrden.Text);
            oObj.Regreso = cbxRegreso.Checked;
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "ObtenerDatos", HttpContext.Current.Request.Url.LocalPath);
        }
        return oObj;
    }

    /// <summary>
    /// Metodo que limpia los datos establecidos en la pagina.
    /// </summary>
    private void LimpiarRuta()
    {
        lblidruta.Text = "0";
        txtNroOrden.Text = string.Empty;
        CargoDDLEstado();
        CargoDDLEstadoAnterior();
        CargoDDLEstadoSiguiente();
        ddlEstadoAnterior.Items.FindByText("").Selected = true;
        ddlEstadoSiguiente.Items.FindByText("").Selected = true;
        ddlEstadoSiguienteB.Items.FindByText("").Selected = true;
        pnlErrorRuta.Visible = false;
        ddlDestino.Items.Clear();
    }

    #endregion

    #region Metodos DDL

    private void CargoDDLEstado()
    {
        ddlEstado.Items.Clear();
        List<Workflow_Estado> wQuery;

        wQuery = OservWorkflowEstado.TraerTodos().ToList();


        ddlEstado.DataSource = wQuery;
        ddlEstado.DataTextField = "Nombre";
        ddlEstado.DataValueField = "Id";
        ddlEstado.DataBind();
    }

    private void CargoDDLEstadoAnterior()
    {
        ddlEstadoAnterior.Items.Clear();
        List<Workflow_Estado> wQuery;

        wQuery = OservWorkflowEstado.TraerTodos().ToList();


        ddlEstadoAnterior.DataSource = wQuery;
        ddlEstadoAnterior.DataTextField = "Nombre";
        ddlEstadoAnterior.DataValueField = "Id";
        ddlEstadoAnterior.DataBind();
        ddlEstadoAnterior.Items.Add("");
    }

    private void CargoDDLEstadoSiguiente()
    {
        ddlEstadoSiguiente.Items.Clear();
        List<Workflow_Estado> wQuery;

        wQuery = OservWorkflowEstado.TraerTodos().ToList();


        ddlEstadoSiguiente.DataSource = wQuery;
        ddlEstadoSiguiente.DataTextField = "Nombre";
        ddlEstadoSiguiente.DataValueField = "Id";
        ddlEstadoSiguiente.DataBind();
        ddlEstadoSiguiente.Items.Add("");

        ddlEstadoSiguienteB.Items.Clear();
        ddlEstadoSiguienteB.DataSource = wQuery;
        ddlEstadoSiguienteB.DataTextField = "Nombre";
        ddlEstadoSiguienteB.DataValueField = "Id";
        ddlEstadoSiguienteB.DataBind();
        ddlEstadoSiguienteB.Items.Add("");
    }
    #endregion

    #region Validacion de Campos
    /// <summary>
    /// Metodo que valida el tramite a cargar
    /// </summary>
    private bool ValidoObjetoRuta(Workflow_Ruta rObj)
    {
        bool comodin = true;
        pnlError.Visible = false;
        Workflow_Estado estado = OservWorkflowEstado.TraerUnicoXId(rObj.IdWorkflowEstado);
        List<Workflow_Ruta> ruta;
        string error = string.Empty;

        ///CONTROLO QUE HAYA UN NUMERO DE ORDEN Y QUE NO EXISTA YA 
        if (rObj.NroOrden == 0)
        {
            error = "Ingrese un numero de orden <br>";
            comodin = false;
        }

        ///CONTROLO QUE NO HAYA YA UNA "RUTA" CREADA PARA ESE ESTADO EN ESTE MISMO WORKFLOW
        ruta = oServWorkflow_Ruta.TraerTodos().Where(p => p.IdWorkflow == rObj.IdWorkflow && p.IdWorkflowEstado == rObj.IdWorkflowEstado).ToList();

        if (ruta.Count != 0)
        {
            ruta.Remove(rObj);
            if (ruta.Count != 0)
            {
                error = error + "Ingrese un estado que no haya sido cargado ya para este workflow <br>";
                comodin = false;
            }
        }


        /// CONTROLO SI PUEDE O NO HABER ESTADO SIGUIENTE Y SI ESTE NO ES IGUAL AL ESTADO ACTUAL
        if (estado.EsFinal && rObj.IdEstadoSiguiente != 0)
        {
            error = error + "Eligió un estado final, no puede tener estado siguiente <br>";
            comodin = false;
        }
        else
        {
            if (rObj.IdEstadoSiguiente == rObj.IdWorkflowEstado)
            {
                error = error + "El estado Anterior y el actual no pueden ser iguales <br>";
                comodin = false;
            }
        }

        /// CONTROLO SI PUEDE HABER ESTADO ANTERIOR Y QUE ESTE NO  SEA IGUAL A LOS OTROS DOS
        if (estado.EsInicial && rObj.IdEstadoAnterior != 0)
        {
            error = error + "Eligió un estado inicial, no puede tener estado anterior <br>";
            comodin = false;
        }
        else
        {
            if (rObj.IdEstadoAnterior == rObj.IdWorkflowEstado || (rObj.IdEstadoSiguiente == rObj.IdEstadoAnterior && rObj.IdEstadoSiguiente != 0))
            {
                error = error + "Los estados no pueden coincidir <br>";
                comodin = false;
            }
        }

        if (!string.IsNullOrEmpty(error))
        {
            pnlErrorRuta.Visible = true;
            LitErrorRuta.Text = error;
        }

        return comodin;
    }
    #endregion
    #endregion
}