﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs"  Inherits="Application_Produccion_Workflow_Ejecutor_Default" %>
<%@ Register Src="~/UserControl/usrUltimaModificacion.ascx" TagName="usrUltimaModificacion" TagPrefix="uc1" %>
<%@ Register TagPrefix="usrError" TagName="Error" Src="~/UserControl/Error.ascx" %>
<%@ Register Src="~/UserControl/userProcesando.ascx" TagPrefix="uc1" TagName="userProcesando" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <%--Vista Grilla--%>
    <asp:Panel ID="pnlGrilla" runat="server">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                </div>
                <div class="title_right">
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Ejecutores</h2>
                            <div class="pull-right">
                                    <button type="submit" id="btnAgregar" runat="server" causesvalidation="False" class="btn btn-round btn-primary" onserverclick="btnAgregar_Click">
                                        <i class="fa fa-plus-circle"></i> Agregar Nuevo
                                    </button>
                                </div>
                            <div class="clearfix"></div>
                        </div>
                        <%--FILTRO--%>
                        <div class="x_content">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Filtro</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-horizontal form-label-left pull-left">
                                    <%--Aca van los filtros que se quieran poner --%>                                 
                                </div> 
                            </div>
                            <%--GRILLA--%>
                            <asp:GridView ID="grdGrilla" runat="server" OnPageIndexChanging="grdGrilla_PageIndexChanging"
                                OnSorting="grdGrilla_Sorting" OnRowCommand="grdGrilla_RowCommand"  AllowSorting ="true" 
                                AutoGenerateColumns="False" CssClass="table table-striped table-bordered">
                                <Columns>
                                    <asp:BoundField DataField="Id" HeaderText="ID" SortExpression="Id" Visible="True" />
                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre" SortExpression="Nombre" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <div class="pull-right">
                                                <asp:LinkButton ID="btnEditar" runat="server" CommandName="Editar" CommandArgument='<%#Eval("Id") %>' 
                                                    CssClass="btn btn-round"> <i class="fa fa-edit"></i> Editar</asp:LinkButton>
                                            </div>
                                        </ItemTemplate>
                                        <ItemStyle Width="10%" />
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle CssClass="pgr" />
                            </asp:GridView>
                            <br />
                            <div class="alert alert-success alert-dismissible fade in" role="alert">
                                <div class="pull-left">
                                    Total de Registros:
                                    <asp:Label ID="lblCantidadFilas" runat="server"></asp:Label>
                                </div>
                                
                                <br /> 
                                <br /> 
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>

    <%--Vista Detalles--%>
    <asp:Panel ID="pnlDetalles" runat="server">
        <div class="col-md-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><asp:Literal ID="litNombre" runat="server" Text='<%#TituloForm%>' /></h2>
                    <div class="clearfix"></div>
                </div>
                <%--PANEL DE ERRORES--%>
                <div class="alert alert-danger alert-dismissible fade in" role="alert" runat="server" id="pnlError" visible="false">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <strong><i class="fa fa-warning"></i>Atencion!!!</strong>
                    <br />
                    <asp:Literal ID="litError" runat="server" Text=""></asp:Literal>
                </div>
                <div class="clearfix"></div>
                <div class="x_content">
                    <br />
                    <div class="form-horizontal form-label-left">
                        <div class="form-group">
                            <asp:Label ID="lblIdTitu" Text="ID" runat="server" Visible="true" class="control-label col-md-3 col-sm-3 col-xs-12">Id:</asp:Label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <asp:Label ID="lblId" runat="server" Visible="true" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" Text="Nombre" class="control-label col-md-3 col-sm-3 col-xs-12"></asp:Label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <asp:TextBox ID="txtNombre" runat="server" class="form-control">
                                </asp:TextBox>
                                <asp:Label Text="" runat="server" ID="lvlValNombre" CssClass="help-inline alert" />
                            </div>
                        </div>
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <div class="form-group">
                                    <asp:Label runat="server" Text="Inactivo" class="control-label col-md-3 col-sm-3 col-xs-12"></asp:Label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:CheckBox ID="chkInactivo" runat="server"  class="form-control" AutoPostBack="true" OnCheckedChanged="chkInactivo_CheckedChanged">
                                        </asp:CheckBox>
                                        <asp:Label Text="" runat="server" ID="lvlValInactivo" CssClass="help-inline alert" />
                                    </div>
                                </div>
                                <div id="pnlInactivo" runat="server" visible="false">   
                                    <div class="form-group">
                                        <asp:Label runat="server" Text="Fecha Inactivo" class="control-label col-md-3 col-sm-3 col-xs-12"></asp:Label>
                                        <div class="col-md-2 col-sm-2 col-xs-3">
                                            <asp:UpdatePanel runat="server">
                                                <ContentTemplate>
                                                    <div class='input-group date dtp' id='dtpInactivoFecha'>
                                                        <asp:TextBox runat="server" ID="txtInactivoFecha" CssClass="form-control det-control" />
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label runat="server" Text="Motivo Inactivo" class="control-label col-md-3 col-sm-3 col-xs-12"></asp:Label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <asp:TextBox ID="txtInactivoMotivo" runat="server" Placeholder="Ingrese un motivo" class="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <uc1:usrUltimaModificacion ID="usrUltimaModificacion1" runat="server" />
                        <div class="pull-right">
                            <button type="submit" id="btnGrabar" runat="server" causesvalidation="False" class="btn btn-round btn-primary" onserverclick="btnGrabar_ServerClick">
                                <i class="fa fa-check"></i> Grabar
                            </button>
                            <button type="submit" id="btnSalir" runat="server" causesvalidation="False" class="btn btn-round btn-alert" onserverclick="btnSalir_ServerClick">
                                <i class="fa fa-arrow-circle-left"></i> Volver
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <usrError:Error ID="errorMensaje" runat="server" />
    <uc1:userProcesando runat="server" ID="userProcesando" />
</asp:Content>
