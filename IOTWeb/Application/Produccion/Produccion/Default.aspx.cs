﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IOT.ORM;
using IOT.Domain;
using System.Transactions;
using System.Web.Security;
using System.Data;
using IOT.Services;
using System.Web.UI.HtmlControls;

public partial class Application_Produccion_Produccion_Default : PageBase
{
    #region Propiedades & Variables
    private ServProduccion oServProduccion = Services.Get<ServProduccion>();
    private ServTipoPerfilMenues oServPerfilMenus = Services.Get<ServTipoPerfilMenues>();
    private ServWorkFlow_Estado oServEstadoWorkflow = Services.Get<ServWorkFlow_Estado>();
    private ServWorkflow oServWorkflow = Services.Get<ServWorkflow>();
    private ServWorkflow_Permisos oServWorkflowPermiso = Services.Get<ServWorkflow_Permisos>();
    private ServWorkflow_Ruta oServWorkflowRuta = Services.Get<ServWorkflow_Ruta>();
    private ServWorkflow_Permisos oservPermiso = Services.Get<ServWorkflow_Permisos>();
    private ServOrdenProduccion oServOrdenProd = Services.Get<ServOrdenProduccion>();
    private long IdOP { get; set; }
    private int Bandera { get; set; }
    #endregion

    #region "Main"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (base.IdUsuarioLogueado == -1)
        {
            Response.Redirect("~/Application/Login/Default.aspx", false);
            return;
        }
        if (!Page.IsPostBack)
            InicializacionPantalla();
    }

    private void InicializacionPantalla()
    {
        CargoPermisosdePantalla();
        Bandera = Convert.ToInt16(Request.QueryString["Bandera"]);

        DireccionOrdenacion = string.Empty;
        VisibilidadPaneles(true, false);


        if (Bandera == 0)
        {
            btnVolverOP.Visible = false;
            Session["Op"] = 0;
        }
        else if (Bandera == 1)
        {
            Session["filtroEstado"] = string.Empty;
            Session["filtroOP"] = string.Empty;
        }
        else
        {
            long id = Convert.ToInt64(Request.QueryString["id"]);
            base.EntityId = id;
            base.EsNuevo = false;
            litNombre.Text = "Ver Producto en Produccion";
            lblId.Text = EntityId.ToString();
            this.CargarControles();
            this.VisibilidadPaneles(false, true);
        }

        CargarDDLFiltro();
        this.CargoGrilla("IdOrdenProduccion ascending", this.ObtenerFiltro());
        LitNombreGrilla.Text = "Mis Producciones";
        txtFiltroNroSerie.Text = string.Empty;

    }

    private void CargoPermisosdePantalla()
    {
        var tipoPerfilMenu = oServPerfilMenus.TraerTodosTipoPefilMenuesPorPerfil(IdPerfilUsuarioLogueado).Where(p => p.oMenu.PathPagina.Contains("/Produccion/Produccion"));
        if (tipoPerfilMenu.Count() == 0)
        {
            Response.Redirect("~/Application/Default.aspx", false);
        }
    }
    #endregion

    #region "Métodos - Generales Pantalla"
    private void VisibilidadPaneles(bool pPnlGrilla, bool pPnlDetalle)
    {
        pnlGrilla.Visible = pPnlGrilla;
        pnlDetalles.Visible = pPnlDetalle;
    }


    /// <summary>
    /// Metodo que carga la DDL del filtro por estado y op
    /// </summary>
    private void CargarDDLFiltro()
    {
        string filtroEstado = (string)Session["filtroEstado"];
        string filtroOP = (string)Session["filtroOP"];


        ddlFiltroEstado.Items.Clear();
        List<Workflow> asistWorkflow = oServWorkflow.TraerTodos(null).Where(w => w.Destino == "Produccion").ToList();
        List<Workflow_Ruta> asistRuta = new List<Workflow_Ruta>();
        foreach (Workflow w in asistWorkflow)
        {
            List<Workflow_Ruta> r = oServWorkflowRuta.TraerTodos().Where(x => x.IdWorkflow == w.Id).ToList();
            asistRuta.AddRange(r);
        }
        List<Workflow_Estado> comodin = new List<Workflow_Estado>();

        foreach (Workflow_Ruta r in asistRuta)
        {
            if (!comodin.Contains(r.oWorkflowEstado))
                comodin.Add(r.oWorkflowEstado);
        }

        List<Workflow_Permisos> asistpermiso = oServWorkflowPermiso.TraerXPerfil(IdPerfilUsuarioLogueado).ToList();
        List<Workflow_Estado> asistWQuery = new List<Workflow_Estado>();

        foreach (Workflow_Permisos p in asistpermiso)
        {
            if (p.PuedeVer)
                asistWQuery.Add(p.oWorkflowEstado);
        }

        List<Workflow_Estado> wQuery2 = new List<Workflow_Estado>();
        foreach (Workflow_Estado e in asistWQuery)
        {
            if (comodin.Contains(e))
                wQuery2.Add(e);
        }


        ddlFiltroEstado.DataSource = wQuery2;
        ddlFiltroEstado.DataTextField = "Nombre";
        ddlFiltroEstado.DataValueField = "Id";
        ddlFiltroEstado.DataBind();

        ddlFiltroEstado.Items.Add("");
        if (string.IsNullOrEmpty(filtroEstado))
            ddlFiltroEstado.Items.FindByText("").Selected = true;
        else
            ddlFiltroEstado.Items.FindByText(filtroEstado).Selected = true;    


        ddlFiltroOP.Items.Clear();
        List<OrdenProd> wQueryOP = oServOrdenProd.TraerTodos().Where(o => o.Estado != "1").ToList();


        ddlFiltroOP.DataSource = wQueryOP;
        ddlFiltroOP.DataTextField = "Nombre";
        ddlFiltroOP.DataValueField = "Id";
        ddlFiltroOP.DataBind();

        ddlFiltroOP.Items.Add("");
        if (string.IsNullOrEmpty(filtroOP))
            ddlFiltroOP.Items.FindByText("").Selected = true;
        else
            ddlFiltroOP.Items.FindByText(filtroOP).Selected = true;
    }

    #endregion

    #region Metodos - Grilla

    /// <summary>
    /// Metodo que obtiene los datos establecidos de los filtros.
    /// </summary>
    /// <returns>Metodo que devuelve un objeto con los valores del filtro.</returns>
    private Produccion ObtenerFiltro()
    {
        Produccion oObj = new Produccion();
        if (!string.IsNullOrEmpty(txtFiltroNroSerie.Text))
        {
            if (!(txtFiltroNroSerie.Text.Length < 9))
            {
                string NroSerieAASS = txtFiltroNroSerie.Text.Substring(0, 4);
                string NroSeriePPPC = txtFiltroNroSerie.Text.Substring(4, 4);
                string NroSerieContador = txtFiltroNroSerie.Text.Substring(8);
                oObj = oServProduccion.TraerTodos().Where(p => p.NroSerieAASS == NroSerieAASS && p.NroSeriePPPC == NroSeriePPPC && p.NroSerieContador == NroSerieContador).FirstOrDefault();
            }
            if (oObj.Id == 0)
            {
                pnlError.Visible = true;
                litError.Text = "Numero de Serie incorrecto";
            }
        }
        else
        {
            if (Bandera == 1)
            {
                IdOP = (long)Session["Op"];
                oObj.IdOrdenProduccion = IdOP;
                OrdenProd op = oServOrdenProd.TraerUnicoXId(IdOP);
                ddlFiltroOP.Items.FindByText("").Selected = false;
                ddlFiltroOP.Items.FindByText(op.Nombre).Selected = true;
                //Hago visible el boton que permite pasar de estado todas las produciones de una OP siempre que el filtro este activo
                btnPasarOPCompleta.Visible = true;
            }
            else
            {
                if (!string.IsNullOrEmpty(ddlFiltroOP.SelectedItem.Text))
                {
                    oObj.IdOrdenProduccion = Convert.ToInt64(ddlFiltroOP.SelectedValue);
                    btnPasarOPCompleta.Visible = true;
                }
            }

            if (!string.IsNullOrEmpty(ddlFiltroEstado.SelectedItem.Text))
            {
                oObj.IdEstadoActual = Convert.ToInt64(ddlFiltroEstado.SelectedValue);
            }
        }
        return oObj;
    }

    /// <summary>
    /// Metodo que obtiene el ordenamiento de las columnas.
    /// </summary>
    private void ObtenerDireccionOrden(GridViewSortEventArgs evt)
    {
        if (DireccionOrdenacion == "")
        {
            DireccionOrdenacion = "ascending";
        }
        else
        {
            if (DireccionOrdenacion == "ascending")
                DireccionOrdenacion = "descending";
            else
                DireccionOrdenacion = "ascending";
        }
    }


    /// <summary>
    /// Metodo que cargs los datos en la grilla de la pagina.
    /// </summary>
    /// <param name="pOrderBy">Criterio de ordenamiento de las columnas</param>
    /// <param name="pObj">Objeto Viaje para determinar si existe algun filtro a aplicar a la grilla</param>
    private void CargoGrilla(string pOrderBy, Produccion pObj)
    {
        CriterioOrdenacion = pOrderBy;
        List<Produccion> wQuery = null;
        try
        {
            wQuery = oServProduccion.TraerTodosXFiltro(IdPerfilUsuarioLogueado, pObj).OrderBy(p => p.IdEstadoActual).ToList();


            grdGrilla.DataSource = wQuery;
            grdGrilla.DataBind();
            lblCantidadFilas.Text = wQuery.Count().ToString();
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargoGrilla", HttpContext.Current.Request.Url.LocalPath);
        }
        finally
        {
            wQuery = null;
        }
    }

    protected void grdGrilla_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdGrilla.PageIndex = e.NewPageIndex;
        this.CargoGrilla(CriterioOrdenacion, this.ObtenerFiltro());
    }

    protected void grdGrilla_Sorting(object sender, GridViewSortEventArgs e)
    {
        this.ObtenerDireccionOrden(e);
        this.CargoGrilla(string.Format("{0} {1}", e.SortExpression, DireccionOrdenacion), this.ObtenerFiltro());
    }

    protected void grdGrilla_RowCommand(Object sender, CommandEventArgs e)
    {
        if (e.CommandName == "Ver")
        {
            base.EntityId = Convert.ToInt64(e.CommandArgument);
            base.EsNuevo = false;
            litNombre.Text = "Ver Producto en Produccion";
            lblId.Text = EntityId.ToString();
            this.CargarControles();
            this.VisibilidadPaneles(false, true);
        }

        if (e.CommandName == "SelectTodos")
        {
            int contador = -1;
            bool asist = true;
            int largoTabla = grdGrilla.Rows.Count;
            for (int i = 0; i < largoTabla; i++)
            {
                CheckBox seleccionado = new CheckBox();
                seleccionado = (CheckBox)grdGrilla.Rows[i].FindControl("chbxSeleccionMasiva");
                if (seleccionado.Checked == true)
                    contador++;
                seleccionado.Checked = asist;
                if (i == largoTabla - 1)
                {
                    if (contador == i)
                    {
                        asist = false;
                        i = -1;
                        contador = 0;
                    }

                }
            }
        }
    }

    protected void grdGrilla_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex >= 0)
        {
            Label mSigEstado = new Label();
            mSigEstado = ((Label)e.Row.FindControl("lblEstadoSiguiente"));

            Label mNroGabinete = new Label();
            mNroGabinete = ((Label)e.Row.FindControl("lblNroGabinete"));

            long idProduccion = Convert.ToInt64(e.Row.Cells[1].Text);
            Produccion prod = oServProduccion.TraerUnicoXId(idProduccion);

            /// Me fijo que haya un siguiente estado, y si no lo hay lleno el espacio en la tabla con ""
            Workflow_Estado estadosig = new Workflow_Estado();
            estadosig = EstadoSiguiente(prod.IdEstadoActual, prod.IdWorkflow);
            if (estadosig != null)
                mSigEstado.Text = estadosig.Nombre;
            else
                mSigEstado.Text = "";

            List<Produccion_Componentes> lstComp = oServProduccion.TraerComponentesXProduccion(prod.Id).ToList();
            if (lstComp != null)
            {
                Produccion_Componentes oComp = lstComp.Where(p => p.oComponente.Nombre.Contains("Gabinete")).FirstOrDefault();
                if (oComp != null)
                {
                    if (oComp.NroSerie != null)
                        mNroGabinete.Text = oComp.NroSerie.ToString();
                    else
                        mNroGabinete.Text = "";
                }
                   
                else
                    mNroGabinete.Text = "";
                
            }
            else
                mNroGabinete.Text = "";
            
        }
    }
    #endregion

    #region "Eventos Botones"

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        try
        {
            pnlError.Visible = false;
            btnPasarOPCompleta.Visible = false;
            this.CargoGrilla("Nombre ascending", this.ObtenerFiltro());
            userProcesando.CerrarModalProcesando();
            Session["filtroEstado"] = ddlFiltroEstado.SelectedItem.Text;
            Session["filtroOP"] = ddlFiltroOP.SelectedItem.Text;
        }
        catch (SystemException ex)
        {
            errorMensaje.ErrorBind(ex, "btnBuscar_Click", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    protected void btnGrabar_Click(object sender, EventArgs e)
    {
        Page.Validate();
        if (Page.IsValid)
        {
            GrabarDatos();
        }
    }

    protected void btnSiguienteEstado_ServerClick(object sender, EventArgs e)
    {
        try
        {
            btnPasarOPCompleta.Visible = false;
            List<Produccion> lista = new List<Produccion>();
            List<long> id = new List<long>();
            litError.Text = string.Empty;
            int largoTabla = grdGrilla.Rows.Count;
            if (!pnlDetalles.Visible)
            {
                for (int i = 0; i < largoTabla; i++)
                {
                    if (comprobarchbx(i))
                    {
                        long idProduccion = Convert.ToInt64(grdGrilla.Rows[i].Cells[1].Text);
                        Produccion prod = oServProduccion.TraerUnicoXId(idProduccion);

                        if (ValidoLista(prod, lista))
                        {
                            if (prod.oEstadoActual.Masivo || lista.Count < 2)
                            {
                                lista.Add(prod);
                                id.Add(prod.Id);
                            }
                            else
                            {
                                litError.Text = "Este estado no se puede pasar masivamente";
                                pnlError.Visible = true;
                                lista.Clear();
                                id.Clear();
                                break;
                            }
                        }
                        else
                        {
                            lista.Clear();
                            id.Clear();
                            break;
                        }
                    }
                }
            }
            else
            {
                Produccion prod = oServProduccion.TraerUnicoXId(EntityId);
                if (ValidoLista(prod, lista))
                {
                    lista.Add(prod);
                    id.Add(prod.Id);
                }

            }

            if (lista.Count > 0)
            {
                Session["listproductos"] = id;
                if (ComprobarPermiso(lista.First().IdEstadoActual, IdPerfilUsuarioLogueado))
                {
                    Response.Redirect(lista.First().oEstadoActual.Pantalla + "destino=p", false);
                }
                else
                {
                    pnlError.Visible = true;
                    litError.Text = "No tiene permiso para pasar estas producciones de estado";
                }

            }
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "Error en el Metodo para pasar de estado", HttpContext.Current.Request.Url.LocalPath);
        }

    }

    protected void btnSalirOP_ServerClick(object sender, EventArgs e)
    {
        txtFiltroNroSerie.Text = string.Empty;
        btnPasarOPCompleta.Visible = false;
        Session["Op"] = null;
        Response.Redirect("~/Application/Produccion/OrdenesProd/Default.aspx", false);
        Session["filtroEstado"] = string.Empty;
        Session["filtroOP"] = string.Empty;
    }


    protected void btnSalir_ServerClick(object sender, EventArgs e)
    {
        btnPasarOPCompleta.Visible = false;
        this.VisibilidadPaneles(true, false);
        Limpiar();

        Bandera = Convert.ToInt16(Request.QueryString["Bandera"]);
        if (Bandera == 2)
        {
            Response.Redirect("~/Application/Produccion/Produccion/Default.aspx", false);
        }
    }

    protected void btnPasarOPCompleta_Click(object sender, EventArgs e)
    {
        btnPasarOPCompleta.Visible = false;
        Produccion pObj = new Produccion();
        List<long> id = new List<long>();
        List<Produccion> wQuery = oServProduccion.TraerTodosXFiltro(IdPerfilUsuarioLogueado, ObtenerFiltro()).OrderBy(p => p.IdEstadoActual).ToList();
        List<Produccion> lista = new List<Produccion>();


        for (int i = 0; i < wQuery.Count; i++)
        {

            if (ValidoLista(wQuery[i], lista))
            {
                if (wQuery[i].oEstadoActual.Masivo || lista.Count < 2)
                {
                    lista.Add(wQuery[i]);
                    id.Add(wQuery[i].Id);
                }
                else
                {
                    litError.Text = "Este estado no se puede pasar masivamente";
                    pnlError.Visible = true;
                    lista.Clear();
                    id.Clear();
                    break;
                }
            }
        }

        if (lista.Count == wQuery.Count)
        {
            Session["listproductos"] = id;
            if (ComprobarPermiso(wQuery.First().IdEstadoActual, IdPerfilUsuarioLogueado))
            {
                Response.Redirect(wQuery.First().oEstadoActual.Pantalla + "destino=p", false);
            }
            else
            {
                pnlError.Visible = true;
                litError.Text = "No tiene permiso para pasar estas producciones de estado";
            }
        }
    }
    #endregion

    #region Metodos - Vista Detalle
    /// <summary>
    /// Metodo que carga los datos de los controles.
    /// </summary>
    private void CargarControles()
    {
        try
        {
            var oObj = oServProduccion.TraerUnicoXId(base.EntityId);
            lblProducto.Text = oObj.oProducto.Nombre;
            lblNroSerie.Text = oObj.NroSerie;
            lblWorkflow.Text = oObj.oWorkflow.Nombre;
            lblNroOP.Text = oObj.oOrdenProduccion.NroPedidoInterno;
            lblEstadoActual.Text = oObj.oEstadoActual.Nombre;

            Workflow_Estado estadosig = new Workflow_Estado();
            estadosig = EstadoSiguiente(oObj.IdEstadoActual, oObj.IdWorkflow);
            if (estadosig != null)
                lblEstadoSiguiente.Text = estadosig.Nombre;
            else
                lblEstadoSiguiente.Text = "-NO HAY-";
            lblFechaCreacion.Text = oObj.FechaCreacion.ToShortDateString();
            lblFechaInicioProduccion.Text = oObj.FechaInicioProduccionText;
            txtFechaEnvíoCliente.Text = oObj.FechaEnvioClienteText;
            txtFechaDeposito.Text = oObj.FechaDepositoText;
            txtNroCaja.Text = oObj.NroCaja;
            lblNroOrdenCompra.Text = oObj.OrdenCompra;
            txtNroRemito.Text = oObj.NroRemito;
            CargoGrillaComponentes();
            CargoGrillaHistorial();

        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargarControles", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    /// <summary>
    /// Metodo que guarda los datos establecidos en la pagina.
    /// </summary>
    private void GrabarDatos()
    {
        try
        {
            pnlError.Visible = false;
            pnlSuccess.Visible = false;
            Produccion oObj = ObtenerDatos();
            oServProduccion.Actualizar(oObj);
            pnlSuccess.Visible = true;
            litSuccess.Text = "Se guardaron los datos correctamente";

        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "GrabarDatos", HttpContext.Current.Request.Url.LocalPath);
            pnlError.Visible = true;
            litError.Text = "Error al guardar los datos";
        }
    }

    /// <summary>
    /// Obtiene los datos de la pagina
    /// </summary>
    /// <returns>Retorna un objecto con los datos establecidos en la pagina</returns>
    private Produccion ObtenerDatos()
    {
        Produccion oObj = null;
        try
        {
            oObj = new Produccion();
            var oUsuario = base.TraerUsuarioLogeado();
            oObj = oServProduccion.TraerUnicoXId(base.EntityId);
            oObj.FechaDeposito = Convert.ToDateTime(txtFechaDeposito.Text);
            oObj.FechaEnvioCliente = Convert.ToDateTime(txtFechaEnvíoCliente.Text);
            oObj.NroCaja = txtNroCaja.Text;
            oObj.NroRemito = txtNroRemito.Text;


        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "ObtenerDatos", HttpContext.Current.Request.Url.LocalPath);
        }
        return oObj;
    }

    /// <summary>
    /// Metodo que limpia los datos establecidos en la pagina.
    /// </summary>
    private void Limpiar()
    {

        lblProducto.Text = string.Empty;
        lblNroSerie.Text = string.Empty;
        lblWorkflow.Text = string.Empty;
        lblNroOP.Text = string.Empty;
        lblEstadoActual.Text = string.Empty;
        lblEstadoSiguiente.Text = string.Empty;
        lblFechaCreacion.Text = string.Empty;
        lblFechaInicioProduccion.Text = string.Empty;
        txtFechaEnvíoCliente.Text = string.Empty;
        txtFechaDeposito.Text = string.Empty;
        txtNroCaja.Text = string.Empty;
        lblNroOrdenCompra.Text = string.Empty;
        txtNroRemito.Text = string.Empty;
        pnlError.Visible = false;
        pnlSuccess.Visible = false;

    }

    #endregion

    #region COMPONENTES
    private void CargoGrillaComponentes()
    {
        List<Produccion_Componentes> wQuery = null;
        try
        {

            wQuery = oServProduccion.TraerComponentesXProduccion(EntityId).ToList();

            grdComponentes.DataSource = wQuery;
            grdComponentes.DataBind();
            //lblCantidadComponentes.Text = wQuery.Count().ToString();
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargoGrilla", HttpContext.Current.Request.Url.LocalPath);
        }
        finally
        {
            wQuery = null;
        }
    }

    protected void grdComponentes_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdComponentes.PageIndex = e.NewPageIndex;
        this.CargoGrillaComponentes();
    }

    protected void grdComponentes_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargoGrillaComponentes();
    }

    protected void grdComponentes_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Editar")
        {
            btnCompBorrar_Click(null, null);

            long mIdComponente = Convert.ToInt64(e.CommandArgument);
            Produccion_Componentes oObj = oServProduccion.TraerUnicoXIdProduccion_Componente(mIdComponente);
            lblComponenteId.Text = mIdComponente.ToString();
            lblComponente.Text = oObj.oComponente.Nombre;
            txtComponenteNrodeSerie.Text = oObj.NroSerie;
            pnlComponenteEditar.Visible = true;
        }
    }

    #region Botones Grabar y Limpiar componentes
    protected void btnCompGrabar_Click(object sender, EventArgs e)
    {
        
        Produccion_Componentes oObj = oServProduccion.TraerUnicoXIdProduccion_Componente(Convert.ToInt64(lblComponenteId.Text));
        oObj.NroSerie = txtComponenteNrodeSerie.Text.Trim();

        oServProduccion.ActualizarProduccion_Componente(oObj);
        btnCompBorrar_Click(null, null);
        CargoGrillaComponentes();
    }

    protected void btnCompBorrar_Click(object sender, EventArgs e)
    {
        lblComponente.Text = "";
        lblComponenteId.Text = "0";
        txtComponenteNrodeSerie.Text = "";
        pnlComponenteEditar.Visible = false;
    }
    #endregion
    #endregion

    #region Historial
    private void CargoGrillaHistorial()
    {
        List<Produccion_Historial_Estado> wQuery = null;
        try
        {

            wQuery = oServProduccion.TraerHistorialXProduccion(EntityId).ToList();


            grdHistorial.DataSource = wQuery;
            grdHistorial.DataBind();
            lblCantiadHistorial.Text = wQuery.Count().ToString();
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargoGrilla", HttpContext.Current.Request.Url.LocalPath);
        }
        finally
        {
            wQuery = null;
        }
    }

    protected void grdHistorial_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdHistorial.PageIndex = e.NewPageIndex;
        this.CargoGrillaHistorial();
    }

    /// <summary>
    /// Habilito o deshabilito el boton para ver el formulario dependiendo de si ese historial corresponde a un estado con form o no
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdHistorial_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex >= 0)
        {
            LinkButton Ver = new LinkButton();
            Ver = ((LinkButton)e.Row.FindControl("btnVer"));

            long idHistorial = Convert.ToInt64(e.Row.Cells[0].Text);
            Produccion_Historial_Estado historial = oServProduccion.TraerUnicoXIdProduccion_Historial(idHistorial);

            if (historial.IdFormulario == 0)
            {
                Ver.Enabled = false;
            }

        }
    }

    protected void grdHistorial_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "VerForm")
        {
            base.EntityId = Convert.ToInt64(e.CommandArgument);
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "AbrirModal", "AbrirModal('mdlForm')", true);
            ucVisualizadorRespuestasFormulario.CargarRespuetasSegunIdFormRespuesta(EntityId, "CerrarForm");
        }

    }

    protected void grdHistorial_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargoGrillaHistorial();
    }

    public void CerrarForm()
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "CerrarModal", "CerrarModal('mdlForm')", true);
    }
    #endregion

    #region Siguiente Estado
    private bool comprobarchbx(int i)
    {
        CheckBox seleccionado = new CheckBox();
        seleccionado = (CheckBox)grdGrilla.Rows[i].FindControl("chbxSeleccionMasiva");

        return seleccionado.Checked;
    }

    private bool ValidoLista(Produccion prod, List<Produccion> lista)
    {
        if (lista.Count != 0)
        {
            Produccion comparador = lista.First();
            if (prod.IdEstadoActual != comparador.IdEstadoActual)
            {
                pnlError.Visible = true;
                litError.Text = "Solo se pueden seleccionar productos en el mismo estado!!!!";
                return false;
            }

        }

        if (EstadoSiguiente(prod.IdEstadoActual, prod.IdWorkflow) == null)
        {
            if (pnlDetalles.Visible)
            {
                pnlErrorDetalles.Visible = true;
                litErrorDetalles.Text = "No hay un estado siguiente para ese producto";
            }
            else
            {
                pnlError.Visible = true;
                litError.Text = "No hay un estado siguiente para esos productos";
            }

            return false;
        }

        return true;
    }
    #endregion
}