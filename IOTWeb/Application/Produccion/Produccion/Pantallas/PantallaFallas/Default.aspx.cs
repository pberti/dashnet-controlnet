﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IOT.Domain;
using IOT.Services;


public partial class Application_Produccion_Produccion_Pantallas_PantallaFallas_Default : PageBase
{
    #region Propiedades & Variables
    private ServProductoST oServIngreso = Services.Get<ServProductoST>();
    private ServWorkflow_Ruta oServRuta = Services.Get<ServWorkflow_Ruta>();
    private ServSTFAlla oServFalla = Services.Get<ServSTFAlla>();
    #endregion

    #region "Main"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (base.IdUsuarioLogueado == -1)
        {
            Response.Redirect("~/Application/Login/Default.aspx", false);
            return;
        }
        if (!Page.IsPostBack)
        {
            InicializacionPantalla();
        }
    }

    private void InicializacionPantalla()
    {

        ProductoST_Ingreso ingreso = new ProductoST_Ingreso();
        long idproductost = (long)Session["productost"];
        ingreso = oServIngreso.TraerUnicoIngresoXId(idproductost);
        lblProducto.Text = ingreso.Producto;
        lblNroSerie.Text = ingreso.NroSerie + "-" + ingreso.NroGabinete;

        CargoRepeaterIngreso(ingreso);

    }
    #endregion

    #region Repeater
    protected void CargoRepeaterIngreso(ProductoST_Ingreso ingreso)
    {
        List<FallasXProducto> fallas = oServFalla.TraerTodosFallaXProducto().Where(f => f.IdTipoProducto == ingreso.oProductoST.IdTipoProducto).ToList();
        rptFallas.DataSource = fallas;
        rptFallas.DataBind();
    }

    protected void rptComponentes_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        string destino = (Request.QueryString["destino"]);

        Label lblFalla = new Label();
        lblFalla = (Label)e.Item.FindControl("lblFalla");

        FallasXProducto oFalla = new FallasXProducto();
        oFalla = (FallasXProducto)e.Item.DataItem;

        lblFalla.Text = oFalla.NombreFalla;
    }
    #endregion

    #region "Eventos Botones"
    protected void btnSiguienteEstado_ServerClick(object sender, EventArgs e)
    {
        long idproductost = (long)Session["productost"];
        ProductoST_Ingreso ingreso = oServIngreso.TraerUnicoIngresoXId(idproductost);

        if (ComprobarPermiso(ingreso.IdEstadoActual, IdPerfilUsuarioLogueado))
        {
            string mFallasAgregadas = "";
            List<FallasXIngreso> fallas = ObtenerDatosIngreso(idproductost, ingreso);

            foreach (FallasXIngreso f in fallas)
            {
                oServFalla.AgregarFallaXIngreso(f);
                //Busco la Fall
                STFalla oFalla = oServFalla.TraerUnicoXId(f.IdFalla);
                if (oFalla != null)
                    mFallasAgregadas = mFallasAgregadas + oFalla.Nombre + ", ";

            }
            if (mFallasAgregadas.Length == 0)
            {
                mFallasAgregadas = "No se agregaron Fallas";
            }

            oServIngreso.PasarEstado(ingreso, IdUsuarioLogueado, "Fallas:" + mFallasAgregadas, 0, 0, ingreso.IdEstadoActual);
            //Controlo en caso de que el paso de estado sea en el se registran las fallas y arreglos de un Servicio Técnico
            int dosPantallas = Convert.ToInt16(Request.QueryString["dosPantallas"]);

            if (dosPantallas != 0)
            {
                Response.Redirect("~/Application/Produccion/Produccion/Pantallas/PantallaNroSerie/Default.aspx?destino=s&dosPantallas=" + dosPantallas.ToString(), false);
            }
            else
                Response.Redirect("~/Application/Servicio_Tecnico/Ingresos/Default.aspx?bandera=0", false);
        }
        else
        {
            Response.Redirect("~/Application/Servicio_Tecnico/Ingresos/Default.aspx?bandera=0", false);
        }
    }

    protected void btnSalir_ServerClick(object sender, EventArgs e)
    {
        string destino = (Request.QueryString["destino"]);

        if (destino == "p")
        {
            Session["listproductos"] = new List<long>();
            Response.Redirect("~/Application/Produccion/Produccion/Default.aspx?Bandera=0", false);
        }
        else
        {
            Session["productost"] = 0;
            Response.Redirect("~/Application/Servicio_Tecnico/Ingresos/Default.aspx?bandera=0", false);
        }
    }
    #endregion

    #region PasarEstado
    private List<FallasXIngreso> ObtenerDatosIngreso(long idIngreso, ProductoST_Ingreso oProductoI)
    {
        List<FallasXIngreso> fallas = new List<FallasXIngreso>();
        for (int i = 0; i < rptFallas.Items.Count; i++)
        {
            CheckBox checkbox = new CheckBox();
            checkbox = (CheckBox)rptFallas.Items[i].FindControl("chkFalla");

            if (checkbox.Checked)
            {
                FallasXIngreso nuevafalla = new FallasXIngreso();

                Label nombre = new Label();
                nombre = (Label)rptFallas.Items[i].FindControl("lblFalla");

                STFalla falla = oServFalla.TraerTodos().Where(f => f.Nombre == nombre.Text).FirstOrDefault();

                TextBox comentario = new TextBox();
                comentario = (TextBox)rptFallas.Items[i].FindControl("txtComentario");

                nuevafalla.IdFalla = falla.Id;
                nuevafalla.IdIngreso = idIngreso;
                nuevafalla.Comentario = comentario.Text;
                nuevafalla.FecUltimaModificacion = DateTime.Now;
                nuevafalla.IdTipoProducto = oProductoI.oProductoST.IdTipoProducto;
                fallas.Add(nuevafalla);
            }
        }

        return fallas;
    }
    #endregion
}