﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Application_Produccion_Produccion_Pantallas_PantallaNroSerie_Default" %>

<%@ Register Src="~/UserControl/usrUltimaModificacion.ascx" TagName="usrUltimaModificacion" TagPrefix="uc1" %>
<%@ Register TagPrefix="usrError" TagName="Error" Src="~/UserControl/Error.ascx" %>
<%@ Register Src="~/UserControl/userProcesando.ascx" TagPrefix="uc1" TagName="userProcesando" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <%--Vista Grilla--%>
    <asp:Panel ID="pnlGrilla" runat="server">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                </div>
                <div class="title_right">
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Cambio de estado de Productos</h2>

                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <%--PANEL DE ERRORES--%>
                            <div class="alert alert-danger alert-dismissible fade in" role="alert" runat="server" id="pnlError" visible="false">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <strong><i class="fa fa-warning"></i>Atencion!!!</strong>
                                <br />
                                <asp:Literal ID="litError" runat="server" Text=""></asp:Literal>
                            </div>
                            <div class="clearfix"></div>
                            <br />
                            <%--Producto y NroSerie--%>
                            <div class="form-horizontal form-label-left">
                                <div class="clearfix">
                                    <asp:Label ID="lblprod" runat="server" Text="Producto:" class="control-label panel-title col-md-2 col-sm-2 col-xs-2"></asp:Label>
                                    <asp:Label ID="lblProducto" runat="server" class="control-label dark panel-title col-md-2 col-sm-2 col-xs-2"></asp:Label>
                                    <asp:Label ID="lblNro" runat="server" Text="Nro Ref:" class="control-label panel-title col-md-2 col-sm-2 col-xs-2"></asp:Label>
                                    <asp:Label ID="lblNroSerie" runat="server" class="control-label dark panel-title col-md-2 col-sm-2 col-xs-2"></asp:Label>
                                </div>
                                <div class="clearfix"></div>
                                <br />
                                <asp:Panel ID="pnlDetalles" runat="server">
                                    <asp:Repeater ID="rptComponentes" runat="server" Visible="true" OnItemDataBound="rptComponentes_ItemDataBound">
                                        <ItemTemplate>
                                            <div class="form-group">
                                                <asp:Label ID="lblIdTitu" Text="ID" runat="server" Visible="true" class="control-label col-md-1 col-sm-1 col-xs-1">Id:</asp:Label>
                                                <div class="col-md-1 col-sm-1 col-xs-1">
                                                    <asp:Label ID="lblId" runat="server" Visible="true" class="form-control" />
                                                </div>
                                                <asp:Label runat="server" Text="Componente:" class="control-label col-md-2 col-sm-2 col-xs-2"></asp:Label>
                                                <div class="col-md-3 col-sm-3 col-xs-3">
                                                    <asp:Label ID="lblComponente" runat="server" Visible="true" class="form-control" />
                                                </div>
                                                <asp:Label runat="server" Text="NroOrden:" class="control-label col-md-1 col-sm-1 col-xs-1"></asp:Label>
                                                <div class="col-md-1 col-sm-1 col-xs-1">
                                                    <asp:Label ID="lblNroOrden" runat="server" Visible="true" class="form-control" />
                                                </div>
                                                <asp:Label ID="lblNroSerieComponente" runat="server" Text="NroSerie:" class="control-label col-md-1 col-sm-1 col-xs-2"></asp:Label>
                                                <div class="col-md-2 col-sm-2 col-xs-2">
                                                    <asp:TextBox ID="txtNroSerieComponente" runat="server" CssClass="form-control fieldPpal"></asp:TextBox>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <br />
                                    <div class="clearfix"></div>
                                    <div class="pull-right">
                                        <asp:LinkButton runat="server" ID="btnVolver" CssClass="btn btn-round btn-alert" OnClick=" btnSalir_ServerClick">
                                         <i class="fa fa-arrow-circle-left"></i> Volver  </asp:LinkButton>
                                        <asp:LinkButton runat="server" ID="btnSiguienteEstado" CssClass="btn btn-round btn-primary" OnClick=" btnSiguienteEstado_ServerClick">
                                          Sig. Estado <i class="fa fa-arrow-circle-right"></i>   </asp:LinkButton>
                                        <asp:LinkButton runat="server" ID="btnSiguienteEstadoB" Visible="false" CssClass="btn btn-round btn-primary" OnClick="btnSiguienteEstadoB_Click">
                                          Sig. Estado <i class="fa fa-arrow-circle-right"></i>   </asp:LinkButton>
                                    </div>
                                </asp:Panel>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <usrError:Error ID="errorMensaje" runat="server" />
    <uc1:userProcesando runat="server" ID="userProcesando" />
</asp:Content>
