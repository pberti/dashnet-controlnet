﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using IOT.Domain;
using IOT.Services;

public partial class Application_Produccion_Produccion_Pantallas_PantallaNroSerie_Default : PageBase
{
    #region Propiedades & Variables
    private ServProduccion oServProduccion = Services.Get<ServProduccion>();
    private ServProductoST oServIngreso = Services.Get<ServProductoST>();
    private ServWorkflow_Ruta oServRuta = Services.Get<ServWorkflow_Ruta>();
    #endregion

    #region "Main"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (base.IdUsuarioLogueado == -1)
        {
            Response.Redirect("~/Application/Login/Default.aspx", false);
            return;
        }
        if (!Page.IsPostBack)
        {
            InicializacionPantalla();
        }
    }

    private void InicializacionPantalla()
    {
        string destino = (Request.QueryString["destino"]);

        if (destino == "p")
        {
            //Cargo LbL con nombre y con Nro de serie
            List<long> id = (List<long>)Session["listproductos"];
            Produccion producto = oServProduccion.TraerUnicoXId(id.First());
            lblProducto.Text = producto.oProducto.Nombre;
            lblNroSerie.Text = producto.NroSerie;

            CargoRepeater(producto);

            Workflow_Ruta ruta = oServRuta.TraerTodos().Where(r => r.IdWorkflowEstado == producto.IdEstadoActual && r.IdWorkflow == producto.IdWorkflow).FirstOrDefault();

            if (ruta.IdEstadoSiguienteB != 0)
            {
                btnSiguienteEstadoB.Visible = true;
                btnSiguienteEstadoB.Text = "Pasar a " + ruta.oEstadoSiguienteB.Nombre;
                btnSiguienteEstado.Text = "Pasar a " + ruta.oEstadoSiguiente.Nombre;
            }
        }

        else
        {

            ProductoST_Ingreso ingreso = new ProductoST_Ingreso();
            long idproductost = (long)Session["productost"];
            ingreso = oServIngreso.TraerUnicoIngresoXId(idproductost);
            lblProducto.Text = ingreso.Producto;
            lblNroSerie.Text = ingreso.NroSerie;

            CargoRepeaterIngreso(ingreso);

            Workflow_Ruta ruta = oServRuta.TraerTodos().Where(r => r.IdWorkflowEstado == ingreso.IdEstadoActual && r.IdWorkflow == ingreso.IdWorkflow).FirstOrDefault();

            if (ruta.IdEstadoSiguienteB != 0)
            {
                btnSiguienteEstadoB.Visible = true;
                btnSiguienteEstadoB.Text = "Pasar a " + ruta.oEstadoSiguienteB.Nombre;
                btnSiguienteEstado.Text = "Pasar a " + ruta.oEstadoSiguiente.Nombre;
            }

            int dosPantallas = Convert.ToInt16(Request.QueryString["dosPantallas"]);

            if (dosPantallas != 0)
            {
                btnVolver.Visible = false;
            }
        }
    }
    #endregion

    #region Repeater
    private void CargoRepeater(Produccion producto)
    {
        List<Produccion_Componentes> componentes = oServProduccion.TraerComponentesXProduccion(producto.Id).ToList();
        rptComponentes.DataSource = componentes;
        rptComponentes.DataBind();
    }

    protected void CargoRepeaterIngreso(ProductoST_Ingreso ingreso)
    {

        List<ProductoST_Componentes> componentes = oServIngreso.TraerTodosComponentesXProducto(ingreso.IdProductoST).ToList();
        rptComponentes.DataSource = componentes;
        rptComponentes.DataBind();
    }

    protected void rptComponentes_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        string destino = (Request.QueryString["destino"]);

        Label lblid = new Label();
        lblid = (Label)e.Item.FindControl("lblId");

        Label lblcomponente = new Label();
        lblcomponente = (Label)e.Item.FindControl("lblComponente");

        Label lblNroOrden = new Label();
        lblNroOrden = (Label)e.Item.FindControl("lblNroOrden");

        if (destino == "p")
        {
            Produccion_Componentes oComponente = new Produccion_Componentes();
            oComponente = (Produccion_Componentes)e.Item.DataItem;

            lblid.Text = oComponente.Id.ToString();
            lblcomponente.Text = oComponente.oComponente.Nombre.ToString();
            lblNroOrden.Text = oComponente.NroOrden.ToString();

            if (!string.IsNullOrEmpty(oComponente.NroSerie))
            {
                TextBox txtNroSerie = new TextBox();
                txtNroSerie = (TextBox)e.Item.FindControl("txtNroSerieComponente");
                txtNroSerie.Text = oComponente.NroSerie;
            }
        }

        else
        {
            ProductoST_Componentes oComponente = new ProductoST_Componentes();
            oComponente = (ProductoST_Componentes)e.Item.DataItem;

            lblid.Text = oComponente.Id.ToString();
            lblcomponente.Text = oComponente.oComponente.Nombre.ToString();
            lblNroOrden.Text = oComponente.NroOrden.ToString();

            if (!string.IsNullOrEmpty(oComponente.NroSerie))
            {
                TextBox txtNroSerie = new TextBox();
                txtNroSerie = (TextBox)e.Item.FindControl("txtNroSerieComponente");
                txtNroSerie.Text = oComponente.NroSerie;
            }
        }
    }
    #endregion

    #region "Eventos Botones"
    protected void btnSiguienteEstado_ServerClick(object sender, EventArgs e)
    {
        Page.Validate();
        if (Page.IsValid)
        {
            string destino = (Request.QueryString["destino"]);

            if (destino == "p")
            {
                List<long> id = (List<long>)Session["listproductos"];
                Produccion producto = oServProduccion.TraerUnicoXId(id.First());

                if (ComprobarPermiso(producto.IdEstadoActual, IdPerfilUsuarioLogueado))
                {
                    List<Produccion_Componentes> componentes = oServProduccion.TraerComponentesXProduccion(producto.Id).ToList();
                    ObtenerDatos(componentes);
                    if (ValidoDatos(componentes))
                    {
                        oServProduccion.ActualizarTodosComponentesXProduccion(componentes);
                        string comentario = "Se agrego numero de serie a los componentes";
                        oServProduccion.PasarEstado(producto, IdUsuarioLogueado, comentario);
                        Session["listproductos"] = new List<long>();
                        Response.Redirect("~/Application/Produccion/Produccion/Default.aspx?Bandera=0", false);
                    }
                }
                else
                {
                    Response.Redirect("~/Application/Produccion/Produccion/Default.aspx?Bandera=0", false);
                }
            }
            else
            {
                long idproductost = (long)Session["productost"];
                ProductoST_Ingreso ingreso = oServIngreso.TraerUnicoIngresoXId(idproductost);

                if (ComprobarPermiso(ingreso.IdEstadoActual, IdPerfilUsuarioLogueado))
                {
                    List<ProductoST_Componentes> componentes = oServIngreso.TraerTodosComponentesXProducto(ingreso.IdProductoST).ToList();
                    ObtenerDatosIngreso(componentes);
                    if (ValidoDatosIngreso(componentes))
                    {
                        oServIngreso.ActualizarTodosComponentesXProducto(componentes);
                        string comentario = "Se Comprobo el numero de serie de los componentes";
                        
                        //Controlo en caso de que el paso de estado sea en el se registran las fallas y arreglos de un Servicio Técnico
                        int dosPantallas = Convert.ToInt16(Request.QueryString["dosPantallas"]);

                        if (dosPantallas != 0)
                        {
                            comentario = (string)Session["comentario"];
                        }

                        oServIngreso.PasarEstado(ingreso, IdUsuarioLogueado, comentario);
                        Session["productost"] = 0;
                        Response.Redirect("~/Application/Servicio_Tecnico/Ingresos/Default.aspx?bandera=0", false);
                    }
                }
                else
                {
                    Response.Redirect("~/Application/Servicio_Tecnico/Ingresos/Default.aspx?bandera=0", false);
                }
            }
        }
    }

    protected void btnSiguienteEstadoB_Click(object sender, EventArgs e)
    {
        Page.Validate();
        if (Page.IsValid)
        {
            string destino = (Request.QueryString["destino"]);

            if (destino == "p")
            {
                List<long> id = (List<long>)Session["listproductos"];
                Produccion producto = oServProduccion.TraerUnicoXId(id.First());

                if (ComprobarPermiso(producto.IdEstadoActual, IdPerfilUsuarioLogueado))
                {
                    List<Produccion_Componentes> componentes = oServProduccion.TraerComponentesXProduccion(producto.Id).ToList();
                    ObtenerDatos(componentes);
                    if (ValidoDatos(componentes))
                    {
                        oServProduccion.ActualizarTodosComponentesXProduccion(componentes);
                        string comentario = "Se agrego numero de serie a los componentes";
                        oServProduccion.PasarEstadoB(producto, IdUsuarioLogueado, comentario);
                        Session["listproductos"] = new List<long>();
                        Response.Redirect("~/Application/Produccion/Produccion/Default.aspx?Bandera=0", false);
                    }
                }
                else
                {
                    Response.Redirect("~/Application/Produccion/Produccion/Default.aspx?Bandera=0", false);
                }
            }
            else
            {
                long idproductost = (long)Session["productost"];
                ProductoST_Ingreso ingreso = oServIngreso.TraerUnicoIngresoXId(idproductost);

                if (ComprobarPermiso(ingreso.IdEstadoActual, IdPerfilUsuarioLogueado))
                {
                    List<ProductoST_Componentes> componentes = oServIngreso.TraerTodosComponentesXProducto(ingreso.IdProductoST).ToList();
                    ObtenerDatosIngreso(componentes);
                    if (ValidoDatosIngreso(componentes))
                    {
                        oServIngreso.ActualizarTodosComponentesXProducto(componentes);
                        string comentario = "Se agrego numero de serie a los componentes";

                        //Controlo en caso de que el paso de estado sea en el se registran las fallas y arreglos de un Servicio Técnico
                        int dosPantallas = Convert.ToInt16(Request.QueryString["dosPantallas"]);
                        long idForm = (long)Session["formST"];
                        long idFormRespuesta = (long)Session["formrespuestaST"];

                        if (dosPantallas != 0)
                        {
                            comentario = (string)Session["comentario"];
                        }

                        oServIngreso.PasarEstadoB(ingreso, IdUsuarioLogueado, comentario, idForm, idFormRespuesta);
                        Session["productost"] = 0;
                        Session["formST"] = 0;
                        Session["formrespuestaST"] = 0;
                        Session["comentario"]= string.Empty;
                        Response.Redirect("~/Application/Servicio_Tecnico/Ingresos/Default.aspx?bandera=0", false);
                    }
                }
                else
                {
                    Response.Redirect("~/Application/Servicio_Tecnico/Ingresos/Default.aspx?bandera=0", false);
                }
            }
        }
    }

    protected void btnSalir_ServerClick(object sender, EventArgs e)
    {
        string destino = (Request.QueryString["destino"]);

        if (destino == "p")
        {
            Session["listproductos"] = new List<long>();
            Response.Redirect("~/Application/Produccion/Produccion/Default.aspx?Bandera=0", false);
        }
        else
        {
            Session["productost"] = 0;
            Response.Redirect("~/Application/Servicio_Tecnico/Ingresos/Default.aspx?bandera=0", false);
        }
    }
    #endregion

    #region PasarEstado
    private void ObtenerDatos(List<Produccion_Componentes> componentes)
    {
        for (int i = 0; i < rptComponentes.Items.Count; i++)
        {
            Label id = new Label();
            id = (Label)rptComponentes.Items[i].FindControl("lblId");

            Produccion_Componentes c = componentes.Find(p => p.Id == Convert.ToInt64(id.Text));

            TextBox nroSerie = new TextBox();
            nroSerie = (TextBox)rptComponentes.Items[i].FindControl("txtNroSerieComponente");

            c.NroSerie = nroSerie.Text;
        }
    }

    private bool ValidoDatos(List<Produccion_Componentes> componentes)
    {
        bool comodin = true;
        string textoError = string.Empty;

        foreach (Produccion_Componentes c in componentes)
        {
            if (c.oComponente.RequiereNroSerie)
            {
                if (string.IsNullOrEmpty(c.NroSerie))
                {
                    comodin = false;
                    pnlError.Visible = true;
                    litError.Text = "Debe  completar todos los Numeros de Serie";
                    break;
                }
            }
        }

        return comodin;
    }


    private void ObtenerDatosIngreso(List<ProductoST_Componentes> componentes)
    {
        for (int i = 0; i < rptComponentes.Items.Count; i++)
        {
            Label id = new Label();
            id = (Label)rptComponentes.Items[i].FindControl("lblId");

            ProductoST_Componentes c = componentes.Find(p => p.Id == Convert.ToInt64(id.Text));

            TextBox nroSerie = new TextBox();
            nroSerie = (TextBox)rptComponentes.Items[i].FindControl("txtNroSerieComponente");

            c.NroSerie = nroSerie.Text;
        }
    }

    private bool ValidoDatosIngreso(List<ProductoST_Componentes> componentes)
    {
        bool comodin = true;
        string textoError = string.Empty;

        foreach (ProductoST_Componentes c in componentes)
        {
            if (c.oComponente.RequiereNroSerie)
            {
                if (string.IsNullOrEmpty(c.NroSerie))
                {
                    comodin = false;
                    pnlError.Visible = true;
                    litError.Text = "Debe  completar todos los Numeros de Serie";
                    break;
                }
            }
        }

        return comodin;
    }
    #endregion
}