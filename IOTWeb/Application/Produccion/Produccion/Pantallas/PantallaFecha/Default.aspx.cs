﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IOT.Domain;
using IOT.Services;

public partial class Application_Produccion_Produccion_Pantallas_PantallaFecha_Default : PageBase
{
    #region Propiedades & Variables
    private ServProduccion oServProduccion = Services.Get<ServProduccion>();
    private ServProductoST oServIngresos = Services.Get<ServProductoST>();
    private ServWorkflow_Ruta oServRuta = Services.Get<ServWorkflow_Ruta>();
    private List<long> id { get; set; }
    #endregion

    #region "Main"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (base.IdUsuarioLogueado == -1)
        {
            Response.Redirect("~/Application/Login/Default.aspx", false);
            return;
        }
        if (!Page.IsPostBack)
        {
            InicializacionPantalla();
        }
    }
    private void InicializacionPantalla()
    {
        string destino = (Request.QueryString["destino"]);

        txtComentario.Text = string.Empty;
        txtFecha.Text = string.Empty;

        if (destino == "p")
        {
            CargoGrilla();
            List<long> prod = (List<long>)Session["listproductos"];

            if (prod.Count == 1)
            {
                Produccion producto = oServProduccion.TraerUnicoXId(prod.FirstOrDefault());
                Workflow_Ruta ruta = oServRuta.TraerTodos().Where(r => r.IdWorkflowEstado == producto.IdEstadoActual && r.IdWorkflow == producto.IdWorkflow).FirstOrDefault();

                if (ruta.IdEstadoSiguienteB != 0)
                {
                    btnSiguienteEstadoB.Visible = true;
                    btnSiguienteEstadoB.Text = "Pasar a " + ruta.oEstadoSiguienteB.Nombre;
                    btnSiguienteEstado.Text = "Pasar a " + ruta.oEstadoSiguiente.Nombre;
                }
            }
        }
        else
        {
            CargoGrillaIngresos();
            long idproductost = (long)Session["productost"];

            ProductoST_Ingreso ingreso = oServIngresos.TraerUnicoIngresoXId(idproductost);
            Workflow_Ruta ruta = oServRuta.TraerTodos().Where(r => r.IdWorkflowEstado == ingreso.IdEstadoActual && r.IdWorkflow == ingreso.IdWorkflow).FirstOrDefault();

            if (ruta.IdEstadoSiguienteB != 0)
            {
                btnSiguienteEstadoB.Visible = true;
                btnSiguienteEstadoB.Text = "Pasar a " + ruta.oEstadoSiguienteB.Nombre;
                btnSiguienteEstado.Text = "Pasar a " + ruta.oEstadoSiguiente.Nombre;
            }
        }
    }


    #endregion

    #region Metodos Grilla
    private void CargoGrilla()
    {
        try
        {
            List<Produccion> producciones = new List<Produccion>();
            id = (List<long>)Session["listproductos"];
            foreach (long i in id)
            {
                producciones.Add(oServProduccion.TraerUnicoXId(i));
            }
            grdGrilla.DataSource = producciones;
            grdGrilla.DataBind();
            lblCantidadFilas.Text = grdGrilla.Rows.Count.ToString();
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargoGrilla", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    protected void grdGrilla_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdGrilla.PageIndex = e.NewPageIndex;
        this.CargoGrilla();
    }

    protected void grdGrilla_Sorting(object sender, GridViewSortEventArgs e)
    {
        this.CargoGrilla();
    }


    protected void grdGrilla_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex >= 0)
        {
            Label mSigEstado = new Label();
            mSigEstado = ((Label)e.Row.FindControl("lblEstadoSiguiente"));

            long idProduccion = Convert.ToInt64(e.Row.Cells[0].Text);
            Produccion prod = oServProduccion.TraerUnicoXId(idProduccion);

            /// Me fijo que haya un siguiente estado, y si no lo hay lleno el espacio en la tabla con ""
            Workflow_Estado estadosig = new Workflow_Estado();
            estadosig = EstadoSiguiente(prod.IdEstadoActual, prod.IdWorkflow);
            if (estadosig != null)
                mSigEstado.Text = estadosig.Nombre;
            else
                mSigEstado.Text = "";
        }
    }

    #endregion

    #region "Eventos Botones"
    protected void btnSiguienteEstado_ServerClick(object sender, EventArgs e)
    {
        Page.Validate();
        if (Page.IsValid)
        {
            string destino = (Request.QueryString["destino"]);

            if (destino == "p")
            {
                // Uso el panel error para detectar si hubo o no un error en el paso de estado.
                pnlError.Visible = false;
                PasarEstado();
                if (!pnlError.Visible)
                {
                    Session["listproductos"] = new List<long>();
                    Response.Redirect("~/Application/Produccion/Produccion/Default.aspx?Bandera=0", false);
                }
            }
            else
            {
                PasarEstadoIngreso();
                if (!pnlError.Visible)
                {
                    Session["productost"] = 0;
                    Response.Redirect("~/Application/Servicio_Tecnico/Ingresos/Default.aspx?bandera=0", false);
                }
            }
        }
    }

    protected void btnSiguienteEstadoB_Click(object sender, EventArgs e)
    {
        Page.Validate();
        if (Page.IsValid)
        {
            string destino = (Request.QueryString["destino"]);

            if (destino == "p")
            {
                // Uso el panel error para detectar si hubo o no un error en el paso de estado.
                pnlError.Visible = false;
                PasarEstadoB();
                if (!pnlError.Visible)
                {
                    Session["listproductos"] = new List<long>();
                    Response.Redirect("~/Application/Produccion/Produccion/Default.aspx?Bandera=0", false);
                }
            }
            else
            {
                PasarEstadoBIngreso();
                if (!pnlError.Visible)
                {
                    Session["productost"] = 0;
                    Response.Redirect("~/Application/Servicio_Tecnico/Ingresos/Default.aspx?bandera=0", false);
                }
            }
        }
    }

    protected void btnSalir_ServerClick(object sender, EventArgs e)
    {
        string destino = (Request.QueryString["destino"]);

        if (destino == "p")
        {
            Session["listproductos"] = new List<long>();
            Response.Redirect("~/Application/Produccion/Produccion/Default.aspx?Bandera=0", false);
        }
        else
        {
            Session["productost"] = 0;
            Response.Redirect("~/Application/Servicio_Tecnico/Ingresos/Default.aspx?bandera=0", false);
        }
    }
    #endregion

    #region PasarEstado
    private void PasarEstado()
    {
        try
        {
            List<Produccion> producciones = new List<Produccion>();
            List<long> id = (List<long>)Session["listproductos"];
            foreach (long i in id)
            {
                producciones.Add(oServProduccion.TraerUnicoXId(i));
            }
            if (ComprobarPermiso(producciones.FirstOrDefault().IdEstadoActual, IdPerfilUsuarioLogueado))
            {
                if (ValidoDatos())
                    if (producciones.First().oEstadoActual.EsInicial)
                    {
                        oServProduccion.PasarInicioProduccion(producciones, IdUsuarioLogueado, txtComentario.Text, Convert.ToDateTime(txtFecha.Text));
                    }
                    else
                    {
                        oServProduccion.PasarEstadoMasivo(producciones, IdUsuarioLogueado, txtFecha.Text + " - " + txtComentario.Text);
                    }
            }
            else
            {
                Response.Redirect("~/Application/Produccion/Produccion/Default.aspx?Bandera=0", false);
            }
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "PasarEstado", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    private void PasarEstadoB()
    {
        try
        {
            Produccion prod = new Produccion();
            List<long> id = (List<long>)Session["listproductos"];
            prod = oServProduccion.TraerUnicoXId(id.FirstOrDefault());

            if (ComprobarPermiso(prod.IdEstadoActual, IdPerfilUsuarioLogueado))
            {
                if (ValidoDatos())
                {
                    oServProduccion.PasarEstadoB(prod, IdUsuarioLogueado, txtFecha.Text + " - " + txtComentario.Text);
                }
            }
            else
            {
                Response.Redirect("~/Application/Produccion/Produccion/Default.aspx?Bandera=0", false);
            }
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "PasarEstado", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    private bool ValidoDatos()
    {
        bool comodin = true;
        string textoError = string.Empty;

        if (string.IsNullOrEmpty(txtFecha.Text))
        {
            comodin = false;
            textoError = "Ingrese una fecha valida";
        }

        if (comodin == false)
        {
            pnlError.Visible = true;
            litError.Text = textoError;
        }

        return comodin;
    }
    #endregion

    #region Ingresos

    #region Metodos Grilla
    private void CargoGrillaIngresos()
    {
        try
        {
            grdGrilla.Visible = false;
            grdServicioTecnico.Visible = true;

            List<ProductoST_Ingreso> ingreso = new List<ProductoST_Ingreso>();
            long idproductost = (long)Session["productost"];

            ingreso = oServIngresos.TraerTodosIngresos().Where(i => i.Id == idproductost).ToList();
            grdServicioTecnico.DataSource = ingreso;
            grdServicioTecnico.DataBind();
            lblCantidadFilas.Text = grdServicioTecnico.Rows.Count.ToString();
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargoGrillaIngreso", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    protected void grdServicioTecnico_Sorting(object sender, GridViewSortEventArgs e)
    {
        this.CargoGrillaIngresos();
    }

    protected void grdServicioTecnico_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex >= 0)
        {
            Label mSigEstado = new Label();
            mSigEstado = ((Label)e.Row.FindControl("lblEstadoSiguiente"));

            long idIngreso = Convert.ToInt64(e.Row.Cells[0].Text);
            ProductoST_Ingreso ingreso = oServIngresos.TraerUnicoIngresoXId(idIngreso);

            /// Me fijo que haya un siguiente estado, y si no lo hay lleno el espacio en la tabla con ""
            Workflow_Estado estadosig = new Workflow_Estado();
            estadosig = EstadoSiguiente(ingreso.IdEstadoActual, ingreso.IdWorkflow);
            if (estadosig != null)
                mSigEstado.Text = estadosig.Nombre;
            else
                mSigEstado.Text = "";
        }
    }
    #endregion

    #region Pasar Estado
    private void PasarEstadoIngreso()
    {
        try
        {
            pnlError.Visible = false;
            ProductoST_Ingreso productost = new ProductoST_Ingreso();
            long idProductost = (long)Session["productost"];

            productost = oServIngresos.TraerUnicoIngresoXId(idProductost);

            if (ComprobarPermiso(productost.IdEstadoActual, IdPerfilUsuarioLogueado))
            {
                if (ValidoDatos())
                {
                    oServIngresos.PasarEstado(productost, IdUsuarioLogueado, txtFecha.Text + " - " + txtComentario.Text);
                }
            }
            else
            {
                Response.Redirect("~/Application/Servicio_Tecnico/Ingresos/Default.aspx?bandera=0", false);
            }
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "PasarEstadoINgreso", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    private void PasarEstadoBIngreso()
    {
        try
        {
            pnlError.Visible = false;
            ProductoST_Ingreso productost = new ProductoST_Ingreso();
            long idProductost = (long)Session["productost"];

            productost = oServIngresos.TraerUnicoIngresoXId(idProductost);

            if (ComprobarPermiso(productost.IdEstadoActual, IdPerfilUsuarioLogueado))
            {
                if (ValidoDatos())
                {
                    oServIngresos.PasarEstadoB(productost, IdUsuarioLogueado, txtFecha.Text + " - " + txtComentario.Text);
                }
            }
            else
            {
                Response.Redirect("~/Application/Servicio_Tecnico/Ingresos/Default.aspx?bandera=0", false);
            }
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "PasarEstadoINgreso", HttpContext.Current.Request.Url.LocalPath);
        }
    }
    #endregion

    #endregion
}