﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Application_Produccion_Produccion_Pantallas_PantallaFecha_Default" %>

<%@ Register Src="~/UserControl/usrUltimaModificacion.ascx" TagName="usrUltimaModificacion" TagPrefix="uc1" %>
<%@ Register TagPrefix="usrError" TagName="Error" Src="~/UserControl/Error.ascx" %>
<%@ Register Src="~/UserControl/userProcesando.ascx" TagPrefix="uc1" TagName="userProcesando" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <%--Vista Grilla--%>
    <asp:Panel ID="pnlGrilla" runat="server">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                </div>
                <div class="title_right">
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Cambio de estado de Productos</h2>

                            <div class="clearfix"></div>
                        </div>
                        <%--FILTRO--%>
                        <div class="x_content">
                            <%--PANEL DE ERRORES--%>
                            <div class="alert alert-danger alert-dismissible fade in" role="alert" runat="server" id="pnlError" visible="false">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <strong><i class="fa fa-warning"></i>Atencion!!!</strong>
                                <br />
                                <asp:Literal ID="litError" runat="server" Text=""></asp:Literal>
                            </div>
                            <div class="clearfix"></div>
                            <br />
                            <%--Comentario y fecha--%>
                            <div class="form-horizontal form-label-left">
                                <div class="form-group">
                                    <asp:Label ID="lblFecha" runat="server" Text="Fecha:" class="control-label col-md-1 col-sm-1 col-xs-2"></asp:Label>
                                    <div class="col-md-2 col-sm-2 col-xs-3">
                                        <asp:UpdatePanel runat="server">
                                            <ContentTemplate>
                                                <div class='input-group date dtp' id='dtpFecha'>
                                                    <asp:TextBox runat="server" ID="txtFecha" CssClass="form-control det-control" />
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar"></span>
                                                    </span>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <asp:Label ID="lblComentario" runat="server" Text="Comentario:" class="control-label col-md-1 col-sm-1 col-xs-2"></asp:Label>
                                    <div class="col-md-8 col-sm-8 col-xs-8">
                                        <asp:TextBox ID="txtComentario" runat="server" CssClass="form-control fieldPpal" MaxLength="255"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="pull-right">
                                    <asp:LinkButton runat="server" ID="btnVolver" CssClass="btn btn-round btn-alert" OnClick=" btnSalir_ServerClick">
                                  <i class="fa fa-arrow-circle-left"></i> Volver  </asp:LinkButton>
                                    <asp:LinkButton runat="server" ID="btnSiguienteEstado" CssClass="btn btn-round btn-primary" OnClick=" btnSiguienteEstado_ServerClick">
                                  Sig. Estado <i class="fa fa-arrow-circle-right"></i>   </asp:LinkButton>
                                    <asp:LinkButton runat="server" ID="btnSiguienteEstadoB" Visible="false" CssClass="btn btn-round btn-primary" OnClick="btnSiguienteEstadoB_Click">
                                  Sig. Estado <i class="fa fa-arrow-circle-right"></i>   </asp:LinkButton>
                                </div>
                                <div class="clearfix"></div>
                                <%--Grilla--%>
                                <asp:GridView ID="grdGrilla" runat="server" OnPageIndexChanging="grdGrilla_PageIndexChanging"
                                    OnSorting="grdGrilla_Sorting" AllowSorting="true" AllowPaging="true" PageSize="50"
                                    AutoGenerateColumns="False" CssClass="table table-striped table-bordered" OnRowDataBound="grdGrilla_RowDataBound">
                                    <Columns>
                                        <asp:BoundField DataField="Id" HeaderText="ID" SortExpression="Id" Visible="True" />
                                        <asp:BoundField DataField="NroSerie" HeaderText="Numero Ref" SortExpression="NroSerie" />
                                        <asp:BoundField DataField="oProducto.Nombre" HeaderText="Producto" SortExpression="oProductoNombre" />
                                        <asp:BoundField DataField="oOrdenProduccion.NroPedidoInterno" HeaderText="OP" SortExpression="oOrdenProduccionNroPedidoInterno" />
                                        <asp:BoundField DataField="oEstadoActual.Nombre" HeaderText="Estado Actual" SortExpression="oEstadoActualNombre" />
                                        <asp:TemplateField HeaderText="Sig. Estado">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEstadoSiguiente" runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="FechaInicioProduccionText" HeaderText="Fec Ini Prod" SortExpression="FechaInicioProduccionText" />
                                    </Columns>
                                    <PagerStyle CssClass="pgr" />
                                </asp:GridView>
                                <asp:GridView ID="grdServicioTecnico" runat="server" Visible="false" OnSorting="grdServicioTecnico_Sorting" AllowSorting="true" 
                                    AutoGenerateColumns="False" CssClass="table table-striped table-bordered" OnRowDataBound="grdServicioTecnico_RowDataBound">
                                    <Columns>
                                        <asp:BoundField DataField="Id" HeaderText="ID" SortExpression="Id" Visible="True" />
                                        <asp:BoundField DataField="Producto" HeaderText="Producto" SortExpression="Producto" />
                                        <asp:BoundField DataField="NroSerie" HeaderText="Nro Serie" SortExpression="NroSerie" />
                                        <asp:BoundField DataField="FechaIngreso" HeaderText="Fecha Ingreso" SortExpression="FechaIngreso" />
                                        <asp:BoundField DataField="EstadoActual" HeaderText="Estado Actual" SortExpression="EstadoActual" />
                                        <asp:TemplateField HeaderText="Sig. Estado">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEstadoSiguiente" runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle CssClass="pgr" />
                                </asp:GridView>
                                <div class="alert alert-success alert-dismissible fade in" role="alert">
                                    <div class="pull-left">
                                        Total de Registros:
                                    <asp:Label ID="lblCantidadFilas" runat="server"></asp:Label>
                                    </div>
                                    <br />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <usrError:Error ID="errorMensaje" runat="server" />
    <uc1:userProcesando runat="server" ID="userProcesando" />
</asp:Content>
