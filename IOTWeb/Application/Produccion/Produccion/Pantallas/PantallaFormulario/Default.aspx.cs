﻿using IOT.Domain;
using IOT.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class Application_Produccion_Produccion_Pantallas_PantallaFormulario_Default : PageBase
{

    #region Propiedades
    public long IdFormCampo
    {
        get { return getValue<long>("_IdFormCampo", 0); }
        set { ViewState["_IdFormCampo"] = value; }
    }

    public bool FormCampoEsnuevo
    {
        get { return IdFormCampo == 0; }
    }

    public long IdFormRespuesta
    {
        get { return getValue<long>("_IdFormRespuesta", 0); }
        set { ViewState["_IdFormRespuesta"] = value; }
    }

    public bool FormRespuestaEsnueva
    {
        get { return IdFormRespuesta == 0; }
    }

    public bool PuedeAgregarFormularios
    {
        get { return getValue<bool>("_PuedeAgregarFormularios", false); }
        set { ViewState["_PuedeAgregarFormularios"] = value; }
    }

    private ServProduccion oServProduccion = Services.Get<ServProduccion>();
    private ServFormulario oServFormulario = Services.Get<ServFormulario>();
    private ServFormRespuesta oServFormRespuesta = Services.Get<ServFormRespuesta>();
    private ServProductoST oServIngreso = Services.Get<ServProductoST>();
    private ServWorkflow_Ruta oServRuta = Services.Get<ServWorkflow_Ruta>();
    #endregion

    #region Main
    protected void Page_Load(object sender, EventArgs e)
    {
        if (base.IdUsuarioLogueado == -1)
        {
            Response.Redirect("~/Application/Login/Default.aspx", false);
            return;
        }
        if (!Page.IsPostBack)
        {
            Inicializar();
        }
    }

    private void Inicializar()
    {
        try
        {
            EntityId = Convert.ToInt64(Request.QueryString["Id"]);
            string destino = (Request.QueryString["destino"]);
            int dosPantallas = Convert.ToInt16(Request.QueryString["dosPantallas"]);

            if (destino == "p")
            {
                //Cargo LbL con nombre y con Nro de serie
                List<long> id = (List<long>)Session["listproductos"];
                Produccion producto = oServProduccion.TraerUnicoXId(id.First());
                lblProducto.Text = "Producto: " + producto.oProducto.Nombre;
                lblNroSerie.Text = "Nro de Ref: " + producto.NroSerie;
                lblEstadoActual.Text = "Estado Actual: " + producto.oEstadoActual.Nombre;
                lblSiguienteEstado.Text = "Siguiente Estado: " + this.EstadoSiguiente(producto.IdEstadoActual, producto.IdWorkflow).Nombre;

                Workflow_Ruta ruta = oServRuta.TraerTodos().Where(r => r.IdWorkflowEstado == producto.IdEstadoActual && r.IdWorkflow == producto.IdWorkflow).FirstOrDefault();

                if (ruta.IdEstadoSiguienteB != 0)
                {
                    btnSiguienteEstadoB.Visible = true;
                    btnSiguienteEstadoB.Text = "Pasar a " + ruta.oEstadoSiguienteB.Nombre;
                    btnSiguienteEstado.Text = "Pasar a " + ruta.oEstadoSiguiente.Nombre;
                }
            }
            else
            {
                long idproductost = (long)Session["productost"];
                ProductoST_Ingreso ingreso = oServIngreso.TraerUnicoIngresoXId(idproductost);
                lblProducto.Text = "Producto: " + ingreso.Producto;
                lblNroSerie.Text = "Nro de Serie: " + ingreso.NroSerie;
                lblEstadoActual.Text = "Estado Actual: " + ingreso.EstadoActual;
                lblSiguienteEstado.Text = "Siguiente Estado: " + this.EstadoSiguiente(ingreso.IdEstadoActual, ingreso.IdWorkflow).Nombre;

                Workflow_Ruta ruta = oServRuta.TraerTodos().Where(r => r.IdWorkflowEstado == ingreso.IdEstadoActual && r.IdWorkflow == ingreso.IdWorkflow).FirstOrDefault();

                if (dosPantallas == 1)
                {
                    btnSiguienteEstado.Text = "Continuar";
                }
                else
                {
                    if (ruta.IdEstadoSiguienteB != 0)
                    {
                        btnSiguienteEstadoB.Visible = true;
                        btnSiguienteEstadoB.Text = "Pasar a " + ruta.oEstadoSiguienteB.Nombre;
                        btnSiguienteEstado.Text = "Pasar a " + ruta.oEstadoSiguiente.Nombre;

                    }
                }
            }

            // Inicializar Controles de formulario
            ucRespuestaFormulario.CargarFormulario(EntityId, 0, "CerrarForm");
        }
        catch (Exception ex)
        {
            throw new Exception("Error en el método Inicializar", ex);
        }
    }
    #endregion

    #region Botones
    protected void btnSiguienteEstado_ServerClick(object sender, EventArgs e)
    {
        Page.Validate();
        if (Page.IsValid)
        {
            string destino = (Request.QueryString["destino"]);

            if (destino == "p")
            {
                PasarEstado();
                // Uso el panel error para detectar si hubo o no un error en el paso de estado.
                if (!pnlError.Visible)
                {
                    Session["listproductos"] = new List<long>();
                    Response.Redirect("~/Application/Produccion/Produccion/Default.aspx?Bandera=0", false);
                }
            }
            else
            {
                PasarEstadoIngreso();
                // Uso el panel error para detectar si hubo o no un error en el paso de estado.
                if (!pnlError.Visible)
                {
                    Session["productost"] = 0;
                    Response.Redirect("~/Application/Servicio_Tecnico/Ingresos/Default.aspx?bandera=0", false);
                }
            }
        }
    }

    protected void btnSiguienteEstadoB_Click(object sender, EventArgs e)
    {
        Page.Validate();
        if (Page.IsValid)
        {
            string destino = (Request.QueryString["destino"]);

            if (destino == "p")
            {
                PasarEstadoB();
                // Uso el panel error para detectar si hubo o no un error en el paso de estado.
                if (!pnlError.Visible)
                {
                    Session["listproductos"] = new List<long>();
                    Response.Redirect("~/Application/Produccion/Produccion/Default.aspx?Bandera=0", false);
                }
            }
            else
            {
                PasarEstadoIngresoB();
                // Uso el panel error para detectar si hubo o no un error en el paso de estado.
                if (!pnlError.Visible)
                {
                    Session["productost"] = 0;
                    Response.Redirect("~/Application/Servicio_Tecnico/Ingresos/Default.aspx?bandera=0", false);
                }
            }
        }
    }

    protected void btnSalir_ServerClick(object sender, EventArgs e)
    {
        string destino = (Request.QueryString["destino"]);

        if (destino == "p")
        {
            Session["listproductos"] = new List<long>();
            Response.Redirect("~/Application/Produccion/Produccion/Default.aspx?Bandera=0", false);
        }
        else
        {
            Session["productost"] = 0;
            Response.Redirect("~/Application/Servicio_Tecnico/Ingresos/Default.aspx?bandera=0", false);
        }
    }
    #endregion

    #region PasarEstado
    private void PasarEstado()
    {
        pnlError.Visible = false;
        List<long> id = (List<long>)Session["listproductos"];
        Produccion producto = oServProduccion.TraerUnicoXId(id.First());
        Formulario form = oServFormulario.TraerXId(EntityId);
        if (ComprobarPermiso(producto.IdEstadoActual, IdPerfilUsuarioLogueado))
        {
            //Pasar de estado
            Produccion_Historial_Estado asistente = oServProduccion.TraerHistorialXProduccion(producto.Id).Where(h => h.IdFormulario != 0).OrderBy("Id descending").FirstOrDefault();
            if (asistente != null)
            {
                Produccion_Historial_Estado historial = new Produccion_Historial_Estado();
                historial.IdFormulario = asistente.IdFormulario;
                historial.IdFormrespuestas = asistente.IdFormrespuestas;
                historial.IdUsuarioModifico = IdUsuarioLogueado;
                historial.IdEstadoActual = asistente.IdEstadoActual;
                historial.IdProduccion = asistente.IdProduccion;
                historial.IdNuevoEstado = EstadoSiguiente(asistente.IdEstadoActual, producto.IdWorkflow).Id;
                historial.Comentario = txtComentario.Text;
                historial.FecUltimaModificacion = DateTime.Now;

                oServProduccion.AgregarProduccion_Historial(historial);
                producto.IdEstadoActual = historial.IdNuevoEstado;
                oServProduccion.Actualizar(producto);
            }
            else
            {
                pnlError.Visible = true;
                litError.Text = "Debe completar el test antes de pasar de estado";
            }
        }
        else
        {
            Response.Redirect("~/Application/Produccion/Produccion/Default.aspx?Bandera=0", false);
        }
    }

    private void PasarEstadoB()
    {
        pnlError.Visible = false;
        List<long> id = (List<long>)Session["listproductos"];
        Produccion producto = oServProduccion.TraerUnicoXId(id.First());
        Formulario form = oServFormulario.TraerXId(EntityId);
        if (ComprobarPermiso(producto.IdEstadoActual, IdPerfilUsuarioLogueado))
        {
            //Pasar de estado
            Produccion_Historial_Estado asistente = oServProduccion.TraerHistorialXProduccion(producto.Id).Where(h => h.IdFormulario != 0).OrderBy("Id descending").FirstOrDefault();
            if (asistente != null)
            {
                Produccion_Historial_Estado historial = new Produccion_Historial_Estado();
                historial.IdFormulario = asistente.IdFormulario;
                historial.IdFormrespuestas = asistente.IdFormrespuestas;
                historial.IdUsuarioModifico = IdUsuarioLogueado;
                historial.IdEstadoActual = asistente.IdEstadoActual;
                historial.IdProduccion = asistente.IdProduccion;
                historial.IdNuevoEstado = EstadoSiguienteB(asistente.IdEstadoActual, producto.IdWorkflow).Id;
                historial.Comentario = txtComentario.Text;
                historial.FecUltimaModificacion = DateTime.Now;

                oServProduccion.AgregarProduccion_Historial(historial);
                producto.IdEstadoActual = historial.IdNuevoEstado;
                oServProduccion.Actualizar(producto);
            }
            else
            {
                pnlError.Visible = true;
                litError.Text = "Debe completar el test antes de pasar de estado";
            }
        }
        else
        {
            Response.Redirect("~/Application/Produccion/Produccion/Default.aspx?Bandera=0", false);
        }
    }

    private void PasarEstadoIngreso()
    {
        pnlError.Visible = false;
        long idproductost = (long)Session["productost"];
        ProductoST_Ingreso ingreso = oServIngreso.TraerUnicoIngresoXId(idproductost);
        Formulario form = oServFormulario.TraerXId(EntityId);
        if (ComprobarPermiso(ingreso.IdEstadoActual, IdPerfilUsuarioLogueado))
        {
            //Pasar de estado
            ProductoST_Historial asistente = oServIngreso.TraerHistorialXIngreso(ingreso.Id).Where(h => h.IdFormulario != 0).OrderBy("Id descending").FirstOrDefault();
            if (asistente != null)
            {
                    ProductoST_Historial historial = new ProductoST_Historial();
                    historial.IdFormulario = asistente.IdFormulario;
                    historial.IdFormrespuestas = asistente.IdFormrespuestas;
                    historial.IdUsuarioModifico = IdUsuarioLogueado;
                    historial.IdEstadoAnterior = asistente.IdEstadoAnterior;
                    historial.IdProductoSTIngreso = asistente.IdProductoSTIngreso;
                    historial.IdNuevoEstado = EstadoSiguiente(asistente.IdEstadoAnterior, ingreso.IdWorkflow).Id;
                    historial.Comentario = txtComentario.Text;
                    historial.FecUltimaModificacion = DateTime.Now;

                    oServIngreso.AgregarHistorial(historial);
                    ingreso.IdEstadoActual = historial.IdNuevoEstado;
                    oServIngreso.ActualizarIngreso(ingreso);
            }
            else
            {
                pnlError.Visible = true;
                litError.Text = "Debe completar el test antes de pasar de estado";
            }
        }
        else
        {
            Response.Redirect("~/Application/Servicio_Tecnico/Ingresos/Default.aspx?bandera=0", false);
        }
    }

    private void PasarEstadoIngresoB()
    {
        pnlError.Visible = false;
        long idproductost = (long)Session["productost"];
        ProductoST_Ingreso ingreso = oServIngreso.TraerUnicoIngresoXId(idproductost);
        Formulario form = oServFormulario.TraerXId(EntityId);
        if (ComprobarPermiso(ingreso.IdEstadoActual, IdPerfilUsuarioLogueado))
        {


            //Pasar de estado
            ProductoST_Historial asistente = oServIngreso.TraerHistorialXIngreso(ingreso.Id).Where(h => h.IdFormulario != 0).OrderBy("Id descending").FirstOrDefault();
            if (asistente != null)
            {
                int dosPantallas = Convert.ToInt16(Request.QueryString["dosPantallas"]);

                if (dosPantallas != 0)
                {
                    Session["formST"] = asistente.IdFormulario;
                    Session["formrespuestaST"] = asistente.IdFormrespuestas;
                    Session["comentario"] = txtComentario.Text;
                    pnlError.Visible = true;

                    Response.Redirect("~/Application/Produccion/Produccion/Pantallas/PantallaNroSerie/Default.aspx?destino=s&dosPantallas=" + dosPantallas.ToString(), false);
                }
                else
                {
                    ProductoST_Historial historial = new ProductoST_Historial();
                    historial.IdFormulario = asistente.IdFormulario;
                    historial.IdFormrespuestas = asistente.IdFormrespuestas;
                    historial.IdUsuarioModifico = IdUsuarioLogueado;
                    historial.IdEstadoAnterior = asistente.IdEstadoAnterior;
                    historial.IdProductoSTIngreso = asistente.IdProductoSTIngreso;
                    historial.IdNuevoEstado = EstadoSiguienteB(asistente.IdEstadoAnterior, ingreso.IdWorkflow).Id;
                    historial.Comentario = txtComentario.Text;
                    historial.FecUltimaModificacion = DateTime.Now;

                    oServIngreso.AgregarHistorial(historial);
                    ingreso.IdEstadoActual = historial.IdNuevoEstado;
                    oServIngreso.ActualizarIngreso(ingreso);
                }
            }
            else
            {
                pnlError.Visible = true;
                litError.Text = "Debe completar el test antes de pasar de estado";
            }
        }
        else
        {
            Response.Redirect("~/Application/Servicio_Tecnico/Ingresos/Default.aspx?bandera=0", false);
        }
    }
    #endregion

    #region Metodos form respuesta
    public void CerrarForm(bool pCancelar)
    {
        if (!pCancelar)
        {
            pnlError.Visible = false;
            GrabarRespuesta();
            GrabarHistorial();
        }

    }

    /// <summary>
    /// Se graban las respuestas del formulario
    /// </summary>
    public void GrabarRespuesta()
    {
        try
        {
            List<FormRespuestaCampo> lFormRespuestaCampos = ucRespuestaFormulario.ObtenerFormRespuestasCampos();
            FormRespuesta oFormRespuesta = new FormRespuesta();
            if (!FormRespuestaEsnueva) oFormRespuesta = oServFormRespuesta.TraerXId(IdFormRespuesta);
            else
            {
                oFormRespuesta.IdFormulario = EntityId;
                oFormRespuesta.IdUsuarioRespondio = IdUsuarioLogueado;
            }
            if (FormRespuestaEsnueva)
                IdFormRespuesta = oServFormRespuesta.Agregar(oFormRespuesta, lFormRespuestaCampos, IdUsuarioLogueado);
            else oServFormRespuesta.Actualizar(oFormRespuesta, lFormRespuestaCampos, IdUsuarioLogueado);

        }
        catch (Exception ex)
        {
            throw new Exception("Error en el método GrabarCancelarRespuesta", ex);
        }
    }

    private void GrabarHistorial()
    {
        try
        {
            string destino = (Request.QueryString["destino"]);

            if (destino == "p")
            {
                List<long> id = (List<long>)Session["listproductos"];
                Produccion producto = oServProduccion.TraerUnicoXId(id.First());
                List<FormRespuesta> listrespuesta = oServFormRespuesta.TraerTodos(null).OrderBy("Id descending").ToList();

                if (listrespuesta.Count != 0)
                {
                    FormRespuesta respuesta = listrespuesta.FirstOrDefault();
                    Produccion_Historial_Estado historial = new Produccion_Historial_Estado();
                    historial.IdEstadoActual = producto.IdEstadoActual;
                    historial.IdNuevoEstado = producto.IdEstadoActual;
                    historial.IdFormulario = respuesta.IdFormulario;
                    historial.IdFormrespuestas = respuesta.Id;
                    historial.IdProduccion = producto.Id;
                    historial.IdUsuarioModifico = IdUsuarioLogueado;
                    historial.FecUltimaModificacion = DateTime.Now;
                    historial.Comentario = "Se realizo el test " + respuesta.oFormulario.Titulo;
                    oServProduccion.AgregarProduccion_Historial(historial);
                }
            }

            else
            {
                long idproductost = (long)Session["productost"];
                ProductoST_Ingreso ingreso = oServIngreso.TraerUnicoIngresoXId(idproductost);
                List<FormRespuesta> listrespuesta = oServFormRespuesta.TraerTodos(null).OrderBy("Id descending").ToList();

                if (listrespuesta.Count != 0)
                {
                    FormRespuesta respuesta = listrespuesta.FirstOrDefault();
                    ProductoST_Historial historial = new ProductoST_Historial();
                    historial.IdEstadoAnterior = ingreso.IdEstadoActual;
                    historial.IdNuevoEstado = ingreso.IdEstadoActual;
                    historial.IdFormulario = respuesta.IdFormulario;
                    historial.IdFormrespuestas = respuesta.Id;
                    historial.IdProductoSTIngreso = ingreso.Id;
                    historial.IdUsuarioModifico = IdUsuarioLogueado;
                    historial.FecUltimaModificacion = DateTime.Now;
                    historial.Comentario = "Se realizo el test " + respuesta.oFormulario.Titulo;
                    oServIngreso.AgregarHistorial(historial);
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error en el método GrabarHistorial", ex);
        }
    }
    #endregion
}