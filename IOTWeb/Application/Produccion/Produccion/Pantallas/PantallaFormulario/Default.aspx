﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Application_Produccion_Produccion_Pantallas_PantallaFormulario_Default" %>

<%@ Register Src="~/UserControl/usrUltimaModificacion.ascx" TagName="usrUltimaModificacion" TagPrefix="uc1" %>
<%@ Register TagPrefix="usrError" TagName="Error" Src="~/UserControl/Error.ascx" %>
<%@ Register Src="~/UserControl/userProcesando.ascx" TagPrefix="uc1" TagName="userProcesando" %>
<%@ Register TagName="ucRespuestaFormulario" TagPrefix="ucRF" Src="~/UserControl/ucRespuestaFormulario.ascx" %>
<%@ Register TagName="ucVisualizadorRespuestasFormulario" TagPrefix="ucVRF" Src="~/UserControl/ucVisualizadorRespuestasFormulario.ascx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <%--Vista Grilla--%>
    <asp:Panel ID="pnlGrilla" runat="server">
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <div class="">
                    <div class="page-title">
                        <div class="title_left">
                        </div>
                        <div class="title_right">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Cambio de estado de Productos</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <%--PANEL DE ERRORES--%>
                                    <div class="alert alert-danger alert-dismissible fade in" role="alert" runat="server" id="pnlError" visible="false">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        <strong><i class="fa fa-warning"></i>Atencion!!!</strong>
                                        <br />
                                        <asp:Literal ID="litError" runat="server" Text=""></asp:Literal>
                                    </div>
                                    <div class="clearfix"></div>
                                    <br />
                                    <%--Producto y NroSerie--%>
                                    <div class="form-horizontal form-label-left">
                                        <div class="clearfix">
                                            <div class="form-group">
                                                <asp:Label ID="Label6" Text="      " runat="server" class="control-label dark panel-title col-md-3 col-sm-3 col-xs-3"></asp:Label>
                                                <asp:Label ID="lblProducto" runat="server" class="control-label dark panel-title col-md-3 col-sm-3 col-xs-3"></asp:Label>
                                                <asp:Label ID="lblNroSerie" runat="server" class="control-label dark panel-title col-md-3 col-sm-3 col-xs-3"></asp:Label>
                                                <asp:Label ID="Label2" Text="       " runat="server" class="control-label dark panel-title col-md-3 col-sm-3 col-xs-3"></asp:Label>
                                            </div>

                                        </div>
                                        <div class="clearfix"></div>
                                        <br />
                                        <asp:Panel ID="pnlDetalles" runat="server">
                                            <%-- ABM RESPUESTA FORMULARIO --%>
                                            <ucRF:ucRespuestaFormulario runat="server" ID="ucRespuestaFormulario" />
                                            <%-- RESPUESTAS FORMULARIO --%>
                                            <ucVRF:ucVisualizadorRespuestasFormulario runat="server" ID="ucVisualizadorRespuestasFormulario" />
                                            <br />
                                            <div class="clearfix"></div>
                                        </asp:Panel>
                                        <div class="form-group">
                                            <asp:Label ID="Label4" Text="      " runat="server" class="control-label dark panel-title col-md-3 col-sm-3 col-xs-3"></asp:Label>
                                            <asp:Label ID="lblEstadoActual" runat="server" class="control-label dark panel-title col-md-3 col-sm-3 col-xs-3"></asp:Label>
                                            <asp:Label ID="lblSiguienteEstado" runat="server" class="control-label dark panel-title col-md-3 col-sm-3 col-xs-3"></asp:Label>
                                            <asp:Label ID="Label5" Text="       " runat="server" class="control-label dark panel-title col-md-3 col-sm-3 col-xs-3"></asp:Label>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="lblComentario" Text="Comentario" runat="server" class="control-label dark panel-title col-md-3 col-sm-3 col-xs-3"></asp:Label>
                                            <div class="col-md-8 col-sm-8 col-xs-8">
                                                <asp:TextBox ID="txtComentario" runat="server" CssClass="form-control fieldPpal" MaxLength="255"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="pull-right">
                                            <asp:LinkButton runat="server" ID="btnVolver" CssClass="btn btn-round btn-alert" OnClick=" btnSalir_ServerClick">
                                         <i class="fa fa-arrow-circle-left"></i> Volver  </asp:LinkButton>
                                            <asp:LinkButton runat="server" ID="btnSiguienteEstado" CssClass="btn btn-round btn-primary" OnClick=" btnSiguienteEstado_ServerClick">
                                          Sig. Estado <i class="fa fa-arrow-circle-right"></i>   </asp:LinkButton>
                                            <asp:LinkButton runat="server" ID="btnSiguienteEstadoB" Visible="false" CssClass="btn btn-round btn-primary" OnClick="btnSiguienteEstadoB_Click">
                                          Sig. Estado <i class="fa fa-arrow-circle-right"></i>   </asp:LinkButton>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>

