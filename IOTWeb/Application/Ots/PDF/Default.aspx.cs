﻿using System; 
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web.UI;
using System.Web.UI.WebControls;
using IOT.Domain;
using System.Data;
using IOT.Services;

public partial class Application_Ots_PDF_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            InicializacionPantalla();
        }
    }

    private void InicializacionPantalla()
    {

        DateTime mFechaDesde = (DateTime) Session["FechaDesdeOts"];
        DateTime mFechaHasta = (DateTime) Session["FechaHastaOts"];
        string filtroProyecto = (string)Session["ddlFiltroProyecto"];
        string filtroCC = (string)Session["ddlFiltroCC"];

        LitNombre.Text = "Metrica de Versiones Entregadas y Cerradas del " + mFechaDesde.ToShortDateString() + " al " + mFechaHasta.ToShortDateString() + ". <br> Filtro proyecto: " + filtroProyecto + " y FiltroCC: " + filtroCC;

        List<OTs> lstOTs = Services.Get<ServOTs>().Indicador_VersionesCerradas(mFechaDesde, mFechaHasta, CargarFiltro());
        if (lstOTs.Count > 0)
        {
            CalculoIndicadores(lstOTs);
        }
    }

    private string CargarFiltro()
    {
        string filtroProyecto = (string)Session["ddlFiltroProyecto"];
        string filtroCC = (string)Session["ddlFiltroCC"];

        string mFiltro = "";
        if (filtroProyecto != "TODOS")
        {
            mFiltro = " AND PROYECTO_NOMBRE = '" + filtroProyecto + "'";
        }
        //ddlFiltroProyecto.ClearSelection();
        if (filtroCC != "TODOS")
        {
            mFiltro = mFiltro + " AND PROYECTO_CC = '" + filtroCC + "'";
        }
        return mFiltro;
    }

    private void CalculoIndicadores(List<OTs> lstOTs)
    {
        string specifier = "#,0.00#;(#,0.0#)";
        //Calculo de los Indicadores
        //Indicador 1 = Procentaje de FEcha de entregas cumplidas
        double mCantEntregas = Convert.ToDouble(lstOTs.Count());
        double mCantEntregas_ok = Convert.ToDouble(lstOTs.Where(p => p.Version_Cerrada_Ok == true).Count());
        double mOperacion = ((mCantEntregas_ok / mCantEntregas) * 100);
        lblIndicador1.Text = mOperacion.ToString(specifier);
        if (lblIndicador1.Text == "NaN" || lblIndicador1.Text == "NuN")
            lblIndicador1.Text = "SD";

        //Indicador 2 Entregas Aceptadas
        double mCant1 = Convert.ToDouble(lstOTs.Count(p => p.Version_AprobCliente == "SI"));
        double mCant2 = Convert.ToDouble(lstOTs.Count(p => p.Version_AprobCliente != "PENDIENTE"));  //Total de las Entregas que no son Pendientes
        mOperacion = ((mCant1 / mCant2) * 100);
        lblIndicador2.Text = mOperacion.ToString(specifier);
        if (lblIndicador2.Text == "NaN" || lblIndicador2.Text == "NuN")
            lblIndicador2.Text = "SD";


        //Indicador 3 - Tiempo Consumido
        double mAcumEstimadas = 0;
        double mAcumUsadas = 0;

        long mOT_Cant_Cerradas = 0;
        long mOT_Cant_Abiertas = 0;
        long mOT_Cant_Pendientes = 0;
        long mOT_Cant_Progreso = 0;
        long mOT_Cant_Finalizadas = 0;
        long mOT_Cant_Total = 0;
        foreach (var item in lstOTs)
        {
            mAcumEstimadas = mAcumEstimadas + Convert.ToDouble(item.OT_Horas_Estimadas_Total);
            mAcumUsadas = mAcumUsadas + Convert.ToDouble(item.OT_Horas_Usadas_Total);

            //Para los Agrupadores acumulo el estado de las OT
            mOT_Cant_Cerradas = mOT_Cant_Cerradas + item.OT_Cant_Cerradas;
            mOT_Cant_Pendientes = mOT_Cant_Pendientes + item.OT_Cant_Pendientes;
            mOT_Cant_Progreso = mOT_Cant_Progreso + item.OT_Cant_Progreso;
            mOT_Cant_Finalizadas = mOT_Cant_Finalizadas + item.OT_Cant_Finalizadas;
        }



        mOperacion = ((mAcumUsadas / mAcumEstimadas) * 100);
        lblIndicador3.Text = mOperacion.ToString(specifier);
        if (lblIndicador3.Text == "NaN" || lblIndicador3.Text == "NuN")
            lblIndicador3.Text = "SD";


        //Cargo el Agrupador de las horas que ya lo tengo aca
        lblInd_Horas_Total.Text = mAcumUsadas.ToString(specifier);
        lblInd_Hrs_Estimadas.Text = mAcumEstimadas.ToString(specifier);
        // Cargo los Agrupadores de OTs
        mOT_Cant_Total = mOT_Cant_Cerradas + mOT_Cant_Abiertas + mOT_Cant_Pendientes + mOT_Cant_Progreso + mOT_Cant_Finalizadas;
        string total = mOT_Cant_Total.ToString();
        string Prog = mOT_Cant_Progreso.ToString();
        string Pend = mOT_Cant_Pendientes.ToString();
        string Cump = mOT_Cant_Finalizadas.ToString();
        string Cerr = mOT_Cant_Cerradas.ToString();

        lblInd_OTs_Total.Text = total;

        //Cantidad de Proyectos
        lblInd_Proyectos_Total.Text = lstOTs.GroupBy(p => p.Proyecto_Nombre).Count().ToString();

        //Cantidad de Versiones
        lblInd_Ver_Total.Text = lstOTs.GroupBy(p => p.Version_Id).Count().ToString();
        string mVersionesCerradasOK = lstOTs.Where(p => p.Version_Cerrada_Ok == true).Count().ToString();
        string mVersionesCerradasNoOK = lstOTs.Where(p => p.Version_Cerrada_Ok == false).Count().ToString();
        string mVersionesAbiertas = (Convert.ToInt32(lblInd_Ver_Total.Text) - Convert.ToInt32(mVersionesCerradasOK) - Convert.ToInt32(mVersionesCerradasNoOK)).ToString();

        List<string> listVersionesTotales = new List<string>();
        listVersionesTotales.Add(mVersionesCerradasNoOK);
        listVersionesTotales.Add(mVersionesCerradasOK);
        listVersionesTotales.Add(mVersionesAbiertas);

        List<string> listVersionesTipos = new List<string>();
        listVersionesTipos.Add("Vencidas");
        listVersionesTipos.Add("Cerradas");
        listVersionesTipos.Add("Abiertas");

        List<string> listOTTotales = new List<string>();
        listOTTotales.Add(Prog);
        listOTTotales.Add(Pend);
        listOTTotales.Add(Cump);
        listOTTotales.Add(Cerr);

        List<string> listOTTipos = new List<string>();
        listOTTipos.Add("Programadas");
        listOTTipos.Add("Pendientes");
        listOTTipos.Add("Cumplimentadas");
        listOTTipos.Add("Cerradas");

        string jsonA = ObtenerObjChartDoughnutRecType(listOTTipos, listOTTotales);
        string jsonB = ObtenerObjChartDoughnutRecType(listVersionesTipos, listVersionesTotales);

        int banderaA = 0;
        int banderaB = 0;

        if (total == "0")
        {
            banderaA = 1;
        }
        if (lblInd_Ver_Total.Text == "0")
        {
            banderaB = 1;
        }

        string datos = "['" + Convert.ToInt16(mAcumUsadas).ToString() + "','" + Convert.ToInt16(mAcumEstimadas).ToString() + "']";
        string backcolor = "['" + ValoresConstantes.listaColores[7] + "','" + ValoresConstantes.listaColores[8] + "']"; ;
        string hoverBackgroundColor = "['" + ValoresConstantes.listaColores[0] + "','" + ValoresConstantes.listaColores[3] + "']"; ;
        int banderaC = 0;
        if (Convert.ToInt16(mAcumUsadas) == 0 && Convert.ToInt16(mAcumEstimadas) == 0)
        {
            banderaC = 1;
        }
        string objbarchar = ObtenerJsonBarChar(datos, backcolor, hoverBackgroundColor);

        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "",
       "renderizarCharts(" + jsonA + "," + jsonB + "," + objbarchar + "," + banderaA + "," + banderaB + "," + banderaC + ");", true);

    }

    private string ObtenerJsonBarChar(string datos, string backcolor, string hoverBackgroundColor)
    {
        string Json = string.Empty;
        try
        {
            Json = "{ " +
                   "type: 'horizontalBar'," +
                   "data: " +
                       "{" +
                          " labels:['Dedicadas','Estimadas']," +
                               "datasets: [{" +
                               "label: 'Cantidad'," +
                                   "data: " + datos + "," +
                                   "backgroundColor: " + backcolor + "," +
                                   "hoverBackgroundColor:" + hoverBackgroundColor +
                               "}]" +
                           "}," +
                              " options: { scales: " +
            " { xAxes: [{ ticks: { " +
                         " beginAtZero: 'true' " +
             "  } " +
              "     }]," +
          " yAxes: [{  stacked: true " +
          " }] } " +
               "}}";
        }

        catch (Exception ex)
        {

        }

        return Json;
    }

    #region Graficos
    private string ObtenerObjChartDoughnutRecType(List<string> listTipos, List<string> listTotales)
    {
        string JSObj = string.Empty;
        try
        {
            //Tipos a graficar
            string types = "['";
            //Cantidades a graficar
            string cantidades = "['";
            // Colores para graficar. Separamos por '.' 
            // debido a que obtendríamos un resultados incorrectos dado que el formato de una cadena de valores rgba se separan también por ','. 
            string colores = "['";

            for (int i = 0; i < listTipos.Count - 1; i++)
            {
                types = types + listTipos[i] + " - ";
                cantidades = cantidades + listTotales[i] + "','";
                types = types + listTotales[i] + "','";
                colores = colores + ValoresConstantes.listaColores[i] + "','";
            }

            types = types + listTipos.Last() + " - " + listTotales.Last() + "']";
            cantidades = cantidades + listTotales.Last() + "']";
            colores = colores + ValoresConstantes.listaColores[listTipos.Count] + "']";

            // por el momento realizaremos el obj JS como un string de c#.
            // pero lo correcto sería crear objetos para encapsular los charts deseados y luego exportar a JS.
            JSObj = "{ " +
                "type: 'doughnut'," +
                "data: " +
                    "{" +
                       " labels: " + types + "," +
                            "datasets: [{" +
                            "label: 'Cantidad'," +
                                "data: " + cantidades + "," +
                                "backgroundColor: " + colores +
                            "}]" +
                        "}," +
                        "options:" +
                    "{" +
                       " animation:" +
                        "{" +
                           " animateRotate: true" +
                            "} " +
                    "}" +
                "}";
        }
        catch (Exception ex)
        {

        }
        return JSObj;
    }

    protected void lbtnBuscar_Click(object sender, EventArgs e)
    {
        string mFiltro = CargarFiltro();
        DateTime mFechaDesde = (DateTime)Session["FechaDesdeOts"];
        DateTime mFechaHasta = (DateTime)Session["FechaHastaOts"];


        List<OTs> lstOTs = Services.Get<ServOTs>().Indicador_VersionesCerradas(mFechaDesde, mFechaHasta, mFiltro);
        CalculoIndicadores(lstOTs);

    }
    #endregion
}