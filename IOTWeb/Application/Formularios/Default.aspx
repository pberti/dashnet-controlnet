﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Application_Formularios_Default" %>

<%@ Register TagName="ucUltimaModificacion" TagPrefix="ucUM" Src="~/UserControl/usrUltimaModificacion.ascx" %>
<%@ Register TagName="ucError" TagPrefix="ucE" Src="~/UserControl/Error.ascx" %>
<%@ Register TagName="ucProcesando" TagPrefix="ucP" Src="~/UserControl/userProcesando.ascx" %>
<%@ Register TagName="ucAltaOBaja" TagPrefix="ucAB" Src="~/UserControl/ucAltaOBaja.ascx" %>
<%@ Register TagName="ucRespuestaFormulario" TagPrefix="ucRF" Src="~/UserControl/ucRespuestaFormulario.ascx" %>
<%@ Register TagName="ucVisualizadorRespuestasFormulario" TagPrefix="ucVRF" Src="~/UserControl/ucVisualizadorRespuestasFormulario.ascx" %>
<%@ Register TagName="ucPNotify" TagPrefix="ucPN" Src="~/UserControl/ucPNotify.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <%-- PANTALLA PPAL --%>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlPrincipal">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <div class="clearfix">
                                    <h2>Administración Formularios</h2>
                                    <div class="pull-right">
                                        <asp:LinkButton runat="server" ID="lbtnAgregarNuevo" CssClass="btn btn-round btn-primary" OnClick="lbtnAgregarNuevo_Click">
                                            <i class="fa fa-plus-circle"></i> Agregar Nuevo
                                        </asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            <div class="x_content">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="form-horizontal form-label-left">
                                            <div class="form-group">
                                                <div class="col-md-4 col-sm-4 col-xs-12">
                                                    <asp:Label runat="server" Text="Título"></asp:Label>
                                                    <div class="field">
                                                        <asp:TextBox runat="server" ID="txtFiltroTitulo" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="col-md-8 col-sm-8 col-xs-12">
                                                    <br />
                                                    <div class="pull-right">
                                                        <asp:LinkButton runat="server" ID="lbtnBuscar" CssClass="btn btn-info btn-round " OnClick="lbtnBuscar_Click">
                                                        <i class="fa fa-search"></i> Buscar
                                                        </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <%-- PANEL INF --%>
                                <div class="alert alert-success alert-dismissable fade in" role="alert" style="height: 45px">
                                    <div>
                                        <div class="pull-left">
                                            <asp:Label runat="server" Text="Total de Registros: " CssClass="control-label"></asp:Label>
                                            <asp:Label runat="server" ID="lblCantidadFilas"> </asp:Label>
                                        </div>
                                    </div>
                                </div>

                                <%-- GRILLA --%>
                                <asp:GridView runat="server" ID="grdFormularios"
                                    AllowPaging="true"
                                    PageSize="20"
                                    OnPageIndexChanging="grdFormularios_PageIndexChanging"
                                    OnRowCommand="grdFormularios_RowCommand"
                                    AllowSorting="true"
                                    OnSorting="grdFormularios_Sorting"
                                    CssClass="table table-striped jambo_table"
                                    GridLines="None"
                                    AutoGenerateColumns="False"
                                    OnRowDataBound="grdFormularios_RowDataBound">
                                    <Columns>
                                        <asp:BoundField DataField="Id" HeaderText="ID" SortExpression="Id" />
                                        <asp:BoundField DataField="Titulo" HeaderText="Título" SortExpression="Titulo" />
                                        <asp:BoundField DataField="Comentario" HeaderText="Comentario" SortExpression="Comentario" />
                                        <asp:BoundField DataField="EsInactivo" HeaderText="Estado" SortExpression="EsInactivo" />
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <div class="pull-right">
                                                    <asp:LinkButton runat="server" CommandName="Editar" CommandArgument='<%#Eval("Id")%>' CssClass="btn btn-info btn-xs"
                                                        Visible='<%#PuedeAgregarFormularios == true%>'>
                                                        <i class="fa fa-edit"></i> Editar
                                                    </asp:LinkButton>
                                                    <asp:LinkButton runat="server" CommandName="Baja" CommandArgument='<%#Eval("Id")%>'
                                                        Visible='<%#bool.Parse(Eval("Inactivo").ToString()) == false && PuedeAgregarFormularios == true%>' CssClass="btn btn-danger btn-xs">
                                                        <i class="fa fa-trash"></i> Baja
                                                    </asp:LinkButton>
                                                    <asp:LinkButton runat="server" CommandName="Alta" CommandArgument='<%#Eval("Id")%>'
                                                        Visible='<%#bool.Parse(Eval("Inactivo").ToString()) == true && PuedeAgregarFormularios == true%>' CssClass="btn btn-success btn-xs">
                                                        <i class="fa fa-check"></i> Alta
                                                    </asp:LinkButton>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle CssClass="table-pagination" HorizontalAlign="Right" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <%-- PANTALLA ABM --%>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlABMFormulario" Visible="false">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <div class="clearfix">
                                    <h2>
                                        <asp:Literal runat="server" ID="litTitDetalle"></asp:Literal>
                                    </h2>
                                </div>
                            </div>
                            <div class="x_content">

                                <%-- DATOS ENTIDAD PPAL --%>
                                <asp:Panel runat="server" ID="pnlEntidadPpal" DefaultButton="lbtnGrabar">
                                    <div class="form-horizontal form-label-left">
                                        <div class="form-group">
                                            <div class="col-md-2 col-sm-2 col-xs-12"></div>
                                            <div class="col-md-1 col-sm-1 col-xs-12">
                                                <asp:Label runat="server" Text="ID"></asp:Label>
                                                <asp:Label runat="server" ID="lblID" Enabled="false" CssClass="form-control"></asp:Label>
                                            </div>
                                            <div class="col-md-7 col-sm-7 col-xs-12">
                                                <asp:Label runat="server" Text="Título"></asp:Label>
                                                <div class="field">
                                                    <asp:TextBox runat="server" ID="txtTitulo" CssClass="form-control fieldPpal" required="required" Placeholder="(hasta 100 caracteres)" data-validate-length-range="0,100"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-2 col-sm-2 col-xs-12"></div>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <asp:Label runat="server" Text="Comentario"></asp:Label>
                                                <div class="field">
                                                    <asp:TextBox ID="txtComentario" runat="server" CssClass="form-control fieldPpal optional" TextMode="MultiLine" Rows="2" Placeholder="(hasta 255 caracteres)" data-validate-length-range="0,255"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="pull-right">
                                                <asp:LinkButton runat="server" ID="lbtnSalir" CssClass="btn btn-round btn-default" OnClick="lbtnSalir_Click">
                                                    <i class="fa fa-arrow-circle-left"></i> Volver
                                                </asp:LinkButton>
                                                <asp:LinkButton runat="server" ID="lbtnGrabar" CssClass="btn btn-round btn-primary"
                                                    OnClientClick="return Validar('fieldPpal');"
                                                    OnClick="lbtnGrabar_Click">
                                                    <i class="fa fa-check"></i> Grabar
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>

                                <%-- TABS --%>
                                <asp:Panel runat="server" ID="pnlTabs" Visible="false">
                                    <div role="tabpanel" data-example-id="togglable-tabs">
                                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                            <li role="presentation" class="active">
                                                <a href="#tabCampos" role="tab" data-toggle="tab" aria-expanded="true">
                                                    <i class="fa fa-list-ul"></i>Campos
                                                </a>
                                            </li>
                                        </ul>
                                        <div id="myTabContent" class="tab-content">
                                            <div role="tabpanel" class="tab-pane fade active in" id="tabCampos" aria-labelledby="home-tab">
                                                <div id="divABMFormCampos">
                                                    <asp:Panel runat="server" ID="pnlABMFormCampos" Visible="false">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4>
                                                                    <asp:Literal ID="litTitABMFormCampo" runat="server" Text="Nuevo Campo de Formulario"></asp:Literal></h4>
                                                            </div>
                                                            <div class="panel-body">
                                                                <div class="form-horizontal form-label-left">
                                                                    <div class="form-group">
                                                                        <div class="col-md-1 col-sm-1 col-xs-12"></div>
                                                                        <div class="col-md-1 col-sm-1 col-xs-12">
                                                                            <asp:Label runat="server" Text="ID"></asp:Label>
                                                                            <asp:Label runat="server" ID="lblFormCampoId" Enabled="false" CssClass="form-control"></asp:Label>
                                                                        </div>
                                                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                                                            <asp:Label runat="server" Text="Pregunta"></asp:Label>
                                                                            <div class="field">
                                                                                <asp:TextBox runat="server" ID="txtFormCampoPregunta" CssClass="form-control fieldFormCampo" required="required" Placeholder="(hasta 100 caracteres)" data-validate-length-range="0,100"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="col-md-1 col-sm-1 col-xs-12"></div>
                                                                        <div class="col-md-3 col-sm-3 col-xs-12">
                                                                            <asp:Label runat="server" Text="Tipo"></asp:Label>
                                                                            <asp:DropDownList runat="server" ID="ddlFormCampoTipoCampo" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlFormCampoTipoCampo_SelectedIndexChanged"></asp:DropDownList>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-3 col-xs-12">
                                                                            <br />
                                                                            <div class="checkbox">
                                                                                <label>
                                                                                    <asp:CheckBox runat="server" ID="chbxFormCampoEsRequerido"></asp:CheckBox>
                                                                                    <asp:Literal runat="server" Text="Es Requerido"></asp:Literal>
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-3 col-xs-12">
                                                                            <br />
                                                                            <div class="checkbox">
                                                                                <label>
                                                                                    <asp:CheckBox runat="server" ID="chbxFormCampoConComentario"></asp:CheckBox>
                                                                                    <asp:Literal runat="server" Text="Con comentario adicional"></asp:Literal>
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="col-md-1 col-sm-1 col-xs-12"></div>
                                                                        <div class="col-md-10 col-sm-10 col-xs-12">
                                                                            <asp:Label runat="server" Text="Opciones de repuesta"></asp:Label>
                                                                            <div class="field">
                                                                                <asp:TextBox ID="txtOpcionesRespuesta" runat="server" CssClass="form-control fieldFormCampo" required="required" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="ln_solid"></div>
                                                                <div class="pull-right">
                                                                    <asp:LinkButton runat="server" ID="lbtnCancelar" CssClass="btn btn-round btn-default" OnClick="lbtnCancelar_Click">
                                                                        <i class="fa fa-trash"></i> Cancelar
                                                                    </asp:LinkButton>
                                                                    <asp:LinkButton runat="server" ID="lbtnGrabarFormCampo" CssClass="btn btn-round btn-primary"
                                                                        OnClientClick="return Validar('fieldFormCampo');"
                                                                        OnClick="lbtnGrabarFormCampo_Click">
                                                                        <i class="fa fa-check"></i> Grabar
                                                                    </asp:LinkButton>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </asp:Panel>
                                                </div>
                                                <asp:Panel runat="server" ID="pnlGrillaFormCampos" Visible="false">
                                                    <div class="alert alert-success alert-dismissable fade in" role="alert" style="height: 45px">
                                                        <div>
                                                            <div class="pull-left">
                                                                <asp:Label runat="server" Text="Total de Registros: " CssClass="control-label"></asp:Label>
                                                                <asp:Label runat="server" ID="lblFormCamposCantidadFilas"> </asp:Label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <asp:GridView runat="server" ID="grdFormCampos"
                                                        AllowPaging="true"
                                                        PageSize="10"
                                                        OnPageIndexChanging="grdFormCampos_PageIndexChanging"
                                                        OnRowCommand="grdFormCampos_RowCommand"
                                                        AllowSorting="true"
                                                        OnSorting="grdFormCampos_Sorting"
                                                        CssClass="table table-striped jambo_table"
                                                        GridLines="None"
                                                        AutoGenerateColumns="False"
                                                        OnRowDataBound="grdFormCampos_RowDataBound">
                                                        <Columns>
                                                            <asp:BoundField DataField="Id" HeaderText="ID" SortExpression="Id" />
                                                            <asp:BoundField DataField="Pregunta" HeaderText="Pregunta" SortExpression="Pregunta" />
                                                            <asp:BoundField DataField="TipoCampo" HeaderText="TipoCampo" SortExpression="TipoCampo" />
                                                            <asp:BoundField DataField="Posicion" HeaderText="Posición" SortExpression="Posicion" />
                                                            <asp:BoundField DataField="Requerido" HeaderText="Requerido" SortExpression="Requerido" />
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    Opciones de Respuesta
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Repeater ID="Repeater1" runat="server" DataSource='<%# Eval("lOpcionesRespuesta") %>'>
                                                                        <ItemTemplate>
                                                                            <span class="label label-primary"><%# Container.DataItem %></span>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    <div style="text-align: center;">
                                                                        <asp:LinkButton runat="server" ID="lbtnAgregarNuevoFormCampo" CssClass="btn btn-success btn-xs" CommandName="Agregar" OnClientClick="Resplandecer('divABMFormCampos');">
                                                                            <i class="fa fa-plus-circle"></i> Agregar
                                                                        </asp:LinkButton>
                                                                    </div>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <div class="pull-right" style="width: 150px; text-align: center;">
                                                                        <asp:LinkButton runat="server" CommandName="Editar" CommandArgument='<%#Eval("Id")%>' CssClass="btn btn-info btn-xs" OnClientClick="Resplandecer('divABMFormCampos');">
                                                                            <i class="fa fa-edit"></i> Editar
                                                                        </asp:LinkButton>
                                                                        <asp:LinkButton runat="server" CommandName="Baja" CommandArgument='<%#Eval("Id")%>' CssClass="btn btn-danger btn-xs">
                                                                            <i class="fa fa-trash"></i> Baja
                                                                        </asp:LinkButton>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <PagerStyle CssClass="table-pagination" HorizontalAlign="Right" />
                                                    </asp:GridView>
                                                </asp:Panel>

                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <%-- ABM RESPUESTA FORMULARIO --%>
    <ucRF:ucRespuestaFormulario runat="server" ID="ucRespuestaFormulario" />
    <%-- RESPUESTAS FORMULARIO --%>
    <ucVRF:ucVisualizadorRespuestasFormulario runat="server" ID="ucVisualizadorRespuestasFormulario" />


    <ucE:ucError ID="errorMensaje" runat="server" />
    <ucP:ucProcesando runat="server" ID="UcProcesando1" />
    <ucAB:ucAltaOBaja runat="server" ID="ucAltaOBaja" />
    <ucPN:ucPNotify runat="server" ID="ucPNotify" />

</asp:Content>

