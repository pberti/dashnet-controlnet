﻿using IOT.Domain;
using IOT.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Application_Formularios_Default : PageBase
{

    #region PROPIEDADES

    public long IdFormCampo
    {
        get { return getValue<long>("_IdFormCampo", 0); }
        set { ViewState["_IdFormCampo"] = value; }
    }
    public bool FormCampoEsnuevo
    {
        get { return IdFormCampo == 0; }
    }

    public long IdFormRespuesta
    {
        get { return getValue<long>("_IdFormRespuesta", 0); }
        set { ViewState["_IdFormRespuesta"] = value; }
    }
    public bool FormRespuestaEsnueva
    {
        get { return IdFormRespuesta == 0; }
    }

    public bool PuedeAgregarFormularios
    {
        get { return getValue<bool>("_PuedeAgregarFormularios", false); }
        set { ViewState["_PuedeAgregarFormularios"] = value; }
    }

    ServTipoPerfilMenues oServPerfilMenus = Services.Get<ServTipoPerfilMenues>();
    #endregion

    #region MAIN

    protected void Page_Load(object sender, EventArgs e)
    {
        if (base.IdUsuarioLogueado == -1)
        {
            Response.Redirect("~/Application/Login/Default.aspx", false);
            return;
        }
        if (!Page.IsPostBack)
        {
            Inicializar();
        }
    }

    #endregion

    #region EVENTOS

    protected void lbtnAgregarNuevo_Click(object sender, EventArgs e)
    {
        pnlPrincipal.Visible = false;
        pnlABMFormulario.Visible = true;
        pnlTabs.Visible = false;
        litTitDetalle.Text = "Nuevo Formulario";
        EsNuevo = true;
        EraNuevo = true;
        UcProcesando1.CerrarModalProcesando();
    }

    protected void grdFormularios_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdFormularios.PageIndex = e.NewPageIndex;
        CargarGrilla(CriterioOrdenacion);
    }

    protected void grdFormularios_Sorting(object sender, GridViewSortEventArgs e)
    {
        DeterminarCriterioOrdenacion(e.SortExpression);
        CargarGrilla(CriterioOrdenacion);
    }

    protected void grdFormularios_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }

    protected void grdFormularios_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Editar")
            {
                EntityId = Convert.ToInt64(e.CommandArgument);
                EsNuevo = false;
                EraNuevo = false;
                CargarControlesABM();
                CargarTabFormCampos();
                litTitDetalle.Text = "Editar Formulario";
                pnlPrincipal.Visible = false;
                pnlABMFormulario.Visible = true;
                pnlTabs.Visible = true;
                UcProcesando1.CerrarModalProcesando();
            }
            if (e.CommandName == "Baja")
            {
                EntityId = Convert.ToInt64(e.CommandArgument);
                ucAltaOBaja.Configurar("DarAltaOBajaFormulario", true);
                Formulario oFormulario = Services.Get<ServFormulario>().TraerXId(EntityId);
                string mensaje = "Está por dar de BAJA el Formulario titulado: '" + oFormulario.Titulo + "'.";
                ucAltaOBaja.SetearMensajeAdvertencia(mensaje);
            }
            if (e.CommandName == "Alta")
            {
                EntityId = Convert.ToInt64(e.CommandArgument);
                ucAltaOBaja.Configurar("DarAltaOBajaFormulario", false);
                Formulario oFormulario = Services.Get<ServFormulario>().TraerXId(EntityId);
                string mensaje = "Está por dar de ALTA el Formulario titulado: '" + oFormulario.Titulo + "'.";
                ucAltaOBaja.SetearMensajeAdvertencia(mensaje);
            }
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "lbtnCancelar_Click", HttpContext.Current.Request.Url.LocalPath);
            throw new Exception("Error en el método lbtnCancelar_Click", ex);
        }
    }

    protected void grdFormCampos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdFormCampos.PageIndex = e.NewPageIndex;
        this.CargarGrillaFormCampos();
    }

    protected void grdFormCampos_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Editar")
        {
            IdFormCampo = Convert.ToInt64(e.CommandArgument);
            CargarControlesABMFormCampo();
            litTitABMFormCampo.Text = "Editar Campo de Formulario";
            pnlABMFormCampos.Visible = true;
            UcProcesando1.CerrarModalProcesando();
        }
        if (e.CommandName == "Agregar")
        {
            IdFormCampo = 0;
            LimpiarABMFormCampo();
            litTitABMFormCampo.Text = "Nuevo Campo de Formulario";
            pnlABMFormCampos.Visible = true;
            UcProcesando1.CerrarModalProcesando();
        }
        if (e.CommandName == "Baja")
        {
            IdFormCampo = Convert.ToInt64(e.CommandArgument);
            ucAltaOBaja.Configurar("DarBajaFormCampo", true);
            string mensaje = "Está por dar de BAJA el Campo (ID: " + IdFormCampo + ") del Formulario.";
            ucAltaOBaja.SetearMensajeAdvertencia(mensaje);
            pnlABMFormCampos.Visible = false;
        }
        if (e.CommandName == "alta")
        {
            IdFormCampo = Convert.ToInt64(e.CommandArgument);
            ucAltaOBaja.Configurar("DarBajaFormCampo", false);
            string mensaje = "Está por dar de ALTA el Campo (ID: " + IdFormCampo + ") del Formulario.";
            ucAltaOBaja.SetearMensajeAdvertencia(mensaje);
            pnlABMFormCampos.Visible = false;
        }
    }

    protected void grdFormCampos_Sorting(object sender, GridViewSortEventArgs e)
    {

    }

    protected void grdFormCampos_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }

    protected void lbtnMostrarVistaPrevia_Click(object sender, EventArgs e)
    {

    }

    protected void lbtnAgregarNuevoCampo_Click(object sender, EventArgs e)
    {

    }

    protected void lbtnGrabar_Click(object sender, EventArgs e)
    {
        EntityId = GrabarFormulario();
        ucPNotify.MostrarNotificacion(
            EsNuevo ? "Alta de Formulario" : "Modificación de Formulario",
            EsNuevo ? "Formulario agregado con éxito!" : "Formulario modificado con éxito!");
        EsNuevo = false;
        lblID.Text = EntityId.ToString();
        pnlTabs.Visible = true;
        CargarTabFormCampos();
        IdFormCampo = 0;
        LimpiarABMFormCampo();
    }

    protected void lbtnSalir_Click(object sender, EventArgs e)
    {
        CargarGrilla(CriterioOrdenacion);
        LimpiarABMFormulario();
        pnlABMFormulario.Visible = false;
        pnlPrincipal.Visible = true;
        // limpiamos...
    }

    protected void lbtnGrabarFormCampo_Click(object sender, EventArgs e)
    {
        GrabarFormCampo();
        LimpiarABMFormCampo();
        CargarGrillaFormCampos();
        pnlABMFormCampos.Visible = false;
        pnlGrillaFormCampos.Visible = true;
    }

    protected void lbtnAgregarNuevoFormCampo_Click(object sender, EventArgs e)
    {
        IdFormCampo = 0;
        litTitABMFormCampo.Text = "Nuevo Campo de Formulario";
        pnlABMFormCampos.Visible = false;
    }

    protected void lbtnCancelar_Click(object sender, EventArgs e)
    {
        try
        {
            Formulario oFormulario = Services.Get<ServFormulario>().TraerXId(EntityId);
            int cantidadFormCampos = oFormulario.lFormCampos.Where(fc => fc.Inactivo == false).Count();
            pnlABMFormCampos.Visible = cantidadFormCampos == 0;
            IdFormCampo = 0;
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "lbtnCancelar_Click", HttpContext.Current.Request.Url.LocalPath);
            throw new Exception("Error en el método lbtnCancelar_Click", ex);
        }
    }

    protected void ddlFormCampoTipoCampo_SelectedIndexChanged(object sender, EventArgs e)
    {
        string seleccion = ddlFormCampoTipoCampo.SelectedValue;
        txtOpcionesRespuesta.Enabled = true;
        if (seleccion == ValoresConstantes.OptFormCampo_Parrafo ||
            seleccion == ValoresConstantes.OptFormCampo_TextoCorto)
        {
            txtOpcionesRespuesta.Attributes.Add("placeholder", "Respuesta por defecto (opcional): Hasta 3000 caracteres");
        }
        else if (seleccion == ValoresConstantes.OptFormCampo_SeleccionMultiple ||
            seleccion == ValoresConstantes.OptFormCampo_SeleccionUnica)
        {
            txtOpcionesRespuesta.Attributes.Add("placeholder", "Opciones separadas por salto de linea (teclee 'Enter') y hasta 3000 caracteres");
        }
        else if (seleccion == ValoresConstantes.OptFormCampo_Valoracion)
        {
            txtOpcionesRespuesta.Attributes.Add("placeholder", "Formato: 'InicioRango:FinRango:Intervalo' (Contempla decimales). Ejemplo: 2,5:10:0,5");
        }
        else if (seleccion == ValoresConstantes.OptFormCampo_VerdaderoFalso)
        {
            txtOpcionesRespuesta.Enabled = false;
            txtOpcionesRespuesta.Text = "Verdadero o Falso";
        }
    }

    protected void lbtnBuscar_Click(object sender, EventArgs e)
    {
        CargarGrilla(CriterioOrdenacion);
        UcProcesando1.CerrarModalProcesando();
    }

    #endregion

    #region METODOS

    private void Inicializar()
    {
        try
        {
            CargarPermisosDePantalla();
            // Inicializar Controles de ABM_FormCampos
            CargarDDL(ValoresConstantes.lFormCampoTipos.Cast<Object>().ToList(), ddlFormCampoTipoCampo, null, null, null, null);
            PuedeAgregarFormularios = TraerUsuarioLogeado().IdTipoPerfil == ValoresConstantes.IdPerfilAdmin;
            lbtnAgregarNuevo.Visible = PuedeAgregarFormularios;
            DeterminarCriterioOrdenacion("Titulo");
            CargarGrilla(CriterioOrdenacion);
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "Inicializar", HttpContext.Current.Request.Url.LocalPath);
            throw new Exception("Error en el método Inicializar", ex);
        }
    }

    private void CargarPermisosDePantalla()
    {
        var tipoPerfilMenu = oServPerfilMenus.TraerTodosTipoPefilMenuesPorPerfil(IdPerfilUsuarioLogueado).Where(p => p.oMenu.PathPagina.Contains("/Formularios"));
        if (tipoPerfilMenu.Count() != 0)
        {
            List<TipoPerfilMenues> t = tipoPerfilMenu.ToList();
            lbtnAgregarNuevo.Visible = t.First().PuedeGrabar;
            lbtnGrabar.Visible = t.First().PuedeGrabar;
            lbtnGrabarFormCampo.Visible = t.First().PuedeGrabar;
        }
        else
            Response.Redirect("~/Application/Default.aspx", false);
    }

    private void CargarGrilla(string pCriterioOrdenacion)
    {
        try
        {
            Formulario oFormularioFiltro = ObtenerFormularioFiltro();
            List<Formulario> lFormularios = Services.Get<ServFormulario>().TraerXFormulario(oFormularioFiltro, true).ToList();
            lblCantidadFilas.Text = lFormularios.Count.ToString();
            grdFormularios.DataSource = lFormularios.AsQueryable().OrderBy(pCriterioOrdenacion).ToList();
            grdFormularios.DataBind();
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargarGrilla", HttpContext.Current.Request.Url.LocalPath);
            throw new Exception("Error en el método CargarGrilla", ex);
        }
    }

    private void CargarControlesABM()
    {
        try
        {
            Formulario oFormulario = Services.Get<ServFormulario>().TraerXId(EntityId);
            lblID.Text = oFormulario.Id.ToString();
            txtTitulo.Text = oFormulario.Titulo;
            txtComentario.Text = oFormulario.Comentario;
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargarControles", HttpContext.Current.Request.Url.LocalPath);
            throw new Exception("Error en el método GrabarFormulario", ex);
        }
    }

    private Formulario ObtenerFormularioFiltro()
    {
        Formulario oFormulario = new Formulario();
        oFormulario.Titulo = txtFiltroTitulo.Text.Trim();
        return oFormulario;
    }

    private long GrabarFormulario()
    {
        try
        {
            long IdFormulario = 0;
            if (ValidarFormulario())
            {
                Formulario oFormulario = ObtenerFormularioCargado();
                if (EsNuevo) Services.Get<ServFormulario>().Agregar(oFormulario);
                else Services.Get<ServFormulario>().Actualizar(oFormulario);
                IdFormulario = oFormulario.Id;
            }
            return IdFormulario;
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "GrabarFormulario", HttpContext.Current.Request.Url.LocalPath);
            throw new Exception("Error en el método GrabarFormulario", ex);
        }
    }

    private bool ValidarFormulario()
    {
        try
        {
            return true;
        }
        catch (Exception ex)
        {
            return true;
        }
    }

    private Formulario ObtenerFormularioCargado()
    {
        try
        {
            Formulario oFormulario = new Formulario();
            if (!EsNuevo) oFormulario.Id = EntityId;
            oFormulario.Titulo = txtTitulo.Text;
            oFormulario.Comentario = txtComentario.Text;

            oFormulario.IdUsuarioModifico = IdUsuarioLogueado;
            oFormulario.FecUltimaModificacion = DateTime.Now;
            return oFormulario;
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "ObtenerFormularioCargado", HttpContext.Current.Request.Url.LocalPath);
            throw new Exception("Error en el método ObtenerFormularioCargado", ex);
        }
    }

    private void LimpiarABMFormulario()
    {
        lblID.Text = string.Empty;
        litTitDetalle.Text = string.Empty;
        txtTitulo.Text = string.Empty;
        txtComentario.Text = string.Empty;
        //...
    }

    #region FORMCAMPO

    public void CargarTabFormCampos()
    {
        try
        {
            CargarGrillaFormCampos();
            bool existeAlMenosUno = grdFormCampos.Rows.Count > 0;
            pnlGrillaFormCampos.Visible = existeAlMenosUno;
            pnlABMFormCampos.Visible = !existeAlMenosUno;
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargarTabFormCampos", HttpContext.Current.Request.Url.LocalPath);
            throw new Exception("Error en el método CargarTabFormCampos", ex);
        }
    }

    private void CargarGrillaFormCampos()
    {
        try
        {
            List<FormCampo> lFormCampos = Services.Get<ServFormCampo>().TraerXIdFormulario(EntityId, true).ToList();
            lblFormCamposCantidadFilas.Text = lFormCampos.Count.ToString();
            grdFormCampos.DataSource = lFormCampos.AsQueryable().ToList();
            grdFormCampos.DataBind();
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargarGrillaFormCampos", HttpContext.Current.Request.Url.LocalPath);
            throw new Exception("Error en el método CargarGrillaFormCampos", ex);
        }
    }

    private long GrabarFormCampo()
    {
        try
        {
            long IdFormCampo = 0;
            if (ValidarFormCampo())
            {
                FormCampo oFormCampo = ObtenerFormCampoCargado();
                if (FormCampoEsnuevo) Services.Get<ServFormCampo>().Agregar(oFormCampo);
                else Services.Get<ServFormCampo>().Actualizar(oFormCampo);
                IdFormCampo = oFormCampo.Id;
            }
            return IdFormCampo;
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "GrabarFormCampo", HttpContext.Current.Request.Url.LocalPath);
            throw new Exception("Error en el método GrabarFormCampo", ex);
        }
    }

    private bool ValidarFormCampo()
    {
        try
        {
            return true;
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "ValidarFormCampo", HttpContext.Current.Request.Url.LocalPath);
            throw new Exception("Error en el método ValidarFormCampo", ex);
        }
    }

    private FormCampo ObtenerFormCampoCargado()
    {
        try
        {
            FormCampo oFormCampo = new FormCampo();
            if (!FormCampoEsnuevo) oFormCampo.Id = IdFormCampo;
            oFormCampo.IdFormulario = EntityId;
            oFormCampo.Pregunta = txtFormCampoPregunta.Text.Trim();
            oFormCampo.OpcionesRespuesta = txtOpcionesRespuesta.Text.Trim();
            oFormCampo.TipoCampo = ddlFormCampoTipoCampo.SelectedValue;
            oFormCampo.EsRequerido = chbxFormCampoEsRequerido.Checked;
            oFormCampo.ConComentarioAdicional = chbxFormCampoConComentario.Checked;

            oFormCampo.IdUsuarioModifico = IdUsuarioLogueado;
            oFormCampo.FecUltimaModificacion = DateTime.Now;
            return oFormCampo;
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "ObtenerFormCampoCargado", HttpContext.Current.Request.Url.LocalPath);
            throw new Exception("Error en el método ObtenerFormCampoCargado", ex);
        }
    }

    private void LimpiarABMFormCampo()
    {
        lblFormCampoId.Text = string.Empty;
        txtFormCampoPregunta.Text = string.Empty;
        ddlFormCampoTipoCampo.SelectedIndex = 0;
        chbxFormCampoEsRequerido.Checked = false;
        chbxFormCampoConComentario.Checked = false;
        txtOpcionesRespuesta.Text = string.Empty;
        litTitABMFormCampo.Text = "Nuevo Campo de Formulario";
    }

    private void CargarControlesABMFormCampo()
    {
        try
        {
            FormCampo oFormCampo = Services.Get<ServFormCampo>().TraerXId(IdFormCampo);
            lblFormCampoId.Text = oFormCampo.Id.ToString();
            txtFormCampoPregunta.Text = oFormCampo.Pregunta;
            ddlFormCampoTipoCampo.SelectedValue = oFormCampo.TipoCampo;
            chbxFormCampoEsRequerido.Checked = oFormCampo.EsRequerido;
            chbxFormCampoConComentario.Checked = oFormCampo.ConComentarioAdicional;
            txtOpcionesRespuesta.Text = oFormCampo.OpcionesRespuesta;
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargarControlesABMFormCampo", HttpContext.Current.Request.Url.LocalPath);
            throw new Exception("Error en el método GrabarFormulario", ex);
        }
    }

    public void GrabarRespuesta()
    {
        try
        {
            List<FormRespuestaCampo> lFormRespuestaCampos = ucRespuestaFormulario.ObtenerFormRespuestasCampos();
            FormRespuesta oFormRespuesta = new FormRespuesta();
            if (!FormRespuestaEsnueva) oFormRespuesta = Services.Get<ServFormRespuesta>().TraerXId(IdFormRespuesta);
            else
            {
                oFormRespuesta.IdFormulario = EntityId;
                oFormRespuesta.IdUsuarioRespondio = IdUsuarioLogueado;
            }
            if (FormRespuestaEsnueva)
                IdFormRespuesta = Services.Get<ServFormRespuesta>().Agregar(oFormRespuesta, lFormRespuestaCampos, IdUsuarioLogueado);
            else Services.Get<ServFormRespuesta>().Actualizar(oFormRespuesta, lFormRespuestaCampos, IdUsuarioLogueado);
            ucPNotify.MostrarNotificacion("Respuesta de Formulario", "Respuesta grabada con éxito (ID:" + IdFormRespuesta + ").");
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "GrabarCancelarRespuesta", HttpContext.Current.Request.Url.LocalPath);
            throw new Exception("Error en el método GrabarCancelarRespuesta", ex);
        }
    }

    public void DarBajaFormCampo(bool pInactivo, string pMotivo)
    {
        try
        {
            FormCampo oFormCampo = Services.Get<ServFormCampo>().TraerXId(IdFormCampo);
            oFormCampo.Inactivo = true;
            if (pInactivo)
            {
                oFormCampo.MotivoInactivo = string.IsNullOrEmpty(pMotivo) ?
                    "Baja desde grilla de Administración Formularios" : pMotivo;
                oFormCampo.FecInactivo = DateTime.Now;
            }
            oFormCampo.IdUsuarioModifico = IdUsuarioLogueado;
            oFormCampo.FecUltimaModificacion = DateTime.Now;
            Services.Get<ServFormCampo>().Actualizar(oFormCampo);
            CargarTabFormCampos();
            IdFormCampo = 0;
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "DarBajaFormCampo", HttpContext.Current.Request.Url.LocalPath);
            throw new Exception("Error en el método DarBajaFormCampo", ex);
        }
    }

    public void DarAltaOBajaFormulario(bool pInactivo, string pMotivo)
    {
        try
        {
            Formulario oFormulario = Services.Get<ServFormulario>().TraerXId(EntityId);
            oFormulario.Inactivo = pInactivo;
            if (pInactivo)
            {
                oFormulario.MotivoInactivo = string.IsNullOrEmpty(pMotivo) ?
                    "Baja desde grilla de Administración de Campos de Formulario del ABM de Formularios." : pMotivo;
                oFormulario.FecInactivo = DateTime.Now;
            }
            oFormulario.IdUsuarioModifico = IdUsuarioLogueado;
            oFormulario.FecUltimaModificacion = DateTime.Now;
            Services.Get<ServFormulario>().Actualizar(oFormulario);
            CargarGrilla(CriterioOrdenacion);
            EntityId = 0;
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "DarAltaOBajaFormulario", HttpContext.Current.Request.Url.LocalPath);
            throw new Exception("Error en el método DarAltaOBajaFormulario", ex);
        }
    }

    public void VolverAPantallaPPal(bool pCancelar)
    {
        if (!pCancelar) GrabarRespuesta();
        VolverAPantallaPPal();
    }

    public void VolverAPantallaPPal()
    {
        CargarGrilla(CriterioOrdenacion);
        pnlPrincipal.Visible = true;
    }

    #endregion

    #endregion

}