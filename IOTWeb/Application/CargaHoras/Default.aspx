﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Application_CargaHoras_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:Panel ID="pnlGrilla" runat="server">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                </div>
                <div class="title_right">
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Control Carga de Horas - Usuarios con faltantes de horas</h2>
                            <div class="clearfix"></div>
                        </div>
                        <%--FILTRO--%>
                        <div class="x_content">
                            <div class="x_panel">
                                <div class="form-horizontal form-label-left">
                                    <div class="col-md-10 col-sm-10 col-xs-12" >
                                        <div class="form-group">
                                            <asp:Label ID="Label1" runat="server" Text="Usuario:" class="control-label col-md-1 col-sm-1 col-xs-12"></asp:Label>
                                            <div class="col-md-3 col-sm-3 col-xs-12">
                                                <asp:DropDownList ID="ddlFiltroUsuario" runat="server" class="form-control">
                                                        
                                                </asp:DropDownList>
                                            </div>
                                            <asp:Label ID="Label2" runat="server" Text="Mes" class="control-label col-md-1 col-sm-1 col-xs-12"></asp:Label>
                                            <div class="col-md-2 col-sm-2 col-xs-12">
                                                <asp:DropDownList ID="ddlFiltroMes" runat="server" class="form-control">
                                                </asp:DropDownList>
                                            </div> 
                                            <asp:Label ID="Label4" runat="server" Text="Año" class="control-label col-md-1 col-sm-1 col-xs-12"></asp:Label>
                                            <div class="col-md-2 col-sm-2 col-xs-12">
                                                <asp:DropDownList ID="ddlFiltroAño" runat="server" class="form-control">
                                                </asp:DropDownList>
                                            </div> 
                                            <asp:Label ID="Label3" runat="server" Text="Periodo" class="control-label col-md-1 col-sm-1 col-xs-12"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" id="btnBuscar" runat="server" causesvalidation="False" class="btn btn-round btn-info pull-right" onserverclick="btnBuscar_Click">
                                    <i class="fa fa-search"></i> Buscar
                                </button>

                            </div>
                            <%--GRILLA--%>
                            <asp:GridView ID="grdGrilla" runat="server"
                                 AllowSorting ="false" 
                                AutoGenerateColumns="False" CssClass="table table-striped table-bordered">
                                <Columns>
                                    <asp:BoundField DataField="Id" HeaderText="ID" SortExpression="Id" Visible="True" />
                                    <asp:BoundField DataField="NombreYApellido" HeaderText="PosicNombreYApellidoion" SortExpression="PNombreYApellidoosicion" />
                                    <asp:BoundField DataField="Semana" HeaderText="Semana" SortExpression="Semana" />
                                    <asp:BoundField DataField="Cantidad" HeaderText="Cantidad" SortExpression="Cantidad" />
                                </Columns>
                                <PagerStyle CssClass="pgr" />
                            </asp:GridView>
                            <br />
                            <div class="alert alert-success alert-dismissible fade in" role="alert">
                                <div class="pull-left">
                                    Total de Registros:
                                    <asp:Label ID="lblCantidadFilas" runat="server"></asp:Label>
                                </div>
                                
                                <br /> 
                                <br /> 
                            </div>  
                        </div>
                    </div>
            </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>

