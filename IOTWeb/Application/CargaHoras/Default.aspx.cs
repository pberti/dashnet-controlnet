﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web.UI;
using System.Web.UI.WebControls;
using IOT.Domain;
using IOT.Services;

public partial class Application_CargaHoras_Default : PageBase
{

    private ServTipoPerfilMenues oServPerfilMenus = Services.Get<ServTipoPerfilMenues>();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (base.IdUsuarioLogueado == -1)
        {
            Response.Redirect("~/Application/Login/Default.aspx", false);
            return;
        }
        if (!Page.IsPostBack)
            InicializacionPantalla();
    }

    private void InicializacionPantalla()
    {
        CargoPermisosdePantalla();
        this.CargoComboFiltro();
        CargoGrilla();
    }


    private void CargoPermisosdePantalla()
    {
        var tipoPerfilMenu = oServPerfilMenus.TraerTodosTipoPefilMenuesPorPerfil(IdPerfilUsuarioLogueado).Where(p => p.oMenu.PathPagina.Contains("/CargaHoras"));
        if (tipoPerfilMenu.Count() != 0)
        {
            List<TipoPerfilMenues> t = tipoPerfilMenu.ToList();
        }
        else
            Response.Redirect("~/Application/Default.aspx", false);

    }

    private void CargoComboFiltro()
    {
        //Combo Usuarios
        ddlFiltroUsuario.Items.Clear();
        var oLista = Services.Get<ServRedmineHoras>().Redmine_UsuarioActivos();

        ddlFiltroUsuario.DataSource = oLista;
        ddlFiltroUsuario.DataTextField = "NombreYApellido";
        ddlFiltroUsuario.DataValueField = "Id";
        ddlFiltroUsuario.DataBind();
        ddlFiltroUsuario.Items.Insert(0, new ListItem("", "0"));

        //Combo Año
        ddlFiltroAño.Items.Add("2017");
        //Combo Mes

        ddlFiltroMes.Items.Add("1");
        ddlFiltroMes.Items.Add("2");
        ddlFiltroMes.Items.Add("3");
        ddlFiltroMes.Items.Add("4");
        ddlFiltroMes.Items.Add("5");
        ddlFiltroMes.Items.Add("6");
        ddlFiltroMes.Items.Add("7");
        ddlFiltroMes.Items.Add("8");
        ddlFiltroMes.Items.Add("9");
        ddlFiltroMes.Items.Add("10");
        ddlFiltroMes.Items.Add("11");
        ddlFiltroMes.Items.Add("12");

        ddlFiltroMes.SelectedValue = DateTime.Now.Month.ToString();

    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        // Verifica que campos del filtro contienen datos para realizar el filtro.
        try
        {
            this.CargoGrilla();
        }
        catch (Exception ex)
        {
            //errorMensaje.ErrorBind(ex, "btnBuscar_Click", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    #region "Eventos Grilla"

    private void CargoGrilla()
    {
        int mYear = Convert.ToInt32(ddlFiltroAño.SelectedValue);
        int mMonth = Convert.ToInt32(ddlFiltroMes.SelectedValue);
        List<RedmineUser> wQuery = null;
        wQuery = Services.Get<ServRedmineHoras>().Redmine_UsuariosHorasIncompletas(mYear, mMonth).ToList();
        grdGrilla.DataSource = wQuery;
        lblCantidadFilas.Text = wQuery.Count().ToString();
        grdGrilla.DataBind();
    }
    #endregion
}
