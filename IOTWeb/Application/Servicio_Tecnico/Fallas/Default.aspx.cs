﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IOT.Domain;
using System.Data;
using IOT.Services;

public partial class Application_Servicio_Tecnico_Fallas_Default : PageBase
{
    #region Propiedades & Variables
    private ServTipoPerfilMenues oServPerfilMenus = Services.Get<ServTipoPerfilMenues>();
    private ServSTFAlla oServFalla = Services.Get<ServSTFAlla>();
    private ServProducto oServTipoProducto = Services.Get<ServProducto>();
    private ServProductoST oServProductoST = Services.Get<ServProductoST>();
    #endregion

    #region "Main"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (base.IdUsuarioLogueado == -1)
        {
            Response.Redirect("~/Application/Login/Default.aspx", false);
            return;
        }
        if (!Page.IsPostBack)
            InicializacionPantalla();
    }
    private void InicializacionPantalla()
    {
        CargoPermisosdePantalla();
        DireccionOrdenacion = string.Empty;
        VisibilidadPaneles(true, false);
        this.CargoGrilla("Nombre ascending", this.ObtenerFiltro());
    }

    private void CargoPermisosdePantalla()
    {
        var tipoPerfilMenu = oServPerfilMenus.TraerTodosTipoPefilMenuesPorPerfil(IdPerfilUsuarioLogueado).Where(p => p.oMenu.PathPagina.Contains("Servicio_Tecnico/Fallas"));
        if (tipoPerfilMenu.Count() != 0)
        {
            List<TipoPerfilMenues> t = tipoPerfilMenu.ToList();
            btnAgregar.Visible = t.First().PuedeGrabar;
            btnGrabar.Visible = t.First().PuedeGrabar;
        }
        else
            Response.Redirect("~/Application/Default.aspx", false);

    }
    #endregion

    #region "Métodos - Generales Pantalla"
    private void VisibilidadPaneles(bool pPnlGrilla, bool pPnlDetalle)
    {
        pnlGrilla.Visible = pPnlGrilla;
        pnlDetalles.Visible = pPnlDetalle;
        pnlSuccess.Visible = false;
    }
    #endregion

    #region Metodos - Grilla
    /// <summary>
    /// Metodo que obtiene los datos establecidos de los filtros.
    /// </summary>
    /// <returns>Metodo que devuelve un objeto con los valores del filtro.</returns>
    private STFalla ObtenerFiltro()
    {
        STFalla oObj = null;
        return oObj;
    }

    /// <summary>
    /// Metodo que obtiene el ordenamiento de las columnas.
    /// </summary>
    private void ObtenerDireccionOrden(GridViewSortEventArgs evt)
    {
        if (DireccionOrdenacion == "")
        {
            DireccionOrdenacion = "ascending";
        }
        else
        {
            if (DireccionOrdenacion == "ascending")
                DireccionOrdenacion = "descending";
            else
                DireccionOrdenacion = "ascending";
        }
    }


    /// <summary>
    /// Metodo que cargs los datos en la grilla de la pagina.
    /// </summary>
    /// <param name="pOrderBy">Criterio de ordenamiento de las columnas</param>
    /// <param name="pObj">Objeto Viaje para determinar si existe algun filtro a aplicar a la grilla</param>
    private void CargoGrilla(string pOrderBy, STFalla pObj)
    {
        CriterioOrdenacion = pOrderBy;
        List<STFalla> wQuery = null;
        try
        {
            if (pObj == null)
            {
                wQuery = oServFalla.TraerTodos().OrderBy(pOrderBy).ToList();
            }
            else
            {
                wQuery = oServFalla.TraerTodos().OrderBy(pOrderBy).ToList();
            }

            grdGrilla.DataSource = wQuery;
            grdGrilla.DataBind();
            lblCantidadFilas.Text = wQuery.Count().ToString();
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargoGrilla", HttpContext.Current.Request.Url.LocalPath);
        }
        finally
        {
            wQuery = null;
        }
    }

    protected void grdGrilla_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdGrilla.PageIndex = e.NewPageIndex;
        this.CargoGrilla(CriterioOrdenacion, this.ObtenerFiltro());
    }

    protected void grdGrilla_Sorting(object sender, GridViewSortEventArgs e)
    {
        this.ObtenerDireccionOrden(e);
        this.CargoGrilla(string.Format("{0} {1}", e.SortExpression, DireccionOrdenacion), this.ObtenerFiltro());
    }

    protected void grdGrilla_RowCommand(Object sender, CommandEventArgs e)
    {
        if (e.CommandName == "Editar")
        {
            base.EntityId = Convert.ToInt64(e.CommandArgument);
            base.EsNuevo = false;
            litNombre.Text = "Editar Falla";
            lblId.Text = EntityId.ToString();
            this.CargarControles();
            this.VisibilidadPaneles(false, true);
        }
    }
    #endregion

    #region "Eventos Botones"

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        try
        {
            this.CargoGrilla("Nombre ascending", this.ObtenerFiltro());

        }
        catch (SystemException ex)
        {
            errorMensaje.ErrorBind(ex, "btnBuscar_Click", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    protected void btnAgregar_Click(object sender, EventArgs e)
    {
        TituloForm = "Nueva Falla";
        this.CrearNuevo();
    }

    protected void btnGrabar_ServerClick(object sender, EventArgs e)
    {
        Page.Validate();
        if (Page.IsValid)
        {
            this.GrabarDatos();
            if (!pnlError.Visible)
            {
                pnlSuccess.Visible = true;
                litSuccess.Text = "Los datos se guardaron correctamente";
            }
        }
    }

    protected void btnSalir_ServerClick(object sender, EventArgs e)
    {
        this.VisibilidadPaneles(true, false);
    }

    #endregion

    #region Metodos - Vista Detalle
    /// <summary>
    /// Metodo que carga los datos de los controles.
    /// </summary>
    private void CargarControles()
    {
        try
        {
            CargoDDLProcuto();
            var oObj = oServFalla.TraerUnicoXId(base.EntityId);
            lblId.Text = base.EntityId.ToString();
            txtNombre.Text = oObj.Nombre;
            txtDescripcion.Text = oObj.Descripcion;

            pnlFallaXProducto.Visible = true;
            CargoGrillaFallaXProducto("Nombre ascending");
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargarControles", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    /// <summary>
    /// Metodo que guarda los datos establecidos en la pagina.
    /// </summary>
    private void GrabarDatos()
    {
        try
        {
            STFalla oObj = ObtenerDatos();

            if (ValidoObjeto(oObj))
            //if (base.ValidateModel(oObj, ref this.frmDetalles))
            {
                if (EsNuevo)
                {
                    EntityId = oServFalla.Agregar(oObj);
                }
                else
                {
                    oServFalla.Actualizar(oObj);
                }
                CargoGrilla("Nombre ascending", this.ObtenerFiltro());
                CargarControles();
                pnlFallaXProducto.Visible = true;
                CargoGrillaFallaXProducto("Nombre ascending");
            }
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "GrabarDatos", HttpContext.Current.Request.Url.LocalPath);
        }
    }



    /// <summary>
    /// Obtiene los datos de la pagina
    /// </summary>
    /// <returns>Retorna un objecto con los datos establecidos en la pagina</returns>
    private STFalla ObtenerDatos()
    {
        STFalla oObj = null;
        try
        {
            oObj = new STFalla();

            if (!EsNuevo)
            {
                oObj = oServFalla.TraerUnicoXId(base.EntityId);
            }

            oObj.Nombre = txtNombre.Text.Trim();
            oObj.Descripcion = txtDescripcion.Text;
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "ObtenerDatos", HttpContext.Current.Request.Url.LocalPath);
        }
        return oObj;
    }

    /// <summary>
    /// Metodo que establece los datos en la pagina para crear un nuevo objecto TipoPerfil
    /// </summary>
    private void CrearNuevo()
    {
        this.Limpiar();

        litNombre.Text = "Nueva Falla";
        this.VisibilidadPaneles(false, true);
        EsNuevo = true;
        pnlFallaXProducto.Visible = false;
    }

    /// <summary>
    /// Metodo que limpia los datos establecidos en la pagina.
    /// </summary>
    private void Limpiar()
    {
        lblId.Text = "0";
        txtNombre.Text = string.Empty;
        txtDescripcion.Text = string.Empty;
        pnlError.Visible = false;

    }

    #endregion

    #region Validacion de Campos
    /// <summary>
    /// Metodo que valida el tramite a cargar
    /// </summary>
    private bool ValidoObjeto(STFalla pObj)
    {
        bool comodin = true;
        pnlError.Visible = false;
        string error = string.Empty;

        if (pObj.Nombre.Trim().Length == 0)
        {
            error = "Debe ingresar un Nombre Valido <br>";
            comodin = false;
        }
        else
        {
            if (EsNuevo)
            {
                //Valido que no existe un componente con ese nombre
                STFalla oObj = oServFalla.TraerTodos().Where(p => p.Nombre == pObj.Nombre.Trim()).FirstOrDefault();
                if (oObj != null)
                {
                    error = error + "Ya existe una falla con ese Nombre, por favor reintente <br>";
                    comodin = false;
                }
            }
            else
            {
                List<STFalla> oObj = oServFalla.TraerTodos().Where(p => p.Nombre == pObj.Nombre.Trim()).ToList();
                oObj.RemoveAll(t => t.Id == pObj.Id);
                if (oObj.Count != 0)
                {
                    error = error + "Ya Existe una falla con ese Nombre, por favor reintente <br>";
                    comodin = false;
                }

            }
        }

        if (!string.IsNullOrEmpty(error))
        {
            pnlError.Visible = true;
            litError.Text = error;
        }

        return comodin;
    }
    #endregion

    #region Falla
    #region Metodos - Grilla

    /// <summary>
    /// Metodo que obtiene el ordenamiento de las columnas.
    /// </summary>
    private void ObteneDireccionOrdenacionFallaXProd(GridViewSortEventArgs evt)
    {
        if (DireccionOrdenacion == "")
        {
            DireccionOrdenacion = "ascending";
        }
        else
        {
            if (DireccionOrdenacion == "ascending")
                DireccionOrdenacion = "descending";
            else
                DireccionOrdenacion = "ascending";
        }
    }

    /// <summary>
    /// Metodo que cargs los datos en la grilla de la pagina.
    /// </summary>
    /// <param name="pOrderBy">Criterio de ordenamiento de las columnas</param>
    /// <param name="pObj">Objeto Viaje para determinar si existe algun filtro a aplicar a la grilla</param>
    private void CargoGrillaFallaXProducto(string pOrderBy)
    {
        CriterioOrdenacion = pOrderBy;
        List<FallasXProducto> wQuery = null;
        try
        {
            wQuery = oServFalla.TraerTodosFallaXProducto().Where(p => p.IdFalla == int.Parse(lblId.Text)).OrderBy(p => p.IdTipoProducto).ToList();

            grdFallaXProducto.DataSource = wQuery;
            grdFallaXProducto.DataBind();
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargoGrillafalla", HttpContext.Current.Request.Url.LocalPath);
        }
        finally
        {
            wQuery = null;
        }
    }

    protected void grdFallaXProducto_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdFallaXProducto.PageIndex = e.NewPageIndex;
        this.CargoGrillaFallaXProducto(CriterioOrdenacion);
    }

    protected void grdFallaXProducto_Sorting(object sender, GridViewSortEventArgs e)
    {
        this.ObtenerDireccionOrden(e);
        this.CargoGrillaFallaXProducto(string.Format("{0} {1}", e.SortExpression, DireccionOrdenacion));
    }
    #endregion

    #region "Eventos Botones"
    protected void btnGrabarFallaXProd_ServerClick(object sender, EventArgs e)
    {
        Page.Validate();
        if (Page.IsValid)
        {
            this.GrabarDatosFalla();
        }
    }
    #endregion

    #region Metodos - Vista Detalle
    /// <summary>
    /// Metodo que guarda los datos establecidos en la pagina.
    /// </summary>
    private void GrabarDatosFalla()
    {
        try
        {
            FallasXProducto oObj = ObtenerDatosFallaXProducto();

            if (ValidoFallaXProducto(oObj))
            {
               oServFalla.AgregarFallaXProducto(oObj);
            }

            this.CargoGrillaFallaXProducto(base.CriterioOrdenacion);
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "GrabarDatos", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    /// <summary>
    /// Obtiene los datos de la pagina
    /// </summary>
    /// <returns>Retorna un objecto con los datos establecidos en la pagina</returns>
    private FallasXProducto ObtenerDatosFallaXProducto()
    {
        FallasXProducto oObj = null;
        try
        {
            oObj = new FallasXProducto();
            oObj.IdFalla = EntityId;
            oObj.IdTipoProducto = Convert.ToInt64(ddlProducto.SelectedValue);
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "ObtenerDatos", HttpContext.Current.Request.Url.LocalPath);
        }
        return oObj;
    }

    bool ValidoFallaXProducto(FallasXProducto oObj)
    {
        bool bandera = true;

        FallasXProducto falla = oServFalla.TraerTodosFallaXProducto().Where(f => f.IdTipoProducto == oObj.IdTipoProducto && f.IdFalla == oObj.IdFalla).FirstOrDefault();
        if (falla != null)
        {
            bandera = false;
        }

        return bandera;
    }
    #endregion

    #region Metodos DDL
    private void CargoDDLProcuto()
    {
        ddlProducto.Items.Clear();
        List<TipoProducto> wQuery;

        wQuery = oServTipoProducto.TraerTodos().ToList();

        ddlProducto.DataSource = wQuery;
        ddlProducto.DataTextField = "Nombre";
        ddlProducto.DataValueField = "Id";
        ddlProducto.DataBind();
    }
    #endregion
    #endregion
}