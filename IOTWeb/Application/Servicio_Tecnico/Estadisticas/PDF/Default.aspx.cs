﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IOT.Domain;
using System.Data;
using IOT.Services;

public partial class Application_Servicio_Tecnico_Estadisticas_PDF_Default : System.Web.UI.Page
{
    #region Propiedades
    private ServSTFAlla oServFalla = Services.Get<ServSTFAlla>();
    private ServProducto oServProducto = Services.Get<ServProducto>();
    private ServProductoST oServIngresos = Services.Get<ServProductoST>();
    DateTime mFechaDesde;
    DateTime mFechaHasta;
    long idProducto;
    #endregion

    #region Eventos Generales de Panalla
    protected void Page_Load(object sender, EventArgs e)
    {
            InicializacionPantalla();
    }

    private void InicializacionPantalla()
    {
        DatosInicio();
        CalculoIndicadores();
        TipoProducto producto = oServProducto.TraerUnicoXId(idProducto);

        LitNombre.Text = "Estadísticas Servicio Técnico desde el " + mFechaDesde.ToShortDateString() + " al " + mFechaHasta.ToShortDateString() + " <br> Producto: " + producto.Nombre;
    }
    #endregion

    #region Eventos del Filtro
    /// <summary>
    /// Completo las fechas y pongo valor cero en los campos de los indicadores
    /// </summary>
    private void DatosInicio()
    {
        //LImpio todo
        lblIndicador1.Text = "0";
        lblIndicador2.Text = "0";
        lblIndicador3.Text = "0";

        mFechaDesde = (DateTime) Session["FechaDesdeST"];
        mFechaHasta = (DateTime) Session["FechaHastaST"];
        idProducto = (long) Session["IdTipoProducto"];
    }
    #endregion Eventos del Filtro

    #region "Calculo Indicadores"
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        try
        {
            DatosInicio();
        }
        catch (Exception ex)
        {

        }
    }

    private void CalculoIndicadores()
    {
        List<ProductoST_Ingreso> ingresos = oServIngresos.TraerTodosIngresos().Where(f => f.FecUltimaModificacion >= mFechaDesde && f.FecUltimaModificacion <= mFechaHasta).ToList();
        List<ProductoST_Ingreso> wQuery = new List<ProductoST_Ingreso>();
        foreach (ProductoST_Ingreso i in ingresos)
        {
            if (i.oProductoST.IdTipoProducto == idProducto)
            {
                wQuery.Add(i);
            }
        }

        List<ProductoST_Ingreso> asistente = new List<ProductoST_Ingreso>();


        lblIndicador1.Text = wQuery.Count().ToString();

        foreach (ProductoST_Ingreso i in wQuery)
        {
            if (i.oEstadoActual.EsFinal)
            {
                asistente.Add(i);
            }
        }

        lblIndicador2.Text = (asistente.Count() * 100 / wQuery.Count()).ToString() + "%";


        asistente.Clear();
        foreach (ProductoST_Ingreso i in wQuery)
        {
            if (i.Garantia)
            {
                asistente.Add(i);
            }
        }

        lblIndicador3.Text = (asistente.Count() * 100 / wQuery.Count()).ToString() + "%";

        string jsonA = "";
        string jsonB = "";
        int banderaA = 0;
        int banderaB = 0;



        //Creo listas para gráficos
        List<string> total = new List<string>();
        List<string> tipo = new List<string>();

        List<FallasXProducto> comodin = oServFalla.TraerTodosFallaXProducto().Where(f => f.IdTipoProducto == wQuery.FirstOrDefault().oProductoST.IdTipoProducto).ToList();
        if (comodin.Count > 0)
        {
            foreach (FallasXProducto f in comodin)
            {
                tipo.Add(f.NombreFalla);
                //List<FallasXIngreso> i = oServFalla.TraerTodosFallasXIngreso().Where(x => x.IdFalla == f.IdFalla && x.IdTipoProducto == f.IdTipoProducto).ToList();
                List<FallasXIngreso> i = oServFalla.TraerTodosFallasXIngreso().Where(x => x.IdFalla == f.IdFalla && x.IdTipoProducto == f.IdTipoProducto).ToList();
                i = i.Where(p => asistente.Any(x => x.Id == p.IdIngreso)).ToList();
                total.Add(i.Count().ToString());
            }
        }
        else
        {
            banderaA = 1;
        }
        jsonA = ObtenerObjChartDoughnutRecType(tipo, total);

        total.Clear();
        tipo.Clear();

        if (wQuery.Count > 0)
        {
            foreach (ProductoST_Ingreso i in wQuery)
            {
                if (!tipo.Contains(i.EstadoActual))
                {
                    tipo.Add(i.EstadoActual);
                    total.Add("0");
                }

                int index = tipo.IndexOf(i.EstadoActual);
                int valor = Convert.ToInt16(total[index]);
                total[index] = (valor + 1).ToString();
            }
        }
        else
        {
            banderaB = 1;
        }
        jsonB = ObtenerObjChartDoughnutRecType(tipo, total);

        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "",
       "renderizarCharts(" + jsonA + "," + banderaA + "," + jsonB + "," + banderaB + ");", true);

    }
    #endregion

    #region Graficos
    private string ObtenerObjChartDoughnutRecType(List<string> listTipos, List<string> listTotales)
    {
        string JSObj = string.Empty;
        try
        {
            //Tipos a graficar
            string types = "['";
            //Cantidades a graficar
            string cantidades = "['";
            // Colores para graficar. Separamos por '.' porque desde JavaScript, al aplicar 'split()' no podremos separar por ',' 
            // debido a que obtendríamos un resultado incorrectos dado que el formato de una cadena de valores rgba se separan también por ','. 
            string colores = "['";

            for (int i = 0; i < listTipos.Count - 1; i++)
            {
                types = types + listTipos[i] + " - ";
                cantidades = cantidades + listTotales[i] + "','";
                types = types + listTotales[i] + "','";
                colores = colores + ValoresConstantes.listaColores[i] + "','";
            }

            types = types + listTipos.Last() + " - " + listTotales.Last() + "']";
            cantidades = cantidades + listTotales.Last() + "']";
            colores = colores + ValoresConstantes.listaColores[listTipos.Count] + "']";

            // por el momento realizaremos el obj JS como un string de c#.
            // pero lo correcto sería crear objetos para encapsular los charts deseados y luego exportar a JS.
            JSObj = "{ " +
                "type: 'doughnut'," +
                "data: " +
                    "{" +
                       " labels: " + types + "," +
                            "datasets: [{" +
                            "label: 'Cantidad'," +
                                "data: " + cantidades + "," +
                                "backgroundColor: " + colores +
                            "}]" +
                        "}," +
                        "options:" +
                    "{" +
                       " animation:" +
                        "{" +
                           " animateRotate: true" +
                            "} " +
                    "}" +
                "}";
        }
        catch (Exception ex)
        {

        }
        return JSObj;
    }
    #endregion 
}