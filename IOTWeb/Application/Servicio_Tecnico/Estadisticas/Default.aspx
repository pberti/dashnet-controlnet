﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Application_Servicio_Tecnico_Estadisticas_Default" %>

<%@ Register Src="~/UserControl/usrUltimaModificacion.ascx" TagName="usrUltimaModificacion" TagPrefix="uc1" %>
<%@ Register TagPrefix="usrError" TagName="Error" Src="~/UserControl/Error.ascx" %>
<%@ Register Src="~/UserControl/userProcesando.ascx" TagPrefix="uc1" TagName="userProcesando" %>
<%--<%@ Register TagName="ucRespuestaFormulario" TagPrefix="ucRF" Src="~/UserControl/ucRespuestaFormulario.ascx" %>
<%@ Register TagName="ucVisualizadorRespuestasFormulario" TagPrefix="ucVRF" Src="~/UserControl/ucVisualizadorRespuestasFormulario.ascx" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
        <asp:Panel ID="pnlGrilla" runat="server">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                </div>
                <div class="title_right">
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div>
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/img/Logo_Cnet_2175x457.png" Width="30%" />
                        </div>
                        <br>
                        <div class="x_title">
                            <h2><asp:Literal ID="litTitulo" runat="server" Text='<%#TituloForm%>' /></h2>
                            <div class="clearfix"></div>
                        </div>
                        <%--FILTRO--%>

                        <div class="x_content">
                            <div class="x_panel hidden-xs">
                                <div class="form-horizontal form-label-left  hidden-xs">
                                    <div class="col-md-12 col-sm-12  ">
                                        <div class="form-group">
                                            <asp:Label ID="lblFechaDesde" runat="server" Text="F. desde:" class="control-label col-md-1 col-sm-1 col-xs-12"></asp:Label>
                                            <div class="col-md-2 col-sm-2 col-xs-2">
                                                <asp:UpdatePanel runat="server">
                                                    <ContentTemplate>
                                                        <div class='input-group date dtp' id='dtpFecDesde'>
                                                            <asp:TextBox runat="server" ID="txtFecDesde" CssClass="form-control det-control" />
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                            <asp:Label ID="lblFechaHasta" runat="server" Text="F. hasta:" class="control-label col-md-1 col-sm-1 col-xs-12"></asp:Label>
                                            <div class="col-md-2 col-sm-2 col-xs-12">
                                                <asp:UpdatePanel runat="server">
                                                    <ContentTemplate>
                                                        <div class='input-group date dtp' id='dtpFecHasta'>
                                                            <asp:TextBox runat="server" ID="txtFecHasta" CssClass="form-control det-control" />
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                            <asp:Label ID="lblProductoFiltro" runat="server" Text="Producto:" class="control-label col-md-1 col-sm-1 col-xs-12"></asp:Label>
                                            <div class="col-md-2 col-sm-2 col-xs-2">
                                                <asp:DropDownList ID="ddlFiltroProducto" runat="server" CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <asp:LinkButton ID="btnBuscar" runat="server" CssClass="btn btn-round  btn-info pull-right hidden-xs" OnClick="btnBuscar_Click">
                                    <i class="fa fa-search"></i> Buscar
                                </asp:LinkButton>
                                <asp:LinkButton ID="btnDetalles" runat="server" CssClass="btn btn-round  btn-info pull-right hidden-xs" OnClick="btnDetalles_Click1">
                                        Detalles
                                </asp:LinkButton>
                                <br>
                                <asp:LinkButton ID="btnExportarPDF" runat="server" CssClass="btn btn-round  btn-info pull-right hidden-xs" OnClick="btnExportarPDF_Click">
                                    <i class="fa fa-file-excel-o"></i> Exportar a PDF
                                </asp:LinkButton>
                            </div>
                            <div class="x_panel" runat="server" id="pnlGeneral">
                                <%--INDICADORES--%>
                                <div class="panel" runat="server" id="pnlIndicadores">
                                    <div class="x_title">
                                        <h2>Indicadores</h2>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="row tile_count">
                                        <div class="col-md-4 col-sm-4 col-xs-4 tile_stats_count">
                                            <span class="count_top">Cantidad de Ingresos</span>
                                            <div class="count">
                                                <asp:Label runat="server" ID="lblIndicador1" class="count green"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-4 tile_stats_count">
                                            <span class="count_top">% Servicios Completados</span>
                                            <div class="count">
                                                <asp:Label runat="server" ID="lblIndicador2" class="count green"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-4 tile_stats_count">
                                            <span class="count_top">% Equipos en Garantía</span>
                                            <div class="count">
                                                <asp:Label runat="server" ID="lblIndicador3" class="count green"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <%--Graficos--%>
                                <div class="form-group" runat="server" id="pnlGráficos">
                                    <div class="panel col-md-6 col-sm-6 col-xs-6" id="pnlGraficoA">
                                        <div class="x_title">
                                            <h2>Tipos de Falla</h2>
                                            <div class="clearfix"></div>
                                        </div>
                                        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Panel runat="server" class="col-md-12 col-sm-12 col-xs-12" ID="Panel1">
                                                    <asp:LinkButton runat="server" ID="lbtnBuscar" CssClass="btn btn-round btn-alert hidden" OnClick="btnBuscar_Click">
                                                              Buscar </asp:LinkButton>
                                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                                        <canvas id="chartDoughnutA"></canvas>
                                                    </div>
                                                    <div id="divDoughnutLegendsA" class="col-md-5 col-sm-5 col-xs-5">
                                                    </div>
                                                </asp:Panel>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div class="panel col-md-6 col-sm-6 col-xs-6" id="pnlGraficoB">
                                        <div class="x_title">
                                            <h2>Ingresos por Estado</h2>
                                            <div class="clearfix"></div>
                                        </div>
                                        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Panel runat="server" class="col-md-12 col-sm-12 col-xs-12" ID="Panel2">
                                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                                        <canvas id="chartDoughnutB"></canvas>
                                                    </div>
                                                    <div id="divDoughnutLegendsB" class="col-md-5 col-sm-5 col-xs-5">
                                                    </div>
                                                </asp:Panel>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                                 
                                
                            </div>
                            <%--GRILLA--%>
                            <div class="x_panel" runat="server" visible="false" id="pnlDetalleGrilla">
                                    <h3>Datos</h3>
                                    <div>
                                        <asp:GridView ID="grdGrilla" runat="server"
                                            AllowSorting="false"
                                            AutoGenerateColumns="False" CssClass="table table-striped table-bordered">
                                            <Columns>
                                                <asp:BoundField DataField="Id" HeaderText="ID" SortExpression="Id" Visible="True" />
                                                <asp:BoundField DataField="Producto" HeaderText="Producto" SortExpression="Producto" />
                                                <asp:BoundField DataField="NroSerie" HeaderText="Nro Serie" SortExpression="NroSerie" />
                                                <asp:BoundField DataField="FechaIngreso" HeaderText="Fecha Ingreso" SortExpression="FechaIngreso" />
                                                <asp:BoundField DataField="EstadoActual" HeaderText="Estado Actual" SortExpression="EstadoActual" />
                                            </Columns>
                                            <PagerStyle CssClass="pgr" />
                                        </asp:GridView>
                                    </div>
                                    <br />
                                    <div class="alert alert-success alert-dismissible fade in" role="alert">
                                        <div class="pull-left">
                                            Total de Registros:
                                        <asp:Label ID="lblCantidadFilas" runat="server"></asp:Label>
                                        </div>
                                        <br />
                                        <br />
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>

    <script>
        // Para renderizar charts
        function renderizarCharts(objChartDoughnutA, banderaA, objChartDoughnutB, banderaB) {
            if (banderaA != 1) {
                var ctxDoughnut = document.getElementById('chartDoughnutA').getContext('2d');
                var chartDoughnut = new Chart(ctxDoughnut, objChartDoughnutA);
                // Generamos las leyendas
                var divDoughnutLegends = document.getElementById("divDoughnutLegendsA");
                var legends = "";
                for (index in objChartDoughnutA.data.labels) {
                    legends += "<p><i class='fa fa-square' style='color: " +
                        objChartDoughnutA.data.datasets[0].backgroundColor[index] + "'></i> " + objChartDoughnutA.data.labels[index] + " </p>";
                }
                divDoughnutLegendsA.innerHTML = legends;
            }

            if (banderaB != 1) {
                var ctxDoughnut = document.getElementById('chartDoughnutB').getContext('2d');
                var chartDoughnut = new Chart(ctxDoughnut, objChartDoughnutB);
                // Generamos las leyendas
                var divDoughnutLegends = document.getElementById("divDoughnutLegendsB");
                var legends = "";
                for (index in objChartDoughnutB.data.labels) {
                    legends += "<p><i class='fa fa-square' style='color: " +
                        objChartDoughnutB.data.datasets[0].backgroundColor[index] + "'></i> " + objChartDoughnutB.data.labels[index] + " </p>";
                }
                divDoughnutLegendsB.innerHTML = legends;
            }

        }

        window.onload = function () {
            document.getElementById('<%= lbtnBuscar.ClientID %>').click();
        };
    </script>
    <usrError:Error ID="errorMensaje" runat="server" />
</asp:Content>
