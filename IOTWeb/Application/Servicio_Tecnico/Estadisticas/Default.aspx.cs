﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IOT.Domain;
using System.Data;
using IOT.Services;

public partial class Application_Servicio_Tecnico_Estadisticas_Default : PageBase
{
    #region Propiedades
    private ServTipoPerfilMenues oServPerfilMenus = Services.Get<ServTipoPerfilMenues>();
    private ServSTFAlla oServFalla = Services.Get<ServSTFAlla>();
    private ServProducto oServProducto = Services.Get<ServProducto>();
    private ServProductoST oServIngresos = Services.Get<ServProductoST>();
    DateTime mFechaDesde;
    DateTime mFechaHasta;
    #endregion

    #region Eventos Generales de Panalla
    protected void Page_Load(object sender, EventArgs e)
    {
        if (base.IdUsuarioLogueado == -1)
        {
            Response.Redirect("~/Application/Login/Default.aspx", false);
            return;
        }
        if (!Page.IsPostBack)
            InicializacionPantalla();
    }

    private void InicializacionPantalla()
    {
        CargoPermisosdePantalla();
        CargoComboFiltro();
        DatosInicio();
        CargoGrilla();
        litTitulo.Text = "Estadísticas Servicio Técnico";
    }

    private void CargoPermisosdePantalla()
    {
        var tipoPerfilMenu = oServPerfilMenus.TraerTodosTipoPefilMenuesPorPerfil(IdPerfilUsuarioLogueado).Where(p => p.oMenu.PathPagina.Contains("Servicio_Tecnico/Estadisticas"));
        if (tipoPerfilMenu.Count() != 0)
        {
            List<TipoPerfilMenues> t = tipoPerfilMenu.ToList();
        }
        else
            Response.Redirect("~/Application/Default.aspx", false);
    }
    #endregion

    #region Eventos del Filtro
    private void CargoComboFiltro()
    {
        //Cargo ComboProductos
        List<TipoProducto> lstTipoProducto = oServProducto.TraerTodos().ToList();

        ddlFiltroProducto.DataSource = lstTipoProducto;
        ddlFiltroProducto.DataTextField = "Nombre";
        ddlFiltroProducto.DataValueField = "Id";
        ddlFiltroProducto.DataBind();
    }

    /// <summary>
    /// Completo las fechas y pongo valor cero en los campos de los indicadores
    /// </summary>
    private void DatosInicio()
    {
        //LImpio todo
        lblIndicador1.Text = "0";
        lblIndicador2.Text = "0";
        lblIndicador3.Text = "0";

        if (!string.IsNullOrEmpty(txtFecDesde.Text.ToString()))
        {
            mFechaDesde = Convert.ToDateTime(txtFecDesde.Text.ToString());
        }
        else
        {
            mFechaDesde = DateTime.Today.AddDays(-30);
            txtFecDesde.Text = mFechaDesde.ToShortDateString();
        }

        if (!string.IsNullOrEmpty(txtFecHasta.Text.ToString()))
        {
            mFechaHasta = Convert.ToDateTime(txtFecHasta.Text.ToString());
        }
        else
        {
            mFechaHasta = DateTime.Today;
            txtFecHasta.Text = mFechaHasta.ToShortDateString();
        }
    }
    #endregion Eventos del Filtro

    #region Botones
    protected void btnDetalles_Click1(object sender, EventArgs e)
    {
        if (pnlGeneral.Visible)
        {
            pnlDetalleGrilla.Visible = true;
            pnlGeneral.Visible = false;
            btnDetalles.Text = "Volver";
        }
        else
        {
            pnlDetalleGrilla.Visible = false;
            pnlGeneral.Visible = true;
            btnDetalles.Text = "Detalles";
        }
    }

    protected void btnExportarPDF_Click(object sender, EventArgs e)
    {
        string baseUrl = "http://" + Request.Url.Authority + Request.Url.Segments[0] + Request.Url.Segments[1];
        string url = baseUrl + "Servicio_Tecnico/Estadisticas/PDF/Default.aspx";

        DatosInicio();
        CargoGrilla();

        Session["FechaDesdeST"] = mFechaDesde;
        Session["FechaHastaST"] = mFechaHasta;
        Session["IdTipoProducto"] = Convert.ToInt64(ddlFiltroProducto.SelectedValue);
        Response.Redirect(url, "_blank", null);

    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        try
        {
            DatosInicio();
            CargoGrilla();

        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "btnBuscar_Click", HttpContext.Current.Request.Url.LocalPath);
        }
    }
    #endregion

    #region "Eventos Grilla"

    private void CargoGrilla()
    {
        List<ProductoST_Ingreso> ingresos = oServIngresos.TraerTodosIngresos().Where(f => f.FecUltimaModificacion >= mFechaDesde && f.FecUltimaModificacion <= mFechaHasta).ToList();
        List<ProductoST_Ingreso> wQuery = new List<ProductoST_Ingreso>();
        foreach (ProductoST_Ingreso i in ingresos)
        {
            if (i.oProductoST.IdTipoProducto == Convert.ToInt64(ddlFiltroProducto.SelectedValue))
            {
                wQuery.Add(i);
            }
        }

        grdGrilla.DataSource = wQuery;
        lblCantidadFilas.Text = wQuery.Count().ToString();
        grdGrilla.DataBind();
        CalculoIndicadores(wQuery);
    }

    private void CalculoIndicadores(List<ProductoST_Ingreso> listIngresos)
    {
        List<ProductoST_Ingreso> asistente = new List<ProductoST_Ingreso>();
        try
        {
            lblIndicador1.Text = listIngresos.Count().ToString();

            foreach (ProductoST_Ingreso i in listIngresos)
            {
                if (i.oEstadoActual.EsFinal)
                {
                    asistente.Add(i);
                }
            }

            if (listIngresos.Count() != 0)
                lblIndicador2.Text = (asistente.Count() * 100 / listIngresos.Count()).ToString() + "%";


            asistente.Clear();
            foreach (ProductoST_Ingreso i in listIngresos)
            {
                if (i.Garantia)
                {
                    asistente.Add(i);
                }
            }

            if (listIngresos.Count() != 0)
                lblIndicador3.Text = (asistente.Count() * 100 / listIngresos.Count()).ToString() + "%";

            string jsonA = "";
            string jsonB = "";
            int banderaA = 0;
            int banderaB = 0;



            //Creo listas para gráficos
            DateTime mFechaHasta = Convert.ToDateTime(txtFecHasta.Text.ToString());
            DateTime mFechaDesde = Convert.ToDateTime(txtFecDesde.Text.ToString());

            List<string> total = new List<string>();
            List<string> tipo = new List<string>();

            if (listIngresos.Count() != 0)
            {
                List<FallasXProducto> comodin = oServFalla.TraerTodosFallaXProducto().Where(f => f.IdTipoProducto == listIngresos.FirstOrDefault().oProductoST.IdTipoProducto).ToList();
                if (comodin.Count > 0)
                {
                    foreach (FallasXProducto f in comodin)
                    {
                        tipo.Add(f.NombreFalla);
                        //List<FallasXIngreso> i = oServFalla.TraerTodosFallasXIngreso().Where(x => x.IdFalla == f.IdFalla  && x.IdTipoProducto == f.IdTipoProducto && x.FecUltimaModificacion >= mFechaDesde && x.FecUltimaModificacion <= mFechaHasta).ToList();
                        List<FallasXIngreso> i = oServFalla.TraerTodosFallasXIngreso().Where(x => x.IdFalla == f.IdFalla && x.IdTipoProducto == f.IdTipoProducto).ToList();
                        i = i.Where(p => listIngresos.Any(x => x.Id == p.IdIngreso)).ToList();
                        total.Add(i.Count().ToString());
                    }
                }
                else
                {
                    banderaA = 1;
                }
                jsonA = ObtenerObjChartDoughnutRecType(tipo, total);

                total.Clear();
                tipo.Clear();

                if (listIngresos.Count > 0)
                {
                    foreach (ProductoST_Ingreso i in listIngresos)
                    {
                        if (!tipo.Contains(i.EstadoActual))
                        {
                            tipo.Add(i.EstadoActual);
                            total.Add("0");
                        }

                        int index = tipo.IndexOf(i.EstadoActual);
                        int valor = Convert.ToInt16(total[index]);
                        total[index] = (valor + 1).ToString();
                    }
                }
                else
                {
                    banderaB = 1;
                }
                jsonB = ObtenerObjChartDoughnutRecType(tipo, total);
            }

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "",
           "renderizarCharts(" + jsonA + "," + banderaA + "," + jsonB + "," + banderaB + ");", true);
        }
        catch (Exception ex)
        {
        }
    }
    #endregion

    #region Graficos
    private string ObtenerObjChartDoughnutRecType(List<string> listTipos, List<string> listTotales)
    {
        string JSObj = string.Empty;
        try
        {
            //Tipos a graficar
            string types = "['";
            //Cantidades a graficar
            string cantidades = "['";
            // Colores para graficar. Separamos por '.' porque desde JavaScript, al aplicar 'split()' no podremos separar por ',' 
            // debido a que obtendríamos un resultado incorrectos dado que el formato de una cadena de valores rgba se separan también por ','. 
            string colores = "['";

            for (int i = 0; i < listTipos.Count - 1; i++)
            {
                types = types + listTipos[i] + " - ";
                cantidades = cantidades + listTotales[i] + "','";
                types = types + listTotales[i] + "','";
                colores = colores + ValoresConstantes.listaColores[i] + "','";
            }

            types = types + listTipos.Last() + " - " + listTotales.Last() + "']";
            cantidades = cantidades + listTotales.Last() + "']";
            colores = colores + ValoresConstantes.listaColores[listTipos.Count] + "']";

            // por el momento realizaremos el obj JS como un string de c#.
            // pero lo correcto sería crear objetos para encapsular los charts deseados y luego exportar a JS.
            JSObj = "{ " +
                "type: 'doughnut'," +
                "data: " +
                    "{" +
                       " labels: " + types + "," +
                            "datasets: [{" +
                            "label: 'Cantidad'," +
                                "data: " + cantidades + "," +
                                "backgroundColor: " + colores +
                            "}]" +
                        "}," +
                        "options:" +
                    "{" +
                       " animation:" +
                        "{" +
                           " animateRotate: true" +
                            "} " +
                    "}" +
                "}";
        }
        catch (Exception ex)
        {

        }
        return JSObj;
    }
    #endregion
}