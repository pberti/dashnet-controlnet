﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Application_Servicio_Técnico_Ingresos_Default" %>

<%@ Register Src="~/UserControl/usrUltimaModificacion.ascx" TagName="usrUltimaModificacion" TagPrefix="uc1" %>
<%@ Register TagPrefix="usrError" TagName="Error" Src="~/UserControl/Error.ascx" %>
<%@ Register Src="~/UserControl/userProcesando.ascx" TagPrefix="uc1" TagName="userProcesando" %>
<%@ Register TagName="ucRespuestaFormulario" TagPrefix="ucRF" Src="~/UserControl/ucRespuestaFormulario.ascx" %>
<%@ Register TagName="ucVisualizadorRespuestasFormulario" TagPrefix="ucVRF" Src="~/UserControl/ucVisualizadorRespuestasFormulario.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <%--Vista Grilla--%>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlGrilla" runat="server">
                <div class="">
                    <div class="page-title">
                        <div class="title_left">
                        </div>
                        <div class="title_right">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Administracion de Ingresos a Servicio Técnico</h2>
                                    <div class="pull-right">
                                        <button type="submit" id="btnAgregar" runat="server" causesvalidation="False" class="btn btn-round btn-primary" onserverclick="btnAgregar_Click">
                                            <i class="fa fa-plus-circle"></i>Agregar Nuevo Ingreso</button>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <%--FILTRO--%>
                                <div class="x_content">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h2>Filtro</h2>
                                            <ul class="nav navbar-right panel_toolbox">
                                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">
                                            <%--Aca van los filtros que se quieran poner --%>
                                            <div class="form-horizontal form-label-left col-md-12 col-sm-12  ">
                                                <div class="form-group">
                                                    <asp:Label runat="server" Text="Producto: " class="control-label col-md-1 col-sm-1 col-xs-1"></asp:Label>
                                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                                        <asp:DropDownList runat="server" ID="ddlProductoFiltro" CssClass="form-control"></asp:DropDownList>
                                                    </div>
                                                    <asp:Label runat="server" Text="Estado: " class="control-label col-md-1 col-sm-1 col-xs-1"></asp:Label>
                                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                                        <asp:DropDownList runat="server" ID="ddlEstadoFiltro" CssClass="form-control"></asp:DropDownList>
                                                    </div>
                                                    <asp:Label runat="server" Text="Clientes: " class="control-label col-md-1 col-sm-1 col-xs-1"></asp:Label>
                                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                                        <asp:DropDownList runat="server" ID="ddlClientesFiltro" CssClass="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <asp:Label runat="server" Text="Nro Ref: " class="control-label col-md-1 col-sm-1 col-xs12"></asp:Label>
                                                    <div class="col-md-2 col-sm-2 col-xs-12">
                                                        <asp:TextBox ID="txtNroSerieFiltro" runat="server" class="form-control"></asp:TextBox>
                                                    </div>
                                                    <asp:Label runat="server" Text="Finalizados:" class="control-label col-md-1 col-sm-1 col-xs-6"></asp:Label>
                                                    <div class="col-md-1 col-sm-1 col-xs-6">
                                                        <asp:CheckBox ID="chkFiltroFinales" runat="server" class="form-control"></asp:CheckBox>
                                                    </div>
                                                    <asp:Label runat="server" Text="Laboratorio:" class="control-label col-md-1 col-sm-1 col-xs-12"></asp:Label>
                                                    <div class="col-md-2 col-sm-2 col-xs-12">
                                                        <asp:DropDownList runat="server" ID="ddlFiltroLaboratorio" CssClass="form-control">
                                                            <asp:ListItem Text="Todos" Value="todos"></asp:ListItem>
                                                            <asp:ListItem Text="Cordoba" Value="cordoba"></asp:ListItem>
                                                            <asp:ListItem Text="Mendoza" Value="mendoza"></asp:ListItem>
                                                            <asp:ListItem Text="Buenos Aires" Value="bsas"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <%--Nro Reclamo--%>
                                                    <asp:Label runat="server" Text="Nro Reclamo:" class="control-label col-md-2 col-sm-2 col-xs-2"></asp:Label>
                                                    <div class="col-md-2 col-sm-2 col-xs-6">
                                                        <asp:TextBox ID="txtFiltroNroReclamo" runat="server" class="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group">|
                                                    <div class="pull-right">
                                                        <asp:LinkButton runat="server" ID="btnBuscar" CssClass="btn btn-round btn-primary" OnClick="btnBuscar_Click">
                                                             <i class="fa fa-search"></i> Buscar</asp:LinkButton>
                                                    </div>

                                                </div>
                                            </div>

                                            <br />
                                        </div>
                                    </div>
                                    <%--PANEL DE ERRORES--%>
                                    <div class="alert alert-danger alert-dismissible fade in" role="alert" runat="server" id="pnlErroresGrilla" visible="false">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        <strong><i class="fa fa-warning"></i>Atencion!!!</strong>
                                        <br />
                                        <asp:Literal ID="litErrorGrilla" runat="server" Text=""></asp:Literal>
                                    </div>
                                    <%--GRILLA--%>
                                    <div class="panel">
                                        <asp:GridView ID="grdGrilla" runat="server" OnPageIndexChanging="grdGrilla_PageIndexChanging"
                                        OnSorting="grdGrilla_Sorting" OnRowCommand="grdGrilla_RowCommand" OnRowDataBound="grdGrilla_RowDataBound" AllowSorting="true"
                                        AutoGenerateColumns="False" CssClass="table table-striped table-bordered">
                                        <Columns>
                                            <asp:BoundField DataField="Id" HeaderText="" SortExpression="Id" Visible="false" />
                                            <asp:BoundField DataField="Producto" HeaderText="Producto" SortExpression="Producto" />
                                            <asp:BoundField DataField="NroReclamo" HeaderText="Nro Reclamo" SortExpression="NroReclamo" />
                                            <asp:BoundField DataField="NroSerie" HeaderText="Nro Ref" SortExpression="NroSerie" />
                                            <asp:BoundField DataField="NroGabinete" HeaderText="Nro Serie" SortExpression="NroGabinete" />
                                            <asp:BoundField DataField="FechaIngreso" HeaderText="Fecha Ingreso" SortExpression="FechaIngreso" />
                                            <asp:BoundField DataField="Cliente" HeaderText="Cliente" SortExpression="Cliente" />
                                            <asp:BoundField DataField="EstadoActual" HeaderText="Estado Actual" SortExpression="EstadoActual" />
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <div class="form-group">
                                                        <asp:LinkButton runat="server" ID="btnBuscar" CssClass="btn btn-info btn-xs" CommandName="Editar" CommandArgument='<%#Eval("Id")%>'>
                                                     <i class="fa fa-search"></i> Editar</asp:LinkButton>
                                                    </div>
                                                </ItemTemplate>
                                                <ItemStyle Width="10%" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerStyle CssClass="pgr" />
                                    </asp:GridView>
                                    </div>
                                    <br />
                                    <div class="alert alert-success alert-dismissible fade in" role="alert">
                                        <div class="pull-left">
                                            Total de Registros:
                                    <asp:Label ID="lblCantidadFilas" runat="server"></asp:Label>
                                        </div>
                                        <br />
                                        <br />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <%--Vista Detalles--%>
            <asp:Panel ID="pnlDetalles" runat="server">
                <div class="col-md-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>
                                <asp:Literal ID="litNombre" runat="server" Text='<%#TituloForm%>' /></h2>
                            <div class="clearfix"></div>
                        </div>
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <%--PANEL DE ERRORES--%>
                                <div class="alert alert-danger alert-dismissible fade in" role="alert" runat="server" id="pnlError" visible="false">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                    <strong><i class="fa fa-warning"></i>Atencion!!!</strong>
                                    <br />
                                    <asp:Literal ID="litError" runat="server" Text=""></asp:Literal>
                                </div>
                                <%--PANEL SUCCESS--%>
                                <div class="alert alert-success alert-dismissible fade in" role="alert" runat="server" id="pnlSuccess" visible="false">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                    <strong><i class="fa fa-check"></i>Se guardaron los cambios exitosamente!!</strong>
                                    <br />
                                    <asp:Literal ID="litSuccess" runat="server" Text=""></asp:Literal>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="clearfix"></div>
                        <div class="x_content">
                            <br />
                            <div class="form-horizontal form-label-left">
                                <div class="form-group">
                                    <asp:Label runat="server" Text="Producto:" class="control-label col-md-2 col-sm-2 col-xs-2"></asp:Label>
                                    <div class="col-md-2 col-sm-2 col-xs-2">
                                        <asp:DropDownList runat="server" ID="ddlProducto" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                    <asp:Label runat="server" Text="Nro Ref:" class="control-label col-md-2 col-sm-2 col-xs-2"></asp:Label>
                                    <div class="col-md-2 col-sm-2 col-xs-2">
                                        <asp:TextBox ID="txtNroSerie" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                    <asp:Label runat="server" Text="Nro Serie:" class="control-label col-md-2 col-sm-2 col-xs-2"></asp:Label>
                                    <div class="col-md-2 col-sm-2 col-xs-2">
                                        <asp:TextBox ID="txtNroGabinete" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" Text="Garantía:" class="control-label col-md-1 col-sm-1 col-xs-12"></asp:Label>
                                    <div class="col-md-1 col-sm-1 col-xs-12">
                                        <asp:CheckBox ID="chkGarantia" runat="server" class="form-control" Checked="false" TextAlign="Left" Text="" />
                                    </div>
                                     <asp:UpdatePanel runat="server">
                                        <ContentTemplate>
                                            <asp:Label runat="server" Text="Cliente:" class="control-label col-md-1 col-sm-1 col-xs-12"></asp:Label>
                                            <div class="col-md-3 col-sm-3 col-xs-12">
                                                <asp:DropDownList runat="server" ID="ddlCliente" CssClass="form-control" Width="100%" AutoPostBack="true" OnSelectedIndexChanged="ddlCliente_SelectedIndexChanged"></asp:DropDownList>
                                            </div>
                                            <%--EMAIL--%>
                                            <asp:Label runat="server" Text="Email:" class="control-label col-md-1 col-sm-1 col-xs-6"></asp:Label>
                                            <div class="col-md-5 col-sm-5 col-xs-12">
                                                <asp:TextBox ID="txtContactoEmail" runat="server" class="form-control"></asp:TextBox>
                                            </div>
                                        </ContentTemplate>
                                </asp:UpdatePanel>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="lblFechaINgreso" runat="server" Text="F. ingreso:" class="control-label col-md-1 col-sm-1 col-xs-12"></asp:Label>
                                    <div class="col-md-2 col-sm-2 col-xs-12">
                                        <asp:UpdatePanel runat="server">
                                            <ContentTemplate>
                                                <div class='input-group date dtp' id='dtpFecHasta'>
                                                    <asp:TextBox runat="server" ID="txtFechaIngreso" CssClass="form-control det-control" />
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar"></span>
                                                    </span>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <asp:Label runat="server" Text="Laboratorio:" class="control-label col-md-1 col-sm-1 col-xs-12"></asp:Label>
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        <asp:DropDownList runat="server" ID="ddlLaboratorio" CssClass="form-control" Width="100%">
                                            <asp:ListItem Text="Cordoba" Value="cordoba"></asp:ListItem>
                                            <asp:ListItem Text="Mendoza" Value="mendoza"></asp:ListItem>
                                            <asp:ListItem Text="Buenos Aires" Value="bsas"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <asp:Label runat="server" Text="Workflow:" class="control-label col-md-1 col-sm-1 col-xs-1"></asp:Label>
                                    <div class="col-md-4 col-sm-4 col-xs-2">
                                        <asp:DropDownList runat="server" ID="ddlWorkflow" CssClass="form-control" Width="100%"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" Text="Observaciones:" class="control-label col-md-1 col-sm-1 col-xs-2"></asp:Label>
                                    <div class="col-md-11 col-sm-11 col-xs-12">
                                        <asp:TextBox ID="txtObservaciones" runat="server" class="form-control"></asp:TextBox>
                                    </div>

                                </div>
                                <div class="form-group">

                                    <asp:Label runat="server" Text="NroRemito:" class="control-label col-md-1 col-sm-1 col-xs-1"></asp:Label>
                                    <div class="col-md-2 col-sm-2 col-xs-2">
                                        <asp:TextBox ID="txtNroRemito" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                    <asp:Label runat="server" Text="Falla Reportada:" class="control-label col-md-2 col-sm-2 col-xs-2"></asp:Label>
                                    <div class="col-md-7 col-sm-7 col-xs-7">
                                        <asp:TextBox ID="txtFallaReportada" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblComentario" Text="Comentario:" class="control-label col-md-1 col-sm-1 col-xs-2"></asp:Label>
                                   <div class="col-md-11 col-sm-11 col-xs-7">
                                        <asp:TextBox ID="txtComentario" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <%--NRO DE RECLAMO--%>
                                    <asp:Label runat="server" Text="Nro de Reclamo:" class="control-label col-md-2 col-sm-2 col-xs-6"></asp:Label>
                                        <div class="col-md-3 col-sm-3 col-xs-6">
                                        <asp:TextBox ID="txtNroReclamo" runat="server" class="form-control" Enabled="false" BackColor="LightGreen" Font-Bold="true" Font-Italic="true" Font-Size="XX-Large"></asp:TextBox>
                                    </div>
                                    <asp:Label runat="server" Text="Est Actual:" class="control-label col-md-1 col-sm-1 col-xs-6"></asp:Label>
                                    <div class="col-md-2 col-sm-2 col-xs-6">
                                        <asp:Label ID="lblEstadoActual" runat="server" Visible="true" class="form-control" />
                                    </div>
                                    <asp:Label runat="server" Text="Est Siguiente:" class="control-label col-md-2 col-sm-2 col-xs-6"></asp:Label>
                                    <div class="col-md-2 col-sm-2 col-xs-6">
                                        <asp:Label ID="lblEstadoSiguiente" runat="server" Visible="true" class="form-control" />
                                    </div>
                                    
                                </div>
                                <div class="pull-right">
                                    <asp:LinkButton runat="server" ID="btnSiguienteEstado" Visible="false" CssClass="btn btn-round btn-primary" OnClick="btnSiguienteEstado_Click">
                                        <i class="fa fa-arrow-circle-right"></i> Siguiente Estado</asp:LinkButton>
                                    <asp:LinkButton runat="server" ID="btnVerHistorialProduccion" Visible="false" CssClass="btn btn-round btn-primary" OnClick="btnVerHistorialProduccion_Click">
                                        <i class="fa fa-search"></i> Historial Producción</asp:LinkButton>
                                    <asp:LinkButton runat="server" ID="btnCancelar" CssClass="btn btn-round btn-danger" OnClick="btnCancelar_Click">
                                        <i class="fa fa-check"></i> Cancelar ST </asp:LinkButton>
                                    <asp:LinkButton runat="server" ID="btnGrabarIngreso" CssClass="btn btn-round btn-primary" OnClick="btnGrabarIngreso_Click">
                                        <i class="fa fa-check"></i> Grabar  </asp:LinkButton>
                                    <asp:LinkButton runat="server" ID="btnVolver" CssClass="btn btn-round btn-primary" OnClick=" btnSalir_ServerClick">
                                        <i class="fa fa-arrow-circle-left"></i> Volver  </asp:LinkButton>
                                </div>
                                <br />
                            </div>
                            <div class="clearfix"></div>
                            <br />
                            <div class="x_panel" runat="server" id="pnlTab" visible="true">
                                <%-- TAB--%>
                                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                        <li role="presentation" class="active">
                                            <a href="#tabComponentes" id="atabComponentes" role="tab" data-toggle="tab" aria-expanded="false">
                                                <i class="fa fa-info-circle"></i>Componentes
                                            </a>
                                        </li>
                                        <li role="presentation" class="">
                                            <a href="#tabHistorial" id="atabHistorial" role="tab" data-toggle="tab" aria-expanded="false">
                                                <i class="fa fa-info-circle"></i>Historial
                                            </a>
                                        </li>
                                        <li role="presentation" class="">
                                            <a href="#tabFallas" id="atabFallas" role="tab" data-toggle="tab" aria-expanded="false">
                                                <i class="fa fa-bug"></i>Fallas
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div id="myTabContent" class="tab-content">
                                    <%-- TAB COMPONENTES--%>
                                    <div role="tabpanel" class="tab-pane fade active in" id="tabComponentes" aria-labelledby="home-tab">
                                        <%--COMPONENTES--%>
                                        <div class="x_panel" runat="server" id="pnlComponentes" visible="true">
                                            <div class="x_title">
                                                <h2>
                                                    <asp:Literal ID="Literal2" runat="server" Text="Componentes del producto" /></h2>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="x_content">
                                                <asp:GridView ID="grdComponentes" runat="server" OnPageIndexChanging="grdComponentes_PageIndexChanging"
                                                    OnSorting="grdComponentes_Sorting" AllowSorting="true"
                                                    AutoGenerateColumns="False" CssClass="table table-striped table-bordered">
                                                    <Columns>
                                                        <asp:BoundField DataField="Id" HeaderText="ID" SortExpression="Id" Visible="True" />
                                                        <asp:BoundField DataField="oComponente.Nombre" HeaderText="Componente" SortExpression="oComponenteNombre" />
                                                        <asp:BoundField DataField="NroOrden" HeaderText="NroOrden" SortExpression="NroOrden" />
                                                        <asp:BoundField DataField="NroSerie" HeaderText="NroSerie" SortExpression="NroSerie" />
                                                    </Columns>
                                                    <PagerStyle CssClass="pgr" />
                                                </asp:GridView>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="alert alert-success alert-dismissible fade in" role="alert">
                                                <div class="pull-left">
                                                    Total de Registros:
                                                                  <asp:Label ID="lblCantidadComponentes" runat="server"></asp:Label>
                                                </div>
                                                <br />
                                                <br />
                                            </div>
                                        </div>
                                    </div>

                                    <%-- TAB HISTORIAL--%>
                                    <div role="tabpanel" class="tab-pane fade in" id="tabHistorial" aria-labelledby="home-tab">
                                        <%--HISTORIAL--%>
                                        <div class="x_panel" runat="server" id="pnlHistorial" visible="true">
                                            <div class="x_title">
                                                <h2>
                                                    <asp:Literal ID="Literal1" runat="server" Text="Historial del producto" /></h2>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="x_content">
                                                <asp:GridView ID="grdHistorial" runat="server" OnPageIndexChanging="grdHistorial_PageIndexChanging"
                                                    OnSorting="grdHistorial_Sorting" AllowSorting="true" OnRowDataBound="grdHistorial_RowDataBound" OnRowCommand="grdHistorial_RowCommand"
                                                    AutoGenerateColumns="False" CssClass="table table-striped table-bordered">
                                                    <Columns>
                                                        <asp:BoundField DataField="Id" HeaderText="ID" SortExpression="Id" Visible="True" />
                                                        <asp:BoundField DataField="FechaUltimaModificacion" HeaderText="Fecha de Modificación" SortExpression="FechaUltimaModificacion" />
                                                        <asp:BoundField DataField="NombreeEstadoActual" HeaderText="Estado Anterior" SortExpression="NombreeEstadoActual" />
                                                        <asp:BoundField DataField="oNuevoEstado.Nombre" HeaderText="Estado Actual" SortExpression="oEstadoActualNombre" />
                                                        <asp:BoundField DataField="Comentario" HeaderText="Comentario" SortExpression="Comentario" />
                                                        <asp:BoundField DataField="oUsuario.ApellidoYNombre2" HeaderText="Usuario" SortExpression="oUsuarioApellidoYNombre2" />
                                                        <asp:BoundField DataField="_Diferencia" HeaderText="Diferencia" SortExpression="Diferencia" />
                                                        <asp:TemplateField HeaderText="Tiempo">
                                                            <ItemTemplate>
                                                                    <asp:Label ID="lblDiferencia" runat="server"></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="10%" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <div class="pull-right">
                                                                    <asp:LinkButton ID="btnVer" runat="server" CommandName="VerForm" CommandArgument='<%#Eval("IdFormrespuestas") %>'
                                                                        CssClass="btn btn-round"> <i class="fa fa-search"></i> Ver Form</asp:LinkButton>
                                                                </div>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="10%" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <PagerStyle CssClass="pgr" />
                                                </asp:GridView>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="alert alert-success alert-dismissible fade in" role="alert">
                                                <div class="pull-left">
                                                    Total de Registros:
                                                                  <asp:Label ID="lblCantiadHistorial" runat="server"></asp:Label>
                                                </div>
                                                <br />
                                                <br />
                                            </div>
                                        </div>
                                    </div>

                                    <%-- TAB FALLAS--%>
                                    <div role="tabpanel" class="tab-pane fade in" id="tabFallas" aria-labelledby="home-tab">
                                        <%--FALLAS--%>
                                        <div class="x_panel" runat="server" id="Div1" visible="true">
                                            <div class="x_title">
                                                <h2>
                                                    <asp:Literal ID="Literal3" runat="server" Text="Historial de Fallas" /></h2>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="x_content table-responsive">
                                                <asp:GridView ID="grdFallas" runat="server" OnPageIndexChanging="grdFallas_PageIndexChanging"
                                                    OnSorting="grdFallas_Sorting" AllowSorting="true"
                                                    AutoGenerateColumns="False" CssClass="table table-striped table-bordered">
                                                    <Columns>
                                                        <asp:BoundField DataField="Id" HeaderText="ID" SortExpression="Id" Visible="false" />
                                                        <asp:BoundField DataField="FecUltimaModificacion" HeaderText="Fecha de Modificación" SortExpression="FecUltimaModificacion" />
                                                        <asp:BoundField DataField="oIngreso.NroSerie" HeaderText="Nro Referencia" SortExpression="NroSerie" />
                                                        <asp:BoundField DataField="oIngreso.NroGabinete" HeaderText="Nro Gabinete" SortExpression="NroSerie" />
                                                        <asp:BoundField DataField="oIngreso.FallaReportada" HeaderText="Falla Reportada" SortExpression="IdIngreso" />
                                                        <asp:BoundField DataField="oFalla.Nombre" HeaderText="Falla Encontrada" SortExpression="IdFalla" />
                                                        <asp:BoundField DataField="Comentario" HeaderText="Comentario Interno" SortExpression="Comentario" />
                                                    </Columns>
                                                    <PagerStyle CssClass="pgr" />
                                                </asp:GridView>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="alert alert-success alert-dismissible fade in" role="alert">
                                                <div class="pull-left">
                                                    Total de Registros:
                                                                  <asp:Label ID="lblCantidadFallas" runat="server"></asp:Label>
                                                </div>
                                                <br />
                                                <br />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </asp:Panel>
            <div class="clearfix"></div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="modal fade" id="mdlForm" tabindex="-1" role="dialog" aria-hidden="true">
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <div class="modal-body">
                    <%-- RESPUESTAS FORMULARIO --%>
                    <ucVRF:ucVisualizadorRespuestasFormulario runat="server" ID="ucVisualizadorRespuestasFormulario" />
                    <br />
                    <div class="clearfix"></div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <usrError:Error ID="errorMensaje" runat="server" />
    <uc1:userProcesando runat="server" ID="userProcesando" />
</asp:Content>

