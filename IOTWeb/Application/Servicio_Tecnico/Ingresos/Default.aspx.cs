﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IOT.ORM;
using IOT.Domain;
using System.Transactions;
using System.Web.Security;
using System.Data;
using IOT.Services;


public partial class Application_Servicio_Técnico_Ingresos_Default : PageBase
{
    #region Propiedades & Variables
    private ServProductoST oServProducto = Services.Get<ServProductoST>();
    private ServProduccion oServProducción = Services.Get<ServProduccion>();
    private ServTipoPerfilMenues oServPerfilMenus = Services.Get<ServTipoPerfilMenues>();
    private ServProducto oServTipoProducto = Services.Get<ServProducto>();
    private ServClientes oServCliente = Services.Get<ServClientes>();
    private ServWorkflow oServWorkflow = Services.Get<ServWorkflow>();
    private ServWorkflow_Ruta oServWorkflowRuta = Services.Get<ServWorkflow_Ruta>();
    private ServWorkFlow_Estado oServWorflowEstado = Services.Get<ServWorkFlow_Estado>();
    private ServWorkflow_Permisos oServWorkflowPermiso = Services.Get<ServWorkflow_Permisos>();
    private ServSTFAlla oServFallas = Services.Get<ServSTFAlla>();

    long idEstadoFiltro;
    #endregion

    #region "Main"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (base.IdUsuarioLogueado == -1)
        {
            Response.Redirect("~/Application/Login/Default.aspx", false);
            return;
        }
        if (!Page.IsPostBack)
            InicializacionPantalla();
    }

    private void InicializacionPantalla()
    {
        int bandera = Convert.ToInt16(Request.QueryString["bandera"]);
        CargoPermisosdePantalla();

        if (bandera == 1)
        {
            base.EntityId = (long)Session["ingreso"];
            base.EsNuevo = false;
            litNombre.Text = "Editar Ingreso";
            this.VisibilidadPaneles(false, true);
            btnSiguienteEstado.Visible = true;
            btnVerHistorialProduccion.Visible = true;
            pnlTab.Visible = true;
            this.CargarControles();
            ddlProducto.Enabled = false;
            ddlWorkflow.Enabled = false;
            txtNroSerie.Enabled = false;
            txtNroGabinete.Enabled = false;
            txtContactoEmail.Enabled = false;
        }
        else
        {
            DireccionOrdenacion = string.Empty;
            CargoDDLFiltro();
            this.CargoGrilla("FecUltimaModificacion descending", this.ObtenerFiltro());
        }
    }

    private void CargoPermisosdePantalla()
    {
        var tipoPerfilMenu = oServPerfilMenus.TraerTodosTipoPefilMenuesPorPerfil(IdPerfilUsuarioLogueado).Where(p => p.oMenu.PathPagina.Contains("Servicio_Tecnico/Ingresos"));
        if (tipoPerfilMenu.Count() != 0)
        {
            List<TipoPerfilMenues> t = tipoPerfilMenu.ToList();
            btnAgregar.Visible = t.First().PuedeGrabar;

        }
        else
            Response.Redirect("~/Application/Default.aspx", false);

    }
    #endregion

    #region "Métodos - Generales Pantalla"
    private void VisibilidadPaneles(bool pPnlGrilla, bool pPnlDetalle)
    {
        pnlGrilla.Visible = pPnlGrilla;
        pnlDetalles.Visible = pPnlDetalle;

        if (pnlDetalles.Visible)
        {
            CargoDDLProductos();
            CargoDDLClientes();
            CargoDDLWorkflow();
        }
    }

    /// <summary>
    /// Metodo para cargar el ddl de productos
    /// </summary>
    private void CargoDDLProductos()
    {
        List<TipoProducto> wQuery = oServTipoProducto.TraerTodos().ToList();

        ddlProducto.DataSource = wQuery;
        ddlProducto.DataTextField = "Nombre";
        ddlProducto.DataValueField = "Id";
        ddlProducto.DataBind();
    }

    private void CargoDDLFiltro()
    {
        ddlProductoFiltro.Items.Clear();
        List<TipoProducto> wQuery = oServTipoProducto.TraerTodos().ToList();

        ddlProductoFiltro.DataSource = wQuery;
        ddlProductoFiltro.DataTextField = "Nombre";
        ddlProductoFiltro.DataValueField = "Id";
        ddlProductoFiltro.DataBind();

        ddlProductoFiltro.Items.Add("");
        ddlProductoFiltro.Items.FindByText("").Selected = true;

        ddlClientesFiltro.Items.Clear();
        List<Clientes> wQueryCli = oServCliente.TraerTodos(null).OrderBy(p => p.Nombre).ToList();

        ddlClientesFiltro.DataSource = wQueryCli;
        ddlClientesFiltro.DataTextField = "Nombre";
        ddlClientesFiltro.DataValueField = "Id";
        ddlClientesFiltro.DataBind();
        ddlClientesFiltro.Items.Add("");
        ddlClientesFiltro.Items.FindByText("").Selected = true;


        ddlEstadoFiltro.Items.Clear();
        List<Workflow> asistWorkflow = oServWorkflow.TraerTodos(null).Where(w => w.Destino == "Servicio Técnico").ToList();
        List<Workflow_Ruta> asistRuta = new List<Workflow_Ruta>();
        foreach (Workflow w in asistWorkflow)
        {
            List<Workflow_Ruta> r = oServWorkflowRuta.TraerTodos().Where(x => x.IdWorkflow == w.Id).ToList();
            asistRuta.AddRange(r);
        }
        List<Workflow_Estado> comodin = new List<Workflow_Estado>();

        foreach (Workflow_Ruta r in asistRuta)
        {
            if (!comodin.Contains(r.oWorkflowEstado))
                comodin.Add(r.oWorkflowEstado);
        }

        List<Workflow_Permisos> asistpermiso = oServWorkflowPermiso.TraerXPerfil(IdPerfilUsuarioLogueado).ToList();
        List<Workflow_Estado> asistWQuery = new List<Workflow_Estado>();

        foreach (Workflow_Permisos p in asistpermiso)
        {
            if (p.PuedeVer)
                asistWQuery.Add(p.oWorkflowEstado);
        }

        List<Workflow_Estado> wQuery2 = new List<Workflow_Estado>();
        foreach (Workflow_Estado e in asistWQuery)
        {
            if (comodin.Contains(e))
                wQuery2.Add(e);
        }


        ddlEstadoFiltro.DataSource = wQuery2;
        ddlEstadoFiltro.DataTextField = "Nombre";
        ddlEstadoFiltro.DataValueField = "Id";
        ddlEstadoFiltro.DataBind();

        ddlEstadoFiltro.Items.Add("");
        ddlEstadoFiltro.Items.FindByText("").Selected = true;
    }

    private void CargoDDLClientes()
    {
        List<Clientes> wQuery = oServCliente.TraerTodos(null).OrderBy(p => p.Nombre).ToList();

        ddlCliente.DataSource = wQuery;
        ddlCliente.DataTextField = "Nombre";
        ddlCliente.DataValueField = "Id";
        ddlCliente.DataBind();
    }

    private void CargoDDLWorkflow()
    {
        List<Workflow> wQuery = oServWorkflow.TraerTodos(null).Where(w => w.Destino == "Servicio Técnico").ToList();

        ddlWorkflow.DataSource = wQuery;
        ddlWorkflow.DataTextField = "Nombre";
        ddlWorkflow.DataValueField = "Id";
        ddlWorkflow.DataBind();
    }
    #endregion

    #region Metodos - Grilla
    /// <summary>
    /// Metodo que obtiene los datos establecidos de los filtros.
    /// </summary>
    /// <returns>Metodo que devuelve un objeto con los valores del filtro.</returns>
    private ProductoST ObtenerFiltro()
    {
        ProductoST oObj = null;
        if (!string.IsNullOrEmpty(ddlProductoFiltro.SelectedItem.Text) || !string.IsNullOrEmpty(txtNroSerieFiltro.Text))
        {
            oObj = new ProductoST();
            if (!string.IsNullOrEmpty(ddlProductoFiltro.SelectedItem.Text))
            {
                oObj.IdTipoProducto = Convert.ToInt64(ddlProductoFiltro.SelectedItem.Value);
            }
            if (!string.IsNullOrEmpty(txtNroSerieFiltro.Text))
            {
                oObj.NroSerie = txtNroSerieFiltro.Text;
            }
        }
        if (!string.IsNullOrEmpty(ddlEstadoFiltro.Text))
        {
            idEstadoFiltro = Convert.ToInt64(ddlEstadoFiltro.SelectedValue);
        }
        return oObj;
    }

    /// <summary>
    /// Metodo que obtiene el ordenamiento de las columnas.
    /// </summary>
    private void ObtenerDireccionOrden(GridViewSortEventArgs evt)
    {
        if (DireccionOrdenacion == "")
        {
            DireccionOrdenacion = "ascending";
        }
        else
        {
            if (DireccionOrdenacion == "ascending")
                DireccionOrdenacion = "descending";
            else
                DireccionOrdenacion = "ascending";
        }
    }


    /// <summary>
    /// Metodo que cargs los datos en la grilla de la pagina.
    /// </summary>
    /// <param name="pOrderBy">Criterio de ordenamiento de las columnas</param>
    /// <param name="pObj">Objeto Viaje para determinar si existe algun filtro a aplicar a la grilla</param>
    private void CargoGrilla(string pOrderBy, ProductoST pObj)
    {
        this.VisibilidadPaneles(true, false);
        CriterioOrdenacion = pOrderBy;
        List<ProductoST_Ingreso> wQuery = null;
        try
        {
            if (pObj == null)
            {
                //wQuery = oServProducto.TraerTodosIngresos().OrderBy("FecUltimaModificacion descending").ToList();
                wQuery = oServProducto.TraerTodosIngresos().OrderBy(CriterioOrdenacion).ToList();
            }
            else
            {
                wQuery = oServProducto.TraerTodosIngresosFiltro(pObj).OrderBy(CriterioOrdenacion).ToList();
            }
            if (wQuery == null || wQuery.Count() == 0)
            {
                if (!string.IsNullOrEmpty(txtNroSerieFiltro.Text))
                {
                    List<ProductoST_Componentes> componentes = oServProducto.TraerTodosComponentes().Where(c => c.NroSerie == txtNroSerieFiltro.Text).ToList();
                    long idProd = componentes.FirstOrDefault().IdProductoST;
                    wQuery = oServProducto.TraerTodosIngresos().Where(p => p.IdProductoST == idProd).ToList();
                }
            }
            //Filtro por estado
            if (idEstadoFiltro != 0)
            {
                wQuery.RemoveAll(e => e.IdEstadoActual != idEstadoFiltro);
            }
            //Filtro finalizados
            if (!chkFiltroFinales.Checked)
            {
                wQuery.RemoveAll(e => e.oEstadoActual.EsFinal);
            }
            //Filtro por Cliente
            if (ddlClientesFiltro.SelectedItem.Text != "")
            {
                wQuery = wQuery.Where(p => p.IdCliente == Convert.ToInt64(ddlClientesFiltro.SelectedValue.ToString())).ToList();
            }
            //Filtro por Laboratorio
            if (ddlFiltroLaboratorio.SelectedValue != "todos")
            {
                string mLaboratrio = ddlFiltroLaboratorio.SelectedValue.ToString();
                wQuery = wQuery.Where(p => p.Laboratorio == mLaboratrio ).ToList();
            }
            //Filtro Nro de Reclamo
            if (txtFiltroNroReclamo.Text.Trim().Length > 0)
            {
                wQuery = wQuery.Where(p => p.NroReclamo != null && p.NroReclamo.Contains(txtFiltroNroReclamo.Text.Trim())).ToList();
            }
            grdGrilla.DataSource = wQuery;
            grdGrilla.DataBind();
            lblCantidadFilas.Text = wQuery.Count().ToString();
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargoGrilla", HttpContext.Current.Request.Url.LocalPath);
        }
        finally
        {
            wQuery = null;
        }
    }

    protected void grdGrilla_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdGrilla.PageIndex = e.NewPageIndex;
        this.CargoGrilla(CriterioOrdenacion, this.ObtenerFiltro());
    }

    protected void grdGrilla_Sorting(object sender, GridViewSortEventArgs e)
    {
        this.ObtenerDireccionOrden(e);
        this.CargoGrilla(string.Format("{0} {1}", e.SortExpression, DireccionOrdenacion), this.ObtenerFiltro());
    }

    protected void grdGrilla_RowCommand(Object sender, CommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Editar")
            {
                base.EntityId = Convert.ToInt64(e.CommandArgument);
                base.EsNuevo = false;
                litNombre.Text = "Editar Ingreso";
                this.VisibilidadPaneles(false, true);
                btnSiguienteEstado.Visible = true;
                btnVerHistorialProduccion.Visible = true;
                pnlTab.Visible = true;
                this.CargarControles();
                ddlProducto.Enabled = false;
                ddlWorkflow.Enabled = false;
                txtNroSerie.Enabled = false;
            }
        }
        catch (Exception)
        {

            throw;
        }
        
    }
    protected void grdGrilla_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            int mDiasAmarillo = 3;
            int mDiasRojo = 4;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                long mId = Convert.ToInt64(DataBinder.Eval(e.Row.DataItem, "Id").ToString());
                ProductoST_Historial oProdHist = oServProducto.TraerTodosHistoriales().Where(p => p.IdProductoSTIngreso == mId && p.IdNuevoEstado == 19).FirstOrDefault();
                if (oProdHist != null)
                {
                    DateTime _FechaIngreso = oProdHist.FecUltimaModificacion;
                    if (_FechaIngreso.DayOfWeek == DayOfWeek.Wednesday || _FechaIngreso.DayOfWeek == DayOfWeek.Thursday || _FechaIngreso.DayOfWeek == DayOfWeek.Friday)
                    {
                        mDiasAmarillo = 5;
                        mDiasRojo = 6;
                    }

                    if (_FechaIngreso.AddDays(mDiasAmarillo) <= DateTime.Now)
                        e.Row.BackColor = System.Drawing.Color.Yellow;
                    if (_FechaIngreso.AddDays(mDiasRojo) <= DateTime.Now)
                        e.Row.BackColor = System.Drawing.Color.LightCoral;

                    //Calculo de Tiempo en este estado
                    string mTemp1 = oProdHist.oEstadoAnterior.Descripcion;
                    string mTemp2 = oProdHist.NombreeEstadoActual;
                }
                
            }
        }
        catch (Exception Ex)
        {

            throw;
        }


    }
    #endregion

    #region "Eventos Botones"
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        try
        {
            this.CargoGrilla("FecUltimaModificacion ascending", this.ObtenerFiltro());

        }
        catch (SystemException ex)
        {
            errorMensaje.ErrorBind(ex, "btnBuscar_Click", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    protected void btnAgregar_Click(object sender, EventArgs e)
    {
        TituloForm = "Nuevo Producto";
        this.CrearNuevo();
    }

    protected void btnSalir_ServerClick(object sender, EventArgs e)
    {
        int bandera = Convert.ToInt16(Request.QueryString["bandera"]);

        if (bandera == 1)
        {
            Response.Redirect("~/Application/Servicio_Tecnico/Ingresos/Default.aspx?bandera=0", false);
        }

        else
        {
            this.CargoGrilla("FecUltimaModificacion ascending", this.ObtenerFiltro());
            Limpiar();
            ddlProducto.Enabled = true;
            ddlWorkflow.Enabled = true;
            txtNroSerie.Enabled = true;
            this.VisibilidadPaneles(true, false);
            ddlProductoFiltro.Items.FindByText("").Selected = true;
            txtNroSerieFiltro.Text = string.Empty;
        }
    }

    protected void btnGrabarIngreso_Click(object sender, EventArgs e)
    {
        Page.Validate();
        if (Page.IsValid)
        {
            pnlSuccess.Visible = false;
            this.GrabarDatos();
        }
        if (!pnlError.Visible)
        {
            CargarControles();
            pnlTab.Visible = true;
            btnSiguienteEstado.Visible = true;
            btnVerHistorialProduccion.Visible = true;
            pnlSuccess.Visible = true;
        }
    }

    protected void btnSiguienteEstado_Click(object sender, EventArgs e)
    {
        try
        {
            ProductoST_Ingreso ingreso = oServProducto.TraerUnicoIngresoXId(EntityId);
            Session["productost"] = EntityId;

            if (ComprobarPermiso(ingreso.IdEstadoActual, IdPerfilUsuarioLogueado))
            {
                if (EstadoSiguiente(ingreso.IdEstadoActual, ingreso.IdWorkflow) != null)
                {
                    Response.Redirect(ingreso.oEstadoActual.Pantalla + "destino=s", false);
                }
                else
                {
                    pnlError.Visible = true;
                    litError.Text = "El producto no tiene un estado siguiente";
                }
            }
            else
            {
                pnlError.Visible = true;
                litError.Text = "No tiene permiso para pasar este producto de estado";
            }
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "Error en el Metodo para pasar de estado", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        //Pasa la solictud a estado cancelado
        try
        {
            ProductoST_Ingreso ingreso = oServProducto.TraerUnicoIngresoXId(EntityId);
            Session["productost"] = EntityId;

            //CAmbio el Estado a Cancelado

            if (ComprobarPermiso(ingreso.IdEstadoActual, IdPerfilUsuarioLogueado))
            {
                Workflow_Estado oEstadoCancelado = oServWorflowEstado.TraerTodos().Where(p => p.Nombre == "ST - Cancelado").FirstOrDefault();
                ingreso.IdEstadoActual = oEstadoCancelado.Id;
                oServProducto.ActualizarIngreso(ingreso);
                Response.Redirect(oEstadoCancelado.Pantalla + "destino=s", false);
            }
            else
            {
                pnlError.Visible = true;
                litError.Text = "No tiene permiso para pasar este producto de estado";
            }
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "Error en el Metodo para pasar de estado", HttpContext.Current.Request.Url.LocalPath);
        }

    }
    protected void btnVerHistorialProduccion_Click(object sender, EventArgs e)
    {
        ProductoST_Ingreso ingreso = oServProducto.TraerUnicoIngresoXId(EntityId);
        ProductoST prod = oServProducto.TraerUnicoXId(ingreso.IdProductoST);

        if (prod.IdProduccion != 0)
        {
            Response.Redirect("~/Application/Produccion/Produccion/Default.aspx?Bandera=2&id=" + prod.IdProduccion.ToString(), "_blank", null);
        }
        else
        {
            pnlError.Visible = true;
            litError.Text = "Este producto no se encuentra registrado el el módulo de producción";
        }
    }
    #endregion

    #region Metodos - Vista Detalle
    /// <summary>
    /// Metodo que carga los datos de los controles.
    /// </summary>
    private void CargarControles()
    {
        try
        {
            var oObj = oServProducto.TraerUnicoIngresoXId(base.EntityId);

            ProductoST producto = oServProducto.TraerUnicoXId(oObj.IdProductoST);

            ddlCliente.Items.FindByValue(oObj.IdCliente.ToString()).Selected = true;
            ddlProducto.Items.FindByValue(oObj.oProductoST.IdTipoProducto.ToString()).Selected = true;
            ddlWorkflow.Items.FindByValue(oObj.IdWorkflow.ToString()).Selected = true;
            ddlLaboratorio.SelectedValue = oObj.Laboratorio.ToString();
            //ddlLaboratorio.Items.FindByValue(oObj.Laboratorio.ToString()).Selected = true;

            lblEstadoActual.Text = oServWorkflowRuta.TraerTodos().Where(r => r.IdWorkflowEstado == oObj.IdEstadoActual).FirstOrDefault().oWorkflowEstado.Nombre;
            lblEstadoSiguiente.Text = oServWorkflowRuta.TraerTodos().Where(r => r.IdWorkflowEstado == oObj.IdEstadoActual).FirstOrDefault().EstadoSiguiente;

            txtFechaIngreso.Text = oObj.FechaIngreso;
            chkGarantia.Checked = oObj.Garantia;
            txtNroRemito.Text = oObj.NroRemito;
            txtNroSerie.Text = producto.NroSerie;
            txtNroGabinete.Text = producto.NroGabinete;
            txtFallaReportada.Text = oObj.FallaReportada;
            txtObservaciones.Text = oObj.Observaciones;

            txtContactoEmail.Text = oObj.ContactoEmail;
            if (oObj.NroReclamo != null)
                txtNroReclamo.Text = oObj.NroReclamo;
            else
                txtNroReclamo.Text = "No Posee";

            CargoGrillaComponentes();
            CargoGrillaHistorial();
            CargoGrillaFallas();
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargarControles", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    /// <summary>
    /// Metodo que guarda los datos establecidos en la pagina.
    /// </summary>
    private void GrabarDatos()
    {
        try
        {
            pnlError.Visible = false;
            ProductoST_Ingreso oObj = ObtenerDatos();

            //Controlo que no haya un ST en proceso
            List<ProductoST_Ingreso> asistente = oServProducto.TraerTodosIngresos().Where(i => i.IdProductoST == oObj.IdProductoST).ToList();
            bool bandera = true;

            foreach (ProductoST_Ingreso i in asistente)
            {
                if (!i.oEstadoActual.EsFinal && EsNuevo == true && i.Id != 0)
                {
                    bandera = false;
                }
            }

            if (bandera)
            {
                if (EsNuevo)
                {
                    EntityId = oServProducto.AgregarIngreso(oObj, IdUsuarioLogueado);
                    if (!pnlError.Visible)
                        EsNuevo = false;
                }
                else
                {
                    oServProducto.ActualizarIngreso(oObj);
                }
            }
            else
            {
                pnlError.Visible = true;
                litError.Text = "Ya hay un servicio técnico en proceso";
            }
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "GrabarDatos", HttpContext.Current.Request.Url.LocalPath);
        }
    }



    /// <summary>
    /// Obtiene los datos de la pagina
    /// </summary>
    /// <returns>Retorna un objecto con los datos establecidos en la pagina</returns>
    private ProductoST_Ingreso ObtenerDatos()
    {
        ProductoST_Ingreso oObj = null;
        try
        {
            ProductoST productoST = new ProductoST();
            oObj = new ProductoST_Ingreso();
            var oUsuario = base.TraerUsuarioLogeado();
            if (!EsNuevo)
            {
                oObj = oServProducto.TraerUnicoIngresoXId(base.EntityId);
                oObj.Garantia = chkGarantia.Checked;
            }
            else
            {
                if (!string.IsNullOrEmpty(txtNroSerie.Text))
                {
                    productoST = oServProducto.TraerTodos().Where(p => (p.NroSerie == txtNroSerie.Text) && p.IdTipoProducto == Convert.ToInt64(ddlProducto.SelectedItem.Value)).FirstOrDefault();

                }
                else
                {
                    productoST = oServProducto.TraerTodos().Where(p => (p.NroGabinete == txtNroGabinete.Text) && p.IdTipoProducto == Convert.ToInt64(ddlProducto.SelectedItem.Value)).FirstOrDefault();
                }

                if (productoST == null)
                {
                    productoST = CrearProductoST(oObj);
                }


                if (!string.IsNullOrEmpty(txtNroSerie.Text))
                {
                    productoST = oServProducto.TraerTodos().Where(p => (p.NroSerie == txtNroSerie.Text) && p.IdTipoProducto == Convert.ToInt64(ddlProducto.SelectedItem.Value)).FirstOrDefault();

                }
                else
                {
                    productoST = oServProducto.TraerTodos().Where(p => (p.NroGabinete == txtNroGabinete.Text) && p.IdTipoProducto == Convert.ToInt64(ddlProducto.SelectedItem.Value)).FirstOrDefault();
                }

                if (productoST != null)
                {
                    oObj.IdWorkflow = Convert.ToInt64(ddlWorkflow.SelectedItem.Value);
                    oObj.IdProductoST = productoST.Id;

                    if (chkGarantia.Checked == false)
                    {
                        if (productoST.IdProduccion != 0)
                        {
                            Produccion prod = new Produccion();
                            prod = oServProducción.TraerUnicoXId(productoST.IdProduccion);
                            if (prod.FechaEnvioCliente >= DateTime.Now.AddYears(-1))
                            {
                                oObj.Garantia = true;
                            }
                        }
                        if (!oObj.Garantia)
                        {
                            ProductoST_Ingreso ingreso = new ProductoST_Ingreso();
                            ingreso = oServProducto.TraerTodosIngresos().Where(i => i.IdProductoST == productoST.Id).OrderBy("FecUltimaModificacion descending").FirstOrDefault();
                            if (ingreso != null && ingreso.FecUltimaModificacion >= DateTime.Now.AddMonths(-6))
                            {
                                oObj.Garantia = true;
                            }
                        }
                    }
                    else
                    {
                        oObj.Garantia = chkGarantia.Checked;
                    }
                }
                else
                {
                    oObj = null;
                    pnlError.Visible = true;
                    litError.Text = "Fallo la creacíon del producto";
                }
            }

            oObj.FecUltimaModificacion = Convert.ToDateTime(txtFechaIngreso.Text);
            oObj.NroRemito = txtNroRemito.Text;
            oObj.Observaciones = txtObservaciones.Text;
            oObj.FallaReportada = txtFallaReportada.Text;
            oObj.IdCliente = Convert.ToInt64(ddlCliente.SelectedItem.Value);

            oObj.Laboratorio = ddlLaboratorio.SelectedValue.ToString();

            oObj.IdUsuarioModifico = oUsuario.Id;
            oObj.ContactoEmail = txtContactoEmail.Text.Trim();
            oObj.NroReclamo = CrearNroReclamo();

        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "ObtenerDatos", HttpContext.Current.Request.Url.LocalPath);
            pnlError.Visible = true;
            litError.Text = "Error";
        }
        return oObj;
    }

    private string CrearNroReclamo()
    {
        string mNroReclamo = DateTime.Now.ToString("yyyyMMdd") + "-";

        string mReclamoAux = null;
        ProductoST_Ingreso oReclamoAux = oServProducto.TraerTodosIngresos().Where(p => p.NroReclamo != null && p.NroReclamo.Contains(mNroReclamo)).OrderByDescending(p => p.Id).FirstOrDefault();
        if (oReclamoAux != null)
        {
            mReclamoAux = oReclamoAux.NroReclamo;
        }

        if (mReclamoAux == null)
        {
            mNroReclamo = mNroReclamo + "201";
        }
        else
        {
            string[] ArrayNroReclamo = mReclamoAux.Split('-');
            int mNro = Convert.ToInt32(ArrayNroReclamo[1]) + 1;
            mNroReclamo = mNroReclamo + mNro.ToString("#0");
        }
        return mNroReclamo;
    }


    /// <summary>
    /// Crea el productoST y en caso de que el producto ya este registrado en produccion lo relaciona con el mismo
    /// </summary>
    /// <param name="ingreso"></param>
    /// <returns></returns>
    private ProductoST CrearProductoST(ProductoST_Ingreso ingreso)
    {
        ProductoST oObj = null;
        try
        {
            oObj = new ProductoST();
            var oUsuario = base.TraerUsuarioLogeado();
            oObj.NroSerie = txtNroSerie.Text;
            oObj.NroGabinete = txtNroGabinete.Text;
            oObj.IdTipoProducto = Convert.ToInt64(ddlProducto.SelectedItem.Value);

            //FALTA VER COMO BUSCO LA COPARACIÓN CON EL NRO DE SERIE DE PRODUCCIÓN PARA HACER ESA RELACIÓN
            string NroSerieAASS = string.Empty;
            string NroSeriePPPC = string.Empty;
            string NroSerieContador = string.Empty;

            if (txtNroSerie.Text.Length == 13)
            {
                NroSerieAASS = txtNroSerie.Text.Substring(0, 4);
                NroSeriePPPC = txtNroSerie.Text.Substring(4, 4);
                NroSerieContador = txtNroSerie.Text.Substring(8);

                Produccion produccion = oServProducción.TraerTodos().Where(p => p.NroSerieAASS == NroSerieAASS && p.NroSeriePPPC == NroSeriePPPC && p.NroSerieContador == NroSerieContador).FirstOrDefault();
                if (produccion == null)
                {
                    Produccion_Componentes componente = oServProducción.TraerTodosProduccion_Componente().Where(c => c.NroSerie == txtNroGabinete.Text).FirstOrDefault();
                    if (componente != null)
                    {
                        produccion = oServProducción.TraerUnicoXId(componente.IdProduccion);
                    }
                }

                if (produccion != null)
                    oObj.IdProduccion = produccion.Id;
            }

            if (string.IsNullOrEmpty(txtNroSerie.Text))
            {
                Produccion_Componentes componente = oServProducción.TraerTodosProduccion_Componente().Where(c => c.NroSerie == txtNroGabinete.Text).FirstOrDefault();
                if (componente != null)
                {
                    Produccion produccion = oServProducción.TraerUnicoXId(componente.IdProduccion);
                    if (produccion != null)
                        oObj.IdProduccion = produccion.Id;
                }
            }

            oObj.Id = oServProducto.Agregar(oObj);
        }

        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CrearProductoSt", HttpContext.Current.Request.Url.LocalPath);
        }
        return oObj;
    }

    /// <summary>
    /// Metodo que establece los datos en la pagina para crear un nuevo objecto TipoPerfil
    /// </summary>
    private void CrearNuevo()
    {
        this.Limpiar();
        litNombre.Text = "Nuevo Ingreso";
        this.VisibilidadPaneles(false, true);
        EsNuevo = true;
        txtFechaIngreso.Text = DateTime.Now.ToShortDateString();
        pnlTab.Visible = false;
        btnSiguienteEstado.Visible = false;
        btnVerHistorialProduccion.Visible = false;
    }

    /// <summary>
    /// Metodo que limpia los datos establecidos en la pagina.
    /// </summary>
    private void Limpiar()
    {
        txtFallaReportada.Text = string.Empty;
        chkGarantia.Checked = false;
        txtNroRemito.Text = string.Empty;
        txtNroSerie.Text = string.Empty;
        txtNroGabinete.Text = string.Empty;
        txtObservaciones.Text = string.Empty;
        lblEstadoActual.Text = string.Empty;
        lblEstadoSiguiente.Text = string.Empty;
        txtFechaIngreso.Text = string.Empty;
        txtContactoEmail.Text = string.Empty;
        txtNroReclamo.Text = string.Empty;
        pnlError.Visible = false;
    }


    protected void ddlCliente_SelectedIndexChanged(object sender, EventArgs e)
    {
        long mIdCliente = Convert.ToInt64(ddlCliente.SelectedValue);
        Clientes oCliente = oServCliente.TraerUnicoXId(mIdCliente);
        if (oCliente != null)
        {
            txtContactoEmail.Text = oCliente.Correo;
        }
    }
    #endregion

    #region COMPONENTES
    private void CargoGrillaComponentes()
    {
        List<ProductoST_Componentes> wQuery = null;
        try
        {
            ProductoST_Ingreso producto = oServProducto.TraerUnicoIngresoXId(EntityId);
            wQuery = oServProducto.TraerTodosComponentes().Where(c => c.IdProductoST == producto.IdProductoST).ToList();

            grdComponentes.DataSource = wQuery;
            grdComponentes.DataBind();
            lblCantidadComponentes.Text = wQuery.Count().ToString();
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargoGrilla", HttpContext.Current.Request.Url.LocalPath);
        }
        finally
        {
            wQuery = null;
        }
    }

    protected void grdComponentes_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdComponentes.PageIndex = e.NewPageIndex;
        this.CargoGrillaComponentes();
    }

    protected void grdComponentes_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargoGrillaComponentes();
    }
    #endregion

    #region Historial
    private void CargoGrillaHistorial()
    {
        List<ProductoST_Historial> wQuery = null;
        try
        {

            wQuery = oServProducto.TraerTodosHistoriales().Where(h => h.IdProductoSTIngreso == EntityId).ToList();


            grdHistorial.DataSource = wQuery;
            grdHistorial.DataBind();
            lblCantiadHistorial.Text = wQuery.Count().ToString();
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargoGrilla", HttpContext.Current.Request.Url.LocalPath);
        }
        finally
        {
            wQuery = null;
        }
    }

    protected void grdHistorial_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdHistorial.PageIndex = e.NewPageIndex;
        this.CargoGrillaHistorial();
    }

    /// <summary>
    /// Habilito o deshabilito el boton para ver el formulario dependiendo de si ese historial corresponde a un estado con form o no
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdHistorial_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex >= 0)
        {
            LinkButton Ver = new LinkButton();
            Ver = ((LinkButton)e.Row.FindControl("btnVer"));

            

            long idHistorial = Convert.ToInt64(e.Row.Cells[0].Text);
            ProductoST_Historial oHistorial = oServProducto.TraerUnicoHistorialXId(idHistorial);


            if (oHistorial.IdFormulario == 0)
            {
                Ver.Enabled = false;
            }
            //CALCULO DE TIEMPO ENTRE LOS ESTADOS
            try
            {
                if (oHistorial != null)
                {
                    if (oHistorial.IdEstadoAnterior != 0 && oHistorial.Diferencia == 0)
                    {
                        //No es un estado inicial
                        //Busco el estado anterior para sacar la fecha de inicio
                        DateTime mFechaIngresoEstado = oServProducto.TraerTodosHistoriales().Where(p => p.IdNuevoEstado == oHistorial.IdEstadoAnterior && p.IdProductoSTIngreso == oHistorial.IdProductoSTIngreso).FirstOrDefault().FecUltimaModificacion;
                        DateTime mFechaEgresoEstado = oHistorial.FecUltimaModificacion;

                        var mDiferencia = Utils.CantidadHorasLaborales_Duracion(mFechaIngresoEstado, mFechaEgresoEstado);

                        Label lblDiferencia = new Label();
                        lblDiferencia = ((Label)e.Row.FindControl("lblDiferencia"));
                        lblDiferencia.Text = Utils.ToReadableString(mDiferencia);


                        oHistorial.Diferencia = Convert.ToInt64(mDiferencia.TotalMinutes);
                        oServProducto.ActualizarHistorial(oHistorial);
                    }
                }
            }
            catch (Exception E)
            {
                throw;
            }
            

        }
    }

    protected void grdHistorial_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "VerForm")
        {
            base.EntityId = Convert.ToInt64(e.CommandArgument);
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "AbrirModal", "AbrirModal('mdlForm')", true);
            ucVisualizadorRespuestasFormulario.CargarRespuetasSegunIdFormRespuesta(EntityId, "CerrarForm");
        }

    }

    protected void grdHistorial_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargoGrillaHistorial();
    }

    public void CerrarForm()
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "CerrarModal", "CerrarModal('mdlForm')", true);
    }
    #endregion

    #region TAB FALLAS
    private void CargoGrillaFallas()
    {
        List<FallasXIngreso> wQuery = null;
        try
        {
            wQuery = oServFallas.TraerTodosFallasXIngreso().Where(h => h.IdIngreso == EntityId).ToList();
            grdFallas.DataSource = wQuery;
            grdFallas.DataBind();
            lblCantidadFallas.Text = wQuery.Count().ToString();
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargoGrilla", HttpContext.Current.Request.Url.LocalPath);
        }
        finally
        {
            wQuery = null;
        }
    }

    protected void grdFallas_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdFallas.PageIndex = e.NewPageIndex;
        this.CargoGrillaFallas();
    }

    protected void grdFallas_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargoGrillaFallas();
    }
    #endregion




}