﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IOT.ORM;
using IOT.Domain;
using System.Transactions;
using System.Web.Security;
using System.Data;
using IOT.Services;

public partial class Application_Servicio_Técnico_ProductosST_Default : PageBase
{
    #region Propiedades & Variables
    private ServProductoST oServProducto = Services.Get<ServProductoST>();
    private ServTipoPerfilMenues oServPerfilMenus = Services.Get<ServTipoPerfilMenues>();
    private ServProducto oServTipoProducto = Services.Get<ServProducto>();
    private ServClientes oServCliente = Services.Get<ServClientes>();
    private ServWorkflow oServWorkflow = Services.Get<ServWorkflow>();
    private ServWorkflow_Ruta oServWorkflowRuta = Services.Get<ServWorkflow_Ruta>();
    private ServProduccion oServProduccion = Services.Get<ServProduccion>();
    #endregion

    #region "Main"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (base.IdUsuarioLogueado == -1)
        {
            Response.Redirect("~/Application/Login/Default.aspx", false);
            return;
        }
        if (!Page.IsPostBack)
            InicializacionPantalla();
    }
    private void InicializacionPantalla()
    {
        CargoPermisosdePantalla();
        VisibilidadPaneles(true, false);
        CargoDDLFiltro();
    }

    private void CargoPermisosdePantalla()
    {
        var tipoPerfilMenu = oServPerfilMenus.TraerTodosTipoPefilMenuesPorPerfil(IdPerfilUsuarioLogueado).Where(p => p.oMenu.PathPagina.Contains("Servicio_Tecnico/ProductosST"));
        if (tipoPerfilMenu.Count() != 0)
        {
            List<TipoPerfilMenues> t = tipoPerfilMenu.ToList();

        }
        else
            Response.Redirect("~/Application/Default.aspx", false);

    }

    #endregion

    #region "Métodos - Generales Pantalla"
    private void VisibilidadPaneles(bool pPnlGrilla, bool pPnlDetalle)
    {
        pnlGrilla.Visible = pPnlGrilla;
        pnlDetalles.Visible = pPnlDetalle;
    }

    private void CargoDDLFiltro()
    {
        List<TipoProducto> wQuery = oServTipoProducto.TraerTodos().ToList();

        ddlProductoFiltro.DataSource = wQuery;
        ddlProductoFiltro.DataTextField = "Nombre";
        ddlProductoFiltro.DataValueField = "Id";
        ddlProductoFiltro.DataBind();

        ddlProductoFiltro.Items.Add("");
        ddlProductoFiltro.Items.FindByText("").Selected = true;


        ddlClientesFiltro.Items.Clear();
        List<Clientes> wQueryCli = oServCliente.TraerTodos(null).OrderBy(p => p.Nombre).ToList();

        ddlClientesFiltro.DataSource = wQueryCli;
        ddlClientesFiltro.DataTextField = "Nombre";
        ddlClientesFiltro.DataValueField = "Id";
        ddlClientesFiltro.DataBind();
        ddlClientesFiltro.Items.Add("");
        ddlClientesFiltro.Items.FindByText("").Selected = true;
    }
    #endregion

    #region Metodos - Grilla
    /// <summary>
    /// Metodo que obtiene los datos establecidos de los filtros.
    /// </summary>
    /// <returns>Metodo que devuelve un objeto con los valores del filtro.</returns>
    private ProductoST ObtenerFiltro()
    {
        ProductoST oObj = null;
        if (!string.IsNullOrEmpty(ddlProductoFiltro.SelectedItem.Text) || !string.IsNullOrEmpty(txtNroSerieFiltro.Text))
        {
            oObj = new ProductoST();
            if (!string.IsNullOrEmpty(ddlProductoFiltro.SelectedItem.Text))
            {
                oObj.IdTipoProducto = Convert.ToInt64(ddlProductoFiltro.SelectedItem.Value);
            }
            if (!string.IsNullOrEmpty(txtNroSerieFiltro.Text))
            {
                oObj.NroSerie = txtNroSerieFiltro.Text;
            }
        }
        return oObj;
    }

    /// <summary>
    /// Metodo que obtiene el ordenamiento de las columnas.
    /// </summary>
    private void ObtenerDireccionOrden(GridViewSortEventArgs evt)
    {
        if (DireccionOrdenacion == "")
        {
            DireccionOrdenacion = "ascending";
        }
        else
        {
            if (DireccionOrdenacion == "ascending")
                DireccionOrdenacion = "descending";
            else
                DireccionOrdenacion = "ascending";
        }
    }


    /// <summary>
    /// Metodo que cargs los datos en la grilla de la pagina.
    /// </summary>
    /// <param name="pOrderBy">Criterio de ordenamiento de las columnas</param>
    /// <param name="pObj">Objeto Viaje para determinar si existe algun filtro a aplicar a la grilla</param>
    private void CargoGrilla(string pOrderBy, ProductoST pObj)
    {
        CriterioOrdenacion = pOrderBy;
        List<ProductoST> wQuery = null;
        try
        {
            if (pObj != null)
            {
                if (!string.IsNullOrEmpty(pObj.NroSerie) || pObj.IdTipoProducto != 0)
                {
                    wQuery = oServProducto.TraerTodosFiltro(pObj).ToList();

                    //Controlo si no existe un componente con el numero de serie buscado
                    if (wQuery.Count == 0 && !string.IsNullOrEmpty(txtNroSerieFiltro.Text))
                    {
                        wQuery = oServProducto.TraerTodos().Where(p => p.NroGabinete == pObj.NroSerie).ToList();

                        if (wQuery.Count == 0)
                        {
                            ProductoST_Componentes componente = oServProducto.TraerTodosComponentes().Where(c => c.NroSerie == txtNroSerieFiltro.Text).FirstOrDefault();
                            if (componente != null)
                            {
                                wQuery = oServProducto.TraerTodos().Where(p => p.Id == componente.IdProductoST).ToList();
                            }
                            else
                            {
                                Produccion_Componentes componenteprod = oServProduccion.TraerTodosProduccion_Componente().Where(c => c.NroSerie == txtNroSerieFiltro.Text).FirstOrDefault();
                                if (componenteprod != null)
                                {
                                    pObj.NroSerie = componenteprod.oProduccion.NroSerie;
                                    wQuery = oServProducto.TraerTodosFiltro(pObj).ToList();
                                }
                            }
                        }
                    }
                }

            }
            else
            {
                wQuery = oServProducto.TraerTodos().ToList();
            }

            if (ddlClientesFiltro.SelectedItem.Text != "")
            {
                //1- Traigo los registros de iot_productost_ingreso por Cliente
                long mIdCliente = Convert.ToInt64(ddlClientesFiltro.SelectedValue.ToString());
                List<ProductoST_Ingreso> lstPI = oServProducto.TraerTodosIngresos().ToList();

                var wQuery2 = from a in wQuery
                            where (from b in lstPI
                                where b.IdCliente == mIdCliente
                                select b.IdProductoST).Contains(a.Id)
                            select a;
                wQuery2.ToList();
                grdGrilla.DataSource = wQuery2;
                lblCantidadFilas.Text = wQuery2.Count().ToString();
            }
            else
            {
                grdGrilla.DataSource = wQuery;
                lblCantidadFilas.Text = wQuery.Count().ToString();
            }
            
            grdGrilla.DataBind();
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargoGrilla", HttpContext.Current.Request.Url.LocalPath);
        }
        finally
        {
            wQuery = null;
        }
    }

    protected void grdGrilla_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdGrilla.PageIndex = e.NewPageIndex;
        this.CargoGrilla(CriterioOrdenacion, this.ObtenerFiltro());
    }

    protected void grdGrilla_Sorting(object sender, GridViewSortEventArgs e)
    {
        this.ObtenerDireccionOrden(e);
        this.CargoGrilla(string.Format("{0} {1}", e.SortExpression, DireccionOrdenacion), this.ObtenerFiltro());
    }

    protected void grdGrilla_RowCommand(Object sender, CommandEventArgs e)
    {
        if (e.CommandName == "Editar")
        {
            base.EntityId = Convert.ToInt64(e.CommandArgument);
            base.EsNuevo = false;
            litNombre.Text = "Detalles Equipo";
            this.VisibilidadPaneles(false, true);
            usrUltimaModificacion1.Visible = true;
            this.CargarControles();
        }
    }
    #endregion

    #region "Eventos Botones"
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        try
        {
            this.CargoGrilla("NroSerie ascending", this.ObtenerFiltro());

        }
        catch (SystemException ex)
        {
            errorMensaje.ErrorBind(ex, "btnBuscar_Click", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    protected void btnSalir_ServerClick(object sender, EventArgs e)
    {
        this.CargoGrilla("NroSerie ascending", this.ObtenerFiltro());
        this.VisibilidadPaneles(true, false);
    }

    protected void btnGrabarIngreso_Click(object sender, EventArgs e)
    {
        Page.Validate();
        if (Page.IsValid)
        {
            this.GrabarDatos();
        }
    }

    protected void btnSiguienteEstado_Click(object sender, EventArgs e)
    {
        try
        {
            ProductoST_Ingreso ingreso = oServProducto.TraerUnicoIngresoXId(EntityId);
            Session["productost"] = EntityId;

            if (ComprobarPermiso(ingreso.IdEstadoActual, IdPerfilUsuarioLogueado))
            {
                if (EstadoSiguiente(ingreso.IdEstadoActual, ingreso.IdWorkflow) != null)
                {
                    Response.Redirect(ingreso.oEstadoActual.Pantalla + "destino=s", false);
                }
                else
                {
                    pnlError.Visible = true;
                    litError.Text = "El producto no tiene un estado siguiente";
                }
            }
            else
            {
                pnlError.Visible = true;
                litError.Text = "No tiene permiso para pasar este producto de estado";
            }
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "Error en el Metodo para pasar de estado", HttpContext.Current.Request.Url.LocalPath);
        }
    }
    #endregion

    #region Metodos - Vista Detalle
    /// <summary>
    /// Metodo que carga los datos de los controles.
    /// </summary>
    private void CargarControles()
    {
        try
        {
            var oObj = oServProducto.TraerUnicoXId(base.EntityId);

            lblProducto.Text = oObj.oTipoProducto.Nombre;
            txtNroSerie.Text = oObj.NroSerie;
            txtNroGabinete.Text = oObj.NroGabinete;
            CargoGrillaIngresos();
            CargoGrillaHistorial();
            CargoGrillaComponentes();
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargarControles", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    /// <summary>
    /// Metodo que guarda los datos establecidos en la pagina.
    /// </summary>
    private void GrabarDatos()
    {
        try
        {
            ProductoST oObj = ObtenerDatos();
            if (!pnlError.Visible)
            {
                oServProducto.Actualizar(oObj);
                this.CargoGrilla("NroSerie ascending", this.ObtenerFiltro());
                this.VisibilidadPaneles(true, false);
            }
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "GrabarDatos", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    /// <summary>
    /// Obtiene los datos de la pagina
    /// </summary>
    /// <returns>Retorna un objecto con los datos establecidos en la pagina</returns>
    private ProductoST ObtenerDatos()
    {
        ProductoST oObj = null;
        try
        {
            var oUsuario = base.TraerUsuarioLogeado();
            oObj = oServProducto.TraerUnicoXId(base.EntityId);

            TipoProducto tipo = oServTipoProducto.TraerTodos().Where(t => t.Nombre == lblProducto.Text).FirstOrDefault();
            ProductoST prod = oServProducto.TraerTodos().Where(p => p.IdTipoProducto == tipo.Id && p.NroSerie == txtNroSerie.Text && p.Id != oObj.Id).FirstOrDefault();

            if (prod == null)
                oObj.NroSerie = txtNroSerie.Text;
            else
            {
                pnlError.Visible = true;
                litError.Text = "Ya existe un producto con ese número de serie";
            }

        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "ObtenerDatos", HttpContext.Current.Request.Url.LocalPath);
        }
        return oObj;
    }
    #endregion

    #region COMPONENTES
    private void CargoGrillaComponentes()
    {
        List<ProductoST_Componentes> wQuery = null;
        try
        {
            wQuery = oServProducto.TraerTodosComponentes().Where(c => c.IdProductoST == EntityId).ToList();

            grdComponentes.DataSource = wQuery;
            grdComponentes.DataBind();
            //lblCantidadComponentes.Text = wQuery.Count().ToString();
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargoGrilla", HttpContext.Current.Request.Url.LocalPath);
        }
        finally
        {
            wQuery = null;
        }
    }

    protected void grdComponentes_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdComponentes.PageIndex = e.NewPageIndex;
        this.CargoGrillaComponentes();
    }

    protected void grdComponentes_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargoGrillaComponentes();
    }
    protected void grdComponentes_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Editar")
        {
            btnCompBorrar_Click(null, null);
            
            long mIdComponente = Convert.ToInt64(e.CommandArgument);
            ProductoST_Componentes oObj = oServProducto.TraerUnicoComponenteXId(mIdComponente);
            lblComponenteId.Text = mIdComponente.ToString();
            lblComponente.Text = oObj.oComponente.Nombre;
            txtComponenteNrodeSerie.Text = oObj.NroSerie;
            pnlComponenteEditar.Visible = true;
        }
    }

    #region Botones Grabar y Limpiar componentes
    protected void btnCompGrabar_Click(object sender, EventArgs e)
    {
        ProductoST_Componentes oObj = oServProducto.TraerUnicoComponenteXId(Convert.ToInt64(lblComponenteId.Text));
        oObj.NroSerie = txtComponenteNrodeSerie.Text.Trim();

        oServProducto.ActualizarComponenete(oObj);
        btnCompBorrar_Click(null, null);
        CargoGrillaComponentes();
    }

    protected void btnCompBorrar_Click(object sender, EventArgs e)
    {
        lblComponente.Text = "";
        lblComponenteId.Text = "0";
        txtComponenteNrodeSerie.Text = "";
        pnlComponenteEditar.Visible = false;
    }
    #endregion

    #endregion

    #region Ingresos
    private void CargoGrillaIngresos()
    {
        List<ProductoST_Ingreso> wQuery = null;
        try
        {

            wQuery = oServProducto.TraerTodosIngresos().Where(i => i.IdProductoST == EntityId).OrderBy("FecUltimaModificacion descending").ToList();

            if (wQuery.Count != 0)
            {
                grdIngresos.DataSource = wQuery;
                grdIngresos.DataBind();
                lblCantidadIngresos.Text = wQuery.Count().ToString();
            }
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargoGrillaIngresos", HttpContext.Current.Request.Url.LocalPath);
        }
        finally
        {
            wQuery = null;
        }
    }

    protected void grdIngresos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdIngresos.PageIndex = e.NewPageIndex;
        this.CargoGrillaIngresos();
    }

    protected void grdIngresos_Sorting(object sender, GridViewSortEventArgs e)
    {
        this.CargoGrillaIngresos();
    }

    protected void grdIngresos_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Editar")
        {
            Session["ingreso"] = Convert.ToInt64(e.CommandArgument);
            Response.Redirect("~/Application/Servicio_Tecnico/Ingresos/Default.aspx?bandera=1", "_blank", null);
        }
    }
    #endregion

    #region Historial
    private void CargoGrillaHistorial()
    {
        List<Produccion_Historial_Estado> wQuery = null;
        try
        {
            ProductoST prod = oServProducto.TraerUnicoXId(EntityId);

            if (prod.IdProduccion != 0)
            {
                wQuery = oServProduccion.TraerHistorialXProduccion(prod.IdProduccion).ToList();

                grdHistorial.DataSource = wQuery;
                grdHistorial.DataBind();
                lblCantiadHistorial.Text = wQuery.Count().ToString();
            }
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargoGrilla", HttpContext.Current.Request.Url.LocalPath);
        }
        finally
        {
            wQuery = null;
        }
    }

    protected void grdHistorial_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdHistorial.PageIndex = e.NewPageIndex;
        this.CargoGrillaHistorial();
    }

    /// <summary>
    /// Habilito o deshabilito el boton para ver el formulario dependiendo de si ese historial corresponde a un estado con form o no
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdHistorial_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex >= 0)
        {
            LinkButton Ver = new LinkButton();
            Ver = ((LinkButton)e.Row.FindControl("btnVer"));

            long idHistorial = Convert.ToInt64(e.Row.Cells[0].Text);
            Produccion_Historial_Estado historial = oServProduccion.TraerUnicoXIdProduccion_Historial(idHistorial);

            if (historial.IdFormulario == 0)
            {
                Ver.Enabled = false;
            }

        }
    }

    protected void grdHistorial_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "VerForm")
        {
            base.EntityId = Convert.ToInt64(e.CommandArgument);
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "AbrirModal", "AbrirModal('mdlForm')", true);
            ucVisualizadorRespuestasFormulario.CargarRespuetasSegunIdFormRespuesta(EntityId, "CerrarForm");
        }

    }

    protected void grdHistorial_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargoGrillaHistorial();
    }

    public void CerrarForm()
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "CerrarModal", "CerrarModal('mdlForm')", true);
    }
    #endregion




}