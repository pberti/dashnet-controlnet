﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Application_Servicio_Técnico_ProductosST_Default" %>

<%@ Register Src="~/UserControl/usrUltimaModificacion.ascx" TagName="usrUltimaModificacion" TagPrefix="uc1" %>
<%@ Register TagPrefix="usrError" TagName="Error" Src="~/UserControl/Error.ascx" %>
<%@ Register Src="~/UserControl/userProcesando.ascx" TagPrefix="uc1" TagName="userProcesando" %>
<%@ Register TagName="ucRespuestaFormulario" TagPrefix="ucRF" Src="~/UserControl/ucRespuestaFormulario.ascx" %>
<%@ Register TagName="ucVisualizadorRespuestasFormulario" TagPrefix="ucVRF" Src="~/UserControl/ucVisualizadorRespuestasFormulario.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <%--Vista Grilla--%>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlGrilla" runat="server">
                <div class="">
                    <div class="page-title">
                        <div class="title_left">
                        </div>
                        <div class="title_right">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Administracion Productos de Servicio Técnico</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <%--FILTRO--%>
                                <div class="x_content">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h2>Filtro</h2>
                                            <ul class="nav navbar-right panel_toolbox">
                                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">
                                            <div class="form-horizontal form-label-left pull-left hidden-xs">
                                                <%--Aca van los filtros que se quieran poner --%>
                                                <div class="col-md-12 col-sm-12  ">
                                                    <asp:Label runat="server" Text="Producto:" class="control-label col-md-1 col-sm-1 col-xs-6"></asp:Label>
                                                    <div class="col-md-3 col-sm-3 col-xs-6">
                                                        <asp:DropDownList runat="server" ID="ddlProductoFiltro" CssClass="form-control"></asp:DropDownList>
                                                    </div>
                                                    <asp:Label runat="server" Text="Nro Serie:" class="control-label col-md-1 col-sm-1 col-xs-6"></asp:Label>
                                                    <div class="col-md-3 col-sm-3 col-xs-6">
                                                        <asp:TextBox ID="txtNroSerieFiltro" runat="server" class="form-control"></asp:TextBox>
                                                    </div>
                                                    <asp:Label runat="server" Text="Clientes: " class="control-label col-md-1 col-sm-1 col-xs-1"></asp:Label>
                                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                                        <asp:DropDownList runat="server" ID="ddlClientesFiltro" CssClass="form-control"></asp:DropDownList>
                                                    </div>
                                                    <br>
                                                </div>
                                            </div>
                                            <div class="pull-right">
                                                <asp:LinkButton runat="server" ID="btnBuscar" CssClass="btn btn-round btn-primary" OnClick="btnBuscar_Click">
                                                     <i class="fa fa-search"></i> Buscar</asp:LinkButton>

                                            </div>
                                            <br />
                                        </div>
                                    </div>
                                    <%--PANEL DE ERRORES--%>
                                    <div class="alert alert-danger alert-dismissible fade in" role="alert" runat="server" id="pnlErroresGrilla" visible="false">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true"></span>
                                        </button>
                                        <strong><i class="fa fa-warning"></i>Atencion!!!</strong>
                                        <br />
                                        <asp:Literal ID="litErrorGrilla" runat="server" Text=""></asp:Literal>
                                    </div>
                                    <%--GRILLA--%>
                                    <asp:GridView ID="grdGrilla" runat="server" OnPageIndexChanging="grdGrilla_PageIndexChanging"
                                        OnSorting="grdGrilla_Sorting" OnRowCommand="grdGrilla_RowCommand" AllowSorting="true"
                                        AutoGenerateColumns="False" CssClass="table table-striped table-bordered">
                                        <Columns>
                                            <asp:BoundField DataField="Id" HeaderText="ID" SortExpression="Id" Visible="True" />
                                            <asp:BoundField DataField="Producto" HeaderText="Producto" SortExpression="Producto" />
                                            <asp:BoundField DataField="NroSerie" HeaderText="Nro Ref" SortExpression="NroSerie" />
                                            <asp:BoundField DataField="NroGabinete" HeaderText="Nro Serie" SortExpression="NroGabinete"/>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <div class="form-group">
                                                        <asp:LinkButton runat="server" ID="btnBuscar" CssClass="btn btn-info btn-xs" CommandName="Editar" CommandArgument='<%#Eval("Id")%>'>
                                                     <i class="fa fa-search"></i> Detalles</asp:LinkButton>
                                                    </div>
                                                </ItemTemplate>
                                                <ItemStyle Width="15%" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerStyle CssClass="pgr" />
                                    </asp:GridView>
                                    <br />
                                    <div class="alert alert-success alert-dismissible fade in" role="alert">
                                        <div class="pull-left">
                                            Total de Registros:
                                    <asp:Label ID="lblCantidadFilas" runat="server"></asp:Label>
                                        </div>

                                        <br />
                                        <br />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <%--Vista Detalles--%>
            <asp:Panel ID="pnlDetalles" runat="server">
                <div class="col-md-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>
                                <asp:Literal ID="litNombre" runat="server" Text='<%#TituloForm%>' /></h2>
                            <div class="clearfix"></div>
                        </div>
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <%--PANEL DE ERRORES--%>
                                <div class="alert alert-danger alert-dismissible fade in" role="alert" runat="server" id="pnlError" visible="false">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                    <strong><i class="fa fa-warning"></i>Atencion!!!</strong>
                                    <br />
                                    <asp:Literal ID="litError" runat="server" Text=""></asp:Literal>
                                </div>
                                <%--PANEL SUCCESS--%>
                                <div class="alert alert-success alert-dismissible fade in" role="alert" runat="server" id="pnlSuccess" visible="false">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                    <strong><i class="fa fa-check"></i>Operacion Exitosa!!!</strong>
                                    <br />
                                    <asp:Literal ID="litSuccess" runat="server" Text=""></asp:Literal>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="clearfix"></div>
                        <div class="x_content">
                            <br />
                            <div class="form-horizontal form-label-left">
                                <div class="form-group">
                                    <asp:Label runat="server" Text="Producto:" class="control-label col-md-2 col-sm-2 col-xs-2"></asp:Label>
                                    <div class="col-md-2 col-sm-2 col-xs-2">
                                        <asp:Label ID="lblProducto" runat="server" Visible="true" class="form-control" />
                                    </div>
                                    <asp:Label runat="server" Text="Nro Ref:" class="control-label col-md-2 col-sm-2 col-xs-2"></asp:Label>
                                    <div class="col-md-2 col-sm-2 col-xs-2">
                                        <asp:TextBox ID="txtNroSerie" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                    <asp:Label runat="server" Text="Nro Serie:" class="control-label col-md-2 col-sm-2 col-xs-2"></asp:Label>
                                    <div class="col-md-2 col-sm-2 col-xs-2">
                                        <asp:TextBox ID="txtNroGabinete" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="pull-right">
                                    <asp:LinkButton runat="server" ID="btnGrabarIngreso" CssClass="btn btn-round btn-primary" OnClick="btnGrabarIngreso_Click">
                             <i class="fa fa-check"></i> Grabar  </asp:LinkButton>
                                    <asp:LinkButton runat="server" ID="btnVolver" CssClass="btn btn-round btn-primary" OnClick=" btnSalir_ServerClick">
                             <i class="fa fa-arrow-circle-left"></i> Volver  </asp:LinkButton>
                                </div>
                                <br />
                            </div>
                            <div class="clearfix"></div>
                            <uc1:usrUltimaModificacion ID="usrUltimaModificacion1" runat="server" />


                            <div class="x_panel" runat="server" id="pnlTab" visible="true">
                                <%-- TAB--%>
                                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                        <li role="presentation" class="active">
                                            <a href="#tabComponentes" id="atabComponentes" role="tab" data-toggle="tab" aria-expanded="false">
                                                <i class="fa fa-info-circle"></i>Componentes
                                            </a>
                                        </li>
                                        <li role="presentation" class="">
                                            <a href="#tabHistorialIngresos" id="atabHistorialIngresos" role="tab" data-toggle="tab" aria-expanded="false">
                                                <i class="fa fa-info-circle"></i>Servicio Técnico
                                            </a>
                                        </li>
                                        <li role="presentation" class="">
                                            <a href="#tabHistorialProduccion" id="atabHistorialProduccion" role="tab" data-toggle="tab" aria-expanded="false">
                                                <i class="fa fa-info-circle"></i>Historial Produccion
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div id="myTabContent" class="tab-content">
                                    <%-- TAB COMPONENTES--%>
                                    <div role="tabpanel" class="tab-pane fade active in" id="tabComponentes" aria-labelledby="home-tab">
                                        <%--COMPONENTES--%>
                                        <div class="x_panel" runat="server" id="pnlComponentes" visible="true">
                                            <div class="x_title">
                                                <h2>
                                                    <asp:Literal ID="Literal2" runat="server" Text="Componentes del producto" /></h2>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="x_content">
                                                <asp:UpdatePanel runat="server">
                                                    <ContentTemplate>

                                                        <asp:GridView ID="grdComponentes" runat="server" 
                                                            OnRowCommand="grdComponentes_RowCommand"
                                                            OnPageIndexChanging="grdComponentes_PageIndexChanging"
                                                            OnSorting="grdComponentes_Sorting" AllowSorting="true"
                                                            AutoGenerateColumns="False" CssClass="table table-striped table-bordered">
                                                            <Columns>
                                                                <asp:BoundField DataField="Id" HeaderText="ID" SortExpression="Id" Visible="True" />
                                                                <asp:BoundField DataField="oComponente.Nombre" HeaderText="Componente" SortExpression="oComponenteNombre" />
                                                                <asp:BoundField DataField="NroOrden" HeaderText="NroOrden" SortExpression="NroOrden" />
                                                                <asp:BoundField DataField="NroSerie" HeaderText="NroSerie" SortExpression="NroSerie" />
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <div class="form-group">
                                                                            <asp:LinkButton runat="server" ID="btnEditar" CssClass="btn btn-info btn-xs" CommandName="Editar" CommandArgument='<%#Eval("Id")%>'>
                                                                         <i class="fa fa-edit"></i> Editar</asp:LinkButton>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="15%" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <PagerStyle CssClass="pgr" />
                                                        </asp:GridView>
                                                        <asp:Panel ID="pnlComponenteEditar" runat="server" Visible="false">
                                                            <div class="form-horizontal form-label-left">
                                                            <div class="form-group">
                                                                <asp:Label ID="lblComponenteId" runat="server" Visible="false" class="form-control" />
                                                                <asp:Label ID="lblComponente" runat="server" Visible="true" class="control-label col-md-2 col-sm-2 col-xs-2" />
                                                                <asp:Label runat="server" Text="Nro de Serie:" class="control-label col-md-2 col-sm-2 col-xs-2"></asp:Label>
                                                                <div class="col-md-2 col-sm-2 col-xs-2">
                                                                    <asp:TextBox ID="txtComponenteNrodeSerie" runat="server" class="form-control"></asp:TextBox>
                                                                </div>
                                                                <div class="pull-right">
                                                                    <asp:LinkButton runat="server" ID="btnCompGrabar" CssClass="btn btn-round btn-primary" OnClick="btnCompGrabar_Click">
                                                                        <i class="fa fa-check"></i> Grabar  </asp:LinkButton>
                                                                    <asp:LinkButton runat="server" ID="btnCompBorrar" CssClass="btn btn-round btn-primary" OnClick="btnCompBorrar_Click">
                                                                        <i class="fa fa-eraser"></i> Limpiar  </asp:LinkButton>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </asp:Panel>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
<%--                                            <div class="clearfix"></div>
                                            <div class="alert alert-success alert-dismissible fade in" role="alert">
                                                <div class="pull-left">
                                                    Total de Registros:
                                                    <asp:Label ID="lblCantidadComponentes" runat="server"></asp:Label>
                                                </div>
                                                <br />
                                                <br />
                                            </div>--%>

                                        </div>
                                    </div>

                                    <%-- TAB INGRESOS--%>
                                    <div role="tabpanel" class="tab-pane fade in" id="tabHistorialIngresos" aria-labelledby="home-tab">
                                        <%--INGRESOS--%>
                                        <div class="x_panel" runat="server" id="Div1" visible="true">
                                            <div class="x_title">
                                                <h2>
                                                    <asp:Literal ID="Literal3" runat="server" Text="Ingresos del Producto" /></h2>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="x_content">
                                                <asp:GridView ID="grdIngresos" runat="server" OnPageIndexChanging="grdIngresos_PageIndexChanging"
                                                    OnSorting="grdIngresos_Sorting" OnRowCommand="grdIngresos_RowCommand" AllowSorting="true"
                                                    AutoGenerateColumns="False" CssClass="table table-striped table-bordered">
                                                    <Columns>
                                                        <asp:BoundField DataField="Id" HeaderText="ID" SortExpression="Id" Visible="True" />
                                                        <asp:BoundField DataField="FechaIngreso" HeaderText="Fecha Ingreso" SortExpression="FechaIngreso" />
                                                        <asp:BoundField DataField="EstadoActual" HeaderText="Estado Actual" SortExpression="EstadoActual" />
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <div class="form-group">
                                                                    <asp:LinkButton runat="server" ID="btnEditar" CssClass="btn btn-info btn-xs" CommandName="Editar" CommandArgument='<%#Eval("Id")%>'>
                                                     <i class="fa fa-search"></i> Detalles</asp:LinkButton>
                                                                </div>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="15%" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <PagerStyle CssClass="pgr" />
                                                </asp:GridView>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="alert alert-success alert-dismissible fade in" role="alert">
                                                <div class="pull-left">
                                                    Total de Registros:
                                                                  <asp:Label ID="lblCantidadIngresos" runat="server"></asp:Label>
                                                </div>
                                                <br />
                                                <br />
                                            </div>
                                        </div>
                                    </div>
                                    <%-- TAB HISTORIAL--%>
                                    <div role="tabpanel" class="tab-pane fade in" id="tabHistorialProduccion" aria-labelledby="home-tab">
                                        <%--HISTORIAL--%>
                                        <div class="x_panel" runat="server" id="pnlHistorial" visible="true">
                                            <div class="x_title">
                                                <h2>
                                                    <asp:Literal ID="Literal1" runat="server" Text="Historial del producto" /></h2>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="x_content">
                                                <asp:GridView ID="grdHistorial" runat="server" OnPageIndexChanging="grdHistorial_PageIndexChanging"
                                                    OnSorting="grdHistorial_Sorting" AllowSorting="true" OnRowDataBound="grdHistorial_RowDataBound" OnRowCommand="grdHistorial_RowCommand"
                                                    AutoGenerateColumns="False" CssClass="table table-striped table-bordered">
                                                    <Columns>
                                                        <asp:BoundField DataField="Id" HeaderText="ID" SortExpression="Id" Visible="True" />
                                                        <asp:BoundField DataField="FechaUltimaModificacion" HeaderText="Fecha de Modificación" SortExpression="FechaUltimaModificacion" />
                                                        <asp:BoundField DataField="NombreeEstadoActual" HeaderText="Estado Actual" SortExpression="NombreeEstadoActual" />
                                                        <asp:BoundField DataField="oNuevoEstado.Nombre" HeaderText="Nuevo Estado" SortExpression="oEstadoActualNombre" />
                                                        <asp:BoundField DataField="Comentario" HeaderText="Comentario" SortExpression="Comentario" />
                                                        <asp:BoundField DataField="oUsuario.ApellidoYNombre2" HeaderText="Usuario" SortExpression="oUsuarioApellidoYNombre2" />
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <div class="pull-right">
                                                                    <asp:LinkButton ID="btnVer" runat="server" CommandName="VerForm" CommandArgument='<%#Eval("IdFormrespuestas") %>'
                                                                        CssClass="btn btn-round"> <i class="fa fa-search"></i> Ver Form</asp:LinkButton>
                                                                </div>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="10%" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <PagerStyle CssClass="pgr" />
                                                </asp:GridView>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="alert alert-success alert-dismissible fade in" role="alert">
                                                <div class="pull-left">
                                                    Total de Registros:
                                                                  <asp:Label ID="lblCantiadHistorial" runat="server"></asp:Label>
                                                </div>
                                                <br />
                                                <br />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="modal fade" id="mdlForm" tabindex="-1" role="dialog" aria-hidden="true">
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <div class="modal-body">
                    <%-- RESPUESTAS FORMULARIO --%>
                    <ucVRF:ucVisualizadorRespuestasFormulario runat="server" ID="ucVisualizadorRespuestasFormulario" />
                    <br />
                    <div class="clearfix"></div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <usrError:Error ID="errorMensaje" runat="server" />
    <uc1:userProcesando runat="server" ID="userProcesando" />
</asp:Content>
