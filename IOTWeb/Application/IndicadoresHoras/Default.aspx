﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Application_IndicadoresHoras_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:Panel ID="pnlGrilla" runat="server">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                </div>
                <div class="title_right">
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div>
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/img/Logo_Cnet_2175x457.png" Width="30%"/>
                        </div>
                        <div class="x_title">
                            <h2>Indicadores Carga de Horas</h2>
                            <div class="clearfix"></div>
                        </div>
                        <%--FILTRO--%>
                        <div class="x_content">
                            <br class="x_panel">
                                <div class="form-horizontal form-label-left">
                                    <div class="col-md-10 col-sm-10 col-xs-10" >

                                        <div class="form-group">
                                            <asp:Label ID="Label2" runat="server" Text="F. desde:" class="control-label col-md-1 col-sm-1 col-xs-2"></asp:Label>
                                            <div class="col-md-2 col-sm-2 col-xs-3">
                                                <asp:UpdatePanel runat="server">
                                                    <ContentTemplate>
                                                        <div class='input-group date dtp' id='dtpFecDesde'>
                                                            <asp:TextBox runat="server" ID="txtFecDesde" CssClass="form-control det-control" />
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div> 
                                            <asp:Label ID="Label4" runat="server" Text="F. hasta:" class="control-label col-md-1 col-sm-1 col-xs-2" ></asp:Label>
                                            <div class="col-md-2 col-sm-2 col-xs-3">
                                                <asp:UpdatePanel runat="server">
                                                    <ContentTemplate>
                                                        <div class='input-group date dtp' id='dtpFecHasta'>
                                                            <asp:TextBox runat="server" ID="txtFecHasta" CssClass="form-control det-control" />
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" id="btnBuscar" runat="server" causesvalidation="False" class="btn btn-round btn-info pull-right hidden-xs" onserverclick="btnBuscar_Click">
                                    <i class="fa fa-search"></i> Buscar
                                </button>

                            </div>
                            <%--GRILLA HORAS POR RECURSO--%>
                            <div class="x_panel" runat="server" visible="false" id="pnlDetalleGrilla">
                                <h3>Horas Por Recurso</h3>
                                <div class="table-responsive">
                                    <asp:GridView ID="grdGrilla" runat="server"
                                         AllowSorting ="false" 
                                        AutoGenerateColumns="False" CssClass="table table-striped table-bordered">
                                        <Columns>
                                            <%-- Pos:0 --%><asp:BoundField DataField="Id" HeaderText="ID" SortExpression="Id" Visible="false" />
                                            <%-- Pos:2 --%><asp:BoundField DataField="NombreYApellido" HeaderText="Usuario" SortExpression="NombreYApellido"  HeaderStyle-Font-Bold="true"/>
                                            <%-- Pos:3 --%><asp:BoundField DataField="_INP" HeaderText="INP" SortExpression="INP"  HeaderStyle-Font-Bold="true" Visible ="true"/>
                                            <%-- Pos:3 --%><asp:BoundField DataField="_IPI" HeaderText="IPI" SortExpression="IPI"  HeaderStyle-Font-Bold="true" Visible ="true"/>
                                            <%-- Pos:3 --%><asp:BoundField DataField="_PHI" HeaderText="PHI" SortExpression="PHI"  HeaderStyle-Font-Bold="true" Visible ="true"/>
                                            <%-- Pos:3 --%><asp:BoundField DataField="_PHP" HeaderText="PHP" SortExpression="PHP"  HeaderStyle-Font-Bold="true" Visible ="true"/>
                                            <%-- Pos:3 --%><asp:BoundField DataField="_SOP" HeaderText="SOP" SortExpression="SOP"  HeaderStyle-Font-Bold="true" Visible ="true"/>
                                            <%-- Pos:3 --%><asp:BoundField DataField="_SSCC" HeaderText="SSCC" SortExpression="SSCC"  HeaderStyle-Font-Bold="true" Visible ="true"/>
                                            <%-- Pos:3 --%><asp:BoundField DataField="_STP" HeaderText="STP" SortExpression="STP"  HeaderStyle-Font-Bold="true" Visible ="true"/>
                                            <%-- Pos:3 --%><asp:BoundField DataField="_CCTOTAL" HeaderText="Total General" SortExpression="CCTOTAL"  HeaderStyle-Font-Bold="true" Visible ="true"/>
                                        </Columns>
                                        <PagerStyle CssClass="pgr" />
                                    </asp:GridView>
                                </div>
                                <button type="submit" id="btnExcel_Recursos" runat="server" causesvalidation="False" class="btn btn-round btn-info pull-right hidden-xs" onserverclick="btnExcel_Recursos_ServerClick">
                                    <i class="fa fa-file-excel-o"></i> Excel
                                </button>
                            </div> 
                            <%--GRILLAS Horas por Centro de Costo,  Por Proyecto y Por Centro de Costo y Proyecto--%>
                            <div class="form-horizontal form-label-left">
                                <div class="col-md-4 col-sm-4 col-xs-6" >
                                    <%--GRILLAS Horas por Centro de Costo--%>
                                    <div class="x_panel" runat="server" visible="false" id="pnlHorasPorCC">
                                        <h3>Horas por Centro de Costo</h3>
                                        <div class="table-responsive">
                                            <asp:GridView ID="grdGrillaCC" runat="server" AllowSorting ="false" AutoGenerateColumns="False" CssClass="table table-striped table-bordered" OnRowDataBound="grdGrilla_RowDataBound">
                                            <Columns>
                                                <%-- Pos:0 --%><asp:BoundField DataField="Id" HeaderText="ID" SortExpression="Id" Visible="false" />
                                                <%-- Pos:2 --%><asp:BoundField DataField="CC" HeaderText="CC" SortExpression="CC"  HeaderStyle-Font-Bold="true"/>
                                                <%-- Pos:3 --%><asp:BoundField DataField="_Horas" HeaderText="Horas" SortExpression="Horas"  HeaderStyle-Font-Bold="true"/>
                                                <%-- Pos:3 --%><asp:BoundField DataField="_Porcentaje" HeaderText="%" SortExpression="Porcentaje"  HeaderStyle-Font-Bold="true"/>
                                            </Columns>
                                            <PagerStyle CssClass="pgr" />
                                        </asp:GridView>
                                        </div>
                                        <button type="submit" id="btnExcel_PorCC" runat="server" causesvalidation="False" class="btn btn-round btn-info pull-right hidden-xs" onserverclick="btnExcel_PorCC_ServerClick">
                                         <i class="fa fa-file-excel-o"></i> Excel
                                        </button>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-6" >
                                    <%--GRILLAS Horas por Centro de Costo--%>
                                    <div class="x_panel" runat="server" visible="false" id="pnlHorasPorProyecto">
                                        <h3>Horas por Proyecto</h3>
                                        <div class="table-responsive">
                                            <asp:GridView ID="grdGrillaProyecto" runat="server" AllowSorting ="false" AutoGenerateColumns="False" CssClass="table table-striped table-bordered" OnRowDataBound="grdGrilla_RowDataBound">
                                            <Columns>
                                                <%-- Pos:0 --%><asp:BoundField DataField="Id" HeaderText="ID" SortExpression="Id" Visible="false" />
                                                <%-- Pos:2 --%><asp:BoundField DataField="Proyecto_Nombre" HeaderText="Proyecto" SortExpression="Proyecto_Nombre"  HeaderStyle-Font-Bold="true" />
                                                <%-- Pos:3 --%><asp:BoundField DataField="_Horas" HeaderText="Horas" SortExpression="Horas"  HeaderStyle-Font-Bold="true"/>
                                                <%-- Pos:3 --%><asp:BoundField DataField="_Porcentaje" HeaderText="%" SortExpression="Porcentaje"  HeaderStyle-Font-Bold="true"/>
                                            </Columns>
                                            <PagerStyle CssClass="pgr" />
                                        </asp:GridView>
                                        </div>
                                        <button type="submit" id="btnExcelProyecto" runat="server" causesvalidation="False" class="btn btn-round btn-info pull-right hidden-xs" onserverclick="btnExcelProyecto_ServerClick">
                                         <i class="fa fa-file-excel-o"></i> Excel
                                        </button>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12" >
                                    <%--GRILLAS Horas por Centro de Costo--%>
                                    <div class="x_panel" runat="server" visible="false" id="pnlHorasPorCCProyecto">
                                        <h3>Horas por CC y Proyecto</h3>
                                        <div class="table-responsive">
                                            <asp:GridView ID="grdGrillaCCProytecto" runat="server" AllowSorting ="false" AutoGenerateColumns="False" CssClass="table table-striped table-bordered" OnRowDataBound="grdGrilla_RowDataBound">
                                                <Columns>
                                                    <%-- Pos:0 --%><asp:BoundField DataField="Id" HeaderText="ID" SortExpression="Id" Visible="false" />
                                                    <%-- Pos:2 --%><asp:BoundField DataField="CC" HeaderText="CC" SortExpression="CC"  HeaderStyle-Font-Bold="true"/>
                                                    <%-- Pos:2 --%><asp:BoundField DataField="Proyecto_Nombre" HeaderText="Proyecto" SortExpression="Proyecto_Nombre"  HeaderStyle-Font-Bold="true"/>
                                                    <%-- Pos:3 --%><asp:BoundField DataField="_Horas" HeaderText="Horas" SortExpression="Horas"  HeaderStyle-Font-Bold="true"/>
                                                </Columns>
                                                <PagerStyle CssClass="pgr" />
                                            </asp:GridView>
                                        </div>
                                        <button type="submit" id="btnExcelCCProyecto" runat="server" causesvalidation="False" class="btn btn-round btn-info pull-right hidden-xs" onserverclick="btnExcelCCProyecto_ServerClick">
                                         <i class="fa fa-file-excel-o"></i> Excel
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>

