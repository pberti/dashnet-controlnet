﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IOT.ORM;
using IOT.Domain;
using System.Transactions;
using System.Web.Security;
using System.Data;
using IOT.Services;
using System.Text;
using System.IO;
using System.Web.UI.HtmlControls;

public partial class Application_IndicadoresHoras_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            InicializacionPantalla();
        }
    }

    private void InicializacionPantalla()
    {
        //CargoGrilla();
        txtFecDesde.Text = "01/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString();
        txtFecHasta.Text = DateTime.Now.ToShortDateString();
    }

    #region Eventos del Filtro
    

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        // Verifica que campos del filtro contienen datos para realizar el filtro.
        try
        {

            this.CargoGrilla();

        }
        catch (Exception ex)
        {
            //errorMensaje.ErrorBind(ex, "btnBuscar_Click", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    #endregion Eventos del Filtro

    #region "Eventos Grilla"
    protected void grdGrilla_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (DataBinder.Eval(e.Row.DataItem, "CC") != null)
                { 
                    string _Texto = DataBinder.Eval(e.Row.DataItem, "CC").ToString();

                    if (_Texto == "Total General")
                        e.Row.BackColor = System.Drawing.Color.LightGreen;
                    else
                    {
                        if (_Texto.Contains("Total"))
                        {
                            e.Row.BackColor = System.Drawing.Color.LightYellow;
                        }
                    }
                }
            }
        }
        catch (Exception Ex)
        {

            throw;
        }
        

    }

    private void CargoGrilla()
    {
        
        grdGrilla.Visible = false;
        grdGrillaCC.Visible = false;
        grdGrillaProyecto.Visible = false;
        grdGrillaCCProytecto.Visible = false;

        DateTime mFechaDesde = Convert.ToDateTime(txtFecDesde.Text.ToString());
        DateTime mFechaHasta = Convert.ToDateTime(txtFecHasta.Text.ToString());

        // 1-Cargo LA GRILLA CON LOS INDICADORES DE HORAS POR RECURSOS
        List<RedmineHoras> oLstHorasEmpleados = Services.Get<ServRedmineHoras>().HorasPorEmpleadoCC(mFechaDesde, mFechaHasta);
        if (oLstHorasEmpleados.Count > 0)
        {
            grdGrilla.Visible = true;
            grdGrilla.DataSource = oLstHorasEmpleados;
            grdGrilla.DataBind();
        }
        // 2-CARGO LA GRILLA CON LOS INDICADORES DE HORAS POR CENTRO DE COSTO
        List<RedmineHoras> oLstHorasCC = Services.Get<ServRedmineHoras>().HorasPorCentroCosto(mFechaDesde, mFechaHasta);
        if (oLstHorasCC.Count > 0)
        {
            grdGrillaCC.Visible = true;
            grdGrillaCC.DataSource = oLstHorasCC;
            grdGrillaCC.DataBind();
        }

        // 3-CARGO LA GRILLA CON LOS INDICADORES DE HORAS POR CENTRO DE COSTO
        List<RedmineHoras> oLstHorasProyecto = Services.Get<ServRedmineHoras>().HorasPorProyecto(mFechaDesde, mFechaHasta);
        if (oLstHorasCC.Count > 0)
        {
            grdGrillaProyecto.Visible = true;
            grdGrillaProyecto.DataSource = oLstHorasProyecto;
            grdGrillaProyecto.DataBind();
        }
        // 4-CARGO LA GRILLA CON LOS INDICADORES DE HORAS POR CENTRO DE COSTO Y POR PROYECTO
        List<RedmineHoras> oLstHorasCCProyecto = Services.Get<ServRedmineHoras>().HorasPorCCProyecto(mFechaDesde, mFechaHasta);
        if (oLstHorasCCProyecto.Count > 0)
        {
            grdGrillaCCProytecto.Visible = true;
            grdGrillaCCProytecto.DataSource = oLstHorasCCProyecto;
            grdGrillaCCProytecto.DataBind();
        }

        pnlDetalleGrilla.Visible = true;
        pnlHorasPorCC.Visible = true;
        pnlHorasPorProyecto.Visible = true;
        pnlHorasPorCCProyecto.Visible = true;

    }
    #endregion

    #region BtnActualizar
    protected void btnActualizar_Click(object sender, EventArgs e)
    {
        // Verifica que campos del filtro contienen datos para realizar el filtro.
        try
        {
            Services.Get<ServOTs>().ActualizacionDatosRedmine();
        }
        catch (Exception)
        {

            throw;
        }
    }
    #endregion

    #region Exportacion a Excel

    protected void btnExcel_Recursos_ServerClick(object sender, EventArgs e)
    {
        // 1-Cargo LA GRILLA CON LOS INDICADORES DE HORAS POR RECURSOS
        DateTime mFechaDesde = Convert.ToDateTime(txtFecDesde.Text.ToString());
        DateTime mFechaHasta = Convert.ToDateTime(txtFecHasta.Text.ToString());
        List<RedmineHoras> oLstHorasEmpleados = Services.Get<ServRedmineHoras>().HorasPorEmpleadoCC(mFechaDesde, mFechaHasta);

        List<Object> mLista = new List<object>();
        foreach (var item in oLstHorasEmpleados)
        {
            Object mLinea = new
            {
                Apellido = item.lastname,
                Nombre = item.firstname,
                INP = item.INP.ToString("##0.00"),
                IPI = item.IPI.ToString("##0.00"),
                PHI = item.PHI.ToString("##0.00"),
                PHP = item.PHP.ToString("##0.00"),
                SOP = item.SOP.ToString("##0.00"),
                SSCC = item.SSCC.ToString("##0.00"),
                STP = item.STP.ToString("##0.00"),
                Total_General = item.CCTOTAL.ToString("##0.00"),
            };
            mLista.Add(mLinea);
        }
        

        // 3-Armo el Excel
        StringBuilder sb = new StringBuilder();
        StringWriter sw = new StringWriter(sb);
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        Page page = new Page();
        HtmlForm form = new HtmlForm();

        GridView grilla = new GridView();
        grilla.EnableViewState = false;
        grilla.AllowPaging = false;
        grilla.DataSource = mLista;
        grilla.DataBind();

        page.EnableEventValidation = false;
        page.DesignerInitialize();
        page.Controls.Add(form);
        form.Controls.Add(grilla);
        page.RenderControl(htw);
        Response.Clear();
        Response.Buffer = true;
        Response.ContentType = "application/ms-excel";
        Response.AddHeader("Content-Disposition", "attachment;filename=HorasPorRecurso.xls");
        Response.Charset = "UTF-8";
        Response.ContentEncoding = Encoding.Default;
        Response.Write(sb.ToString());
        Response.End();
    }

    protected void btnExcel_PorCC_ServerClick(object sender, EventArgs e)
    {
        // 1-Cargo LA GRILLA CON LOS INDICADORES DE HORAS POR RECURSOS
        DateTime mFechaDesde = Convert.ToDateTime(txtFecDesde.Text.ToString());
        DateTime mFechaHasta = Convert.ToDateTime(txtFecHasta.Text.ToString());
        List<RedmineHoras> oLstHorasCC = Services.Get<ServRedmineHoras>().HorasPorCentroCosto(mFechaDesde, mFechaHasta);

        List<Object> mLista = new List<object>();
        foreach (var item in oLstHorasCC)
        {
            Object mLinea = new
            {
                Centro_Costo = item.CC,
                Horas = item._Horas,
                Porcentaje = item._Porcentaje
            };
            mLista.Add(mLinea);
        }


        // 3-Armo el Excel
        StringBuilder sb = new StringBuilder();
        StringWriter sw = new StringWriter(sb);
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        Page page = new Page();
        HtmlForm form = new HtmlForm();

        GridView grilla = new GridView();
        grilla.EnableViewState = false;
        grilla.AllowPaging = false;
        grilla.DataSource = mLista;
        grilla.DataBind();

        page.EnableEventValidation = false;
        page.DesignerInitialize();
        page.Controls.Add(form);
        form.Controls.Add(grilla);
        page.RenderControl(htw);
        Response.Clear();
        Response.Buffer = true;
        Response.ContentType = "application/ms-excel";
        Response.AddHeader("Content-Disposition", "attachment;filename=HorasPorCC.xls");
        Response.Charset = "UTF-8";
        Response.ContentEncoding = Encoding.Default;
        Response.Write(sb.ToString());
        Response.End();
    }

    protected void btnExcelProyecto_ServerClick(object sender, EventArgs e)
    {
        // 1-Cargo LA GRILLA CON LOS INDICADORES DE HORAS POR RECURSOS
        DateTime mFechaDesde = Convert.ToDateTime(txtFecDesde.Text.ToString());
        DateTime mFechaHasta = Convert.ToDateTime(txtFecHasta.Text.ToString());
        List<RedmineHoras> oLstHorasProyecto = Services.Get<ServRedmineHoras>().HorasPorProyecto(mFechaDesde, mFechaHasta);

        List<Object> mLista = new List<object>();
        foreach (var item in oLstHorasProyecto)
        {
            Object mLinea = new
            {
                Proyecto = item.Proyecto_Nombre,
                Horas = item._Horas,
                Porcentaje = item._Porcentaje
            };
            mLista.Add(mLinea);
        }


        // 3-Armo el Excel
        StringBuilder sb = new StringBuilder();
        StringWriter sw = new StringWriter(sb);
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        Page page = new Page();
        HtmlForm form = new HtmlForm();

        GridView grilla = new GridView();
        grilla.EnableViewState = false;
        grilla.AllowPaging = false;
        grilla.DataSource = mLista;
        grilla.DataBind();

        page.EnableEventValidation = false;
        page.DesignerInitialize();
        page.Controls.Add(form);
        form.Controls.Add(grilla);
        page.RenderControl(htw);
        Response.Clear();
        Response.Buffer = true;
        Response.ContentType = "application/ms-excel";
        Response.AddHeader("Content-Disposition", "attachment;filename=HorasPorProyecto.xls");
        Response.Charset = "UTF-8";
        Response.ContentEncoding = Encoding.Default;
        Response.Write(sb.ToString());
        Response.End();
    }

    protected void btnExcelCCProyecto_ServerClick(object sender, EventArgs e)
    {
        // 1-Cargo LA GRILLA CON LOS INDICADORES DE HORAS POR RECURSOS
        DateTime mFechaDesde = Convert.ToDateTime(txtFecDesde.Text.ToString());
        DateTime mFechaHasta = Convert.ToDateTime(txtFecHasta.Text.ToString());
        List<RedmineHoras> oLstHorasCCProyecto = Services.Get<ServRedmineHoras>().HorasPorCCProyecto(mFechaDesde, mFechaHasta);

        List<Object> mLista = new List<object>();
        foreach (var item in oLstHorasCCProyecto)
        {
            Object mLinea = new
            {
                Centro_Costo = item.CC,
                Proyecto = item.Proyecto_Nombre,
                Horas = item._Horas
            };
            mLista.Add(mLinea);
        }


        // 3-Armo el Excel
        StringBuilder sb = new StringBuilder();
        StringWriter sw = new StringWriter(sb);
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        Page page = new Page();
        HtmlForm form = new HtmlForm();

        GridView grilla = new GridView();
        grilla.EnableViewState = false;
        grilla.AllowPaging = false;
        grilla.DataSource = mLista;
        grilla.DataBind();

        page.EnableEventValidation = false;
        page.DesignerInitialize();
        page.Controls.Add(form);
        form.Controls.Add(grilla);
        page.RenderControl(htw);
        Response.Clear();
        Response.Buffer = true;
        Response.ContentType = "application/ms-excel";
        Response.AddHeader("Content-Disposition", "attachment;filename=HorasPorCCProyecto.xls");
        Response.Charset = "UTF-8";
        Response.ContentEncoding = Encoding.Default;
        Response.Write(sb.ToString());
        Response.End();
    }
    #endregion






}