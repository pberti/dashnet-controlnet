﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Application_Usuarios_Pass_Default" %>

<%@ Register TagName="usrUltimaModificacion" TagPrefix="uc1" Src="~/UserControl/usrUltimaModificacion.ascx"  %>
<%@ Register TagName="Error" TagPrefix="usrError" Src="~/UserControl/Error.ascx" %>
<%@ Register TagName="userProcesando" TagPrefix="uc1" Src="~/UserControl/userProcesando.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <%--Vista Detalles--%>
    <asp:Panel ID="pnlDetalles" runat="server">
        <div class="col-md-5 col-sm-12 col-xs-12 center-margin">
            <div class="x_panel">
                <div class="x_title">
                    <h2><asp:Literal ID="litNombre" runat="server" Text='Cambiar Contraseña' /></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <div class="form-horizontal form-label-left">
                        <div class="form-group">
                            <asp:Label runat="server" Text="Actual" class="control-label col-md-5 col-sm-5 col-xs-12"></asp:Label>
                            <div class="col-md-5 col-sm-5 col-xs-12">
                                <asp:TextBox ID="txtPassOld" TextMode="Password" runat="server" class="form-control">
                                </asp:TextBox>
                                <asp:Label Text="" runat="server" ID="lvlValPassword" CssClass="help-inline" />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" Text="Nueva" class="control-label col-md-5 col-sm-5 col-xs-12"></asp:Label>
                            <div class="col-md-5 col-sm-5 col-xs-12">
                                <asp:TextBox ID="txtPassNew" TextMode="Password" runat="server" class="form-control">
                                </asp:TextBox>
                                <asp:Label Text="" runat="server" ID="lblValNewPassword" CssClass="help-inline" />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" Text="Repetir Nueva" class="control-label col-md-5 col-sm-5 col-xs-12"></asp:Label>
                            <div class="col-md-5 col-sm-5 col-xs-12">
                                <asp:TextBox ID="txtPassNewRpt" TextMode="Password" runat="server" class="form-control">
                                </asp:TextBox>
                                <asp:Label Text="" runat="server" ID="lblValNewPasswordRep" CssClass="help-inline" />
                            </div>
                        </div>
                        
                        <br />
                        <div class="">
                            <div class="col-md-6 col-sm-6 col-xs-7">
                                <button type="submit" id="btnCambiar" runat="server" causesvalidation="False" class="btn btn-round btn-primary pull-right" onserverclick="btnCambiar_Click">
                                <i class="fa fa-check"></i> Grabar
                            </button>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-5">
                                <button type="submit" id="btnSalir" runat="server" causesvalidation="False" class="btn btn-round btn-alert" onserverclick="btnVolver_Click">
                                    <i class="fa fa-arrow-circle-left"></i> Volver
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>

    <usrError:Error ID="errorMensaje" runat="server" />
    <uc1:userProcesando runat="server" ID="userProcesando" />
</asp:Content>
