﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using IOT.Domain;
using IOT.Services;


public partial class Application_Usuarios_Pass_Default : PageBase
{

    #region Variables

    protected string PassOld
    {
        get { return Convert.ToString(Session["_PassOld"]); }
        set { Session["_PassOld"] = value; }
    }
    protected string PassNew
    {
        get { return Convert.ToString(Session["_PassNew"]); }
        set { Session["_PassNew"] = value; }
    }
    protected string PassNewRpt
    {
        get { return Convert.ToString(Session["_PassNewRpt"]); }
        set { Session["_PassNewRpt"] = value; }
    }

    #endregion

    #region Main

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    #endregion

    #region Metodos

    private bool TexBoxCompletos()
    {
        this.PassOld = txtPassOld.Text;
        this.PassNew = txtPassNew.Text;
        this.PassNewRpt = txtPassNewRpt.Text;

        bool completos = true;

        //TODO: revisar uso
        //cgPassOld.Attributes.Remove("class");
        //cgPassOld.Attributes.Add("class", "control-group");
        //cgPassNew.Attributes.Remove("class");
        //cgPassNew.Attributes.Add("class", "control-group");
        //cgPassNewRpt.Attributes.Remove("class");
        //cgPassNewRpt.Attributes.Add("class", "control-group");

        //if (txtPassOld.Text == string.Empty)
        //{
        //    cgPassOld.Attributes.Remove("class");
        //    cgPassOld.Attributes.Add("class", "control-group error");
        //    completos = false;
        //}

        //if (txtPassNew.Text == string.Empty)
        //{
        //    cgPassNew.Attributes.Remove("class");
        //    cgPassNew.Attributes.Add("class", "control-group error");
        //    completos = false;
        //}

        //if (txtPassNewRpt.Text == string.Empty)
        //{
        //    cgPassNewRpt.Attributes.Remove("class");
        //    cgPassNewRpt.Attributes.Add("class", "control-group error");
        //    completos = false;
        //}


        return completos;
    }

    private void CambiarPass()
    {
        string a = this.txtPassNewRpt.Text;
        if (txtPassNew.Text == txtPassNewRpt.Text)
        {
            Usuario oUsrLogueado = base.TraerUsuarioLogeado();
            if (Membership.ValidateUser(oUsrLogueado.UserName, txtPassOld.Text))
            {
                
                oUsrLogueado.Password = Encriptacion.Encriptar(txtPassNew.Text);
                Services.Get<ServUsuario>().ActualizarUsuario(oUsrLogueado);
               
                Redirect("~/Application/Usuarios/MisDatos/Default.aspx", false);
            }
            else
            {
                txtPassOld.Text = string.Empty;
                txtPassNew.Text = string.Empty;
                txtPassNewRpt.Text = string.Empty;
                //TODO: revisar uso
                //usrMensajeUsuario.CargarMensaje("Contraseña actual inválida");
                //usrMensajeUsuario.MensajeBind();
            }
        }
        else
        {
            txtPassNew.Text = string.Empty;
            txtPassNewRpt.Text = string.Empty;

            //TODO: revisar uso
            //usrMensajeUsuario.CargarMensaje("Las contraseñas nuevas no coinciden");
            //usrMensajeUsuario.MensajeBind();
        }
 
    }
    
    #endregion

    #region Eventos Botones

    protected void btnCambiar_Click(object sender, EventArgs e)
    {
        if (TexBoxCompletos())
        {
            CambiarPass();
        }
        else
        {
            //TODO: revisar uso
            //usrMensajeUsuario.CargarMensaje("Hay campos incompletos");
            //usrMensajeUsuario.MensajeBind();

            txtPassOld.Text = this.PassOld;
            txtPassNew.Text = this.PassNew;
            txtPassNewRpt.Text = this.PassNewRpt;
        }
    }

    protected void btnVolver_Click(object sender, EventArgs e)
    {
        Redirect("~/Application/Usuarios/MisDatos/Default.aspx", false);
    }

    #endregion

}