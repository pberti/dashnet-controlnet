﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IOT.ORM;
using IOT.Domain;
using System.Transactions;
using System.Web.Security;
using System.Data;
using IOT.Services;
using System.Web.UI.HtmlControls;

public partial class Application_Usuarios_MisDatos_Default : PageBase
{

    #region Propiedades

    public static string BaseSiteUrl
    {
        get
        {
            HttpContext context = HttpContext.Current;
            string baseUrl = context.Request.Url.Scheme + "://" + context.Request.Url.Authority + context.Request.ApplicationPath.TrimEnd('/');
            return baseUrl;
        }
    }

    #endregion

    #region Main

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CargarMisDatos();
        }
    }

    #endregion

    #region Metodos

    private void CargarMisDatos()
    {
        ServUsuario oServ = Services.Get<ServUsuario>();
        Usuario oUsuario;

        try
        {
            oUsuario = oServ.TraerUnicoUsuario(base.IdUsuarioLogueado);
            lblId.Text = oUsuario.Id.ToString();
            txtApellido.Text = oUsuario.Apellido.ToUpper();
            txtNombre.Text = oUsuario.Nombre.ToUpper();
            txtEmail.Text = oUsuario.Email.ToLower();
            CargarUserControls(oUsuario.IdUsuarioModifico, oUsuario.FecUltimaModificacion);
            String path = Server.MapPath("~/Files/");
            imgImagenUsuario.ImageUrl = path + oUsuario.URLImagenUsuario;
        }
        catch (SystemException ex)
        {
            errorMensaje.ErrorBind(ex, "CargarMisDatos", HttpContext.Current.Request.Url.LocalPath);
        }
        finally
        {
            oServ = null;
            oUsuario = null;
        }
    }

    private void CargarUserControls(long pIdUsuarioModifico, DateTime pFechaUltimaModificacion)
    {
        usrUltimaModificacion1.FecUltimaModificacion = pFechaUltimaModificacion.ToShortDateString();
        usrUltimaModificacion1.IdUsuarioModifico = pIdUsuarioModifico;
        usrUltimaModificacion1.UltimaModificacionBind();
    }

    private void GrabarDatos()
    {
        ServUsuario oServ = Services.Get<ServUsuario>();

        try
        {
            var oUsuario = ObtenerDatos();

            if (/*base.ValidateModel(oUsuario, ref frmDetalles*/true)
            {
                SubirImagenDeUsuario();
                oServ.ActualizarUsuario(oUsuario);
                Redirect("~/Default.aspx", false);
            }
        }
        catch (SystemException ex)
        {
            errorMensaje.ErrorBind(ex, "GrabarDatos", HttpContext.Current.Request.Url.LocalPath);
        }
        finally
        {
            oServ = null;
        }
    }

    private Usuario ObtenerDatos()
    {
        ServUsuario oServ = Services.Get<ServUsuario>();
        Usuario oUsuario = new Usuario();

        try
        {

            oUsuario = oServ.TraerUnicoUsuario(base.IdUsuarioLogueado);

            oUsuario.Apellido = txtApellido.Text;
            oUsuario.Nombre = txtNombre.Text;
            oUsuario.Email = txtEmail.Text;
            oUsuario.FecUltimaModificacion = DateTime.Now;
            oUsuario.IdUsuarioModifico = base.IdUsuarioLogueado;
            return oUsuario;

        }
        catch (SystemException ex)
        {
            errorMensaje.ErrorBind(ex, "ObtenerDatos", HttpContext.Current.Request.Url.LocalPath);
        }
        finally
        {
            oServ = null;
            oUsuario = null;
        }

        return oUsuario;

    }

    public bool ArchivoSelecionado()
    {
        if (fuArchivo.PostedFile != null)
        {
            if (!string.IsNullOrWhiteSpace(fuArchivo.PostedFile.FileName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public void SubirImagenDeUsuario()
    {
        try
        {
            if (ArchivoSelecionado())
            {
                string directoryName = "Files";
                if (string.IsNullOrEmpty(directoryName))
                {
                    throw new Exception("Falta especificar la carpeta del tipo de archivo donde se va a guardar el archivo");
                }
                if ((fuArchivo.PostedFile.FileName != null) && (fuArchivo.PostedFile.ContentLength > 0))
                {
                    string fn = System.IO.Path.GetFileName(fuArchivo.PostedFile.FileName);
                    Guid newGuid = Guid.NewGuid();

                    String path = Server.MapPath("~/Files/");
                    string SaveLocation = path + newGuid;
                    fuArchivo.PostedFile.SaveAs(SaveLocation);
                    Response.Write("El archivo se ha cargado.");
                }
            }
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "btnSubir_Click", HttpContext.Current.Request.Url.LocalPath);
        }
    }
    #endregion

    #region Eventos Botones

    protected void btnGrabar_Click(object sender, EventArgs e)
    {
        Page.Validate();
        if (Page.IsValid)
        {
            GrabarDatos();
        }
    }

    protected void btnSalir_Click(object sender, EventArgs e)
    {
        Redirect("~/Default.aspx", false);
    }

    protected void btnCambiarPassword_Click(object sender, EventArgs e)
    {
        Redirect("~/Application/Usuarios/Pass/Default.aspx", false);
    }
    
    #endregion

}