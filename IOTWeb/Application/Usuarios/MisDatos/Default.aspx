﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Application_Usuarios_MisDatos_Default" %>

<%@ Register TagName="usrUltimaModificacion" TagPrefix="uc1" Src="~/UserControl/usrUltimaModificacion.ascx" %>
<%@ Register TagName="Error" TagPrefix="usrError" Src="~/UserControl/Error.ascx" %>
<%@ Register TagName="userProcesando" TagPrefix="uc1" Src="~/UserControl/userProcesando.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript">
        function previewImage(input) {
            var preview = document.getElementById('<%= imgImagenUsuario.ClientID%>');
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    preview.setAttribute('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <%--Vista Detalles--%>
    <asp:Panel ID="pnlDetalles" runat="server">
        <div class="col-md-9 col-sm-10 col-xs-12 center-margin">
            <div class="x_panel">
                <div class="x_title">
                    <h2>
                        <asp:Literal ID="litNombre" runat="server" Text='Mis Datos' /></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <div class="form-horizontal form-label-left">
                        <div class="form-group">
                            <asp:Label ID="lblIdTitu" Text="ID" runat="server" Visible="true" class="control-label col-md-3 col-sm-3 col-xs-12">Id:</asp:Label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <asp:Label ID="lblId" runat="server" Visible="true" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" Text="" class="control-label col-md-3 col-sm-3 col-xs-12"></asp:Label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <asp:Image runat="server" ID="imgImagenUsuario" Height="64" Width="64"/>
                            </div>
                            <asp:Label ID="lblImagenUsuario" Text="Imagen de Usuario" runat="server" Visible="true" class="control-label col-md-3 col-sm-3 col-xs-12"></asp:Label>
                            <asp:FileUpload runat="server" ID="fuArchivo" class="btn btn-upload col-md-7 col-sm-7 col-xs-12" onchange="previewImage(this)"/>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" Text="Nombre" class="control-label col-md-3 col-sm-3 col-xs-12"></asp:Label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <asp:TextBox ID="txtNombre" runat="server" Placeholder="Nombre real del Usuario" class="form-control">
                                </asp:TextBox>
                                <asp:Label Text="" runat="server" ID="lblValNombre" CssClass="help-inline" />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" Text="Apellido" class="control-label col-md-3 col-sm-3 col-xs-12"></asp:Label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <asp:TextBox ID="txtApellido" runat="server" Placeholder="Apellido del Usuario" class="form-control">
                                </asp:TextBox>
                                <asp:Label Text="" runat="server" ID="lblValApellido" CssClass="help-inline" />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" Text="Email" class="control-label col-md-3 col-sm-3 col-xs-12"></asp:Label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <asp:TextBox ID="txtEmail" runat="server" Placeholder="Email del Usuario" class="form-control">
                                </asp:TextBox>
                                <asp:Label Text="" runat="server" ID="lblValEmail" CssClass="help-inline" />
                            </div>
                        </div>

                        <uc1:usrUltimaModificacion ID="usrUltimaModificacion1" runat="server" />
                        <div class="pull-right">
                            <button type="submit" id="btnGrabar" runat="server" causesvalidation="False" class="btn btn-round btn-primary" onserverclick="btnGrabar_Click">
                                <i class="fa fa-check"></i> Grabar
                            </button>
                            <button type="submit" id="btnCambiarPassword" runat="server" causesvalidation="False" class="btn btn-round btn-primary" onserverclick="btnCambiarPassword_Click">
                                <i class="fa fa-pencil"></i> Cambiar Contraseña
                            </button>
                            <button type="submit" id="btnSalir" runat="server" causesvalidation="False" class="btn btn-round btn-alert" onserverclick="btnSalir_Click">
                                <i class="fa fa-arrow-circle-left"></i> Volver
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>

    <usrError:Error ID="errorMensaje" runat="server" />
    <uc1:userProcesando runat="server" ID="userProcesando" />
</asp:Content>

