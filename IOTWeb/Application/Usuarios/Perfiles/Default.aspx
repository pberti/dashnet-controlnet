﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Application_Usuarios_Perfiles_Default" %>

<%@ Register TagName="usrUltimaModificacion" TagPrefix="uc1" Src="~/UserControl/usrUltimaModificacion.ascx"  %>
<%@ Register TagName="Error" TagPrefix="usrError" Src="~/UserControl/Error.ascx" %>
<%@ Register TagName="userProcesando" TagPrefix="uc1" Src="~/UserControl/userProcesando.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <%--Vista Grilla--%>
    <asp:Panel ID="pnlGrilla" runat="server">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                </div>
                <div class="title_right">
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Administracion de Perfiles</h2>
                            <div class="pull-right">
                                    <button type="submit" id="btnAgregar" runat="server" causesvalidation="False" class="btn btn-round btn-primary" onserverclick="btnAgregar_Click">
                                        <i class="fa fa-plus-circle"></i> Agregar Nuevo
                                    </button>
                                </div>
                            <div class="clearfix"></div>
                        </div>
                        <%--FILTRO--%>
                        <div class="x_content">
                            <div class="x_panel">
                                <div class="form-horizontal form-label-left pull-left">
                                    <asp:Label ID="lblFiltroPerfil" runat="server" Text="Perfil" class="control-label col-md-1 col-sm-1 col-xs-12"></asp:Label>
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        <asp:TextBox ID="txtFiltroPerfil" runat="server" class="form-control">
                                        </asp:TextBox>
                                    </div>
                                    <asp:Label ID="lblFiltroDescripcion" runat="server" Text="Descripcion" class="control-label col-md-2 col-sm-1 col-xs-12"></asp:Label>
                                    <div class="col-md-5 col-sm-3 col-xs-12">
                                        <asp:TextBox ID="txtFiltroDescripcion" runat="server" class="form-control">
                                        </asp:TextBox>
                                    </div>                                     
                                </div> 
                                <button type="submit" id="btnBuscar" runat="server" causesvalidation="False" class="btn btn-round btn-info pull-right" onserverclick="btnFiltro_Click">
                                    <i class="fa fa-search"></i> Buscar
                                </button>

                            </div>
                            <%--GRILLA--%>
                            <asp:GridView ID="grdGrilla" runat="server" OnPageIndexChanging="grdGrilla_PageIndexChanging"
                                OnSorting="grdGrilla_Sorting" OnRowCommand="grdGrilla_RowCommand"  AllowSorting ="true" 
                                AutoGenerateColumns="False" CssClass="table table-striped table-bordered">
                                <Columns>
                                    <asp:BoundField DataField="Id" HeaderText="ID" SortExpression="Id" Visible="True" />
                                    <asp:BoundField DataField="Perfil" HeaderText="Perfil" SortExpression="Perfil" />
                                    <asp:BoundField DataField="Descripcion" HeaderText="Descripcion" SortExpression="Descripcion" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <div class="pull-right">
                                                <asp:LinkButton ID="btnEditar" runat="server" CommandName="Editar" CommandArgument='<%#Eval("Id") %>' 
                                                    CssClass="btn btn-round"> <i class="fa fa-edit"></i> Editar</asp:LinkButton>
                                            </div>
                                        </ItemTemplate>
                                        <ItemStyle Width="10%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <div class="pull-right">
                                                <asp:LinkButton ID="btnMostrarMenues" runat="server" CommandName="MostrarMenues" CommandArgument='<%#Eval("Id") %>' 
                                                    CssClass="btn btn-round"> <i class="fa fa-edit"></i> Menues</asp:LinkButton>
                                            </div>
                                        </ItemTemplate>
                                        <ItemStyle Width="10%" />
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle CssClass="pgr" />
                            </asp:GridView>
                            <br />
                            <div class="alert alert-success alert-dismissible fade in" role="alert">
                                <div class="pull-left">
                                    Total de Registros:
                                    <asp:Label ID="lblCantidadFilas" runat="server"></asp:Label>
                                </div>
                                
                                <br /> 
                                <br /> 
                            </div>  
                        </div>
                    </div>
            </div>
            </div>
        </div>
    </asp:Panel>

    <%--Vista Detalles--%>
    <asp:Panel ID="pnlDetalles" runat="server">
        <div class="col-md-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><asp:Literal ID="litNombre" runat="server" Text='<%#TituloForm%>' /></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <div class="form-horizontal form-label-left">
                        <div class="form-group">
                            <asp:Label ID="lblIdTitu" Text="ID" runat="server" Visible="true" class="control-label col-md-3 col-sm-3 col-xs-12">Id:</asp:Label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <asp:Label ID="lblId" runat="server" Visible="true" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" Text="Perfil" class="control-label col-md-3 col-sm-3 col-xs-12"></asp:Label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <asp:TextBox ID="txtPerfil" runat="server" Placeholder="Nombre o Designacion del Perfil" class="form-control">
                                </asp:TextBox>
                                <asp:Label Text="" runat="server" ID="lvlValPerfil" CssClass="help-inline" />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" Text="Descripcion" class="control-label col-md-3 col-sm-3 col-xs-12"></asp:Label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <asp:TextBox ID="txtDescripcion" runat="server" Placeholder="Descripcion del Perfil" class="form-control">
                                </asp:TextBox>
                                <asp:Label Text="" runat="server" ID="lblValDescripcion" CssClass="help-inline" />
                            </div>
                        </div>
                        
                        <uc1:usrUltimaModificacion ID="usrUltimaModificacion1" runat="server" />

                        <div class="pull-right">
                            <button type="submit" id="btnGrabar" runat="server" causesvalidation="False" class="btn btn-round btn-primary" onserverclick="btnGrabar_Click">
                                <i class="fa fa-check"></i> Grabar
                            </button>
                            <button type="submit" id="btnSalir" runat="server" causesvalidation="False" class="btn btn-round btn-alert" onserverclick="btnSalir_Click">
                                <i class="fa fa-arrow-circle-left"></i> Volver
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>

    <%--Vista Grilla Menues--%>
    <asp:Panel ID="pnlListaMenues" runat="server">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                </div>
                <div class="title_right">
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2><asp:Literal ID="litMenuPorPerfil" runat="server" Text="" /></h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <%--GRILLA--%>
                            <asp:GridView ID="grdGrillaMenues" runat="server" OnPageIndexChanging="grdGrillaMenues_PageIndexChanging"
                                OnSorting="grdGrillaMenues_Sorting" OnRowCommand="grdGrillaMenues_RowCommand"  AllowSorting ="true" 
                                AutoGenerateColumns="False" CssClass="table table-striped table-bordered">
                                <Columns>
                                    <asp:BoundField DataField="Id" HeaderText="ID" SortExpression="Id" Visible="True" />
                                    <asp:BoundField DataField="Nombre" HeaderText="Menu" SortExpression="Nombre" />
                                    <asp:BoundField DataField="Posicion" HeaderText="Árbol Posición" SortExpression="Posocion" />
                                    <asp:TemplateField HeaderText="Mostrar">
                                        <ItemTemplate>
                                            <div class="center">
                                                <asp:CheckBox ID="chbxSeleccionado" Checked='<%#Eval("Seleccionado") %>' runat="server"/>
                                                <asp:Label ID="lblSeleccionado" Text='<%#Eval("Id") %>' runat="server" Visible="false" />
                                            </div>
                                        </ItemTemplate>
                                        <ItemStyle Width="10%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Puede Grabar">
                                       <ItemTemplate>
                                            <div class="center">
                                                 <asp:CheckBox ID="chbkGrabar" Checked='<%#Eval("PuedeGrabar") %>' runat="server"/>
                                                 
                                            </div>
                                        </ItemTemplate>
                                  </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Puede Borrar">
                                      <ItemTemplate>
                                            <div class="center">
                                                  <asp:CheckBox ID="chbkBorrar" Checked='<%#Eval("PuedeBorrar") %>' runat="server"/>
                                                
                                            </div>
                                        </ItemTemplate>
                                  </asp:TemplateField>
                                </Columns>
                                <PagerStyle CssClass="pgr" />
                            </asp:GridView>
                            <br />
                                <uc1:usrUltimaModificacion ID="usrUltimaModificacion2" runat="server" />

                            <div class="alert alert-success alert-dismissible fade in" role="alert">
                                <div class="pull-left">
                                    Total de Registros:
                                    <asp:Label ID="lblCantMenues" runat="server"></asp:Label> - <asp:Label ID="lblCantSeleccionados" runat="server"></asp:Label> 
                                </div>
                                <div class="pull-right">
                                    <button type="submit" id="btnGrabarMenues" runat="server" causesvalidation="False" class="btn btn-round btn-primary" onserverclick="btnGrabarMenues_Click">
                                        <i class="fa fa-check"></i> Grabar Menues
                                    </button>
                                    <button type="submit" id="btnSalir2" runat="server" causesvalidation="False" class="btn btn-round btn-alert" onserverclick="btnSalir_Click">
                                        <i class="fa fa-arrow-circle-left"></i> Volver
                                    </button>
                                </div>
                                <br /> 
                                <br /> 
                            </div>
                        </div>
                    </div>
            </div>
            </div>
        </div>
    </asp:Panel>

    <usrError:Error ID="errorMensaje" runat="server" />
    <uc1:userProcesando runat="server" ID="userProcesando" />

</asp:Content>