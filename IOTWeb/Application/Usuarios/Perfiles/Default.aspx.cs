﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IOT.ORM;
using IOT.Domain;
using System.Transactions;
using System.Web.Security;
using System.Data;
using IOT.Services;

public partial class Application_Usuarios_Perfiles_Default : PageBase
{
    #region Propiedades & Variables

    private ServUsuario oServUsr = Services.Get<ServUsuario>();
    private ServMenues oServMenues = Services.Get<ServMenues>();
    private ServTipoPerfilMenues oServPerfilMenu = Services.Get<ServTipoPerfilMenues>();

    private List<Menues> oListaMenus;
    private List<TipoPerfilMenues> oListaMenuPermiso;

    #endregion

    #region "Main"

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            this.InicializacionPantalla();
        }
    }

    #endregion

    #region "Métodos"

    /// <summary>
    /// Metodo que iniciliza los valores por defecto de la pagina.
    /// </summary>
    private void InicializacionPantalla()
    {
        CargoPermisosdePantalla();
        DireccionOrdenacion = string.Empty;
        this.CargoGrilla("Perfil descending", this.ObtenerTipoPerfilFiltro());
    }

    private void CargoPermisosdePantalla()
    {
        var tipoPerfilMenu = oServPerfilMenu.TraerTodosTipoPefilMenuesPorPerfil(IdPerfilUsuarioLogueado).Where(p => p.oMenu.PathPagina.Contains("Usuarios/Perfiles"));
        if (tipoPerfilMenu.Count() != 0)
        {
            List<TipoPerfilMenues> t = tipoPerfilMenu.ToList();
            btnAgregar.Visible = t.First().PuedeGrabar;
            btnGrabar.Visible = t.First().PuedeGrabar;
        }
        else
            Response.Redirect("~/Application/Default.aspx", false);

    }

    /// <summary>
    /// Metodo que cargs los datos en la grilla de la pagina.
    /// </summary>
    /// <param name="pOrderBy">Criterio de ordenamiento de las columnas</param>
    /// <param name="pTipoPerfil">Objeto Tipo Perfil para determinar si existe algun filtro a aplicar a la grilla</param>
    private void CargoGrilla(string pOrderBy, TipoPerfil pTipoPerfil)
    {
        this.VisibilidadPaneles(true, false, false);
        CriterioOrdenacion = pOrderBy;
        List<TipoPerfil> wQuery = null;
        try
        {
            if (pTipoPerfil == null)
            {
                wQuery = oServUsr.TraerTodosPerfiles().OrderBy(pOrderBy).ToList();
            }
            else
            {
                wQuery = oServUsr.TraerTodosPerfilesParam(this.txtFiltroPerfil.Text, this.txtFiltroDescripcion.Text).OrderBy(pOrderBy).ToList();
            }

            grdGrilla.DataSource = wQuery;
            grdGrilla.DataBind();
            lblCantidadFilas.Text = wQuery.Count().ToString();
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargoGrilla", HttpContext.Current.Request.Url.LocalPath);
        }
        finally
        {
            wQuery = null;
        }
    }

    /// <summary>
    /// Metodo que obtiene los datos establecidos de los filtros.
    /// </summary>
    /// <returns>Metodo que devuelve un objeto TipoPerfil con los valores del filtro.</returns>
    private TipoPerfil ObtenerTipoPerfilFiltro()
    {
        TipoPerfil oPerfil = null;
        // Verifica que campos del filtro contienen datos para realizar el filtro.
        if (!string.IsNullOrWhiteSpace(this.txtFiltroPerfil.Text.Trim()))
        {
            oPerfil = new TipoPerfil();
            oPerfil.Perfil = this.txtFiltroPerfil.Text.Trim();
        }

        if (!string.IsNullOrWhiteSpace(this.txtFiltroDescripcion.Text.Trim()))
        {
            if (oPerfil == null)
            {
                oPerfil = new TipoPerfil();
            }
            oPerfil.Descripcion = this.txtFiltroDescripcion.Text.Trim();
        }
        return oPerfil;
    }

    /// <summary>
    /// Metodo que determina la visibilidad de los paneles.
    /// </summary>
    private void VisibilidadPaneles(bool pPnlGrilla, bool pPnlDetalle, bool pPnlPantalla)
    {
        this.pnlGrilla.Visible = pPnlGrilla;
        this.pnlDetalles.Visible = pPnlDetalle;
        this.pnlListaMenues.Visible = pPnlPantalla;
    }

    /// <summary>
    /// Metodo que obtiene el ordenamiento de las columnas.
    /// </summary>
    private void ObtenerDireccionOrden(GridViewSortEventArgs evt)
    {
        if (DireccionOrdenacion == "")
        {
            DireccionOrdenacion = "descending";
        }
        else
        {
            if (DireccionOrdenacion == "ascending")
                DireccionOrdenacion = "descending";
            else
                DireccionOrdenacion = "ascending";
        }
    }

    /// <summary>
    /// Metodo que carga los datos de los controles.
    /// </summary>
    private void CargarControles()
    {
        try
        {
            var oPerfil = oServUsr.TraerUnicoTipoPerfil(base.EntityId);
            this.lblId.Text = oPerfil.Id.ToString();
            this.txtPerfil.Text = oPerfil.Perfil.ToUpper();
            this.txtDescripcion.Text = oPerfil.Descripcion;
            this.CargarUserControl(oPerfil.IdUsuarioModifico, oPerfil.FecUltimaModificacion);
        }
        catch (Exception ex)
        {
            this.errorMensaje.ErrorBind(ex, "CargarControles", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    /// <summary>
    ///  Metodo que carga los datos del usercontrol de usrUltimaModificacion
    /// </summary>
    private void CargarUserControl(long pIdUsuarioMofifico, DateTime pFechaUltimaModificacion)
    {
        this.usrUltimaModificacion1.FecUltimaModificacion = pFechaUltimaModificacion.ToShortDateString();
        this.usrUltimaModificacion1.IdUsuarioModifico = pIdUsuarioMofifico;
        this.usrUltimaModificacion1.UltimaModificacionBind();
    }

    /// <summary>
    /// Metodo que establece los datos en la pagina para crear un nuevo objecto TipoPerfil
    /// </summary>
    private void CrearNuevo()
    {
        this.Limpiar();
        litNombre.Text = "Nuevo Tipo de Perfil";
        usrUltimaModificacion1.Visible = false;
        this.VisibilidadPaneles(false, true, false);
        EsNuevo = true;
    }

    /// <summary>
    /// Metodo que guarda los datos establecidos en la pagina.
    /// </summary>
    private void GrabarDatos()
    {
        try
        {
            TipoPerfil oPerfil = ObtenerDatos();

            if (/*base.ValidateModel(oPerfil, ref frmDetalles*/ true)
            {
                if (EsNuevo)
                {
                    EntityId = oServUsr.AgregarTipoPerfil(oPerfil);
                    this.LimpiarFiltros();
                }
                else
                {
                    oServUsr.ActualizarTipoPerfil(oPerfil);
                }
                this.CargoGrilla(base.CriterioOrdenacion, this.ObtenerTipoPerfilFiltro());
            }
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "GrabarDatos", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    /// <summary>
    /// Obtiene los datos de la pagina
    /// </summary>
    /// <returns>Retorna un objecto de TipoPerfil con los datos establecidos en la pagina</returns>
    private TipoPerfil ObtenerDatos()
    {
        TipoPerfil oTipoPerfil = null;
        try
        {
            oTipoPerfil = new TipoPerfil();
            var oUsuario = base.TraerUsuarioLogeado();
            if (!EsNuevo)
            {
                oTipoPerfil = oServUsr.TraerUnicoTipoPerfil(base.EntityId);
            }

            oTipoPerfil.Perfil = this.txtPerfil.Text.ToUpper();
            oTipoPerfil.Descripcion = this.txtDescripcion.Text;
            oTipoPerfil.IdUsuarioModifico = oUsuario.Id;
            oTipoPerfil.FecUltimaModificacion = DateTime.Now;
        }
        catch (Exception ex)
        {
            this.errorMensaje.ErrorBind(ex, "ObtenerDatos", HttpContext.Current.Request.Url.LocalPath);
        }
        return oTipoPerfil;
    }

    /// <summary>
    /// Metodo que limpia los datos establecidos en la pagina.
    /// </summary>
    private void Limpiar()
    {
        this.lblId.Text = string.Empty;
        this.txtPerfil.Text = string.Empty;
        this.txtDescripcion.Text = string.Empty;

        //TO DO: consultar esto ¿que es?
        //chkFacExtraordinaria.Checked = false; //TK5835
        //chkPermiteImpresionCertificado.Checked = false;
    }

    /// <summary>
    /// Metodo que limpia los datos establecidos en la pagina en los filtros.
    /// </summary>
    private void LimpiarFiltros()
    {
        this.txtFiltroPerfil.Text = string.Empty;
        this.txtFiltroPerfil.Text = string.Empty;
    }

    #endregion

    #region "Eventos Botones"

    protected void btnFiltro_Click(object sender, EventArgs e)
    {
        try
        {
            this.CargoGrilla("Perfil ascending", this.ObtenerTipoPerfilFiltro());

        }
        catch (SystemException ex)
        {
            this.errorMensaje.ErrorBind(ex, "btnBuscar_Click", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    protected void btnAgregar_Click(object sender, EventArgs e)
    {
        TituloForm = "Nuevo Tipo de Perfil";
        this.CrearNuevo();
    }

    protected void btnGrabar_Click(object sender, EventArgs e)
    {
        Page.Validate();
        if (Page.IsValid)
        {
            this.GrabarDatos();
        }
    }

    protected void btnSalir_Click(object sender, EventArgs e)
    {
        this.VisibilidadPaneles(true, false, false);
    }

    #endregion

    #region "Eventos Grilla"

    protected void grdGrilla_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdGrilla.PageIndex = e.NewPageIndex;
        this.CargoGrilla(CriterioOrdenacion, this.ObtenerTipoPerfilFiltro());
    }

    protected void grdGrilla_Sorting(object sender, GridViewSortEventArgs e)
    {
        this.ObtenerDireccionOrden(e);
        this.CargoGrilla(string.Format("{0} {1}", e.SortExpression, DireccionOrdenacion), this.ObtenerTipoPerfilFiltro());
    }

    protected void grdGrilla_RowCommand(Object sender, CommandEventArgs e)
    {
        if (e.CommandName == "Editar")
        {
            base.EntityId = Convert.ToInt64(e.CommandArgument);
            base.EsNuevo = false;
            litNombre.Text = "Editar Tipo de Perfil";
            this.CargarControles();
            usrUltimaModificacion1.Visible = true;
            this.VisibilidadPaneles(false, true, false);
        }
        if (e.CommandName == "MostrarMenues")
        {
            base.EntityId = Convert.ToInt64(e.CommandArgument);
            this.CargoGrillaMenues("Posicion ascending");
            VisibilidadPaneles(false, false, true);
        }

    }

    #endregion

    #region Menúes por Perfil

    #region Métodos

    /// <summary>
    /// Metodo que carga los datos en la grilla de Menues.
    /// </summary>
    private void CargoGrillaMenues(string pOrderBy)
    {
        CriterioOrdenacion = pOrderBy;
        List<Menues> wQuery = null;
        List<Menues> lstMenuConPermiso = null;
        var perfil = Services.Get<ServUsuario>().TraerUnicoTipoPerfil(base.EntityId);
        litMenuPorPerfil.Text = string.Format("{0} - {1}", "Menúes por Perfil", perfil.Perfil);
        try
        {
            wQuery = oServMenues.TraerTodosMenuesHojas().OrderBy(pOrderBy).ToList();
            oListaMenus = oServMenues.TraerTodosMenues().OrderBy("Posicion ascending").ToList();
            lstMenuConPermiso = VerificarPermisos(wQuery);

            grdGrillaMenues.DataSource = lstMenuConPermiso;
            lblCantMenues.Text = lstMenuConPermiso.Count().ToString();
            grdGrillaMenues.DataBind();
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargoGrillaMenues", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    /// <summary>
    /// Metodo que verifica los permisos establecidos para los menus..
    /// </summary>    
    private List<Menues> VerificarPermisos(List<Menues> pListMenu)
    {
        List<TipoPerfilMenues> lst_PerfilMenu = null;
        int cantSel = 0;
        bool hayDatos = false;
        try
        {
            lst_PerfilMenu = oServPerfilMenu.TraerTodosTipoPefilMenuesPorPerfil(base.EntityId).ToList();

            foreach (TipoPerfilMenues PerfilItem in lst_PerfilMenu)
            {
                foreach (Menues oMenu in pListMenu)
                {
                    if (PerfilItem.oMenu.Id == oMenu.Id)
                    {
                        oMenu.Seleccionado = true;
                        oMenu.PuedeBorrar = PerfilItem.PuedeBorrar;
                        oMenu.PuedeGrabar = PerfilItem.PuedeGrabar;
                        hayDatos = true;
                        cantSel++;
                        this.CargarUserControl2(PerfilItem.IdUsuarioModifico, PerfilItem.FecUltimaModificacion);
                        break;
                    }
                }
            }
            //TO DO: averiguar que es esto.
            usrUltimaModificacion2.Visible = hayDatos;
            this.lblCantSeleccionados.Text = "Seleccionados: " + cantSel;
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "VerificarPermisos", HttpContext.Current.Request.Url.LocalPath);
        }

        return pListMenu;
    }

    /// <summary>
    /// Metodo que carga los datos en el usercontrol usrUltimaModificacion2
    /// </summary>
    private void CargarUserControl2(long pIdUsuarioModifico, DateTime pFechaUltimaModificacion)
    {
        usrUltimaModificacion2.FecUltimaModificacion = pFechaUltimaModificacion.ToShortDateString();
        usrUltimaModificacion2.IdUsuarioModifico = pIdUsuarioModifico;
        usrUltimaModificacion2.UltimaModificacionBind();
    }

    /// <summary>
    /// Metodo que  guarda los datos de los permiso de los menues.
    /// </summary>
    private void GrabarPermisosMenues()
    {
        try
        {
            var oUsrLogueado = base.TraerUsuarioLogeado();
            bool yaTiene1 = false;
            oListaMenuPermiso = new List<TipoPerfilMenues>();
            Services.Get<ServTipoPerfilMenues>().BorrarTipoPerfilMenuPorPerfil(base.EntityId);

            foreach (GridViewRow gvRow in grdGrillaMenues.Rows)
            {
                CheckBox check = (CheckBox)gvRow.FindControl("chbxSeleccionado");
                CheckBox checkGrabar = (CheckBox)gvRow.FindControl("chbkGrabar");
                CheckBox checkBorrar = (CheckBox)gvRow.FindControl("chbkBorrar");

                if (check.Checked)
                {
                    Label lbl = (Label)gvRow.FindControl("lblSeleccionado");
                    string mId = lbl.Text;

                    var oPerfilMenu = new TipoPerfilMenues();
                    oPerfilMenu.IdMenu = Int64.Parse(mId);
                    oPerfilMenu.IdTipoPerfil = base.EntityId;
                    oPerfilMenu.IdUsuarioModifico = oUsrLogueado.Id;
                    oPerfilMenu.FecUltimaModificacion = DateTime.Now;
                    if (checkBorrar.Checked)
                        oPerfilMenu.PuedeBorrar = true;
                    else
                        oPerfilMenu.PuedeBorrar = false;
                    if (checkGrabar.Checked)
                        oPerfilMenu.PuedeGrabar = true;
                    else
                        oPerfilMenu.PuedeGrabar = false;

                    oListaMenuPermiso.Add(oPerfilMenu);
                    AgregarPadres(!yaTiene1);
                    yaTiene1 = true;
                }
            }

            if (oListaMenuPermiso.Count > 0)
            {
                oServPerfilMenu.AgregarNuevaListaPerfilMenu(oListaMenuPermiso);
            }

            this.VisibilidadPaneles(true, false, false);
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "GrabarDatos", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    /// <summary>
    /// Metodo que agrega un elemento padre.
    /// </summary>
    private void AgregarPadres(bool pAgregar_1)
    {
        var oUsrLogueado = TraerUsuarioLogeado();
        var oPerfilMenu = oListaMenuPermiso[oListaMenuPermiso.Count - 1];
        var mMenu = oServMenues.TraerUnicoMenues(oPerfilMenu.IdMenu);

        // Agrego el Padre
        bool padreAgregado = false;
        foreach (TipoPerfilMenues item in oListaMenuPermiso)
        {
            if (item.IdMenu == mMenu.IdMenuPadre)
            {
                padreAgregado = true;
                break;
            }
        }
        if (!padreAgregado)
        {
            oPerfilMenu = new TipoPerfilMenues();
            oPerfilMenu.IdMenu = mMenu.IdMenuPadre;
            oPerfilMenu.IdTipoPerfil = base.EntityId;
            oPerfilMenu.IdUsuarioModifico = oUsrLogueado.Id;
            oPerfilMenu.FecUltimaModificacion = DateTime.Now;

            oListaMenuPermiso.Add(oPerfilMenu);
        }

        //Agrego el x.x.0
        bool _0Agregado = false;
        String pos = (String)mMenu.Posicion;
        pos = pos.Remove(pos.Length - 1);
        pos = pos + "0";
        var mMenu_0 = oServMenues.TraerUnicoMenues(pos);
        if (mMenu_0 != null)
        {
            foreach (TipoPerfilMenues item in oListaMenuPermiso)
            {
                if (item.IdMenu == mMenu_0.Id)
                {
                    _0Agregado = true;
                    break;
                }
            }
            if (!_0Agregado)
            {
                oPerfilMenu = new TipoPerfilMenues();
                oPerfilMenu.IdMenu = mMenu_0.Id;
                oPerfilMenu.IdTipoPerfil = base.EntityId;
                oPerfilMenu.IdUsuarioModifico = oUsrLogueado.Id;
                oPerfilMenu.FecUltimaModificacion = DateTime.Now;

                oListaMenuPermiso.Add(oPerfilMenu);
            }

            if (pAgregar_1) //esto es para agregar sólo una vez la raíz
            {
                oPerfilMenu = new TipoPerfilMenues();
                oPerfilMenu.IdMenu = 1;
                oPerfilMenu.IdTipoPerfil = base.EntityId;
                oPerfilMenu.IdUsuarioModifico = oUsrLogueado.Id;
                oPerfilMenu.FecUltimaModificacion = DateTime.Now;

                oListaMenuPermiso.Add(oPerfilMenu);
            }
        }
    }

    #endregion

    #region Métodos De grilla de Menúes

    protected void grdGrillaMenues_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.grdGrillaMenues.PageIndex = e.NewPageIndex;
        this.CargoGrillaMenues(CriterioOrdenacion);
    }

    protected void grdGrillaMenues_Sorting(object sender, GridViewSortEventArgs e)
    {
        this.ObtenerDireccionOrden(e);
        this.CargoGrillaMenues(string.Format("{0} {1}", e.SortExpression, DireccionOrdenacion));
    }

    protected void grdGrillaMenues_RowCommand(Object sender, CommandEventArgs e)
    {

    }

    #endregion

    #region Botones

    protected void btnGrabarMenues_Click(object sender, EventArgs e)
    {
        this.GrabarPermisosMenues();
    }

    #endregion

    #endregion

}