﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IOT.ORM;
using IOT.Domain;
using System.Transactions;
using System.Web.Security;
using System.Data;
using IOT.Services;


public partial class Usuarios_Menues_Default : PageBase
{

    private ServTipoPerfilMenues oServPerfilMenus = Services.Get<ServTipoPerfilMenues>();

    #region Propiedades

    enum Estado
    {
        Activo,
        Inactivo
    }

    #endregion

    #region "Main"

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            InicializacionPantalla();
        }
    }

    #endregion

    #region "Metodos"

    /// <summary>
    /// Metodo que inicializa 
    /// </summary>
    private void InicializacionPantalla()
    {
        CargoPermisosdePantalla();
        this.CargarDDLMenues();
        this.CargoGrilla("Posicion ascending", this.ObtenerMenuFiltro());
    }

    private void CargoPermisosdePantalla()
    {
        var tipoPerfilMenu = oServPerfilMenus.TraerTodosTipoPefilMenuesPorPerfil(IdPerfilUsuarioLogueado).Where(p => p.oMenu.PathPagina.Contains("Usuarios/Menues"));
        if (tipoPerfilMenu.Count() != 0)
        {
            List<TipoPerfilMenues> t = tipoPerfilMenu.ToList();
            btnAgregar.Visible = t.First().PuedeGrabar;
            btnGrabar.Visible = t.First().PuedeGrabar;
        }
        else
            Response.Redirect("~/Application/Default.aspx", false);

    }

    /// <summary>
    /// Metodo que carga los datos en los combos.    /// </summary>
    private void CargarDDLMenues()
    {
        var oLista = Services.Get<ServMenues>().TraerMenuesRaiz();

        ddlFiltroMenuPadre.DataSource = oLista;
        ddlFiltroMenuPadre.DataTextField = "Nombre";
        ddlFiltroMenuPadre.DataValueField = "Id";
        ddlFiltroMenuPadre.DataBind();
        ddlFiltroMenuPadre.Items.Insert(0, new ListItem("", "0"));

        ddlMenuPadre.DataSource = oLista;
        ddlMenuPadre.DataTextField = "Nombre";
        ddlMenuPadre.DataValueField = "Id";
        ddlMenuPadre.DataBind();
    }

    /// <summary>
    /// Metodo que carga los datos en la grilla
    /// </summary>
    /// <param name="pOrderBy">Criterio de ordenacion</param>
    /// <param name="pMenues">Objecto para determinar si hay filtro aplicado.</param>
    private void CargoGrilla(string pOrderBy, Menues pMenues)
    {
        this.VisibilidadPaneles(true, false);
        CriterioOrdenacion = pOrderBy;

        List<Menues> wQuery = null;
        try
        {
            // Verificamos si el tipo obtenido es nulo, 
            // Si es nulo se carga la grilla sin filtro.
            if (pMenues == null)
            {
                wQuery = Services.Get<ServMenues>().TraerTodosMenues().OrderBy(pOrderBy).ToList();
            }
            // Si el tipo no es nulo, carga los datos de filtro en la grilla.
            else
            {
                wQuery = Services.Get<ServMenues>().TraerTodosMenuesParam(pMenues).OrderBy(pOrderBy).ToList();
            }
            grdGrilla.DataSource = wQuery;
            lblCantidadFilas.Text = wQuery.Count().ToString();
            grdGrilla.DataBind();
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "CargoGrilla", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    /// <summary>
    /// Metodo que obtiene los datos del filtro.
    /// </summary>
    /// <returns>Retorna un objecto de tipo Menu</returns>
    private Menues ObtenerMenuFiltro()
    {
        Menues oMenues = null;
        if (!string.IsNullOrWhiteSpace(this.txtFiltroMenu.Text.Trim()))
        {
            oMenues = new Menues();
            oMenues.Nombre = this.txtFiltroMenu.Text.Trim();
        }

        if (this.ddlFiltroMenuPadre.SelectedValue != "-1")
        {
            if (oMenues == null)
            {
                oMenues = new Menues();
            }
            oMenues.IdMenuPadre = Convert.ToInt64(this.ddlFiltroMenuPadre.SelectedValue);
        }
        return oMenues;
    }

    /// <summary>
    /// Metodo que establece la visibilidad de los paneles.
    /// </summary>
    private void VisibilidadPaneles(bool pPnlGrilla, bool pPnlDetalle)
    {
        pnlGrilla.Visible = pPnlGrilla;
        pnlDetalles.Visible = pPnlDetalle;
    }

    /// <summary>
    /// Metodo que establece el criterio de ordenacion de las columnas de las grillas.
    /// </summary>
    private void ObtenerDireccionOrden(GridViewSortEventArgs evt)
    {
        if (DireccionOrdenacion == "")
        {
            DireccionOrdenacion = "ascending";
        }
        else
        {
            if (DireccionOrdenacion == "ascending")
                DireccionOrdenacion = "descending";
            else
                DireccionOrdenacion = "ascending";
        }
    }

    /// <summary>
    /// Metodo que carga los datos en los controles.
    /// </summary>
    private void CargarControles()
    {
        var oMenues = Services.Get<ServMenues>().TraerUnicoMenues(base.EntityId);
        this.lblId.Text = oMenues.Id.ToString();
        this.txtNombre.Text = oMenues.Nombre;
        this.txtDescripcion.Text = oMenues.Descripcion;
        this.txtPathPagina.Text = oMenues.PathPagina;
        this.txtPosicion.Text = oMenues.Posicion;
        this.ddlMenuPadre.SelectedValue = oMenues.IdMenuPadre.ToString();
        this.CargarUserControl(oMenues.IdUsuarioModifico, oMenues.FecUltimaModificacion);
    }

    /// <summary>
    /// Metodo que carga los datos en el usercontrol usrUltimaModificacion1.
    /// </summary>
    private void CargarUserControl(long pIdUsuarioMofifico, DateTime pFechaUltimaModificacion)
    {
        usrUltimaModificacion1.FecUltimaModificacion = pFechaUltimaModificacion.ToShortDateString();
        usrUltimaModificacion1.IdUsuarioModifico = pIdUsuarioMofifico;
        usrUltimaModificacion1.UltimaModificacionBind();
    }

    /// <summary>
    /// Metodo que establece los valores para crear un nuevo objeto Menu
    /// </summary>
    private void CrearNuevo()
    {
        this.Limpiar();
        this.VisibilidadPaneles(false, true);
        EsNuevo = true;
        usrUltimaModificacion1.Visible = false;
    }

    /// <summary>
    /// Metodo que guarda los datos de la pagina
    /// </summary>
    private void GrabarDatos()
    {
        try
        {
            var oMenues = ObtenerDatos();
            if (/*base.ValidateModel(oMenues, ref frmDetalles)*/true)
            {

                if (base.EsNuevo)
                {
                    EntityId = Services.Get<ServMenues>().AgregarMenues(oMenues);
                    this.LimpiarFiltros();
                }
                else
                {
                    Services.Get<ServMenues>().ActualizarMenues(oMenues);
                }
                this.Limpiar();
                this.CargoGrilla(base.CriterioOrdenacion, this.ObtenerMenuFiltro());
            }
        }
        catch (Exception ex)
        {
            this.errorMensaje.ErrorBind(ex, "GrabarDatos", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    /// <summary>
    /// Metodo que obtiene los datos de la pagina
    /// </summary>
    /// <returns>Retorna un objeto Menues</returns>
    private Menues ObtenerDatos()
    {
        Menues oMenues = null;
        try
        {
            oMenues = new Menues();
            if (!EsNuevo)
            {
                oMenues = Services.Get<ServMenues>().TraerUnicoMenues(base.EntityId);
            }
            oMenues.Descripcion = txtDescripcion.Text.Trim();
            oMenues.IdMenuPadre = 0;
            oMenues.Nombre = txtNombre.Text.Trim();
            oMenues.PathPagina = txtPathPagina.Text.Trim();
            oMenues.IdMenuPadre = long.Parse(ddlMenuPadre.SelectedValue);
            oMenues.FecUltimaModificacion = DateTime.Now;
            oMenues.Posicion = txtPosicion.Text.Trim();
            oMenues.IdUsuarioModifico = TraerUsuarioLogeado().Id;
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "ObtenerDatos", HttpContext.Current.Request.Url.LocalPath);
        }
        return oMenues;
    }

    /// <summary>
    /// Metodo que limpia los datos en los filtros.
    /// </summary>
    private void LimpiarFiltros()
    {
        this.txtFiltroMenu.Text = string.Empty;
        this.ddlFiltroMenuPadre.SelectedIndex = -1;
    }

    /// <summary>
    /// Metodo que limpia los datos de los elementos de la pagina.
    /// </summary>
    private void Limpiar()
    {
        this.lblId.Text = string.Empty;
        txtDescripcion.Text = string.Empty;
        txtNombre.Text = string.Empty;
        txtPathPagina.Text = string.Empty;
        txtPosicion.Text = string.Empty;
        ddlMenuPadre.SelectedIndex = -1;
    }

    #endregion

    #region "Eventos Botones"

    protected void btnAgregar_Click(object sender, EventArgs e)
    {
        litNombre.Text = "Nuevo Menu - Pagina";
        CrearNuevo();
    }

    protected void btnGrabar_Click(object sender, EventArgs e)
    {
        Page.Validate();
        if (Page.IsValid)
        {
            GrabarDatos();
        }
    }

    protected void btnSalir_Click(object sender, EventArgs e)
    {
        Menues oMenues = null;
        try
        {
            oMenues = new Menues();
            //base.ValidateModelFalse(oMenues, ref frmDetalles);
            VisibilidadPaneles(true, false);
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "btnSalir_Click", HttpContext.Current.Request.Url.LocalPath);
        }
        finally
        {
            oMenues = null;
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        // Verifica que campos del filtro contienen datos para realizar el filtro.
        try
        {
            this.CargoGrilla("Posicion ascending", this.ObtenerMenuFiltro());
        }
        catch (Exception ex)
        {
            errorMensaje.ErrorBind(ex, "btnBuscar_Click", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    #endregion

    #region "Eventos Grilla"

    protected void grdGrilla_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdGrilla.PageIndex = e.NewPageIndex;
        this.CargoGrilla(CriterioOrdenacion, this.ObtenerMenuFiltro());
    }

    protected void grdGrilla_Sorting(object sender, GridViewSortEventArgs e)
    {
        this.ObtenerDireccionOrden(e);
        this.CargoGrilla(string.Format("{0} {1}", e.SortExpression, DireccionOrdenacion), this.ObtenerMenuFiltro());
    }

    protected void grdGrilla_RowCommand(Object sender, CommandEventArgs e)
    {
        if (e.CommandName == "Editar")
        {
            base.EntityId = Convert.ToInt64(e.CommandArgument);
            base.EsNuevo = false;
            this.CargarControles();
            litNombre.Text = "Editar Menu - Pagina";
            this.VisibilidadPaneles(false, true);
            usrUltimaModificacion1.Visible = true;

        }
    }
    
    #endregion

}
