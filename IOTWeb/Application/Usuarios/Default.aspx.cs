﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IOT.ORM;
using IOT.Domain;
//using System.Transactions;
using System.Web.Security;
using System.Data;
using IOT.Services;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Configuration;

public partial class Usuarios_Default : PageBase
{

    #region Propiedades
    private ServTipoPerfilMenues oServPerfilMenus = Services.Get<ServTipoPerfilMenues>();

    public string MensajeInformacion
    {
        get { return (ViewState["MensajeInformacion"]).ToString(); }
        set { ViewState["MensajeInformacion"] = value; }
    }
    
    public bool EsExistenteUserName
    {
        get { return Convert.ToBoolean(ViewState["EsExistenteUserName"]); }
        set { ViewState["EsExistenteUserName"] = value; }
    }

    public bool EsExistenteMail
    {
        get { return Convert.ToBoolean(ViewState["EsExistenteMail"]); }
        set { ViewState["EsExistenteMail"] = value; }
    }

    public bool EsExistenteDNI
    {
        get { return Convert.ToBoolean(ViewState["EsExistenteDNI"]); }
        set { ViewState["EsExistenteDNI"] = value; }
    }

    public bool EsHabilitadoParaEnviarMail
    {
        get { return Convert.ToBoolean(ViewState["EsHabilitadoParaEnviarMail"]); }
        set { ViewState["EsHabilitadoParaEnviarMail"] = value; }
    }

    protected string EsNuevoQueryString
    {
        get
        {
            if (Request.QueryString["Nuevo"] != null)
            {
                return Request.QueryString["Nuevo"].ToString();
            }
            else
            {
                return "";
            }
        }
    }
    
    #endregion

    #region Enumeraciones

    enum Estado
    {
        Activo,
        Inactivo
    }

    enum EnumPerfil
    {
        SU,
        AD,
        JBC,
        JMAC,
        MES,
        DGRT,
        JCI,
        I,
        TH,
        ADM,
        ADMUR,
        VUFO
    }

    #endregion

    #region "Main"

    protected void Page_Load(object sender, EventArgs e)
    {
        if (base.IdUsuarioLogueado == -1)
        {
            Response.Redirect("~/Application/Login/Default.aspx", false);
            return;
        }
        if (!Page.IsPostBack)
            InicializacionPantalla();
    }

    #endregion

    #region "Metodos"

    private void InicializacionPantalla()
    {
        CargoPermisosdePantalla();
        bool EsHabilitarTodosCamposABMUsuario = false;
        var oTipoPerfil = Services.Get<ServUsuario>().TraerUnicoTipoPerfil(this.IdPerfilUsuarioLogueado);
        try
        {
            EsHabilitarTodosCamposABMUsuario = true;
            EsHabilitadoParaEnviarMail = !EsHabilitarTodosCamposABMUsuario;
            HabilitarCamposUsuarioABM(EsHabilitarTodosCamposABMUsuario);
        }
        catch (SystemException ex)
        {
            errorMensaje.ErrorBind(ex, "InicializacionPantalla", HttpContext.Current.Request.Url.LocalPath);
        }

        if (EsNuevoQueryString != null)
        {
            if (EsNuevoQueryString == "true")
            {
                CrearNuevo();
            }
        }
    }

    private void CargoPermisosdePantalla()
    {
        var tipoPerfilMenu = oServPerfilMenus.TraerTodosTipoPefilMenuesPorPerfil(IdPerfilUsuarioLogueado).Where(p => p.oMenu.PathPagina.Contains("Usuarios"));
        if (tipoPerfilMenu.Count() != 0)
        {
            List<TipoPerfilMenues> t = tipoPerfilMenu.ToList();
            btnAgregar.Visible = t.First().PuedeGrabar;
            btnGrabar.Visible = t.First().PuedeGrabar;
        }
        else
            Response.Redirect("~/Application/Default.aspx", false);

    }

    private void HabilitarCamposUsuarioABM(bool pEsHabilitarTodosCamposABMUsuario)
    {
        if (pEsHabilitarTodosCamposABMUsuario)
        {
            CargoDDLFiltroActivo();
            CargoDDLFiltroPerfil();
            CargoGrilla("Id ascending", null);
        }
        else
        {
            VisibilidadPaneles(true, false);
            VisibilidadCamposDetalle(false);
        }
    }

    private void CargoDDLFiltroPerfil()
    {
        ServUsuario oServ = Services.Get<ServUsuario>();
        try
        {
            var query = oServ.TraerTodosPerfiles().OrderBy(p => p.Descripcion);

            ddlFiltroPerfil.DataSource = query;
            ddlFiltroPerfil.DataTextField = "Descripcion";
            ddlFiltroPerfil.DataValueField = "Id";
            ddlFiltroPerfil.DataBind();

            ddlFiltroPerfil.Items.Insert(0, new ListItem("", "0"));
        }
        catch (SystemException ex)
        {
            this.errorMensaje.ErrorBind(ex, "CargoDDLFiltroPerfil", HttpContext.Current.Request.Url.LocalPath);
        }
        finally
        {
            oServ = null;
        }

    }

    private void CargoDDLPerfil()
    {
        ServUsuario oServ = Services.Get<ServUsuario>();
        try
        {
            var query = oServ.TraerTodosPerfiles().OrderBy(p => p.Descripcion);
            this.ddlPerfil.DataSource = query;
            this.ddlPerfil.DataTextField = "Descripcion";
            this.ddlPerfil.DataValueField = "Id";
            this.ddlPerfil.DataBind();
        }
        catch (SystemException ex)
        {
            this.errorMensaje.ErrorBind(ex, "CargoDDLPerfil", HttpContext.Current.Request.Url.LocalPath);
        }
        finally
        {
            oServ = null;
        }
    }

    private void CargoDDLUbicacion() { }

    private void CargoDDLFiltroActivo()
    {
        ddlFiltroActivo.Items.Add(new ListItem("", "0"));
        ddlFiltroActivo.Items.Add(new ListItem(Enum.GetName(typeof(Estado), 0), "1"));
        ddlFiltroActivo.Items.Add(new ListItem(Enum.GetName(typeof(Estado), 1), "2"));
        ddlFiltroActivo.DataBind();
    }

    private void CargoGrilla(string pOrderBy, Usuario pUsuario)
    {
        this.VisibilidadPaneles(true, false);
        this.CriterioOrdenacion = pOrderBy;
        List<Usuario> wQuery = null;
        try
        {
            // Verificamos si el tipo obtenido es nulo, 
            // Si es nulo se carga la grilla sin filtro.
            if (pUsuario == null)
            {
                wQuery = Services.Get<ServUsuario>().TraerTodosUsuario().OrderBy(pOrderBy).ToList();
            }
            // Si el tipo no es nulo, carga los datos de filtro en la grilla.
            else
            {
                wQuery = Services.Get<ServUsuario>().TraerTodosUsuariosParam(pUsuario).OrderBy(pOrderBy).ToList();
            }
            grdGrilla.DataSource = wQuery;
            grdGrilla.DataBind();
            lblCantidadFilas.Text = wQuery.Count().ToString();
        }
        catch (Exception ex)
        {
            this.errorMensaje.ErrorBind(ex, "CargoGrilla", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    private Usuario GrabarDatos()
    {
        Usuario oUsuario = null;

        try
        {
            oUsuario = ObtenerDatos();

            if (/*base.ValidateModel(oUsuario, ref frmDetalles)*/ true)
            {
            if (EsNuevo)
            {
                if (!ValidarUsuarioExiste(Services.Get<ServUsuario>(), oUsuario))
                {
                    //Verifico si es 
                    EntityId = Services.Get<ServUsuario>().AgregarUsuario(oUsuario);
                }
                else
                {
                    this.MostrarMensajeUsuario(MensajeInformacion);
                    this.LimpiarMensaje();
                    return null;
                }
            }
            else
            {
                Services.Get<ServUsuario>().ActualizarUsuario(oUsuario);
            }

                this.Limpiar();
                if (!EsHabilitadoParaEnviarMail)
                {
                    this.CargoGrilla(base.CriterioOrdenacion, this.ObtenerDatosFiltro());
                }
            }
            else
            {
                oUsuario = null;
            }

        }
        catch (SystemException ex)
        {
            this.errorMensaje.ErrorBind(ex, "GrabarDatos", HttpContext.Current.Request.Url.LocalPath);
        }
        return oUsuario;
    }

    private Usuario ObtenerDatos()
    {
        Usuario oUsuario = new Usuario();
        try
        {
            if (!EsNuevo)
            {
                oUsuario = Services.Get<ServUsuario>().TraerUnicoUsuario(base.EntityId);
                oUsuario.UserName = this.txtUsuario.Text.ToUpper();
            }
            else
            {
                oUsuario.Password = Encriptacion.Encriptar("123456789");
                if (EsHabilitadoParaEnviarMail)
                {
                    oUsuario.UserName = txtEmail.Text.ToUpper();
                }
                else
                {
                    oUsuario.UserName = txtUsuario.Text.ToUpper();
                }
            }
            oUsuario.Apellido = txtApellido.Text.ToUpper();
            oUsuario.Nombre = txtNombre.Text.ToUpper();
            oUsuario.Activo = chbxActivo.Checked;
            oUsuario.Email = txtEmail.Text.ToLower();
            oUsuario.IdTipoPerfil = long.Parse(this.ddlPerfil.SelectedValue);
            oUsuario.URLImagenUsuario = "default.jpg";
            oUsuario.FecUltimaModificacion = DateTime.Now;
            oUsuario.IdUsuarioModifico = base.IdUsuarioLogueado; //TODO: Usuario Logueado
            return oUsuario;
        }
        catch (SystemException ex)
        {
            this.errorMensaje.ErrorBind(ex, "ObtenerDatos", HttpContext.Current.Request.Url.LocalPath);
            oUsuario = null;
        }

        return oUsuario;
    }

    public static string GenerarCodigoActivacion()
    {
        Guid guid = Guid.NewGuid();
        return guid.ToString();
    }

    private void CrearNuevo()
    {
        CargoDDLPerfil();
        CargoDDLUbicacion();
        Limpiar();
        VisibilidadPaneles(false, true);
        EsNuevo = true;
        usrUltimaModificacion1.Visible = false;

    }

    private void Limpiar()
    {
        this.lblId.Text = string.Empty;
        this.txtUsuario.Text = string.Empty;
        this.txtApellido.Text = string.Empty;
        this.txtNombre.Text = string.Empty;
        this.chbxActivo.Checked = false;
        this.txtEmail.Text = string.Empty; ;
        this.ddlPerfil.SelectedValue = "1";
    }

    private void LimpiarMensaje()
    {
        if (EsExistenteUserName)
        {
            txtUsuario.Text = string.Empty;
        }
        if (EsExistenteMail)
        {
            txtEmail.Text = string.Empty;
        }
        MensajeInformacion = string.Empty;
    }

    private void ResetearPassword()
    {
        ServUsuario oServ = Services.Get<ServUsuario>();
        Usuario oUsuario;
        try
        {
            oUsuario = oServ.TraerUnicoUsuario(base.EntityId);
            oUsuario.Password = Encriptacion.Encriptar("123456789");
            oServ.ActualizarUsuario(oUsuario);
            MostrarMensajeUsuario(IOT.Services.ResourceSysMsg.sys_msg_ResetPassword);
        }
        catch (SystemException ex)
        {
            errorMensaje.ErrorBind(ex, "ResetearPassword", HttpContext.Current.Request.Url.LocalPath);
        }
        finally
        {
            oServ = null;
            oUsuario = null;
        }
    }

    //TO DO: esto se elimina?
    private void MostrarMensajeUsuario(string pMensaje)
    {
        //lblModalMensaje.Text = pMensaje;
        //string script = "MostrarMensajeUsuario();";
        //ScriptManager.RegisterStartupScript(this, typeof(Page), "MostrarMensajeUsuario", script, true);
    }

    private void VisibilidadPaneles(bool pPnlGrilla, bool pPnlDetalle)
    {
        pnlGrilla.Visible = pPnlGrilla;
        pnlDetalles.Visible = pPnlDetalle;
    }

    private void VisibilidadCamposDetalle(bool pVisible)
    {
        //TO DO: ya que la estructura del aspx cambió, averiguar cual seria el codigo de remplazo aqui. 

        //divIDTitu.Visible = pVisible;
        //cgUserName.Visible = pVisible;
        //divPerfil.Visible = pVisible;
        //divActivo.Visible = pVisible;
        //usrUltimaModificacion1.Visible = pVisible;
        //divFiltros.Visible = pVisible;
        //divGrilla.Visible = pVisible;
        //spanTotalRegistro.Visible = pVisible;
    }

    private void EnviarMail(Usuario pUsuario, string pAccion)
    {
        
    }

    private void CargarControles()
    {
        try
        {
            var oUsuario = Services.Get<ServUsuario>().TraerUnicoUsuario(base.EntityId);
            lblId.Text = oUsuario.Id.ToString();
            txtUsuario.Text = oUsuario.UserName.ToUpper();
            txtApellido.Text = oUsuario.Apellido.ToUpper();
            txtNombre.Text = oUsuario.Nombre.ToUpper();
            chbxActivo.Checked = (bool)oUsuario.Activo;
            txtEmail.Text = oUsuario.Email.ToLower();
            ddlPerfil.SelectedValue = oUsuario.IdTipoPerfil.ToString();
            CargarUserControl(oUsuario.IdUsuarioModifico, oUsuario.FecUltimaModificacion);
        }
        catch (SystemException ex)
        {
            this.errorMensaje.ErrorBind(ex, "CargarControles", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    private void CargarUserControl(long pIdUsuarioMofifico, DateTime pFechaUltimaModificacion)
    {
        usrUltimaModificacion1.FecUltimaModificacion = pFechaUltimaModificacion.ToShortDateString();
        usrUltimaModificacion1.IdUsuarioModifico = pIdUsuarioMofifico;
        usrUltimaModificacion1.UltimaModificacionBind();
    }

    private Usuario ObtenerDatosFiltro()
    {
        Usuario oUsuario = null;

        // Verifica que campos del filtro contienen datos para realizar el filtro.
        if (!string.IsNullOrWhiteSpace(this.txtFiltroUsuario.Text.Trim()))
        {
            oUsuario = new Usuario();
            oUsuario.UserName = this.txtFiltroUsuario.Text.Trim();
            oUsuario.IdTipoPerfil = int.Parse(this.ddlFiltroPerfil.SelectedValue.ToString());
        }
        if (ddlFiltroActivo.SelectedIndex > 0)
        {
            if (oUsuario == null)
            {
                oUsuario = new Usuario();
            }
            oUsuario.Activo = (Convert.ToInt32(this.ddlFiltroActivo.SelectedValue) == 1 ? true : false);
        }
        else
        {
            if (oUsuario == null)
            {
                oUsuario = new Usuario();
            }
            //TODO: 21/02/2013
            oUsuario.Activo = null;
        }
        if (this.ddlFiltroPerfil.SelectedIndex > 0)
        {
            if (oUsuario == null)
            {
                oUsuario = new Usuario();
            }
            oUsuario.IdTipoPerfil = Convert.ToInt32(this.ddlFiltroPerfil.SelectedValue);
        }
        return oUsuario;
    }

    private void ObtenerDireccionOrden(GridViewSortEventArgs evt)
    {
        if (DireccionOrdenacion == "")
        {
            DireccionOrdenacion = "ascending";
        }
        else
        {
            if (DireccionOrdenacion == "ascending")
                DireccionOrdenacion = "descending";
            else
                DireccionOrdenacion = "ascending";
        }
    }

    #endregion

    #region "Eventos Botones"

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        try
        {
            this.CargoGrilla(this.CriterioOrdenacion, this.ObtenerDatosFiltro());
        }
        catch (SystemException ex)
        {
            this.errorMensaje.ErrorBind(ex, "btnBuscar_Click", HttpContext.Current.Request.Url.LocalPath);
        }
    }

    protected void btnAgregar_Click(object sender, EventArgs e)
    {
        this.litNombre.Text = "Nuevo Usuario";
        this.CrearNuevo();
    }

    protected void btnGrabar_Click(object sender, EventArgs e)
    {
        this.Page.Validate();
        if (this.Page.IsValid)
        {
            var oUsu = this.GrabarDatos();
            if (oUsu != null)
            {
                if (this.EsHabilitadoParaEnviarMail)
                {
                    this.EnviarMail(oUsu, "Activacion");
                }
            }
            // Por si viene de otra web distinta de esta, entro en el if
            if (this.Request.QueryString["Nuevo"] != null)
            {
                if (this.Request.QueryString["Nuevo"].ToString() == "true")
                {
                    if (oUsu != null)
                    {
                        this.Redirect(base.URLAnterior);
                    }
                }
            }
            else
            {
                if (oUsu != null)
                {
                    this.VisibilidadPaneles(true, false);
                }
            }
        }
    }

    protected void btnSalir_Click(object sender, EventArgs e)
    {
        Usuario oUsuario = null;
        try
        {
            // Por si viene de otra web distinta de esta, entro en el if
            if (Request.QueryString["Nuevo"] != null)
            {
                if (Request.QueryString["Nuevo"].ToString() == "true")
                {
                    Redirect(base.URLAnterior);
                }
            }
            else
            {
                oUsuario = new Usuario();
                //TO DO: averiguar que es lo que hace esto
                //base.ValidateModelFalse(oUsuario, ref frmDetalles);
                VisibilidadPaneles(true, false);
            }
        }
        catch (Exception ex)
        {
            this.errorMensaje.ErrorBind(ex, "btnSalir_Click", HttpContext.Current.Request.Url.LocalPath);
        }
        finally
        {
            oUsuario = null;
        }
    }

    protected void btnResetear_Click(object sender, EventArgs e)
    {
        ResetearPassword();
    }

    #endregion

    #region "Eventos Grilla"

    protected void grdGrilla_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdGrilla.PageIndex = e.NewPageIndex;
        this.CargoGrilla(CriterioOrdenacion, this.ObtenerDatosFiltro());
    }

    protected void grdGrilla_Sorting(object sender, GridViewSortEventArgs e)
    {
        ObtenerDireccionOrden(e);
        this.CargoGrilla(string.Format("{0} {1}", e.SortExpression, DireccionOrdenacion), this.ObtenerDatosFiltro());
    }

    protected void grdGrilla_RowCommand(Object sender, CommandEventArgs e)
    {
        if (e.CommandName == "Editar")
        {
            base.EntityId = Convert.ToInt64(e.CommandArgument);
            base.EsNuevo = false;

            this.CargoDDLPerfil();
            this.CargoDDLUbicacion();
            this.CargarControles();
            this.litNombre.Text = "Editar Usuario";
            this.VisibilidadPaneles(false, true);
            this.btnResetear.Visible = true;
            this.usrUltimaModificacion1.Visible = true;

        }
    }

    #endregion

    #region "Validaciones"

    protected void cvSucursal_ServerValidate(object source, ServerValidateEventArgs args) { }

    protected void cvPerfil_ServerValidate(object source, ServerValidateEventArgs args) { }

    private bool ValidarUsuarioExiste(ServUsuario pServUsuario, Usuario pUsuario)
    {
        EsExistenteUserName = false;
        EsExistenteMail = false;
        EsExistenteDNI = false;
        MensajeInformacion = string.Empty;

        Usuario oUsu = null;

        try
        {
            if (pServUsuario.ExisteUsuarioInterno(pUsuario) != null)
            {
                oUsu = pServUsuario.ExisteUsuarioInterno(pUsuario);
            }
            else
            {
                return false;
            }
            if (oUsu.UserName.ToUpper() == pUsuario.UserName.ToUpper())
            {
                EsExistenteUserName = true;
                MensajeInformacion = "-" + IOT.Services.ResourceSysMsg.sys_msg_UsuarioExistente;
            }
            if (oUsu.Email.ToUpper() == pUsuario.Email.ToUpper())
            {
                EsExistenteMail = true;
                MensajeInformacion = MensajeInformacion + Convert.ToChar(10) + "-" + IOT.Services.ResourceSysMsg.sys_msg_UsuarioMailExistente;
            }
            if (!EsExistenteMail && !EsExistenteUserName && !EsExistenteDNI)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        catch (SystemException ex)
        {
            this.errorMensaje.ErrorBind(ex, "ValidarUsuarioExiste", HttpContext.Current.Request.Url.LocalPath);
        }
        finally
        {
            oUsu = null;
        }
        return true; //TO DO:
    }

    #endregion

}
