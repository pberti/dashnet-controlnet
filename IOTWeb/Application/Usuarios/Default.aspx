﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Usuarios_Default" %>

<%@ Register TagName="usrUltimaModificacion" TagPrefix="uc1" Src="~/UserControl/usrUltimaModificacion.ascx"  %>
<%@ Register TagName="Error" TagPrefix="usrError" Src="~/UserControl/Error.ascx" %>
<%@ Register TagName="userProcesando" TagPrefix="uc1" Src="~/UserControl/userProcesando.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   
    <%--Vista Grilla--%>
    <asp:Panel ID="pnlGrilla" runat="server">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                </div>
                <div class="title_right">
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Administracion de Usuarios</h2>
                            <div class="pull-right">
                                    <button type="submit" id="btnAgregar" runat="server" causesvalidation="False" class="btn btn-round btn-primary" onserverclick="btnAgregar_Click">
                                        <i class="fa fa-plus-circle"></i> Agregar Nuevo
                                    </button>
                                </div>
                            <div class="clearfix"></div>
                        </div>
                        <%--FILTRO--%>
                        <div class="x_content">
                            <div class="x_panel">
                                <div class="form-horizontal form-label-left pull-left">
                                    <asp:Label ID="lblFiltroUsuario" runat="server" Text="Usuario" class="control-label col-md-1 col-sm-1 col-xs-12"></asp:Label>
                                    <div class="col-md-2 col-sm-2 col-xs-12">
                                        <asp:TextBox ID="txtFiltroUsuario" runat="server" class="form-control">
                                        </asp:TextBox>
                                    </div>

                                    <asp:Label ID="lblFiltroActivo" runat="server" Text="Estado" class="control-label col-md-1 col-sm-1 col-xs-12"></asp:Label>
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        <asp:DropDownList ID="ddlFiltroActivo" runat="server" class="form-control">
                                        </asp:DropDownList>
                                    </div>
                                    <asp:Label ID="lblFiltroPerfil" runat="server" Text="Perfil" class="control-label col-md-1 col-sm-1 col-xs-12"></asp:Label>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <asp:DropDownList ID="ddlFiltroPerfil" runat="server" class="form-control">
                                        </asp:DropDownList>
                                    </div>                                     
                                </div> 
                                <button type="submit" id="btnBuscar" runat="server" causesvalidation="False" class="btn btn-round btn-info pull-right" onserverclick="btnBuscar_Click">
                                    <i class="fa fa-search"></i> Buscar
                                </button>

                            </div>
                            <%--GRILLA--%>
                            <asp:GridView ID="grdGrilla" runat="server" OnPageIndexChanging="grdGrilla_PageIndexChanging"
                                OnSorting="grdGrilla_Sorting" OnRowCommand="grdGrilla_RowCommand"  AllowSorting ="true" 
                                AutoGenerateColumns="False" CssClass="table table-striped table-bordered">
                                <Columns>
                                    <asp:BoundField DataField="Id" HeaderText="ID" SortExpression="Id" Visible="True" />
                                    <asp:BoundField DataField="UserName" HeaderText="Usuario" SortExpression="UserName" />
                                    <asp:BoundField DataField="Apellido" HeaderText="Apellido" SortExpression="Apellido" />
                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre" SortExpression="Nombre" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <div class="pull-right">
                                                <asp:LinkButton ID="btnEditar" runat="server" CommandName="Editar" CommandArgument='<%#Eval("Id") %>' 
                                                    CssClass="btn btn-round"> <i class="fa fa-edit"></i> Editar</asp:LinkButton>
                                            </div>
                                        </ItemTemplate>
                                        <ItemStyle Width="10%" />
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle CssClass="pgr" />
                            </asp:GridView>
                            <br />
                            <div class="alert alert-success alert-dismissible fade in" role="alert">
                                <div class="pull-left">
                                    Total de Registros:
                                    <asp:Label ID="lblCantidadFilas" runat="server"></asp:Label>
                                </div>
                                
                                <br /> 
                                <br /> 
                            </div>  
                        </div>
                    </div>
            </div>
            </div>
        </div>
    </asp:Panel>

    <%--Vista Detalles--%>
    <asp:Panel ID="pnlDetalles" runat="server">
        <div class="col-md-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><asp:Literal ID="litNombre" runat="server" Text='<%#TituloForm%>' /></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <div class="form-horizontal form-label-left">
                        <div class="form-group">
                            <asp:Label ID="lblIdTitu" Text="ID" runat="server" Visible="true" class="control-label col-md-3 col-sm-3 col-xs-12">Id:</asp:Label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <asp:Label ID="lblId" runat="server" Visible="true" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" Text="Usuario" class="control-label col-md-3 col-sm-3 col-xs-12"></asp:Label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <asp:TextBox ID="txtUsuario" runat="server" Placeholder="Nick de Usuario" class="form-control">
                                </asp:TextBox>
                                <asp:Label Text="" runat="server" ID="lvlValUsuario" CssClass="help-inline" />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" Text="Nombre" class="control-label col-md-3 col-sm-3 col-xs-12"></asp:Label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <asp:TextBox ID="txtNombre" runat="server" Placeholder="Nombre del Usuario" class="form-control">
                                </asp:TextBox>
                                <asp:Label Text="" runat="server" ID="lblValNombre" CssClass="help-inline" />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" Text="Apellido" class="control-label col-md-3 col-sm-3 col-xs-12"></asp:Label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <asp:TextBox ID="txtApellido" runat="server" Placeholder="Apellido del Usuario" class="form-control">
                                </asp:TextBox>
                                <asp:Label Text="" runat="server" ID="lblValApellido" CssClass="help-inline" />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" Text="Email" class="control-label col-md-3 col-sm-3 col-xs-12"></asp:Label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <asp:TextBox ID="txtEmail" runat="server" Placeholder="Email del Usuario" class="form-control">
                                </asp:TextBox>
                                <asp:Label Text="" runat="server" ID="lblValEmail" CssClass="help-inline" />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" Text="Perfil" class="control-label col-md-3 col-sm-3 col-xs-12"></asp:Label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <asp:DropDownList ID="ddlPerfil" runat="server" class="form-control">
                                    <asp:ListItem Value="-1">Seleccionar Perfil</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label Text="" runat="server" ID="lblValPerfil" CssClass="help-inline" />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" Text="Activo" class="control-label col-md-3 col-sm-3 col-xs-12"></asp:Label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <asp:CheckBox ID="chbxActivo" runat="server" class="form-control">
                                </asp:CheckBox>
                            </div>
                        </div>
                        <uc1:usrUltimaModificacion ID="usrUltimaModificacion1" runat="server" />
                        <div class="pull-right">
                            <button type="submit" id="btnGrabar" runat="server" causesvalidation="False" class="btn btn-round btn-primary" onserverclick="btnGrabar_Click">
                                <i class="fa fa-check"></i> Grabar
                            </button>
                            <button type="submit" id="btnResetear" runat="server" causesvalidation="False" class="btn btn-round btn-danger" onserverclick="btnResetear_Click">
                                <i class="fa fa-refresh"></i> Resetear Password
                            </button>
                            <button type="submit" id="btnSalir" runat="server" causesvalidation="False" class="btn btn-round btn-alert" onserverclick="btnSalir_Click">
                                <i class="fa fa-arrow-circle-left"></i> Volver
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <usrError:Error ID="errorMensaje" runat="server" />
    <uc1:userProcesando runat="server" ID="userProcesando" />

</asp:Content>
