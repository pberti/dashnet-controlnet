﻿
using System;

public partial class _Default : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CargoPagina();
    }

    void CargoPagina()
    {
        if (base.IdUsuarioLogueado  == -1)
            Response.Redirect("~/Application/Login/Default.aspx");
    }
}
