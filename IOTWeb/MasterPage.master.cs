﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IOT.Services;
using System.Web.Security;
using IOT.ORM;
using IOT.Domain;
using System.Collections.Specialized;


public partial class MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CargoMenu();
        }
    }
    
    #region Carga Los Repetear del Menu
    private List<Menues> CargoRepeaterMenu(long mMenuPadre)
    {
        var lstMenues = new List<Menues>();
        //Creo el Menu de Inicio
        var oMemUser = Membership.GetUser();
        if (oMemUser != null)
        {
            pnlPerfil.Visible = true;
            ServUsuario oServUsu = Services.Get<ServUsuario>();
            
            //Cargo los datos del usuario
            var oUsuario = oServUsu.TraerUnicoUsuario(Convert.ToInt64(oMemUser.ProviderUserKey));
            litUsuarioAyN.Text = oUsuario.ApellidoYNombre2;
            string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/";
            imgUsuario.ImageUrl = baseUrl + oUsuario.URLImagenUsuario;

            var auxTipoPerfil = oUsuario.oTipoPerfil;
            //var lstTipoPerfilesMenues = auxTipoPerfil.listTipoPerfilMenues.Where(p => p.oMenu.IdMenuPadre == 1).OrderBy(p => p.oMenu.Posicion);
            var lstTipoPerfilesMenues = auxTipoPerfil.listTipoPerfilMenues.Where(p => p.oMenu.IdMenuPadre == mMenuPadre).OrderBy(p => p.oMenu.Posicion);

            foreach (var xMenu in lstTipoPerfilesMenues)
            {
                if (xMenu.oMenu.Id != 1)
                {
                    Menues oMenu = new Menues();
                    oMenu.Id = xMenu.oMenu.Id;
                    oMenu.Nombre = xMenu.oMenu.Nombre;
                    oMenu.PathPagina = baseUrl + "Application/" + xMenu.oMenu.PathPagina;
                    lstMenues.Add(oMenu);
                }

            }
        }
        else
        {
            pnlPerfil.Visible = false;
        }
        return lstMenues;
    }

    private void CargoMenu()
    {
        ParentRepeater.DataSource = CargoRepeaterMenu(1);
        ParentRepeater.DataBind();
    }
    protected void ParentRepeater_ItemDataBound(object sender, RepeaterItemEventArgs args)
    {
        if (args.Item.ItemType == ListItemType.Item || args.Item.ItemType == ListItemType.AlternatingItem)
        {
            Repeater childRepeater = (Repeater)args.Item.FindControl("ChildRepeater");
            Menues oMenu = (Menues)args.Item.DataItem;
            childRepeater.DataSource = CargoRepeaterMenu(oMenu.Id);
            childRepeater.DataBind();
        }
    }

    #endregion
}
