﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="userProcesando.ascx.cs" Inherits="UserControl_userProcesando" %>

<div id="mdlProcesando" class="modal fade" role="dialog">
    <asp:UpdatePanel runat="server" ID="upProcesando">
        <ContentTemplate>
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4>Procesando...</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped active" role="progressbar" style="width: 100%">
                                </div>
                            </div>
                        </div>
                    </div>
               </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
