﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Error.ascx.cs" Inherits="UserControl_Error" %>
<script type="text/javascript">
    function LoadModalError() {
        $("#myModal").modal();
    }
</script>
 

<div id="myModal" class="modal hide fade alert alert-block" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" >×</button>
    <h3 id="myModalLabel">Alerta!</h3>
  </div>
  <div class="modal-body">
    <p><i class="icon-warning-sign"></i> Se ha producido un error, intente nuevamente, o actualice la página, en caso de que el problema persista, consulte con el Administrador.</p>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Aceptar</button>
  </div>
</div>