﻿using IOT.Domain;
using IOT.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControl_ucAltaoBaja : System.Web.UI.UserControl
{

    private string SV_ucB_MetodoMiembro = "SV_ucB_MetodoMiembro";
    private string SV_ucB_Accion = "SV_ucB_Baja";
    private string MensajeError = "Ocurrió un ERROR al intentar dar la Baja. Por favor inténtelo nuevamente.";
    private string MensajeBaja = "Dado de Baja desde ABM Formularios.";


    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void Configurar(string pMetodoMiembro, bool pBaja)
    {
        //Registramos la Variables de Sesión y sus respectivos valores.
        //Se debe asegurar que cada vez que el UserControl sea instanciado el this.ID debe ser diferente
        pnlInactivo.Visible = pBaja;
        pnlAtencioBaja.Visible = pBaja;
        pnlAtencioAlta.Visible = !pBaja;
        HttpContext.Current.Session[this.ID + SV_ucB_Accion] = pBaja;
        HttpContext.Current.Session[this.ID + SV_ucB_MetodoMiembro] = pMetodoMiembro;
    }

    public void SetearMensajeAdvertencia(string pMensajeAdvertencia)
    {
        lblMensajeAdvertencia.Text = pMensajeAdvertencia;
        txtMotivoInactivo.Text = string.Empty;
        ScriptManager.RegisterStartupScript(upMdlBaja, upMdlBaja.GetType(), "Abrir Modal", "AbrirModal('mdlBaja');", true);
    }

    protected void lbnConfirmarBaja_Click(object sender, EventArgs e)
    {
        EjecutarAccion();
    }

    private void EjecutarAccion()
    {
        try
        {
            var svAccion = HttpContext.Current.Session[this.ID + SV_ucB_Accion];
            var svMetodoMiembro = HttpContext.Current.Session[this.ID + SV_ucB_MetodoMiembro];
            // si lo hubiera, invocamos al método miembro.
            if (svMetodoMiembro != null)
            {
                string MetodoMiembro = svMetodoMiembro.ToString();
                bool esBaja = bool.Parse(svAccion.ToString());
                if (MetodoMiembro != string.Empty)
                {
                    this.Page.GetType().InvokeMember(
                        MetodoMiembro,
                        System.Reflection.BindingFlags.InvokeMethod,
                        null,
                        this.Page,
                        new object[] { esBaja, txtMotivoInactivo.Text });
                }
                ScriptManager.RegisterStartupScript(upMdlBaja, upMdlBaja.GetType(), "Cerrar Modal", "CerrarModal('mdlBaja');", true);
            }
            else
            {
                lblMensajeAdvertencia.Text = "Ocurrió un ERROR al intentar dar la Baja. Por favor inténtelo nuevamente.";
            }
        }
        catch (Exception ex)
        {
            lblMensajeAdvertencia.Text = "Ocurrió un ERROR al intentar dar la Baja. Por favor inténtelo nuevamente.";
        }
        LimpiarSVs();
    }

    public void LimpiarSVs()
    {
        Session.Remove(this.ID + SV_ucB_MetodoMiembro);
        Session.Remove(this.ID + SV_ucB_Accion);
    }

}