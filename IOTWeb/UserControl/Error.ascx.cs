﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Text;
using System.IO;

public partial class UserControl_Error : System.Web.UI.UserControl
{

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    private string path
    {
        get
        {
            var context = HttpContext.Current;
            string AppPath = string.Format("{0}", context.Request.PhysicalApplicationPath);
            return AppPath;
        }
    }


    public void ErrorBind(Exception ex, string pMetodoFront, string pPagina)
    {
        StringBuilder sbError = new StringBuilder();
        sbError.AppendFormat("Error en el metodo: {0}, pagina: {1} ", pMetodoFront, pPagina);
        Logger.Log.Debug(sbError.ToString(), ex);
        string script = "LoadModalError();";
        ScriptManager.RegisterStartupScript(this, typeof(Page), "LoadModalError", script, true);
    }
}