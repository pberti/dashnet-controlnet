﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAltaOBaja.ascx.cs" Inherits="UserControl_ucAltaoBaja" %>

<div class="modal fade bs-example-modal-sm"  id="mdlBaja" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <asp:UpdatePanel runat="server" ID="upMdlBaja">
        <ContentTemplate>
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="clearfix">
                            <asp:Panel runat="server" ID="pnlAtencioBaja" CssClass="alert alert-danger" role="alert">
                                <h4 class="modal-title">
                                    <asp:Label runat="server" ID="lblTitulo">ATENCIÓN!!!</asp:Label>
                                </h4>
                            </asp:Panel>
                            <asp:Panel runat="server"  ID="pnlAtencioAlta"  CssClass="alert alert-success" role="alert">
                                <h4 class="modal-title">
                                    <asp:Label runat="server" ID="Label1">ATENCIÓN!!!</asp:Label>
                                </h4>
                            </asp:Panel>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="clearfix">
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <b><asp:Label runat="server" ID="lblMensajeAdvertencia"></asp:Label></b>
                                </div>
                            </div>
                            <br />
                            <asp:Panel runat="server" ID="pnlInactivo">
                                <div class="form-group">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label runat="server" Text="Motivo"></asp:Label>
                                        <asp:TextBox runat="server" ID="txtMotivoInactivo" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div></div>
                        <div class="pull-rigth">
                            <asp:LinkButton runat="server" ID="lbnConfirmarBaja" class="btn btn-round btn-primary" OnClick="lbnConfirmarBaja_Click">
                                <i class="fa fa-check"></i> Confirmar
                            </asp:LinkButton>
                            <asp:LinkButton runat="server" ID="lbtnCancelar" class="btn btn-round btn-default" data-dismiss="modal">
                                <i class="fa fa-trash"></i> Cancelar
                            </asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
