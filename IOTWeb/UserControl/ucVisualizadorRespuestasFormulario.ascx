﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucVisualizadorRespuestasFormulario.ascx.cs" Inherits="UserControl_ucVisualizadorRespuestasFormulario" %>
<asp:UpdatePanel runat="server" ID="upFormulario">
    <ContentTemplate>
        <asp:Panel runat="server" ID="pnlRespuestas" Visible="false">
            <div class="row">
                <div class="col-md-2 col-sm-1 col-xs-12"></div>
                <div class="col-md-8 col-sm-9 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <div class="clearfix">
                                <h2><b>
                                    <asp:Literal runat="server" ID="litTituloFormulario"></asp:Literal></b></h2>
                            </div>
                        </div>
                        <div class="x_content">
                            <asp:Repeater runat="server" ID="rptFormRespuestas" OnItemCommand="rptFormRespuestas_ItemCommand">
                                <ItemTemplate>
                                    <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
                                        <div class="panel">
                                            <a class="panel-heading collapsed" role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" href='<%#"#" + Eval("Id") + "Collapse" %>' aria-expanded="false" aria-controls='<%#Eval("Id") + "Collapse" %>'>
                                                <div class="clearfix">
                                                    <h4 class="panel-title">
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            Respuesta N° <%# Container.ItemIndex + 1 %>: 
                                                            <asp:Literal runat="server" ID="litUsuario" Text='<%#((IOT.Domain.Usuario)Eval("oUsuarioRespondio")).ApellidoYNombre2 %>'></asp:Literal>
                                                            <asp:LinkButton visible="false" runat="server" CommandName="Editar Respuesta" CommandArgument='<%#Eval("Id")%>'></asp:LinkButton>
                                                        </div>
                                                    </h4>
                                                </div>
                                            </a>
                                            <div id='<%#Eval("Id") + "Collapse" %>' class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                                <div class="panel-body">
                                                    <asp:Repeater runat="server" DataSource='<%#Eval("lRespuestas") %>' OnItemDataBound="rptFormRespuestas_ItemDataBound">
                                                        <ItemTemplate>
                                                            <asp:Panel runat="server" ID="pnlFormRespuestaCampo" CssClass="col-md-12 col-sm-12 col-xs-12">
                                                                <div class="panel panel-default">
                                                                    <div class="panel-body">
                                                                        <h4>
                                                                            <asp:Label runat="server" Text='<%#Eval("oFormCampo.Pregunta") %>'></asp:Label>
                                                                            <div class="ln_solid"></div>
                                                                        </h4>
                                                                        <asp:Panel runat="server" Visible='<%#Eval("TipoCampo").ToString() == IOT.Domain.ValoresConstantes.OptFormCampo_Parrafo ||
                                                                            Eval("TipoCampo").ToString() == IOT.Domain.ValoresConstantes.OptFormCampo_TextoCorto %>'>
                                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                <asp:Literal runat="server" ID="litTexto" Text='<%#Eval("Respuesta") %>'></asp:Literal>
                                                                            </div>
                                                                        </asp:Panel>
                                                                        <asp:Panel runat="server" Visible='<%#Eval("TipoCampo").ToString() != IOT.Domain.ValoresConstantes.OptFormCampo_Parrafo &&
                                                                            Eval("TipoCampo").ToString() != IOT.Domain.ValoresConstantes.OptFormCampo_TextoCorto %>'>
                                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                <asp:ListView runat="server" ID="lvSelecciones" DataSource='<%#Eval("lSelecciones") %>'>
                                                                                    <ItemTemplate>
                                                                                        <li>
                                                                                            <asp:Label id="lblRespueta" runat="server" Text='<%#Container.DataItem.ToString()%>'></asp:Label></li>
                                                                                    </ItemTemplate>
                                                                                </asp:ListView>
                                                                            </div>
                                                                        </asp:Panel>
                                                                        <asp:Panel runat="server" Visible='<%#Eval("oFormCampo.ConComentarioAdicional") %>'>
                                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                <div class="ln_solid"></div>
                                                                                <i><asp:Literal runat="server" Text='<%#Eval("ComentarioAdicional") %>'></asp:Literal></i>
                                                                            </div>
                                                                        </asp:Panel>
                                                                    </div>
                                                                </div>
                                                            </asp:Panel>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="pull-right">
                                        <asp:LinkButton runat="server" ID="lbtnListo" CssClass="btn btn-round btn-default" OnClick="lbtnListo_Click">
                                            <i class="fa fa-check"></i> Listo
                                        </asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
