﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Linq.Dynamic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IOT.ORM;
using IOT.Domain;
//using System.Transactions;
using System.Web.Security;
using System.Data;
using IOT.Services;

public partial class UserControl_usrMensaje : System.Web.UI.UserControl
{
    
    public int IdMensaje
    {
        get { return Convert.ToInt32(Session["IdMensaje"]); }
        set { Session["IdMensaje"] = value; }
    }
    

    public string Mensaje
    {
        get { return Session["Mensaje"].ToString(); }
        set { Session["Mensaje"] = value; }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
    }


    public void CargarMensaje(string pMensaje)
    {
        Mensaje = pMensaje;
        lblMensaje.Text = Mensaje;
    }


    public void MensajeBind()
    {
        string script = "MostrarMensaje();";
        ScriptManager.RegisterStartupScript(this, typeof(Page), "MostrarMensaje", script, true);  
    }
}