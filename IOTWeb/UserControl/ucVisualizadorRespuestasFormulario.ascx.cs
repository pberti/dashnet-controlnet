﻿using IOT.Domain;
using IOT.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControl_ucVisualizadorRespuestasFormulario : System.Web.UI.UserControl
{

    private string SV_ucVRF_MetodoMiembro = "SV_ucVRF_MetodoMiembro";
    private FormRespuesta oFormRespuesta = null;

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void CargarRespuetasSegunIdFormRespuesta(long IdFormRespuesta, string pMetodoMiembro)
    {
        oFormRespuesta = Services.Get<ServFormRespuesta>().TraerXId(IdFormRespuesta);
        CargarRespuestasSegunIdFormulario(oFormRespuesta.IdFormulario, pMetodoMiembro);
    }

    public void CargarRespuestasCampo(long IdFormRespuesta, string pMetodoMiembro)
    {
        try
        {
            oFormRespuesta = Services.Get<ServFormRespuesta>().TraerXId(IdFormRespuesta);
            HttpContext.Current.Session[this.ID + SV_ucVRF_MetodoMiembro] = pMetodoMiembro;
            pnlRespuestas.Visible = true;

            List<FormRespuestaCampo> lFormRespuestas = Services.Get<ServFormRespuestaCampo>().TraerTodos(true).Where(fr => fr.IdFormRespuesta == oFormRespuesta.Id).ToList();
            litTituloFormulario.Text = oFormRespuesta.oFormulario.Titulo.ToUpper();
            rptFormRespuestas.DataSource = lFormRespuestas;
            rptFormRespuestas.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception("Error en el mpetodo CargarRespuestasSegunIdFormulario", ex);
        }
    }
    public void CargarRespuestasSegunIdFormulario(long pIdFormulario, string pMetodoMiembro)
    {
        try
        {
            HttpContext.Current.Session[this.ID + SV_ucVRF_MetodoMiembro] = pMetodoMiembro;
            pnlRespuestas.Visible = true;
            Formulario oFormulario = Services.Get<ServFormulario>().TraerXId(pIdFormulario);
            List<FormRespuesta> lFormRespuestas = oFormRespuesta != null ?
                new List<FormRespuesta>() { oFormRespuesta } :
                Services.Get<ServFormRespuesta>().TraerTodos(true).Where(fr => fr.IdFormulario == oFormulario.Id).ToList();
            litTituloFormulario.Text = oFormulario.Titulo.ToUpper();
            rptFormRespuestas.DataSource = lFormRespuestas;
            rptFormRespuestas.DataBind();
        }
        catch(Exception ex)
        {
            throw new Exception("Error en el mpetodo CargarRespuestasSegunIdFormulario", ex);
        }
    }


    protected void lbtnListo_Click(object sender, EventArgs e)
    {
        pnlRespuestas.Visible = false;
        var svMetodoMiembro = HttpContext.Current.Session[this.ID + SV_ucVRF_MetodoMiembro];
        // si lo hubiera, invocamos al método miembro.
        if (svMetodoMiembro != null)
        {
            string MetodoMiembro = svMetodoMiembro.ToString();
            if (MetodoMiembro != string.Empty)
                this.Page.GetType().InvokeMember(
                    MetodoMiembro,
                    System.Reflection.BindingFlags.InvokeMethod,
                    null,
                    this.Page,
                    new object[] { });
        }
        Session.Remove(this.ID + SV_ucVRF_MetodoMiembro);
    }

    protected void rptFormRespuestas_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "Editar Respuesta")
        {

        }
    }

    protected void rptFormRespuestas_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

    }
}