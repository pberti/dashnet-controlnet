﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControl_ucPNotify : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void MostrarNotificacion(string pTitulo, string pMensaje)
    {
        try
        {
            ScriptManager.RegisterStartupScript(
                upPNotifySuccess,
                upPNotifySuccess.GetType(),
                "Abrir PNotify", "AbrirNotificacion('" + pTitulo + "', '" + pMensaje + "','success');",
                true);
        }
        catch (Exception ex)
        {
            throw new Exception();
        }
    }

}