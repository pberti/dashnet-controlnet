﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucRespuestaFormulario.ascx.cs" Inherits="UserControl_ucRespuestaFormulario" %>
<script>
    function AlertarValidacionNoExitosa() {
        if (Validar("fieldToValidate")) return true;
        else {
            AbrirModal('mdlMensaje');
            return false;
        }
    }
</script>
<asp:UpdatePanel runat="server" ID="upFormulario">
    <ContentTemplate>
        <asp:Panel runat="server" ID="pnlFormulario" Visible="false" DefaultButton="lbtnGrabarRespuesta">
            <div class="row">
                <div class="col-md-2 col-sm-1 col-xs-12"></div>
                <div class="col-md-8 col-sm-9 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <div class="clearfix">
                                <h2><b><asp:Literal runat="server" ID="litTituloFormulario"></asp:Literal></b></h2>
                                <asp:Literal runat="server" Visible="false" ID="litIdFormRespuesta"></asp:Literal>
                                <asp:Literal runat="server" Visible="false" ID="litIdFormulario"></asp:Literal>
                            </div>
                        </div>
                        <div class="x_content">
                            <asp:Repeater runat="server" ID="rptFormCampos" OnItemDataBound="rptFormCampos_ItemDataBound">
                                <ItemTemplate>
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="x_title">
                                                <div class="clearfix">
                                                    <asp:Literal runat="server" Visible="false" ID="litIdFormCampo" Text='<%#Eval("Id") %>'></asp:Literal>
                                                    <h2>
                                                        <asp:Literal runat="server" ID="litPregunta" Text='<%#Eval("Pregunta") %>'></asp:Literal></h2>
                                                    <div class="pull-right">
                                                        <asp:Panel runat="server" ID="pnlREquerido" Visible='<%#Eval("EsRequerido")%>'>
                                                            <span style="color: red;"><b>OBLIGATORIO</b></span>
                                                        </asp:Panel>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="x_content">
                                                <div class="clearfix">
                                                    <asp:Panel runat="server" ID="pnlFormCampo" CssClass="col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Literal runat="server" ID="litID" Text='<%#Eval("Id") %>' Visible="false"></asp:Literal>
                                                        <asp:Panel runat="server" Visible='<%#Eval("TipoCampo").ToString() == IOT.Domain.ValoresConstantes.OptFormCampo_Parrafo%>'>
                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                <div class="field">
                                                                    <asp:TextBox runat="server" ID="txtParrafo" Text='<%#Eval("OpcionesRespuesta") %>'
                                                                        CssClass='<%#bool.Parse(Eval("EsRequerido").ToString()) == true ? "form-control fieldToValidate" : "form-control"%>' TextMode="MultiLine" Rows="5"
                                                                        required='<%#Eval("EsRequerido") %>'
                                                                        placeholder="(Hasta 3000 caracteres)" data-validate-length-range="0,3000"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </asp:Panel>
                                                        <asp:Panel runat="server" Visible='<%#Eval("TipoCampo").ToString() == IOT.Domain.ValoresConstantes.OptFormCampo_TextoCorto%>'>
                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                <div class="field">
                                                                    <asp:TextBox runat="server" ID="txtTextoCorto" Text='<%#Eval("OpcionesRespuesta") %>'
                                                                        CssClass='<%#bool.Parse(Eval("EsRequerido").ToString()) == true ? "form-control fieldToValidate" : "form-control"%>'
                                                                        required='<%#Eval("EsRequerido") %>'
                                                                        placeholder="(Hasta 1000 caracteres)" data-validate-length-range="0,1000"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </asp:Panel>
                                                        <asp:Panel runat="server" Visible='<%#Eval("TipoCampo").ToString() == IOT.Domain.ValoresConstantes.OptFormCampo_SeleccionMultiple%>'>
                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                <div class="field">
                                                                    <asp:CheckBoxList runat="server" ID="chbxlSeleccionMultiple"></asp:CheckBoxList>
                                                                </div>
                                                            </div>
                                                        </asp:Panel>
                                                        <asp:Panel runat="server" Visible='<%#Eval("TipoCampo").ToString() == IOT.Domain.ValoresConstantes.OptFormCampo_SeleccionUnica%>'>
                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                <asp:RadioButtonList runat="server" ID="rbtnlSeleccionUnica"></asp:RadioButtonList>
                                                            </div>
                                                        </asp:Panel>
                                                        <asp:Panel runat="server" Visible='<%#Eval("TipoCampo").ToString() == IOT.Domain.ValoresConstantes.OptFormCampo_Valoracion%>'>
                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                <asp:DropDownList runat="server" ID="ddlValoracion" CssClass="form-control"></asp:DropDownList>
                                                            </div>
                                                        </asp:Panel>
                                                        <asp:Panel runat="server" Visible='<%#Eval("TipoCampo").ToString() == IOT.Domain.ValoresConstantes.OptFormCampo_VerdaderoFalso%>'>
                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                <asp:RadioButtonList runat="server" ID="rbtnlVerdaderoFalso"></asp:RadioButtonList>
                                                            </div>
                                                        </asp:Panel>
                                                        <asp:Panel runat="server" Visible='<%#bool.Parse(Eval("ConComentarioAdicional").ToString())%>'>
                                                            <div class="form-group">
                                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="ln_solid"></div>
                                                                    <asp:Label runat="server" Text="Comentario Adicional"></asp:Label>
                                                                </div>
                                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="field">
                                                                        <asp:TextBox runat="server" ID="txtComentarioAdicional" TextMode="MultiLine" Rows="2" CssClass="form-control optional"
                                                                            placeholder="(Hasta 3000 caracteres)" data-validate-length-range="0,3000"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </asp:Panel>
                                                    </asp:Panel>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                            <div class="panel panel-primary">
                                <div class="panel-body">
                                    <div class="pull-right">
                                        <asp:LinkButton runat="server" ID="lbtnCancelar" CssClass="btn btn-round btn-default" OnClientClick="AbrirModal('mdlCancelar')">
                                            <i class="fa fa-trash"></i> Cancelar
                                        </asp:LinkButton>
                                        <asp:LinkButton runat="server" ID="lbtnGrabarRespuesta" CssClass="btn btn-round btn-primary" OnClientClick="return AlertarValidacionNoExitosa();" OnClick="lbtnGrabarRespuesta_Click">
                                            <i class="fa fa-check"></i> Grabar Respuesta
                                        </asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>

<div class="modal fade" id="mdlCancelar" tabindex="-1" role="dialog" aria-hidden="true">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Cancelación de Respuestas de Formulario</h4>
                    </div>
                    <div class="modal-body">
                        <p>¿Realmente desea cancelar las respuestas?</p>
                    </div>
                    <div class="modal-footer">
                        <asp:LinkButton runat="server" ID="lbtnConfirmarCancelacion" CssClass="btn btn-round btn-default" OnClick="lbtnConfirmarCancelacion_Click">
                            <i class="fa fa-trash"></i> Si, Cancelar!
                        </asp:LinkButton>
                        <asp:LinkButton runat="server" ID="lbtnContinuar" CssClass="btn btn-round btn-primary" data-dismiss="modal">
                            <i class="fa fa-check"></i> Continuar Formulario
                        </asp:LinkButton>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
<div class="modal fade" id="mdlMensaje" tabindex="-1" role="dialog" aria-hidden="true">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Campos incompletos!</h4>
                    </div>
                    <div class="modal-body">
                        <p>Por favor complete todos los campos OBLIGATORIOS.</p>
                    </div>
                    <div class="modal-footer">
                        <asp:LinkButton runat="server" CssClass="btn btn-round btn-primary" data-dismiss="modal">
                            <i class="fa fa-check"></i> OK!
                        </asp:LinkButton>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
