﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="usrUltimaModificacion.ascx.cs"
    Inherits="UserControl_usrUltimaModificacion" %>
<div class="alert alert-info ">
    <small>Fecha Ultima Modificación:<asp:Label id="fecModificacion" runat="server"></asp:Label>
        Usuario Modificó:<asp:Label id="usrModificacion" runat="server"></asp:Label>
    </small>
</div>
