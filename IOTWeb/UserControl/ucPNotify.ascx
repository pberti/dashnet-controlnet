﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucPNotify.ascx.cs" Inherits="UserControl_ucPNotify" %>

<script>
    function AbrirNotificacion(titulo, mensaje, tipo) {
        PNotify.prototype.options.delay = 2000;
        var pn = new PNotify({
            title: titulo,
            text: mensaje,
            type: tipo,
            styling: 'bootstrap3'
        });
    }
</script>

<asp:UpdatePanel runat="server" ID="upPNotifySuccess">
    <ContentTemplate>
    </ContentTemplate>
</asp:UpdatePanel>
