﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Linq.Dynamic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IOT.ORM;
using IOT.Domain;
//using System.Transactions;
using System.Web.Security;
using System.Data;
using IOT.Services;

public partial class UserControl_usrUltimaModificacion : System.Web.UI.UserControl
{
    public string FecUltimaModificacion { get; set; }
    public long IdUsuarioModifico { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void UltimaModificacionBind()
    {
        fecModificacion.Text = FecUltimaModificacion;
        ServUsuario Serv = Services.Get<ServUsuario>();
        Usuario Usr = new Usuario();
        Usr = Serv.TraerUnicoUsuario(IdUsuarioModifico);
        if (Usr != null)
        {
            usrModificacion.Text = string.Format("{0} {1}", Usr.Nombre, Usr.Apellido);
        }
    }
}