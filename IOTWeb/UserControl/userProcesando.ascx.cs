﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControl_userProcesando : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public string RegisterControl(string pControl)
    {
        string script = "$(document).ready(function () { $('[id*=" + pControl + "]').click(); });";
        return script;
    }

    public void CerrarModalProcesando()
    {
        ScriptManager.RegisterStartupScript(upProcesando, upProcesando.GetType(), "HideModal", "CerrarModal('mdlProcesando');", true);
    }
}