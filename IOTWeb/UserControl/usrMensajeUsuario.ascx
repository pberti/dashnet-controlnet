﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="usrMensajeUsuario.ascx.cs" Inherits="UserControl_usrMensaje" %>


 <script type="text/javascript">

     function MostrarMensaje() 
     {
         $("#IdModalMensajeUsuario").modal();
     }


 </script>

<!-- Modal -->
<div id="IdModalMensajeUsuario" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            ×</button>
        <h3 id="myModalLabel">
             Información</h3>
    </div>
    <div class="modal-body">
        <asp:Label ID="lblMensaje"  runat="server">
        </asp:Label> 
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">
            Aceptar</button>
    </div>
</div>
