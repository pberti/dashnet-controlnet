﻿using IOT.Domain;
using IOT.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class UserControl_ucRespuestaFormulario : System.Web.UI.UserControl
{

    private string SV_ucRF_MetodoMiembro = "SV_ucRF_MetodoMiembro";
    private FormRespuesta oFormRespuesta = null;

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void CargarFormulario(long pIdFormulario, long pIdFormRespuesta, string pMetodoMiembro)
    {
        try
        {
            HttpContext.Current.Session[this.ID + SV_ucRF_MetodoMiembro] = pMetodoMiembro;
            pnlFormulario.Visible = true;
            litIdFormulario.Text = pIdFormRespuesta.ToString();
            litIdFormRespuesta.Text = pIdFormRespuesta.ToString();
            oFormRespuesta = Services.Get<ServFormRespuesta>().TraerXId(pIdFormRespuesta);

            Formulario oFormulario = Services.Get<ServFormulario>().TraerXId(pIdFormulario);
            litTituloFormulario.Text = oFormulario.Titulo.ToUpper();
            List<FormCampo> lFormCampos = oFormulario.lFormCampos.Where(fc => fc.Inactivo == false).OrderBy(fc => fc.Posicion).ToList();
            rptFormCampos.DataSource = oFormulario.lFormCampos.Where(fc => fc.Inactivo == false).ToList();
            rptFormCampos.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception("Error en el mpetodo CargarFormulario", ex);
        }
    }

    protected void lbtnGrabarRespuesta_Click(object sender, EventArgs e)
    {
        pnlFormulario.Visible = false;
        var svMetodoMiembro = HttpContext.Current.Session[this.ID + SV_ucRF_MetodoMiembro];
        // si lo hubiera, invocamos al método miembro.
        if (svMetodoMiembro != null)
        {
            string MetodoMiembro = svMetodoMiembro.ToString();
            if (MetodoMiembro != string.Empty)
                this.Page.GetType().InvokeMember(
                    MetodoMiembro,
                    System.Reflection.BindingFlags.InvokeMethod,
                    null,
                    this.Page,
                    new object[] { false });
        }
        Session.Remove(this.ID + SV_ucRF_MetodoMiembro);
    }

    protected void lbtnConfirmarCancelacion_Click(object sender, EventArgs e)
    {
        pnlFormulario.Visible = false;
        var svMetodoMiembro = HttpContext.Current.Session[this.ID + SV_ucRF_MetodoMiembro];
        // si lo hubiera, invocamos al método miembro.
        if (svMetodoMiembro != null)
        {
            string MetodoMiembro = svMetodoMiembro.ToString();
            if (MetodoMiembro != string.Empty)
                this.Page.GetType().InvokeMember(
                    MetodoMiembro,
                    System.Reflection.BindingFlags.InvokeMethod,
                    null,
                    this.Page,
                    new object[] { true });
        }
        Session.Remove(this.ID + SV_ucRF_MetodoMiembro);
        ScriptManager.RegisterStartupScript(upFormulario, upFormulario.GetType(), "Cerrar Modal Cancelacion Formulario", "CerrarModal('mdlCancelar');", true);
    }

    public List<FormRespuestaCampo> ObtenerFormRespuestasCampos()
    {
        try
        {
            List<FormRespuestaCampo> lFormRespuestasCampos = new List<FormRespuestaCampo>();
            long IdFormRespuesta = long.Parse(litIdFormRespuesta.Text);
            if (IdFormRespuesta > 0) oFormRespuesta = Services.Get<ServFormRespuesta>().TraerXId(IdFormRespuesta);
            foreach (RepeaterItem rptI in rptFormCampos.Items)
            {
                var ctrlId = rptI.FindControl("litIdFormCampo");
                if (ctrlId != null)
                {
                    long idFormCampo = long.Parse(((Literal)ctrlId).Text);
                    FormRespuestaCampo oFormRespuestaCampo = null;
                    if (oFormRespuesta != null)
                        oFormRespuestaCampo = oFormRespuesta.lRespuestas.Find(frc => frc.IdFormCampo == idFormCampo);
                    if (oFormRespuestaCampo == null)
                        oFormRespuestaCampo = new FormRespuestaCampo();
                    FormCampo oFormCampo = Services.Get<ServFormCampo>().TraerXId(idFormCampo);

                    string ComentarioAdicional = string.Empty;
                    if (oFormCampo.ConComentarioAdicional)
                    {
                        TextBox txtComentarioAdicional = (TextBox)rptI.FindControl("txtComentarioAdicional");
                        oFormRespuestaCampo.ComentarioAdicional = txtComentarioAdicional.Text;
                    }

                    if (oFormCampo.TipoCampo == ValoresConstantes.OptFormCampo_Parrafo)
                    {
                        TextBox txtParrafo = (TextBox)rptI.FindControl("txtParrafo");
                        oFormRespuestaCampo.IdFormCampo = idFormCampo;
                        oFormRespuestaCampo.Respuesta = txtParrafo.Text;
                    }
                    else if (oFormCampo.TipoCampo == ValoresConstantes.OptFormCampo_TextoCorto)
                    {
                        TextBox txtTextoCorto = (TextBox)rptI.FindControl("txtTextoCorto");
                        oFormRespuestaCampo.IdFormCampo = idFormCampo;
                        oFormRespuestaCampo.Respuesta = txtTextoCorto.Text;
                    }
                    else if (oFormCampo.TipoCampo == ValoresConstantes.OptFormCampo_SeleccionMultiple)
                    {
                        CheckBoxList chbxlSeleccionMultiple = (CheckBoxList)rptI.FindControl("chbxlSeleccionMultiple");
                        List<ListItem> selectedItems = chbxlSeleccionMultiple.Items.Cast<ListItem>()
                            .Where(li => li.Selected)
                            .ToList();
                        oFormRespuestaCampo.IdFormCampo = idFormCampo;
                        oFormRespuestaCampo.Respuesta = string.Empty;
                        selectedItems.ForEach(si => oFormRespuestaCampo.Respuesta += si.Value + "\n");
                        oFormRespuestaCampo.Respuesta.TrimEnd('\r', '\n');
                    }
                    else if (oFormCampo.TipoCampo == ValoresConstantes.OptFormCampo_SeleccionUnica)
                    {
                        RadioButtonList rbtnlSeleccionUnica = (RadioButtonList)rptI.FindControl("rbtnlSeleccionUnica");
                        oFormRespuestaCampo.IdFormCampo = idFormCampo;
                        oFormRespuestaCampo.Respuesta = rbtnlSeleccionUnica.SelectedValue;
                    }
                    else if (oFormCampo.TipoCampo == ValoresConstantes.OptFormCampo_Valoracion)
                    {
                        DropDownList ddlValoracion = (DropDownList)rptI.FindControl("ddlValoracion");
                        oFormRespuestaCampo.IdFormCampo = idFormCampo;
                        oFormRespuestaCampo.Respuesta = ddlValoracion.SelectedValue;
                    }
                    else if (oFormCampo.TipoCampo == ValoresConstantes.OptFormCampo_VerdaderoFalso)
                    {
                        RadioButtonList rbtnlVerdaderoFalso = (RadioButtonList)rptI.FindControl("rbtnlVerdaderoFalso");
                        oFormRespuestaCampo.IdFormCampo = idFormCampo;
                        oFormRespuestaCampo.Respuesta = rbtnlVerdaderoFalso.SelectedValue;
                    }
                    lFormRespuestasCampos.Add(oFormRespuestaCampo);
                }
            }
            return lFormRespuestasCampos;
        }
        catch (Exception ex)
        {
            throw new Exception("Error en el método ObtenerFormRespuestasCampos", ex);
        }
    }

    protected void rptFormCampos_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            FormCampo oFormCampo = (FormCampo)e.Item.DataItem;
            var ctrlId = e.Item.FindControl("litIdFormCampo");
            Literal oLiteral = (Literal)ctrlId;
            long IdFormCampo = long.Parse(oLiteral.Text);
            FormRespuestaCampo oFormRespuestaCampo = null;
            if (oFormRespuesta != null)
                oFormRespuestaCampo = oFormRespuesta.lFormRespuestasCampos.ToList().Where(frc => frc.IdFormCampo == IdFormCampo).ToList().FirstOrDefault();
            if (oFormCampo.ConComentarioAdicional)
            {
                TextBox txtComentarioAdicional = (TextBox)e.Item.FindControl("txtComentarioAdicional");
                if (oFormRespuestaCampo != null)
                {
                    txtComentarioAdicional.Text = oFormRespuestaCampo.ComentarioAdicional;
                }
            }

            if (oFormCampo.TipoCampo == ValoresConstantes.OptFormCampo_Parrafo)
            {
                TextBox txtParrafo = (TextBox)e.Item.FindControl("txtParrafo");
                if (oFormRespuestaCampo != null)
                {
                    txtParrafo.Text = oFormRespuestaCampo.Respuesta;
                }
            }
            else if (oFormCampo.TipoCampo == ValoresConstantes.OptFormCampo_TextoCorto)
            {
                TextBox txtTextoCorto = (TextBox)e.Item.FindControl("txtTextoCorto");
                if (oFormRespuestaCampo != null)
                {
                    txtTextoCorto.Text = oFormRespuestaCampo.Respuesta;
                }
            }
            else if (oFormCampo.TipoCampo == ValoresConstantes.OptFormCampo_SeleccionMultiple)
            {
                CheckBoxList chbxlSeleccionMultiple = (CheckBoxList)e.Item.FindControl("chbxlSeleccionMultiple");
                chbxlSeleccionMultiple.DataSource = oFormCampo.lOpcionesRespuesta;
                chbxlSeleccionMultiple.DataBind();
                if (oFormRespuestaCampo != null)
                {
                    oFormRespuestaCampo.lSelecciones.ForEach(
                        slc =>
                        {
                            foreach (ListItem li in chbxlSeleccionMultiple.Items)
                            {
                                if (li.Text == slc) li.Selected = true;
                            }
                        }
                    );
                }
            }
            else if (oFormCampo.TipoCampo == ValoresConstantes.OptFormCampo_SeleccionUnica)
            {
                RadioButtonList rbtnlSeleccionUnica = (RadioButtonList)e.Item.FindControl("rbtnlSeleccionUnica");
                rbtnlSeleccionUnica.DataSource = oFormCampo.lOpcionesRespuesta;
                rbtnlSeleccionUnica.DataBind();
                if (oFormRespuestaCampo != null)
                {
                    oFormRespuestaCampo.lSelecciones.ForEach(
                        slc =>
                        {
                            foreach (ListItem li in rbtnlSeleccionUnica.Items)
                            {
                                if (li.Text == slc) li.Selected = true;
                            }
                        }
                    );
                }
            }
            else if (oFormCampo.TipoCampo == ValoresConstantes.OptFormCampo_Valoracion)
            {
                DropDownList ddlValoracion = (DropDownList)e.Item.FindControl("ddlValoracion");
                ddlValoracion.DataSource = oFormCampo.lOpcionesRespuesta;
                ddlValoracion.DataBind();
                if (oFormRespuestaCampo != null)
                {
                    oFormRespuestaCampo.lSelecciones.ForEach(
                        slc =>
                        {
                            foreach (ListItem li in ddlValoracion.Items)
                            {
                                if (li.Text == slc) li.Selected = true;
                            }
                        }
                    );
                }
            }
            else if (oFormCampo.TipoCampo == ValoresConstantes.OptFormCampo_VerdaderoFalso)
            {
                RadioButtonList rbtnlVerdaderoFalso = (RadioButtonList)e.Item.FindControl("rbtnlVerdaderoFalso");
                rbtnlVerdaderoFalso.DataSource = oFormCampo.lOpcionesRespuesta;
                rbtnlVerdaderoFalso.DataBind();
                if (oFormRespuestaCampo != null)
                {
                    oFormRespuestaCampo.lSelecciones.ForEach(
                        slc =>
                        {
                            foreach (ListItem li in rbtnlVerdaderoFalso.Items)
                            {
                                if (li.Text == slc) li.Selected = true;
                            }
                        }
                    );
                }
            }
        }
        catch (Exception ex)
        {

        }
    }

}