﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using IOT.ORM;
using IOT.Domain;
//using System.Transactions;
using System.Web.Security;
using IOT.Services;
using System.Collections;

public partial class Site : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CargarMenu();
            CargoMenuPersonal();
        }
    }

    private void CargoMenuPersonal()
    {
        var oMemUser = Membership.GetUser();
        if (oMemUser != null)
        {
            ServUsuario oServUsu = Services.Get<ServUsuario>();
            var oUsuario = oServUsu.TraerUnicoUsuario(Convert.ToInt64(oMemUser.ProviderUserKey));
            LoginName loginName = LoginView1.FindControl("HeadLoginName") as LoginName;
            if (loginName != null)
            {
                loginName.FormatString = oUsuario.Nombre + " " + oUsuario.Apellido;
            }
        }
    }

    //Generacion del Menu
    #region Generacion del Menu de la Intranet
    private void CargarMenu()
    {
        //Creo el Menu de Inicio
        MenuItem MenuRaizItem1 = new MenuItem("Inicio");
        MenuRaizItem1.Selectable = true;
        MenuRaizItem1.NavigateUrl = "~/Default.aspx";
        NavigationMenu.Items.Add(MenuRaizItem1);

        var oMemUser = Membership.GetUser();
        if (oMemUser != null)
        {
            ServUsuario oServUsu = Services.Get<ServUsuario>();
            var oUsuario = oServUsu.TraerUnicoUsuario(Convert.ToInt64(oMemUser.ProviderUserKey));

            Menu MenuAux = new Menu();

            var auxTipoPerfil = oUsuario.oTipoPerfil;
            var lstTipoPerfilesMenues = auxTipoPerfil.listTipoPerfilMenues.Where(p => p.oMenu.IdMenuPadre == 1).OrderBy(p => p.oMenu.Posicion);

            foreach (var xMenu in lstTipoPerfilesMenues)
            {
                
                if (xMenu.oMenu.Id != 1)
                {

                    // Anulo el Nro de Posicion en los Menues Padres
                    //MenuItem MenuRaizItem = new MenuItem(xMenu.oMenu.Posicion + ". " + xMenu.oMenu.Nombre, xMenu.oMenu.Nombre);
                    MenuItem MenuRaizItem = new MenuItem(xMenu.oMenu.Nombre, xMenu.oMenu.Nombre);
                    if (xMenu.oMenu.PathPagina.Trim().Length > 0)
                    {
                        MenuRaizItem.Selectable = true;
                        if (xMenu.oMenu.PathPagina.IndexOf("?") > 0)
                        {
                            MenuRaizItem.NavigateUrl = "~/Application/" + xMenu.oMenu.PathPagina + "";
                        }
                        else
                        {
                            MenuRaizItem.NavigateUrl = "~/Application/" + xMenu.oMenu.PathPagina + "/";
                        }
                    }
                    else
                    {
                        MenuRaizItem.Selectable = false;
                    }

                    NavigationMenu.Items.Add(MenuRaizItem);

                    //Busco los SubMenues de esta Categoria Raiz
                    var oMenuesHijos = auxTipoPerfil.listTipoPerfilMenues.Where(p => p.oMenu.IdMenuPadre == xMenu.oMenu.Id).OrderBy(p => p.oMenu.Posicion);
                    foreach (var oMenuHijo in oMenuesHijos)
                    {
                        MenuItem MenuHijoItem = new MenuItem();
                        if (oMenuHijo.oMenu.PathPagina == "")
                        {
                            //Creo el MenuItem Separador
                            MenuItem MenuSeparador = new MenuItem();
                            MenuSeparador.Selectable = false;
                            MenuSeparador.Enabled = true;
                            MenuSeparador.Text = "";
                            MenuSeparador.Value = "#";
                            string m1 = Server.MapPath(Request.AppRelativeCurrentExecutionFilePath);

                            MenuSeparador.NavigateUrl = "";
                            MenuRaizItem.ChildItems.Add(MenuSeparador);

                            //MenuHijoItem = new MenuItem(oMenuHijo.Posicion + ". " + oMenuHijo.Nombre, oMenuHijo.Nombre, "", oMenuHijo.PathPagina);
                            MenuHijoItem.Text = "  " + oMenuHijo.oMenu.Nombre;
                            MenuHijoItem.Value = oMenuHijo.oMenu.Nombre;
                            MenuHijoItem.NavigateUrl = "";

                            //Lo Pongo Disable
                            MenuHijoItem.Selectable = false;
                            MenuHijoItem.Enabled = true;

                        }
                        else
                        {
                            MenuHijoItem.Text = oMenuHijo.oMenu.Nombre;
                            MenuHijoItem.Value = oMenuHijo.oMenu.Nombre;
                            if (oMenuHijo.oMenu.PathPagina.IndexOf("?") > 0)
                            {
                                MenuHijoItem.NavigateUrl = "~/Application/" + oMenuHijo.oMenu.PathPagina + "";
                            }
                            else
                            {
                                MenuHijoItem.NavigateUrl = "~/Application/" + oMenuHijo.oMenu.PathPagina + "/";
                            }
                        }
                        MenuRaizItem.ChildItems.Add(MenuHijoItem);
                    }
                }
            }
        }
    }
    #endregion


    
}
