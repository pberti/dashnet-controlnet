﻿using IOT.Domain;
using FluentNHibernate.Mapping;

namespace IOT.Mapping
{
    public class MapSoporte_Estados : ClassMap<Soporte_Estados>
    {
        public MapSoporte_Estados()
        {
            base.Table("iot_soporte_estados");
            Id(c => c.Id).GeneratedBy.Identity();
            Map(c => c.Nombre);
            Map(c => c.NroOrden);

        }
    }
}
