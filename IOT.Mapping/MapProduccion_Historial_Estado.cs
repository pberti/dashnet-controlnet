﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IOT.Domain;
using FluentNHibernate.Mapping;

namespace IOT.Mapping
{
    public class MapProduccion_Historial_Estado: ClassMap<Produccion_Historial_Estado>
    {
        public MapProduccion_Historial_Estado()
        {
            base.Table("iot_produccion_historial_estado");
            Id(c => c.Id).GeneratedBy.Identity();
            Map(c => c.IdEstadoActual);
            Map(c => c.IdNuevoEstado);
            Map(c => c.IdProduccion);
            Map(c => c.IdUsuarioModifico);
            Map(c => c.FecUltimaModificacion);
            Map(c => c.Comentario);
            Map(c => c.IdFormulario);
            Map(c => c.IdFormrespuestas);
          
            References(o => o.oEstadoActual, "IdEstadoActual").ReadOnly();
            References(o => o.oProduccion, "IdProduccion").ReadOnly();
            References(o => o.oUsuario, "IdUsuarioModifico").ReadOnly();
            References(o => o.oNuevoEstado, "IdNuevoEstado").ReadOnly();
            References(o => o.oFormulario, "IdFormulario").ReadOnly();
            References(o => o.oFormrespuestas, "IdFormrespuestas").ReadOnly();

        }
    }
}
