﻿using FluentNHibernate.Mapping;
using IOT.Domain;

namespace IOT.Mapping
{
    public class MapTipoProducto : ClassMap<TipoProducto>
    {
        public MapTipoProducto()
        {
            Table("iot_tipoproducto");
            Id(c => c.Id).GeneratedBy.Identity();
            Map(c => c.Nombre);
            Map(c => c.Descripcion);
            Map(c => c.Inactivo);
            Map(c => c.InactivoFecha);
            Map(c => c.InactivoComentario);
            Map(c => c.IdUsuarioModifico);
            Map(c => c.FecUltimaModificacion);
            Map(c => c.CodigoProducto);
        }

    }
}
