﻿using IOT.Domain;
using FluentNHibernate.Mapping;

namespace IOT.Mapping
{
    public class MapClientes : ClassMap<Clientes>
    {
        public MapClientes()
        {
            base.Table("iot_clientes");
            Id(c => c.Id).GeneratedBy.Identity();
            Map(c => c.Nombre);
            Map(c => c.Contacto);
            Map(c => c.Correo);
            Map(c => c.Cuit);
            Map(c => c.Domicilio);
            Map(c => c.Provincia);
            Map(c => c.Telefono);
            Map(c => c.Comentario);
            Map(c => c.Inactivo);
            Map(c => c.InactivoFecha);
            Map(c => c.InactivoComentario);
            Map(c => c.IdUsuarioModifico);
            Map(c => c.FecUltimaModificacion);
        }
    }
}