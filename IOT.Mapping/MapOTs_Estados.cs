﻿using FluentNHibernate.Mapping;
using IOT.Domain;

namespace IOT.Mapping
{
    public class MapOTs_Estados : ClassMap<OTs_Estados>
    {
        public MapOTs_Estados()
        {
            Table("cnet_otdash_estados");
            Id(c => c.Id).GeneratedBy.Identity();
            Map(c => c.Estado_Actual);
            Map(c => c.Estado_Inicial);
            Map(c => c.Fecha_Creacion);
            Map(c => c.Id_Estado_Actual);
            Map(c => c.Id_Estado_Inicial);
            Map(c => c.Id_OTDash);
            Map(c => c.Id_Usuario);
            Map(c => c.Nro_Orden_Actual);
            Map(c => c.Nro_Orden_Inicial);
            Map(c => c.OT);
            Map(c => c.Usuario);
        }

    }
}
