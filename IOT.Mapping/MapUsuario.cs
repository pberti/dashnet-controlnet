﻿using FluentNHibernate.Mapping;
using IOT.Domain;

namespace IOT.Mapping
{
    public class MapUsuario: ClassMap<Usuario>
    {
        public MapUsuario()
        {
            base.Table("iot_usuarios");
            Id(c => c.Id).GeneratedBy.Identity();
            base.Map((Usuario c) => (object)c.Activo);
            base.Map((Usuario c) => c.Apellido);
            base.Map((Usuario c) => c.Email);
            base.Map((Usuario c) => c.Nombre);
            base.Map((Usuario c) => c.Password);
            base.Map((Usuario c) => c.UserName);
            base.Map((Usuario c) => (object)c.IdTipoPerfil);
            base.Map((Usuario c) => c.URLImagenUsuario);
            base.Map((Usuario c) => (object)c.IdUsuarioModifico);
            base.Map((Usuario c) => (object)c.FecUltimaModificacion);
            base.References<TipoPerfil>((Usuario o) => o.oTipoPerfil, "IdTipoPerfil").ReadOnly();
        }
    }
}
