﻿using FluentNHibernate.Mapping;
using IOT.Domain;

namespace IOT.Mapping
{
    public class MapProductoST : ClassMap<ProductoST>
    {
        public MapProductoST()
        {
            Table("iot_productost");
            Id(c => c.Id).GeneratedBy.Identity();
            Map(c => c.IdProduccion);
            Map(c => c.IdTipoProducto);
            Map(c => c.NroSerie);
            Map(c => c.NroGabinete);

            References(o => o.oProduccion, "IdProduccion").ReadOnly();
            References(o => o.oTipoProducto, "IdTipoProducto").ReadOnly();
        }

    }
}
