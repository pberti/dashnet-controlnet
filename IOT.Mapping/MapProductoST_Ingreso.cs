﻿using FluentNHibernate.Mapping;
using IOT.Domain;


namespace IOT.Mapping
{
    public class MapProductoST_Ingreso : ClassMap<ProductoST_Ingreso>
    {
        public MapProductoST_Ingreso()
        {
            Table("iot_productost_ingreso");
            Id(c => c.Id).GeneratedBy.Identity();
            Map(c => c.IdCliente);
            Map(c => c.ContactoEmail);
            Map(c => c.NroReclamo);
            Map(c => c.IdEstadoActual);
            Map(c => c.IdProductoST);
            Map(c => c.NroOrden);
            Map(c => c.NroRemito);
            Map(c => c.FecUltimaModificacion);
            Map(c => c.IdUsuarioModifico);
            Map(c => c.Observaciones);
            Map(c => c.FallaReportada);
            Map(c => c.IdWorkflow);
            Map(c => c.Garantia);
            Map(c => c.Laboratorio);

            References(o => o.oProductoST, "IdProductoST").ReadOnly();
            References(o => o.oEstadoActual, "IdEstadoActual").ReadOnly();
            References(o => o.oWorkflow, "IdWorkflow").ReadOnly();
            References(o => o.oCliente, "IdCliente").ReadOnly();
            References(o => o.oUsuario, "IdUsuarioModifico").ReadOnly();
        }

    }
}
