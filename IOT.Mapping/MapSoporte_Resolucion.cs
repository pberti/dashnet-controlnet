﻿using IOT.Domain;
using FluentNHibernate.Mapping;

namespace IOT.Mapping
{
    public class MapSoporte_Resolucion : ClassMap<Soporte_Resolucion>
    {
        public MapSoporte_Resolucion()
        {
            base.Table("iot_soporte_resolucion");
            Id(c => c.Id).GeneratedBy.Identity();
            Map(c => c.Nombre);
        }
    }
}
