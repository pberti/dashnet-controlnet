﻿using IOT.Domain;
using FluentNHibernate.Mapping;

namespace IOT.Mapping
{
    public class MapSoporte_Categoria : ClassMap<Soporte_Categoria>
    {
        public MapSoporte_Categoria()
        {
            base.Table("iot_soporte_categoria");
            Id(c => c.Id).GeneratedBy.Identity();
            Map(c => c.Nombre);
            Map(c => c.IdPadre);
        }
    }
}
