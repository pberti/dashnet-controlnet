﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IOT.Domain;
using FluentNHibernate.Mapping;

namespace IOT.Mapping
{
    public class MapTipoPerfil : ClassMap<TipoPerfil>
    {
        public MapTipoPerfil()
        {
            base.Table("iot_tipoperfil");
            Id(c => c.Id).GeneratedBy.Identity();
            Map(c => c.Descripcion);
            Map(c => c.Perfil);
            Map(c => c.IdUsuarioModifico);
            Map(c => c.FecUltimaModificacion);

            HasMany(o => o.listUsuarios).KeyColumns.Add("IdTipoPerfil").Inverse();
            HasMany(o => o.listTipoPerfilMenues).KeyColumns.Add("IdTipoPerfil").Inverse();
        }
    }
}
