﻿using IOT.Domain;
using FluentNHibernate.Mapping;

namespace IOT.Mapping
{
    public class MapSoporte_Historial : ClassMap<Soporte_Historial>
    {
        public MapSoporte_Historial()
        {
            base.Table("iot_soporte_historial");
            Id(c => c.Id).GeneratedBy.Identity();
            Map(c => c.IdSoporteCaso);
            Map(c => c.Comentario);
            Map(c => c.Fecha);
            Map(c => c.IdUsuario);
            Map(c => c.IdEstado);
            Map(c => c.IdResolucion);

            References(o => o.oUsuario, "IdUsuario").ReadOnly();
            References(o => o.oEstado, "IdEstado").ReadOnly();
            References(o => o.oResolucion, "IdResolucion").ReadOnly();
        }
    }
}
