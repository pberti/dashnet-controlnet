﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using IOT.Domain;

namespace IOT.Mapping
{
    public class MapWorkflow : ClassMap<Workflow>
    {
        public MapWorkflow()
        {
            base.Table("iot_workflow");
            Id(c => c.Id).GeneratedBy.Identity();
            Map(c => c.Nombre);
            Map(c => c.Descripcion);
            Map(c => c.FechaUltimaModificacion);
            Map(c => c.idUsuarioCreo);
            Map(c => c.Inactivo);
            Map(c => c.InactivoFecha);
            Map(c => c.InactivoComentario);
            Map(c => c.Destino);

            References(o => o.oUsuario, "IdusuarioCreo").ReadOnly();
        }
    }
}
