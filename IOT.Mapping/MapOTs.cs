﻿using FluentNHibernate.Mapping;
using IOT.Domain;
using System;

namespace IOT.Mapping
{
    public class MapOTs : ClassMap<OTs>
    {
        public MapOTs()
        {
            Table("cnet_otdash");
            Id(c => c.Id).GeneratedBy.Identity();
            Map((OTs c) => (object)c.Id_OT);
            Map((OTs c) => c.OT_Asunto);
            Map((OTs c) => c.OT_Horas_Estimadas);
            Map((OTs c) => (object)c.Id_Estado);
            Map((OTs c) => c.OT_Estado);
            Map((OTs c) => c.OT_Cerrada);
            Map((OTs c) => (object)c.OT_Fecha_Cierre);
            Map((OTs c) => c.Proyecto_Nombre);
            Map((OTs c) => c.Proyecto_CC);
            Map((OTs c) => c.Proyecto_Descipcion);
            Map((OTs c) => c.Proyecto_Padre_Nombre);
            Map((OTs c) => c.Version_Proyecto);
            Map((OTs c) => c.Version_Nombre);
            Map((OTs c) => (object)c.Version_Fecha_Inicio);
            Map((OTs c) => (object)c.Version_Fecha_Est_FiN);
            Map((OTs c) => (object)c.Version_Fecha_Real_FiN);
            Map((OTs c) => c.Version_Producto);
            Map((OTs c) => c.Version_Id);
        }
    }
}
