﻿using IOT.Domain;
using FluentNHibernate.Mapping;

namespace IOT.Mapping
{
    public class MapSTFalla : ClassMap<STFalla>
    {
        public MapSTFalla()
        {
            base.Table("iot_st_fallas");
            Id(c => c.Id).GeneratedBy.Identity();
            Map(c => c.Nombre);
            Map(c => c.Descripcion);
        }
    }
}
