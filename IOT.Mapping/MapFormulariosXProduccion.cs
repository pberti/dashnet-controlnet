﻿using FluentNHibernate.Mapping;
using IOT.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace IOT.Mapping
{
    class MapFormulariosXProduccion : ClassMap<FormulariosXProduccion>
    {
        public MapFormulariosXProduccion()
        {
            Table("iot_formulariosxproduccion");
            Id(c => c.Id).GeneratedBy.Identity();
            Map(c => c.IdFormulario);
            Map(c => c.IdProduccion);
            Map(c => c.IdFormRespuestas);
            Map(c => c.IdEstado);

            References(o => o.oFormulario, "IdFormulario").ReadOnly();
            References(o => o.oProduccion, "IdProduccion").ReadOnly();
            References(o => o.oRespuestas, "IdFormRespuestas").ReadOnly();
            References(o => o.oEstado, "IdEstado").ReadOnly();
        }
    }
}
