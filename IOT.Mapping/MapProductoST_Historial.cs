﻿using IOT.Domain;
using FluentNHibernate.Mapping;

namespace IOT.Mapping
{
    public class MapProductoST_Historial : ClassMap<ProductoST_Historial>
    {
        public MapProductoST_Historial()
        {
            base.Table("iot_productost_historial");
            Id(c => c.Id).GeneratedBy.Identity();
            Map(c => c.IdEstadoAnterior);
            Map(c => c.IdNuevoEstado);
            Map(c => c.IdProductoSTIngreso);
            Map(c => c.IdUsuarioModifico);
            Map(c => c.FecUltimaModificacion);
            Map(c => c.Comentario);
            Map(c => c.IdFormulario);
            Map(c => c.IdFormrespuestas);
            Map(c => c.Diferencia);
            References(o => o.oEstadoAnterior, "IdEstadoAnterior").ReadOnly();
            References(o => o.oProductoSTIngreso, "IdProductoSTIngreso").ReadOnly();
            References(o => o.oUsuario, "IdUsuarioModifico").ReadOnly();
            References(o => o.oNuevoEstado, "IdNuevoEstado").ReadOnly();
            References(o => o.oFormulario, "IdFormulario").ReadOnly();
            References(o => o.oFormrespuestas, "IdFormrespuestas").ReadOnly();

        }
    }
}
