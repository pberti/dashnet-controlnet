﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IOT.Domain;
using FluentNHibernate.Mapping;

namespace IOT.Mapping
{
    public class MapOpNovedades : ClassMap<OpNovedades>
    {
        public MapOpNovedades()
        {
            base.Table("iot_op_novedades");
            Id(c => c.Id).GeneratedBy.Identity();
            Map(c => c.IdOP);
            Map(c => c.Comentario);
            Map(c => c.Fecha);
            Map(c => c.IdUsuarioModifico);
            Map(c => c.FecUltimaModificacion);

            References(o => o.oUsuario, "IdUsuarioModifico").ReadOnly();
            References(o => o.oOP, "IdOP").ReadOnly();
        }
    }
}
