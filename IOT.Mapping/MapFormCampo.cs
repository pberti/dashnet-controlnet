﻿using FluentNHibernate.Mapping;
using IOT.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOT.Mapping
{
    public class MapFormCampo : ClassMap<FormCampo>
    {
        public MapFormCampo()
        {
            Table("iot_formcampos");
            Id(c => c.Id).GeneratedBy.Identity();
            Map(c => c.IdFormulario);
            Map(c => c.Posicion);
            Map(c => c.TipoCampo);
            Map(c => c.Pregunta);
            Map(c => c.OpcionesRespuesta);
            Map(c => c.EsRequerido);
            Map(c => c.ConComentarioAdicional);
            Map(c => c.Inactivo);
            Map(c => c.FecInactivo);
            Map(c => c.MotivoInactivo);
            Map(c => c.FecUltimaModificacion);
            Map(c => c.IdUsuarioModifico);
        }
    }
}
