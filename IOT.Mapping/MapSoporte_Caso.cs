﻿using IOT.Domain;
using FluentNHibernate.Mapping;

namespace IOT.Mapping
{
    public class MapSoporte_Caso : ClassMap<Soporte_Caso>
    {
        public MapSoporte_Caso()
        {
            base.Table("iot_soporte_caso");
            Id(c => c.Id).GeneratedBy.Identity();
            Map(c => c.Comentario);
            Map(c => c.Contacto);
            Map(c => c.ContactoEmail);
            Map(c => c.Fecha);
            Map(c => c.IdCliente);
            Map(c => c.IdEstadoCaso);
            Map(c => c.IdProducto);
            Map(c => c.IdResponsable);
            Map(c => c.IdTipoProducto);
            Map(c => c.MedioContacto);
            Map(c => c.NroCaso);
            Map(c => c.NroReclamo);
            Map(c => c.NroConsola);

            Map(c => c.IdResolucion);
            Map(c => c.IdCategoria);
            Map(c => c.IdSubCategoria);

            References(o => o.oUsuario, "IdResponsable").ReadOnly();
            References(o => o.oCliente, "IdCliente").ReadOnly();
            References(o => o.oCategoriaPadre, "IdCategoria").ReadOnly();
            References(o => o.oEstado, "IdEstadoCaso").ReadOnly();
            References(o => o.oResolucion, "IdResolucion").ReadOnly();
            References(o => o.oTipoProducto, "IdTipoProducto").ReadOnly();
        }
    }
}
