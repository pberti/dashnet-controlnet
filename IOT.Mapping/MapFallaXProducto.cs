﻿using IOT.Domain;
using FluentNHibernate.Mapping;

namespace IOT.Mapping
{
    public class MapFallaXProducto : ClassMap<FallasXProducto>
    {
        public MapFallaXProducto()
        {
            base.Table("iot_st_fallasxproducto");
            Id(c => c.Id).GeneratedBy.Identity();
            Map(c => c.IdFalla);
            Map(c => c.IdTipoProducto);

            References(o => o.oFalla, "IdFalla").ReadOnly();
            References(o => o.oProducto, "IdTipoProducto").ReadOnly();
        }
    }
}
