﻿using IOT.Domain;
using FluentNHibernate.Mapping;

namespace IOT.Mapping
{
    public class MapEjecutor : ClassMap<Ejecutor>
    {
        public MapEjecutor()
        {
            base.Table("iot_ejecutor");
            Id(c => c.Id).GeneratedBy.Identity();
            Map(c => c.Nombre);
            Map(c => c.Inactivo);
            Map(c => c.InactivoFecha);
            Map(c => c.InactivoComentario);
            Map(c => c.IdUsuarioModifico);
            Map(c => c.FecUltimaModificacion);
        }
    }
}
