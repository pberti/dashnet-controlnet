﻿using IOT.Domain;
using FluentNHibernate.Mapping;

namespace IOT.Mapping
{
    public class MapFallaXIngreso : ClassMap<FallasXIngreso>
    {
        public MapFallaXIngreso()
        {
            base.Table("iot_st_fallasxingreso");
            Id(c => c.Id).GeneratedBy.Identity();
            Map(c => c.IdFalla);
            Map(c => c.IdIngreso);
            Map(c => c.Comentario);
            Map(c => c.IdTipoProducto);
            Map(c => c.FecUltimaModificacion);

            References(o => o.oFalla, "IdFalla").ReadOnly();
            References(o => o.oIngreso, "IdIngreso").ReadOnly();
            References(o => o.oProducto, "IdTipoProducto").ReadOnly();
        }
    }
}
