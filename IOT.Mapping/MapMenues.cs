﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IOT.Domain;
using FluentNHibernate.Mapping;

namespace IOT.Mapping
{
    public class MapMenues: ClassMap<Menues>
    {
        public MapMenues()
        {
            base.Table("iot_menues");
            Id(c => c.Id).GeneratedBy.Identity();
            Map(c => c.Nombre);
            Map(c => c.Descripcion);
            Map(c => c.PathPagina);
            Map(c => c.IdMenuPadre);
            Map(c => c.IdUsuarioModifico);
            Map(c => c.FecUltimaModificacion);
            Map(c => c.Posicion);

            References(o => o.oMenuPadre, "IdMenuPadre").ReadOnly();
        }
    }
}
