﻿using IOT.Domain;
using FluentNHibernate.Mapping;

namespace IOT.Mapping
{
    public class MapTempExcel : ClassMap<TempExcel>
    {
        public MapTempExcel()
        {
            base.Table("iot_tempexcel");
            Id(c => c.Id).GeneratedBy.Identity();
            Map(c => c.Cliente);
            Map(c => c.Contacto);
            Map(c => c.Correo);
            Map(c => c.Cuit);
            Map(c => c.Domicilio);
            Map(c => c.NroSerie);
            Map(c => c.Provincia);
            Map(c => c.Telefono);
        }
    }
}
