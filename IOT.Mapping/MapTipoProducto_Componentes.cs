﻿using FluentNHibernate.Mapping;
using IOT.Domain;

namespace IOT.Mapping
{
    public class MapTipoProducto_Componentes : ClassMap<TipoProducto_Componente>
    {
        public MapTipoProducto_Componentes()
        {
            Table("iot_tipoproducto_componentes");
            Id(c => c.Id).GeneratedBy.Identity();
            Map(c => c.IdProducto);
            Map(c => c.IdComponente);
            Map(c => c.Cantidad);
            Map(c => c.Comentario);

            Map(c => c.IdUsuarioModifico);
            Map(c => c.FecUltimaModificacion);

            References(o => o.oProducto, "IdProducto").ReadOnly();
            References(o => o.oComponentes, "IdComponente").ReadOnly();
            References(o => o.oUsuario, "IdUsuarioModifico").ReadOnly();

        }

    }
}
