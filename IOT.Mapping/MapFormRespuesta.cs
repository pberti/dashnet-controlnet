﻿using FluentNHibernate.Mapping;
using IOT.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOT.Mapping
{
    public class MapFormRespuesta : ClassMap<FormRespuesta>
    {
        public MapFormRespuesta()
        {
            Table("iot_formrespuestas");
            Id(c => c.Id).GeneratedBy.Identity();
            Map(c => c.IdFormulario);
            Map(c => c.IdUsuarioRespondio);
            Map(c => c.Inactivo);
            Map(c => c.FecInactivo);
            Map(c => c.MotivoInactivo);
            Map(c => c.FecUltimaModificacion);
            Map(c => c.IdUsuarioModifico);
            Map(c => c.FecAlta);

            References(o => o.oUsuarioRespondio, "IdUsuarioRespondio").ReadOnly();
            References(o => o.oFormulario, "IdFormulario").ReadOnly();
            HasMany(l => l.lFormRespuestasCampos).KeyColumns.Add("IdFormRespuesta").Inverse();
        }
    }
}
