﻿using FluentNHibernate.Mapping;
using IOT.Domain;


namespace IOT.Mapping
{
    public class MapProductoST_Componentes : ClassMap<ProductoST_Componentes>
    {
        public MapProductoST_Componentes()
        {
            Table("iot_productost_componente");
            Id(c => c.Id).GeneratedBy.Identity();
            Map(c => c.IdProductoST);
            Map(c => c.IdComponente);
            Map(c => c.NroSerie);
            Map(c => c.NroOrden);

            References(o => o.oProductoST, "IdProductoST").ReadOnly();
            References(o => o.oComponente, "IdComponente").ReadOnly();
        }
    }
}
