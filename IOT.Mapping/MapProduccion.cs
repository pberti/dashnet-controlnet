﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IOT.Domain;
using FluentNHibernate.Mapping;

namespace IOT.Mapping
{
   

    public class MapProduccion : ClassMap<Produccion>
    {
        public MapProduccion()
        {
            base.Table("iot_produccion");
            Id(c => c.Id).GeneratedBy.Identity();
            Map(c => c.IdEstadoActual);
            Map(c => c.IdOrdenProduccion);
            Map(c => c.IdProducto);
            Map(c => c.IdWorkflow);
            Map(c => c.NroCaja);
            Map(c => c.NroSerieAASS);
            Map(c => c.NroSeriePPPC);
            Map(c => c.NroSerieContador);
            Map(c => c.NroRemito);
            Map(c => c.FechaCreacion);
            Map(c => c.FechaDeposito);
            Map(c => c.FechaEnvioCliente);
            Map(c => c.FechaInicioProduccion);
            Map(c => c.OrdenCompra);


            References(o => o.oEstadoActual, "IdEstadoActual").ReadOnly();
            References(o => o.oOrdenProduccion, "IdOrdenProduccion").ReadOnly();
            References(o => o.oWorkflow, "IdWorkflow").ReadOnly();
            References(o => o.oProducto, "IdProducto").ReadOnly();
        }
    }
}
