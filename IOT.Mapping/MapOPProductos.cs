﻿using FluentNHibernate.Mapping;
using IOT.Domain;

namespace IOT.Mapping
{
    public class MapOpProductos : ClassMap<OpProductos>
    {
        public MapOpProductos()
        {
            base.Table("iot_op_productos");
            Id(c => c.Id).GeneratedBy.Identity();
            Map(c => c.IdOP);
            Map(c => c.IdMaestroProducto);
            Map(c => c.Cantidad);
            Map(c => c.Comentario);
            Map(c => c.IdWorkflow);
            Map(c => c.IdUsuarioModifico);
            Map(c => c.FecUltimaModificacion);
            Map(c => c.OpcionMontaje);

            References(o => o.oUsuario, "IdUsuarioModifico").ReadOnly();
            References(o => o.oOP, "IdOP").ReadOnly();
            References(o => o.oTipoProducto, "IdMaestroProducto").ReadOnly();
            References(o => o.oWorkflow, "IdWorkflow").ReadOnly();
        }
    }
}
