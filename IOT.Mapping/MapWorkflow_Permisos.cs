﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IOT.Domain;
using FluentNHibernate.Mapping;

namespace IOT.Mapping
{
    public class MapWorkflow_Permisos : ClassMap<Workflow_Permisos>
    {
        public MapWorkflow_Permisos()
        {
            base.Table("iot_workflow_permisos");
            Id(c => c.Id).GeneratedBy.Identity();
            Map(c => c.IdWorkflowEstado);
            Map(c => c.IdPerfil);
            Map(c => c.PuedeVolver);
            Map(c => c.PuedeAvanzar);
            Map(c => c.PuedeVer);
            Map(c => c.PuedeGrabar);

            References(o => o.oWorkflowEstado, "IdWorkflowEstado").ReadOnly();
            References(o => o.oPerfil, "IdPerfil").ReadOnly();
        }
    }
}
