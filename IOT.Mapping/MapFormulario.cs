﻿using FluentNHibernate.Mapping;
using IOT.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOT.Mapping
{
    public class MapFormulario : ClassMap<Formulario>
    {
        public MapFormulario()
        {
            Table("iot_formularios");
            Id(c => c.Id).GeneratedBy.Identity();
            Map(c => c.Titulo);
            Map(c => c.Comentario);
            Map(c => c.Inactivo);
            Map(c => c.FecInactivo);
            Map(c => c.MotivoInactivo);
            Map(c => c.FecUltimaModificacion);
            Map(c => c.IdUsuarioModifico);

            HasMany(l => l.lFormCampos).KeyColumns.Add("IdFormulario").Inverse();
        }
    }
}
