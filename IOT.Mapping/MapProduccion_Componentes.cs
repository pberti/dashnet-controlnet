﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IOT.Domain;
using FluentNHibernate.Mapping;

namespace IOT.Mapping
{

    public class MapProduccion_Componentes : ClassMap<Produccion_Componentes>
    {
        public MapProduccion_Componentes()
        {
            base.Table("iot_produccion_componentes");
            Id(c => c.Id).GeneratedBy.Identity();
            Map(c => c.IdComponente);
            Map(c => c.IdProduccion);
            Map(c => c.NroOrden);
            Map(c => c.NroSerie);


            References(o => o.oComponente, "IdComponente").ReadOnly();
            References(o => o.oProduccion, "IdProduccion").ReadOnly();

        }
    }
}
