﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using IOT.Domain;

namespace IOT.Mapping
{
    public class MapOpEntregaEstado : ClassMap<OpEntregasEstado>
    {
        public MapOpEntregaEstado()
        {
            base.Table("iot_op_entrega_Estado");
            Id(c => c.Id).GeneratedBy.Identity();
            Map(c => c.Nombre);
            Map(c => c.NroOrden);
            Map(c => c.AceptaModificaciones);
            Map(c => c.EstadoInicial);
            Map(c => c.EstadoFinal);
        }
    }
}
