﻿using FluentNHibernate.Mapping;
using IOT.Domain;

namespace IOT.Mapping
{
    public class MapOpEntregas : ClassMap<OpEntregas>
    {
        public MapOpEntregas()
        {
            base.Table("iot_op_entregas");
            Id(c => c.Id).GeneratedBy.Identity();
            Map(c => c.IdOP);
            Map(c => c.Comentario);
            Map(c => c.FechaEstimada);
            Map(c => c.FechaReal);
            Map(c => c.EstadoEntrega);
            Map(c => c.IdusuarioCerro);
            Map(c => c.ComentarioCierre);

            References(o => o.oUsuario, "IdusuarioCerro").ReadOnly();
            References(o => o.oOP, "IdOP").ReadOnly();
            References(o => o.oEstadoEntrega, "EstadoEntrega").ReadOnly();
        }
    }
}
