﻿using FluentNHibernate.Mapping;
using IOT.Domain;
using System;

namespace IOT.Mapping
{
    public class MapTipoPerfilMenues : ClassMap<TipoPerfilMenues>
    {
        public MapTipoPerfilMenues()
        {
            base.Table("iot_tipoperfilmenu");
            Id(c => c.Id).GeneratedBy.Identity();
            base.Map((TipoPerfilMenues c) => (object)c.IdMenu);
            base.Map((TipoPerfilMenues c) => (object)c.IdTipoPerfil);
            base.Map((TipoPerfilMenues c) => (object)c.IdUsuarioModifico);
            base.Map((TipoPerfilMenues c) => (object)c.FecUltimaModificacion);
            base.Map((TipoPerfilMenues c) => (object)c.PuedeBorrar);
            base.Map((TipoPerfilMenues c) => (object)c.PuedeGrabar);
            base.References<Menues>((TipoPerfilMenues o) => o.oMenu, "IdMenu").ReadOnly();
            base.References<TipoPerfil>((TipoPerfilMenues o) => o.oTipoPerfil, "IdTipoPerfil").ReadOnly();
        }
    }
}
