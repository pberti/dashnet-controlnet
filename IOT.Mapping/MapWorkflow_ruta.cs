﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IOT.Domain;
using FluentNHibernate.Mapping;

namespace IOT.Mapping
{
    public class MapWorkflow_ruta : ClassMap<Workflow_Ruta>
    {
        public MapWorkflow_ruta()
        {
            base.Table("iot_workflow_ruta");
            Id(c => c.Id).GeneratedBy.Identity();
            Map(c => c.IdWorkflowEstado);
            Map(c => c.IdEstadoAnterior);
            Map(c => c.IdEstadoSiguiente);
            Map(c => c.NroOrden);
            Map(c => c.IdWorkflow);
            Map(c => c.Regreso);
            Map(c => c.IdEstadoSiguienteB);

            References(o => o.oWorkflow, "IdWorkflow").ReadOnly();
            References(o => o.oWorkflowEstado, "IdWorkflowEstado").ReadOnly();
            References(o => o.oEstadoAnterior, "IdEstadoAnterior").ReadOnly();
            References(o => o.oEstadoSiguiente, "IdEstadoSiguiente").ReadOnly();
            References(o => o.oEstadoSiguienteB, "IdEstadoSiguienteB").ReadOnly();
        }
    }


}
