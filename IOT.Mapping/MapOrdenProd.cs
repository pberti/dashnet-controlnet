﻿using FluentNHibernate.Mapping;
using IOT.Domain;

namespace IOT.Mapping
{
    public class MapOrdenProd : ClassMap<OrdenProd>
    {
        public MapOrdenProd()
        {
            base.Table("iot_ordenproduccion");
            Id(c => c.Id).GeneratedBy.Identity();
            Map(c => c.Nombre);
            Map(c => c.Descripcion);
            Map(c => c.SolicitadoPor);
            Map(c => c.NroPedidoExterno);
            Map(c => c.NroPedidoInterno);
            Map(c => c.Estado);
            Map(c => c.FechaPedido);
            Map(c => c.IdUsuarioCreo);
            Map(c => c.IdUsuarioModifico);
            Map(c => c.FecUltimaModificacion);
            Map(c => c.IdCliente);

            References(o => o.oUsuario, "IdUsuarioModifico").ReadOnly();
            References(o => o.oUsuarioCreo, "IdUsuarioCreo").ReadOnly();
            References(o => o.oEstado, "Estado").ReadOnly();
        }
    }
}
