﻿using FluentNHibernate.Mapping;
using IOT.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOT.Mapping
{
    public class MapFormRespuestaCampo: ClassMap<FormRespuestaCampo>
    {
        public MapFormRespuestaCampo()
        {
            Table("iot_formrespuestascampos");
            Id(c => c.Id).GeneratedBy.Identity();
            Map(c => c.IdFormRespuesta);
            Map(c => c.IdFormCampo);
            Map(c => c.Respuesta);
            Map(c => c.ComentarioAdicional);
            Map(c => c.Inactivo);
            Map(c => c.FecInactivo);
            Map(c => c.MotivoInactivo);
            Map(c => c.FecUltimaModificacion);
            Map(c => c.IdUsuarioModifico);

            References(o => o.oFormCampo, "IdFormCampo").ReadOnly();
        }
    }
}
