﻿using FluentNHibernate.Mapping;
using IOT.Domain;

namespace IOT.Mapping
{
    public class MapTipoComponentes : ClassMap<TipoComponentes>
    {
        public MapTipoComponentes()
        {
            Table("iot_tipocomponentes");
            Id(c => c.Id).GeneratedBy.Identity();
            Map(c => c.Nombre);
            Map(c => c.Descripcion);
            Map(c => c.Inactivo);
            Map(c => c.RequiereNroSerie);
            Map(c => c.InactivoFecha);
            Map(c => c.InactivoComentario);
            Map(c => c.IdUsuarioModifico);
            Map(c => c.FecUltimaModificacion);
        }

    }
}
