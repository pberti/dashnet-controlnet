﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IOT.Domain;
using FluentNHibernate.Mapping;

namespace IOT.Mapping
{
    public class MapWorkflow_Estado : ClassMap<Workflow_Estado>
    {
        public MapWorkflow_Estado()
        {
            base.Table("iot_workflow_estados");
            Id(c => c.Id).GeneratedBy.Identity();
            Map(c => c.Nombre);
            Map(c => c.Descripcion);
            Map(c => c.EsFinal);
            Map(c => c.EsInicial);
            Map(c => c.Pantalla);
            Map(c => c.IdUsuarioModifico);
            Map(c => c.FecUltimaModificacion);
            Map(C => C.Masivo);
            Map(c => c.IdEjecutor);

            References(o => o.oUsuario, "IdUsuarioModifico").ReadOnly();
            References(o => o.oEjecutor, "IdEjecutor").ReadOnly();
        }
    }
}
