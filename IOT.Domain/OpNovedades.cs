﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOT.Domain
{
    public class OpNovedades: IEntidad
    {
        public virtual long Id { get; set; }
        public virtual long IdOP { get; set; }
        public virtual string Comentario { get; set; }
        public virtual DateTime Fecha { get; set; }

        #region IEntidad Members
        public virtual long IdUsuarioModifico { get; set; }
        public virtual DateTime FecUltimaModificacion { get; set; }
        #endregion

        #region Relaciones con Otros Objetos
        public virtual OrdenProd oOP { get; set; }
        public virtual Usuario oUsuario { get; set; }
        #endregion

        public virtual string FechaSinHora
        {
            get
            {
                return Fecha.ToShortDateString();
            }
        }
    }
}
