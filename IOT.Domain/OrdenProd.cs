﻿using System;
using System.ComponentModel.DataAnnotations;

namespace IOT.Domain
{
    [Serializable()]
    public class OrdenProd : IEntidad
    {

        public virtual long Id { get; set; }
        public virtual string Nombre { get; set; }
        public virtual string Descripcion { get; set; }
        public virtual string SolicitadoPor { get; set; }
        public virtual string NroPedidoExterno { get; set; }
        public virtual string NroPedidoInterno { get; set; }
        public virtual string Estado { get; set; }
        public virtual DateTime FechaPedido { get; set; }
        public virtual long IdUsuarioCreo { get; set; }
        public virtual long IdCliente { get; set; }

        #region IEntidad Members
        public virtual long IdUsuarioModifico { get; set; }
        public virtual DateTime FecUltimaModificacion { get; set; }
        #endregion

        #region Relaciones con Otros Objetos
        public virtual Usuario oUsuarioCreo { get; set; }
        public virtual Usuario oUsuario { get; set; }
        public virtual Clientes oCliente { get; set; }
        public virtual OpEstados oEstado { get; set; }
        #endregion

        public virtual string FechaPedidoSinHora
        {
            get
            {
                return FechaPedido.ToShortDateString();
            }
        }
    }
}
