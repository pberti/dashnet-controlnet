﻿using System;

namespace IOT.Domain
{
    public class Soporte_Categoria
    {
        public virtual long Id { get; set; }
        public virtual string Nombre { get; set; }
        public virtual long IdPadre { get; set; }
    }
}
