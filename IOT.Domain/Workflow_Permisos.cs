﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOT.Domain
{
    public class Workflow_Permisos
    {
        public virtual long Id { get; set; }
        public virtual long IdWorkflowEstado { get; set; }
        public virtual long IdPerfil { get; set; }
        public virtual bool PuedeVer { get; set; }
        public virtual bool PuedeGrabar { get; set; }
        public virtual bool PuedeVolver { get; set; }
        public virtual bool PuedeAvanzar { get; set; }

        public virtual Workflow_Estado oWorkflowEstado { get; set; }
        public virtual TipoPerfil oPerfil { get; set; }
    }
}
