﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOT.Domain
{
    [Serializable()]
    public class Workflow
    {
        public virtual long Id { get; set; }
        public virtual string Nombre { get; set; }
        public virtual string Descripcion { get; set; }
        public virtual long idUsuarioCreo { get; set; }
        public virtual DateTime FechaUltimaModificacion { get; set; }

        public virtual bool Inactivo { get; set; }
        public virtual DateTime InactivoFecha { get; set; }
        public virtual string InactivoComentario { get; set; }
        public virtual string Destino { get; set; }

        #region Relacion con otras clases
        public virtual Usuario oUsuario { get; set; }
        #endregion
    }
}
