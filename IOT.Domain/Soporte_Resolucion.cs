﻿using System;

namespace IOT.Domain
{
    public class Soporte_Resolucion
    {
        public virtual long Id { get; set; }
        public virtual string Nombre { get; set; }
    }
}
