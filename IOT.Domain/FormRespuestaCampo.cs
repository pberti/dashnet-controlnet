﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOT.Domain
{
    public class FormRespuestaCampo : IEntidad
    {
        public virtual long Id { get; set; }
        public virtual long IdFormRespuesta { get; set; }
        public virtual long IdFormCampo { get; set; }
        public virtual FormCampo oFormCampo { get; set; }
        public virtual string Respuesta { get; set; }
        public virtual string ComentarioAdicional { get; set; }

        public virtual bool Inactivo { get; set; }
        public virtual DateTime FecInactivo { get; set; }
        public virtual string MotivoInactivo { get; set; }
        public virtual DateTime FecUltimaModificacion { get; set; }
        public virtual long IdUsuarioModifico { get; set; }

        public virtual string TipoCampo { get
            {
                return oFormCampo.TipoCampo;
            }
        }

        public virtual List<string> lSelecciones
        {
            get
            {
                List<string> lOpRespuestas = new List<string>();
               // if (oFormCampo.TipoCampo != ValoresConstantes.OptFormCampo_TextoCorto && oFormCampo.TipoCampo != ValoresConstantes.OptFormCampo_Parrafo)
                {
                    if (string.IsNullOrEmpty(this.Respuesta)) this.Respuesta = string.Empty;
                    string[] aOptRespuesta = this.Respuesta.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
                    foreach (string opcion in aOptRespuesta)
                    {
                        lOpRespuestas.Add(opcion);
                    }
                }
                return lOpRespuestas;
            }
        }
    }
}
