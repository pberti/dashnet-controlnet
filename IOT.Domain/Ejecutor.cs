﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOT.Domain
{
    public class Ejecutor
    {
        public virtual long Id { get; set; }
        public virtual string Nombre { get; set; }

        #region Inactivo
        public virtual bool Inactivo { get; set; }
        public virtual DateTime InactivoFecha { get; set; }
        public virtual string InactivoComentario { get; set; }
        #endregion

        #region IEntidad Members
        public virtual long IdUsuarioModifico { get; set; }
        public virtual DateTime FecUltimaModificacion { get; set; }
        #endregion

    }
}