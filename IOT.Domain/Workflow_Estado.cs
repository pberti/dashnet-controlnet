﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOT.Domain
{
    [Serializable()]
    public class Workflow_Estado : IEntidad
    {
        public virtual long Id { get; set; }
        public virtual string Nombre { get; set; }
        public virtual string Descripcion { get; set; }
        public virtual bool EsInicial { get; set; }
        public virtual bool EsFinal { get; set; }
        public virtual string Pantalla { get; set; }
        public virtual bool Masivo { get; set; }
        public virtual long IdEjecutor { get; set; }

        #region IEntidad Members
        public virtual long IdUsuarioModifico { get; set; }
        public virtual DateTime FecUltimaModificacion { get; set; }
        #endregion

        #region Relacion con otros objetos
        public virtual Usuario oUsuario { get; set; }
        public virtual Ejecutor oEjecutor { get; set; }
        #endregion
    }
}
