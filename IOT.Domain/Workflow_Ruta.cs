﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOT.Domain
{
    public class Workflow_Ruta
    {
        public virtual long Id { get; set; }
        public virtual long IdWorkflow { get; set; }
        public virtual long IdWorkflowEstado { get; set; }
        public virtual long IdEstadoAnterior { get; set; }
        public virtual long IdEstadoSiguiente { get; set; }
        public virtual int NroOrden { get; set; }
        public virtual bool Regreso { get; set; }
        public virtual long IdEstadoSiguienteB { get; set; }


        #region Relacion con otros objetos
        public virtual Workflow oWorkflow { get; set; }
        public virtual Workflow_Estado oWorkflowEstado { get; set; }
        public virtual Workflow_Estado oEstadoAnterior { get; set; }
        public virtual Workflow_Estado oEstadoSiguiente { get; set; }
        public virtual Workflow_Estado oEstadoSiguienteB { get; set; }

        #endregion

        #region Nombre del Estado
        public virtual string Nombre
        {

            get
            {

                return oWorkflow.Nombre + "-" + oWorkflowEstado.Nombre;
            }

        }

        public virtual string EstadoSiguiente
        {

            get
            {

                if (IdEstadoSiguiente != 0)
                    return oEstadoSiguiente.Nombre;
                else
                    return "--NO TIENE--";
            }

        }

        public virtual string EstadoSiguienteB
        {

            get
            {

                if (IdEstadoSiguienteB != 0)
                    return oEstadoSiguienteB.Nombre;
                else
                    return "--NO TIENE--";
            }

        }

        public virtual string EstadoAnterior
        {

            get
            {
                if (IdEstadoAnterior != 0)
                    return oEstadoAnterior.Nombre;
                else
                    return "--NO TIENE--";
            }

        }
        #endregion
    }
}
