﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace IOT.Domain
{
    public static class ValoresConstantes
    {
        /// <summary>
        /// Palabras claves de ordenación por defecto para todas las grillas (Ascendentemente por Id)
        /// </summary>
        public static string DescendingById = "Id descending";

        #region GENERALES

        public static string opcionTodos = "Todos";
        public static string OpcionTodas = "Todas";
        public static string opcionNinguno = "Ninguno";
        public static string opcionNinguna = "Ninguna";
        public static string opcionOtra = "Otra";
        public static string opcionOtro = "Otro";
        public static string valueCeroString = "0";
        public static int valueCeroInt = 0;
        public static string sinOpciones = "*** Sin opciones ***";
        public static int noOpcionesInt = -1;
        public static string noOpcionesString = "-1";
        public static string nombreSistema = "ISO-ASG";

        /// <summary>
        /// ListItem utilizado para cargar controles con opciiones donde se necesita una opción genérica
        /// </summary>
        public static ListItem LITodos = new ListItem() { Text = opcionTodos, Value = valueCeroString };

        #endregion

        #region CONTEXTO - ENTIDAD MOTIVO_ACTIVIDAD

        public static int EnviarmeMail = 1;
        public static int EnviarARecursos = 2;
        public static int Noenviar = 3;

        #endregion

        #region CONTEXTO - ENTIDAD EMPRESA

        /// <summary>
        /// Lista de tipos de Empresas
        /// </summary>
        public static List<string> lTiposEmpresas { get { return new List<string>() { "Sociedad", "Unipersonal", "Sub-Empresa", "Cooperativa" }; } }

        /// <summary>
        /// Lista de tipos de Unidades de Gestion
        /// </summary>
        public static List<string> lUnidadesGestion { get { return new List<string>() { "Córdoba", "Rio Cuarto", "CABA", "Salta", "San Juan" }; } }

        /// <summary>
        /// Lista de tipos de Situaciones de IVA
        /// </summary>
        public static List<string> lSituacionesIVA { get { return new List<string>() { "Consumidor Final", "Exento", "Monotributo", "Responsable Inscripto" }; } }

        /// <summary>
        /// Lista de tipos de Estados de Cliente
        /// </summary>
        public static List<string> lEstodosClientes { get { return new List<string>() { "Cliente", "Cliente Activo", "Llame Ya", "Para Llamar", "Contactado", "Cotizado en Seguimiento", "Cotizado" }; } }

        #endregion

        #region CONTEXTO - ENTIDAD PROYECTO

        /// <summary>
        /// Constante que contiene el valor del Id del TipoPerfil de Empleado
        /// NOTA: si en la BD este TipoPerfil cambia de Id, entonces los cambios también deben reflejarse aquí.
        /// </summary>
        public static long IdTipoPerfilEmpleado = 3;

        public static List<string> EstadosProyecto { get { return new List<string>() { OpcionEnCurso, OpcionSinIniciar, OpcionFinalizado }; } }

        public static string OpcionEnCurso = "En Curso";
        public static string OpcionSinIniciar = "Sin Iniciar";
        public static string OpcionFinalizado = "Finalizado";

        #endregion

        #region CONTEXTO - ENTIDAD ACTIVIDADES

        public static string EstadoIniciada = "Iniciada";
        public static string EstadoFinalizada = "Finalizada";
        public static string EstadoFinalizadaPorSistema = "Finalizada por Sistema";
        public static string EstadoProgramada = "Programada";

        public static List<string> lEstadosActividades = new List<string>() { EstadoIniciada, EstadoProgramada, EstadoFinalizada, EstadoFinalizadaPorSistema };

        public static string Productiva = "Productiva";
        public static string NoProductiva = "No Productiva";

        public static long IdMotivoFranco = 30;
        public static long IdEmpresaAsg = 5;
        public static long IdTipoActividadTrabajoEnOficina = 3;
        #endregion

        #region ESTADOS GRALES

        public static string EstadoCumplimentado = "Cumplimentado";

        #endregion

        #region CONTEXTO - ENTIDAD PROY_HITOS

        /// Estado 'En Proceso' para instancias de Entidad ProyHito
        /// </summary>
        public static string EstadoEnProceso = "En Proceso";

        /// <summary>
        /// Lista de estados posibles para instancias de Entidad ProyHito
        /// </summary>
        public static List<string> EstadosProyHito = new List<string>() { EstadoCumplimentado, EstadoEnProceso };

        #endregion

        #region CONTEXTO - ENTIDAD PROY_PACC_ENTREGABLES

        public static string EstadoPendiente = "Pendiente";

        #endregion

        #region CONTEXTO - ENTIDAD BASES_CONOCIMIENTOS
        
        public static List<string> EstadosBasesConocimientos { get { return new List<string>() { OpcionAbierta, OpcionResuelta }; } }

        public static string OpcionAbierta = "Abierta";
        public static string OpcionResuelta = "Resuelta";

        #endregion

        #region CONTEXTO - RRHH_EMPLEADOS

        /// <summary>
        /// Lista de Entidades Bancarias
        /// </summary>
        public static List<string> lBancos { get { return new List<string>() { "Banco de Córdoba", "Banco Santiago del Estero", "Credicoop", "BBVA Fraces","Galicia", "HSBC", "Hypotecario", "Macro", "Patagonia", "Santander Río", "Otro" }; } }

        #endregion

        #region CONTEXTO NOTIFICACIONES

        public static string OpNot_SinLeer = "Sin leer";
        public static string OpNot_Leida = "Leida";
        public static List<string> lEstadosNotificaciones = new List<string>() { OpNot_Leida, OpNot_SinLeer };

        #endregion

        #region CONTEXTO HALLAZGOS

        public static List<string> lHallazgos = new List<string>() { "No Conformidad", "Observación", "Oportunidad de Mejora" };
        public static List<string> lAcciones = new List<string>() { "Acción Correctiva", "Acción Inmediata" };

        #endregion

        #region CONTEXTO NIVELES DE OCUPACIÓN

        public static List<string> lPorcentajes
        {
            get
            {
                return new List<string>()
                    { RangoOcu_MayorIgual0_Hasta25, RangoOcu_Mayor25_Hasta50,RangoOcu_Mayor50_Hasta75, RangoOcu_Mayor75_Hasta100,RangoOcu_Mayor100};
            }
        }

        public static string RangoOcu_MayorIgual0_Hasta25 = "Mayor o igual a 0 - Hasta 25";
        public static string RangoOcu_Mayor25_Hasta50 = "Mayor a 25 - Hasta 50";
        public static string RangoOcu_Mayor50_Hasta75 = "Mayor a 50 - Hasta 75";
        public static string RangoOcu_Mayor75_Hasta100 = "Mayor a 75 - Hasta 100";
        public static string RangoOcu_Mayor100 = "Mayor a 100";

        public static double NivOcu_100Porc = 100;
        public static double NivOcu_75Porc = 75;
        public static double NivOcu_50Porc = 50;
        public static double NivOcu_25Porc = 25;

        public static bool VerificarSiAplica(string pRango, double pPorcentajeUsuario)
        {
            if(RangoOcu_Mayor100 == pRango && pPorcentajeUsuario > 100)
                return true;
            else if (RangoOcu_Mayor75_Hasta100 == pRango && pPorcentajeUsuario <= NivOcu_100Porc && pPorcentajeUsuario > NivOcu_75Porc)
                return true;
            else if (RangoOcu_Mayor50_Hasta75 == pRango && pPorcentajeUsuario <= NivOcu_75Porc && pPorcentajeUsuario > NivOcu_50Porc)
                return true;
            else if (RangoOcu_Mayor25_Hasta50 == pRango && pPorcentajeUsuario <= NivOcu_50Porc && pPorcentajeUsuario > NivOcu_25Porc)
                return true;
            else if (RangoOcu_MayorIgual0_Hasta25 == pRango && pPorcentajeUsuario <= NivOcu_25Porc)
                return true;
            else return false;
        }

        #endregion

        #region CONTEXTO FORMULARIOS

        public static string OptFormCampo_Parrafo = "Párrafo";
        public static string OptFormCampo_SeleccionMultiple = "Selección Múltiple";
        public static string OptFormCampo_SeleccionUnica = "Selección Única";
        public static string OptFormCampo_TextoCorto = "Texto Corto";
        public static string OptFormCampo_Valoracion = "Valoración";
        public static string OptFormCampo_VerdaderoFalso = "Verdadero o Falso";

        public static List<string> lFormCampoTipos
        {
            get
            {
                return new List<string>()
                {
                    OptFormCampo_Parrafo,
                    OptFormCampo_SeleccionMultiple,
                    OptFormCampo_SeleccionUnica,
                    OptFormCampo_TextoCorto,
                    OptFormCampo_VerdaderoFalso,
                    OptFormCampo_Valoracion
                };
            }
        }

        public static string OptVerdadero = "Verdadero";
        public static string OptFalso = "Falso";

        public static List<string> lOptsVF = new List<string>() { OptVerdadero, OptFalso };

        #endregion

        #region FORMATOS

        /// <summary>
        /// Constante que contiene el código de formateo para decimales. Formato: 0,00
        /// </summary>
        public static string Formato_DECIMAL_F2 = "F2";

        /// <summary>
        /// Contante que contiene el código de formateo para Fecha. Formato: "dd/MM/yyyy HH: mm"
        /// </summary>
        public static string FormatoDD_MM_YYYY_HH_MM = "dd'-'MM'-'yyyy HH: mm";

        public static string FormatoDateTimeISO = "yyyy-MM-dd HH':'mm':'ss";
        #endregion

        #region NOMBRE DIRECTORIOS

        public static string DirectorioFiles = "Files";

        #endregion

        #region COLORES

        public static string rojo1 = "#D02020";
        public static string azul1 = "#17246b";
        public static string amarillo1 = "#FFFF00";
        public static string verde1 = "#4f9310";
        public static string naranja1 = "#FFAC1B";
        public static string violeta1 = "#bd57c6";

        public static string amarillo2 = "#FFFF51";
        public static string rojo2= "#ff7777";
        public static string verde2 = "#94c858";
        public static string naranja2 = "#FFC867";
        public static string azul2 = "#9fc0e0";
        public static string violeta2 = "#ff7cb7";

        public static string naranja3 = "#FFE4B4";
        public static string verde3 = "#ADD57F";
        public static string azul3 = "#4fb4c6";

        public static List<string> listaColores { 
            get
            {
                return new List<string>()
                {
                    rojo1,
                    azul1,
                    amarillo1,
                    verde1,
                    naranja1,
                    violeta1,
                    amarillo2,
                    rojo2,
                    verde2,
                    naranja2,
                    azul2,
                    violeta2,
                    naranja3,
                    verde3,
                    azul3,
                    rojo1,
                    azul1,
                    amarillo1,
                    verde1,
                    naranja1,
                    violeta1,
                    amarillo2,
                    rojo2,
                    verde2,
                    naranja2,
                    azul2,
                    violeta2,
                    naranja3,
                    verde3,
                    azul3
                };
            }
        }

        /// <summary>
        /// Metodo que devuelve un color hexadecimal de tipo string según el estado de la instancia de Actividad enviada por parámetro.
        /// </summary>
        /// <param name="oActividad">Instancia de Actividad</param>
        /// <returns>string</returns>
        /* public static string ObtenerColorXEstadoActividad(Actividad oActividad)
         {
             string color = string.Empty;
             if (oActividad.Inactivo) color = GrisPastel;
             else if (oActividad.Estado == EstadoProgramada && oActividad.IdActividadPadre > 0) color = PurpuraPastel;
             else if (oActividad.Estado == EstadoProgramada) color = AmarilloPastel;
             else if (oActividad.Estado == EstadoFinalizada) color = VerdePastel;
             else if (oActividad.Estado == EstadoIniciada) color = AnaranjadoPastel;
             else if (oActividad.Estado == EstadoFinalizadaPorSistema) color = RojoPastel;
             return color;
         }*/

        #endregion

        #region IDs DE LA BD PARA SER USADOS COMO VALORES POR DEFECTO

        /// <summary>
        /// Id del registro de la tabla Roles de la BD. Corresponde al Rol de Asesor Auxiliar -> con Porcentaje de Asignación igual 0.
        /// </summary>
        public static long IdRol_AsesorAuxiliar = 5;

        /// <summary>
        /// Id del registro de la tabla Roles de la BD. Corresponde al Rol de PMO
        /// </summary>
        public static long IdRol_PMO = 1;

        /// <summary>
        /// Administrador General
        /// </summary>
        public static long IdPerfilAdmin = 1;

        /// <summary>
        /// Administrador de ASG (menos permisos que Administrador Gral por cuestiones de seguridad)
        /// </summary>
        public static long IdPerfilASGAdmin = 1;

        /// <summary>
        /// Id del registro de la tabla TiposActividad de la BD. Corresponde al TipoActividad "Visita".
        /// </summary>
        public static long IdTipoActividadVisita = 1;

        /// <summary>
        /// Id del registro de la tabla TiposActividad de la BD. Corresponde al TipoActividad "Visita".
        /// </summary>
        public static long IdMotivoActividadViaje = 10;

        /// <summary>
        /// Id del registro de la tabla TiposActividad de la BD. Corresponde al TipoActividad "Auditoría Interna".
        /// </summary>
        public static long IdMotivoActividadAudInt = 4;

        /// <summary>
        /// Id del registro de la tabla TiposActividad de la BD. Corresponde al TipoActividad "Auditoría Esterna".
        /// </summary>
        public static long IdMotivoActividadAudExt = 5;

        #endregion

        /// <summary>
        /// Método que genera un item a modo de detalle con estructura HTML "div{label, div{label}}"
        /// </summary>
        /// <param name="key">Nominación del Detalle</param>
        /// <param name="value">Valor del Detalle</param>
        /// <returns>HtmlGenericControl</returns>
        public static HtmlGenericControl CrearItemDetalle(string key, string value)
        {
            HtmlGenericControl divGroup = new HtmlGenericControl("div");
            HtmlGenericControl lblKey = new HtmlGenericControl("label");
            HtmlGenericControl divLabel = new HtmlGenericControl("div");
            HtmlGenericControl lblValue = new HtmlGenericControl("label");

            //Asignamos clases
            divGroup.Attributes["class"] = "form-group";
            lblKey.Attributes["class"] = "control-label col-md-3 col-sm-3 col-xs-12";
            divLabel.Attributes["class"] = "col-md-8 col-sm-8 col-xs-12";
            lblValue.Attributes["class"] = "control-label text-primary";

            //Seteamos los valores a mostrar
            lblKey.InnerText = key + ":";
            lblValue.InnerText = value;

            //Anidamos los controles
            divLabel.Controls.Add(lblValue);
            divGroup.Controls.Add(lblKey);
            divGroup.Controls.Add(divLabel);
            return divGroup;
        }

        public static bool DeterminarSiEsTodoElDia(DateTime pFecInicio, DateTime pFecFin)
        {
            bool resultado = false;
            resultado =
                pFecInicio.Hour == 0 &&
                pFecInicio.Minute == 0 &&
                pFecFin.Hour == 0 &&
                pFecFin.Minute == 0 &&
                (pFecFin.Day - pFecInicio.Day) == 1;
            return resultado;
        }

        public static List<string> EstadosActivas { get { return new List<string>() { OpcionActiva, OpcionInactiva }; } }
        public static List<string> EstadosActivos { get { return new List<string>() { OpcionActivo, OpcionInactivo }; } }

        public static string OpcionActiva = "Activa";
        public static string OpcionInactiva = "Inactiva";
        public static string OpcionActivo = "Activo";
        public static string OpcionInactivo = "Inactivo";

    }
}
