﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOT.Domain
{
    [Serializable()]
    public class Produccion
    {
        public virtual long Id { get; set; }
        public virtual long IdOrdenProduccion { get; set; }
        public virtual long IdWorkflow { get; set; }
        public virtual long IdProducto { get; set; }
        public virtual string NroSerieAASS { get; set; }
        public virtual string NroSeriePPPC { get; set; }
        public virtual string NroSerieContador { get; set; }
        public virtual long IdEstadoActual { get; set; }
        public virtual DateTime FechaCreacion { get; set; }
        public virtual DateTime FechaInicioProduccion { get; set; }
        public virtual DateTime FechaDeposito { get; set; }
        public virtual DateTime FechaEnvioCliente { get; set; }
        public virtual string NroRemito { get; set; }
        public virtual string NroCaja { get; set; }
        public virtual string OrdenCompra { get; set; }

        #region Relacion con otras clases
        public virtual OrdenProd oOrdenProduccion { get; set; }
        public virtual Workflow oWorkflow { get; set; }
        public virtual TipoProducto oProducto { get; set; }
        public virtual Workflow_Estado oEstadoActual { get; set; }
        #endregion


        #region Propiedades
       public virtual string NroSerie {
            get
            {
                return NroSerieAASS + NroSeriePPPC + NroSerieContador;
            }

            }

        public virtual string FechaInicioProduccionText
        {
            get
            {
                if (this.FechaInicioProduccion == DateTime.MinValue)
                    return " ";
                else
                    return FechaInicioProduccion.ToShortDateString();
            }

        }


        public virtual string FechaDepositoText
        {
            get
            {
                if (this.FechaDeposito == DateTime.MinValue)
                    return " ";
                else
                    return FechaDeposito.ToShortDateString();
            }

        }
        public virtual string FechaEnvioClienteText
        {
            get
            {
                if (this.FechaEnvioCliente == DateTime.MinValue)
                    return " ";
                else
                    return FechaEnvioCliente.ToShortDateString();
            }

        }
        #endregion
    }
}
