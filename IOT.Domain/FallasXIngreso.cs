﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOT.Domain
{
    public class FallasXIngreso
    {
        public virtual long Id { get; set; }
        public virtual long IdFalla { get; set; }
        public virtual long IdIngreso { get; set; }
        public virtual string Comentario { get; set; }
        public virtual DateTime FecUltimaModificacion { get; set; }
        public virtual long IdTipoProducto { get; set; }

        #region Relaciones con otros objetos
        public virtual STFalla oFalla { get; set; }
        public virtual ProductoST_Ingreso oIngreso { get; set; }
        public virtual TipoProducto oProducto { get; set; }
        #endregion
    }
}
