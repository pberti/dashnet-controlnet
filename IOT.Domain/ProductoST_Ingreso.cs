﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOT.Domain
{
    public class ProductoST_Ingreso: IEntidad
    {
        public virtual long Id { get; set; }
        public virtual long IdCliente { get; set; }
        public virtual string ContactoEmail { get; set; }
        public virtual string NroReclamo { get; set; }
        public virtual long IdProductoST { get; set; }
        public virtual string NroRemito { get; set; }
        public virtual string FallaReportada { get; set; }
        public virtual long IdEstadoActual { get; set; }
        public virtual long IdWorkflow { get; set; }
        public virtual string Observaciones { get; set; }
        public virtual string NroOrden { get; set; }
        public virtual bool Garantia { get; set; }
        public virtual string Laboratorio { get; set; }

        #region IEntidad Members
        public virtual long IdUsuarioModifico { get; set; }
        public virtual DateTime FecUltimaModificacion { get; set; }
        #endregion

        #region Relación con otros objetos
        public virtual ProductoST oProductoST { get; set; }
        public virtual Workflow_Estado oEstadoActual { get; set; }
        public virtual Clientes oCliente { get; set; }
        public virtual Usuario oUsuario { get; set; }
        public virtual Workflow oWorkflow { get; set; }
        #endregion

        public virtual string Cliente
        {
            get
            {
                return oCliente.Nombre;
            }

        }

        public virtual string NroSerie
        {
            get
            {
                return oProductoST.NroSerie;
            }

        }

        public virtual string NroGabinete
        {
            get
            {
                return oProductoST.NroGabinete;
            }

        }

        public virtual string Producto
        {
            get
            {
                return oProductoST.oTipoProducto.Nombre;
            }

        }

        public virtual string EstadoActual
        {
            get
            {
                if (IdEstadoActual != 0)
                    return oEstadoActual.Nombre;
                else
                    return "0";
            }

        }

        public virtual string FechaIngreso
        {
            get
            {
                if (this.FecUltimaModificacion == DateTime.MinValue)
                    return " ";
                else
                    return FecUltimaModificacion.ToShortDateString();
            }

        }
    }
}
