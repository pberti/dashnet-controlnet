﻿using System;


namespace IOT.Domain
{
    public class OTs_Excel
    {
        public virtual string Proyecto_Nombre
        {
            get;
            set;
        }

        public virtual string Proyecto_CC
        {
            get;
            set;
        }

        public virtual string Proyecto_Padre_Nombre
        {
            get;
            set;
        }

        public virtual string Version_Nombre
        {
            get;
            set;
        }

        public virtual string Version_Fecha_Inicio
        {
            get;
            set;
        }

        public virtual string Version_Fecha_Est_FiN
        {
            get;
            set;
        }

        public virtual string Version_Fecha_Real_FiN
        {
            get;
            set;
        }

        public virtual string Version_Producto
        {
            get;
            set;
        }

        public virtual int Version_AMS
        {
            get;
            set;
        }

        public virtual string OT_Cant_Total
        {
            get;
            set;
        }

        public virtual string OT_Cant_Abiertas
        {
            get;
            set;
        }

        public virtual string OT_Cant_Pendientes
        {
            get;
            set;
        }

        public virtual string OT_Cant_Progreso
        {
            get;
            set;
        }

        public virtual string OT_Cant_Finalizadas
        {
            get;
            set;
        }

        public virtual string OT_Cant_AprobCliente
        {
            get;
            set;
        }

        public virtual string OT_Cant_Cerradas
        {
            get;
            set;
        }

        public virtual string OT_Porc_Cerradas
        {
            get;
            set;
        }

        public virtual string OT_Horas_Estimadas_Total
        {
            get;
            set;
        }

        public virtual string OT_Horas_Usadas_Total
        {
            get;
            set;
        }

        public virtual string OT_Horas_Porc_Consumido
        {
            get;
            set;
        }

        public virtual string Version_Cantidad
        {
            get;
            set;
        }

        public virtual string Version_FechaInicio
        {
            get;
            set;
        }

        public virtual string Version_FechaEstFinal
        {
            get;
            set;
        }

        public virtual string Version_FechaFinal
        {
            get;
            set;
        }

        public virtual string Version_AprobCliente
        {
            get;
            set;
        }
    }
}
