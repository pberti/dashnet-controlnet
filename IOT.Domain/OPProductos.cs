﻿using System;

namespace IOT.Domain
{
    public class OpProductos : IEntidad
    {
        public virtual long Id { get; set; }
        public virtual long IdOP { get; set; }
        public virtual long IdMaestroProducto { get; set; }
        public virtual int Cantidad { get; set; }
        public virtual string Comentario { get; set; }
        public virtual long IdWorkflow { get; set; }
        public virtual string OpcionMontaje { get; set; }

        #region IEntidad Members
        public virtual long IdUsuarioModifico { get; set; }
        public virtual DateTime FecUltimaModificacion { get; set; }
        #endregion

        #region Relaciones con Otros Objetos
        public virtual OrdenProd oOP { get; set; }
        public virtual Usuario oUsuario { get; set; }
        public virtual TipoProducto oTipoProducto { get; set; }
        public virtual Workflow oWorkflow { get; set; }
        #endregion
    }
}
