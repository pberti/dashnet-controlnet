﻿using System;

namespace IOT.Domain
{
    public class Soporte_Estados
    {
        public virtual long Id { get; set; }
        public virtual string Nombre { get; set; }
        public virtual int NroOrden { get; set; }
    }
}
