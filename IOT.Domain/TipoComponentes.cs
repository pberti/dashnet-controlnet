﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IOT.Domain
{
    public class TipoComponentes : IEntidad
    {
        public virtual long Id { get; set; }
        public virtual string Nombre { get; set; }
        public virtual string Descripcion { get; set; }
        public virtual bool Inactivo { get; set; }
        public virtual bool RequiereNroSerie { get; set; }
        public virtual DateTime InactivoFecha { get; set; }
        public virtual string InactivoComentario { get; set; }

        public virtual long IdUsuarioModifico { get; set; }
        public virtual DateTime FecUltimaModificacion { get; set; }
    }
}
