﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOT.Domain
{
    public class FallasXProducto
    {
        public virtual long Id { get; set; }
        public virtual long IdFalla { get; set; }
        public virtual long IdTipoProducto { get; set; }

        #region Relaciones con otros objetos
        public virtual STFalla oFalla { get; set; }
        public virtual TipoProducto oProducto { get; set; }
        #endregion


        public virtual string Nombre
        {
            get
            {
                return oProducto.Nombre;
            }

        }

        public virtual string NombreFalla
        {
            get
            {
                return oFalla.Nombre;
            }

        }
    }
}
