﻿using System;

namespace IOT.Domain
{
   public class Clientes : IEntidad
    {
        public virtual long Id { get; set; }
        public virtual string Nombre { get; set; }

        public virtual string Cuit { get; set; }
        public virtual string Provincia { get; set; }
        public virtual string Contacto { get; set; }
        public virtual string Domicilio { get; set; }
        public virtual string Telefono { get; set; }
        public virtual string Correo { get; set; }
        public virtual string Comentario { get; set; }
        #region Inactivo
        public virtual bool Inactivo { get; set; }
        public virtual DateTime InactivoFecha { get; set; }
        public virtual string InactivoComentario { get; set; }
        #endregion

        #region IEntidad Members
        public virtual long IdUsuarioModifico { get; set; }
        public virtual DateTime FecUltimaModificacion { get; set; }
        #endregion
    }
}
