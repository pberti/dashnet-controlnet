﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IOT.Domain
{
    [Serializable()]
    public class TipoProducto : IEntidad
    {
        public virtual long Id { get; set; }
        public virtual string Nombre { get; set; }
        public virtual string Descripcion { get; set; }
        public virtual string CodigoProducto { get; set; }

        public virtual bool Inactivo { get; set; }
        public virtual DateTime InactivoFecha { get; set; }
        public virtual string InactivoComentario { get; set; }

        public virtual long IdUsuarioModifico { get; set; }
        public virtual DateTime FecUltimaModificacion { get; set; }
    }
}
