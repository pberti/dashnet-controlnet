﻿using System;

namespace IOT.Domain
{
    public class OTs_Estados
    {
        public virtual long Id { get; set; }
        public virtual long Id_OTDash { get; set; }
        public virtual string OT { get; set; }
        public virtual long Id_Estado_Inicial { get; set; }
        public virtual string Estado_Inicial { get; set; }
        public virtual int Nro_Orden_Inicial { get; set; }
        public virtual long Id_Estado_Actual { get; set; }
        public virtual string Estado_Actual { get; set; }
        public virtual int Nro_Orden_Actual { get; set; }
        public virtual DateTime Fecha_Creacion { get; set; }
        public virtual long Id_Usuario { get; set; }
        public virtual string Usuario { get; set; }

    }
}
