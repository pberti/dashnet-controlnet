﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOT.Domain
{
    public class ProductoST
    {
        public virtual long Id { get; set; }
        public virtual long IdTipoProducto { get; set; }
        public virtual long IdProduccion { get; set; }
        public virtual string NroSerie { get; set; }
        public virtual string NroGabinete { get; set; }

        public virtual TipoProducto oTipoProducto { get; set; }
        public virtual Produccion oProduccion { get; set; }

        public virtual string Producto
        {
            get
            {
                return oTipoProducto.Nombre;
            }

        }


    }
}
