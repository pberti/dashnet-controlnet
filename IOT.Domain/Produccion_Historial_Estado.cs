﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOT.Domain
{
   public  class Produccion_Historial_Estado: IEntidad
    {
        public virtual long Id { get; set; }
        public virtual long IdProduccion { get; set; }
        public virtual long IdNuevoEstado { get; set; }
        public virtual long IdEstadoActual { get; set; }
        public virtual string Comentario { get; set; }
        public virtual long IdFormulario { get; set; }
        public virtual long IdFormrespuestas { get; set; }


        #region IEntidad Members
        public virtual long IdUsuarioModifico { get; set; }
        public virtual DateTime FecUltimaModificacion { get; set; }
        #endregion

        #region Relaciones con Otros Objetos
        public virtual Produccion oProduccion { get; set; }
        public virtual Workflow_Estado oEstadoActual { get; set; }
        public virtual Usuario oUsuario { get; set; }
        public virtual Workflow_Estado oNuevoEstado { get; set; }
        public virtual Formulario oFormulario { get; set; }
        public virtual FormRespuesta oFormrespuestas { get; set; }
        #endregion

        public virtual string FechaUltimaModificacion
        {
            get
            {
                return FecUltimaModificacion.ToShortDateString();
            }
        }

        public virtual string NombreeEstadoActual
        {
            get
            {
                if (IdEstadoActual == 0)
                    return "-NO HAY-";
                else
                    return oEstadoActual.Nombre;
            }
        }
    }
}
