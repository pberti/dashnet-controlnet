﻿using System;


namespace IOT.Domain
{
    public class RedmineUser
    {
        public virtual long Id
        {
            get;
            set;
        }

        public virtual string firstname
        {
            get;
            set;
        }

        public virtual string lastname
        {
            get;
            set;
        }

        public virtual string Semana
        {
            get;
            set;
        }

        public virtual string Cantidad
        {
            get;
            set;
        }

        public virtual string NombreYApellido
        {
            get
            {
                return this.firstname + " " + this.lastname;
            }
        }
    }
}
