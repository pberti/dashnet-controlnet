﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace IOT.Domain
{
    public class Menues : IEntidad
    {
        [Required(ErrorMessageResourceName = "valRequerido", ErrorMessageResourceType = typeof(Resources))]
        public virtual long Id { get; set; }
        public virtual long IdUsuarioModifico { get; set; }
        public virtual DateTime FecUltimaModificacion { get; set; }

        [Required(ErrorMessageResourceName = "valRequerido", ErrorMessageResourceType = typeof(Resources))]
        [StringLength(50, ErrorMessageResourceName = "valTam50", ErrorMessageResourceType = typeof(Resources))]
        public virtual string Nombre { get; set; }

        [StringLength(255, ErrorMessageResourceName = "valTam255", ErrorMessageResourceType = typeof(Resources))]
        public virtual string Descripcion { get; set; }

        [StringLength(255, ErrorMessageResourceName = "valTam255", ErrorMessageResourceType = typeof(Resources))]
        public virtual string PathPagina { get; set; }

        [Required(ErrorMessageResourceName = "valRequerido", ErrorMessageResourceType = typeof(Resources))]
        [StringLength(10, ErrorMessageResourceName = "valTam10", ErrorMessageResourceType = typeof(Resources))]
        public virtual string Posicion { get; set; }

        public virtual long IdMenuPadre { get; set; }

        public virtual Menues oMenuPadre { get; set; }

        public virtual bool PuedeGrabar { get; set; }
        public virtual bool PuedeBorrar { get; set; }

        //No Mapeado
        public virtual bool Seleccionado { get; set; }

    }
}
