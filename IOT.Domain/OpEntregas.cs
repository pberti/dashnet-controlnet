﻿using System;

namespace IOT.Domain
{
    public class OpEntregas
    {
        public virtual long Id { get; set; }
        public virtual long IdOP { get; set; }
        public virtual string Comentario { get; set; }

        public virtual DateTime FechaEstimada { get; set; }
        public virtual DateTime FechaReal { get; set; }
        public virtual long IdusuarioCerro{ get; set; }
        public virtual string ComentarioCierre { get; set; }

        public virtual string EstadoEntrega { get; set; } //Pendiente - En Curso - Finalizada - Suspendida - Cancelada


        #region Relaciones con Otros Objetos
        public virtual OrdenProd oOP { get; set; }
        public virtual Usuario oUsuario { get; set; }
        public virtual OpEntregasEstado oEstadoEntrega { get; set; }
        #endregion


        public virtual string FechaEstimadaSinHora
        {
            get
            {
                return FechaEstimada.ToShortDateString();
            }
        }

        public virtual string FechaRealSinHora
        {
            get
            {
                return FechaReal.ToShortDateString();
            }
        }
    }
}
