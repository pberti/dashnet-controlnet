﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOT.Domain
{
    public class OpEstados 
    {
        public virtual long Id { get; set; }
        public virtual string Nombre { get; set; }
        public virtual int NroOrden { get; set; }
        public virtual bool AceptaModificaciones { get; set; }
        public virtual bool EstadoInicial { get; set; }
        public virtual bool EstadoFinal { get; set; }
    }
}
