﻿using System;

namespace IOT.Domain
{
    public class RedmineHoras
    {
        public virtual long IdUsuario
        {
            get;
            set;
        }

        public virtual string firstname
        {
            get;
            set;
        }

        public virtual string lastname
        {
            get;
            set;
        }

        public virtual DateTime Fecha
        {
            get;
            set;
        }

        public virtual long Proyecto_Id
        {
            get;
            set;
        }

        public virtual string Proyecto_Nombre
        {
            get;
            set;
        }

        public virtual string CC
        {
            get;
            set;
        }

        public virtual double Horas
        {
            get;
            set;
        }

        public virtual double Porcentaje
        {
            get;
            set;
        }

        public virtual double INP
        {
            get;
            set;
        }

        public virtual double IPI
        {
            get;
            set;
        }

        public virtual double PHI
        {
            get;
            set;
        }

        public virtual double PHP
        {
            get;
            set;
        }

        public virtual double SOP
        {
            get;
            set;
        }

        public virtual double SSCC
        {
            get;
            set;
        }

        public virtual double STP
        {
            get;
            set;
        }

        public virtual double CCTOTAL
        {
            get;
            set;
        }

        public virtual string NombreYApellido
        {
            get
            {
                return this.firstname + " " + this.lastname;
            }
        }

        public virtual string _INP
        {
            get
            {
                return this.INP.ToString("#0.00;(#0.00)");
            }
        }

        public virtual string _SSCC
        {
            get
            {
                return this.SSCC.ToString("#0.00;(#0.00)");
            }
        }

        public virtual string _IPI
        {
            get
            {
                return this.IPI.ToString("#0.00;(#0.00)");
            }
        }

        public virtual string _PHI
        {
            get
            {
                return this.PHI.ToString("#0.00;(#0.00)");
            }
        }

        public virtual string _PHP
        {
            get
            {
                return this.PHP.ToString("#0.00;(#0.00)");
            }
        }

        public virtual string _CCTOTAL
        {
            get
            {
                return this.CCTOTAL.ToString("#0.00;(#0.00)");
            }
        }

        public virtual string _SOP
        {
            get
            {
                return this.SOP.ToString("#0.00;(#0.00)");
            }
        }

        public virtual string _STP
        {
            get
            {
                return this.STP.ToString("#0.00;(#0.00)");
            }
        }

        public virtual string _Porcentaje
        {
            get
            {
                return this.Porcentaje.ToString("#,0.000#;(#,0.000#)") + "%";
            }
        }

        public virtual string _Horas
        {
            get
            {
                return this.Horas.ToString("#,0.000#;(#,0.000#)");
            }
        }
    }
}
