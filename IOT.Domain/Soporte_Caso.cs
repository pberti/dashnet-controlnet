﻿using System;

namespace IOT.Domain
{
    public class Soporte_Caso
    {
        public enum MedioComunicacion { Telefono, WhatsApp, Mail };

        public virtual long Id { get; set; }

        public virtual DateTime Fecha { get; set; }
        public virtual string NroCaso { get; set; }
        public virtual string NroReclamo { get; set; }
        public virtual long IdEstadoCaso { get; set; }
        public virtual long IdCliente { get; set; }
        public virtual string Contacto { get; set; }
        public virtual string ContactoEmail { get; set; }
        
        public virtual string NroConsola { get; set; }
        public virtual long IdProducto { get; set; }
        public virtual long IdTipoProducto { get; set; }
        public virtual MedioComunicacion MedioContacto { get; set; }
        public virtual string Comentario { get; set; }
        public virtual long IdResponsable { get; set; }

        public virtual long IdCategoria { get; set; }
        public virtual long IdSubCategoria { get; set; }
        public virtual long IdResolucion { get; set; }

        public virtual Clientes oCliente { get; set; }
        public virtual Usuario oUsuario { get; set; }
        public virtual Soporte_Estados oEstado { get; set; }
        public virtual Soporte_Categoria oCategoriaPadre { get; set; }
        public virtual Soporte_Resolucion oResolucion { get; set; }
        public virtual TipoProducto oTipoProducto { get; set; }

        public virtual string _Fecha
        {
            get
            {
                return Fecha.ToShortDateString();
            }

        }
    }
}
