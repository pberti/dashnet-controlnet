﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IOT.Domain
{
    public class TipoPerfil : IEntidad
    {
        public virtual long Id { get; set; }

        [Required(ErrorMessageResourceName = "valRequerido", ErrorMessageResourceType = typeof(Resources))]
        [StringLength(10, ErrorMessageResourceName = "valTam10", ErrorMessageResourceType = typeof(Resources))]
        public virtual string Perfil { get; set; }

        [StringLength(45, ErrorMessageResourceName = "valTam45", ErrorMessageResourceType = typeof(Resources))]
        public virtual string Descripcion { get; set; }

        public virtual long IdUsuarioModifico { get; set; }
        public virtual DateTime FecUltimaModificacion { get; set; }
        public virtual IList<Usuario> listUsuarios { get; set; }
        public virtual IList<TipoPerfilMenues> listTipoPerfilMenues { get; set; }
    }
}
