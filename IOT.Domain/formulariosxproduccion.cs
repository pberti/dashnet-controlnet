﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOT.Domain
{
    public class FormulariosXProduccion
    {
        public virtual long Id { get; set; }
        public virtual long IdFormulario { get; set; }
        public virtual long IdProduccion { get; set; }
        public virtual long IdFormRespuestas { get; set; }
        public virtual long IdEstado { get; set; }

        #region Relaciones con otros objetos
        public virtual Formulario oFormulario { get; set; }
        public virtual Produccion oProduccion { get; set; }
        public virtual FormRespuesta oRespuestas { get; set; }
        public virtual Workflow_Estado oEstado { get; set; }
        #endregion
    }
}
