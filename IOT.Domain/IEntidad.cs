﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IOT.Domain
{
    public interface IEntidad
    {
        long Id { get; set; }
        long IdUsuarioModifico { get;  set; }
        DateTime FecUltimaModificacion { get; set; }
    }
}
