﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOT.Domain
{
    public class Produccion_Componentes
    {

        public virtual long Id { get; set; }
        public virtual long IdProduccion { get; set; }
        public virtual long IdComponente { get; set; }
        public virtual int NroOrden { get; set; }
        public virtual string NroSerie { get; set; }

        #region Relaciones con Otros Objetos
        public virtual Produccion oProduccion { get; set; }
        public virtual TipoComponentes oComponente { get; set; }
        #endregion

    }
}
