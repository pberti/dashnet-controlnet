﻿using System;

namespace IOT.Domain
{
    public class Soporte_Historial
    {
        public virtual long Id { get; set; }
        public virtual long IdSoporteCaso { get; set; }
        public virtual DateTime Fecha { get; set; }
        public virtual string Comentario { get; set; }
        public virtual long IdUsuario { get; set; }
        public virtual long IdEstado { get; set; }
        public virtual long IdResolucion { get; set; }

        public virtual Usuario oUsuario { get; set; }
        public virtual Soporte_Estados oEstado { get; set; }
        public virtual Soporte_Resolucion oResolucion { get; set; }


    }
}
