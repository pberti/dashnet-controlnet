﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOT.Domain
{
    public class TempExcel
    {
        public virtual long Id { get; set; }
        public virtual string Cliente { get; set; }
        public virtual string Cuit { get; set; }
        public virtual string Provincia { get; set; }
        public virtual string Contacto { get; set; }
        public virtual string Domicilio { get; set; }
        public virtual string Telefono { get; set; }
        public virtual string Correo { get; set; }
        public virtual string NroSerie { get; set; }
    }
}
