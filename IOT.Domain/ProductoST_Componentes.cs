﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOT.Domain
{
   public class ProductoST_Componentes
    {
        public virtual long Id { get; set; }
        public virtual long IdProductoST { get; set; }
        public virtual long IdComponente { get; set; }
        public virtual int NroOrden { get; set; }
        public virtual string NroSerie { get; set; }

        public virtual ProductoST oProductoST { get; set; }
        public virtual TipoComponentes oComponente { get; set; }
    }
}
