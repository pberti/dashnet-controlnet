﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace IOT.Domain
{
    public class FormCampo : IEntidad
    {
        public virtual long Id { get; set; }
        public virtual long IdFormulario { get; set; }
        public virtual int Posicion { get; set; }
        public virtual string TipoCampo { get; set; }
        public virtual string Pregunta { get; set; }
        public virtual string OpcionesRespuesta { get; set; }
        public virtual bool EsRequerido { get; set; }
        public virtual bool ConComentarioAdicional { get; set; }

        public virtual bool Inactivo { get; set; }
        public virtual DateTime FecInactivo { get; set; }
        public virtual string MotivoInactivo { get; set; }
        public virtual DateTime FecUltimaModificacion { get; set; }
        public virtual long IdUsuarioModifico { get; set; }

        public virtual string Requerido { get { return EsRequerido ? "Si" : "No"; } }
        public virtual List<string> lOpcionesRespuesta
        {
            get
            {
                List<string> lOpRespuestas = new List<string>();
                if (TipoCampo == ValoresConstantes.OptFormCampo_SeleccionMultiple || TipoCampo == ValoresConstantes.OptFormCampo_SeleccionUnica)
                {
                    string[] aOptRespuesta = this.OpcionesRespuesta.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
                    foreach (string opcion in aOptRespuesta)
                    {
                        lOpRespuestas.Add(opcion);
                    }
                }
                else if (TipoCampo == ValoresConstantes.OptFormCampo_VerdaderoFalso)
                    lOpRespuestas = ValoresConstantes.lOptsVF;
                else if (TipoCampo == ValoresConstantes.OptFormCampo_Parrafo || TipoCampo == ValoresConstantes.OptFormCampo_TextoCorto)
                    lOpRespuestas.Add(OpcionesRespuesta);
                else if (TipoCampo == ValoresConstantes.OptFormCampo_Valoracion)
                {
                    lOpRespuestas = ObtenerSecuencia(OpcionesRespuesta);
                }
                return lOpRespuestas;
            }
        }

        private List<string> ObtenerSecuencia(string pPatron)
        {
            try
            {
                List<string> lOpRespuestas = new List<string>();
                string[] aOptRespuesta = pPatron.Split(':');
                if (aOptRespuesta.Length == 3)
                {
                    double inicioRango = double.Parse(aOptRespuesta[0]);
                    double finRango = double.Parse(aOptRespuesta[1]);
                    double paso = double.Parse(aOptRespuesta[2]);
                    if (inicioRango < finRango && paso <= (finRango - inicioRango))
                    {
                        for (double i = inicioRango; i <= finRango; i += paso)
                        {
                            lOpRespuestas.Add(i.ToString());
                        }
                    }
                    else lOpRespuestas.Add(0.ToString());
                }
                else lOpRespuestas.Add(0.ToString());
                return lOpRespuestas;
            }
            catch (Exception ex)
            {
                throw new Exception("Error en el método ObtenerSecuencia", ex);
            }
        }
    }
}
