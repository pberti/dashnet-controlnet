﻿using System;
using System.ComponentModel.DataAnnotations;

namespace IOT.Domain
{
    public class TipoPerfilMenues : IEntidad
    {
        [Required(ErrorMessageResourceName = "valRequerido", ErrorMessageResourceType = typeof(Resources))]
        public virtual long Id
        {
            get;
            set;
        }

        public virtual long IdUsuarioModifico
        {
            get;
            set;
        }

        public virtual DateTime FecUltimaModificacion
        {
            get;
            set;
        }

        public virtual long IdTipoPerfil
        {
            get;
            set;
        }

        public virtual long IdMenu
        {
            get;
            set;
        }

        public virtual TipoPerfil oTipoPerfil
        {
            get;
            set;
        }

        public virtual Menues oMenu
        {
            get;
            set;
        }

        public virtual bool PuedeBorrar
        {
            get;
            set;
        }

        public virtual bool PuedeGrabar
        {
            get;
            set;
        }
    }
}
