﻿using System;
using System.ComponentModel.DataAnnotations;

namespace IOT.Domain
{
    [Serializable()]
    public class Usuario : IEntidad
    {

        public virtual long Id { get; set; }
        [Required(ErrorMessageResourceName = "valRequerido", ErrorMessageResourceType = typeof(Resources))]
        [StringLength(100, ErrorMessageResourceName = "valTam100", ErrorMessageResourceType = typeof(Resources))]

        public virtual string Nombre { get; set; }
        [Required(ErrorMessageResourceName = "valRequerido", ErrorMessageResourceType = typeof(Resources))]
        [StringLength(100, ErrorMessageResourceName = "valTam100", ErrorMessageResourceType = typeof(Resources))]

        public virtual string Apellido { get; set; }
        [Required(ErrorMessageResourceName = "valRequerido", ErrorMessageResourceType = typeof(Resources))]
        [StringLength(100, ErrorMessageResourceName = "valTam100", ErrorMessageResourceType = typeof(Resources))]

        public virtual string Email { get; set; }

        [Required(ErrorMessageResourceName = "valRequerido", ErrorMessageResourceType = typeof(Resources))]
        [StringLength(100, ErrorMessageResourceName = "valTam100", ErrorMessageResourceType = typeof(Resources))]

        public virtual string UserName { get; set; }
        [Required(ErrorMessageResourceName = "valRequerido", ErrorMessageResourceType = typeof(Resources))]
        [StringLength(100, ErrorMessageResourceName = "valTam100", ErrorMessageResourceType = typeof(Resources))]

        public virtual string Password { get; set; }
        public virtual long IdTipoPerfil { get; set; }
        public virtual TipoPerfil oTipoPerfil { get; set; }
        public virtual string URLImagenUsuario { get; set; }
        public virtual bool? Activo { get; set; }
        public virtual DateTime FecInactivo { get; set; }
        public virtual string MotivoInactivo { get; set; }
        
        public virtual bool PuedeAsignarActividades { get; set; }

        #region IEntidad Members
        public virtual long IdUsuarioModifico { get; set; }
        public virtual DateTime FecUltimaModificacion { get; set; }
        #endregion

        #region Apellido Nombre
        //Se usa para mostrar el nombre y apellido en la vista de mensajes recibidos y enviados
        public virtual string ApellidoYNombre { get; set; }

        public virtual string NombreYApellido
        {
            get
            {
                return Nombre + " " + Apellido;
            }
        }

        public virtual string ApellidoYNombre2
        {

            get
            {

                return Apellido + " " + Nombre;
            }

        }

        #endregion

    }
}