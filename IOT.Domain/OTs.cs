﻿using System;

namespace IOT.Domain
{
    public class OTs : IEntidad
    {
        public virtual long Id
        {
            get;
            set;
        }

        public virtual long Id_OT
        {
            get;
            set;
        }

        public virtual string OT_Asunto
        {
            get;
            set;
        }

        public virtual string OT_Horas_Estimadas
        {
            get;
            set;
        }

        public virtual long Id_Estado
        {
            get;
            set;
        }

        public virtual string OT_Estado
        {
            get;
            set;
        }

        public virtual string OT_Cerrada
        {
            get;
            set;
        }

        public virtual DateTime OT_Fecha_Cierre
        {
            get;
            set;
        }

        public virtual string Proyecto_Nombre
        {
            get;
            set;
        }

        public virtual string Proyecto_CC
        {
            get;
            set;
        }

        public virtual string Proyecto_Descipcion
        {
            get;
            set;
        }

        public virtual string Proyecto_Padre_Nombre
        {
            get;
            set;
        }

        public virtual string Version_Proyecto
        {
            get;
            set;
        }

        public virtual string Version_Nombre
        {
            get;
            set;
        }

        public virtual DateTime Version_Fecha_Inicio
        {
            get;
            set;
        }

        public virtual DateTime Version_Fecha_Est_FiN
        {
            get;
            set;
        }

        public virtual DateTime Version_Fecha_Real_FiN
        {
            get;
            set;
        }

        public virtual string Version_Producto
        {
            get;
            set;
        }

        public virtual string Version_Id
        {
            get;
            set;
        }

        public virtual string Version_EstadoClientes
        {
            get;
            set;
        }

        public virtual int Version_AMS
        {
            get;
            set;
        }

        public virtual long OT_Cant_Total
        {
            get;
            set;
        }

        public virtual long OT_Cant_Abiertas
        {
            get;
            set;
        }

        public virtual long OT_Cant_Pendientes
        {
            get;
            set;
        }

        public virtual long OT_Cant_Progreso
        {
            get;
            set;
        }

        public virtual long OT_Cant_Finalizadas
        {
            get;
            set;
        }

        public virtual long OT_Cant_AprobCliente
        {
            get;
            set;
        }

        public virtual long OT_Cant_Cerradas
        {
            get;
            set;
        }

        public virtual string OT_Porc_Cerradas
        {
            get;
            set;
        }

        public virtual string OT_Horas_Estimadas_Total
        {
            get;
            set;
        }

        public virtual string OT_Horas_Usadas_Total
        {
            get;
            set;
        }

        public virtual string OT_Horas_Usadas_Mes
        {
            get;
            set;
        }

        public virtual string OT_Horas_Porc_Consumido
        {
            get;
            set;
        }

        public virtual long Version_Cantidad
        {
            get;
            set;
        }

        public virtual string Version_FechaInicio
        {
            get;
            set;
        }

        public virtual string Version_FechaEstFinal
        {
            get;
            set;
        }

        public virtual string Version_FechaFinal
        {
            get;
            set;
        }

        public virtual bool Version_Cerrada_Ok
        {
            get;
            set;
        }

        public virtual string Version_AprobCliente
        {
            get;
            set;
        }

        public virtual TimeSpan Demora_Ticket_Aceptada_Cumplimentada
        {
            get;
            set;
        }
        public virtual string _Demora_Ticket_Aceptada_Cumplimentada
        {
            get
            {
                return Demora_Ticket_Aceptada_Cumplimentada.TotalHours.ToString();
            }
        }

        public virtual long IdUsuarioModifico
        {
            get;
            set;
        }

        public virtual DateTime FecUltimaModificacion
        {
            get;
            set;
        }

        public virtual string _Version_AprobCliente
        {
            get
            {
                bool flag = this.Version_AprobCliente.Length > 3;
                string result;
                if (flag)
                {
                    result = this.Version_AprobCliente.Substring(0, 4);
                }
                else
                {
                    result = this.Version_AprobCliente;
                }
                return result;
            }
        }
    }
}
