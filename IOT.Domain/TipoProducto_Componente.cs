﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IOT.Domain
{
    public class TipoProducto_Componente : IEntidad
    {
        public virtual long Id { get; set; }
        public virtual long IdProducto { get; set; }
        public virtual long IdComponente { get; set; }

        public virtual int Cantidad { get; set; }
        public virtual string Comentario { get; set; }

        public virtual TipoProducto oProducto { get; set; }
        public virtual TipoComponentes oComponentes { get; set; }
        public virtual Usuario oUsuario { get; set; }

        public virtual long IdUsuarioModifico { get; set; }
        public virtual DateTime FecUltimaModificacion { get; set; }
    }
}
