﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOT.Domain
{
    public class Formulario : IEntidad
    {
        public virtual long Id { get; set; }
        public virtual string Titulo { get; set; }
        public virtual string Comentario { get; set; }
        public virtual bool Inactivo { get; set; }
        public virtual DateTime FecInactivo { get; set; }
        public virtual string MotivoInactivo { get; set; }
        public virtual DateTime FecUltimaModificacion { get; set; }
        public virtual long IdUsuarioModifico { get; set; }

        public virtual IList<FormCampo> lFormCampos { get; protected set; }
        
        public Formulario()
        {
            lFormCampos = new List<FormCampo>();
        }

        public virtual string EsInactivo { get { return Inactivo.ToString();  } }
        //public virtual string EsInactivo { get { return Inactivo ? ValoresConstantes.OpcionInactivo : ValoresConstantes.OpcionActivo; } }
    }
}
