﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace IOT.Domain
{
    public class FormRespuesta : IEntidad
    {
        public virtual long Id { get; set; }
        public virtual long IdFormulario { get; set; }
        public virtual long IdUsuarioRespondio { get; set; }
        public virtual Usuario oUsuarioRespondio { get; set; }
        public virtual bool Inactivo { get; set; }
        public virtual DateTime FecInactivo { get; set; }
        public virtual string MotivoInactivo { get; set; }
        public virtual DateTime FecUltimaModificacion { get; set; }
        public virtual long IdUsuarioModifico { get; set; }
        public virtual DateTime FecAlta { get; set; }
        public virtual Formulario oFormulario { get; set; }

        public virtual IList<FormRespuestaCampo> lFormRespuestasCampos { get; protected set; }
        public virtual List<FormRespuestaCampo> lRespuestas { get { return lFormRespuestasCampos.Where(frc => frc.oFormCampo.Inactivo == false).ToList(); } }

        public virtual string FechaAlta { get { return FecAlta.ToShortDateString(); } }
        public virtual string FechaUltimaModificación { get { return FecUltimaModificacion.ToShortDateString(); } }

        public FormRespuesta()
        {
            this.lFormRespuestasCampos = new List<FormRespuestaCampo>();
        }
    }
}
