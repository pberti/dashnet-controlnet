﻿namespace IOT.ORM
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Linq.Expressions;
    using NHibernate;

    /// <summary>
    /// Interface de repositorio generico
    /// </summary>
    public interface IRepositorio<T> : IDisposable where T : class
    {
        #region métodos para contar

        /// <summary>
        /// Cuenta la cantidad de entidades
        /// </summary>
        /// <typeparam name="T">Tipo</typeparam>
        /// <returns>Cuenta</returns>
        int Cuenta();

        /// <summary>
        /// Cuenta usando un filtro
        /// </summary>
        /// <typeparam name="T">Tipo</typeparam>
        /// <param name="expression">Exresion</param>
        /// <returns>Cuenta</returns>
        int Cuenta(Expression<Func<T, bool>> expression);

        #endregion

        #region métodos de consulta

        T TraerPorId(long id);

        /// <summary>
        /// Devuelve todas las entidades
        /// </summary>
        /// <typeparam name="T">Tipo</typeparam>
        /// <returns></returns>
        IQueryable<T> TraerTodos();

        /// <summary>
        /// Devuelve todas las entidades usando un filtro
        /// </summary>
        /// <typeparam name="T">Tipo</typeparam>
        /// <param name="expression">Expresion</param>
        /// <returns></returns>
        IQueryable<T> TraerTodos(Expression<Func<T, bool>> expression);

        /// <summary>
        /// Devuelve una unica entidad
        /// </summary>
        /// <typeparam name="T">Tipo</typeparam>
        /// <param name="expression">Expresion</param>
        /// <returns></returns>
        T TraerUnico(Expression<Func<T, bool>> expression);

        #endregion

        #region métodos de ABM

        /// <summary>
        /// Actualiza una entidad existente
        /// </summary>
        /// <typeparam name="T">Tipo</typeparam>
        /// <param name="entity">Entidad</param>
        /// <returns>Cantidad de entidades afectadas</returns>
        int Actualizar(T entity);

        /// <summary>
        /// Agrega una nueva entidad
        /// </summary>
        /// <typeparam name="T">Tipo</typeparam>
        /// <param name="entity">Entidad</param>
        /// <returns></returns>
        int Agregar(T entity);

        /// <summary>
        /// Borra una entidad existente
        /// </summary>
        /// <typeparam name="T">Tipo</typeparam>
        /// <param name="entity">Entidad</param>
        /// <returns>Cantidad de entidades afectadas</returns>
        int Borrar(T entity);

        #endregion
    }
}
