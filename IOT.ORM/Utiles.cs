﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Threading;

namespace IOT.ORM
{
    public static class Utiles
    {
        public static byte[] StringToByteArray(string str)
        {
            System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            return encoding.GetBytes(str);
        }

        public static string ByteArrayToString(byte[] pBytes)
        {
            if (pBytes != null)
            {
                string s = System.Text.ASCIIEncoding.ASCII.GetString(pBytes);
                return s;
            }
            else
            {
                return string.Empty;
            }
        }

        public static bool StringToBoolean(string str)
        {
            if (str == "T" || str == "True")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static IEnumerable<T> SelectRecursive<T>(this IEnumerable<T> subjects, Func<T, IEnumerable<T>> selector)
        {
            // Stop if subjects are null or empty 
            if (subjects == null)
                yield break;

            // For each subject 
            foreach (var subject in subjects)
            {
                // Yield it 
                yield return subject;

                // Then yield all its decendants 
                foreach (var decendant in SelectRecursive(selector(subject), selector))
                    yield return decendant;
            }
        }

        public static string ProperCase(string stringInput)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            bool fEmptyBefore = true;
            foreach (char ch in stringInput)
            {
                char chThis = ch;
                if (Char.IsWhiteSpace(chThis))
                    fEmptyBefore = true;
                else
                {
                    if (Char.IsLetter(chThis) && fEmptyBefore)
                        chThis = Char.ToUpper(chThis);
                    else
                        chThis = Char.ToLower(chThis);
                    fEmptyBefore = false;
                }
                sb.Append(chThis);
            }
            return sb.ToString();
        }
    }
}
