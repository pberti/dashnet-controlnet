﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using IOT.Mapping;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using System;

namespace IOT.ORM
{
    public static class SessionHelper
    {
        private static ISessionFactory sessionFectory;

        public static ISessionFactory BuildSessionFactory()
        {

            if (sessionFectory == null)
            {
                try
                {
                    sessionFectory = Fluently.Configure()
                     .Database(MySQLConfiguration
                     .Standard
                     .ShowSql()
                     .QuerySubstitutions("1 true, 0 false")
                        //.Cache(c => c.UseQueryCache().ProviderClass(typeof(NHibernate.Caches.SysCache.SysCacheProvider).AssemblyQualifiedName))
                    .ConnectionString(c => c.
                        FromConnectionStringWithKey
                        ("IOT.ORM.Properties.Settings.ConnectionString")))
                    .Mappings(m => m.FluentMappings.AddFromAssemblyOf<MapUsuario>())
                    .BuildSessionFactory();
                }
                catch (Exception ex)
                {
                    if (ex.InnerException == null)
                    {
                        throw ex;
                    }
                    else
                    {
                        throw ex.InnerException;
                    }
                }
            }

            return sessionFectory;
        }

        private static void BuildSchema(Configuration config)
        {
            new SchemaExport(config).Create(false, true);
        }
    }
}
