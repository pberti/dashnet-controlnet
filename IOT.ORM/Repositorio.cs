﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using FluentNHibernate;
using NHibernate.Linq;

namespace IOT.ORM
{
    public class Repositorio<T> : IRepositorio<T> where T : class
    {
        #region constructores

        public Repositorio()
        {
            SessionFactory = SessionHelper.BuildSessionFactory();
            SessionFactory = SessionHelper.BuildSessionFactory();
            Session = SessionFactory.OpenSession();
        }

        public Repositorio(ISession pSession)
        {
            Session = pSession;
        }

        #endregion

        #region campos privados

        private ISessionFactory SessionFactory;

        public ISession Session;

        #endregion

        #region IRepositorio Members

        public int Cuenta()
        {
            return Session.Linq<T>().Count();
        }

        public int Cuenta(System.Linq.Expressions.Expression<Func<T, bool>> expression)
        {
            return Session.Linq<T>().Where(expression).Count();
        }

        public T TraerPorId(long id)
        {
            return Session.Get<T>(id);
        }

        public IQueryable<T> TraerTodos()
        {
            return Session.Linq<T>();
        }

        public IQueryable<T> TraerTodos(System.Linq.Expressions.Expression<Func<T, bool>> expression)
        {
            return Session.Linq<T>().Where(expression);
        }

        public T TraerUnico(System.Linq.Expressions.Expression<Func<T, bool>> expression)
        {
            return Session.Linq<T>().Where(expression).FirstOrDefault();
        }


        public int Actualizar(T entity)
        {
            try
            {
                Session.Update(Session.Merge(entity));
                Session.Flush();
                Session.Clear();
                return 1;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Agregar(T entity)
        {
            try
            {
                Session.Save(entity);
                Session.Flush();
                Session.Clear();
                return 1;
            }
            catch(Exception e)
            {
                throw;
            }

        }

        public int Borrar(T entity)
        {
            try
            {
                Session.Delete(entity);
                Session.Flush();
                Session.Clear();
                return 1;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            Session.Transaction.Dispose();
            Session.Dispose();
        }

        #endregion
    }
}
